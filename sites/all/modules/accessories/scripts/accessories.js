(function($) {
Drupal.behaviors.accessoriesModule = {
  attach: function (context, settings) {
    //code starts
    
    //jQuery.noConflict();
    
     //$.getScript("/sites/all/themes/meprintvchbxfh/scripts/jquery.tablesorter.min.js", function(){
        //alert("Script loaded but not necessarily executed.");
        //$(".sticky-enabled").tablesorter(); 
     //});
     
     //console.log($('#addScnt'));
     
        function loadScript(url, callback) {
            var script = document.createElement("script")
            script.type = "text/javascript";
            if (script.readyState) { //IE
                script.onreadystatechange = function () {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { //Others
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        }
        
        loadScript("/sites/all/themes/meprintvchbxfh/scripts/jquery.tablesorter.min.js", function () {
            //jQuery loaded
            //console.log('jquery loaded');
            $(".sticky-enabled").tablesorter();
        });
                
                
      $("#edit-productgroup-id").change(function() {
       file_url = "tonic/getsubcategory_list"
               //alert(file_url); 
	  
	  //alert($(this).id);
    });
    /*** add website start ***/
    $("#edit-languages").change(function(){
        var lan = $("#edit-languages").val(); 
        if(lan =="it") {
           $("#website_id").val("meprint.it") 
        } else if(lan =="fr") {
             $("#website_id").val("meprint.fr") 
        }
      
    });
    
    $(".form-select option").each(function(){
        if ($(this).val() == 2 && $(this).html() == "File ricaricato")
            $(this).addClass('red');
    });
    
/*$("#edit-prlanguage").change(function() {
	var plang = $("#edit-prlanguage").val(); 
	alert(plang);
	$.ajax({
		url: "http://localhost/nmeprint/sites/all/themes/meprint/search-lang.php",
		data: { term: plang},
		dataType: "html",
		type: "POST",
		success: function(data){			
			//console.log(data);
			//var yourArray = data.toArray();
			
				//var newj = data.toString();
				//console.log(yourArray);
			$("#pr_groups_update").html(data);
		}		
	});     
});*/

 $(".delete").click(function(event) {
     
     event.preventDefault();
     
       var value = $(this).attr('id');
   var mySplitResult = value.split("/");
  
    var s = mySplitResult[1];
    var s1='';
    if(s == 2) {
        s1+='InActive';
    } else {
         s1+='Active';
    }
    if(confirm("Are You Sure You Want to "+s1+" Record!")) {  

       
         var name = $(this).attr('name');
         var url=''; 
         if(name === "longproduct") {
             url+='ajax/deletelongproduct';
         }else if(name === 'websites') {
             url+='ajax/deletewebsite';
         } else if(name === "templates") {
               url+='ajax/deletetemplate';
         } else if(name === "weight") {
               url+='ajax/deleteweight';
         } else if(name === "price") {
              url+='ajax/deleteprice';
         } else if(name === "dimension") {
              url+='ajax/deletedimensions';
         } else if(name === "extraprice") {
              url+='ajax/deleteextraprice';
         } else if(name === "material") {
              url+='ajax/deletematerial';
         } else if(name === "materialgroup") {
               url+='ajax/deletematerialgroup';
         } else if(name === "productgroup") {
              url+='ajax/deleteproductgroup';
         } else if(name === "productsubgroup") {
             url+='ajax/deleteproductsubgroup';
         } else if(name === "accessory") {
             url+='ajax/deleteaccessories';
         } else if(name === "processingkind") {
             url+='ajax/deleteprocessingkind';
         } else if(name === "processingbig") {
               url+='ajax/deleteprocessingbig';
         } else if(name === "discounts") {
             url+='ajax/deletediscounts';
         } else if(name === "promotionkind") {
             url+='ajax/deletepromotionkind';
         } else if(name === "squote") {
             url+='ajax/deletesmallquote';
         } else if(name === "quote") {
             url+='ajax/deletebigquote';
         } else if(name == "holidays") {
              url+='ajax/deleteholidays';
         } else if(name == "shipping") {
              url+='ajax/deleteshipping';
         }
       
         $.ajax({ 
         url : url,
         type: "POST",
         data: 'name='+name+'&value='+value,
        success:function(result) {  
           //window.location.href='view';
           location.reload();
        },error: function (error) {  
            //window.location.href='view';
            location.reload();
         }    
	   
        }); 
        } 
        else {
          return false;
        }
 });
 
          if($('input[name=fire_proof]:checked').val() == 1 )
            $(".form-item-files-fire-proof-pdf").show();   // checked
         else
           $(".form-item-files-fire-proof-pdf").hide();  // unchecked
       
       
       /////////// Add Material  JS END////////////

        var i = $('#pricecalc p').size() + 1;
        
        $('#addScnt').on('click', function() {
            var scntDiv = '<div id="abc_'+ i +'"><p><label for="p_scnts">From: <input type="text" id="p_scnt_frm'+ i +'" size="20" name="p_scnt_frm' + i +'" value="" placeholder="Input Value" /></label>';
            scntDiv += '<label for="p_scnts">To: <input type="text" id="p_scnt_to'+ i +'" size="20" name="p_scnt_to' + i +'" value="" placeholder="Input Value" /></label>';
            scntDiv += '<label for="p_scnts">Euro: <input type="text" id="p_scnt_price'+ i +'" size="20" name="p_scnt_price' + i +'" value="" placeholder="Input Value" /></label></p></div>';
            $("#hideCount").val(i);
          
            $("#pricecalc").append(scntDiv);
            //$('#p_scnt_frm'+i).attr("disabled","disabled");
            if( i == 1){
               
                //$('input[name=p_scnt_frm'+i+']').change(function() {  });
                $("#p_scnt_frm"+1).attr("value", 1);
                $("#p_scnt_to"+1).attr("value", 999);
            }
            

            
            i++;
            return false;
        });
        
        $('#remScnt').on('click', function() { 
            var j = i-1;
                if( i > 1 ) {
                        //$(this).parents('p').remove();
                        $( "#abc_"+j ).remove();
                        i--;
                }
                 $("#hideCount").val(i-1);
           
                return false;
        });

  }
};
    
})(jQuery);

var iCnt1=0;
function myFunction() {
  
    iCnt1 = iCnt1 + 1;
    var htmls = '';
    htmls +='<br/><input type="text" id=tb' + iCnt1 + ' name=extra_processing[] ><input type="button" value="-" onclick="myFunctionRemove(iCnt1);"  >';
    $('#pricecalc').append(htmls);
    
     
}
function myFunctionRemove(iCnt1) {       
    alert(iCnt1);
         $('tb' + iCnt1).remove();
       /*  if (iCnt1 != 0) { $('tb' + iCnt1).remove(); iCnt1 = iCnt1 - 1; }
        
         if (iCnt1 == 0) { $('#pricecalc').empty(); 
                 $('#pricecalc').remove(); 
                $('.removepricebtn').removeAttr('disabled'); 
               // $('#addId').attr('class', 'bt') 
        } */

}