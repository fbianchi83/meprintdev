<?php
include_once '/home/meprint/sites/all/libraries/tcpdf/tcpdf.php';
include_once '/home/meprint/sites/all/libraries/barcodes/php-barcode.php';
    	
function mp_create_barcodes($orderid, $nome, $barcodes, $type, $brtlocal) {
    global $base_url;
    $order= MP\OrderShippingDetailsQuery::create()->filterByOrderId($orderid)->findOne();
    
    $recipient = $order->getName();   
	    
    $address = $order->getAddress();
    $zip= "";
    if (strlen($order->getZipcode()) < 5) {
        for ($index= 0; $index < (5-strlen($order->getZipcode())); $index++)
            $zip .= 0;
            $zip .= $order->getZipcode();
    }
    else
        $zip = $order->getZipcode();
    
    $city = $order->getLocation();
    $province = $order->getProvinance();
    
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    
    $pdf->SetMargins(0, 0, 0);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);    
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    if ($brtlocal == 1) {
        $resolution= array(65, 94);
        $pdf->AddPage('P',$resolution);
    }
    else {
        $pdf->AddPage();
    }
    ob_start(); 
    
    $style = array(
        'position' => '',
        'align' => 'C',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'hpadding' => 'auto',
        'vpadding' => 'auto',
        'fgcolor' => array(0,0,0),
        'bgcolor' => false, //array(255,255,255),
        'text' => true,
        'font' => 'helvetica',
        'fontsize' => 8,
        'stretchtext' => 4
    );
    
    if ($brtlocal == 1) {
        $pdf->write1DBarcode($barcodes[0], 'EAN13', 10, 11, '', 20, 0.4, $style, 'N');
        $html = <<<EOD
            <strong>CARTOONS SRL PI <br />
            Ordine # $orderid</strong><br />
            <strong>Destinatario:</strong><br />
            $recipient<br />
            $address<br />
            $zip $city ($province)
EOD;

        $pdf->writeHTMLCell(50, 38, 10, 30, $html, 0, 1, 0, true, '', true);
        $pdf->Ln();
    }
    else {
        for ($y=0; $y<sizeof($barcodes); $y++) {
            $pdf->write1DBarcode($barcodes[$y], 'EAN13', 10, 25+(40*$y), '', 20, 0.4, $style, 'N');
            $html = <<<EOD
                <strong>Mittente:</strong><br />
                CARTOONS Srl<br />
                VIA TORINO 6<br />
                56038 PONSACCO (PI)
EOD;

            $pdf->writeHTMLCell(100, 100, 70, 25+(40*$y), $html, 0, 1, 0, true, '', true);

            $html = <<<EOD
                <strong>Destinatario:</strong><br />
                $recipient<br />
                $address<br />
                $zip $city ($province)
EOD;

            $pdf->writeHTMLCell(100, 100, 140, 25+(40*$y), $html, 0, 1, 0, true, '', true);

            $pdf->Ln();
        }
        $pdf->lastPage();
    
    }
    //$pdf->writeHTML($final, true, 0, true, 0);
    //dd($type);
    if ($type== 'D')
        $pdf->Output(DRUPAL_ROOT . base_path() . 'sites/default/files/dat/barcode'.$nome.'.pdf', 'FD');
    else
        $pdf->Output(DRUPAL_ROOT . base_path() . 'sites/default/files/dat/barcode'.$nome.'.pdf', 'F');
}
	
function mp_create_bordero($orderid, $nome, $packages, $barcodes, $weight) {
    global $base_url;
    
    $order= MP\OrderShippingDetailsQuery::create()->filterByOrderId($orderid)->findOne();
    
    $recipient = $order->getName();   
	    
    $address = $order->getAddress();
    $zip= "";
    if (strlen($order->getZipcode()) < 5) {
        for ($index= 0; $index < (5-strlen($order->getZipcode())); $index++)
            $zip .= 0;
            $zip .= $order->getZipcode();
    }
    else
        $zip = $order->getZipcode();
              
    $city = $order->getLocation();
    $province = $order->getProvinance();
    
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);    
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    $pdf->AddPage();
    ob_start(); 
    
    $html = <<<EOD
            <div style="border-color: black; border-type: solid; border-width: 2px 2px 1px 2px; text-align:center; font-size: 10px"> <br> <strong> CODICE CLIENTE BRT XXXXXXX CARTOONS SRL </strong> <br> </div>
EOD;
    $pdf->writeHTMLCell('', '', 14, 12, $html, 0, 1, 0, true, '', true);
    
    $nofpackages= sizeof($packages);
    $day= date("d"); $month= date("m"); $year= date("Y"); $hour= date("H"); $mins= date("i");
    $html = <<<EOD
<div style="font-size: 11px; border-left: 2px solid black; border-right: 2px solid black; border-bottom: 1px solid black;">
   <br>
   <table width="100%" >
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><b>Data Stampa:</b> $day/$month/$year</td>
                </tr>
                <tr>
                    <td><b>Ora Stampa:</b>$hour:$mins</td>
                </tr>
                <tr>
                    <td><b>Totale Spedizioni</b> 1</td>
                </tr>
                <tr>
                    <td><b>Totale Colli</b> $nofpackages</td>
                </tr>
            </table>
        </td>
        <td width="33%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><b>Luogo di partenza Merce:</b></td>
                </tr>
                <tr>
                    <td>Cartoons Srl</td>
                </tr>
                <tr>
                    <td>VIA TORINO 6</td>
                </tr>
                <tr>
                    <td>56038 PONSACCO (PI)</td>
                </tr>
            </table>
        </td>
        <td width="33%" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><b>Cliente:</b></td>
                </tr>
                <tr>
                    <td>Cartoons Srl</td>
                </tr>
                <tr>
                    <td>VIA TORINO 6</td>
                </tr>
                <tr>
                    <td>56038 PONSACCO (PI)</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
            <br>
 <div style="text-align:center"> <strong> DESTINAZIONI </strong> </div>
</div>
EOD;
    $pdf->writeHTMLCell('', '', 14, 23, $html, 0, 1, 0, true, '', true);

    $first_barcode = $barcodes[0];
    $last_barcode = $barcodes[(sizeof($barcodes)-1)];
		
    $html = <<<EOD
    <div style="border-left: 2px solid black; border-right: 2px solid black; font-size: 11px;">
        <br><br>
        $recipient<br>
        $address<br>
        $zip $city ($province))
        <br><br>
    </div>
EOD;
    $pdf->writeHTMLCell('', '', 14, 54, $html, 0, 1, 0, true, '', true);
    
    $style = array(
        'position' => '',
        'align' => 'C',
        'stretch' => false,
        'fitwidth' => true,
        'cellfitalign' => '',
        'border' => false,
        'hpadding' => 'auto',
        'vpadding' => 'auto',
        'fgcolor' => array(0,0,0),
        'bgcolor' => false, //array(255,255,255),
        'text' => true,
        'font' => 'helvetica',
        'fontsize' => 8,
        'stretchtext' => 4
    );
    $pdf->writeHTMLCell('', '', 74, 58, "<div style='font-size:4px'><strong>Collo: </strong></div>", 0, 1, 0, true, '', true);
    $pdf->write1DBarcode($first_barcode, 'EAN13', 74, 62, '', 20, 0.4, $style, 'N');
    $pdf->writeHTMLCell('', '', 134, 58, "<div style='font-size:4px'><strong>Collo: </strong></div>", 0, 1, 0, true, '', true);
    if(sizeof($barcodes)>1){
        $pdf->write1DBarcode($last_barcode, 'EAN13', 134, 62, '', 20, 0.4, $style, 'N');
    }
    
    $html = <<<EOD
    <div style="border-left: 2px solid black; border-right: 2px solid black; border-bottom: 2px solid black; font-size: 11px;">
        <br><br> 
        <table width="100%" >
            <tr>
                <td width="15%" rowspan="2" valign="top"> $nofpackages colli</td>
                <td width="15%" rowspan="2" valign="top"> $weight kg</td>
                <td width="20%">Rif. Num: $orderId </td>
                <td width="20%">Contrassegno: </td>
                <td width="20%" rowspan="2" valign="top">Assicurazione:</td>
            </tr>
            <tr>
                <td>Rif. Alpha: Cartoons Srl</td>
                <td width="20%">Mod.Incasso:</td>
            </tr>
            <tr>
                <td height="15" colspan="5" align="center" style="font-weight:bold; text-transform:uppercase;">&nbsp;</td>
            </tr>
        </table>
    </div>
EOD;
            
    $pdf->writeHTMLCell('', '', 14, 76, $html, 0, 1, 0, true, '', true);
    /*$html = '<html>
        	<head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>Bordero</title>
		</head>

		<body style="font-family:Arial, Helvetica, sans-serif">
		<table style="border:2px solid #000; font-family:Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
			<td colspan="3" width="100%"><!-- first table -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                                    <td width="100%" height="30px" colspan="3" align="center" valign="middle" style="font-weight:bold; border-bottom:1px solid #000; text-transform:uppercase;">
                                        
                                    </td>
                                </tr>
				<tr>
                                    <td height="15" colspan="3" align="center">&nbsp;</td>
				</tr>
				
			</td>
                    </tr>
                    <tr>
			<td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                    	<td colspan="3" align="center" style="font-weight:bold; text-transform:uppercase; border-bottom:1px solid #000;">Destinazioni</td>
                    </tr>
                    <tr>
                    	<td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="33%" valign="top">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    ';
			
			
		$html.= '<tr>
                            <td>'.$recipient.'</td>
			</tr>
			<tr>
                            <td>'.$address.'</td>
			</tr>
			<tr>
			    <td>'.$zip.' '.$city. ' ('.$province.')</td>
			</tr>
			<tr>
			    <td></td>
			</tr>
			<tr>
			    <td>&nbsp;</td>
			</tr>
                    </table>
		</td>
		<td width="33%" valign="top">
		    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
                            <td><b>Collo:</b></td>
			</tr>
			<tr>
                            <td align="center"><img width="100" src="'. $base_url .'/sites/all/libraries/barcodes/barcode.php?code='. $first_barcode .'" /></td>
                        </tr>
		   </table>
		</td>
		<td width="33%" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><b>Collo:</b></td>
                        </tr>
                        <tr>
                            <td align="center">';
			if(sizeof($barcodes)>1){
                            $html .= '<img width="100" src="'. $base_url .'/sites/all/libraries/barcodes/barcode.php?code='. $last_barcode .'" />';
			} else {
                            $html .= "&nbsp;";
			}
			  
			$html .= '</td>
                        </tr>
                    </table>
		</td>
            </tr>
            <tr>
                <td colspan="3" valign="top">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="10%" rowspan="2" valign="top">' . sizeof($packages) . ' colli</td>
                            <td width="10%" rowspan="2" valign="top">' . $weight . ' kg</td>
                            <td width="20%">Rif. Num: ' . $orderId . '</td>
                            <td width="20%">Contrassegno: </td>
                            <td width="20%" rowspan="2" valign="top">Assicurazione:</td>
                        </tr>
                        <tr>
                            <td>Rif. Alpha: MePrint</td>
                            <td width="20%">Mod.Incasso:</td>
                        </tr>
                        <tr>
                            <td height="15" colspan="5" align="center" style="font-weight:bold; text-transform:uppercase;">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
	  
	    <tr>
		<td colspan="3"><hr /></td>
	    </tr>
	</table>
    </body>
</html>';*/
		
    $pdf->lastPage();
    $pdf->Output(DRUPAL_ROOT . base_path() . 'sites/default/files/dat/bordero'.$nome.'.pdf', 'F');
    
    return 1;
}