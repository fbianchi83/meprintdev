(function($) {
Drupal.behaviors.smallformatModule = {
	attach: function (context, settings) {  
    

/************ Promo Price *******************/


$(".form-item-promotion-prices-small-it").hide();
$(".form-item-price-type-it").hide();
$("#edit-promo-flags-small-it-promo").click(function () {  
    if($("#edit-promo-flags-small-it-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-it").show();
        $(".form-item-price-type-it").show();
    }
    
});

if($("#edit-promo-flags-small-it-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-it").show();
        $(".form-item-price-type-it").show();
    }

$("#edit-promo-flags-small-it-best-price").click(function () {  
    if($("#edit-promo-flags-small-it-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small-it").hide();
          $(".form-item-price-type-it").hide();
    }
    
});

$(".form-item-promotion-prices-small").hide();
$(".form-item-price-type").hide();

$("#edit-promo-flags-small-promo").click(function () {  
    if($("#edit-promo-flags-small-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small").show();
        $(".form-item-price-type").show();
    }
    
});

if($("#edit-promo-flags-small-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small").show();
        $(".form-item-price-type").show();
    }

$("#edit-promo-flags-small-best-price").click(function () {  
    if($("#edit-promo-flags-small-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small").hide();
        $(".form-item-price-type").hide();
    }
});
$("#edit-promo-flags-small-").click(function () {  
    if($("#edit-promo-flags-small-").is(':checked')){ 
        $(".form-item-promotion-prices-small").hide();
        $(".form-item-price-type").hide();
    }
});
$("#edit-promo-flags-small-new").click(function () {  
    if($("#edit-promo-flags-small-new").is(':checked')){ 
        $(".form-item-promotion-prices-small").hide();
        $(".form-item-price-type").hide();
    }
});

$(".form-item-promotion-prices-small-fr").hide();
$(".form-item-price-type-fr").hide();
$("#edit-promo-flags-small-fr-promo").click(function () {  
    if($("#edit-promo-flags-small-fr-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-fr").show();
        $(".form-item-price-type-fr").show();
    }
    
}); 
if($("#edit-promo-flags-small-fr-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-fr").show();
        $(".form-item-price-type-fr").show();
    }
    
$("#edit-promo-flags-small-fr-best-price").click(function () {  
    if($("#edit-promo-flags-small-fr-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small-fr").hide();
        $(".form-item-price-type-fr").hide();
    }
    
});


/************ Promo Price *******************/

//edit-promo-flags-small-it-best-price
/************ Extra Processing *******************/
$("#hasextra_process_small").hide();
 if($("#edit-small-extra-processing-small-1").is(':checked')){    
	$("#hasextra_process_small").show();	
} else {
	 $("#hasextra_process_small").hide();
}
$("#edit-small-extra-processing-small-1").click(function () {      
    if($("#edit-small-extra-processing-small-1").is(':checked')){    
        $("#hasextra_process_small").show();      
    } else {
         $("#hasextra_process_small").hide();
    }
});
/************ Extra Processing End *******************/

 $(document).ready (   	 
            function() {   
		
                $('.fwdarrow').on('click',  
                    function(e) {
                        //console.log("puppa fwd!");
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).parents('.multiselect').find('.leftsel > option:selected').appendTo($(this).parents('.multiselect').find('.rightsel'));
                        //$(this).parents('.multiselect').find('.leftsel > option').prop('selected', true);
                        $(this).parents('.multiselect').find('.rightsel > option').prop('selected', true);
                        //$('#gallery_id > option:selected').appendTo('#gallery_list');    
                    });  
                    
                $('.bwdarrow').on('click',
                    function(e) {
                        //console.log("puppa bwd!");
                        e.preventDefault();
                        e.stopPropagation();
                        $(this).parents('.multiselect').find('.rightsel > option:selected').appendTo($(this).parents('.multiselect').find('.leftsel'));
                        //$(this).parents('.multiselect').find('.leftsel > option').prop('selected', true);
                        //$(this).parents('.multiselect').find('.rightsel > option').prop('selected', true);
                        //$('#gallery_id > option:selected').appendTo('#gallery_list');  
                    });
                    
                    
               $('.form-submit').on('click',
                    function(e) {
                        $('.rightsel > option').prop('selected', true);
                        $('input[name="newfid"]').val($("input[name='instructions_file[fid]']").val());
                        $('input[name="ct_newfid"]').val($("input[name='custom_template[fid]']").val());
                        $('input[name="newfidm"]').val($("input[name='certification_file[fid]']").val());
                        $('input[name="newfids"]').val($("input[name='instructions_file_small[fid]']").val());
                        $('input[name="newfids2"]').val($("input[name='instructions_file_small2[fid]']").val());
                        //alert($("input[name='instructions_file_small2[fid]']").val());
                });
               /*$('#edit_gallery_arrow_back').click(  
                    function(e) {   
                        console.log($('#gallery_list').val());
                        $('#gallery_list > option:selected').appendTo('#gallery_id');  
                        e.preventDefault();  
                });*/ 
                
              /************ Processing End *******************/
               $('.rmvsfimg').on('click', function () {

                var fid = $(this).attr('id');
                var fid = fid.split('_');
                var fid = fid[1];
                var id_bigformat = $(this).data('idbigformat');

                $.ajax({
                    type: 'POST',
                    url: 'ajax/rmvbfimg',
                    data: 'id_smallformat=' + id_bigformat + '&fid=' + fid,
                    dataType: 'json',
                    success: function (results) {
                        $('#rmvbfimg_' + fid).parent().hide();
                    }
                });
            }); 
            
                
               
            });
            /************** Multi Site Start **********/
$("#fr_web").hide();
if ($('#edit-choose-website-small-fr').is(":checked")) {       
    $("#fr_web").show();     
}  else {
	$("#fr_web").hide();
}
 $("#edit-choose-website-small-fr").click(function () { 
    if ($('#edit-choose-website-small-fr').is(":checked")) {       
        $("#fr_web").show();     
    }
	else {
        $("#fr_web").hide();
    }
});
$("#it_web").hide(); 
if ($('#edit-choose-website-small-it').is(":checked")) {       
       $("#it_web").show();     
    } 
    else {
          $("#it_web").hide();
    }
$("#edit-choose-website-small-it").click(function () { 
    if ($('#edit-choose-website-small-it').is(":checked")) {       
       $("#it_web").show();     
    } 
    else {
          $("#it_web").hide();
    }
});


$("#es_web").hide();
if ($('#edit-choose-website-small-es').is(":checked")) {       
    $("#es_web").show();     
}  else {
	$("#es_web").hide();
}
 $("#edit-choose-website-small-es").click(function () { 
    if ($('#edit-choose-website-small-es').is(":checked")) {       
        $("#es_web").show();     
    }
	else {
        $("#es_web").hide();
    }
});

    /************** Multi Site End **********/ 
$(".form-item-min-copies-small").hide();  
if ($('#edit-choice-of-copy-number-small-1').is(":checked")) {
   $(".form-item-min-copies-small").show();     
}  else {
	 $(".form-item-min-copies-small").hide();   
}
$("#edit-choice-of-copy-number-small-1").click(function () { 
    if ($('#edit-choice-of-copy-number-small-1').is(":checked")) {
           $(".form-item-min-copies-small").show();     
        }  else {
             $(".form-item-min-copies-small").hide();   
        }

});

/************** Edit JS ***************/
//edit-choose-website-small-edit-fr 
$("#fr_web_edit").hide();
$("#it_web_edit").hide();


if ($('#edit-choose-website-small-edit-fr').is(":checked")) {
         $("#fr_web_edit").show();
} else {
     $("#fr_web_edit").hide();
}



if ($('#edit-choose-website-small-edit-it').is(":checked")) {
         $("#it_web_edit").show();
} else {
     $("#it_web_edit").hide();
}


$("#edit-choose-website-small-edit-fr").click(function () { 
    if ($('#edit-choose-website-small-edit-fr').is(":checked")) {
          $("#fr_web_edit").show();  
        }  else {
             $("#fr_web_edit").hide();  
        }

});


$("#edit-choose-website-small-edit-it").click(function () { 
    if ($('#edit-choose-website-small-edit-it').is(":checked")) {
          $("#it_web_edit").show();  
        }  else {
             $("#it_web_edit").hide();  
        }

});

/*********** Edit For Promo Best Price Start  *******************/
$(".form-item-promotion-prices-small-edit").hide();
$(".form-item-price-type-small-edit").hide();


$("#edit-promo-flags-small-edit-promo").click(function () {  
    if($("#edit-promo-flags-small-edit-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit").show();
        $(".form-item-price-type-small-edit").show();
    }
});

if($("#edit-promo-flags-small-edit-promo").is(':checked')){ 
    $(".form-item-promotion-prices-small-edit").show();
    $(".form-item-price-type-small-edit").show();
}

$("#edit-promo-flags-small-edit-best-price").click(function () {  
    if($("#edit-promo-flags-small-edit-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit").hide();
        $(".form-item-price-type-small-edit").hide();
    }
});

$("#edit-promo-flags-small-edit-").click(function () {  
    if($("#edit-promo-flags-small-edit-").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit").hide();
        $(".form-item-price-type-small-edit").hide();
    }
});
$("#edit-promo-flags-small-edit-new").click(function () {  
    if($("#edit-promo-flags-small-edit-new").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit").hide();
        $(".form-item-price-type-small-edit").hide();
    }
}); 

/*********** Edit For Promo Best Price End  *******************/ 

/*********** Edit For Promo Best Price Italian Site Start  *******************/
$(".form-item-promotion-prices-small-edit-it").hide();
$(".form-item-price-type-small-edit-it").hide();
$("#edit-promo-flags-small-edit-it-promo").click(function () {  
    if($("#edit-promo-flags-small-edit-it-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-it").show();
        $(".form-item-price-type-small-edit-it").show();
    }
    
});

if($("#edit-promo-flags-small-edit-it-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-it").show();
         $(".form-item-price-type-small-edit-it").show();
    }

$("#edit-promo-flags-small-edit-it-best-price").click(function () {  
    if($("#edit-promo-flags-small-edit-it-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-it").hide();
         $(".form-item-price-type-small-edit-it").hide();
    }
    
}); 

/*********** Edit For Promo Best Price Italian Site End  *******************/ 

/*********** Edit For Promo Best Price French Site Start  *******************/ 
$(".form-item-promotion-prices-small-edit-fr").hide();
$(".form-item-price-type-small-edit-fr").hide();
$("#edit-promo-flags-small-edit-fr-promo").click(function () {  
    if($("#edit-promo-flags-small-edit-fr-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-fr").show();
        $(".form-item-price-type-small-edit-fr").show();
    }
    
});

if($("#edit-promo-flags-small-edit-fr-promo").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-fr").show();
          $(".form-item-price-type-small-edit-fr").show();
    }

$("#edit-promo-flags-small-edit-fr-best-price").click(function () {  
    if($("#edit-promo-flags-small-edit-fr-best-price").is(':checked')){ 
        $(".form-item-promotion-prices-small-edit-fr").hide();
          $(".form-item-price-type-small-edit-fr").hide();
    }
    
}); 


/*********** Edit For Promo Best Price French Site End  *******************/ 


/*** Gallery Arrow ****/
$('#gallery_arrow').click(
        function (e) {
            $('#gallery_id > option:selected').appendTo('#gallery_list');
            $('#gallery_list > option:selected');
            e.preventDefault();
        });
$('#gallery_arrow_back').click(
        function (e) {
            $('#gallery_list > option:selected').appendTo('#gallery_id');
            e.preventDefault();
        });

 // Edit Accessories for Fr
$('#edit_access_id_arrow_fr').click(
        function (e) {
            $('#acc_id_fr > option:selected').appendTo('#acc_id_list_fr');
            $('#acc_id_list_fr > option:selected');
            $('#acc_id_list_fr').each(function () {
                $('#acc_id_list_fr option').attr("selected", "selected");
            });
            e.preventDefault();
        });
$('#edit_access_id_arrow_back_fr').click(
        function (e) {
            $('#acc_id_list_fr > option:selected').appendTo('#acc_id_fr');
            e.preventDefault();
        });
// Edit Accessories for IT
$('#edit_access_id_arrow_it').click(
        function (e) {
            $('#acc_id_it > option:selected').appendTo('#acc_id_list_it');
            $('#acc_id_list_it > option:selected');
            $('#acc_id_list_it').each(function () {
                $('#acc_id_list_it option').attr("selected", "selected");
            });
            e.preventDefault();
        });
$('#edit_access_id_arrow_back_it').click(
        function (e) {
            $('#acc_id_list_it > option:selected').appendTo('#acc_id_it');
            e.preventDefault();
        });
        
// Edit Accessories
$('#edit_access_id_arrow').click(
        function (e) {
            $('#acc_id > option:selected').appendTo('#acc_id_list');
            $('#acc_id_list > option:selected');
            $('#acc_id_list').each(function () {
                $('#acc_id_list option').attr("selected", "selected");
            });
            e.preventDefault();
        });
$('#edit_access_id_arrow_back').click(
        function (e) {
            $('#acc_id_list > option:selected').appendTo('#acc_id');
            e.preventDefault();
        });
        
/*****  Accessories ****/
$('#access_id_arrow_it').click(
        function (e) {
            $('#acc_id_it > option:selected').appendTo('#acc_id_list_it');
            e.preventDefault();
        });
$('#access_id_arrow_back_it').click(
        function (e) {
            $('#acc_id_list_it > option:selected').appendTo('#acc_id_it');
            e.preventDefault();
        });
/*****  Accessories ****/
$('#access_id_arrow_fr').click(
        function (e) {
            $('#acc_id_fr > option:selected').appendTo('#acc_id_list_fr');
            e.preventDefault();
        });
$('#access_id_arrow_back_fr').click(
        function (e) {
            $('#acc_id_list_fr > option:selected').appendTo('#acc_id_fr');
            e.preventDefault();
        });
/*****  Accessories ****/

/*****  Accessories ****/
$('#access_id_arrow').click(
        function (e) {
            $('#acc_id > option:selected').appendTo('#acc_id_list');
            e.preventDefault();
        });
$('#access_id_arrow_back').click(
        function (e) {
            $('#acc_id_list > option:selected').appendTo('#acc_id');
            e.preventDefault();
        });
/*****  Accessories ****/

 /*** Long Time  ****/
$('#lproduct_arrow').click(
        function (e) {
            $('#long_product_id > option:selected').appendTo('#long_product_list');
            $('#long_product_list > option:selected');
            e.preventDefault();
        });
$('#lproduct_arrow_back').click(
        function (e) {
            $('#long_product_list > option:selected').appendTo('#long_product_id');
            e.preventDefault();
        });
        
 $(".form-item-destination-use-small").hide();
    if ($("#edit-destination-id-small-1").is(':checked')) {
        $(".form-item-destination-use-small").show();  // checked
    } else {
        $(".form-item-destination-use-small").hide();
    }
    $("#edit-destination-id-small-1").click(function () {
        if ($("#edit-destination-id-small-1").is(':checked')) {
            $(".form-item-destination-use-small").show();  // checked
        } else {
            $(".form-item-destination-use-small").hide();
        }
    });



  }
};

      
})(jQuery);



   
