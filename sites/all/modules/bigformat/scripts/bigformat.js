(function ($) {
    Drupal.behaviors.bigformatModule = {
        attach: function (context, settings) { //code starts	
            $('#dvfr').hide();
            $('#dvit').hide();
            $('#editor').hide();
            $('#editorlist').hide();
            $('#hasextra_it').hide();
            $('#hasextra_fr').hide();
            $('input[name="biglanguages[fr]"]').click(function () {
                if ($('input[name="biglanguages[fr]"]').is(':checked')) {
                    $('#dvfr').show();
                } else {
                    $('#dvfr').hide();
                }
            });
            $('.form-item-acc-with-listss').hide();
            $("#edit-sell-with-structuress-1").click(function () {
                if ($("#edit-sell-with-structuress-1").is(':checked')) {
                    $('.form-item-acc-with-listss').show();
                }
                else {
                    $('.form-item-acc-with-listss').hide();
                }
            });
            if ($("#edit-sell-with-structuress-1").is(':checked')) {
                $('.form-item-acc-with-listss').show();
            }
            else {
                $('.form-item-acc-with-listss').hide();
            }

            if ($("#edit-is-preset-amount-1").is(':checked')) {
                $('.form-item-amounts').show();
                $('.form-item-amounts-discounts').show();
            }
            else {
                $('.form-item-amounts').hide();
                $('.form-item-amounts-discounts').hide();
            }

            $('input[name="use_of_external_designer_program[1]"]').click(function () {
                if ($('input[name="use_of_external_designer_program[1]"]').is(':checked')) {
                    $('#editor').show();
                }
                else {
                    $('#editor').hide();
                }
            });
            
            $("#edit-is-preset-amount-1").click(function () {
                if ($("#edit-is-preset-amount-1").is(':checked')) {
                    $('.form-item-amounts').show();
                    $('.form-item-amounts-discounts').show();
                }
                else {
                    $('.form-item-amounts').hide();
                    $('.form-item-amounts-discounts').hide();
                }
            });

            $('input[name="extra_processing_fr[1]"]').click(function () {
                if ($('input[name="extra_processing_fr[1]"]').is(':checked')) {
                    $('#hasextra_fr').show();
                } else {
                    $('#hasextra_fr').hide();
                }
            });
            $('input[name="extra_processing_it[1]"]').click(function () {
                if ($('input[name="extra_processing_it[1]"]').is(':checked')) {
                    $('#hasextra_it').show();
                } else {
                    $('#hasextra_it').hide();
                }
            });
            $('#edit-editor-color-type').on('change', function () {
                if (this.value == 2) {
                    $('#editorlist').show();
                } else {
                    $('#editorlist').hide();
                }
            });
            
            if ($('input[name="use_of_external_designer_program[1]"]').is(':checked')) {
                $('#editor').show();
            }
            if ($('#edit-editor-color-type').val() == 2)
                $('#editorlist').show();
            
            $('.rmvbfimg').on('click', function () {

                var fid = $(this).attr('id');
                var fid = fid.split('_');
                var fid = fid[1];
                var id_bigformat = $(this).data('idbigformat');

                $.ajax({
                    type: 'POST',
                    url: 'ajax/rmvbfimg',
                    data: 'id_bigformat=' + id_bigformat + '&fid=' + fid,
                    dataType: 'json',
                    success: function (results) {
                        $('#rmvbfimg_' + fid).parent().hide();
                    }
                });
            });


            $('input[name="biglanguages[it]"]').click(function () {
                if ($('input[name="biglanguages[it]"]').is(':checked')) {
                    $('#dvit').show();
                } else {
                    $('#dvit').hide();
                }
            });

            /*****  Process ****/
            /*$('#arrow_process_it').click(
                    function (e) {
                        $('#small_extra_processing_id_it > option:selected').appendTo('#small_extra_processing_list_it');
                        $('#small_extra_processing_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_process_back_it').click(
                    function (e) {
                        $('#small_extra_processing_list_it > option:selected').appendTo('#small_extra_processing_id_it');
                        $('#small_extra_processing_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_process_fr').click(
                    function (e) {
                        $('#small_extra_processing_id_fr > option:selected').appendTo('#small_extra_processing_list_fr');
                        $('#small_extra_processing_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_process_back_fr').click(
                    function (e) {
                        $('#small_extra_processing_list_fr > option:selected').appendTo('#small_extra_processing_id_fr');
                        $('#small_extra_processing_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            /*****  Process ****/
            /*** Big Format ****/
            /*****  Process ****/
            /*$('#arrow_big_process_it').click(
                    function (e) {
                        $('#big_extra_processing_id_it > option:selected').appendTo('#big_extra_processing_list_it');
                        $('#big_extra_processing_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_big_process_back_it').click(
                    function (e) {
                        $('#big_extra_processing_list_it > option:selected').appendTo('#big_extra_processing_id_it');
                        $('#big_extra_processing_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_big_process_fr').click(
                    function (e) {
                        $('#big_extra_processing_id_fr > option:selected').appendTo('#big_extra_processing_list_fr');
                        $('#big_extra_processing_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_big_process_back_fr').click(
                    function (e) {
                        $('#big_extra_processing_list_fr > option:selected').appendTo('#big_extra_processing_id_fr');
                        $('#big_extra_processing_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            /*** promo discount ****/
            $(".form-item-promotion-prices-it").hide();
            $(".form-item-promotion-prices-fr").hide();
            
            $(".form-item-promotion-prices").hide();
            
            $(".form-item-promotion-price-type-it").hide();
            $(".form-item-promotion-price-type-fr").hide();
            
            $(".form-item-promotion-price-type").hide();
            
            if ($("#edit-promo-flags-it-promo").is(':checked')) {
                $(".form-item-promotion-prices-it").show();  // checked
                $(".form-item-promotion-price-type-it").show();
            }
            if ($("#edit-promo-flags-it-best-price").is(':checked')) {
                $(".form-item-promotion-prices-it").hide();  // checked
                $(".form-item-promotion-price-type-it").hide();
            }
            if ($("#edit-promo-flags-fr-promo").is(':checked')) {
                $(".form-item-promotion-prices-fr").show();
                $(".form-item-promotion-price-type-fr").show();			// checked
            }
            if ($("#edit-promo-flags-fr-best-price").is(':checked')) {
                $(".form-item-promotion-prices-fr").hide();
                $(".form-item-promotion-price-type-fr").hide();// checked
            }
            
            
            if ($("#edit-promo-flags-promo").is(':checked')) {
                $(".form-item-promotion-prices").show();  // checked
                $(".form-item-promotion-price-type").show();
            }
            if ($("#edit-promo-flags-best-price").is(':checked')) {
                $(".form-item-promotion-prices").hide();  // checked
                $(".form-item-promotion-price-type").hide();
            }
            if ($("#edit-promo-flags-new").is(':checked')) {
                $(".form-item-promotion-prices").hide();  // checked
                $(".form-item-promotion-price-type").hide();
            }
            if ($("#edit-promo-flags-").is(':checked')) {
                $(".form-item-promotion-prices").hide();  // checked
                $(".form-item-promotion-price-type").hide();
            }
            
            
            $("#edit-promo-flags-it-promo").click(function () {
                if ($("#edit-promo-flags-it-promo").is(':checked')) {
                    $(".form-item-promotion-prices-it").show();
                    $(".form-item-promotion-price-type-it").show();
                    //checked
                }
            });
            $("#edit-promo-flags-it-best-price").click(function () {
                if ($("#edit-promo-flags-it-best-price").is(':checked')) {
                    $(".form-item-promotion-prices-it").hide();
                    $(".form-item-promotion-price-type-it").hide();
                    // checked
                }
            });
            $("#edit-promo-flags-fr-promo").click(function () {
                if ($("#edit-promo-flags-fr-promo").is(':checked')) {
                    $(".form-item-promotion-prices-fr").show();
                    $(".form-item-promotion-price-type-fr").show();
                    // checked
                }
            });
            $("#edit-promo-flags-fr-best-price").click(function () {
                if ($("#edit-promo-flags-fr-best-price").is(':checked')) {
                    $(".form-item-promotion-prices-fr").hide();
                    $(".form-item-promotion-price-type-fr").hide();
                   //checked
                }
            });
            
            $("#edit-promo-flags-promo").click(function () {
                if ($("#edit-promo-flags-promo").is(':checked')) {
                    $(".form-item-promotion-prices").show();
                    $(".form-item-promotion-price-type").show();
                    //checked
                }
            });
            $("#edit-promo-flags-best-price").click(function () {
                if ($("#edit-promo-flags-best-price").is(':checked')) {
                    $(".form-item-promotion-prices").hide();
                    $(".form-item-promotion-price-type").hide();
                    // checked
                }
            });
            $("#edit-promo-flags-new").click(function () {
                if ($("#edit-promo-flags-new").is(':checked')) {
                    $(".form-item-promotion-prices").hide();
                    $(".form-item-promotion-price-type").hide();
                    // checked
                }
            });
            $("#edit-promo-flags-").click(function () {
                if ($("#edit-promo-flags-").is(':checked')) {
                    $(".form-item-promotion-prices").hide();
                    $(".form-item-promotion-price-type").hide();
                    // checked
                }
            });
            
            
            $("#edit-promo-flags-promo").click(function () {
                if ($("#edit-promo-flags-promo").is(':checked')) {
                    $(".form-item-promotion-prices").show();
                    $(".form-item-promotion-price-type").show(); 			// checked
                }
            });
            $("#edit-promo-flags-best-price").click(function () {
                if ($("#edit-promo-flags-best-price").is(':checked')) {
                    $(".form-item-promotion-prices").hide();
                    $(".form-item-promotion-price-type").hide(); 			// checked
                }
            });
            
            
            $(".form-item-destination-use-big").hide();
            if ($("#edit-destination-id-big-1").is(':checked')) {
                $(".form-item-destination-use-big").show();  // checked
            } else {
                $(".form-item-destination-use-big").hide();
            }
            $("#edit-destination-id-big-1").click(function () {
                if ($("#edit-destination-id-big-1").is(':checked')) {
                    $(".form-item-destination-use-big").show();  // checked
                } else {
                    $(".form-item-destination-use-big").hide();
                }
            });
            $(".form-item-min-copies").hide();
            $("#edit-choice-of-copy-number-1").click(function () {
                if ($("#edit-choice-of-copy-number-1").is(':checked')) {
                    $(".form-item-min-copies").show();  // checked
                } else {
                    $(".form-item-min-copies").hide();
                }
            });


            //edit-choice-of-copy-number-1


            $("#edit-promo-flag-fr-best-price").click(function () {
                if ($("#edit-promo-flag-fr-best-price").is(':checked')) {
                    $(".form-item-promotion-price-fr").hide();  // checked
                }
            });
            // end promo discount
            // products groups 

            $("#productgroup_id").change(function () {
                var data = $("#productgroup_id").val();
                var language = 'it';
                $.ajax({
                    type: 'POST',
                    url: 'ajax/values1',
                    data: 'sub=' + data + '&lang=' + language,
                    dataType: 'json',
                    success: function (results) {
                        var html12 = '<option value="">-Select-</option>';
                        $.each(results, function (key, value) {

                            html12 += '<option value=' + key + '>' + value + '</option>';
                        });

                        $('[name="subgroup_id"]').html(html12);
                    }
                });
            });
            // Has extra
            ;
            $('#hasextrabig').hide();
            $('input[name="extra_processing_big[1]"]').click(function () {
                if ($('input[name="extra_processing_big[1]"]').is(':checked')) {
                    $('#hasextrabig').show();
                } else {
                    $('#hasextrabig').hide();
                }
            });
            //Edit Dimensions Arrow	
            /*$('#edit_arrow_dimensions').click(
                    function (e) {
                        $('#dimension_ids > option:selected').appendTo('#dimension_lists');
                        $('#dimension_lists > option:selected');
                        $('#dimension_lists').each(function () {
                            $('#dimension_lists option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_arrow_dimensions_back').click(
                    function (e) {
                        $('#dimension_lists > option:selected').appendTo('#dimension_ids');
                        $('#dimension_lists option').prop('selected', true);
                        e.preventDefault();
                    });
            //Edit LongProduct Arrow
            $('#edit_lproduct_arrow').click(
                    function (e) {
                        $('#long_product_id > option:selected').appendTo('#long_product_list');
                        $('#long_product_list > option:selected');
                        $('#long_product_list').each(function () {
                            $('#long_product_list option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_lproduct_arrow_back').click(
                    function (e) {
                        $('#long_product_list > option:selected').appendTo('#long_product_id');
                        $('#long_product_list option').prop('selected', true);
                        e.preventDefault();
                    });
            //Edit Meterial Arrow
            $('#edit_arrow_material_big').click(
                    function (e) {
                        $('#material_group_ids > option:selected').appendTo('#material_group_lists');
                        $('#material_group_lists > option:selected');
                        $('#material_group_lists').each(function () {
                            $('#material_group_lists option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_arrow_material_big_back').click(
                    function (e) {
                        $('#material_group_lists > option:selected').appendTo('#material_group_ids');
                        $('#material_group_lists option').prop('selected', true);
                        e.preventDefault();
                    });
            // material arrow
            $('#arrow_process_big').click(
                    function (e) {
                        $('#material_group_id > option:selected').appendTo('#material_group_list');
                        $('#material_group_list > option:selected');
                        e.preventDefault();
                    });
            $('#arrow_process_big_back').click(
                    function (e) {
                        $('#material_group_list > option:selected').appendTo('#material_group_id');
                        $('#material_group_lists option').prop('selected', true);
                        e.preventDefault();
                    });
            // Edit Extra process arrow
            $('#edit_arrow_big_process').click(
                    function (e) {
                        $('#big_extra_processing_ids > option:selected').appendTo('#big_extra_processing_lists');
                        $('#big_extra_processing_lists > option:selected');
                        $('#big_extra_processing_lists').each(function () {
                            $('#big_extra_processing_lists option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_arrow_big_process_back').click(
                    function (e) {
                        $('#big_extra_processing_lists > option:selected').appendTo('#big_extra_processing_ids');
                        $('#big_extra_processing_lists option').prop('selected', true);
                        e.preventDefault();
                    });
            // Edit Accessories for Fr
            $('#edit_access_id_arrow_fr').click(
                    function (e) {
                        $('#acc_id_fr > option:selected').appendTo('#acc_id_list_fr');
                        $('#acc_id_list_fr > option:selected');
                        $('#acc_id_list_fr').each(function () {
                            $('#acc_id_list_fr option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_access_id_arrow_back_fr').click(
                    function (e) {
                        $('#acc_id_list_fr > option:selected').appendTo('#acc_id_fr');
                        $('#acc_id_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            // Edit Accessories for IT
            $('#edit_access_id_arrow_it').click(
                    function (e) {
                        $('#acc_id_it > option:selected').appendTo('#acc_id_list_it');
                        $('#acc_id_list_it > option:selected');
                        $('#acc_id_list_it').each(function () {
                            $('#acc_id_list_it option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_access_id_arrow_back_it').click(
                    function (e) {
                        $('#acc_id_list_it > option:selected').appendTo('#acc_id_it');
                        $('#acc_id_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            
            
            $('#edit_access_id_arrow').click(
                    function (e) {
                        $('#acc_id > option:selected').appendTo('#acc_id_list');
                        $('#acc_id_list > option:selected');
                        $('#acc_id_list').each(function () {
                            $('#acc_id_list option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_access_id_arrow_back').click(
                    function (e) {
                        $('#acc_id_list > option:selected').appendTo('#acc_id');
                        $('#acc_id_list option').prop('selected', true);
                        e.preventDefault();
                    });
                    
                    
            
            // Extra process arrow
            $('#arrow_big_process').click(
                    function (e) {
                        $('#big_extra_processing_id > option:selected').appendTo('#big_extra_processing_list');
                        $('#big_extra_processing_list > option:selected');
                        e.preventDefault();
                    });
            $('#arrow_big_process_back').click(
                    function (e) {
                        $('#big_extra_processing_list > option:selected').appendTo('#big_extra_processing_id');
                        $('#big_extra_processing_list option').prop('selected', true);
                        e.preventDefault();
                    });

            /************* Big Format Edit Js Start ************/
            $("#hasextrabig_edit").hide();
            $("#edit-extra-processing-big-edit-1").click(function () {
                if ($("#edit-extra-processing-big-edit-1").is(':checked')) {
                    $("#hasextrabig_edit").show();  // checked
                } else {
                    $("#hasextrabig_edit").hide();
                }
            });
            if ($("#edit-extra-processing-big-edit-1").is(':checked')) {
                $("#hasextrabig_edit").show();
            }
            if ($("#edit-biglanguages-edit-it").is(':checked')) {

            }
            $("#dv_fr").hide();
            if ($("#edit-biglanguages-edit-fr").is(':checked')) {
                $("#dv_fr").show();
            }
            $("#edit-biglanguages-edit-fr").click(function () {
                if ($("#edit-biglanguages-edit-fr").is(':checked')) {
                    $("#dv_fr").show();
                } else {
                    $("#dv_fr").hide();
                }
            });
            $("#dv_it").hide();
            $("#edit-biglanguages-edit-it").click(function () {
                if ($("#edit-biglanguages-edit-it").is(':checked')) {
                    $("#dv_it").show();
                }
            });
            if ($("#edit-biglanguages-edit-it").is(':checked')) {
                $("#dv_it").show();
            }

            //edit-biglanguages-edit-it

            /********** Big Format Edit Js End ************* /
             /*** Long Time  ****/
            /*$('#lproduct_arrow').click(
                    function (e) {
                        $('#long_product_id > option:selected').appendTo('#long_product_list');
                        $('#long_product_list > option:selected');
                        e.preventDefault();
                    });
            $('#lproduct_arrow_back').click(
                    function (e) {
                        $('#long_product_list > option:selected').appendTo('#long_product_id');
                        $('#long_product_list option').prop('selected', true);
                        e.preventDefault();
                    });
            // Dimensions
            $('#arrow_dimensions').click(
                    function (e) {
                        $('#dimension_id > option:selected').appendTo('#dimension_list');
                        $('#dimension_list > option:selected');
                        e.preventDefault();
                    });
            $('#arrow_dimensions_back').click(
                    function (e) {
                        $('#dimension_list > option:selected').appendTo('#dimension_id');
                        $('#dimension_list option').prop('selected', true);
                        e.preventDefault();
                    });*/
            if ($("#edit-choice-of-copy-number-1").is(':checked')) {

                $(".form-item-min-copies").show();  // checked 

            } else {
                $(".form-item-min-copies").hide();
            }

            if ($("#edit-promo-flag-promo").is(':checked')) {

                $(".form-item-promotion-price").show();  // checked
            } else {
                $(".form-item-promotion-price").hide();
            }
            /** has Extra process ***/
            if ($("#edit-destination-id-1").is(':checked')) {
                $(".form-item-destination-use").show();
            } else {
                $(".form-item-destination-use").hide();
            }
            $('#edit-destination-id-1').click(function (e) {
                if ($("#edit-destination-id-1").is(':checked')) {
                    $(".form-item-destination-use").show();
                } else {
                    $(".form-item-destination-use").hide();
                }
            });
            if ($("#edit-extra-processing-fr-1").is(':checked')) {
                $("#hasextra_fr").show();  // checked
            }
            if ($("#edit-extra-processing-big-1").is(':checked')) {
                $("#hasextrabig").show();  // checked
            }
            /*** Main Langugaes ****/
            if ($("#edit-biglanguages-fr").is(':checked')) {
                $('#dvfr').show();// checked
            }
            if ($("#edit-biglanguages-it").is(':checked')) {
                $('#dvit').show();// checked
            }


            /*****  Dimensions ****/
            /*$('#arrow_dimensions_it').click(
                    function (e) {
                        $('#dimension_id_it > option:selected').appendTo('#dimension_list_it');
                        $('#dimension_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_dimensions_back_it').click(
                    function (e) {
                        $('#dimension_list_it > option:selected').appendTo('#dimension_id_it');
                        $('#dimension_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_dimensions_fr').click(
                    function (e) {
                        $('#dimension_id_fr > option:selected').appendTo('#dimension_list_fr');
                        $('#dimension_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_dimensions_back_fr').click(
                    function (e) {
                        $('#dimension_list_fr > option:selected').appendTo('#dimension_id_fr');
                        $('#dimension_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            /*****  Materails ****/
            /*$('#arrow_it').click(
                    function (e) {
                        $('#material_group_id_it > option:selected').appendTo('#material_group_list_it');
                        $('#material_group_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrowback_it').click(
                    function (e) {
                        $('#material_group_list_it > option:selected').appendTo('#material_group_id_it');
                        $('#material_group_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrow_fr').click(
                    function (e) {
                        $('#material_group_id_fr > option:selected').appendTo('#material_group_list_fr');
                        $('#material_group_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#arrowback_fr').click(
                    function (e) {
                        $('#material_group_list_fr > option:selected').appendTo('#material_group_id_fr');
                        $('#material_group_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            /*****  Materails ****/
            /*****  Accessoties ****/
            /*$('#access_id_arrow_it').click(
                    function (e) {
                        $('#acc_id_it > option:selected').appendTo('#acc_id_list_it');
                        $('#acc_id_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#access_id_arrow_back_it').click(
                    function (e) {
                        $('#acc_id_list_it > option:selected').appendTo('#acc_id_it');
                        $('#acc_id_list_it option').prop('selected', true);
                        e.preventDefault();
                    });
            /*****  Accessoties ****/
            /*$('#access_id_arrow_fr').click(
                    function (e) {
                        $('#acc_id_fr > option:selected').appendTo('#acc_id_list_fr');
                        $('#acc_id_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#access_id_arrow_back_fr').click(
                    function (e) {
                        $('#acc_id_list_fr > option:selected').appendTo('#acc_id_fr');
                        $('#acc_id_list_fr option').prop('selected', true);
                        e.preventDefault();
                    });
                    
            /*****  Accessories ****/
            /*$('#access_id_arrow').click(
                    function (e) {
                        $('#acc_id > option:selected').appendTo('#acc_id_list');
                        $('#acc_id_list > option:selected');
                        $('#acc_id_list').each(function () {
                            $('#acc_id_list option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_access_id_arrow_back').click(
                    function (e) {
                        $('#acc_id_list > option:selected').appendTo('#acc_id');
                        $('#acc_id_list option').prop('selected', true);
                        e.preventDefault();
                    });
            
            /*****  Accessoties ****/
            //$('#edit-packaging-price-calculation1').hide();
            $('#scost').show();
            $('#dvpr1').hide();
            $('#edit-packaging-price-calculation1').show();
            $('#dvpkg1').hide();
            for (var h = 2; h < 11; h++) {
                $('#row' + h).hide();
            }
            for (var p = 2; p < 11; p++) {
                $('#rowpk' + p).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttl"]').val();
            if (copies > 0 && ($('[name="deter_cost_copies"]').val() == 2 || $('[name="deter_cost_copies"]').val() == 3)) {
                for (var l = 1; l <= copies; l++) {
                    $('#row' + l).show();
                }
            }
            // cost packages
            var copies_kgs = $('[name="hdpackg_ttl"]').val();
            if (copies_kgs > 0 && $('[name="deter_cost_copies"]').val() == 3 && $('[name="packaging_price_calculation1[1]"]').is(':checked')) {
                for (var k = 1; k <= copies_kgs; k++) {
                    $('#rowpk' + k).show();
                }
            }
            $('[name="packaging_price_calculation1[1]"]').click(function (e) {
                if ($('[name="packaging_price_calculation1[1]"]').is(':checked')) {
                    //$('#dvpkg1').show();   // checked
                }
                else {
                    //$('#dvpkg1').hide(); 
                }
            });
            if ($('[name="packaging_price_calculation1[1]"]').is(':checked')) {
                //$('#dvpkg1').show();   // checked
            }
            else {
                //$('#dvpkg1').hide(); 
            }
            if ($('[name="deter_cost_copies"]').val() == 2) {
                //$('#edit-packaging-price-calculation1').hide();
                $('#scost').hide();
//			$('#dvpkg1').hide(); 
                $('#dvpr1').show();
            }
            if ($('[name="deter_cost_copies"]').val() == "3") {
                //$('#edit-packaging-price-calculation1').show();
                $('#scost').hide();
                $('#dvpr1').show();
                if ($('[name="packaging_price_calculation1[1]"]').is(':checked')) {
                    //$('#dvpkg1').show();   // checked
                }
                else {
                    //$('#dvpkg1').hide(); 
                }
            }
            if ($('[name="deter_cost_copies"]').val() == "1") {
                $('#scost').show();
                $('#dvpr1').hide();
                //$('#edit-packaging-price-calculation1').hide();
                //$('#dvpkg1').hide(); 
            }
            $('[name="deter_cost_copies"]').change(function () {
                var vlse = $('[name="deter_cost_copies"]').val();
                if (vlse == "1") {
                    $('#scost').show();
                    $('#dvpr1').hide();
                    //$('#edit-packaging-price-calculation1').hide();
                    //$('#dvpkg1').hide(); 
                }
                else if (vlse == "3") {
                    //$('#edit-packaging-price-calculation1').show();
                    $('#scost').hide();
                    $('#dvpr1').show();
                    if ($('[name="packaging_price_calculation1[1]"]').is(':checked')) {
                        //$('#dvpkg1').show();   // checked
                    }
                    else {
                        //$('#dvpkg1').hide(); 
                    }
                }
                else {
//$('#edit-packaging-price-calculation1').hide();
                    $('#scost').hide();
                    $('#dvpkg1').hide();
                    $('#dvpr1').show();
                }
            });
            $('.addlbl').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lbl");

                var newid = Number(latest[1]) + Number(1);

                $('#row' + newid).show();
            });
            $('.rmvlbl').on('click', function () {
                var lid = $(this).attr('id');
                //alert(lid);
                var latest = lid.split("lbl");
                //alert(latest[1]);
                $('#row' + latest[1]).find('input').val('');
                $('#row' + latest[1]).hide();
            });
            $('.addlblpk').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpk");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpk' + newid).show();
            });
            $('.rmvlblpk').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpk");
                $('#rowpk' + latest[1]).find('input').val('');
                $('#rowpk' + latest[1]).hide();
            });
            $('.addlblpack').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpk");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpack' + newid).show();
            });
            $('.rmvlblpack').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpk");
                $('#rowpack' + latest[1]).find('input').val('');
                $('#rowpack' + latest[1]).hide();
            });
            
            
            /*for (var pack = 2; pack < 30; pack++) {
                $('.rowpack' + pack).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttlpack_it"]').val();
            if (copies > 0) {
                for (var l = 1; l <= copies; l++) {
                    $('#rowpack_it' + l).show();
                }
            }*/
            
            /*** Form submit ***/
            /*$("button").click(function(){
             $("div").text($("form").serialize());
             });*/

            /*** Gallery Arrow ****/
            /*$('#gallery_arrow').click(
                    function (e) {
                        $('#gallery_id > option:selected').appendTo('#gallery_list');
                        $('#gallery_list > option:selected');
                        $('#gallery_id option').prop('selected', true);
                        e.preventDefault();
                    });
            $('#gallery_arrow_back').click(
                    function (e) {
                        $('#gallery_list > option:selected').appendTo('#gallery_id');
                        $('#gallery_list option').prop('selected', true);
                        e.preventDefault();
                    });*/

            //Edit Gallery Arrow
            /*$('#edit_gallery_arrow').click(
                    function (e) {
                        $('#gallery_id > option:selected').appendTo('#gallery_list');
                        $('#gallery_list > option:selected');
                        $('#gallery_list').each(function () {
                            $('#gallery_list option').attr("selected", "selected");
                        });
                        e.preventDefault();
                    });
            $('#edit_gallery_arrow_back').click(
                    function (e) {
                        $('#gallery_list > option:selected').appendTo('#gallery_id');
                        $('#gallery_list option').prop('selected', true);
                        e.preventDefault();
                    });*/

        }
    };
})(jQuery);
