/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    Drupal.behaviors.shippingModule = {
        attach: function (context, settings) {
            /************** it start ***************/
            $("#shippingit").hide();
            $("#edit-shipping-website-it").click(function () {
                if ($('#edit-shipping-website-it').is(":checked")) {
                    $("#shippingit").show();
                }
                else {
                    $("#shippingit").hide();
                }
            });
//edit-shipping-website-edit-it
            $("#shippingit").hide();
            $("#edit-shipping-website-edit-it").click(function () {
                if ($('#edit-shipping-website-edit-it').is(":checked")) {
                    $("#shippingit").show();
                }
                else {
                    $("#shippingit").hide();
                }
            });
            if ($('#edit-shipping-website-edit-it').is(":checked")) {
                $("#shippingit_edit").show();
            }



            /************** it end ***************/
            /************** fr start ***************/
            $("#shippingfr").hide();
            $("#edit-shipping-website-fr").click(function () {
                if ($('#edit-shipping-website-fr').is(":checked")) {
                    $("#shippingfr").show();
                }
                else {
                    $("#shippingfr").hide();
                }
            });
            $("#shippingfr").hide();
            $("#edit-shipping-website-edit-fr").click(function () {
                if ($('#edit-shipping-website-edit-fr').is(":checked")) {
                    $("#shippingfr").show();
                }
                else {
                    $("#shippingfr").hide();
                }
            });
            if ($('#edit-shipping-website-edit-fr').is(":checked")) {
                $("#shippingfr_edit").show();
            }
            /************** fr end ***************/

            /***************** fr start ************************/
            for (var pack = 2; pack < 10; pack++) {
                $('#rowpack_fr' + pack).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttlpack_fr"]').val();
            if (copies > 0) {
                for (var l = 1; l <= copies; l++) {
                    $('#rowpack_fr' + l).show();
                }
            }
            $('.addlblpack_fr').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_fr_");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpack_fr' + newid).show();
            });
            $('.rmvlblpack_fr').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_fr_");
                $('#rowpack_fr' + latest[1]).find('input').val('');
                $('#rowpack_fr' + latest[1]).hide();
            });
            /***************** fr end ************************/

            /***************** it start ************************/
            for (var pack = 2; pack < 10; pack++) {
                $('#rowpack_it' + pack).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttlpack_it"]').val();
            if (copies > 0) {
                for (var l = 1; l <= copies; l++) {
                    $('#rowpack_it' + l).show();
                }
            }
            $('.addlblpack_it').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_it_");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpack_it' + newid).show();
            });
            $('.rmvlblpack_it').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_it_");
                $('#rowpack_it' + latest[1]).find('input').val('');
                $('#rowpack_it' + latest[1]).hide();
            });
            /***************** it end ************************/
            /***************** fr cofficent start ************************/
            for (var pack = 2; pack < 13; pack++) {
                $('#rowpack_co_fr' + pack).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttlpack_co_fr"]').val();
            if (copies > 0) {
                for (var l = 1; l <= copies; l++) {
                    $('#rowpack_co_fr' + l).show();
                }
            }
            //lblpack_co_fr_
            $('.addlblpack_co_fr').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_co_fr_");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpack_co_fr' + newid).show();
            });
            $('.rmvlblpack_co_fr').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_co_fr_");
                $('#rowpack_co_fr' + latest[1]).find('input').val('');
                $('#rowpack_co_fr' + latest[1]).hide();
            });
            /***************** fr cofficent start ************************/




            /***************** it cofficent start ************************/
            for (var pack = 2; pack < 13; pack++) {
                $('#rowpack_co_it' + pack).hide();
            }
            // cost copies
            var copies = $('[name="hdcost_ttlpack_co_it"]').val();
            if (copies > 0) {
                
                for (var l = 1; l <= copies; l++) {
                    $('#rowpack_co_it' + l).show();
                }
            }
            //lblpack_co_fr_
            $('.addlblpack_co_it').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_co_it_");
                var newid = Number(latest[1]) + Number(1);
                $('#rowpack_co_it' + newid).show();
            });
            $('.rmvlblpack_co_it').on('click', function () {
                var lid = $(this).attr('id');
                var latest = lid.split("lblpack_co_it_");
                $('#rowpack_co_it' + latest[1]).find('input').val('');
                $('#rowpack_co_it' + latest[1]).hide();
            });
            /***************** it cofficent start ************************/

            /****************** End Files *********************/
        }
    };
})(jQuery);

jQuery(document).ready(function() {
    if (jQuery('#edit-shipping-website-edit-it').is(":checked")) {
        jQuery("#shippingit").show();
    }
    else {
        jQuery("#shippingit").hide();
    }
    if (jQuery('#edit-shipping-website-edit-fr').is(":checked")) {
        jQuery("#shippingfr").show();
    }
    else {
        jQuery("#shippingfr").hide();
    }
});