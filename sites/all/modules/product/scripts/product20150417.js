(function($) {
Drupal.behaviors.productModule = {
  attach: function (context, settings) {
    //code starts

       $("#edit-languages").change(function(){
        var lan = $("#edit-languages").val(); 
        if(lan =="it") {
           $("#website_id").val("meprint.it") 
        } else if(lan =="fr") {
             $("#website_id").val("meprint.fr") 
        }
      
    });
      /*** add website end ***/
       $('#arrow_group_mat').click(
                                 function (e) {
                                     $('#total_mat_list > option:selected').appendTo('#assign_mat_list');
                                     e.preventDefault();
                                 });

       $('#arrow_group_back_mat').click(
             function (e) {
                 $('#assign_mat_list > option:selected').appendTo('#total_mat_list');
                 e.preventDefault();
             });
      //code ends
       $(".form-item-extra-field-name-fr").hide();
       $(".form-item-extra-field-name-it").hide();
       $('input[name="is_extrafield_required[1]"]').click(function () {
           if ($('input[name="is_extrafield_required[1]"]').is(':checked')) {
               $(".form-item-extra-field-name-fr").show();
               $(".form-item-extra-field-name-it").show();
           }
           else {
               $(".form-item-extra-field-name-fr").hide();
               $(".form-item-extra-field-name-it").hide();
           }
       });
     var iCnt2 = 0;
    $("#determination").change(function() {
        var id = $("#determination").val();
        var htmls = '';
        if(id === '2') {
         htmls +='Specific Cost Copies:\n\
                    <input type="button" name="addbutton" class="addbutton" id="addbutton"/>\n\
                    <input type="removebutton" name="removebutton" class="removebutton" id="removebutton"/><br/>';
                      /*  html +='Cost Varies According to no of Copies<br/>';
                        for(var i=0;i<10;i++) {
                         html +='From<input type="textfield" name="dim'+i+'" value=""><br/>';
                         
                         html +='To<input type="textfield" name="dim'+i+'" value=""><br/>';
                     } */
          
                        $("#copy").html(htmls);
        }  else if(id === "3") {
           
                    /*   html +='Cost Varies According to sq<br/>';
                        for(var i=0;i<10;i++) {
                         html +='From<input type="textfield" name="dim'+i+'" value="">';
                        
                         html +='To<input type="textfield" name="dim'+i+'" value="">';
                     }
                        $("#copy").html(html); */
           
        }
      
       $('#addbutton').click(function(){
            
       //$("#copy").html("Gdfgdfgdf");
       //if (iCnt2 <= 10) {
                        // iCnt2 = iCnt2 + 1;
                  /*  var ghj = '';
                    ghj +='To <input type="text" name="to" value=""/>\n\
                        From<input type="text" name="from" value=""/>\n\
                        <input type="text" name="cost" value=""/>'
                         
                       $("#result").html(ghj); */
      // } 
      
  });
    });
    $('#dvfr').hide();$('#dvit').hide();$('#hasextra_it').hide();$('#hasextra_fr').hide();
	$('input[name="biglanguages[fr]"]').click(function() { 
		if($('input[name="biglanguages[fr]"]').is(':checked')){ 
                $('#dvfr').show();
         }else {
                $('#dvfr').hide();
         }
	});
	$('input[name="extra_processing_fr[1]"]').click(function() { 
		if($('input[name="extra_processing_fr[1]"]').is(':checked')){ 
                $('#hasextra_fr').show();
         }else {
                $('#hasextra_fr').hide();
         }
	});
	$('input[name="extra_processing_it[1]"]').click(function() { 
		if($('input[name="extra_processing_it[1]"]').is(':checked')){ 
                $('#hasextra_it').show();
         }else {
                $('#hasextra_it').hide();
         }
	});
	$('input[name="biglanguages[it]"]').click(function() { 
		if($('input[name="biglanguages[it]"]').is(':checked')){ 
                $('#dvit').show();
         }else {
                $('#dvit').hide();
         }
	});
	$("#hasextra").hide();	
    /////////// Add a quote  JS start////////////
         $("#edit-extra-processing-1").click(function () {  
         if($("#edit-extra-processing-1").is(':checked')){ 

                  $("#hasextra").show();
         }else {

                $("#hasextra").hide();
         }
      });
	  //new
	  if($("#edit-extra-processing-1").is(':checked')){ 

                  $("#hasextra").show();
         }
	  
   $(".form-item-acc-with-list").hide();
      $(".form-item-min-copies").hide();  $(".form-item-destination-use").hide();    
       $("#edit-destination-id-1").click(function () {  
           if($("#edit-destination-id-1").is(':checked')){
                  $(".form-item-destination-use").show();
            }else {
                   $(".form-item-destination-use").hide();
            } 
       }); $("#edit-sell-with-structure-1").click(function () {
         if($("#edit-sell-with-structure-1").is(':checked')){
              $(".form-item-acc-with-list").show();
              $("#dvaccess").hide();
         } else {
               $(".form-item-acc-with-list").hide();
               $("#dvaccess").show();

         }
      });
$("#edit-promo-flag-promo").click(function () {

        if($("#edit-promo-flag-promo").is(':checked')){
            $(".form-item-promotion-price").show();  // checked
            }       
});
/*** promo discount ****/
 $(".form-item-promo-flag-it").hide();$(".form-item-promotion-price-fr").hide();
$("#edit-promo-flag-it-promo").click(function () {
    if($("#edit-promo-flag-it-promo").is(':checked')){
            $(".form-item-promo-flag-it").show();  // checked
       }      
	});
	$("#edit-promo-flag-it-best-price").click(function () {
       if($("#edit-promo-flag-it-best-price").is(':checked')){
            $(".form-item-promo-flag-it").hide();  // checked
       }      
	});
	$("#edit-promo-flag-fr-promo").click(function () {
       if($("#edit-promo-flag-fr-promo").is(':checked')){
            $(".form-item-promotion-price-fr").show();  // checked
       }      
	});
	$("#edit-promo-flag-fr-best-price").click(function () {
       if($("#edit-promo-flag-fr-best-price").is(':checked')){
            $(".form-item-promotion-price-fr").hide();  // checked
       }      
	});
	// end promo discount
	/** has Extra process ***/
	if($("#edit-extra-processing-fr-1").is(':checked')){
		$("#hasextra_fr").show();  // checked
	}
	/*** Main Langugaes ****/
	if($("#edit-biglanguages-fr").is(':checked')){
		$('#dvfr').show();// checked
	}
	if($("#edit-biglanguages-it").is(':checked')){
		$('#dvit').show();// checked
	}
	/*** Destionation ****/
	if($("#edit-destination-id-1").is(':checked')){
		$('.form-item-destination-use').show();// checked
	}
	if($("#edit-choice-of-copy-number-1").is(':checked')) {    
		$(".form-item-min-copies").show();  // checked     
	}//new
	if($("#edit-choice-of-copy-number-1").is(':checked')) { 	   
		$(".form-item-min-copies").show();  // checked 
		
	}
	else {
		  $(".form-item-min-copies").hide(); 
	}
//new
 if($("#edit-promo-flag-promo").is(':checked')){
            $(".form-item-promotion-price").show();  // checked
            } 
$("#edit-promo-flag-best-price").click(function () {
       if($("#edit-promo-flag-best-price").is(':checked')){
            $(".form-item-promotion-price").hide();  // checked
       }      
});
 
  
 
 if($("#edit-promo-flag-promo").is(':checked')){ 
     
            $(".form-item-promotion-price").show();  // checked
 }   else {
     $(".form-item-promotion-price").hide();
 }
 
 
 
 
$("#edit-choice-of-copy-number-1").click(function () {
       if($("#edit-choice-of-copy-number-1").is(':checked')){
            $(".form-item-min-copies").show();  // checked
       }
       else{
           $(".form-item-min-copies").hide();  // unchecked
       }
});
 $("#extra_processing_id").hide(); 
 $(".form-item-accessories-id").hide(); 
 //$(".form-item-min-copies").hide(); 
 //$(".form-item-promotion-price").hide();
  $(".extra_processing_id").hide(); 
    /////////// Add a quote  JS END////////////


    /////////// Add a Processing  JS start////////////
	$('#dvextraprocess').hide();
    $("#edit-is-extra-file-1").click(function () {
      if($("#edit-is-extra-file-1").is(':checked')){
            $('#dvextraprocess').show(); // checked
         }
       else{
           $('#dvextraprocess').hide();  // unchecked
       }
    });
	
	if($("#edit-is-extra-file-1").is(':checked')){
            $('#dvextraprocess').show(); // checked
         }
	$(".form-item-extra-field-name_fr").hide();
    $("#edit-is-extrafield-required-1").click(function () {
       if($("#edit-is-extrafield-required-1").is(':checked')){
           $(".form-item-extra-field-name_fr").show();  // checked
                   }
       else{
           $(".form-item-extra-field-name_fr").hide();  // unchecked
       }
     });	 	  
      $("#processing-add-record-form").submit(function() {
        
          if($("#edit-is-extra-file-1").is(':checked')){
           if($('#edit-extra-file-name').val().length == 0 ) {
              alert("1");
              event.preventDefault();
             } 
           }
       if($("#edit-is-extrafield-required-1").is(':checked')){
         if($('#edit-extra-field-name').val().length == 0 ) {
            alert("2");
            event.preventDefault();
                 } 
            }  
      });     
     
         if($("#edit-is-extra-file-1").is(':checked'))
            $(".form-item-extra-file-name").show();  // checked
        else
           $(".form-item-extra-file-name").hide();  // unchecked
       
        if($("#edit-is-extrafield-required-1").is(':checked'))
             $(".form-item-extra-field-name_fr").show();  // checked
        else
           $(".form-item-extra-field-name_fr").hide();  // unchecked
       
 
    /////////// Add processing  JS END////////////
 /////////// Date popup  JS start////////////
$("#edit-prlanguage1").change(function() {
	var plang = $("#edit-prlanguage").val(); 
	alert(plang);
	$.ajax({
		url: "http://meprint.xool.it/sites/all/themes/meprint/search-lang.php",
		data: { term: plang},
		dataType: "html",
		type: "POST",
		success: function(data){			
			//console.log(data);
			//var yourArray = data.toArray();
			
				//var newj = data.toString();
				//console.log(yourArray);
			$("#pr_groups_update").html(data);
		}		
	});     
});
 $('input[name="validity_end_date[date]"]').change(function() { 
   
    var start_date = $("input[name='validity_start_date[date]']").val();
    var end_date = $("input[name='validity_end_date[date]']").val();
    if (Date.parse(start_date) > Date.parse(end_date)) { 
        $("#error").show();
        return false;
    } else { 
        
         $("#error").hide();
         return true;
    }
    
  });
  
  $("#hasextra_process").hide();  
     $(document).ready(   
	 
            function() {   
			/*****  Materails ****/
               $('#arrow_it').click(  
                    function(e) {  
                        $('#material_group_id_it > option:selected').appendTo('#material_group_list_it');  
                        e.preventDefault();  
                    });  
               $('#arrowback_it').click(  
                    function(e) {  
                        $('#material_group_list_it > option:selected').appendTo('#material_group_id_it');  
                        e.preventDefault();  
                }); 
				$('#arrow_fr').click(  
                    function(e) {  
                        $('#material_group_id_fr > option:selected').appendTo('#material_group_list_fr');  
                        e.preventDefault();  
                    });  
               $('#arrowback_fr').click(  
                    function(e) {  
                        $('#material_group_list_fr > option:selected').appendTo('#material_group_id_fr');  
                        e.preventDefault();  
                }); 
/*****  Materails ****/	
/*****  Process ****/			
               $('#arrow_process_it').click(  
                    function(e) {  
                        $('#small_extra_processing_id_it > option:selected').appendTo('#small_extra_processing_list_it');  
                        e.preventDefault();  
                    });  
               $('#arrow_process_back_it').click(  
                    function(e) {  
                        $('#small_extra_processing_list_it > option:selected').appendTo('#small_extra_processing_id_it');  
                        e.preventDefault();  
                });  
				 $('#arrow_process_fr').click(  
                    function(e) {  
                        $('#small_extra_processing_id_fr > option:selected').appendTo('#small_extra_processing_list_fr');  
                        e.preventDefault();  
                    });  
               $('#arrow_process_back_fr').click(  
                    function(e) {  
                        $('#small_extra_processing_list_fr > option:selected').appendTo('#small_extra_processing_id_fr');  
                        e.preventDefault();  
                }); 
				/*****  Process ****/
				/*****  Process ****/
				 $('#arrow_big_process_it').click(  
                    function(e) {  
                        $('#big_extra_processing_id_it > option:selected').appendTo('#big_extra_processing_list_it');  
                        e.preventDefault();  
                    });  
               $('#arrow_big_process_back_it').click(  
                    function(e) {  
                        $('#big_extra_processing_list_it > option:selected').appendTo('#big_extra_processing_id_it');  
                        e.preventDefault();  
                });
				$('#arrow_big_process_fr').click(  
                    function(e) {  
                        $('#big_extra_processing_id_fr > option:selected').appendTo('#big_extra_processing_list_fr');  
                        e.preventDefault();  
                    });  
               $('#arrow_big_process_back_fr').click(  
                    function(e) {  
                        $('#big_extra_processing_list_fr > option:selected').appendTo('#big_extra_processing_id_fr');  
                        e.preventDefault();  
                });
				/*****  Process ****/
				/*****  Dimensions ****/
                  $('#arrow_dimensions_it').click(  
                    function(e) {  
                        $('#dimension_id_it > option:selected').appendTo('#dimension_list_it');  
                        e.preventDefault();  
                    });  
               $('#arrow_dimensions_back_it').click(  
                    function(e) {  
                        $('#dimension_list_it > option:selected').appendTo('#dimension_id_it');  
                        e.preventDefault();  
                });  
				$('#arrow_dimensions_fr').click(  
                    function(e) {  
                        $('#dimension_id_fr > option:selected').appendTo('#dimension_list_fr');  
                        e.preventDefault();  
                    });  
               $('#arrow_dimensions_back_fr').click(  
                    function(e) {  
                        $('#dimension_list_fr > option:selected').appendTo('#dimension_id_fr');  
                        e.preventDefault();  
                });    
				/*****  Dimensions ****/
                /*****  Accessoties ****/
                  $('#access_id_arrow_it').click(  
                    function(e) {  
                        $('#acc_id_it > option:selected').appendTo('#acc_id_list_it');  
                        e.preventDefault();  
                    });  
               $('#access_id_arrow_back_it').click(  
                    function(e) {  
                        $('#acc_id_list_it > option:selected').appendTo('#acc_id_it');  
                        e.preventDefault();  
                });
				/*****  Accessoties ****/
                  $('#access_id_arrow_fr').click(  
                    function(e) {  
                        $('#acc_id_fr > option:selected').appendTo('#acc_id_list_fr');  
                        e.preventDefault();  
                    });  
               $('#access_id_arrow_back_fr').click(  
                    function(e) {  
                        $('#acc_id_list_fr > option:selected').appendTo('#acc_id_fr');  
                        e.preventDefault();  
                });
                 /*****  Accessoties ****/
                
               
            });

/////////// Date popup  JS end////////////

    /////////// Add Material  JS start////////////
    
    if($('input[id=edit-small-extra-processing-1]:checked').val() == 1 ){
                $("#hasextra_process").show();   // checked
     }
     else {
               $("#hasextra_process").hide();  // unchecked
      }//for edit process
      if ($('#edit-languages-it').is(":checked")) {
                 $("#it").show(); 
        } else {
        $("#it").hide(); 
        }//for edit

if ($('#edit-languages-fr').is(":checked"))  { 
 $("#fr_edit").show(); 
 }
 if ($('#edit-languages-it').is(":checked"))  { 
 $("#fr_edit").show(); 
 }//for edit
      $("#edit-small-extra-processing-1").click(function () {  
      if($('input[id=edit-small-extra-processing-1]:checked').val() == 1 ){
            $("#hasextra_process").show();   // checked
         }
       else{
           $("#hasextra_process").hide();  // unchecked
       }
    });   
	$("#it").hide();
	$("#fr").hide();
	$("#edit-languages-it").click(function () {  
		
		if ($('#edit-languages-it').is(":checked"))
{
  $("#it").show(); 
   $("#it_edit").show();
} else {
$("#it").hide(); 
  $("#it_edit").hide();

}
       
	
	}); 
	
	$("#edit-languages-fr").click(function(){
		
		if ($('#edit-languages-fr').is(":checked"))
{
  $("#fr").show(); 
  $("#fr_edit").show();
} else {
$("#fr").hide(); 
$("#fr_edit").hide();
}
	
	});
	
  $("#edit-fire-proof").click(function () {
         
      if($('input[name=fire_proof]:checked').val() == 1 ){
            $(".form-item-files-fire-proof-pdf").show();   // checked
         }
       else{
           $(".form-item-files-fire-proof-pdf").hide();  // unchecked
       }
    });   	
      $("#material-add-record-form").submit(function() {
         if($('input[name=fire_proof]:checked').val() == 1 ){
              
           if( $("#edit-fire-proof-pdf").val == '') {
               
             alert("Plese attach a file");
             event.preventDefault(); 
           }
           }

      });     
      var iCnt = 0;
            var container = $(document.createElement('div')).css({
            padding: '5px', margin: '20px', width: '170px', border: '1px dashed',
            borderTopColor: '#999', borderBottomColor: '#999',
            borderLeftColor: '#999', borderRightColor: '#999'
        });
        
        $("#productgroup_id_fr").change(function() {  
       
        var data = $("#productgroup_id_fr").val(); 
        $.ajax({
            type:'POST',
            url: 'ajax/values1',
            data: 'sub='+data+'&lang=fr',
            dataType:'json',
            success:function(results) {			
                 var html12= '<option value="">-Select-</option>';
                 $.each(results,function(key,value) {  
                    html12 +='<option value='+key+'>'+value+'</option>';
                 });
                // $('name#subcat_id').html(html12);
                $('[name="subgroup_id_fr"]').html(html12);
             }
       });
    });
	//new
	$("#productgroup_id").change(function() {  
       
        var data = $("#productgroup_id").val(); 
		var language = $('[name="oldlanguageid"]').val();
		$.ajax({
            type:'POST',
            url: 'ajax/values1',
            data: 'sub='+data+'&lang='+language,
            dataType:'json',
            success:function(results) {			
                 var html12= '<option value="">-Select-</option>';
                 $.each(results,function(key,value) {   
				
                    html12 +='<option value='+key+'>'+value+'</option>';
                 });
              
                $('[name="subgroup_id"]').html(html12);
             }
       });
    });
	
	 $("#productgroup_id_it").change(function() {  
       
        var data = $("#productgroup_id_it").val(); 
        $.ajax({
            type:'POST',
            url: 'ajax/values1',
             data: 'sub='+data+'&lang=it',
            dataType:'json',
            success:function(results) {
                 var html12= '<option value="">-Select-</option>';
                 $.each(results,function(key,value) {  
                    html12 +='<option value='+key+'>'+value+'</option>';
                 });               
                $('[name="subgroup_id_it"]').html(html12);
             }
       });
    });
  
         $("#subgroup_id").change(function() {
             var newValue = $("#subgroup_id").val();
            // alert(newValue);
             //alert($("#hiddenValue").val(newValue));
             $("#hiddenValue").val(newValue); 
         });
        
        $("#addId").click(function() {
            $.ajax({
             url: 'ajax/extraprocessing',
             dataType:'json',
             success:function(result) {
                 
                   var html1 = '';
                   if (iCnt <= 5) {
                         iCnt = iCnt + 1;
                         html1 +='<br/><select class="input" id=tb' + iCnt + ' name=extra_processing[] >';
                         $.each(result,function(key,value) { 
                             
                           html1 +='<option value="'+key+'">'+value+'</option>';
                         });
                         html1 +='</select>';
                      
                        $('#dvupdate1').append(html1);
                      
                   } else {
                        $('#dvupdate1').append('<label>Reached the limit</label>'); 
                        $('#addId').attr('class', 'bt-disable'); 
                        $('#addId').attr('disabled', 'disabled');
                   }   
             }
            });
               return false;
        });
 /////////// Add Button   JS end////////////
 /////////// REMOVE Button   JS start////////////
$('#removeId').click(function() {   // REMOVE ELEMENTS ONE PER CLICK.
         if (iCnt != 0) { $('#tb' + iCnt).remove();  iCnt = iCnt - 1; }
         if (iCnt == 0) { $(container).empty(); 
                $(container).remove(); 
                $('#addId').removeAttr('disabled'); 
                $('#addId').attr('class', 'bt') 
        }
        return false;
});
 /////////// REMOVE Button   JS end//////////// 
 
  
 
 $(".delete").click(function() {

    if(confirm("Are You Sure You Want to Delete Record!")) {  
         var value = $(this).attr('id');
         var name = $(this).attr('name');
         var url='';
         if(name === 'websites') {
             url+='ajax/deletewebsite';
         } else if(name === "templates") {
               url+='ajax/deletetemplate';
         } else if(name === "weight") {
               url+='ajax/deleteweight';
         } else if(name === "price") {
              url+='ajax/deleteprice';
         } else if(name === "product") {
              url+='ajax/deleteproduct';
         } else if(name === "dimension") {
              url+='ajax/deletedimensions';
         } else if(name === "material") {
              url+='ajax/deletematerial';
         } else if(name === "materialgroup") {
               url+='ajax/deletematerialgroup';
         } else if(name === "productgroup") {
              url+='ajax/deleteproductgroup';
         } else if(name === "productsubgroup") {
             url+='ajax/deleteproductsubgroup';
         } else if(name === "accessory") {
             url+='ajax/deleteaccessories';
         } else if(name === "processingkind") {
             url+='ajax/deleteprocessingkind';
         } else if(name === "processingbig") {
               url+='ajax/deleteprocessingbig';
         } else if(name === "discounts") {
             url+='ajax/deletediscounts';
         } else if(name === "promotionkind") {
             url+='ajax/deletepromotionkind';
         } else if(name === "squote") {
             url+='ajax/deletesmallquote';
         }
         $.ajax({
         type: "POST",
         data: 'name='+name+'&value='+value,
         url : url,
		
       success:function(result) {  
            window.location.href='view';
         }, error: function (error) {  
           
            $("#update").append("<h4 style='color:red;'>Unable to delete record</h4>").fadeOut(5000);
            window.location.href='view';
         }     
        }); 
        } else {
          return false;
        }
 });
          if($('input[name=fire_proof]:checked').val() == 1 )
            $(".form-item-files-fire-proof-pdf").show();   // checked
         else
           $(".form-item-files-fire-proof-pdf").hide();  // unchecked
       
       
       /////////// Add Material  JS END////////////

   

  }
};

        var i = $('#pricecalc p').size() + 1;
        
        $('#addScnt').live('click', function() {
            var scntDiv = '<div id="abc_'+ i +'"><p><label for="p_scnts">From: <input type="text" id="p_scnt_frm'+ i +'" size="20" name="p_scnt_frm' + i +'" value="" placeholder="Input Value" /></label>';
            scntDiv += '<label for="p_scnts">To: <input type="text" id="p_scnt_to'+ i +'" size="20" name="p_scnt_to' + i +'" value="" placeholder="Input Value" /></label>';
            scntDiv += '<label for="p_scnts">Euro: <input type="text" id="p_scnt_price'+ i +'" size="20" name="p_scnt_price' + i +'" value="" placeholder="Input Value" /></label></p></div>';
            $("#hideCount").val(i);
          
            $("#pricecalc").append(scntDiv);
            //$('#p_scnt_frm'+i).attr("disabled","disabled");
            if( i == 1){
               
                //$('input[name=p_scnt_frm'+i+']').change(function() {  });
                $("#p_scnt_frm"+1).attr("value", 1);
                $("#p_scnt_to"+1).attr("value", 999);
            }
            

            
            i++;
            return false;
        });
        
        $('#remScnt').live('click', function() { 
            var j = i-1;
                if( i > 1 ) {
                        //$(this).parents('p').remove();
                        $( "#abc_"+j ).remove();
                        i--;
                }
                 $("#hideCount").val(i-1);
				 
				 $("#edit-subgroup-id-fr").change(function() { 
    var material_id_selected_name = $("#edit-subgroup-id-fr option:selected").val();
	alert(material_id_selected_name);
	
	});
        
                return false;
        });
})(jQuery);

var iCnt1=0;
function myFunction() {
  
    iCnt1 = iCnt1 + 1;
    var htmls = '';
    htmls +='<br/><input type="text" id=tb' + iCnt1 + ' name=extra_processing[] ><input type="button" value="-" onclick="myFunctionRemove(iCnt1);"  >';
    $('#pricecalc').append(htmls);
    
     
}
function myFunctionRemove(iCnt1) {       
    alert(iCnt1);
         $('tb' + iCnt1).remove();
       /*  if (iCnt1 != 0) { $('tb' + iCnt1).remove(); iCnt1 = iCnt1 - 1; }
        
         if (iCnt1 == 0) { $('#pricecalc').empty(); 
                 $('#pricecalc').remove(); 
                $('.removepricebtn').removeAttr('disabled'); 
               // $('#addId').attr('class', 'bt') 
        } */

}