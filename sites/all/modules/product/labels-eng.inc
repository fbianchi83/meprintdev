<?php 
global $language ;
$lang_name = $language->language ;
if($lang_name == "en"){
$lbl_sno = t('S.No');
$lbl_name = t('Name');
$lbl_desc = t('Description');
$lbl_status= t('Status');
$lbl_edit= t('Edit');
$lbl_delete= t('Delete');
$lbl_manage_categories= t('MANAGE CATEGORIES');
$lbl_add_category=t('ADD PRODUCT GROUP');
$lbl_active = t('Active');
$lbl_inactive = t('In-Active');
$lbl_product_group = t('Product Group');
$lbl_group_status = t('Group Status');
$lbl_product_title = t('Product Group Name');
$lbl_desc = t('Description');
$lbl_product = t('Product');
$lbl_product_details = t('Product Details');
$lbl_add_Product_group = t('Add Product Group');
$lbl_add_product_group_record = t('Add Product group record');
$lbl_manage_product_group = t('MANAGE PRODUCT GROUP');
$lbl_view_productgroup_list = t('View Product Group List');
$lbl_edit_product_group = t('Edit Product Group');
$lbl_edit_accessory = t('Edit Accessory');
$lbl_delete_accessory = t('Delete Accessory');
$lbl_delete_product_group = t('Delete Product Group');
$lbl_get_extra_processing_list= t('Get Extra Processing List');
$lbl_Add_Product_Subgroup = t('Add Product Subgroup');
$lbl_Add_Product_subgroup_record = t('Add Product subgroup record');

$lbl_product_sub_group_name = t('Product Sub Group Name');
$lbl_select_product_group = t('Select Product Group');


$lbl_submit = t('Submit');
$lbl_cancel = t('Cancel');
}
else if ($lang_name == "fr"){
$lbl_sno = t('S.No');
$lbl_name = t('Nom');
$lbl_desc = t('description');
$lbl_status= t('Statut');
$lbl_edit= t('éditer');
$lbl_delete= t('effacer');
$lbl_manage_categories= t('Gérer les catégories');
$lbl_add_category=t('Ajouter des catégories');
$lbl_active = t('actif');
 $lbl_inactive = t('En-Active');
$lbl_product_group = t('Groupe de produits');
$lbl_group_status = t('Statut de groupe');
$lbl_product_title = t('Nom Groupe produit');
$lbl_desc = t('description');
$lbl_product = t('produit');
$lbl_product_details = t('Détails du produit');
$lbl_add_Product_group = t('Ajouter groupe de produits');
$lbl_add_product_group_record = t('Ajouter les fiches de groupe de produit');
$lbl_manage_product_group = t('GESTION GROUPE DE PRODUIT');
$lbl_view_productgroup_list = t('Liste Voir groupe de produits');
$lbl_edit_product_group = t('Modifier le groupe produit');
$lbl_edit_accessory = t('Modifier accessoire');
$lbl_delete_accessory = t('supprimer accessoire');
$lbl_delete_product_group = t('Supprimer le groupe produit');
$lbl_get_extra_processing_list= t('Obtenir la liste de traitement supplémentaire');
$lbl_Add_Product_Subgroup = t('Ajouter sous-groupe produit');
$lbl_Add_Product_subgroup_record = t('Ajouter un produit record du sous-groupe');

// Sub Group

$lbl_product_sub_group_name = t('Nom du produit Sub Groupe');
$lbl_select_product_group = t('Sélectionnez Groupe de produits');

$lbl_submit = t('Soumettre');
$lbl_cancel = t('Annuler');
}
else if($lang_name == "it"){
$lbl_sno = t('S.No');$lbl_name = t('Name');
$lbl_desc = t('descrizione');
$lbl_status= t('Stato');
$lbl_edit= t('Modifica');
$lbl_delete= t('Elimina');
$lbl_manage_categories= t('GESTIRE CATEGORIE');
$lbl_add_category=t('AGGIUNGI CATEGORIA');
$lbl_product_group = t('Gruppo prodotti');
$lbl_active = t('attivo');
$lbl_inactive = t('disattiva'); 
$lbl_group_status = t('Stato gruppo');
$lbl_product_title = t('Gruppo prodotti');
$lbl_desc = t('descrizione');
$lbl_product = t('prodotto');
$lbl_product_details = t('Dettagli prodotto');
$lbl_add_Product_group = t('Aggiungi gruppo prodotti');
$lbl_add_product_group_record = t('Aggiungere record gruppo prodotti');
$lbl_manage_product_group = t('GESTIRE IL GRUPPO PRODOTTI');
$lbl_view_productgroup_list = t('Elenco Visualizza Gruppo prodotti');
$lbl_edit_product_group = t('Modifica gruppo prodotti');
$lbl_edit_accessory = t('Modifica Accessori');
$lbl_delete_accessory = t('Elimina Accessori');
$lbl_delete_product_group = t('Elimina gruppo prodotti');
$lbl_get_extra_processing_list= t('Get List Processing Extra');
$lbl_Add_Product_Subgroup = t('Aggiungi prodotto Sottogruppo');
$lbl_Add_Product_subgroup_record = t('Aggiungi sottogruppo di registrazione del prodotto');

// product Sub Group

$lbl_product_sub_group_name = t('Sub Group Product Nome');
$lbl_select_product_group = t('Seleziona gruppo di prodotti');

$lbl_submit = t('Prosegui');
$lbl_cancel = t('Annulla');
}

?>