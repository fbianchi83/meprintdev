(function($) {
Drupal.behaviors.ordersModule = {
  attach: function (context, settings) {
    //code starts
	$('img.downloadable').each(function(){           
        var $this = $(this);
        $this.wrap('<a href="' + $this.attr('src') + '" download />')
    });
    
    $('.cartdetails--open').click(function() {
                $(this).children('.fa').toggleClass('fa-plus fa-minus');
                var collapse = $(this).parents('.cartdetailspage').find('.collapse');
                if( collapse.hasClass('in') ){
                  collapse.css('margin-top','0');
                } else {
                  collapse.css('margin-top','7px')
                }

              });
              
	$(".ttlcom").hide();
	/**** INline edit for order elements ***/
	/*$(".inlineedit").change(function(){
		var r = confirm("Sicuro di voler cambiare stato del prodotto?");
		if (r == true) {		
			var id = $(this).attr("id");
			var myval = $(this).val();
			$.ajax({
			 type: "POST",
			 data: 'name='+id+'&value='+myval,
			 url : 'order_elements/ajax/inlinedit',
			 success:function(result) {  
			//  location.reload=();
			   window.location.href='order_elements';
			 }
			});
		}
		else{
			window.location.href='order_elements';
		}
	});*/
	$(".inline_operator_edit").change(function(){
		var r = confirm("Sicuro di voler cambiare stato al prodotto?");
		if (r == true) {		
			var id = $(this).attr("id");
			var myval = $(this).val();
			$.ajax({
			 type: "POST",
			 data: 'order_element_id='+id+'&status='+myval,
			 url : 'ajax/inline_product_edit',
			 success:function(result) { 
				location.reload();
				//window.location.href='order_elements';
			 }
			});
		}
		else{
			//window.location.href='order_elements';
		}
	});	
        
        $(".inline_operator_edit2").change(function(){
		var r = confirm("Sicuro di voler cambiare stato al prodotto?");
		if (r == true) {		
			var id = $(this).attr("id");
			var myval = $(this).val();
			$.ajax({
			 type: "POST",
			 data: 'order_element_id='+id+'&status='+myval,
			 url : 'ajax/inline_product_edit2',
			 success:function(result) { 
				location.reload();
				//window.location.href='order_elements';
			 }
			});
		}
		else{
			//window.location.href='order_elements';
		}
	});	
        
        $(".inline_operator_edit3").off().on('change', function(){
            console.log($(this).val());
		var r = confirm("Sicuro di voler cambiare stato all'ordine?");
		if (r == true) {		
			var id = $(this).attr("id");
			var myval = $(this).val();
			$.ajax({
			 type: "POST",
			 data: 'order_id='+id+'&status='+myval,
			 url : 'ajax/inline_order_edit',
			 success:function(result) { 
				location.reload();
				//window.location.href='order_elements';
			 }
			});
		}
		else{
			//window.location.href='order_elements';
		}
	});	
        
        $(".inline_operator_edit4").change(function(){
		var r = confirm("Sicuro di voler cambiare stato al prodotto?");
		if (r == true) {		
			var id = $(this).attr("id");
			var myval = $(this).val();
			$.ajax({
			 type: "POST",
			 data: 'order_element_id='+id+'&status='+myval,
			 url : 'ajax/inline_product_edit',
			 success:function(result) { 
				location.reload();
				//window.location.href='order_elements';
			 }
			});
		}
		else{
			//window.location.href='order_elements';
		}
	});	
        
        $(".plusorder").click(function(){
		var id = $(this).attr("id");
		var n = id.lastIndexOf('_');
                id = id.substring(n + 1);
                
		$.ajax({
                    type: "POST",
                    data: 'order_id='+id,
                    url : 'ajax/order_info',
                    success:function(result) { 
                        //console.log(result);
                        $('#order'+id).html(result);
			//window.location.href='order_elements';
                    }
		});
        });	
        
	function htmlEntities(str) {
	return String(str).replace('<', '&amp;lt;').replace('>', '&amp;gt;').replace('"', '&amp;quot;');
	}	
	$(".leave-comments").click(function () {
		var id = $(this).attr("id");
		var strid = id.split("edit-leave-comment-");
		var str1id = strid[1].split("-");	
       if($(".leave-comments").is(':checked')){		
            $("#ttl_"+str1id[0]).show(); // checked
			
       } else {
           $("#ttl_"+str1id[0]).hide();
       }
	});
	$(".errortemplates").change(function(){		
		var id = $(this).attr("id");			
		var myvals = $(this).val();			
		var myurl = $('#hdbasepath').val();	
		var usern = $('#ndusername').val();
		var imgpath = $("#hdimgpath").val();
		var bpath = $("#hdbasepath").val();
		$.ajax({
			type: "POST",
			url: myurl,
			data:'name='+id+'&temp='+myvals+'&user='+usern+'&imgpath='+imgpath+'&bpath='+bpath,		
			dataType:'json',
			success:function(res){   							
				$.each(res, function(i,item){								
					$('input[name="leave_subject_'+item.id+'"]').val(item.subject);				
					$("textarea#edit-leave-message-"+item.id).val(htmlEntities(item.message));                
				});
			}
		});
	});
	$("#order-view").on("click", function () {
            var divContents = $(".content").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Product Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
    });	
    
    $(".adminupload").change(function(){
         var id = $(this).attr('name');    
         var strnumber = id.split("img_");
         var F = this.files;
          if(F && F[0]) for(var i=0; i<F.length; i++) dispalyImage(F[i],strnumber[1],id);
    });
    function dispalyImage(file,divID,id) {
            var productwidth = 0;
            var productheight = 0;
             var displaywidth = 100;
             var displayheight = 100;
              var dimensionFinder ;
              dimensionFinder = $('input[name=dbdim]').val();
              var res = dimensionFinder.split("x");
              productwidth = res[0];
              productheight = res[1];
              var productarea = parseFloat(productheight) * parseFloat(productwidth);
              var productno = parseFloat(productarea)/ 10000;
              productno = Math.ceil(productno);
                if(productno == 0){
                    productno = 1;
                }
         var minwdth = 2400 * parseFloat(productno);
         var maxwdth = 7500 * parseFloat(productno);
         var reader = new FileReader();
         var image  = new Image();
         reader.readAsDataURL(file);  
         reader.onload = function(_file) {
         image.src    = _file.target.result;              // url.createObjectURL(file);
         image.onload = function() {
            var w = this.width,
                h = this.height,
                t = file.type,                           // ext only: // file.type.split('/')[1],
                n = file.name,
                s = ~~(file.size/1024) +'KB';
        
         if (w >= minwdth && w <= maxwdth) {
                  $(".dbimage").show();
                  $("#img_"+divID).val(null);
                  $(".dvpreview_"+divID).html('');
                 /* $(".error").val("");
                  $(".error").show();
                  $(".error").html("Please choose a proper image" ); */
                 //alert("Please choose a proper image");
                 
                  
                  
                  return;
                 
     
         }else {  
            
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);
            if(provideimgaspect == dbimgaspect) { 
                 /*$(".error").hide();
                 $(".error").html('');
                  */
                 $(".dbimage").hide();
                 $(".dvpreview_"+divID).html('<img src="'+ this.src +'" style="width:50px;height:50px; margin-bottom:5px;" >');
            } else {
                 $(".dbimage").show();
                 $("#img_"+divID).val(null);
                 $(".dvpreview_"+divID).html('');
                 //alert("Image ratios are not same");
               /*  $(".error").show();
                 $(".error").html('Image ratios are not same'); */
                
            }
             
             
         }
        
          //  $('#uploadPreview').append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
        };
        image.onerror= function() {
            //alert('Invalid file type: '+ file.type);
        };      
    };
    
}
   
    
  }
};      
})(jQuery);