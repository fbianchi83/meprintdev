<?php

function meprint_js_alter(&$javascript) {
  
    //dd($javascript);
    
    /*$path = request_uri();
    if(preg_match("/piccolo-formato/", $path, $matches)){
        unset($javascript['sites/all/themes/meprintvchbxfh/scripts/price.js']);
    }*/
}

/**
 * Override of theme_breadcrumb().
 */
function meprint_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the maintenance page template.
 */
function meprint_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // meprint_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  meprint_preprocess_html($vars);
}

/**
 * Override or insert variables into the html template.
 */
function meprint_preprocess_html(&$vars) {
  // Toggle fixed or fluid width.
  if (theme_get_setting('meprint_width') == 'fluid') {
    $vars['classes_array'][] = 'fluid-width';
  }
  // Add conditional CSS for IE6.
  drupal_add_css(path_to_theme() . '/fix-ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lt IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  
  //ADD regions
  
  $vars['header_top'] = block_get_blocks_by_region('header_top');
  $vars['header_top_left'] = block_get_blocks_by_region('header_top_left');
  $vars['header_middle'] = block_get_blocks_by_region('header_middle');
  $vars['frontpage_left'] = block_get_blocks_by_region('frontpage_left');
  $vars['first_preface'] = block_get_blocks_by_region('first_preface');
  $vars['second_preface_header'] = block_get_blocks_by_region('second_preface_header');
  $vars['second_preface_left'] = block_get_blocks_by_region('second_preface_left');
  $vars['second_preface_center'] = block_get_blocks_by_region('second_preface_center');
  $vars['second_preface_right'] = block_get_blocks_by_region('second_preface_right');
  $vars['header_bottom_thirdsection'] = block_get_blocks_by_region('header_bottom_thirdsection');
  $vars['header_bottom_fourthsection'] = block_get_blocks_by_region('header_bottom_fourthsection');
  $vars['header_bottom_fifthsection'] = block_get_blocks_by_region('header_bottom_fifthsection');
  $vars['footer'] = block_get_blocks_by_region('footer');
  
}

/**
 * Override or insert variables into the html template.
 */
function meprint_process_html(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

/**
 * Override login template.
 */
function meprint_theme() {
  $items = array();
  // create custom user-login.tpl.php
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'meprint') . '/templates',
    'template' => 'user-login',
    'preprocess functions' => array(
      'your_themename_preprocess_user_login'
    ),
  );
  return $items;
}




function meprint_preprocess(&$vars, $hook) {

  if(file_exists(DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc'))
      include DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';


  $settings = MP\CostSettingsQuery::create()->findPK(1);
  $vars['VAT_RATE'] = $settings->getVatRate();
  $vars['REDUCED_VAT_RATE'] = $settings->getReducedVatRate();

  $settings = MP\ShippingCostSettingsQuery::create()->filterByStatus(1)->findOne();
  $vars['FILE_REVIEW_COST'] = $settings->getCostCheckUploadedFiles();
}


/**
 * Override or insert variables into the page template.
 */
function meprint_preprocess_page(&$vars, $hook) {

  drupal_add_js('http://fonts.googleapis.com/css?family=Open+Sans:400,700,300', 'external');

  // Move secondary tabs into a separate variable.
  $vars['tabs2'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );
  unset($vars['tabs']['#secondary']);

  if (isset($vars['main_menu'])) {
    $vars['primary_nav'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('links', 'inline', 'main-menu'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  } else {
    $vars['primary_nav'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_nav'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'inline', 'secondary-menu'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  } else {
    $vars['secondary_nav'] = FALSE;
  }

  // Prepare header.
  $site_fields = array();
  if (!empty($vars['site_name'])) {
    $site_fields[] = $vars['site_name'];
  }
  if (!empty($vars['site_slogan'])) {
    $site_fields[] = $vars['site_slogan'];
  }
  $vars['site_title'] = implode(' ', $site_fields);
  if (!empty($site_fields)) {
    $site_fields[0] = '<span>' . $site_fields[0] . '</span>';
  }
  $vars['site_html'] = implode(' ', $site_fields);

  // Set a variable for the site name title and logo alt attributes text.
  $slogan_text = $vars['site_slogan'];
  $site_name_text = $vars['site_name'];
  $vars['site_name_and_slogan'] = $site_name_text . ' ' . $slogan_text;

  if (isset($vars['node'])) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "page--my-machine-name.tpl.php".
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
  if (isset($vars['node']->nid)) {
    if ($vars['node']->nid == '18') {
      $vars['theme_hook_suggestions'][] = 'page__product';
    }
    if ($vars['node']->nid == '28') {
      $vars['theme_hook_suggestions'][] = 'page__product';
    }
    if ($vars['node']->nid == '29') {
      $vars['theme_hook_suggestions'][] = 'page__product';
    }
    if ($vars['node']->nid == '20') {
      $vars['theme_hook_suggestions'][] = 'page__search';
    }

    if ($vars['node']->nid == '41') {
      $vars['theme_hook_suggestions'][] = 'page__cart';
    }
    if ($vars['node']->nid == '46') {
      $vars['theme_hook_suggestions'][] = 'page__orderconfirm';
    }
    if ($vars['node']->nid == '48') {
      $vars['theme_hook_suggestions'][] = 'page__shippingdetails';
    }
    if ($vars['node']->nid == '54') {
      $vars['theme_hook_suggestions'][] = 'page__editcart';
    }
    if ($vars['node']->nid == '55') {
      $vars['theme_hook_suggestions'][] = 'page__carteditsuccess';
    }
    if ($vars['node']->nid == '61') {
      $vars['theme_hook_suggestions'][] = 'page__editwishlist';
    }
    if ($vars['node']->nid == '60') {
      $vars['theme_hook_suggestions'][] = 'page__wishlistsuccess';
    }
    if ($vars['node']->nid == '62') {
      $vars['theme_hook_suggestions'][] = 'page__ordersucess';
    }
  }
  
  
  $status = drupal_get_http_header("status");
  $needBlocks = FALSE;
  if ($status == "404 Not Found" || arg(0) == 'pagina-non-trovata') {
    $vars['theme_hook_suggestions'][] = 'page__404';
    $needBlocks = TRUE;
  }
  elseif ($status == "403 Forbidden") {
    $vars['theme_hook_suggestions'][] = 'page__403';
    $needBlocks = TRUE;
  }
  
  
}

/**
 * Override or insert variables into the node template.
 */
function meprint_preprocess_node(&$vars) {
  $vars['submitted'] = $vars['date'] . ' — ' . $vars['name'];
}

/**
 * Override or insert variables into the comment template.
 */
function meprint_preprocess_comment(&$vars) {
  $vars['submitted'] = $vars['created'] . ' — ' . $vars['author'];
}

/**
 * Override or insert variables into the block template.
 */
function meprint_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'title';
  $vars['classes_array'][] = 'clearfix';
}

/**
 * Override or insert variables into the page template.
 */
function meprint_process_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

function meprint_form_alter(&$form, &$form_state, $form_id) {


  // exit;
  if ($form_id == "productgroup_add_record_form") {
    $form['#validate'][] = "name_validate";
  } /* else if($form_id =="api_add_record_form") {  
    $form['#validate'][] = "shipping_company_validate";
    } else if($form_id == "productgroup_edit_record_form"){
    $form['#validate'][] = "edit_productgroup_name_validate";
    } else if($form_id == "productsubgroup_add_record_form") {
    $form['#validate'][] = "product_sub_group_name_validate";
    } else if($form_id == "productsubgroup-edit-record-form"){
    $form['#validate'][] = "product_sub_group_edit_name_validate";
    } */
  
  if($form_id == "user_pass"){
    /*$form[] = array(
      '#prefix' => "<p>".t("Inserisci l'indirizzo email con il quale ti sei registrato. Riceverai per email un link con il quale potrati accedere a MePrint e cambiare la tua password.")."</p>",
      '#weight' => -15,
    );*/
    $form['info'] = array(
      '#type' => 'fieldset', 
      '#title' => t("Inserisci l'indirizzo email con il quale ti sei registrato. Riceverai per email un link con il quale potrati accedere a MePrint e cambiare la tua password."), 
      '#weight' => -5, 
      '#collapsible' => FALSE, 
      '#collapsed' => FALSE,
    );
    $form['actions']['submit']['#value'] = t('Invia');
  }
  
  if($form_id == "change_pwd_page_form"){
    //dd($form);
    $form['current_pass'] = array(
      '#required' => FALSE,
    );
  }
  
}

function product_sub_group_edit_name_validate($form, &$form_state) {
  /* $editvalues = $form_state['input'];
    echo "<pre>";
    print_r($editvalues);
    exit; */
}

function product_sub_group_name_validate($form, &$form_state) {
  $editvalues = $form_state['input'];

  $frc = MP\ProductsubgroupLangQuery::create()->filterByProductgroupId($editvalues['productgroup_id_fr'])
    ->filterByLanguageId('fr')
    ->filterByName($editvalues['name_fr'])
    ->count();

  if ($frc != 0) {
    form_set_error('name_fr', t($form_state['values']['name_fr'] . 'Name Already Existed In French '));
  }

  $itc = MP\ProductsubgroupLangQuery::create()->filterByProductgroupId($editvalues['productgroup_id_it'])
    ->filterByLanguageId('it')
    ->filterByName($editvalues['name_it'])
    ->count();
  if ($itc != 0) {
    form_set_error('name_it', t($form_state['values']['name_it'] . 'Name Already Existed In Italy '));
  }
}

function edit_productgroup_name_validate($form, &$form_state) {

  $editvalues = $form_state['input'];
  if ($editvalues['db_fr'] != $editvalues['name_fr']) {
    $frCount = MP\ProductsgroupLangQuery::create()->filterByLanguageId('fr')
      ->filterByName($editvalues['name_fr'])
      ->count();
    if ($frCount != 0) {
      form_set_error('name_fr', t($form_state['values']['name_fr'] . 'Name Already Existed French '));
    }
  }
  if ($editvalues['db_it'] != $editvalues['name_it']) {
    $frCount = MP\ProductsgroupLangQuery::create()->filterByLanguageId('it')
      ->filterByName($editvalues['name_it'])
      ->count();
    if ($frCount != 0) {
      form_set_error('name_it', t($form_state['values']['name_it'] . 'Name Already Existed French '));
    }
  }
}

function name_validate($form, &$form_state) {

  $frCount = MP\ProductsgroupLangQuery::create()->filterByLanguageId('fr')
    ->filterByName($form_state['values']['name_fr'])
    ->count();
  if ($frCount != 0) {
    form_set_error('name_fr', t($form_state['values']['name_fr'] . 'Name Already Existed French '));
  }
  $itCount = MP\ProductsgroupLangQuery::create()->filterByLanguageId('it')
    ->filterByName($form_state['values']['name_it'])
    ->count();
  if ($itCount != 0) {
    form_set_error('name_it', t($form_state['values']['name_it'] . ' Name Already Existed Italay'));
  }
}

function shipping_company_validate($form, &$form_state) {
  $nameCount = MP\ShippingCompaniesQuery::create()
    ->filterByName($form_state['values']['shipping_comapny'])
    ->count();
  if ($nameCount != 0) {
    form_set_error('shipping_comapny', t($form_state['values']['shipping_comapny'] . ' Name Already Existed '));
  }
}

/**
 * Override or insert variables into the region template.
 */
function meprint_preprocess_region(&$vars) {
  if ($vars['region'] == 'header') {
    $vars['classes_array'][] = 'clearfix';
  }
}
                                                                                                                               /*                                                                                                                                             * ********************** Three Column Big Format ************ */

//Samll Format Start 

/*                                                                                                                                             * ********************** Three Column Small Format ************ */
function productthreecolumSmall($id, $type) {

  $sProduct = $id;
  global $base_url;
  global $language;
  $lang_name = $language->language;
  include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';


  $result = MP\FormtwoLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->find();
  $smallresult = MP\FormtwoSmallFormatQuery::create()->filterByFormId($id)->find();
  $product_material = MP\FormtwoMaterialsQuery::create()->filterByFormId($id)->find();
  $formtwo_dimensions = MP\FormtwoDimensionsQuery::create()->filterByFormId($id)->find();
  $result_info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();
  $smallextra_processing = MP\FormtwoExtraProcessingQuery::create()->filterByFormId($id)->find();

  $smallpriceget = MP\FormtwoBasePriceQuery::create()->filterByFormId($id)->find();


  $mystr = "";
  foreach ($result as $precord) {
    //$sproductgroup_id = $precords->getProductgroupId();
    foreach ($result_info as $info) {
      $imguri = $info->getImage();
      $uuu = file_load($imguri)->uri;
      $my_image = explode("://", $uuu);
    }

    $mystr .= '<input type ="hidden" value ="0" id = "productgroupid">';
    ?>

<h3>&nbsp; <?php echo $precord->getName(); ?> &nbsp; <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="ProductImage"></span></h3>
<div class="space-3"></div>
<!--Image-->
<img src="<?php echo $base_url . '/sites/default/files/' . $my_image[1]; ?>" width="550px" height="235px" alt="Product-Image">
<div class="col-sm-12 col-md-12 ">
<div>
<h4 class="order-h"><?php echo $productdesc; ?> </h4>
</div>
<?php
echo $precord->getDescription();
'<br>';
?>
</div>
<div class="space-3"></div>
<form method="POST" id="product-small-format" enctype="multipart/form-data" action="<?php echo $base_url . '/' . $lang_name; ?>/cart" autocomplete="off">
<div class="col-sm-12 col-md-12 ">
<div class="row">
<div class="order-view-hd"><?php echo $ordertitle; ?></div>
<div class="order-view-top">

<div class="form-group col-md-5 col-sm-5">
<h4 class="order-h"><?php echo $quanitity; ?></h4>
<input type="text" class="form-control" id="sproductQuanity" name="squanitity" placeholder="1" min="1" value="1">
</div>
<!-- Template download for cart -->
<div class="form-group col-md-7 col-sm-7">
    <?php
    foreach ($result_info as $template) {
      $tId = $template->getTemplateId();
      $TemplateImage = MP\TemplatesQuery::create()->filterByTemplateId($tId)->find();
      if (count($TemplateImage) > 0) {
        foreach ($TemplateImage as $value) {
          $image1 = $value->getFile();
          $yyyyyy = file_load($image1)->uri;

          $templateImage = explode("://", $yyyyyy);
          ?>

      <h4 class="order-h"><?php echo $download; ?></h4>
      <div class="space-3"></div>


      <span class="downloadable " src="<?php echo $base_url . '/sites/default/files/' . $templateImage[1]; ?>"><?php echo $bdownload; ?></span>
      <!--<img alt="" src="<?php echo $base_url . '/sites/default/files/' . $templateImage[1]; ?>" width="50px" height="50" class="downloadable">-->

          <?php
        }
      }
      ?>

      <?php
      $spromoprice = $template->getPromotionPrice();
      $spromotype = $template->getPromoPriceType();
      $mystr .= '<input type ="hidden" value ="' . $spromotype . '" id = "spromotype">';
      $mystr .= '<input type ="hidden" value ="' . $spromoprice . '" id = "spromoprice">';
    }
    ?>

</div>
<!--end  Template download for cart -->

</div>

<div class="order-view-top">
<div class="space-3 col-md-12"></div>
<div class="form-group ">
    <div class="col-md-12 col-sm-12">
        <h4 class="order-h"><?php echo $dimension; ?></h4>
    </div>
    <?php
    $first = TRUE;
    foreach ($formtwo_dimensions as $formonedimensions) {

      $dimensionsName = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->find();

      foreach ($dimensionsName as $dname) {
        $image = $dname->getFile();
        $uuu = file_load($image)->uri;
        $my_image = explode("://", $uuu);
        ?>
          <div class="form-group col-md-3 col-sm-3">
              <div class="col-md-12 col-sm-12">
          <?php if ($my_image[1] != '') { ?>
                    <img class="dimdiv" src="<?php echo $base_url . '/sites/default/files/' . $my_image[1]; ?>" width="55" height="55"/>
        <?php
        } else {
          echo '<i class="fa fa-camera" ></i>';
        }
        ?>
              </div>
              <div class="col-md-12 col-sm-12">
                  <input type="radio" name="sdimensions" id="sdimensions" value="<?php echo $dname->getDimensionId() ?>" <?php if ($first): ?>checked="checked" <?php endif; ?> >&nbsp<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>
              </div>
        <?php echo $js = '<input type ="hidden" value ="' . $dname->getWidth() . '*' . $dname->getHeight() . '" id = "bd' . $dname->getDimensionId() . '">'; ?>
        <?php
        $first = FALSE;
        ?>
                </div>
              <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="order-view-top">

        <div class="form-group col-md-5 col-sm-5">
            <h4 class="order-h"><?php echo $tmaterial; ?> </h4>
            <select class="form-control" id="smaterialId" name="smaterialId">
    <?php
    foreach ($product_material as $material) {

      $smallmaterial = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->find();
      foreach ($smallmaterial as $smaterial) {
        $first = TRUE;
        if ($first) {
          $matid = $smaterial->getMaterialId();
        }
        $first = FALSE;
        ?>

        <option value="<?php echo $smaterial->getMaterialId(); ?>"><?php echo $smaterial->getName() . '<br>'; ?></option>
        <?php
        $materials_price_values = MP\MaterialsQuery::create()->filterByMaterialId($smaterial->getMaterialId())->find();
        foreach ($materials_price_values as $mat_price) {
          $mystr .= '<input type ="hidden" value ="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+' . $mat_price->getShortSideMinLength() . '+' . $mat_price->getLongSideMinLength() . '+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '" id = "smtid' . $material->getMaterialId() . '">';
        }
        ?>
        <?php //$mystr.='<input type ="hidden" value ="'.$material->getPrice().'+'.$material->getMinSize().'+'.$material->getShortSideMinLength().'+'.$material->getLongSideMinLength().'+'.$material->getMinimumProductionTime().'+'.$material->getWeight().'+'.$material->getThickness().'" id = "smtid'.$material->getMaterialId().'">'; 
        ?>
        <?php
      }
    }
    $mystr .= '<input type ="hidden" value ="' . $id . '" id = "formid">';
    ?>
      </select>
  </div>

</div>

<div class="order-view-top">

  <div class="form-group col-md-12 col-sm-12">
      <h4 class="order-h"><?php echo $processing; ?> </h4>
          <?php
          foreach ($smallresult as $smallprocessing) {
            $baseprice = $smallprocessing->getBasePriceForQuantity();

            $mystr .= '<input type ="hidden" value ="' . $baseprice . '" id = "basePricesmall">';
            $baseprocessing_id = $smallprocessing->getProcessingId();
            $product_processing = MP\ProcessingLangQuery::create()->filterByProcessingId($smallprocessing->getProcessingId())->filterByLanguageId($lang_name)->find();
            foreach ($product_processing as $base_processing) {
              $first = TRUE;
              if ($first) {
                $prcssid = $base_processing->getProcessingId();
              }
              ?>
          <div class="col-md-5 col-sm-5">
              <input type="radio" name="processing" id="sprocessing" checked="checked" value="<?php echo $base_processing->getProcessingId(); ?>">&nbsp<?php
      echo $base_processing->getName();
    }
    $fist = FALSE;
    ?><?php } ?></div>
    <?php
    foreach ($smallextra_processing as $seprocessing) {
      $processingName = MP\processingLangQuery::create()->filterByProcessingId($seprocessing->getProcessingId())->filterByLanguageId($lang_name)->find();
      foreach ($processingName as $ename) {
        ?>
              <div class="col-md-5 col-sm-5">
                  <input type="radio" name="processing" id="sprocessing" value="<?php echo $ename->getProcessingId(); ?> ">&nbsp<?php echo $ename->getName() . '<br>'; ?> </div>
            <?php } ?>
          <?php } ?>
      </div>

  </div>
  <!-- file upload  for cart -->
  <div class="order-view-top">
      <div class="form-group col-md-12 col-sm-12">
          <!--<div class="dvPreview"></div>-->
          <h4 class="order-h"><?php echo $uploadimagetitle; ?></h4>
          <?php foreach ($smallresult as $number) { ?>
            <input type="hidden" name="minQuantitySmall" value="<?php echo $number->getMinCopies(); ?>"/>
      <?php
      $files = $number->getNumberOfFilesTobeUploaded();
      if ($files != 0) {
        for ($i = 1; $i <= $files; $i++) {
          ?>
            <div class="uploadproductimage">
                <input type="hidden" name="simagecount" value="<?php echo $number->getNumberOfFilesTobeUploaded(); ?>"/>

                <div class="dvPreviewSmall_<?php echo $i; ?>"></div>

                <input type="file" name="image_<?php echo $i; ?>" id="image_<?php echo $i; ?>" class="ImageUploadsmall"/>

                <div class="ImageError_<?php echo $i; ?> imageerror"></div>
                <div class="errorImageUpload_<?php echo $i; ?>"></div>

            </div>

          <?php
        }
        $or = $number->getUseOfExternalDesignerProgram();
        if ($or == 1) {
          ?>
          <div class='clearfix'></div>
          <div class="order-view-top">
              <div class="form-group col-md-12 col-sm-12">
                  <h4 class="order-h"><?php echo $operatorreviewtitle; ?></h4>

                  <div style="margin-bottom:10px;">
                      <input type="checkbox" name="soperatorreview" id="operatorreview" value="1"/>&nbsp; <?php echo $operatorreview . ' &euro;' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?>
                  </div>
              </div>
          </div>

          <?php
        }
      }
    }
    ?>
        </div>
    </div>

    <!-- End file upload  for cart -->
    <!-- Shipping Company Details --->
    <div class="order-view-top">
        <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingtype; ?></h4>

        <div class="col-md-12 col-sm-12">

    <?php
    $shippingName = MP\ShippingCompaniesQuery::create()->filterByStatus(1)->find();
    $ss = 1;
    foreach ($shippingName as $shipname) {
      $first = TRUE;
      ?>

      <div class="shipzone"> <span>
              <li class="fa  fa-truck fa-4x"></li>
          </span>

          <p class="dym-p"><input type="radio" name="shipping" id="shipping" <?php if ($ss == 1): ?>checked="checked"<?php endif; ?> value="<?php echo $shipname->getShippingId(); ?>">
      <?php echo $shipname->getName(); ?><!--<span class="shipzone-span">Thursday &euro;16</span>--></p>
          <!--&euro;12 -->
      </div>
      <?php $ss++;
    }
    ?>

          </div>
          <div class="space-3 col-md-12 col-sm-12"></div>
          <div class="col-md-11 col-sm-11 text-left">
              <p> <?php echo $shipdesc; ?> </p>
          </div>
      </div>
      <!-- Shipping Company Details --->
      <div class="order-view-top">
          <div class="form-group col-md-12 col-sm-12">
              <div class="form-group col-md-6 col-sm-6">
                  <input type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="lngtmprdctn" checked="checked" value="">&nbsp<?php echo "Estimated Shipping date(Regular)"; ?></div>
              <h4 class="order-h" id="shippingdateprintdiv0"></h4>
              <!--<div id ="shippingdateprintdiv"></div>-->
              <!--</div> -->
          </div>
      </div>
      <!-- Anonymous Shipping Start  -->
      <div class="order-view-top">
          <div class="form-group col-md-12 col-sm-12">
              <h4 class="order-h"><?php echo "Anonymous Shipping"; ?></h4>
              <input type="checkbox" name="anonshipping" id="annonshipping" value="1">&nbsp<?php echo "Anonymous Shipping"; ?> </div>
      </div>

      <!-- Anonymous Shipping Start End  -->
      <input type="hidden" id="shippingprice" name="shippingprice" value="">

  </div>
</div>
    <?php
    $smffstlst = MP\FormtwoBasePriceQuery::create()->filterByFormId($id)
      ->useFormtwoBasePriceMaterialsQuery('a')
      ->filterByMaterialId($matid)
      ->orderByMaterialId()
      ->endUse()
      ->useFormtwoBasePriceProcessingQuery('b')
      ->filterByProcessingId($prcssid)
      ->orderByProcessingId()
      ->endUse()
      ->select(array('PriceId', 'FormId', 'DimensionId', 'NoCopies', 'a.MaterialId', 'a.Price', 'b.ProcessingId', 'b.Price'))
      ->orderByNoCopies()
      ->limit(1)
      ->find();

    foreach ($smffstlst as $lstsmf) {
      $smatprice = $lstsmf['a.Price'];
      $procesprice = $lstsmf['b.Price'];
    }
    $sproductqty = 1;
    $smatprice = round($smatprice, 2);
    $sprocesprice = round($procesprice, 2);
    $ssingleproductcost = $smatprice + $procesprice;
    $ssingleproductcost = round($ssingleproductcost, 2);

    // echo $matprice."<br/>".$procesprice."vishal";?>

    <div class="clearfix"></div>
    <div class="space-2 col-sm-12 col-md-12"></div>
    <div class="col-sm-12 col-md-12">
        <div class="order-view-hd"><?php echo $summery; ?></div>
        <div class="panel panel-default">
            <div class="table-responsive table-summary">
                <table id="sample-table-1" class="table table-hover">
                    <thead>
                        <tr>
                            <th class="left"><?php echo $product; ?></th>
                            <th><?php echo $qty; ?></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $price; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="left"><strong><?php echo "Cost of Printing and Materials" ?></strong></td>
                            <td>1</td>
                            <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                            <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                            <td class="hidden-480">
                                <div class="fa fa-eur" id="sprintigmaterialdiv"><?php //echo $smatprice; ?> </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="left"><strong><?php echo $processcost; ?></strong></td>
                            <td>1</td>
                            <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                            <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                            <td class="hidden-480">
                                <div class="fa fa-eur" id="sadtnlcostdiv"><?php //echo $sprocesprice; ?> </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="left"><strong><?php echo $singleproductcost; ?></strong></td>
                            <td>1</td>
                            <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                            <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
    <?php //$tot_prc = $matrl_fst_prc+$addtnl_prc_cpy;?>
          <td class="hidden-480">
              <div class="fa fa-eur" id="ssingleproductcostdiv"> <?php //echo $ssingleproductcost; ?></div>
          </td>

      </tr>
      <tr id="spromotd">
          <td class="left"><strong><?php echo $beforpromoprice; ?></strong></td>
          <td>
              <div class="stotalproductcostdivqnty"><?php //echo $sproductqty; ?></div>
          </td>
          <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
          <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
          <td class="hidden-480">
              <div style='text-decoration:line-through' class="fa fa-eur" id="spromodiv"><?php //echo $ssingleproductcost; ?></div>
          </td>

      </tr>
      <tr>
          <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
          <td>
              <div class="stotalproductcostdivqnty"><?php //echo $sproductqty; ?></div>
      </td>
      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
      <td class="hidden-480">
          <div class="fa fa-eur" id="stotalproductcostdiv"><?php //echo $ssingleproductcost; ?></div>
              </td>

          </tr>
      </tbody>
  </table>
</div>
    <?php
    $workload_dets = MP\ProductsGroupQuery::create()->filterByProductgroupId(0)->find();
    foreach ($workload_dets as $workloadinfo) {
      $workloadtype = $workloadinfo->getWorkloadType();
      $workloadlimit = $workloadinfo->getWorkloadLimit();
    }
    ?>

                  <input type="hidden" id="smalltotlprice" name="smalltotlprice" value="">
                  <input type="hidden" name="sproductgroupid" value="0">
                  <input type="hidden" name="productgroupid" id="productgroupid" value="0">
                  <input type="hidden" id="formid" value="<?php echo $id; ?>">
                  <input type="hidden" id="formtype" value="small">
                  <input type="hidden" id="languageid" value="<?php echo $lang_name; ?>">
                  <input type="hidden" id="lngrmprdtnid" value="0">
                  <input type="hidden" value="'.$workloadtype.'" id="workloadtype">
                  <input type="hidden" value="'.$workloadlimit.'" id="workloadlimit">
                                          <?php
                                          $price_path = $base_url . "/sites/all/themes/meprint/get-price.php";
                                          $dates_path = $base_url . "/sites/all/themes/meprint/get-dates.php";
                                          $shipping_path = $base_url . "/sites/all/themes/meprint/set-shipping.php";
                                          ?>
                  <input type="hidden" name="price_path" value="<?php echo $price_path; ?>" id="price_path">
                  <input type="hidden" value="<?php echo $dates_path; ?>" name="dates_path" id="dates_path">
                  <input type="hidden" value="<?php echo $shipping_path; ?>" name="shipping_path" id="shipping_path">

                  <input type="hidden" name="getId" value="<?php echo $id ?>">
                  <input type="hidden" name="type" value="small">
              </div>
              <div class="col-md-12 col-sm-12">
                  <div class="col-md-4 col-sm-4">
                      <button class="btn btn-orange" type="submit" name="wishlist" value="wishlist">Add to Wishlist</button>

                  </div>
                  <div class="col-md-5 col-sm-5"></div>
                  <div class="col-md-3 col-sm-3">
                      <button class="btn btn-success" name="sbutton" type="submit" value="sbutton">Proceed</button>
                  </div>
              </div>

          </div>
      </form>

      <div class="clearfix"></div>
      <div class="col-md-12 col-sm-12"></div>


      <section class="row">
          <div class="col-md-12 col-sm-12">
    <?php
    foreach ($smallresult as $sresult) {
      $ssubgroup = $sresult->getSubcategoryId();
    }
    $srelresult = MP\FormtwoSmallFormatQuery::create()
        ->where('FormtwoSmallFormat.SubcategoryId =?', $sresult->getSubcategoryId())
        ->where('FormtwoSmallFormat.FormId !=?', $sresult->getFormId())
        ->orderByFormId('desc')
        ->limit(3)->find();
    //->filterBySubcategoryId($precord->getSubcategoryId())->orderByFormId('desc')->limit(3)->find();

    foreach ($srelresult as $srelres) {
      $srelated_product = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($srelres->getFormid())
          ->filterByWebsiteId($lang_name)->find();
      $srel_language = MP\FormtwoLangQuery::create()
        ->where('FormtwoLang.FormId =?', $srelres->getFormid())
        ->where('FormtwoLang.LanguageId =?', $lang_name)
        ->find();
      foreach ($srel_language as $srellanguage) {
        $name = substr($srellanguage->getName(), 0, 20);
        $description = substr($srellanguage->getdescription(), 0, 125);
      }
      foreach ($srelated_product as $srelproduct) {
        $id = $srelproduct->getFormId();
        //$name = $res->getName();
        $image = $srelproduct->getImage();
        $uuu = file_load($image)->uri;
        $my_image = explode("://", $uuu);
        //$bestprice = $relproduct->getBestPriceFlag();
        $promo = $srelproduct->getPromoFlag();
        $baseprice = $srelres->getBasePriceForQuantity();
        $promoprice = $srelproduct->getPromotionPrice();
        //$promotype = $relproduct->getPromoPriceType();
        //$baseprice = $srelres->getBasePriceForQuantity();
        //$pdate = $relproduct->getCreatedDate();
      }
      ?>

<div class="col-sm-4 col-md-4 ">
    <a href="<?php echo $base_url . '/product-three-column?' . $id . '&type=small'; ?>">
        <div class=" products-Thumbnail">
            <div class="item item-type-line"><span class="item-hover"> 
                    <div class="item-info">
                        <div class="date"><?php echo $description; ?></div>
                        <div class="line"></div>
                        <div class="date"><?php //echo $pdate; ?></div>
                        </div>
                        <div class="mask"></div>
                    </span>

                    <div class="item-img"><img src="<?php echo $base_url . '/sites/default/files/' . $my_image[1]; ?>" width="125px" height="125px"/></div>
                </div>
    <?php if ($promo == 'Promo') { ?>
                  <div class="products-title"><em><?php echo $promo; ?></em></div>
    <?php } ?>
    <?php if ($promo == 'Best Price') { ?>
                  <div class="products-title1"><em><?php echo $bestprice; ?></em></div>
    <?php } ?>
                <div class="clr"></div>
                <p><?php echo $name; ?> </p>
      <?php
      if ($promo == 'Promo') {

        $actualprice = $baseprice;
        $baseprice1 = $baseprice - $promoprice;
        ?>
                          <p class="p">&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/ &euro; <?php echo $baseprice1; ?></p>
       <?php } else { ?>
                          <p class="p">&euro; <?php echo $baseprice; ?></p><?php } ?>

                    </div>
                </a>
            </div>

                        <?php
                      }
                      ?>
      </div>
  </section>

      <?php
      //}
    }
    return $mystr;
  }

function cartBigIteams() {

  include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
  global $base_path;
  global $base_url;
  global $language;
  /*$cartpagetittle = t('Cart Details');
  $cartproduct = t('Product');
  $file = t('File');
  $quantity = t('Quantity');
  $price = t('Price');
  $nocopies = t('No of Copies :');
  $weight = t('Weight');
  $cartdate = t('Cart Date :');
  $totprice = t('Total Price :');
  $tmaterial = t('Material:');
  $dimension = t('Dimensions:');
  $conshoping = t('Continue Shopping');
  $processing = t('Processing:');
  $accessorie = t('Accessories:');
  $proceed = t('Proceed');
  $noimage = t('No Image');
  $noproductsavail = t('No products available in your cart');
  $shippinginfo = t('SHIPPING INFO');
  $orderconfirm = t('ORDER CONFIRM');
  $producttotalprice = t('Product Total Price:');
  $discountprice = t('Discount Price:');
  $finalprice = t('Total Price:');
  $coupanlabel = t('I have an offer code or cash coupon');
  $scoupon = t('Coupon code updated successfully');
  $fcoupon = t('Invalid Coupon Code');
  $pshipping = t('Partial Shipping');
  $cshipping = t('Complete Shipping');*/
  $lang_name = $language->language;

  session_start();
  $_SESSION['session_id'] = session_id();

  if (isset($_POST['proceed']) && $_POST['proceed'] != '') {

    if (count($_POST) != 0) {

      $purl = explode("?", $_SERVER['HTTP_REFERER']);
      $url = explode("?", $_SERVER["REQUEST_URI"]);
      $purl1 = explode("&type=", $purl['1']);
      $id = $purl1[0];
      $type = $purl1[1];
      $count = $_POST['imagecount'];
      for ($i = 1; $i <= $count; $i++) {

        $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/' . $_FILES['image_' . $i]["name"];
        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      }
      $cart = new MP\CartItems();
      $cart->setSessionId(session_id());
      $cart->setProductType("Big");
      $cart->setProductId(isset($_POST['productid']) ? $_POST['productid'] : '');
      $cart->setProductgroupId(isset($_POST['productgroupid']) ? $_POST['productgroupid'] : '');
      $cart->setAccessoryId(isset($_POST['baseaccessories']) ? $_POST['baseaccessories'] : '');
      if (isset($_POST['dimensions']) && $_POST['dimensions'] == "custom") {
        $cart->setWidth(isset($_POST['productWidth']) ? $_POST['productWidth'] : '');
        $cart->setHeight(isset($_POST['productHeight']) ? $_POST['productHeight'] : '');
      } else if (isset($_POST['dimensions']) && $_POST['dimensions'] != "custom") {
        $cart->setDimensionId(isset($_POST['dimensions']) ? $_POST['dimensions'] : '');
      }
      $cart->setMaterialId(isset($_POST['materialId']) ? $_POST['materialId'] : '');
      $cart->setProcessingId(isset($_POST['baseprocessing']) ? $_POST['baseprocessing'] : '');

      $cart->setShipping(isset($_POST['shipping']) ? $_POST['shipping'] : '');
      if ($_POST['quanitity'] == "") {
        $cart->setQuanitity(1);
      } else {
        $cart->setQuanitity(isset($_POST['quanitity']) ? $_POST['quanitity'] : '');
      }
      $cart->setLongtermId(isset($_POST['lngtmprdctn']) ? $_POST['lngtmprdctn'] : '');
      $cart->setAnonymousPack(isset($_POST['anonshipping']) ? $_POST['anonshipping'] : '');
      $cart->setExtraDefValue(isset($_POST['extradefValue']) ? $_POST['extradefValue'] : '');
      $cart->setExtraProcessingTop(isset($_POST['baseisExtraTop']) ? $_POST['baseisExtraTop'] : '');
      $cart->setExtraProcessingRight(isset($_POST['baseisExtraRight']) ? $_POST['baseisExtraRight'] : '');
      $cart->setExtraProcessingLeft(isset($_POST['baseisExtraLeft']) ? $_POST['baseisExtraLeft'] : '');
      $cart->setExtraProcessingBottom(isset($_POST['baseisExtraBottom']) ? $_POST['baseisExtraBottom'] : '');
      $cart->setPrice(isset($_POST['bigtotlprice']) ? $_POST['bigtotlprice'] : '');
      $cart->setOperatorReview(isset($_POST['operatorreview']) ? $_POST['operatorreview'] : '');
      $cart->setCreatedDate(time());
      $cart->setModifiedDate(time());
      $cart->save();
      $idsss = $cart->getCartId();

      /*                                                                                                                                               * ****** cart extra processing start************ */
      if (isset($_POST['extraprocessing']) && $_POST['extraprocessing'] != '') {

        $cartExtra = new MP\CartExtraProcessing();
        $cartExtra->setCartId($idsss);
        $cartExtra->setProcessingId($_POST['extraprocessing']);
        $cartExtra->setExtraDefValue($_POST['epextadefValue_' . $_POST['extraprocessing']]);
        $cartExtra->setCreatedDate(time());
        $cartExtra->setModifiedDate(time());
        $cartExtra->setExtraProcessingTop(isset($_POST['isextratop_' . $_POST['extraprocessing']]) ? $_POST['isextratop_' . $_POST['extraprocessing']] : '');
        $cartExtra->setExtraProcessingRight(isset($_POST['isextraright_' . $_POST['extraprocessing']]) ? $_POST['isextraright_' . $_POST['extraprocessing']] : '');
        $cartExtra->setExtraProcessingLeft(isset($_POST['isextraleft_' . $_POST['extraprocessing']]) ? $_POST['isextraleft_' . $_POST['extraprocessing']] : '');
        $cartExtra->setExtraProcessingBottom(isset($_POST['isextrabottom_' . $_POST['extraprocessing']]) ? $_POST['isextrabottom_' . $_POST['extraprocessing']] : '');
        $cartExtra->save();
      }

      /*                                                                                                                                               * ****** cart extra processing end************ */

      /*                                                                                                                                               * ****** cart extra Accessories start************ */
      if (isset($_POST['extraaccessories']) && $_POST['extraaccessories'] != '') {

        $cartExtra = new MP\CartWithoutAccessories();
        $cartExtra->setCartId($idsss);
        $cartExtra->setAccessoriesId($_POST['extraaccessories']);
        $cartExtra->setCreatedDate(time());
        $cartExtra->setModifiedDate(time());
        $cartExtra->save();
      }


      /*                                                                                                                                               * ****** cart extra Accessories end************ */

      /*                                                                                                                                               * ****** cart images start************ */
      for ($i = 1; $i <= $count; $i++) {
        if ($_FILES['image_' . $i]["name"] != '') {
          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($_FILES['image_' . $i]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setCreatedDate(time());
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        }
      }

      unset($_POST);
    }
  }
  $sessionid = $_SESSION['session_id'];
  global $language;
  $lang_name = $language->language;
  $count = MP\CartItemsQuery::create()
    ->where('CartItems.SessionId =?', $sessionid)
    ->count();

  if ($count > 0) {
    $cartQuery = MP\CartItemsQuery::create()
      ->where('CartItems.SessionId =?', $sessionid)
      ->orderByCartId('desc');
    ?>
<form method="POST" action="<?php echo $base_url . '/' . $lang_name . '/shippingdetails'; ?>">
<div class="col-md-12 col-sm-12 product_name">
<div class="col-md-5 col-sm-5"><b><?php echo $cartproduct; ?></b></div>
<div class="col-md-3 col-sm-3"><b><?php echo $file; ?></b></div>
<div class="col-md-2 col-sm-2"><b><?php echo $quantity; ?></b></div>
<div class="col-md-2 col-sm-2"><b><?php echo $price; ?></b></div>
</div>
<?php
$i = 1;
foreach ($cartQuery as $result) {
if ($result->getProductType() == 'Big') {
$pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();

$returnquery = MP\FormoneBigFormatQuery::create()->filterByFormId($result->getProductId())->find();
foreach ($pquery as $presult) {
$mml = $presult->getName();
}
foreach ($returnquery as $rquery) {

$url = $base_url . '/productgridlist?' . $rquery->getSubcategoryId();
}
} else if ($result->getProductType() == 'small') {
$pquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
$returnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($result->getProductId())->find();
foreach ($pquery as $presult) {
$mml = $presult->getName();
}
foreach ($returnquery as $rquery) {
$url = $base_url . '/smallformat?' . $rquery->getSubcategoryId();
}
}
?>
<div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $result->getCartId() ?>">

<div class="col-md-5 col-sm-5 ">
<div class="col-md-12 col-sm-12 cartdetails_4">
  <b><?php echo $mml; ?></b></div>

<div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
<?php echo $nocopies; ?> 
  </span><span class="datacart">
<?php echo $result->getQuanitity() ?>
  </span></div>
<div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
<?php echo $dimension; ?> 
  </span><span class="datacart">
<?php
// echo $result->getDimensionId();
if ($result->getDimensionId() != 0) {
$dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
foreach ($dquery as $mquery) {
echo $mquery->getWidth() . '*' . $mquery->getHeight();
}
} else {
echo $result->getWidth() . "*" . $result->getHeight();
}
?>

  </span></div>
<div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
<?php echo $tmaterial; ?>
  </span><span class="datacart">
<?php
$cquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
foreach ($cquery as $mquery) {
echo $mquery->getName();
}
?>
  </span></div>

<div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
      <?php echo $cartdate; ?>
        </span><span class="datacart">
            <?php
            $cDATE = $result->getCreatedDate();
            echo $cDATE->format('Y-m-d');
            ?>
        </span></div>
</div>

<div class="col-md-3 col-sm-3 ">
            <?php
            $IMAGES = MP\CartImagesQuery::create()
              ->withColumn('Max(Id)')
              ->where('CartImages.CartId =?', $result->getCartId())
              ->select(array('image'))
              ->findOne();

            // echo $IMAGES['image'];
            if ($result->getProductType() == 'small') {
              $imageurls = $base_url . "/sites/default/files/cart/smallformat/" . $IMAGES['image'];
            } else if ($result->getProductType() == 'Big') {
              $imageurl = $base_url . "/sites/default/files/cart/" . $IMAGES['image'];
            }
            ?>
            <?php if ($IMAGES['image'] != '') { ?>
      <img src="<?php echo $imageurl; ?>" width="100" heigth="100"/>
            <?php
            } else {
              echo '<i class="fa fa-camera"></i>';
            }
            ?>
</div>

<div class="col-md-2 col-sm-2 cartdetailsline">
    <div class="col-md-6 col-sm-6 cartdetails">
            <?php echo $result->getQuanitity() ?>
    </div>
    <div class="col-md-3 col-sm-3 cartdetails ">
            <?php $editurl = $base_url . '/' . $lang_name . '/editcart?' . $result->getCartId(); ?>
        <div class="col-md-12 col-sm-12 edit_icon cartdetails">
            <a href="<?php echo $editurl; ?>">
                <i class="fa fa-pencil pencil"></i></a>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 cartdetails ">
        <div class="col-md-12 col-sm-12  delete_icon cartdetails">
            <input type="hidden" name="path" value="<?php echo $base_url; ?>/sites/all/themes/meprint/cart.php">
            <a class="deletecart" url="<?php echo $result->getCartId(); ?>">
                <i class="fa fa-trash-o"></i></a>
        </div>
    </div>
</div>
<div class="col-md-2 col-sm-2 cartdetailsline">
    <div class="col-md-10 col-sm-10 cartdetails"><b>
    <?php
    $totlprc = $result->getPrice();
    echo '&euro;' . number_format((float) $totlprc, 2, '.', '');
    ?>
        </b></div>
</div>
</div>
<div class="clearfix"></div>

    <?php $i++;
  }
  ?>

<div class="space-2"></div>
<!-- Coupon code Start **************-->
    <?php
    $totalPrice = MP\CartItemsQuery::create()
      ->withColumn('SUM(Price)')
      ->where('CartItems.SessionId =?', $_SESSION['session_id'])
      ->groupBySessionId()
      ->select(array('price'))
      ->findOne(); //$totalPrice['SUMPrice']
    ?>

    <div class="col-md-12 col-sm-12 discountdiv">
        <div class="col-md-7 col-sm-7">
            <input type="checkbox" name="discountcheck" id="discountcheck"> <span class="coupanlabel"><?php echo $coupanlabel; ?></span>

            <div class="discountyes">Coupon Code : <input type="text" name="coupancode" value="">
                <input type="hidden" name="session_id" value="<?php echo $_SESSION['session_id']; ?>"/>
                <input type="hidden" name="totalprice" value="<?php echo $totalPrice['SUMPrice']; ?>"/>
                <input type="hidden" name="productprice" value=""/>
                <input type="hidden" name="discount_id" value=""/>
                <input type="hidden" name="couponpath" readonly="readonly" value="<?php echo $base_url; ?>/sites/all/themes/meprint/discount.php"/>
                <button type="button" name="discountbutton" id="discountbutton" value="Submit">Update</button>
                <div class="valid" style="display:none;"><span><?php echo $scoupon; ?> </span></div>
                <div class="invalid" style="display:none;"><span><?php echo $fcoupon; ?> </span></div>
            </div>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="col-md-7 col-sm-7">
                <span class="discountlabel"><?php echo $producttotalprice; ?></span>
            </div>
            <div class="col-md-5 col-sm-5 discount">
                &euro; <input size="7" type="text" name="finalprice" value="<?php echo round($totalPrice['SUMPrice'], 2) ?>" readonly="readonly" class="hiddp"/>
            </div>
            <div class="dishide">
                <div class="col-md-7 col-sm-7">
                    <span class="discountlabel"><?php echo $discountprice; ?></span>
                </div>
                <div class="col-md-5 col-sm-5 discount">
                    &euro; <input size="7" type="text" name="dicountamount" readonly="readonly" value="0"/>
                </div>

                <div class="col-md-7 col-sm-7">
                    <span class="discountlabel"><?php echo $finalprice; ?></span>
                </div>
                <div class="col-md-5 col-sm-5 discount">
                    &euro; <input size="7" type="text" name="finalamount" readonly="readonly" value="<?php echo round($totalPrice['SUMPrice'], 2) ?>"/>
                </div>
            </div>
        </div>
    </div>

    <div class="space-2"></div>

    <div class="clearfix"></div>
    <?php if ($count > 1) { ?>
        <div class="col-md-12 col-sm-12">
            <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="partial" checked="checked">&nbsp; <?php echo $pshipping; ?></div>
            <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="complete">&nbsp; <?php echo $cshipping; ?></div>

        </div>
        <div class="space-2"></div>
    <?php } ?>

        <!-- Coupon code end **************-->

        <input type="hidden" name="type" value="<?php echo $type; ?>">
        <input type="hidden" id="URLValue" name="URLVALUE" value="<?php echo $base_url; ?>"/>

        <div class="col-md-3 col-sm-6 continue"><a href="<?php echo $url; ?>"><span class="btn "> <?php echo $conshoping; ?></span></a></div>

        <div class="col-md-3 col-sm-6 payment">
            <button class="btn" name="proceedtoshipping" value="proceedtoshipping" type="submit"><?php echo $proceed; ?></button>
        </div>
    </form>
  <?php
  } else {

    echo '<h4>' . $noproductsavail . '</h4>';
  }
}

function cartSmallIteams() {

  global $base_path;
  global $base_url;
  session_start();
  $cartpagetittle = t('Cart Details');
  $cartproduct = t('Product');
  $file = t('File');
  $quantity = t('Quantity');
  $price = t('Price');
  $nocopies = t('No of Copies :');
  $weight = t('Weight');
  $cartdate = t('Cart Date :');
  $totprice = t('Total Price :');
  $tmaterial = t('Material:');
  $dimension = t('Dimensions:');
  $conshoping = t('Continue Shopping');
  $processing = t('Processing:');
  $accessorie = t('Accessories:');
  $proceed = t('Proceed');
  $noimage = t('No Image');
  $noproductsavail = t('No products available in your cart');
  $shippinginfo = t('SHIPPING INFO');
  $orderconfirm = t('ORDER CONFIRM');
  $producttotalprice = t('Product Total Price:');
  $discountprice = t('Discount Price:');
  $finalprice = t('Total Price:');
  $coupanlabel = t('I have an offer code or cash coupon');
  $scoupon = t('Coupon code updated successfully');
  $fcoupon = t('Invalid Coupon Code');
  $pshipping = t('Partial Shipping');
  $cshipping = t('Complete Shipping');
  $_SESSION['session_id'] = session_id();
  if (isset($_POST['sbutton']) && $_POST['sbutton'] != '') {
    if (count($_POST) != 0) {

      $purl = explode("?", $_SERVER['HTTP_REFERER']);
      $url = explode("?", $_SERVER["REQUEST_URI"]);
      $purl1 = explode("&type=", $purl['1']);
      $id = $purl1[0];
      $type = $purl1[1];
      $count = $_POST['simagecount'];

      for ($i = 1; $i <= $count; $i++) {

        $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/smallformat/' . $_FILES['image_' . $i]["name"];
        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      }

      $cartsmall = new MP\CartItems();
      $cartsmall->setSessionId(session_id());
      $cartsmall->setProductType('small');
      $cartsmall->setProductId($_POST['getId']);
      $cartsmall->setProductgroupId($_POST['sproductgroupid']);
      $cartsmall->setDimensionId(isset($_POST['sdimensions']) ? $_POST['sdimensions'] : '');
      $cartsmall->setMaterialId(isset($_POST['smaterialId']) ? $_POST['smaterialId'] : '');
      $cartsmall->setProcessingId(isset($_POST['processing']) ? $_POST['processing'] : '');
      $cartsmall->setShipping(isset($_POST['sshipping']) ? $_POST['sshipping'] : '');
      if ($_POST['squanitity'] == "") {
        $cartsmall->setQuanitity(1);
      } else {
        $cartsmall->setQuanitity(isset($_POST['squanitity']) ? $_POST['squanitity'] : '');
      }

      $cartsmall->setPrice(isset($_POST['smalltotlprice']) ? $_POST['smalltotlprice'] : '');
      $cartsmall->setOperatorReview(isset($_POST['soperatorreview']) ? $_POST['soperatorreview'] : '');
      $cartsmall->setCreatedDate(time());
      $cartsmall->setModifiedDate(time());
      $cartsmall->setAnonymousPack($_POST['anonshipping']);
      $cartsmall->save();
      $idsss = $cartsmall->getCartId();

      /*       * ****** cart images start************ */
      for ($i = 1; $i <= $count; $i++) {
        if ($_FILES['image_' . $i]["name"] != '') {
          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($_FILES['image_' . $i]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setCreatedDate(time());
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        }
      } //small format images
    }
  }


  $sessionid = $_SESSION['session_id'];
  global $language;
  $lang_name = $language->language;
  $countS = MP\CartItemsQuery::create()
    ->where('CartItems.SessionId =?', $sessionid)
    ->count();

  if ($count > 0) {
    $cartQueryS = MP\CartItemsQuery::create()
      ->where('CartItems.SessionId =?', $sessionid);
    ?>
                                                                                                                        <form method="POST" action="<?php echo $base_url . '/' . $lang_name . '/shippingdetails'; ?>">
                                                                                                                            <div class="col-md-12 col-sm-12 product_name">
                                                                                                                                <div class="col-md-5 col-sm-5"><b><?php echo $cartproduct; ?></b></div>
                                                                                                                                <div class="col-md-3 col-sm-3"><b><?php echo $file; ?></b></div>
                                                                                                                                <div class="col-md-2 col-sm-2"><b><?php echo $quantity; ?></b></div>
                                                                                                                                <div class="col-md-2 col-sm-2"><b><?php echo $price; ?></b></div>
                                                                                                                            </div>
                                                                                                                        <?php
                                                                                                                        $i = 1;
                                                                                                                        foreach ($cartQueryS as $result) {
                                                                                                                          if ($result->getProductType() == 'Big') {
                                                                                                                            $pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();


                                                                                                                            foreach ($pquery as $presult) {
                                                                                                                              $sname = $presult->getName();
                                                                                                                            }
                                                                                                                            foreach ($returnquery as $rquery) {

                                                                                                                              $url = $base_url . '/productgridlist?' . $rquery->getSubcategoryId();
                                                                                                                            }
                                                                                                                          }
                                                                                                                          if ($result->getProductType() == 'small') {
                                                                                                                            $pquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                                                                                                                            $returnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($result->getProductId())->find();
                                                                                                                            foreach ($returnquery as $rquery) {
                                                                                                                              $url = $base_url . '/smallformat?' . $rquery->getSubcategoryId();
                                                                                                                            }
                                                                                                                            foreach ($pquery as $presult) {
                                                                                                                              $sname = $presult->getName();
                                                                                                                            }
                                                                                                                          }
                                                                                                                          ?>
                                                                                                                              <div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $result->getCartId() ?>">
                                                                                                                                  <div class="col-md-5 col-sm-5 ">
                                                                                                                                      <div class="cart_4">
                                                                                                                                          <div class="col-md-12 col-sm-12 discountdiv">
                                                                                                                                              <b><?php echo $sname; ?></b></div>

                                                                                                                                          <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">

      <?php echo $nocopies; ?>
                                                                                                                                              </span><span class="datacart">
      <?php echo $result->getQuanitity() ?>
                                                                                                                                              </span></div>
                                                                                                                                          <!-- Start Dimensions -->
                                                                                                                                          <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                                                                                                                              <?php echo $dimension; ?>
                                                                                                                                              </span><span class="datacart">
                                                                                                                              <?php
                                                                                                                              $dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
                                                                                                                              foreach ($dquery as $mquery) {
                                                                                                                                echo $mquery->getWidth() . '*' . $mquery->getHeight();
                                                                                                                              }
                                                                                                                              ?>

                                                                                                                                              </span></div>
                                                                                                                                          <!-- End Dimensions -->
                                                                                                                                          <!-- Start Material -->
                                                                                                                                          <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                                                                                                                              <?php echo $tmaterial; ?>
                                                                                                                                              </span><span class="datacart">
                                                                                                                              <?php
                                                                                                                              $cquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
                                                                                                                              foreach ($cquery as $mquery) {
                                                                                                                                echo $mquery->getName();
                                                                                                                              }
                                                                                                                              ?>
                                                                                                                                              </span></div>

                                                                                                                                          <!-- End Material -->
                                                                                                                                          <!-- Start Cart Date -->
                                                                                                                                          <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                                                                                                                              <?php echo $cartdate; ?>
                                                                                                                                              </span><span class="datacart">
                                                                                                                              <?php
                                                                                                                              $cBIGDATE = $result->getCreatedDate();
                                                                                                                              echo $cBIGDATE->format('Y-m-d');
                                                                                                                              ?>
                                                                                                                                              </span></div>
                                                                                                                                          <!-- End Cart Date -->
                                                                                                                                      </div>
                                                                                                                                  </div>
                                                                                                                                  <div class="col-md-3 col-sm-3 ">

                                                                                                                                                  <?php
                                                                                                                                                  $IMAGESS = MP\CartImagesQuery::create()
                                                                                                                                                    ->withColumn('Max(Id)')
                                                                                                                                                    ->where('CartImages.CartId =?', $result->getCartId())
                                                                                                                                                    ->select(array('image'))
                                                                                                                                                    ->findOne();
                                                                                                                                                  if ($result->getProductType() == 'small') {
                                                                                                                                                    $imageurls = $base_url . "/sites/default/files/cart/smallformat/" . $IMAGESS['image'];
                                                                                                                                                  } else if ($result->getProductType() == 'Big') {
                                                                                                                                                    $imageurls = $base_url . "/sites/default/files/cart/" . $IMAGESS['image'];
                                                                                                                                                  }
                                                                                                                                                  ?>
                                                                                                                                                  <?php if ($IMAGESS['image'] != '') { ?>
                                                                                                                                        <img src="<?php echo $imageurls; ?>" width="100" heigth="100"/>
      <?php
      } else {
        echo '<i class="fa fa-camera"></i>';
      }
      ?>
                                                                                                                                  </div>

                                                                                                                                  <div class="col-md-2 col-sm-2 cartdetailsline">
                                                                                                                                      <div class="col-md-6 col-sm-6 cartdetails">
                                                                                                                                                  <?php echo $result->getQuanitity() ?>
                                                                                                                                      </div>
                                                                                                                                      <div class="col-md-3 col-sm-3 cartdetails ">
      <?php $editurl = $base_url . '/' . $lang_name . '/editcart?' . $result->getCartId(); ?>
                                                                                                                                          <div class="col-md-12 col-sm-12 edit_icon cartdetails">
                                                                                                                                              <a href="<?php echo $editurl; ?>">
                                                                                                                                                  <i class="fa fa-pencil pencil"></i>

                                                                                                                                              </a>
                                                                                                                                          </div>
                                                                                                                                      </div>
                                                                                                                                      <div class="col-md-3 col-sm-3 cartdetails ">
                                                                                                                                          <div class="col-md-12 col-sm-12  delete_icon cartdetails">
                                                                                                                                              <input type="hidden" name="path" value="<?php echo $base_url; ?>/sites/all/themes/meprint/cart.php">
                                                                                                                                              <a class="deletecart" url="<?php echo $result->getCartId(); ?>">
                                                                                                                                                  <i class="fa fa-trash-o"></i></a>
                                                                                                                                          </div>
                                                                                                                                      </div>
                                                                                                                                  </div>
                                                                                                                                  <div class="col-md-2 col-sm-2 cartdetailsline">
                                                                                                                                      <div class="col-md-10 col-sm-10 cartdetails"><b>
                                                                                                                                      <?php
                                                                                                                                      $totlprc = $result->getPrice();
                                                                                                                                      echo '&euro;' . number_format((float) $totlprc, 2, '.', '');
                                                                                                                                      ?>
                                                                                                                                          </b></div>
                                                                                                                                  </div>
                                                                                                                                  <div class="clearfix"></div>
                                                                                                                              </div>
                                                                                                                                      <?php $i++;
                                                                                                                                    }
                                                                                                                                    ?>
                                                                                                                            <div class="space-2"></div>
                                                                                                                            <!-- Coupon code Start **************-->
                                                                                                                                    <?php
                                                                                                                                    $totalPrice = MP\CartItemsQuery::create()
                                                                                                                                      ->withColumn('SUM(Price)')
                                                                                                                                      ->where('CartItems.SessionId =?', $_SESSION['session_id'])
                                                                                                                                      ->groupBySessionId()
                                                                                                                                      ->select(array('price'))
                                                                                                                                      ->findOne(); //$totalPrice['SUMPrice']
                                                                                                                                    ?>

                                                                                                                            <div class="col-md-12 col-sm-12 discountdiv">
                                                                                                                                <div class="col-md-7 col-sm-7">
                                                                                                                                    <input type="checkbox" name="discountcheck" id="discountcheck"> <span class="coupanlabel"><?php echo $coupanlabel; ?></span>

                                                                                                                                    <div class="discountyes">Coupon Code : <input type="text" name="coupancode" value="">
                                                                                                                                        <input type="hidden" name="session_id" value="<?php echo $_SESSION['session_id']; ?>"/>
                                                                                                                                        <input type="hidden" name="totalprice" value="<?php echo $totalPrice['SUMPrice']; ?>"/>
                                                                                                                                        <input type="hidden" name="productprice" value=""/>
                                                                                                                                        <input type="hidden" name="discount_id" value=""/>
                                                                                                                                        <input type="hidden" name="couponpath" readonly="readonly" value="<?php echo $base_url; ?>/sites/all/themes/meprint/discount.php"/>
                                                                                                                                        <button type="button" name="discountbutton" id="discountbutton" value="Submit">Update</button>
                                                                                                                                        <div class="valid" style="display:none;"><span><?php echo $scoupon; ?> </span></div>
                                                                                                                                        <div class="invalid" style="display:none;"><span><?php echo $fcoupon; ?> </span></div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="col-md-5 col-sm-5">
                                                                                                                                    <div class="col-md-7 col-sm-7">
                                                                                                                                        <span class="discountlabel"><?php echo $producttotalprice; ?></span>
                                                                                                                                    </div>
                                                                                                                                    <div class="col-md-5 col-sm-5 discount">
                                                                                                                                        &euro; <input size="7" type="text" name="finalprice" value="<?php echo round($totalPrice['SUMPrice'], 2) ?>" readonly="readonly" class="hiddp"/>
                                                                                                                                    </div>
                                                                                                                                    <div class="dishide">
                                                                                                                                        <div class="col-md-7 col-sm-7">
                                                                                                                                            <span class="discountlabel"><?php echo $discountprice; ?></span>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-5 col-sm-5 discount">
                                                                                                                                            &euro; <input size="7" type="text" name="dicountamount" readonly="readonly" value="0"/>
                                                                                                                                        </div>

                                                                                                                                        <div class="col-md-7 col-sm-7">
                                                                                                                                            <span class="discountlabel"><?php echo $finalprice; ?></span>
                                                                                                                                        </div>
                                                                                                                                        <div class="col-md-5 col-sm-5 discount">
                                                                                                                                            &euro; <input size="7" type="text" name="finalamount" readonly="readonly" value="<?php echo round($totalPrice['SUMPrice'], 2) ?>"/>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>

                                                                                                                            <div class="space-2"></div>

                                                                                                                            <div class="clearfix"></div>
                                                                                                                            <?php if ($count > 1) { ?>
                                                                                                                              <div class="col-md-12 col-sm-12">
                                                                                                                                  <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="partial" checked="checked">&nbsp; <?php echo $pshipping; ?></div>
                                                                                                                                  <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="complete">&nbsp; <?php echo $cshipping; ?></div>

                                                                                                                              </div>
                                                                                                                              <div class="space-2"></div>
    <?php } ?>

                                                                                                                            <!-- Coupon code end **************-->

                                                                                                                            <input type="hidden" name="type" value="<?php echo $type; ?>">

                                                                                                                            <div class="col-md-3 col-sm-6 continue"><a href="<?php echo $url; ?>"><span class="btn "> <?php echo $conshoping; ?></span></a></div>

                                                                                                                            <div class="col-md-3 col-sm-6 payment">
                                                                                                                                <button class="btn " name="proceedtoshipping" value="proceedtoshipping" type="submit"><?php echo $proceed; ?></button>
                                                                                                                            </div>
                                                                                                                        </form>
    <?php
  } else {


    echo '<h4>' . $noproductsavail . '</h4>';
  }
}

/* * ************************ End small format cart small iteams ************** */

function whishlist() {
  global $user;
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
  if ($user->uid == 0) {
    $purl = $_SERVER['HTTP_REFERER'];
    ?>
                                                                                                                        <h3>Please Login</h3>

    <?php
    header('Refresh: 2; url=' . $purl);
  } else {

    session_start();
    $purl = explode("?", $_SERVER['HTTP_REFERER']);
    $url = explode("?", $_SERVER["REQUEST_URI"]);
    $purl1 = explode("&type=", $purl['1']);
    $id = $purl1[0];
    $type = $purl1[1];

    if (isset($_POST['wishlist']) && $_POST['wishlist'] != '') {
      if ($type == 'Big') {
        $count = $_POST['imagecount'];
        for ($i = 1; $i <= $count; $i++) {
          $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/wishlist/' . $_FILES['image_' . $i]["name"];
          move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
        }

        $wishlistinsert = new MP\WishlistItems();
        $wishlistinsert->setUserId($user->uid);
        $wishlistinsert->setProductType('Big');
        if ($_POST['dimensions'] == "custom") {
          $wishlistinsert->setWidth(isset($_POST['productWidth']) ? $_POST['productWidth'] : '');
          $wishlistinsert->setHeight(isset($_POST['productHeight']) ? $_POST['productHeight'] : '');
        } else {
          $wishlistinsert->setDimensionId(isset($_POST['dimensions']) ? $_POST['dimensions'] : '');
        }
        $wishlistinsert->setProductId(isset($_POST['productid']) ? $_POST['productid'] : '');
        $wishlistinsert->setProductgroupId(isset($_POST['productgroupid']) ? $_POST['productgroupid'] : '');
        $wishlistinsert->setAccessoryId(isset($_POST['baseaccessories']) ? $_POST['baseaccessories'] : '');
        $wishlistinsert->setMaterialId(isset($_POST['materialId']) ? $_POST['materialId'] : '');
        $wishlistinsert->setProcessingId(isset($_POST['baseprocessing']) ? $_POST['baseprocessing'] : '');
        $wishlistinsert->setShipping(isset($_POST['shipping']) ? $_POST['shipping'] : '');
        if ($_POST['quanitity'] == "") {
          $wishlistinsert->setQuanitity(1);
        } else {
          $wishlistinsert->setQuanitity(isset($_POST['quanitity']) ? $_POST['quanitity'] : '');
        }
        $wishlistinsert->setPrice(isset($_POST['bigtotlprice']) ? $_POST['bigtotlprice'] : '');
        $wishlistinsert->setCreatedDate(time());
        $wishlistinsert->setModifiedDate(time());
        $wishlistinsert->setExtraDefId(isset($_POST['extradefValue']) ? $_POST['extradefValue'] : '');
        $wishlistinsert->setExtraProcessingTop(isset($_POST['baseisExtraTop']) ? $_POST['baseisExtraTop'] : '');
        $wishlistinsert->setExtraProcessingRight(isset($_POST['baseisExtraRight']) ? $_POST['baseisExtraRight'] : '');
        $wishlistinsert->setExtraProcessingLeft(isset($_POST['baseisExtraLeft']) ? $_POST['baseisExtraLeft'] : '');
        $wishlistinsert->setExtraProcessingBottom(isset($_POST['baseisExtraBottom']) ? $_POST['baseisExtraBottom'] : '');
        $wishlistinsert->setLongtermId(isset($_POST['lngtmprdctn']) ? $_POST['lngtmprdctn'] : '');
        $wishlistinsert->setAnonymousPack(isset($_POST['anonshipping']) ? $_POST['anonshipping'] : '');
        $wishlistinsert->setOperatorReview(isset($_POST['operatorreview']) ? $_POST['operatorreview'] : '');


        $wishlistinsert->save();
        $whishlist_id = $wishlistinsert->getId();

        /*         * *** wishlist images start ********* */

        for ($i = 1; $i <= $count; $i++) {
          if ($_FILES['image_' . $i]["name"] != '') {
            $wishlistimage = new MP\WishlistImages();
            $wishlistimage->setUserId($user->uid);
            $wishlistimage->setImage($_FILES['image_' . $i]["name"]);
            $wishlistimage->setStatus(1);
            $wishlistimage->setWishlistId($whishlist_id);
            $wishlistimage->setCreatedDate(time());
            $wishlistimage->setModifiedDate(time());
            $wishlistimage->save();
          }
        }
        /*         * *** wishlist images end ********* */
        /*         * *** wishlist extra processing start ********* */
        if (isset($_POST['extraprocessing']) && $_POST['extraprocessing'] != '') {
          $wishlistextra = new MP\WishlistExtraProcessing();
          $wishlistextra->setWishlistId($whishlist_id);
          $wishlistextra->setProcessingId($_POST['extraprocessing']);
          $wishlistextra->setCreatedDate(time());
          $wishlistextra->setModifiedDate(time());
          $wishlistextra->setExtraDefValue($_POST['epextadefValue_' . $_POST['extraprocessing']]);
          $wishlistextra->setExtraProcessingTop(isset($_POST['isextratop_' . $_POST['extraprocessing']]) ? $_POST['isextratop_' . $_POST['extraprocessing']] : '');
          $wishlistextra->setExtraProcessingRight(isset($_POST['isextraright_' . $_POST['extraprocessing']]) ? $_POST['isextraright_' . $_POST['extraprocessing']] : '');
          $wishlistextra->setExtraProcessingLeft(isset($_POST['isextraleft_' . $_POST['extraprocessing']]) ? $_POST['isextraleft_' . $_POST['extraprocessing']] : '');
          $wishlistextra->setExtraProcessingBottom(isset($_POST['isextrabottom_' . $_POST['extraprocessing']]) ? $_POST['isextrabottom_' . $_POST['extraprocessing']] : '');
          $wishlistextra->save();
        }

        if (isset($_POST['extraaccessories']) && $_POST['extraaccessories'] != '') {
          $wishlistacc = new MP\WishlistWithoutAccessories();
          $wishlistacc->setWishlistId($whishlist_id);
          $wishlistacc->setAccessoriesId($_POST['extraaccessories']);
          $wishlistacc->setCreatedDate(time());
          $wishlistacc->setModifiedDate(time());
          $wishlistacc->save();
        }
        /*         * *** wishlist extra processing end ********* */

        /** end with big format   ** */
      } else if ($type == 'small') {

        $count = $_POST['simagecount'];
        for ($i = 1; $i <= $count; $i++) {

          $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/wishlist/smallformat/' . $_FILES['image_' . $i]["name"];
          move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
        }
        $wishlistinsert = new MP\WishlistItems();
        $wishlistinsert->setUserId($user->uid);
        $wishlistinsert->setProductType('small');
        $wishlistinsert->setProductId($_POST['getId']);
        $wishlistinsert->setProductgroupId($_POST['sproductgroupid']);
        $wishlistinsert->setDimensionId(isset($_POST['sdimensions']) ? $_POST['sdimensions'] : '');
        $wishlistinsert->setMaterialId(isset($_POST['smaterialId']) ? $_POST['smaterialId'] : '');
        $wishlistinsert->setProcessingId(isset($_POST['processing']) ? $_POST['processing'] : '');
        if ($_POST['squanitity'] == "") {
          $wishlistinsert->setQuanitity(1); // 
        } else {
          $wishlistinsert->setQuanitity(isset($_POST['squanitity']) ? $_POST['squanitity'] : '');
        }
        $wishlistinsert->setShipping(isset($_POST['sshipping']) ? $_POST['sshipping'] : '');
        $wishlistinsert->setPrice(isset($_POST['smalltotlprice']) ? $_POST['smalltotlprice'] : '');
        $wishlistinsert->setOperatorReview(isset($_POST['operatorreview']) ? $_POST['operatorreview'] : '');
        $wishlistinsert->setAnonymousPack(isset($_POST['anonshipping']) ? $_POST['anonshipping'] : '');
        $wishlistinsert->setCreatedDate(time());
        $wishlistinsert->setModifiedDate(time());
        $wishlistinsert->save();
        $whishlist_id = $wishlistinsert->getId();

        for ($i = 1; $i <= $count; $i++) {
          if ($_FILES['image_' . $i]["name"] != '') {
            $wishlistimage = new MP\WishlistImages();
            $wishlistimage->setWishlistId($whishlist_id);
            $wishlistimage->setUserId($user->uid);
            $wishlistimage->setImage($_FILES['image_' . $i]["name"]);
            $wishlistimage->setStatus(1);
            $wishlistimage->setCreatedDate(time());
            $wishlistimage->setModifiedDate(time());
            $wishlistimage->save();
          }
        }
      }
    }
    $wishurl = $base_url . '/' . $lang_name . '/mywishlist';
    header('Refresh: 2; url=' . $wishurl);
  }
}

function orderconfirm() {


  if (isset($_POST['orderconfirm']) && $_POST['orderconfirm'] != '') {
    global $base_url;
    global $user;
    session_start();

    $_SESSION['session_id'];
    $cartItems = MP\CartItemsQuery::create()
      ->where('CartItems.SessionId =?', $_SESSION['session_id'])
      ->find();

    $totalPrice = MP\CartItemsQuery::create()
      ->withColumn('SUM(Price)')
      ->where('CartItems.SessionId =?', $_SESSION['session_id'])
      ->groupBySessionId()
      ->select(array('price'))
      ->findOne(); //$totalPrice['SUMPrice']
    $cartElements = MP\CartItemsQuery::create()
      ->where('CartItems.SessionId =?', $_SESSION['session_id'])
      ->find()
      ->count(); //count 

    if ($user->uid != 0) {


      /*       * ************* order table start ************* */
      $order = new MP\Orders();
      $order->setUserId($user->uid);
      $order->setUserName($user->name);
      $order->setOrderDate(date("Y-m-d"));
      $order->setGrandtotalPrice($totalPrice['SUMPrice']);
      $order->setPayOption(isset($_POST['pay_options']) ? $_POST['pay_options'] : '');
      $order->setPayStatus('Payment made');
      $order->setPartialCompleteShip($_POST['shipping']);
      $order->setNoofProducts($cartElements);
      //$order->setNotes('desc');
      $order->setAnonymousPackage(1);
      $order->setOrderStatus(1);
      $order->setCreatedDate(time());
      $order->setModifiedDate(time());
      $order->save();
      $orderId = $order->getOrderId();

      //grandtotal_price
      //   MP\OrdersQuery::create()->filterByGrandtotalPrice(0)->delete();

      /*       * ************* order table end ************* */

      /*       * *** order discount start **** */
      if ($_POST['coupon_code'] != '' && $_POST['dicountamount'] != '' && $_POST['dicountamount'] != 0) {
        $orderdiscounts = new MP\OrderDiscounts();
        $orderdiscounts->setDiscountId(isset($_POST['discount_id']) ? $_POST['discount_id'] : ''); //
        //
        //discount_id
        $orderdiscounts->setCouponCode(isset($_POST['coupon_code']) ? $_POST['coupon_code'] : ''); //
        //coupon_code
        $orderdiscounts->setUsedOnUserid($user->uid); //
        $orderdiscounts->setUsedOnOrderid($orderId); //
        $orderdiscounts->setUsedIndate(date('Y-m-d'));
        //productprice
        //dicountamount
        $orderdiscounts->setActualPrice(isset($_POST['productprice']) ? $_POST['productprice'] : '');
        $orderdiscounts->setDiscountPrice(isset($_POST['dicountamount']) ? $_POST['dicountamount'] : '');
        $orderdiscounts->setCreatedDate(time());
        $orderdiscounts->setModifiedDate(time());
        $orderdiscounts->save();
      }
      /*       * *** order discount end **** */


      /*       * ************* order shipping table start ************* */

      $shipping = new MP\OrderShippingDetails();
      $shipping->setUserId($user->uid);
      $shipping->setOrderId($orderId);

      $shipping->setName(isset($_POST['fname']) ? $_POST['fname'] : '');
      $shipping->setEmail(isset($_POST['email']) ? $_POST['email'] : '');
      $shipping->setPhone(isset($_POST['phone']) ? $_POST['phone'] : '');
      $shipping->setAddress(isset($_POST['saddress']) ? $_POST['saddress'] : '');
      $shipping->setLocation(isset($_POST['slocation']) ? $_POST['slocation'] : '');
      $shipping->setZipcode(isset($_POST['szipcode']) ? $_POST['szipcode'] : '');
      $shipping->setProvinance(isset($_POST['sprovience']) ? $_POST['sprovience'] : '');
      $shipping->setCountry(isset($_POST['scountry']) ? $_POST['scountry'] : '');
      $shipping->setCreatedDate(time());
      $shipping->setModifiedDate(time());
      $shipping->save();
      /*       * ************* order shipping table end ************* */


      /*       * ************ Production Start date ************* */
      date_default_timezone_set('Asia/Kolkata');
      $today = date("Y-m-d");
      $todaytime = date("H");
      $kdate = date("l", strtotime($today));
      if ($todaytime >= 12) {
        if (($kdate == 'Friday') || ($kdate == 'Saturday') || ($kdate == 'Sunday')) {
          $nextmonday = strtotime("next Monday", strtotime($today));
          $startdate = date("Y-m-d", $nextmonday);
        } else {
          $productiondate = strtotime("+1 day", strtotime($today));
          $startdate = date("Y-m-d", $productiondate);
        }
      } else if ($todaytime < 12) {
        if (($kdate == 'Saturday') || ($kdate == 'Sunday')) {
          $nextmonday = strtotime("next Monday", strtotime($today));
          $startdate = date("Y-m-d", $nextmonday);
        } else {
          $startdate = date("Y-m-d");
        }
      }

      /*       * ******** End Production start date *********** */


      /*       * ***********Order Elements start ************* */

      foreach ($cartItems as $values) {

        $elements = new MP\OrderElements();
        $elements->setUserId($user->uid);
        $elements->setOrderId($orderId);
        $elements->setProductionStatusId(1);
        $elements->setNotes('desc');
        $elements->setPrintOrientation('Horizontal');
        $elements->setExtraFieldPriceCal('ExtraFieldPriceCal');
        $elements->setNoofElements($values->getQuanitity());
        $elements->setPrice($values->getPrice());
        $elements->setWidth($values->getWidth());
        $elements->setHeight($values->getHeight());
        $elements->setProductionStartDate($startdate);
        $elements->setEstimatedShippingDate($startdate);
        $elements->setFormId($values->getProductId());
        $elements->setProductgroupId($values->getProductgroupId());
        $elements->setFormType($values->getProductType());
        $elements->setMaterialId($values->getMaterialId());
        $elements->setProcessingId($values->getProcessingId());
        $elements->setAcessoryId($values->getAccessoryId());
        $elements->setCheckUploadedFiles($values->getOperatorReview());
        $elements->setAnonymousPackaging($values->getAnonymousPack());
        $elements->setDimensionId($values->getDimensionId());
        $elements->setProductgroupId($values->getProductgroupId());
        $elements->setExtraProcessingTop($values->getExtraProcessingTop());
        $elements->setExtraProcessingBottom($values->getExtraProcessingBottom());
        $elements->setExtraProcessingLeft($values->getExtraProcessingLeft());
        $elements->setExtraProcessingRight($values->getExtraProcessingRight());

        $elements->setCreatedDate(time());
        $elements->setModifiedDate(time());
        $elements->save();


        $EID = $elements->getOrderElementId();

        $cartImagesN = MP\CartImagesQuery::create()
          ->where('CartImages.CartId =?', $values->getCartId())
          ->find();

        foreach ($cartImagesN as $hj) {
          $elementfiles = new MP\OrderElementsFiles();
          $elementfiles->setOrderElementId($EID);
          $elementfiles->setFilename($hj->getImage());
          $elementfiles->setFileStatus(1);
          $elementfiles->setCreatedDate(time());
          $elementfiles->setModifiedDate(time());
          $elementfiles->save();
          //order_elements_files
        }
      }

      /*       * ***********Order Elements end ************* */

      foreach ($cartItems as $delete) {

        $DELETEIMAGES = MP\CartImagesQuery::create()->filterByCartId($delete->getCartId())->delete();
        $DELETEEXTRAPROCESSING = MP\CartExtraProcessingQuery::create()->filterByCartId($delete->getCartId())->delete();
        $DELETECARTITEMS = MP\CartItemsQuery::create()->filterByCartId($delete->getCartId())->delete();
      }
    } else if ($user->uid == 0) {
      $query = "SELECT max(uid) as idds FROM users";
      $project = db_query($query)
        ->fetchObject();

      //echo $project->idds;
      //  exit;

      $name = isset($_POST['fnames']) ? $_POST['fnames'] : '';
      $mail = isset($_POST['emails']) ? $_POST['emails'] : '';
      $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
      $address = isset($_POST['address']) ? $_POST['address'] : '';
      $zipcode = isset($_POST['zipcode']) ? $_POST['zipcode'] : '';
      $provience = isset($_POST['provience']) ? $_POST['provience'] : '';
      $location = isset($_POST['location']) ? $_POST['location'] : '';
      $insertQuery = db_insert('users')
        ->fields(array(
        'uid' => $project->idds + 1,
        'name' => $name,
        'mail' => $mail,
        'status' => 1,
      ));


      $d = $insertQuery->execute();

      $LastQuery = "SELECT max(uid) as latest FROM users";
      $lastRecord = db_query($LastQuery)
        ->fetchObject();
      $field_data_field_phone = db_insert('field_data_field_phone')
        ->fields(array(
        'entity_type' => 'user',
        'bundle' => 'user',
        'entity_id' => $lastRecord->latest,
        'revision_id' => $lastRecord->latest,
        'delta' => 0,
        'field_phone_value' => $phone
      ));

      $field_data_field_phone->execute(); //ok

      $add = db_insert('field_data_field_address')
        ->fields(array(
        'entity_type' => 'user',
        'bundle' => 'user',
        'entity_id' => $lastRecord->latest,
        'revision_id' => $lastRecord->latest,
        'delta' => 0,
        'field_address_value' => $address
      ));

      $add->execute(); //not 

      $k = db_insert('field_data_field_zip_code')
        ->fields(array(
        'entity_type' => 'user',
        'bundle' => 'user',
        'entity_id' => $lastRecord->latest,
        'revision_id' => $lastRecord->latest,
        'delta' => 0,
        'field_zip_code_value' => $zipcode
      ));
      $k->execute();


      $field_data_field_province = db_insert('field_data_field_province')
        ->fields(array(
        'entity_type' => 'user',
        'bundle' => 'user',
        'entity_id' => $lastRecord->latest,
        'revision_id' => $lastRecord->latest,
        'delta' => 0,
        'field_province_value' => $provience
      ));

      $field_data_field_province->execute();


      $field_data_field_location = db_insert('field_data_field_location')
        ->fields(array(
        'entity_type' => 'user',
        'bundle' => 'user',
        'entity_id' => $lastRecord->latest,
        'revision_id' => $lastRecord->latest,
        'delta' => 0,
        'field_location_value' => $location
      )); //ok

      $field_data_field_location->execute();

      /*       * ************* order table start ************* */
      $order = new MP\Orders();
      $order->setUserId($lastRecord->latest);
      $order->setUserName(isset($_POST['fnames']) ? $_POST['fnames'] : '');
      $order->setOrderDate(date("Y-m-d"));
      $order->setGrandtotalPrice($totalPrice['SUMPrice']);
      $order->setPayOption(isset($_POST['pay_options']) ? $_POST['pay_options'] : '');
      $order->setPayStatus('Payment made');
      $order->setNoofProducts($cartElements);
      $order->setPartialCompleteShip($_POST['shipping']);
      // $order->setNotes('desc');
      $order->setAnonymousPackage(1);
      $order->setOrderStatus(1);
      $order->setCreatedDate(time());
      $order->setModifiedDate(time());
      $order->save();
      $orderId = $order->getOrderId();


      // MP\OrdersQuery::create()->filterByGrandtotalPrice(0)->delete();
      /*       * ************* order table end ************* */

      /*       * *** order discount start **** */
      if ($_POST['coupon_code'] != '' && $_POST['dicountamount'] != '') {
        $orderdiscounts = new MP\OrderDiscounts();
        $orderdiscounts->setDiscountId($_POST['discount_id']); //
        $orderdiscounts->setCouponCode($_POST['coupon_code']); //
        $orderdiscounts->setUsedOnUserid($lastRecord->latest); //
        $orderdiscounts->setUsedOnOrderid($orderId); //
        $orderdiscounts->setUsedIndate(date('Y-m-d'));
        $orderdiscounts->setActualPrice($_POST['productprice']);
        $orderdiscounts->setDiscountPrice($_POST['dicountamount']);
        $orderdiscounts->setCreatedDate(time());
        $orderdiscounts->setModifiedDate(time());
        $orderdiscounts->save();
      }
      /*       * *** order discount end **** */
      /*       * ************* order shipping table start ************* */

      $shipping = new MP\OrderShippingDetails();
      $shipping->setUserId($lastRecord->latest);
      $shipping->setOrderId($orderId);

      $shipping->setName(isset($_POST['fnames']) ? $_POST['fnames'] : '');
      $shipping->setEmail(isset($_POST['emails']) ? $_POST['emails'] : '');
      $shipping->setPhone(isset($_POST['phone']) ? $_POST['phone'] : '');
      $shipping->setAddress(isset($_POST['saddress']) ? $_POST['saddress'] : '');
      $shipping->setLocation(isset($_POST['slocation']) ? $_POST['slocation'] : '');
      $shipping->setZipcode(isset($_POST['szipcode']) ? $_POST['szipcode'] : '');
      $shipping->setProvinance(isset($_POST['sprovience']) ? $_POST['sprovience'] : '');
      $shipping->setCountry(isset($_POST['scountry']) ? $_POST['scountry'] : '');
      $shipping->setCreatedDate(time());
      $shipping->setModifiedDate(time());
      $shipping->save();
      /*       * ************* order shipping table end ************* */


      foreach ($cartItems as $values) {

        $elements = new MP\OrderElements();
        $elements->setUserId($lastRecord->latest);
        $elements->setOrderId($orderId);
        $elements->setProductionStatusId(1);
        $elements->setNotes('desc');
        $elements->setPrintOrientation('Horizontal');
        $elements->setExtraFieldPriceCal('ExtraFieldPriceCal');
        $elements->setNoofElements($values->getQuanitity());
        $elements->setPrice($values->getPrice());
        $elements->setWidth($values->getWidth());
        $elements->setHeight($values->getHeight());
        $elements->setProductionStartDate($startdate);
        $elements->setEstimatedShippingDate($startdate);
        $elements->setFormId($values->getProductId());
        $elements->setProductgroupId($values->getProductgroupId());
        $elements->setFormType($values->getProductType());
        $elements->setMaterialId($values->getMaterialId());
        $elements->setProcessingId($values->getProcessingId());
        $elements->setAcessoryId($values->getAccessoryId());
        $elements->setCheckUploadedFiles($values->getOperatorReview());
        $elements->setAnonymousPackaging($values->getAnonymousPack());
        $elements->setDimensionId($values->getDimensionId());
        $elements->setProductgroupId($values->getProductgroupId());
        $elements->setExtraProcessingTop($values->getExtraProcessingTop());
        $elements->setExtraProcessingBottom($values->getExtraProcessingBottom());
        $elements->setExtraProcessingLeft($values->getExtraProcessingLeft());
        $elements->setExtraProcessingRight($values->getExtraProcessingRight());

        $elements->setCreatedDate(time());
        $elements->setModifiedDate(time());
        $elements->save();


        $EID = $elements->getOrderElementId();
        $cartImagesN = MP\CartImagesQuery::create()
          ->where('CartImages.CartId =?', $values->getCartId())
          ->find();
        /*         * **** order element files start *********** */

        foreach ($cartImagesN as $hj) {
          $elementfiles = new MP\OrderElementsFiles();
          $elementfiles->setOrderElementId($EID);
          $elementfiles->setFilename($hj->getImage());
          $elementfiles->setFileStatus(1);
          $elementfiles->setCreatedDate(time());
          $elementfiles->setModifiedDate(time());
          $elementfiles->save();
          //order_elements_files
        }
        /*         * **** order element files end *********** */
      }
      foreach ($cartItems as $delete) {

        $DELETEIMAGES = MP\CartImagesQuery::create()->filterByCartId($delete->getCartId())->delete();
        $DELETEEXTRAPROCESSING = MP\CartExtraProcessingQuery::create()->filterByCartId($delete->getCartId())->delete();
        $DELETECARTITEMS = MP\CartItemsQuery::create()->filterByCartId($delete->getCartId())->delete();
      }
    }
    $_SESSION["orderid"] = $orderId;
    if ($_POST['pay_options'] == 'paypal') {

      paypaldata($_POST);
    } else if ($_POST['pay_options'] == 'wire') {
      echo '<h4>Order sucessfully created</h4>';
      ordersucess($orderId);
    } else if ($_POST['pay_options'] == 'cash') {
      echo '<h4>Order sucessfully created</h4>';
      ordersucess($orderId);
    }
  } else {
    echo '<h2>You Are Not Authorized to Access this Page</h2>';
  }
}

function paypaldata($data) {

  global $base_url;
  global $language;
  $lang_name = $language->language;
  $returnurl = $base_url . '/' . $lang_name . '/ordersucess';
  $query = array();
  $query['notify_url'] = 'koteswararao@swayaminfologic.com';

  $query['cmd'] = '_cart';
  $query['upload'] = '1';
  $query['no_note'] = '0';
  $query['bn'] = 'PP-BuyNowBF';
  $query['tax'] = '0';
  $query['rm'] = '2';
  $query['business'] = 'amministrazione-facilitator@meprint.it';
  $query['handling_cart'] = '0';
  $query['currency_code'] = 'EUR';
  $query['lc'] = 'GB';
  $query['discount_amount_cart'] = $data['dicountamount'];
  $query['return'] = $returnurl;
  $query['cancel_return'] = $base_url;
  $query['cbt'] = $base_url;
  $query['custom'] = '';
  $query['address_override'] = '1';
  if ($data['fname'] != '' && $data['email'] != '') {
    $query['first_name'] = $data['fname'];
    $query['email'] = $data['email'];
  } else {
    $query['first_name'] = $data['fnames'];
    $query['email'] = $data['emails'];
  }
  $query['address1'] = $data['saddress'];
  $query['city'] = $data['slocation'];
  $query['state'] = $data['sprovience'];
  $query['country'] = $data['scountry'];
  $query['zip'] = $data['szipcode'];
  $query['night_phone_a'] = $data['phone'];
  for ($i = 1; $i <= $data['cartcount']; $i++) {
    $query['item_name_' . $i] = $data['item_name_' . $i];
    $query['amount_' . $i] = '1';
  }
  // Prepare query string
  $query_string = http_build_query($query);

  header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr?' . $query_string);
}

function ordersucess($orderId) {
  global $base_url;
  ?>
                                                                                                                      <link rel="stylesheet" href="<?php echo $base_url; ?>/sites/all/themes/meprint/css/meprint.css" type="text/css" media="print">
                                                                                                                      <style type="text/css" media="print">

                                                                                                                          .nonPrintable {
                                                                                                                              display: none;
                                                                                                                          }

                                                                                                                          /*class for the element we don’t want to print*/
                                                                                                                      </style>

                                                                                                                      <script type="text/javascript">
                                                                                                                        function PrintDiv() {
                                                                                                                            var divToPrint = document.getElementById('divToPrint');
                                                                                                                            var popupWin = window.open('', '_blank', 'width=650,height=650');
                                                                                                                            popupWin.document.open();
                                                                                                                            popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
                                                                                                                            popupWin.document.close();
                                                                                                                        }
                                                                                                                      </script>
                                                                                                                      <?php
                                                                                                                      $orderid = $orderId;
                                                                                                                      $orderquery = MP\OrderShippingDetailsQuery::create()
                                                                                                                        ->filterByOrderId($orderid)
                                                                                                                        ->find();
                                                                                                                      $payorderquery = MP\OrdersQuery::create()
                                                                                                                        ->filterByOrderId($orderid)
                                                                                                                        ->find();
                                                                                                                      foreach ($orderquery as $oquery) {
                                                                                                                        
                                                                                                                      }
                                                                                                                      foreach ($payorderquery as $poquery) {
                                                                                                                        
                                                                                                                      }
                                                                                                                      $orderdetailquery = MP\OrderElementsQuery::create()
                                                                                                                        ->filterByOrderId($orderid)
                                                                                                                        ->find();
                                                                                                                      $totalorderPrice = MP\OrderElementsQuery::create()
                                                                                                                        ->withColumn('SUM(Price)')
                                                                                                                        ->where('OrderElements.OrderId =?', $orderid)
                                                                                                                        ->groupByOrderId()
                                                                                                                        ->select(array('price'))
                                                                                                                        ->findOne(); //$totalPrice['SUMPrice']
                                                                                                                      $orderdiscountscount = MP\OrderDiscountsQuery::create()
                                                                                                                        ->where('OrderDiscounts.UsedOnOrderid =?', $orderid)
                                                                                                                        ->groupByUsedOnOrderid()
                                                                                                                        ->select(array('discount_price'))
                                                                                                                        ->count();

                                                                                                                      $orderdiscountsPrice = MP\OrderDiscountsQuery::create()
                                                                                                                        ->where('OrderDiscounts.UsedOnOrderid =?', $orderid)
                                                                                                                        ->groupByUsedOnOrderid()
                                                                                                                        ->select(array('discount_price'))
                                                                                                                        ->findOne(); //$totalPrice['SUMPrice']
                                                                                                                      ?>
                                                                                                                      <div id="divToPrint" class="clearfix">
                                                                                                                          <div class="col-sm-12 col-md-12 personal_border">
                                                                                                                              <div class="col-sm-12 col-md-12 order_title"><b>Personal Details</b></div>
                                                                                                                              <div class="col-sm-6 col-md-6 ordersucess"><label class="orderlabel">Name :</label> <?php echo $oquery->getName(); ?></div>
                                                                                                                              <div class="col-sm-6 col-md-6 ordersucess"><label class="orderlabel">Amount :</label>&euro;<?php
                                                                                                                      if ($orderdiscountscount != 0) {
                                                                                                                        echo round($totalorderPrice['SUMPrice'], 2) - $orderdiscountsPrice;
                                                                                                                      } else {
                                                                                                                        echo round($totalorderPrice['SUMPrice'], 2);
                                                                                                                      }
                                                                                                                      ?></div>

                                                                                                                              <div class="col-sm-6 col-md-6 ordersucess"><label class="orderlabel">Email :</label><?php echo $oquery->getEmail(); ?> </div>
                                                                                                                              <div class="col-sm-6 col-md-6 ordersucess"><label class="orderlabel">Payment Type :</label><?php echo $poquery->getPayOption(); ?></div>
                                                                                                                              <div class="col-sm-6 col-md-6 ordersucess"><label class="orderlabel">Phone No :</label><?php echo $oquery->getPhone(); ?> </div>
                                                                                                                      <?php if ($poquery->getPayOption() == 'paypal') { ?>
                                                                                                                                <div class="col-sm-6 col-md-6 ordersucess"><label>Transaction Id :</label><?php echo $poquery->getTransactionId(); ?></div>
                                                                                                                      <?php } ?>
                                                                                                                          </div>
                                                                                                                          <div class="col-sm-12 col-md-12 personal_border_1">
                                                                                                                              <div class="col-sm-6 col-md-6">
                                                                                                                                  <div class="order_title"><b>Shipping Address</b></div>
                                                                                                                                  <div>
                                                                                                                      <?php
                                                                                                                      echo '<label class="orderlabel">Address:</label>' . $oquery->getAddress() . '</br>';
                                                                                                                      echo '<label class="orderlabel">Location:</label>' . $oquery->getLocation() . '</br>';
                                                                                                                      echo '<label class="orderlabel">Provinance:</label>' . $oquery->getProvinance() . '</br>';
                                                                                                                      echo '<label class="orderlabel">Country:</label>' . $oquery->getCountry() . '-';
                                                                                                                      echo $oquery->getZipcode();
                                                                                                                      ?>
                                                                                                                                  </div>

                                                                                                                              </div>

                                                                                                                          </div>

                                                                                                                          <div class="col-sm-12 col-md-12 personal_border_1">
                                                                                                                              <div class="col-sm-12 col-md-12 order_title"><b>Product Details</b></div>
                                                                                                                              <table class="col-sm-12 col-md-12">
                                                                                                                                  <tbody style="border:none;">
                                                                                                                                      <tr style="border:none;">
                                                                                                                                          <th class="col-sm-2 col-md-2" style="border:none;">S.No</th>
                                                                                                                                          <th class="col-sm-6 col-md-6" style="border:none;">Product Name</th>
                                                                                                                                          <th class="col-sm-2 col-md-2" style="border:none;"></th>
                                                                                                                                          <th class="col-sm-2 col-md-2" style="border:none;">QTY</th>

                                                                                                                                          <th class="col-sm-2 col-md-2" style="border:none;">Price</th>
                                                                                                                                      </tr><?php
                                                                                                                    $i = 0;
                                                                                                                    foreach ($orderdetailquery as $odquery) {
                                                                                                                      $i++;
                                                                                                                      ?>
                                                                                                                                        <tr class="tdborder">
                                                                                                                                            <td class="col-sm-2 col-md-2"><?php echo $i; ?></td>
                                                                                                                                            <td class="col-sm-6 col-md-6"><?php
                                                                                                                      if ($odquery->getFormType() == 'Big') {
                                                                                                                        $borderelement = MP\FormoneLangQuery::create()
                                                                                                                          ->where('FormoneLang.FormId =?', $odquery->getFormId())
                                                                                                                          ->groupByFormId()
                                                                                                                          ->select(array('name'))
                                                                                                                          ->findOne();
                                                                                                                        $bordermaterial = MP\MaterialsLangQuery::create()
                                                                                                                          ->where('MaterialsLang.MaterialId =?', $odquery->getMaterialId())
                                                                                                                          ->groupByMaterialId()
                                                                                                                          ->select(array('name'))
                                                                                                                          ->findOne();
                                                                                                                        if ($odquery->getDimensionId() != 0) {
                                                                                                                          $borderdimesion = MP\DimensionsQuery::create()
                                                                                                                            ->where('Dimensions.DimensionId =?', $odquery->getDimensionId())
                                                                                                                            ->groupByDimensionId()
                                                                                                                            ->select(array('name'))
                                                                                                                            ->findOne();
                                                                                                                        } else {
                                                                                                                          $borderdimesion = $odquery->getWidth() . '*' . $odquery->getHeight();
                                                                                                                        }
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Name:</label><b>' . $borderelement . '</b></div>';
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Dimensions:</label>' . $borderdimesion . '</div>';
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Material:</label>' . $bordermaterial . '</div>';
                                                                                                                        $oBIGDATE = $odquery->getCreatedDate();
                                                                                                                        echo '<label class="orderlabel">Order Date:</label>' . $oBIGDATE->format('Y-m-d');
                                                                                                                      } else if ($odquery->getFormType() == 'small') {
                                                                                                                        $sorderelement = MP\FormtwoLangQuery::create()
                                                                                                                          ->where('FormtwoLang.FormId =?', $odquery->getFormId())
                                                                                                                          ->groupByFormId()
                                                                                                                          ->select(array('name'))
                                                                                                                          ->findOne();
                                                                                                                        $sordermaterial = MP\MaterialsLangQuery::create()
                                                                                                                          ->where('MaterialsLang.MaterialId =?', $odquery->getMaterialId())
                                                                                                                          ->groupByMaterialId()
                                                                                                                          ->select(array('name'))
                                                                                                                          ->findOne();
                                                                                                                        if ($odquery->getDimensionId() != 0) {
                                                                                                                          $sorderdimesion = MP\DimensionsQuery::create()
                                                                                                                            ->where('Dimensions.DimensionId =?', $odquery->getDimensionId())
                                                                                                                            ->groupByDimensionId()
                                                                                                                            ->select(array('name'))
                                                                                                                            ->findOne();
                                                                                                                        } else {
                                                                                                                          $sorderdimesion = $odquery->getWidth() . '*' . $odquery->getHeight();
                                                                                                                        }
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Name:</label><b>' . $sorderelement . '</b></div>';
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Dimensions:</label>' . $sorderdimesion . '</div>';
                                                                                                                        echo '<div class="order_line"><label class="orderlabel">Material:</label>' . $sordermaterial . '</div>';
                                                                                                                        $osDATE = $odquery->getCreatedDate();
                                                                                                                        echo '<label class="orderlabel">Order Date:</label>' . $osDATE->format('Y-m-d');
                                                                                                                      }
                                                                                                                        ?></td>

                                                                                                                                            <td class="col-sm-2 col-md-2"></td>
                                                                                                                                            <td class="col-sm-2 col-md-2"><?php echo $odquery->getNoofElements(); ?></td>
                                                                                                                                            <td class="col-sm-2 col-md-2">&euro;<?php echo $odquery->getPrice(); ?></td>
                                                                                                                                        </tr>
  <?php }
  ?>
                                                                                                                                      <tr style="border:none;">
                                                                                                                                          <td class="col-sm-2 col-md-2"></td>
                                                                                                                                          <td class="col-sm-6 col-md-6"></td>
                                                                                                                                          <td class="col-sm-2 col-md-2"></td>
                                                                                                                                          <td class="col-sm-2 col-md-2"><label>Total Price:</label></td>
                                                                                                                                          <td class="col-sm-2 col-md-2">&euro;<?php echo round($totalorderPrice['SUMPrice'], 2) ?></td>
                                                                                                                                      </tr>
                                                                                                                                      <?php if ($orderdiscountscount != 0) { ?>
                                                                                                                                        <tr style="border:none;">
                                                                                                                                            <td class="col-sm-2 col-md-2"></td>
                                                                                                                                            <td class="col-sm-6 col-md-6"></td>
                                                                                                                                            <td class="col-sm-2 col-md-2"></td>
                                                                                                                                            <td class="col-sm-2 col-md-2"><label>Discount Price:</label></td>
                                                                                                                                            <td class="col-sm-2 col-md-2">&euro;<?php echo $orderdiscountsPrice; ?></td>
                                                                                                                                        </tr>
                                                                                                                                        <tr style="border:none;">
                                                                                                                                            <td class="col-sm-2 col-md-2"></td>
                                                                                                                                            <td class="col-sm-6 col-md-6"></td>
                                                                                                                                            <td class="col-sm-2 col-md-2"></td>
                                                                                                                                            <td class="col-sm-2 col-md-2"><label>Final Price:</label></td>
                                                                                                                                            <td class="col-sm-2 col-md-2">&euro;<?php echo round($totalorderPrice['SUMPrice'], 2) - $orderdiscountsPrice; ?></td>
                                                                                                                                        </tr>
  <?php } ?>
                                                                                                                                  </tbody>
                                                                                                                              </table>
                                                                                                                          </div>
                                                                                                                      </div>
                                                                                                                      <div class="col-sm-10 col-md-10 payment">
                                                                                                                          <input type="button" value="print" onclick="PrintDiv();"/>
                                                                                                                      </div>
  <?php
  Custom_order_mail($message_subject, $message_body, $orderId);
}

/* * ************ Cart Update Function start          ************** */

function carteditsuccess() {

  global $user;
  global $base_url;
  global $language;
  $lang_name = $language->language;
  if ($_POST['type'] == "Big") {

    if ($_POST['dimensions'] == "custom") {
      $width = $_POST['productWidth'];
      $height = $_POST['productHeight'];
      $dime = '';
    } else {
      $dime = $_POST['dimensions'];
      $width = '';
      $height = '';
    }
    $count = $_POST['imagecount'];
    for ($i = 1; $i <= $count; $i++) {
      $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/' . $_FILES['image_' . $i]["name"];
      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
    }
    $result = MP\CartItemsQuery::create()
      ->filterByCartId($_POST['cartid'])
      ->update(array(
      'MaterialId' => $_POST['materialId'],
      'Quanitity' => $_POST['quanitity'],
      'Shipping' => $_POST['shipping'],
      'DimensionId' => $dime,
      'Width' => $width,
      'Height' => $height,
      'ProcessingId' => $_POST['baseprocessing'],
      'Price' => $_POST['bigtotlprice'],
      'ModifiedDate' => time(),
      'ExtraProcessingTop' => $_POST['baseisExtraTop'],
      'ExtraProcessingRight' => $_POST['baseisExtraRight'],
      'ExtraProcessingBottom' => $_POST['baseisExtraBottom'],
      'ExtraProcessingLeft' => $_POST['baseisExtraLeft'],
      'ExtraDefValue' => $_POST['extadefValue'],
      'AccessoryId' => $_POST['baseaccessories'],
      'OperatorReview' => $_POST['operatorreview'],
      'AnonymousPack' => $_POST['anonshipping'],
      'LongtermId' => $_POST['lngtmprdctn'],
    ));


    if ($_POST['extraprocessing'] && $_POST['extraprocessing'] != '') {

      $result = MP\CartExtraProcessingQuery::create()
        ->filterByCartId($_POST['cartid'])
        ->delete();

      $cartExtra = new MP\CartExtraProcessing();
      $cartExtra->setCartId($_POST['cartid']);
      $cartExtra->setProcessingId($_POST['extraprocessing']);
      $cartExtra->setExtraDefValue($_POST['epextadefValue_' . $_POST['extraprocessing']]);
      $cartExtra->setExtraProcessingTop(isset($_POST['isextratop_' . $_POST['extraprocessing']]) ? $_POST['isextratop_' . $_POST['extraprocessing']] : '');
      $cartExtra->setExtraProcessingRight(isset($_POST['isextraright_' . $_POST['extraprocessing']]) ? $_POST['isextraright_' . $_POST['extraprocessing']] : '');
      $cartExtra->setExtraProcessingLeft(isset($_POST['isextraleft_' . $_POST['extraprocessing']]) ? $_POST['isextraleft_' . $_POST['extraprocessing']] : '');
      $cartExtra->setExtraProcessingBottom(isset($_POST['isextrabottom_' . $_POST['extraprocessing']]) ? $_POST['isextrabottom_' . $_POST['extraprocessing']] : '');
      $cartExtra->setCreatedDate(time());
      $cartExtra->setModifiedDate(time());
      $cartExtra->save();
    }


    if (isset($_POST['extraaccessories']) && $_POST['extraaccessories'] != '') {
      $result = MP\CartWithoutAccessoriesQuery::create()
        ->filterByCartId($_POST['cartid'])
        ->delete();
      $cartExtra = new MP\CartWithoutAccessories();
      $cartExtra->setCartId($_POST['cartid']);
      $cartExtra->setAccessoriesId($_POST['extraaccessories']);
      $cartExtra->setCreatedDate(time());
      $cartExtra->setModifiedDate(time());
      $cartExtra->save();
    }

    $fdfd = MP\CartImagesQuery::create()->filterByCartId($_POST['cartid'])->find();
    if (count($fdfd) != 0) {
      foreach ($fdfd as $vb) {
        if ($_FILES['upload_' . $primaryKey]["name"] != '') {
          MP\CartImagesQuery::create()->filterById($vb->getId())->update(array('Status' => 0));
          $primaryKey = $vb->getId();
          $path = DRUPAL_ROOT . '/sites/default/files/cart/' . $_FILES['upload_' . $primaryKey]["name"];
          move_uploaded_file($_FILES['upload_' . $primaryKey]['tmp_name'], $path);
          if ($_FILES['upload_' . $primaryKey]["name"] != '') {
            $cartimage = new MP\CartImages();
            $cartimage->setCartId($_POST['cartid']);
            $cartimage->setImage($_FILES['upload_' . $primaryKey]["name"]);
            $cartimage->setStatus(1);
            $cartimage->setModifiedDate(time());
            $cartimage->save();
          }
        }
      }
    }

    for ($i = 1; $i <= $_POST['imagecount']; $i++) {

      $path = DRUPAL_ROOT . '/sites/default/files/cart/' . $_FILES['image_' . $i]["name"];

      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      if ($_FILES['image_' . $i]["name"] != '') {
        $cartimage = new MP\CartImages();
        $cartimage->setCartId($_POST['cartid']);
        $cartimage->setImage($_FILES['image_' . $i]["name"]);
        $cartimage->setStatus(1);
        $cartimage->setModifiedDate(time());
        $cartimage->save();
      }
    }
  } else if ($_POST['type'] == "small") {

    $count = $_POST['simagecount'];
    for ($i = 1; $i <= $count; $i++) {
      $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/smallformat/' . $_FILES['image_' . $i]["name"];
      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
    }
    $smallresultCart = MP\CartItemsQuery::create()
      ->filterByCartId($_POST['cartid'])
      ->update(array(
      'MaterialId' => $_POST['smaterialId'],
      'Quanitity' => $_POST['squanitity'],
      'Shipping' => $_POST['sshipping'],
      'DimensionId' => $_POST['sdimensions'],
      'ProcessingId' => $_POST['processing'],
      'ProductType' => $_POST['type'],
      'Price' => $_POST['smalltotlprice'],
      'OperatorReview' => $_POST['operatorreview'],
      'AnonymousPack' => $_POST['anonshipping'],
      'ProcessingId' => $_POST['processing'],
      'ModifiedDate' => time()
    ));  //update cart items


    for ($i = 1; $i <= $_POST['simagecount']; $i++) {
      $path = DRUPAL_ROOT . '/sites/default/files/cart/smallformat/' . $_FILES['image_' . $i]["name"];
      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      if ($_FILES['image_' . $i]["name"] != '') {
        $cartimage = new MP\CartImages();
        $cartimage->setCartId($_POST['cartid']);
        $cartimage->setImage($_FILES['image_' . $i]["name"]);
        $cartimage->setStatus(1);
        $cartimage->setModifiedDate(time());
        $cartimage->save();
      }
    } //insert new files upload

    /*     * *********Existed files upload start ********** */

    $Smallfdfd = MP\CartImagesQuery::create()->filterByCartId($_POST['cartid'])->find();
    if (count($Smallfdfd) != 0) {
      if ($_FILES['upload_' . $primaryKey]["name"] != '') {
        foreach ($Smallfdfd as $vb) {
          MP\CartImagesQuery::create()->filterById($vb->getId())->update(array('Status' => 0));
          $primaryKey = $vb->getId();
          $path = DRUPAL_ROOT . '/sites/default/files/cart/smallformat/' . $_FILES['upload_' . $primaryKey]["name"];
          move_uploaded_file($_FILES['upload_' . $primaryKey]['tmp_name'], $path);
          if ($_FILES['upload_' . $primaryKey]["name"] != '') {
            $cartimage = new MP\CartImages();
            $cartimage->setCartId($_POST['cartid']);
            $cartimage->setImage($_FILES['upload_' . $primaryKey]["name"]);
            $cartimage->setStatus(1);
            $cartimage->setModifiedDate(time());
            $cartimage->save();
          }
        }
      }
    }
    /*     * *********Existed files upload end ********** */
  }
  $cartURL = $base_url . "/" . $lang_name . "/cart";
  echo '<h4> Cart details are sucessfully updated.</h4>';
  header('Refresh: 0; url=' . $cartURL);
}

/* * ************   Cart Update Function end       ************** */

/* * ************   Wishlist Update Function Start       ***************** */

function wishlistsuccess() {

  global $user;
  global $base_url;
  global $language;
  $lang_name = $language->language;
  if (isset($_POST['proceedc']) && $_POST['proceedc'] != '') {
    addtoCart($_POST);
  } else if (isset($_POST['proceed']) && $_POST['proceed'] != '') {
    if ($_POST['type'] == "Big") {

      if ($_POST['dimensions'] == "custom") {
        $width = $_POST['productWidth'];
        $height = $_POST['productHeight'];
        $dime = '';
      } else {
        $dime = $_POST['dimensions'];
        $width = '';
        $height = '';
      }
      $count = $_POST['imagecount'];
      for ($i = 1; $i <= $count; $i++) {
        $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/wishlist/' . $_FILES['image_' . $i]["name"];
        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      }
      $result = MP\WishlistItemsQuery::create()
        ->filterById($_POST['wishId'])
        ->update(array(
        'MaterialId' => $_POST['materialId'],
        'Quanitity' => $_POST['quanitity'],
        'Shipping' => $_POST['shipping'],
        'DimensionId' => $dime,
        'Width' => $width,
        'Height' => $height,
        'ProcessingId' => $_POST['baseprocessing'],
        'Price' => $_POST['bigtotlprice'],
        'ModifiedDate' => time(),
        'ExtraProcessingTop' => $_POST['baseisExtraTop'],
        'ExtraProcessingRight' => $_POST['baseisExtraRight'],
        'ExtraProcessingBottom' => $_POST['baseisExtraBottom'],
        'ExtraProcessingLeft' => $_POST['baseisExtraLeft'],
        'ExtraDefId' => $_POST['extadefValue'],
        'AccessoryId' => $_POST['baseaccessories'],
        'OperatorReview' => $_POST['operatorreview'],
        'LongtermId' => $_POST['lngtmprdctn'],
        'AnonymousPack' => $_POST['anonshipping'],
        'ModifiedDate' => time(),
      ));


      if ($_POST['extraprocessing'] && $_POST['extraprocessing'] != '') {

        $result = MP\WishlistExtraProcessingQuery::create()
          ->filterByWishlistId($_POST['wishId'])
          ->delete();

        $cartExtra = new MP\WishlistExtraProcessing();
        $cartExtra->setWishlistId($_POST['wishId']);
        $cartExtra->setProcessingId($_POST['extraprocessing']);
        $cartExtra->setExtraDefValue($_POST['epextadefValue_' . $_POST['extraprocessing']]);
        $cartExtra->setExtraProcessingTop($_POST['isextratop_' . $_POST['extraprocessing']]);
        $cartExtra->setExtraProcessingBottom($_POST['isextrabottom_' . $_POST['extraprocessing']]);
        $cartExtra->setExtraProcessingRight($_POST['isextraright_' . $_POST['extraprocessing']]);
        $cartExtra->setExtraProcessingLeft($_POST['isextraleft_' . $_POST['extraprocessing']]);
        $cartExtra->setCreatedDate(time());
        $cartExtra->setModifiedDate(time());
        $cartExtra->save();
      }

      if (isset($_POST['extraaccessories']) && $_POST['extraaccessories'] != '') {
        $result = MP\WishlistWithoutAccessoriesQuery::create()
          ->filterByWishlistId($_POST['wishId'])
          ->delete();

        $cartExtracc = new MP\WishlistWithoutAccessories();
        $cartExtracc->setAccessoriesId($_POST['extraaccessories']);
        $cartExtracc->setModifiedDate(time());
        $cartExtracc->save();
      }

      $fdfd = MP\WishlistImagesQuery::create()->filterByWishlistId($_POST['wishId'])->find();
      if (count($fdfd) != 0) {
        foreach ($fdfd as $vb) {
          $primaryKey = $vb->getImageId();
          if ($_FILES['upload_' . $primaryKey]["name"] != '') {
            MP\WishlistImagesQuery::create()->filterByImageId($vb->getImageId())->update(array('Status' => 0));

            $path = DRUPAL_ROOT . '/sites/default/files/wishlist/' . $_FILES['upload_' . $primaryKey]["name"];
            move_uploaded_file($_FILES['upload_' . $primaryKey]['tmp_name'], $path);
            if ($_FILES['upload_' . $primaryKey]["name"] != '') {
              $cartimage = new MP\WishlistImages();
              $cartimage->setWishlistId($_POST['wishId']);
              $cartimage->setUserId($user->uid);
              $cartimage->setImage($_FILES['upload_' . $primaryKey]["name"]);
              $cartimage->setStatus(1);
              $cartimage->setModifiedDate(time());
              $cartimage->save();
            }
          }
        }
      }

      for ($i = 1; $i <= $_POST['imagecount']; $i++) {

        $path = DRUPAL_ROOT . '/sites/default/files/wishlist/' . $_FILES['image_' . $i]["name"];

        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
        if ($_FILES['image_' . $i]["name"] != '') {
          $cartimage = new MP\WishlistImages();
          $cartimage->setWishlistId($_POST['wishId']);
          $cartimage->setUserId($user->uid);
          $cartimage->setImage($_FILES['image_' . $i]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        }
      }
      $cartURL = $base_url . "/" . $lang_name . "/mywishlist";
      echo '<h4> Wish details are sucessfully updated.</h4>';
      header('Refresh:0; url=' . $cartURL);
    } else if ($_POST['type'] == "small") {

      $count = $_POST['simagecount'];
      for ($i = 1; $i <= $count; $i++) {
        $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/wishlist/smallformat/' . $_FILES['image_' . $i]["name"];
        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
      }
      $smallresultCart = MP\WishlistItemsQuery::create()
        ->filterById($_POST['wishId'])
        ->update(array(
        'MaterialId' => $_POST['smaterialId'], //
        'Quanitity' => $_POST['squanitity'], //
        'Shipping' => $_POST['sshipping'], //
        'DimensionId' => $_POST['sdimensions'], //
        'ProcessingId' => $_POST['processing'], //
        'ProductType' => 'small', //
        'Price' => $_POST['smalltotlprice'], //
        'OperatorReview' => $_POST['operatorreview'],
        'AnonymousPack' => $_POST['anonshipping'],
        'ModifiedDate' => time()
      ));  //update cart items


      for ($i = 1; $i <= $_POST['simagecount']; $i++) {
        $path = DRUPAL_ROOT . '/sites/default/files/wishlist/smallformat/' . $_FILES['image_' . $i]["name"];
        move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
        if ($_FILES['image_' . $i]["name"] != '') {
          $cartimage = new MP\WishlistImages();
          $cartimage->setWishlistId($_POST['wishId']);
          $cartimage->setUserId($user->uid);
          $cartimage->setImage($_FILES['image_' . $i]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        }
      } //insert new files upload

      /*       * *********Existed files upload start ********** */

      $Smallfdfd = MP\WishlistImagesQuery::create()->filterByWishlistId($_POST['wishId'])->find();
      if (count($Smallfdfd) != 0) {

        foreach ($Smallfdfd as $vb) {
          $primaryKey = $vb->getImageId();
          if ($_FILES['upload_' . $primaryKey]["name"] != '') {
            MP\WishlistImagesQuery::create()->filterByImageId($vb->getImageId())->update(array('Status' => 0));
            $path = DRUPAL_ROOT . '/sites/default/files/wishlist/smallformat/' . $_FILES['upload_' . $primaryKey]["name"];
            move_uploaded_file($_FILES['upload_' . $primaryKey]['tmp_name'], $path);
            if ($_FILES['upload_' . $primaryKey]["name"] != '') {
              $cartimage = new MP\WishlistImages();
              $cartimage->setWishlistId($_POST['wishId']);
              $cartimage->setUserId($user->uid);
              $cartimage->setImage($_FILES['upload_' . $primaryKey]["name"]);
              $cartimage->setStatus(1);
              $cartimage->setModifiedDate(time());
              $cartimage->save();
            }
          }
        }
      }
      /*       * *********Existed files upload end ********** */


      $cartURL = $base_url . "/" . $lang_name . "/mywishlist";
      echo '<h4> Wish details are sucessfully updated.</h4>';
      header('Refresh:0; url=' . $cartURL);
    }
  }
}

/* * ************   Wishlist Update Function End       ***************** */

function addtoCart($data) {

  global $base_path;
  global $base_url;
  global $language;
  $lang_name = $language->language;
  session_start();
  $_SESSION['session_id'] = session_id();


  if ($data['type'] == "Big") {

    $count = $data['imagecount'];

    for ($i = 1; $i <= $count; $i++) {
      $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/' . $_FILES['image_' . $i]["name"];
      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
    }
    $cart = new MP\CartItems();
    $cart->setSessionId(session_id());
    $cart->setProductType("Big");
    $cart->setProductId(isset($data['productid']) ? $data['productid'] : ''); //
    $cart->setProductgroupId(isset($data['productgroupid']) ? $data['productgroupid'] : '');
    $cart->setAccessoryId(isset($data['baseaccessories']) ? $data['baseaccessories'] : ''); //
    if (isset($data['dimensions']) && $data['dimensions'] == "custom") {
      $cart->setWidth(isset($data['productWidth']) ? $data['productWidth'] : ''); //
      $cart->setHeight(isset($data['productHeight']) ? $data['productHeight'] : ''); //
    } else if (isset($data['dimensions']) && $data['dimensions'] != "custom") {
      $cart->setDimensionId(isset($data['dimensions']) ? $data['dimensions'] : ''); //
    }
    $cart->setMaterialId(isset($data['materialId']) ? $data['materialId'] : ''); //
    $cart->setProcessingId(isset($data['baseprocessing']) ? $data['baseprocessing'] : ''); //

    $cart->setShipping(isset($data['shipping']) ? $data['shipping'] : ''); //
    if ($data['quanitity'] == "") {
      $cart->setQuanitity(1);
    } else {
      $cart->setQuanitity(isset($data['quanitity']) ? $data['quanitity'] : '');
    }
    //extadefValue
    $cart->setExtraProcessingId(isset($data['extadefValue']) ? $data['extadefValue'] : ''); //
    $cart->setExtraProcessingTop(isset($data['baseisExtraTop']) ? $data['baseisExtraTop'] : ''); //
    $cart->setExtraProcessingRight(isset($data['baseisExtraRight']) ? $data['baseisExtraRight'] : ''); //
    $cart->setExtraProcessingLeft(isset($data['baseisExtraLeft']) ? $data['baseisExtraLeft'] : ''); //
    $cart->setExtraProcessingBottom(isset($data['baseisExtraBottom']) ? $data['baseisExtraBottom'] : ''); //
    $cart->setPrice(isset($data['bigtotlprice']) ? $data['bigtotlprice'] : ''); //
    $cart->setOperatorReview(isset($data['operatorreview']) ? $data['operatorreview'] : '');
    $cart->setLongtermId(isset($data['lngtmprdctn']) ? $data['lngtmprdctn'] : '');
    $cart->setAnonymousPack(isset($data['anonshipping']) ? $data['anonshipping'] : '');


    $cart->setCreatedDate(time());
    $cart->setModifiedDate(time());
    $cart->save();
    $idsss = $cart->getCartId();
    /*     * ****** cart extra processing start************ */
    if (isset($data['extraprocessing']) && $data['extraprocessing'] != '') {

      $cartExtra = new MP\CartExtraProcessing();
      $cartExtra->setCartId($idsss);
      $cartExtra->setProcessingId($data['extraprocessing']);
      $cartExtra->setExtraDefValue($data['epextadefValue_' . $data['extraprocessing']]);
      $cartExtra->setExtraProcessingTop($data['isextratop_' . $data['extraprocessing']]);
      $cartExtra->setExtraProcessingBottom($data['isextrabottom_' . $data['extraprocessing']]);
      $cartExtra->setExtraProcessingRight($data['isextraright_' . $data['extraprocessing']]);
      $cartExtra->setExtraProcessingLeft($data['isextraleft_' . $data['extraprocessing']]);
      $cartExtra->setCreatedDate(time());
      $cartExtra->setModifiedDate(time());
      $cartExtra->save();
    }


    if (isset($data['extraaccessories']) && $data['extraaccessories'] != '') {


      $cartExtracc = new MP\CartWithoutAccessories();
      $cartExtracc->setCartId($idsss);
      $cartExtracc->setAccessoriesId($data['extraaccessories']);
      $cartExtracc->setModifiedDate(time());
      $cartExtracc->save();
    }

    /*     * ****** cart extra processing end************ */

    /*     * ****** cart images step1************ */

    $fdfd = MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->find();
    if (count($fdfd) != 0) {
      foreach ($fdfd as $vb) {
        $primaryKey = $vb->getImageId();
        if ($vb->getImage() != '') {
          $source = DRUPAL_ROOT . '/sites/default/files/wishlist/' . $vb->getImage();
          $destination = DRUPAL_ROOT . '/sites/default/files/cart/' . $vb->getImage();
          copy($source, $destination);
          unlink($source);

          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($vb->getImage());
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        } //moving images wishlist to cart


        /* if($_FILES['upload_'.$primaryKey]["name"] !='') {
          $path =  DRUPAL_ROOT.'/sites/default/files/cart/' . $_FILES['upload_'.$primaryKey]["name"];
          move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
          if($_FILES['upload_'.$primaryKey]["name"] !='') {
          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
          }
          } */
      }
    }

    /*     * ****** cart images step1 end  ************ */


    for ($i = 1; $i <= $count; $i++) {
      if ($_FILES['image_' . $i]["name"] != '') {
        $cartimage = new MP\CartImages();
        $cartimage->setCartId($idsss);
        $cartimage->setImage($_FILES['image_' . $i]["name"]);
        $cartimage->setStatus(1);
        $cartimage->setStatus(1);
        $cartimage->setModifiedDate(time());
        $cartimage->save();
      }
    }
    /*     * ****** delete images in wishlist start  ************ */

    MP\WishlistItemsQuery::create()->filterById($data['wishId'])->delete();
    MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->delete();
    MP\WishlistExtraProcessingQuery::create()->filterByWishlistId($data['wishId'])->delete();
    /*     * ****** delete images in wishlist end  ************ */

    $cartURL = $base_url . '/' . $lang_name . '/cart';
    header('Refresh: 0; url=' . $cartURL);

    //big end  
  } else if ($data['type'] == "small") {
    $count = $data['simagecount'];
    for ($i = 1; $i <= $count; $i++) {
      $path = $_SERVER['DOCUMENT_ROOT'] . $base_path . 'sites/default/files/cart/smallformat/' . $_FILES['image_' . $i]["name"];
      move_uploaded_file($_FILES['image_' . $i]['tmp_name'], $path);
    }


    $cart = new MP\CartItems();
    $cart->setSessionId(session_id());
    $cart->setProductType("small");
    $cart->setProductId(isset($data['productid']) ? $data['productid'] : '');
    $cart->setProductgroupId(isset($data['sproductgroupid']) ? $data['sproductgroupid'] : '');
    $cart->setDimensionId(isset($data['sdimensions']) ? $data['sdimensions'] : '');
    $cart->setMaterialId(isset($data['smaterialId']) ? $data['smaterialId'] : '');
    $cart->setProcessingId(isset($data['processing']) ? $data['processing'] : '');

    $cart->setShipping(isset($data['sshipping']) ? $data['sshipping'] : '');
    if ($data['squanitity'] == "") {
      $cart->setQuanitity(1);
    } else {
      $cart->setQuanitity(isset($data['squanitity']) ? $data['squanitity'] : '');
    }
    $cart->setPrice(isset($data['smalltotlprice']) ? $data['smalltotlprice'] : ''); //
    $cart->setOperatorReview(isset($data['operatorreview']) ? $data['operatorreview'] : '');
    $cart->setAnonymousPack(isset($data['anonshipping']) ? $data['anonshipping'] : '');
    $cart->setCreatedDate(time());
    $cart->setModifiedDate(time());
    $cart->save();
    $idsss = $cart->getCartId();
    for ($i = 1; $i <= $count; $i++) {
      if ($_FILES['image_' . $i]["name"] != '') {
        $cartimage = new MP\CartImages();
        $cartimage->setCartId($idsss);
        $cartimage->setImage($_FILES['image_' . $i]["name"]);
        $cartimage->setStatus(1);
        $cartimage->setStatus(1);
        $cartimage->setModifiedDate(time());
        $cartimage->save();
      }
    }


    $fdfd = MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->find();
    if (count($fdfd) != 0) {
      foreach ($fdfd as $vb) {
        if ($vb->getImage() != '') {
          $source = DRUPAL_ROOT . '/sites/default/files/wishlist/smallformat/' . $vb->getImage();
          $destination = DRUPAL_ROOT . '/sites/default/files/cart/smallformat/' . $vb->getImage();
          copy($source, $destination);
          unlink($source);

          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($vb->getImage());
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
        } //moving images wishlist to cart
        //  $primaryKey =  $vb->getImageId();
        /* if($_FILES['upload_'.$primaryKey]["name"] !='') {
          $path =  DRUPAL_ROOT.'/sites/default/files/cart/smallformat/' . $_FILES['upload_'.$primaryKey]["name"];
          move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
          if($_FILES['upload_'.$primaryKey]["name"] !='') {
          $cartimage = new MP\CartImages();
          $cartimage->setCartId($idsss);
          $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
          $cartimage->setStatus(1);
          $cartimage->setModifiedDate(time());
          $cartimage->save();
          }
          } */ //
      }
    } //inser end

    /*     * ****** delete images in wishlist start  ************ */

    MP\WishlistItemsQuery::create()->filterById($data['wishId'])->delete();
    MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->delete();
    MP\WishlistExtraProcessingQuery::create()->filterByWishlistId($data['wishId'])->delete();
    /*     * ****** delete images in wishlist end  ************ */

    $cartURL = $base_url . '/' . $lang_name . '/cart';
    header('Refresh: 0; url=' . $cartURL);
  }
}

//main funciton 

function Custom_order_mail($message_subject, $message_body, $orderId) {
  include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
  //echo $orderId;
  $orderid = $orderId;
  $orderquery = MP\OrderShippingDetailsQuery::create()
    ->filterByOrderId($orderid)
    ->find();
  $payorderquery = MP\OrdersQuery::create()
    ->filterByOrderId($orderid)
    ->find();
  foreach ($orderquery as $oquery) {
    
  }
  foreach ($payorderquery as $poquery) {
    
  }
  $orderdetailquery = MP\OrderElementsQuery::create()
    ->filterByOrderId($orderid)
    ->find();
  $totalorderPrice = MP\OrderElementsQuery::create()
    ->withColumn('SUM(Price)')
    ->where('OrderElements.OrderId =?', $orderid)
    ->groupByOrderId()
    ->select(array('price'))
    ->findOne(); //$totalPrice['SUMPrice']
  $orderdiscountscount = MP\OrderDiscountsQuery::create()
    ->where('OrderDiscounts.UsedOnOrderid =?', $orderid)
    ->groupByUsedOnOrderid()
    ->select(array('discount_price'))
    ->count();

  $orderdiscountsPrice = MP\OrderDiscountsQuery::create()
    ->where('OrderDiscounts.UsedOnOrderid =?', $orderid)
    ->groupByUsedOnOrderid()
    ->select(array('discount_price'))
    ->findOne(); //$totalPrice['SUMPrice']

  if ($orderdiscountscount != 0) {
    $efinalprice = round($totalorderPrice['SUMPrice'], 2) - $orderdiscountsPrice;
  } else {
    $efinalprice = round($totalorderPrice['SUMPrice'], 2);
  }

  if ($poquery->getPayOption() == 'paypal') {
    $transid = $poquery->getTransactionId();
  } else {
    $transid = 'Bank/Wire Transfer';
  }
  $message_body .= '
    <div style="width:100%; margin-bottom:10px;">
   <div style="width:100%; font-weight:bold; font-size:12px; color:#ed7502;"><b>' . $pdetails . '</b></div>  
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $name . '</label>' . $oquery->getName() . '</div>
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $amount . '</label>&euro;' . $efinalprice . '</div>       
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $email . '</label>' . $oquery->getEmail() . '</div>
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $ptype . '</label>' . $poquery->getPayOption() . '</div>        
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $phone . '</label>' . $oquery->getPhone() . '</div>        
   <div class="col-sm-6 col-md-6 ordersucess"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $tnameid . '</label>' . $transid . '</div>       
   </div><div style="clear:both;"></div><div style="width:100%;  margin-bottom:10px;">          
   <div style="width:100%; font-size:12px; color:#ed7502; font-weight:bold;">' . $billingandshipping . '</div>
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $address . '</label>' . $oquery->getAddress() . '</div>         
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $location . '</label>' . $oquery->getLocation() . '</div>
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $province . '</label>' . $oquery->getProvinance() . '</div>
   <div style="width:50%; float:left;"><label style="font-size:12px; width: 28%; font-weight:bold;">' . $contry . '</label>' . $oquery->getCountry() . '-' . $oquery->getZipcode() . '</div></div>';


  $message_body .= '<div style="width:100%; margin-bottom:10px;">
            <div style="width:100%; font-weight:bold; font-size:12px; color:#ed7502;"><b>' . $productdetails . '</b></div>
            <table style="width:100%;"><tbody style="border:none;"><tr style="border:none;">
             <th style="width:15%; float:left;">' . $sno . '</th>
            <th style="width:45%; float:left;">' . $pname . '</th>
             <th style="width:10%; float:left;"></th>
            <th style="width:15%; float:left;">' . $qty . '</th>
          
            <th style="width:15%; float:left;">' . $price . '</th></tr>';
  $i = 0;
  foreach ($orderdetailquery as $odquery) {
    $i++;
    $message_body .= '<tr class="tdborder">
                    <td style="width:15%; float:left;">' . $i;
    $message_body .= ' </td>
                    <td style="width:45%; float:left;">';
    if ($odquery->getFormType() == 'Big') {
      $borderelement = MP\FormoneLangQuery::create()
        ->where('FormoneLang.FormId =?', $odquery->getFormId())
        ->groupByFormId()
        ->select(array('name'))
        ->findOne();
      $bordermaterial = MP\MaterialsLangQuery::create()
        ->where('MaterialsLang.MaterialId =?', $odquery->getMaterialId())
        ->groupByMaterialId()
        ->select(array('name'))
        ->findOne();
      if ($odquery->getDimensionId() != 0) {
        $borderdimesion = MP\DimensionsQuery::create()
          ->where('Dimensions.DimensionId =?', $odquery->getDimensionId())
          ->groupByDimensionId()
          ->select(array('name'))
          ->findOne();
      } else {
        $borderdimesion = $odquery->getWidth() . '*' . $odquery->getHeight();
      }
      $message_body .= '<div style="line-height20px;"><label style="font-size:12px; width: 28%; font-weight:bold;">Name:</label><b>' . $borderelement . '</b></div>
                  <div style="line-height20px;"><label style="font-size:12px; width: 28%; font-weight:bold;">Dimensions:</label>' . $borderdimesion . '</div>
                <div style="line-height20px;"><label style="font-size:12px; width: 28%; font-weight:bold;">Material:</label>' . $bordermaterial . '</div>';
      $oBIGDATE = $odquery->getCreatedDate();
      $var1 .= '<label style="font-size:12px; width: 28%; font-weight:bold;">Order Date:</label>' . $oBIGDATE->format('Y-m-d');
    } else if ($odquery->getFormType() == 'small') {
      $sorderelement = MP\FormtwoLangQuery::create()
        ->where('FormtwoLang.FormId =?', $odquery->getFormId())
        ->groupByFormId()
        ->select(array('name'))
        ->findOne();
      $sordermaterial = MP\MaterialsLangQuery::create()
        ->where('MaterialsLang.MaterialId =?', $odquery->getMaterialId())
        ->groupByMaterialId()
        ->select(array('name'))
        ->findOne();
      if ($odquery->getDimensionId() != 0) {
        $sorderdimesion = MP\DimensionsQuery::create()
          ->where('Dimensions.DimensionId =?', $odquery->getDimensionId())
          ->groupByDimensionId()
          ->select(array('name'))
          ->findOne();
      } else {
        $sorderdimesion = $odquery->getWidth() . '*' . $odquery->getHeight();
      }
      $message_body .= '<div class="order_line"><label style="font-size:12px; width: 28%; font-weight:bold;">Name:</label><b>' . $sorderelement . '</b></div>
                        <div style="line-height20px;"><label style="font-size:12px; width: 28%; font-weight:bold;">Dimensions:</label>' . $sorderdimesion . '</div>
                         <div style="line-height20px;"><label style="font-size:12px; width: 28%; font-weight:bold;">Material:</label>' . $sordermaterial . '</div>';
      $osDATE = $odquery->getCreatedDate();
      $message_body .= '<label style="font-size:12px; width: 28%; font-weight:bold;">Order Date:</label>' . $osDATE->format('Y-m-d');
    }

    $message_body .= '</td>
                    
                    <td style="width:10%; float:left;"></td>
                    <td style="width:15%; float:left;">';
    $message_body .= $odquery->getNoofElements() . '</td>
                    <td style="width:15%; float:left;">&euro;' . $odquery->getPrice() . '</td></tr>';
  }
  $message_body .= '<tr style="border:none;"><td style="width:15%; float:left;"></td>
                    <td style="width:45%; float:left;"></td>
                    <td style="width:10%; float:left;"></td>
                    <td style="width:15%; float:left;"><label>Total Price:</label></td>
                    <td style="width:15%; float:left;">&euro;' . round($totalorderPrice['SUMPrice'], 2) . '</td> </tr>';
  if ($orderdiscountscount != 0) {
    $message_body .= ' <tr style="border:none;"><td style="width:15%; float:left;"></td>
                    <td style="width:45%; float:left;"></td>
                    <td style="width:10%; float:left;"></td>
                    <td style="width:15%; float:left;"><label>' . $DiscountPrice . '</label></td>
                    <td style="width:15%; float:left;">&euro;' . $orderdiscountsPrice . '</td> </tr>
                  <tr style="border:none;"><td style="width:15%; float:left;"></td>
                    <td style="width:45%; float:left;"></td>
                    <td style="width:10%; float:left;"></td>
                    <td style="width:15%; float:left;"><label>' . $finalprice . '</label></td>
                    <td style="width:15%; float:left;">&euro;' . round($totalorderPrice['SUMPrice'], 2) - $orderdiscountsPrice . '</td> </tr>';
  }
  $message_body .= '</tbody> </table>
        </div>';

  $message_body;
// @FIXME Set this value to your email address.
  $site_email = variable_get('site_mail', '');
  $my_email = 'koteswararao.vuyyuru@gmail.com';
  $message_subject = $orderconfi;

  // These value can remain empty.
  $my_module = '';
  $my_mail_token = '';
  $from = 'koteswararao@swayaminfologic.com';

  $from = variable_get('system_mail', $my_email);
  $headers['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
  $message = array(
    'id' => $my_module . '_' . $my_mail_token,
    'to' => $my_email,
    'subject' => $message_subject,
    'body' => array($message_body),
    'headers' => array(
      'From' => $from,
      'Sender' => $from,
      'CC' => 'koteswararao@swayaminfologic.com, nagaraja.ch@swayaminfologic.com',
      'Return-Path' => $from,
    ),
  );
  $system = drupal_mail_system($my_module, $my_mail_token);

  // The format function must be called before calling the mail function. 
  $message = $system->format($message);

  if ($system->mail($message)) {
    drupal_set_message('_custom_simple_mail SUCCESS');
  } else {
    drupal_set_message('_custom_simple_mail FAILURE');
  }
}