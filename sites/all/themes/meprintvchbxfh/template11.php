<?php

/**
 * Override of theme_breadcrumb().
 */
function meprint_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' › ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Override or insert variables into the maintenance page template.
 */
function meprint_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // meprint_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  meprint_preprocess_html($vars);
}

/**
 * Override or insert variables into the html template.
 */
function meprint_preprocess_html(&$vars) {
  // Toggle fixed or fluid width.
  if (theme_get_setting('meprint_width') == 'fluid') {
    $vars['classes_array'][] = 'fluid-width';
  }
  // Add conditional CSS for IE6.
  drupal_add_css(path_to_theme() . '/fix-ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lt IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the html template.
 */
function meprint_process_html(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

/**
 * Override or insert variables into the page template.
 */
function meprint_preprocess_page(&$vars, $hook) {
  // Move secondary tabs into a separate variable.
  $vars['tabs2'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );
  unset($vars['tabs']['#secondary']);

  if (isset($vars['main_menu'])) {
    $vars['primary_nav'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('links', 'inline', 'main-menu'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['primary_nav'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_nav'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'inline', 'secondary-menu'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['secondary_nav'] = FALSE;
  }

  // Prepare header.
  $site_fields = array();
  if (!empty($vars['site_name'])) {
    $site_fields[] = $vars['site_name'];
  }
  if (!empty($vars['site_slogan'])) {
    $site_fields[] = $vars['site_slogan'];
  }
  $vars['site_title'] = implode(' ', $site_fields);
  if (!empty($site_fields)) {
    $site_fields[0] = '<span>' . $site_fields[0] . '</span>';
  }
  $vars['site_html'] = implode(' ', $site_fields);

  // Set a variable for the site name title and logo alt attributes text.
  $slogan_text = $vars['site_slogan'];
  $site_name_text = $vars['site_name'];
  $vars['site_name_and_slogan'] = $site_name_text . ' ' . $slogan_text;
  
  if (isset($vars['node'])) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "page--my-machine-name.tpl.php".
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
 if(isset($vars['node']->nid)){
  if($vars['node']->nid == '18'){
         $vars['theme_hook_suggestions'][] = 'page__product';
       
   }
    if($vars['node']->nid == '28'){
         $vars['theme_hook_suggestions'][] = 'page__product';
       
   }
    if($vars['node']->nid == '29'){
         $vars['theme_hook_suggestions'][] = 'page__product';
       
   }
   if($vars['node']->nid == '20'){
         $vars['theme_hook_suggestions'][] = 'page__search';
       
   }
   
   if($vars['node']->nid == '41'){
         $vars['theme_hook_suggestions'][] = 'page__cart';
       
   }
   if($vars['node']->nid == '46'){
         $vars['theme_hook_suggestions'][] = 'page__orderconfirm';
       
   }
    if($vars['node']->nid == '48'){
         $vars['theme_hook_suggestions'][] = 'page__shippingdetails';
       
   }
    if($vars['node']->nid == '49'){
         $vars['theme_hook_suggestions'][] = 'page__editcart';
       
   }
    if($vars['node']->nid == '50'){
         $vars['theme_hook_suggestions'][] = 'page__carteditsuccess';
       
   }
    if($vars['node']->nid == '51'){
         $vars['theme_hook_suggestions'][] = 'page__editwishlist';
       
   }
   if($vars['node']->nid == '52'){
         $vars['theme_hook_suggestions'][] = 'page__wishlistsuccess';
       
   }
    if($vars['node']->nid == '62'){
         $vars['theme_hook_suggestions'][] = 'page__wishlistsuccess';
       
   }
   
   
  }
  
}

/**
 * Override or insert variables into the node template.
 */
function meprint_preprocess_node(&$vars) {
  $vars['submitted'] = $vars['date'] . ' — ' . $vars['name'];
}

/**
 * Override or insert variables into the comment template.
 */
function meprint_preprocess_comment(&$vars) {
  $vars['submitted'] = $vars['created'] . ' — ' . $vars['author'];
}

/**
 * Override or insert variables into the block template.
 */
function meprint_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'title';
  $vars['classes_array'][] = 'clearfix';
}

/**
 * Override or insert variables into the page template.
 */
function meprint_process_page(&$vars) {
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Override or insert variables into the region template.
 */
function meprint_preprocess_region(&$vars) {
  if ($vars['region'] == 'header') {
    $vars['classes_array'][] = 'clearfix';
  }
}


/************************ Three Column Big Format *************/ 
function productthreecolum($id, $type){
global $base_url;
global $language ;

include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
    //$prod_dets = explode("&",$id);
    //$product_id = $prod_dets[0];
    //$prod_frmt_det = $prod_dets[1];
    //$prod_dets = explode("=",$prod_frmt_det);
   // $product_format = $prod_dets[1];

    $lang_name = $language->language ;
    $result = MP\FormoneLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->find();
    $bigresult = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->find();
    $result_info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();$product_material = MP\FormoneMaterialsQuery::create()->filterByFormId($id)->find();
    $extra_processing = MP\FormoneExtraProcessingQuery::create()->filterByFormId($id)->find();	
    $formone_dimensions = MP\FormoneDimensionsQuery::create()->filterByFormId($id)->find();
    $formone_accessories = MP\FormoneAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();
    
    $product_material = MP\FormoneMaterialsQuery::create()->filterByFormId($id)->find();
    $cost_copies = MP\FormoneCostCopiesQuery::create()->filterByFormId($id)->find();             
          
             
            
            
            
    $mystr="";
 
    foreach($result as $precord){
       
    foreach($bigresult as $precords){
	 
         $mystr.='<input type ="hidden" value ="'.$precords->getAdditionalPriceForCopy().'"  id = "additonal_price_copy">';
         $mystr.='<input type ="hidden" value ="'.$precords->getDeterminationCostCopies().'" id = "det_cost_copy">';
         $mystr.='<input type ="hidden" value ="'.$precords->getBasePriceForQuantity().'" id = "base_price">';
         
         $addtnl_prc_cpy = $precords->getAdditionalPriceForCopy();
         $det_cst = $precords->getDeterminationCostCopies();
         $free_dimesions = $precords->getFreeDimensionsAvailable();
         if($det_cst != 1){
             foreach($cost_copies as $crecord){ 
                       $cc .= $crecord->getFromCost()." ".$crecord->getToCost()." ".$crecord->getPrice()."+"; 
             }
             $mystr.='<input type ="hidden" value ="'.$cc.'" id = "costCopies">';
             
             
         }
    
	    foreach($result_info as $info){
		
                       $image = $info->getImage();
                       $uuu=file_load($image)->uri;
			$my_image=explode("://",$uuu);
                        $mystr.='<input type ="hidden" value ="'.$info->getPromoFlag().'" id = "promotionalFlag">';
                        $mystr.='<input type ="hidden" value ="'.$info->getPromoPriceType().'" id = "promotionalType">';
                        $mystr.='<input type ="hidden" value ="'.$info->getPromotionPrice().'" id = "promotionalValue">';
                         
                     
            }
           
            
            ?>
            <!--Tittle -->
            <form method="POST" enctype="multipart/form-data" action="<?php echo base_path().$lang_name;?>/cart">
                
                <input type="hidden" name="productid" value="<?php echo $id; ?>">
            <h3>&nbsp <?php print($precord->getName()); ?> &nbsp <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image"></span></h3>
             <div class="space-3"></div>
             <!--Image-->
             <?php 
           
}

 ?> 
    	<img alt="" src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="550px" height="235px" alt="Product-Image">
      
        <div class="col-sm-12 col-md-12 ">
        <div ><h4 class="order-h"><?php echo $productdesc; ?></h4></div>
            <?php echo $precord->getDescription();'<br>'; ?>
        </div>
    <div class="space-3"></div>
  
   <div class="col-sm-12 col-md-12 ">
      
    <div class="row">
     
      <div class="order-view-hd"><?php echo $ordertitle; ?> </div>
                <div class="order-view-top">
                 
                     
                    <div class="form-group col-md-5 col-sm-5">
                      <h4 class="order-h"><?php echo $quanitity; ?></h4>

                      <input type="text" name="quanitity" class="form-control " id="productQuanity" placeholder="1" min="1">

                    </div>
		<!-- Template download for cart -->
                    <div class="form-group col-md-7 col-sm-7">
                         <h4 class="order-h"><?php echo $download; ?></h4>
                          <div class="space-3"></div>
                       <?php  
                       foreach($result_info as $template) {  
                           $tId =  $template->getTemplateId();
                          // $tId = 2;
                           $TemplateImage = MP\TemplatesQuery::create()->filterByTemplateId($tId)->find(); 
                           if(count($TemplateImage)>0) {
                           foreach($TemplateImage as $value) { 
                            $image1  = $value->getFile();
                            $uuu=file_load($image1)->uri;
                            $templateImage=explode("://",$uuu);  
           ?>
                         <span class="downloadable " src="<?php echo $base_url.'/sites/default/files/'.$templateImage[1];?>">Download</span>
                         <!--<img alt="" src="<?php echo $base_url.'/sites/default/files/'.$templateImage[1];?>" width="50px" height="50" class="downloadable">-->
          
                <?php
                           } 
                           }                      
                           
                       }                        
                       ?>
                    </div>
                    
                    <!--end  Template download for cart -->
                  
                  
                </div>
         
                <div class="order-view-top">
                        <div class="space-3 col-md-12"></div>
                          <div class="form-group">
                              <div class="col-md-12 col-sm-12"> 
                               <h4 class="order-h"><?php echo $dimension;?></h4></div>
                             <?php                              
                             foreach($formone_dimensions as $formonedimensions){
                                
                                 $dimensionsName = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->find();
                                 $first = "true";
                                 foreach($dimensionsName as $dname){  
                                     ?>
                         <div class="form-group col-md-5 col-sm-5">                            
                             <input type="radio" name="dimensions" id="dimensions" <?php if($first == "true"){ ?> Checked ="checked" <?php } ?> value="<?php echo $dname->getDimensionId(); ?> ">&nbsp<?php echo $dname->getWidth().'*'. $dname->getHeight().'<br>'; 
   $mystr.='<input type ="hidden" value ="'.$dname->getWidth().'*'. $dname->getHeight().'" id = "bd'.$dname->getDimensionId().'">';?> 
                            <?php $first = "false" ?>
                            </div>           
                        <?php } ?>  
                             <?php } ?>
                            <div class="clearfix"></div>
                            <?php if($free_dimesions == 1){ ?>
                             <div class="form-group ">  
                           <div class="col-md-12 col-sm-12">             
                                <h4 class="order-h">  <input type="radio" name="dimensions" id="dimensions"  value="custom"> 
                                <?php echo $custdimension; ?></h4>
                            </div>
                             </div> 
                           
                            <div class="clearfix"></div>
                            <div id="cstdisp">
                            <div class="col-md-5 col-sm-5">
							 <div class="col-md-12 col-sm-12">             
                                <h4 class="order-h"><?php echo $height; ?></h4>
                            </div>
                              <div class="col-md-11 col-sm-11">							 
                                <input type="text" name="productHeight" class="form-control" id="productHeight" placeholder="1">
                              </div>
                              X </div>
                            <div class="col-md-5 col-sm-5">
							<div class="col-md-12 col-sm-12">             
                                <h4 class="order-h"><?php echo $width; ?></h4>
                            </div>
                              <div class="col-md-10 col-sm-10">							  
                                <input type="text" name="productWidth" class="form-control " id="productWidth" placeholder="">
                              </div>
                            cm </div></div> <?php } ?>
                          </div>
                   </div>
    
                    <div class="order-view-top">
                     
                       <div class="form-group col-md-5 col-sm-5">
                         <h4 class="order-h"><?php echo $material."Material"; ?></h4>
                         <select class="form-control" id="materialId" name="materialId">
                             <?php                              
                            
                             foreach($product_material as $material){
                                
                                  $big_material = $material->getMaterialId();                                 
                                 $getMaterial = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->find();
                                 foreach($getMaterial as $material){ ?>
                             <option value ="<?php echo $material->getMaterialId(); ?>"><?php echo $material->getName().'<br>'; ?></option>             
                        <?php $materials_price_values = MP\MaterialsQuery::create()->filterByMaterialId($material->getMaterialId())->find();
                                foreach($materials_price_values as $mat_price){
                                $mystr.='<input type ="hidden" value ="'.$mat_price->getPrice().'+'.$mat_price->getMinSize().'+'.$mat_price->getShortSideMinLength().'+'.$mat_price->getLongSideMinLength().'" id = "mtid'.$material->getMaterialId().'">';
                                } } ?>  
                        <?php }?>
                         </select>
                       </div>
                    
                    </div>
      
                                   
                  
     <div class="order-view-top">
          
              <div class="form-group col-md-12 col-sm-12">
               <h4 class="order-h"><?php echo $processing; ?></h4>
               <div class="form-group col-md-12 col-sm-12">
                   <?php foreach($bigresult as $precord){
				   <input type="hidden"  name="productgroupid" value="<?php echo $precord->getProductgroupId(); ?>">
                       $basepocess_id = $precord->getProcessingId();
                          $product_processing = MP\ProcessingLangQuery::create()->filterByProcessingId($precord->getProcessingId())->filterByLanguageId($lang_name)->find();
			  $precessvalues = MP\ProcessingQuery::create()->filterByProcessingId($precord->getProcessingId())->find();			 
                           foreach($product_processing as $base_processing){?> 
				<?php foreach($precessvalues as $values){
                                    // $basepocess_id = $values->getProcessingId();
                                     $pocess_baseprice = $values->getBasePrice();
                                     $pocess_fucntionid = $values->getPriceCalcFunctionId();
                                     $min_sqm = $values->getMinOneSqm();
                                     $basedimminmax= $values->getIsDimensionMinMaxRequired();
                                     $ssmin_length = $values->getShortSideMinLength();
                                     $lsmin_height = $values->getLongSideMinLength();
                                     $extrafieldreq = $values->getIsExtrafieldRequired();
                                     $extrafieldname = $values->getExtraFieldName();
                                     $baseisExtra = $values->getIsExtra();
                                     $baseisExtraTop = $values->getExtraProcessingTop();
                                     $baseisExtraRight = $values->getExtraProcessingRight();
                                     $baseisExtraLeft = $values->getExtraProcessingLeft();
                                     $baseisExtraBottom = $values->getExtraProcessingBottom();       
                                     ?>
                         <input type="radio" name="baseprocessing" id="baseprocessing" checked="checked" value="<?php echo $basepocess_id; ?>" style="display:none">&nbsp  <?php // echo $base_processing->getName(); 
                         // to insert hidden variables
                         $mystr.='<input type ="hidden" value ="'.$pocess_baseprice.'+'.$pocess_fucntionid.'+'.$min_sqm.'+'.$ssmin_length.'+'.$lsmin_height.'+'.$basedimminmax.'" id = "bsprcs'.$basepocess_id.'">';
                         ?>
                        <?php if($extrafieldreq == 1){?>
                         <div class="col-md-5 col-sm-5">
                            <h4 class="order-h"><?php echo $base_processing->getExtraFieldName(); ?></h4>
                            <input type="text" name="extadefValue" id="extradefValue">
                            </div>
                       <?php } ?>
                             <div class="col-md-5 col-sm-5">
                            <?php if($baseisExtra == 1): ?>
                            
                                <?php if($baseisExtraTop == 1): ?> 
                                 <input type="checkbox" name="baseisExtraTop" id="baseisExtraTop"  value="1">&nbsp<?php echo $ExtraProcessingTop.'<br>'; ?> 
                                 <?php endif; ?> 
                                 <?php if($baseisExtraRight == 1): ?> 
                                 <input type="checkbox" name="baseisExtraRight" id="baseisExtraRight" value="1">&nbsp<?php echo $ExtraProcessingRight.'<br>'; ?> 
                                 <?php endif; ?> 
                                 <?php if($baseisExtraLeft == 1): ?> 
                                 <input type="checkbox" name="baseisExtraLeft" id="baseisExtraLeft" value="1">&nbsp<?php echo $ExtraProcessingLeft.'<br>'; ?> 
                                 <?php endif; ?> 
                                <?php if($baseisExtraBottom == 1): ?> 
                                 <input type="checkbox" name="baseisExtraBottom" id="baseisExtraBottom" value="1">&nbsp<?php echo $ExtraProcessingBottom.'<br>'; ?>
                                 <?php endif; ?> 
                             <?php endif; ?>
                           
                           </div> </div> 
                         <?php }
                          } 
                }?>
               
               
               <?php         
                                 foreach($extra_processing as $eprocessing){ 
				$pocess_id = $eprocessing->getProcessingId();					
                   $processingName = MP\ProcessingLangQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->filterByLanguageId($lang_name)->find();
                   
                       $precessvals = MP\ProcessingQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->find();     
                     foreach($processingName as $ename){
                     //$baseprocessing_id = $precord->getProcessingId(); 
                         foreach($precessvals as $valus){
                             //$pocess_id = $values->getProcessingId();
                             $pocess_baseprice = $valus->getBasePrice();
                             $pocess_fucntionid = $valus->getPriceCalcFunctionId();
                             $min_sqm = $valus->getMinOneSqm();
                             $dimminmax= $valus->getIsDimensionMinMaxRequired();
                             $ssmin_length = $valus->getShortSideMinLength();
                             $lsmin_height = $valus->getLongSideMinLength();
                             $extrafieldreq = $valus->getIsExtrafieldRequired();
                             $extrafieldname = $valus->getExtraFieldName();
                             $isExtra = $valus->getIsExtra();
                             $isExtraTop = $valus->getExtraProcessingTop();
                             $isExtraRight = $valus->getExtraProcessingRight();
                             $isExtraLeft = $valus->getExtraProcessingLeft();
                             $isExtraBottom = $valus->getExtraProcessingBottom();       
                             ?>
 
                            <div class="form-group col-md-12 col-sm-12">
                               <h4 class="order-h"><?php echo $extraprocessing; ?></h4>
                                </div>
                         <div class="form-group col-md-12 col-sm-12"><div class="form-group col-md-5 col-sm-5">  
                             <input type="checkbox" name="extraprocessing" class ="extraprocessing_class" id="extraprocessing<?php echo $pocess_id; ?>" class="chkclass" value="<?php echo $pocess_id; ?>">&nbsp<?php echo $ename->getName().'<br>'; ?> </div>           
                             <?php
                           // to insert hidden variables
                         $mystr.='<input type ="hidden" value ="'.$pocess_baseprice.'+'.$pocess_fucntionid.'+'.$min_sqm.'+'.$ssmin_length.'+'.$lsmin_height.'+'.$dimminmax.'" id = "extprcs'.$pocess_id.'">';
                         ?>
                             </div>
                             <div class="form-group col-md-12 col-sm-12">
                             <div class="extinfodiv">
                             <?php
                         if($extrafieldreq == 1){?>
                             <div class="form-group col-md-5 col-sm-5">    
                            <div id="epextradefdiv<?php echo $pocess_id;?>" style="display:none;"> 
                            <h4 class="order-h"><?php echo $ename->getExtraFieldName(); ?></h4>
                            <input type="text" name="epextadefValue" id="epextradefValue<?php echo $pocess_id; ?>">
                            </div>
                                 </div>
                       <?php } $extdefdividlst .= $pocess_id."+"?>	  
                      <?php if($isExtra == 1): ?>
                             <div id="epextraprcsdiv<?php echo $pocess_id;?>" style="display:none;"> 
                            <div class="form-group col-md-5 col-sm-5">
                                <?php if($isExtraTop == 1): ?> 
                                 <input type="checkbox" name="isextratop" id="isextratop"  value="">&nbsp<?php echo $ExtraProcessingTop.'<br>'; ?> 
                                 <?php endif; ?> 
                                 <?php if($isExtraRight == 1): ?> 
                                 <input type="checkbox" name="isextraright" id="isextraright" value="">&nbsp<?php echo $ExtraProcessingRight.'<br>'; ?> 
                                 <?php endif; ?> 
                                 <?php if($isExtraLeft == 1): ?> 
                                 <input type="checkbox" name="isextraleft" id="isextraleft" value="">&nbsp<?php echo $ExtraProcessingLeft.'<br>'; ?> 
                                 <?php endif; ?> 
                                <?php if($isExtraBottom == 1): ?> 
                                 <input type="checkbox" name="isextrabottom" id="isextrabottom" value="">&nbsp<?php echo $ExtraProcessingBottom.'<br>'; ?>
                                 <?php endif; ?> 
                            </div>
                            </div>     
                             <?php endif; ?>
                         </div>
                         
                         </div> <?php  }
                        } ?>  
                        <?php }$mystr.='<input type ="hidden" value ="'.$extdefdividlst.'" id="extdefdividlst">';?>
               
               </div>
               
          
      </div>
      
      <div class="order-view-top"> 
                         <div class="form-group col-md-12 col-sm-12">              
                        <h4 class="order-h"><?php echo $accessorie; ?></h4>
                       <!-- <input type="radio" name="accessories" id="accessories"  value="0" checked="checked" style="display:none">-->
                         <?php    foreach($bigresult as $faccessories){  
                              $accId = $faccessories->getAccessoriesId();
                              $accessoryvals = MP\AccessoriesQuery::create()->filterByAccessoryId($accId)->find();  
                              $accessoriesName = MP\AccessoriesLangQuery::create()->filterByAccessoryId($faccessories->getAccessoriesId())->filterByLanguageId($lang_name)->find();
                                 foreach($accessoryvals as $accval){ 
                                     $baseaccprice = $accval->getPrice();
                                     $baseaccid = $accval->getAccessoryId();
                                 foreach($accessoriesName as $aname){ 
                                 ?>
                         <div class="form-group col-md-5 col-sm-5">  
                             <input type="checkbox" name="baseaccessories" id="baseaccessories" checked="checked" value="<?php echo $accId;?>"> &nbsp<?php echo $aname->getName().'<br>'; ?> 
                         <?php 
                         $mystr.='<input type ="hidden" value ="'.$baseaccprice.'" id="baseacc'.$accId.'">';
                         ?>
                         </div>           
                        <?php } ?> 
                        <?php } ?>  
                        <?php }
                        foreach($formone_accessories as $accsrysitms){
                          $accessoryvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->find(); 
                             $accessoriesName = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->find();
                                  foreach($accessoryvls as $eaccvls){
                                     $eaccprice = $eaccvls->getPrice();
                                     $eaccid = $accsrysitms->getAccessoryId();
                                     foreach($accessoriesName as $aname)
                                         { 
                                   ?>
                          <div class="form-group col-md-5 col-sm-5">  
                            <input type="checkbox" class ="extraaccessories_class" name="extraaccessories" id="extraaccessories" value="<?php echo $eaccid;  ?>">&nbsp<?php echo $aname->getName(); ?> </div>  
                   <?php 
                         $mystr.='<input type ="hidden" value ="'.$eaccprice.'" id="extacc'.$eaccid.'">';
                     }
                        }
                        }
                        ?>
                         
                       </div>
                </div>
				 
      <!-- file upload  for cart -->
      <div class="order-view-top"> 
           <div class="form-group col-md-12 col-sm-12">  
              <!--<div class="dvPreview"></div>-->
              <?php foreach($bigresult as $number) {  
                  $files = $number->getNumberOfFilesTobeUploaded(); 
                  if($files !=0) { for($i=1;$i<=$files;$i++) { ?>
              <div>
                  <input type="hidden"  name="imagecount" value="<?php echo $number->getNumberOfFilesTobeUploaded();  ?>" />
                    <div class="dvPreview_<?php echo $i;?>"></div>
                    <input type="file" name="image_<?php echo $i; ?>" class="ImageUpload"/></div>
              <div class='clearfix'></div>
                    
                      
                <?php
                       } ?>
              <div>
                  <input type="checkbox" name="operatorreview" id ="operatorreview" value="1"/><?php echo $operatorreview; ?>
              </div>
               
               <?php
                      
                  }
                   
                  
              } ?>
           </div>
       </div>         
           
       <!-- End file upload  for cart -->
                        <div class="order-view-top">
                          <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingtype; ?></h4>
                          <div class="col-md-12 col-sm-12">
                            <div class="shipzone"> <span>
                              <li class="fa  fa-truck fa-4x"></li>
                              </span>
                              <p class="dym-p">Regular<!--<span class="shipzone-span">Thursday &euro;16</span>--></p>
                              <input type="radio" name="shipping" id="shipping" value="0" style="display:none;" checked="checked"> 
                              <input type="radio" name="shipping" id="shipping" value="10"> &euro;10
                            </div>
                            <div class="shipzone"> <span>
                              <li class="fa  fa-truck fa-4x"></li>
                              </span>
                              <p class="dym-p">Express<!--<span class="shipzone-span">Thursday &euro;16</span>--></p>
                              <input type="radio" name="shipping" id="shipping" value="12"> &euro;12
                            </div>
                            <div class="shipzone"> <span>
                              <li class="fa  fa-truck fa-4x"></li>
                              </span>
                              <p class="dym-p">Fedex<!--<span class="shipzone-span">Thursday &euro;16</span>--></p>
                              <input type="radio" name="shipping" id="shipping" value="14"> &euro;14 
                            </div>      
                            <div class="shipzone"> <span>
                              <li class="fa  fa-plane fa-4x"></li>
                              </span>
                              <p class="dym-p">DHl<!--<span class="shipzone-span">Thursday &euro;16</span>--></p>
                              <input type="radio" name="shipping" id="shipping" value="20"> &euro;20 
                            </div>            
                          </div>
                          <div class="space-3 col-md-12 col-sm-12"></div>
                         <div class="col-md-11 col-sm-11 text-left">
                        <p> <?php echo $shipdesc; ?> </p>
                         </div>
                    </div>
         </div>
     
    </div>
   </div> 
   <div class="clearfix"></div>
   <div class="space-2 col-sm-12 col-md-12"></div>
   
   
  <div class="space-2 col-sm-12 col-md-12"></div>

   <div class="col-sm-12 col-md-12" id = "pricing">
        	<div class="order-view-hd"><?php echo $summery; ?></div>
            <div class="panel panel-default">
              
              <div class="table-responsive">
                                <table id="sample-table-1" class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th class="left"><?php echo $product; ?></th>
                                      <th><?php echo $qty; ?></th>
                                      <th></th>
                                      <th></th>
                                      <th><?php echo $price; ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td class="left"> <strong><?php echo $costpfprintingandmaterial; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"> <div class="fa fa-eur" id="printigmaterialdiv"> </div></td>
                                    </tr>
                                    <tr>
                                     <td class="left"><strong><?php echo $processcost; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"> <div class="fa fa-eur" id="adtnlcostdiv"> </div></td>
                                    </tr>
                                  
                                   
                                    <tr id="baseaccessoriestd">
                                     <td class="left"><strong><?php echo "Accesory Cost"; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"> <div class="fa fa-eur" id="baseacccstdiv"> </div></td>
                                    </tr>
                                  
                                    
                                   <tr>
                                      <td class="left"><strong><?php echo $singleproductcost; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <?php //$tot_prc = $matrl_fst_prc+$addtnl_prc_cpy;?>
                                      <td class="hidden-480"><div class="fa fa-eur" id="singleproductcostdiv"> </div></td>
                                    </tr>
                                    
                                    <tr id="extraprocessingtd">
                                      <td class="left"><strong><?php echo $extraprocessingcost; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <?php //$tot_prc = $matrl_fst_prc+$addtnl_prc_cpy;?>
                                      <td class="hidden-480"><div class="fa fa-eur" id="extraprocessingdiv"> </div></td>
                                    </tr>
                                    
                                    <tr id="promopricetd">
                                      <td class="left"><strong><?php echo $beforpromoprice; ?></strong></td>
                                      <td><div class="totalnoofproductbigfrmtdiv"></div></td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <?php //$tot_prc = $matrl_fst_prc+$addtnl_prc_cpy;?>
                                      <td class="hidden-480"><div style='text-decoration:line-through' class="fa fa-eur" id="promopricediv"> </div></td>
                                    </tr>
                                    
                                    <tr>
                                      <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
                                      <td><div class="totalproductcostdivqnty"></div></td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                    <td class="hidden-480"><div class="fa fa-eur" id="totalproductcostdiv">
                                          </div></td>
                                      
                                    </tr>                                    
                                  </tbody>
                                </table>
                              </div>
                       <input type="hidden" id="bigtotlprice" name="bigtotlprice" value="">   
                       <input type="hidden"  name="type" value="Big">  
            </div>
          <div class="col-md-12 col-sm-12">
    	<div class="col-md-4 col-sm-4"><button class="btn" type="submit" name="wishlist" value="wishlist" >Add to Wishlist </button></div>
                <div class="col-md-5 col-sm-5"></div>
                <div class="col-md-3 col-sm-6">
                    <button class="btn " type="submit" name="proceed" value="Proceed">Proceed </button></div>
                
        </div>

            
        </div>
            </form>
   
       
   
   <div class="clearfix"></div>
   <div class="col-md-12 col-sm-12"></div>
      <section class="row">
      <div  class="col-md-12 col-sm-12">
    <?php $subgroup = $precord->getSubcategoryId();
    
        $result = MP\FormoneBigFormatQuery::create()->filterBySubcategoryId($precord->getSubcategoryId())->orderByFormId('desc')->limit(3)->find();
        foreach($result as $res){
          $related_product = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($res->getFormid())->find();
          foreach($related_product as $relproduct){
              $id = $relproduct->getFormId();
             //$name = $res->getName();
             $image = $relproduct->getImage();
			 $uuu=file_load($image)->uri;
             $my_image=explode("://",$uuu);
             //$bestprice = $relproduct->getBestPriceFlag();
             $promo= $relproduct->getPromoFlag();
             $price = $res->getBasePriceForQuantity();
             //$pdate = $relproduct->getCreatedDate();
             ?>
   
   <div class="col-sm-4 col-md-4 ">
        <div class=" products-Thumbnail">
          <div class="item item-type-line"> <a class="item-hover" href="<?php echo $base_url.'/product-three-column?'.$id.'&type=Big';?>" >
            <div class="item-info">
              <div class="headline">L'adesivo murale � un film lucido per uso interno utilizzato per la decorazione di pareti.
                </div>
              <div class="line"></div>
              <div class="date"><?php //echo $pdate; ?></div>
            </div>
            <div class="mask"></div>
            </a>
            <div class="item-img"><img src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="125px" height="125px" /></div>
          </div>
		    <?php if($promo == 1){ ?>
          <div class="products-title"><em>Promo</em></div>
		  <?php } ?>
                    <?php if($promo == 'Best Price'){ ?>
					 <div class="products-title1"><em>Best price</em></div>
					 <?php } ?>
          <div class="clr"></div>
          <p><?php echo $name; ?> </p>
          <p class="p">&euro; <?php echo $price; ?></p>
        </div>
      </div>             
     
   <?php            
          }
        }?>
         </div>
      </section>
        
         <?php }
    return $mystr;
    }

/************************ Three Column Big Format *************/ 

	//Samll Format Start 

/************************ Three Column Small Format *************/ 
function productthreecolumSmall($id, $type){
    $sProduct = $id;
    global $base_url;
    global $language ;
    $lang_name = $language->language;
    include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
    $result = MP\FormtwoLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->find();
    $smallresult = MP\FormtwoSmallFormatQuery::create()->filterByFormId($id)->find();
    $product_material = MP\FormtwoMaterialsQuery::create()->filterByFormId($id)->find();
    $formtwo_dimensions = MP\FormtwoDimensionsQuery::create()->filterByFormId($id)->find();
    $result_info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();
    $smallextra_processing = MP\FormtwoExtraProcessingQuery::create()->filterByFormId($id)->find();
    
    $smallpriceget = MP\FormtwoBasePriceQuery::create()->filterByFormId($id)->find();
     

    $mystr="";
 foreach($result as $precord){
	    foreach($result_info as $info){
		$imguri = $info->getImage();		
		$uuu=file_load($imguri)->uri;
		 $my_image=explode("://",$uuu);
                 } ?>

            
            
            <h3>&nbsp; <?php echo $precord->getName(); ?> &nbsp; <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="ProductImage"></span></h3>
            <div class="space-3"></div>
             <!--Image-->
    	<img src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="550px" height="235px" alt="Product-Image">
         <div class="col-sm-12 col-md-12 ">
              <div >             
                <h4 class="order-h"><?php echo $productdesc; ?> </h4>
               </div>
            <?php 
         echo $precord->getDescription();'<br>';  
	?>
         </div>
    <div class="space-3"></div>
	  <form method="POST" enctype="multipart/form-data" action="<?php echo $base_url.'/'.$lang_name;?>/cart">
<div class="col-sm-12 col-md-12 ">
    <div class="row">
             <div class="order-view-hd"><?php echo $ordertitle; ?></div>
                <div class="order-view-top">
                 

                    <div class="form-group col-md-5 col-sm-5">
                      <h4 class="order-h"><?php echo $quanitity; ?></h4>
                      <input type="text" class="form-control" id="sproductQuanity" name="squanitity" placeholder="1" min="1" value="1">
                    </div>
                    <!-- Template download for cart -->
                    <div class="form-group col-md-7 col-sm-7">
                         <h4 class="order-h"><?php echo $download; ?></h4>
                          <div class="space-3"></div>
                       <?php  
                       foreach($result_info as $template) {  
                           $tId =  $template->getTemplateId();
                          // $tId = 2;
                           $TemplateImage = MP\TemplatesQuery::create()->filterByTemplateId($tId)->find(); 
                           if(count($TemplateImage)>0) {
                           foreach($TemplateImage as $value) { 
                            $image1  = $value->getFile();
                            $uuu=file_load($image1)->uri;
                            $templateImage=explode("://",$uuu);  
           ?>
                         <span class="downloadable " src="<?php echo $base_url.'/sites/default/files/'.$templateImage[1];?>">Download</span>
                         <!--<img alt="" src="<?php echo $base_url.'/sites/default/files/'.$templateImage[1];?>" width="50px" height="50" class="downloadable">-->
          
                <?php
                           } 
                           }                      
                           
                       }                        
                       ?>
                    </div>
                    
                    <!--end  Template download for cart -->
                 
                </div>
          
                <div class="order-view-top">
                              <div class="space-3 col-md-12"></div>
                                <div class="form-group ">
                                  <div class="col-md-12 col-sm-12">             
                                      <h4 class="order-h"><?php echo $dimension; ?></h4>
                                  </div>
                                    <?php 
                                    $first = TRUE;
                                   foreach($formtwo_dimensions as $formonedimensions){

                                       $dimensionsName = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->find();
                                       
                                       foreach($dimensionsName as $dname){ 
                                           
                                          
                                           ?>
                               <div class="form-group col-md-5 col-sm-5">  
                                   <?php  ?>
                                   <input type="radio" name="sdimensions" id="sdimensions" value="<?php echo $dname->getDimensionId() ?>" <?php if ($first): ?>checked="checked" <?php endif; ?> >&nbsp<?php echo $dname->getWidth().'*'. $dname->getHeight(); ?> 
                                   <?php 
                                   $first = FALSE;
                                    ?>
                                   </div>           
                              <?php  } ?>  
                                   <?php } ?>
                                 </div>
                         </div>
          
                        <div class="order-view-top">
                                   
                                     <div class="form-group col-md-5 col-sm-5">
                                       <h4 class="order-h"><?php echo $tmaterial; ?> </h4>
                                       <select class="form-control" id="smaterialId" name="smaterialId">
                                           <?php 
                                           foreach($product_material as $material){
                                            
                                               $smallmaterial = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->find();
                                               foreach($smallmaterial as $smaterial){ 
                                                   $first = TRUE;
                                                   if($first){
                                                   $matid = $smaterial->getMaterialId();
                                                   } $first = FALSE;
                                               ?>
                                           <option value ="<?php echo $smaterial->getMaterialId(); ?>" ><?php echo $smaterial->getName().'<br>'; ?></option>             
                                      <?php } ?>  
                                      <?php }$mystr.='<input type ="hidden" value ="'.$id.'" id = "formid">';?>
                                       </select>
                                     </div>
                                   
                        </div>
          
                            <div class="order-view-top">
                                          
                                         <div class="form-group col-md-12 col-sm-12">
                                           <h4 class="order-h"><?php echo $processing; ?> </h4>
                                           <?php 
                                            
                                             foreach($smallresult as $smallprocessing){
                                                 $baseprice = $smallprocessing->getBasePriceForQuantity();
                                                 
                         $mystr.='<input type ="hidden" value ="'.$baseprice.'" id = "basePricesmall">';
                                           $baseprocessing_id = $smallprocessing->getProcessingId();
                                           $product_processing = MP\ProcessingLangQuery::create()->filterByProcessingId($smallprocessing->getProcessingId())->filterByLanguageId($lang_name)->find();
                                          foreach($product_processing as $base_processing){ 
                                              $first = TRUE;
                                              if($first){ $prcssid =$base_processing->getProcessingId(); }
                                                  ?>
                                           <div class="col-md-5 col-sm-5">
                                           <input type="radio" name="processing" id="sprocessing" checked="checked" value="<?php echo $base_processing->getProcessingId(); ?>">&nbsp<?php  echo $base_processing->getName();  
                                             } $fist = FALSE; ?><?php } ?></div>
                                          <?php 
                                              foreach($smallextra_processing as $seprocessing){
                                              $processingName = MP\processingLangQuery::create()->filterByProcessingId($seprocessing->getProcessingId())->filterByLanguageId($lang_name)->find();
                                              foreach($processingName as $ename){ ?>
                                            <div class="col-md-5 col-sm-5">
                                               <input type="radio" name="processing" id="sprocessing"  value="<?php echo $ename->getProcessingId(); ?> ">&nbsp<?php echo $ename->getName().'<br>'; ?> </div>           
                                          <?php } ?>  
                                          <?php }?>
                                         </div>
                                       
                            </div>
             <!-- file upload  for cart -->
      <div class="order-view-top"> 
           <div class="form-group col-md-12 col-sm-12">  
              <!--<div class="dvPreview"></div>-->
              <?php foreach($smallresult as $number) {  
                  $sfiles = $number->getNumberOfFilesTobeUploaded();
                  if($sfiles !=0) { for($i=1;$i<=$sfiles;$i++) { ?>
              <div>
                  <input type="hidden"  name="simagecount" value="<?php echo $number->getNumberOfFilesTobeUploaded(); ?>" />
                    <div class="dvPreview_<?php echo $i;?>"></div>
                    <input type="file" name="image_<?php echo $i; ?>" class="ImageUpload"/></div>
              <div class='clearfix'></div>
                    
                      
                <?php
                      
                      
                      
                  } ?>
               
               <?php
                      
                  }
                   
                  
              } ?>
           </div>
       </div>         
           
       <!-- End file upload  for cart -->
                <div class="clearfix"></div>  
                        <div class="order-view-top">
                                        <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingtype; ?></h4>
                                        <div class="col-md-12 col-sm-12">
                                          <div class="shipzone"> <span>
                                            <li class="fa  fa-truck fa-4x"></li>
                                            </span>
                                            <p class="dym-p">example shipping <span class="shipzone-span">Thursday &euro;16</span></p>
                                            <input type="radio" name="sshipping" id="shipping" value="15"> &euro;15,39
                                          </div>
                                          <div class="shipzone"> <span>
                                            <li class="fa  fa-truck fa-4x"></li>
                                            </span>
                                            <p class="dym-p">example shipping <span class="shipzone-span">Thursday &euro;16</span></p>
                                            <input type="radio" name="sshipping" id="shipping" value="16"> &euro;15,39
                                          </div>
                                          <div class="shipzone"> <span>
                                            <li class="fa  fa-truck fa-4x"></li>
                                            </span>
                                            <p class="dym-p">example shipping <span class="shipzone-span">Thursday &euro;16</span></p>
                                            <input type="radio" name="sshipping" id="shipping" value="17"> &euro;15,39 
                                          </div>      
                                          <div class="shipzone"> <span>
                                            <li class="fa  fa-plane fa-4x"></li>
                                            </span>
                                            <p class="dym-p">example shipping <span class="shipzone-span">Thursday &euro;16</span></p>
                                            <input type="radio" name="sshipping" id="shipping" value="18"> &euro;15,39 
                                          </div>            
                                        </div>
                                        <div class="space-3 col-md-12 col-sm-12"></div>
                                       <div class="col-md-11 col-sm-11 text-left">
                                      <p> <?php echo $shipdesc; ?></p>
                                       </div>
                        </div>
            </div>
        </div>
    <?php
      $smffstlst = MP\FormtwoBasePriceQuery::create()->filterByFormId($id)
          ->useFormtwoBasePriceMaterialsQuery('a')
          ->filterByMaterialId($matid) 
          ->orderByMaterialId()  
          ->endUse()
          ->useFormtwoBasePriceProcessingQuery('b')
          ->filterByProcessingId($prcssid)
          ->orderByProcessingId()  
          ->endUse()
          ->select(array('PriceId','FormId','DimensionId','NoCopies','a.MaterialId', 'a.Price','b.ProcessingId','b.Price'))
          ->orderByNoCopies()
           ->limit(1)   
          ->find();
      
  foreach ($smffstlst as $lstsmf){
      $smatprice = $lstsmf['a.Price'];
      $procesprice = $lstsmf['b.Price'];
  }
      $sproductqty = 1;
      $smatprice = round($smatprice, 2);
      $sprocesprice = round($procesprice, 2);
      $ssingleproductcost = $smatprice + $procesprice;
      $ssingleproductcost = round($ssingleproductcost, 2);
      
     // echo $matprice."<br/>".$procesprice."vishal";
  ?>
    
                <div class="clearfix"></div>
                <div class="space-2 col-sm-12 col-md-12"></div>
                        <div class="col-sm-12 col-md-12">
                                     <div class="order-view-hd"><?php echo $summery; ?></div>
                                 <div class="panel panel-default">
                                 <div class="table-responsive">
                                <table id="sample-table-1" class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th class="left"><?php echo $product; ?></th>
                                      <th><?php echo $qty; ?></th>
                                      <th></th>
                                      <th></th>
                                      <th><?php echo $price; ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td class="left"> <strong><?php echo "Cost of Printing and Materials" ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"> <div class="fa fa-eur" id="sprintigmaterialdiv"><?php //echo $smatprice; ?> </div></td>
                                    </tr>
                                    <tr>
                                     <td class="left"><strong><?php echo $processcost; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"> <div class="fa fa-eur" id="sadtnlcostdiv"><?php //echo $sprocesprice; ?> </div></td>
                                    </tr>
                                    <tr>
                                      <td class="left"><strong><?php echo $singleproductcost; ?></strong></td>
                                      <td>1</td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <?php //$tot_prc = $matrl_fst_prc+$addtnl_prc_cpy;?>
                                      <td class="hidden-480"><div class="fa fa-eur" id="ssingleproductcostdiv"> <?php //echo $ssingleproductcost; ?></div></td>

                                    </tr>
                                    <tr>
                                      <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
                                      <td><div id="stotalproductcostdivqnty"><?php //echo $sproductqty; ?></div></td>
                                      <td><!--<i class="fa fa-pencil fa-1x grid-fa-pencil"></i>--></td>
                                      <td><!--<i class="fa fa fa-trash-o fa-1x"></i>--></td>
                                      <td class="hidden-480"><div class="fa fa-eur" id="stotalproductcostdiv"><?php //echo $ssingleproductcost; ?></div></td>

                                    </tr>                                    
                                  </tbody>
                                </table>
                              </div>
                                   <input type="hidden" id="smalltotlprice" name="smalltotlprice" value=""> 
									<input type="hidden" name="sproductgroupid"  value="0"> 
                                 <input type="hidden" name="getId"  value="<?php echo $id ?>"> 
                                 <input type="hidden" name="type"  value="small">  
                                 </div>
                                 <div class="col-md-12 col-sm-12">
                                     <div class="col-md-4 col-sm-4">
                                       <button class="btn " type="submit" name="wishlist" value="wishlist">Add to Wishlist </button>
                                     
                                     </div>
                                     <div class="col-md-5 col-sm-5"></div>
                                     <div class="col-md-3 col-sm-3"><button class="btn btn-success" name="sbutton" type="submit" value="sbutton">Proceed</button></div>
                                 </div>

                             </div>
		 </form>

            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12"></div>
                <section class="row">
                <div  class="col-md-12 col-sm-12">

                  <?php  
                  
                     foreach($smallresult as $precord){
                        
                      $relresult = MP\FormtwoSmallFormatQuery::create()->filterBySubcategoryId($precord->getSubcategoryId())->orderByFormId('desc')->limit(3)->find();
                      foreach($relresult as $res){
                        $related_product = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($res->getFormid())->find();
                        foreach($related_product as $relproduct){
                           //$name = $res->getName();
                           $image = $relproduct->getImage();
                                        $uuu=file_load($image)->uri;
                           $my_image=explode("://",$uuu);
                           //$bestprice = $relproduct->getBestPriceFlag();
                           $promo= $relproduct->getPromoFlag();
                           $price = $res->getBasePriceForQuantity();
                           //$pdate = $relproduct->getCreatedDate();
                           
                           ?>


                       <div  class="col-md-4 col-sm-4">
                      <div class=" products-Thumbnail">
                        <div class="item item-type-line"> <a class="item-hover" href="#" target="_blank">
                          <div class="item-info">
                            <div class="headline">L'adesivo murale � un film lucido per uso interno utilizzato per la decorazione di pareti.</div>
                            <div class="line"></div>
                            <div class="date"><?php //echo $pdate; ?></div>
                          </div>
                          <div class="mask"></div>
                          </a>
                          <div class="item-img"><img src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="125px" height="125px" /></div>
                        </div>
                        <div class="products-title"><em>Promo</em></div>
                        <div class="clr"></div>
                        <p><?php echo $name; ?> </p>
                        <p class="p">&euro; <?php echo $price; ?></p>
                      </div>

                  </div>
                 <?php   
                        }
                      } 

                   ?>
                      </div>
                     </section> 
                <?php }
 }
            return $mystr;
            }
       
   
            
function cartBigIteams() { 
     
       global $base_path;
       global $base_url;
       global $language;
      $lang_name = $language->language ;
     
       session_start();
       $_SESSION['session_id'] = session_id();
   
      if(isset($_POST['proceed']) && $_POST['proceed']!='') { 
          
         if(count($_POST) != 0) {                  
                
                $purl = explode("?",$_SERVER['HTTP_REFERER']);
                $url = explode("?",$_SERVER["REQUEST_URI"]);
                $purl1 = explode("&type=",$purl['1']);
                $id = $purl1[0];
                $type = $purl1[1];
                $count = $_POST['imagecount'];
                for($i=1;$i<=$count;$i++) {    
                 
                    $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/' . $_FILES['image_'.$i]["name"];
                    move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                
                }
                $cart = new MP\CartItems();
                $cart->setSessionId(session_id());
                $cart->setProductType("Big");
                $cart->setProductId(isset($_POST['productid'])?$_POST['productid']:'');
				$cart->setProductgroupId(isset($_POST['productgroupid'])?$_POST['productgroupid']:'');				
                $cart->setAccessoryId(isset($_POST['baseaccessories'])?$_POST['baseaccessories']:'');
                if(isset($_POST['dimensions']) && $_POST['dimensions'] == "custom" ) {
                $cart->setWidth(isset($_POST['productWidth'])?$_POST['productWidth']:'');
                $cart->setHeight(isset($_POST['productHeight'])?$_POST['productHeight']:'');
                } else  if(isset($_POST['dimensions']) && $_POST['dimensions'] != "custom" ) {
                     $cart->setDimensionId(isset($_POST['dimensions'])?$_POST['dimensions']:'');
                }
                $cart->setMaterialId(isset($_POST['materialId'])?$_POST['materialId']:'');
                $cart->setProcessingId(isset($_POST['baseprocessing'])?$_POST['baseprocessing']:'');
             
                $cart->setShipping(isset($_POST['shipping'])?$_POST['shipping']:'');
                if($_POST['quanitity'] == "") { 
                    $cart->setQuanitity(1); 
                } else {
                     $cart->setQuanitity(isset($_POST['quanitity'])?$_POST['quanitity']:''); 
                }
                //extadefValue
                $cart->setExtraProcessingId(isset($_POST['extadefValue'])?$_POST['extadefValue']:''); 
                $cart->setExtraProcessingTop(isset($_POST['baseisExtraTop'])?$_POST['baseisExtraTop']:''); 
                $cart->setExtraProcessingRight(isset($_POST['baseisExtraRight'])?$_POST['baseisExtraRight']:''); 
                $cart->setExtraProcessingLeft(isset($_POST['baseisExtraLeft'])?$_POST['baseisExtraLeft']:''); 
                $cart->setExtraProcessingBottom(isset($_POST['baseisExtraBottom'])?$_POST['baseisExtraBottom']:''); 
                $cart->setPrice(isset($_POST['bigtotlprice'])?$_POST['bigtotlprice']:'');
                $cart->setCreatedDate(time());
		$cart->setModifiedDate(time());
                $cart->save(); 
                $idsss = $cart->getCartId();
                
                /******** cart extra processing start************ */
                if(isset($_POST['extraprocessing']) && $_POST['extraprocessing'] !='') {
                   
                    $cartExtra = new MP\CartExtraProcessing();
                    $cartExtra->setCartId($idsss);
                    $cartExtra->setProcessingId($_POST['extraprocessing']);
                    $cartExtra->setCreatedDate(time());
		    $cartExtra->setModifiedDate(time()); 
                   $cartExtra->save(); 
                    
                }
                
                /******** cart extra processing end************ */
                
                
                 /******** cart images start************ */
                for($i=1;$i<=$count;$i++) {   
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setCreatedDate(time());
		        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 }
                 
                  
          
             
            
                unset($_POST);
         } 
         
      }    
                  $sessionid = $_SESSION['session_id'];
                  global $language ;
                  $lang_name = $language->language ;
                  $count = MP\CartItemsQuery::create()
                                            ->where('CartItems.SessionId =?', $sessionid)
                                            ->count();
                 
                  if($count >0){                        
                     $cartQuery = MP\CartItemsQuery::create()
                                 ->where('CartItems.SessionId =?', $sessionid)
                                 ->orderByCartId('desc');
                  ?>
            
            <?php
                $i =1;
                  foreach($cartQuery as $result) {  
                    if($result->getProductType() == 'Big'){
                  $pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                  
                  $returnquery = MP\FormoneBigFormatQuery::create()->filterByFormId($result->getProductId())->find();
                    foreach($pquery as $presult){                         
				 $mml = $presult->getName();			
                    }	foreach($returnquery as $rquery){

						$url = $base_url.'/productgridlist?'.$rquery->getSubcategoryId();
                           
                        }
                    }else  if($result->getProductType() == 'small'){
                       $pquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                       $returnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($result->getProductId())->find();
                    foreach($pquery as $presult){ 
				 $mml = 	$presult->getName();	
                    }
					foreach($returnquery as $rquery){
						$url = $base_url.'/smallformat?'.$rquery->getSubcategoryId();
						
                    }
					}
                  ?> 
                    <div class="col-md-12 col-sm-12 cart_<?php echo $result->getCartId()?>">
                    <div class="col-md-12 col-sm-12 product_name">
                    <div class="col-md-10 col-sm-10 ">
                        <b><?php echo $mml; ?></b>
                    </div>
                        <?php  $editurl = $base_url.'/'.$lang_name.'/editcart?'.$result->getCartId(); ?>
                     <div class="col-md-1 col-sm-1 edit_icon">
                        <a href="<?php echo $editurl; ?>">
                            <i class="icon-edit-sign"></i></a>
                    </div>
                    <div class="col-md-1 col-sm-1  delete_icon">
                         <input type="hidden" name="path" value="<?php echo $base_url;  ?>/sites/all/themes/meprint/cart.php">
                        <a  class="deletecart" url="<?php echo $result->getCartId(); ?>">
                            <i class="icon-remove-sign"></i></a>
                    </div>
                    </div>
                          <div class="space-2"></div>
                   <!--<div class="col-md-12 col-sm-12 "> <div class="col-md-4 col-sm-4 ">
                        Offer :
                    </div> <div class="col-md-6 col-sm-6 ">
                        Details :
                    </div></div>-->
                   <div class="col-md-12 col-sm-12 ">
                        <div class="col-md-8 col-sm-8 ">
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        No of Copies :
                    </div><div class="col-md-4 col-sm-4 ">
                        <?php echo $result->getQuanitity() ?>
                    </div></div>
                    <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Dimensions :
                    </div><div class="col-md-4 col-sm-4 ">
                       <?php 
                       // echo $result->getDimensionId();
                        if($result->getDimensionId() !=0) {
                        $dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
                         foreach($dquery as $mquery){
                             echo $mquery->getWidth() .'*'.$mquery->getHeight();
                         } 
                  } else {
                      echo $result->getWidth()."*".$result->getHeight();
                  }
                         ?>
                        
                    </div></div>
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Material :
                    </div><div class="col-md-4 col-sm-4 ">
                         <?php 
                         $cquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
                         foreach($cquery as $mquery){
                             echo $mquery->getName();
                         }
                         ?>
                    </div></div>
                     <!--<div class="col-md-12 col-sm-12 "><div class="col-md-4 col-sm-4 ">
                        Accessories :
                    </div><div class="col-md-6 col-sm-6 ">
                        Details :
                    </div></div>-->
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Weight:
                    </div><div class="col-md-4 col-sm-4 ">
                        -NA-
                    </div></div>
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Cart date :
                    </div><div class="col-md-4 col-sm-4 ">
                        <?php 
                      $cDATE  = $result->getCreatedDate();
                      echo  $cDATE->format('Y-m-d');
                      
                     
                        
                        ?>
                    </div></div></div>
                     
                     <div class="col-md-4 col-sm-4 "
                         <?php   
                         $IMAGES = MP\CartImagesQuery::create()
                        ->withColumn('Max(Id)')
                        ->where('CartImages.CartId =?', $result->getCartId())
                         ->select(array('image'))
                        ->findOne();
                         
                        // echo $IMAGES['image'];
                   
                     $imageurl = $base_path."/sites/default/files/cart/".$IMAGES['image'];?>
                         <?php if($IMAGES['image'] !='') { ?>
                         <img src="<?php echo $imageurl; ?>" width="150" heigth="150" />
                         <?php } ?>
                     </div></div>
                        <div class="space-2"></div>
                        <div class="col-md-12 col-sm-12 cart_price"><b><div class="col-md-10 col-sm-10 ">Total Price :
                    </div><div class="col-md-2 col-sm-2 ">
                        <?php 
                        $totlprc = $result->getPrice();
                        echo '&euro;'.number_format((float)$totlprc, 2, '.', '');
                                ?>
                    </div></b></div>
                         <div class="space-2"></div>
                     </div>
                    
                 
                 
                <?php  $i++;  } ?>
               
            <div class="space-2"></div>
               
         
           <input type="hidden" name="type"  value="<?php echo $type; ?>"> 
           <input type="hidden" id="URLValue" name="URLVALUE" value="<?php echo $base_url; ?>" />
           <div class="col-md-3 col-sm-6 continue"><a href="<?php echo $url; ?>"><button class="btn "> Continue shopping</button></a></div>
             
            <div class="col-md-3 col-sm-6 payment"><a href="<?php echo $base_url.'/discount'; ?>"><button class="btn " type="submit" >Proceed to Payment</button></a></div>
             
      <?php }else{
          
          echo '<h4> No products available in your cart.</h4>';
      } 
      
  
  }
  
 function whishlist() {
     global $user;
     global $base_url;
     global $base_path;
     global $language ;
     $lang_name = $language->language;
     if($user->uid==0) {
         $purl = $_SERVER['HTTP_REFERER'];
      
         ?>
    <h3>Please Login</h3>
          
     <?php 
     
     header('Refresh: 2; url='.$purl);
    
     } else {
                session_start(); 
                $purl = explode("?",$_SERVER['HTTP_REFERER']);
                $url = explode("?",$_SERVER["REQUEST_URI"]);
                $purl1 = explode("&type=",$purl['1']);
                $id = $purl1[0];
                $type = $purl1[1];
              
                if(isset($_POST['wishlist']) && $_POST['wishlist'] !='') { 
                if($type == 'Big') { 
                     $count = $_POST['imagecount'];
                     for($i=1;$i<=$count;$i++) {    
                            $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/wishlist/' . $_FILES['image_'.$i]["name"];
                            move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                
                    } 

                $wishlistinsert = new MP\WishlistItems();
		$wishlistinsert->setUserId($user->uid); 
		$wishlistinsert->setProductType('Big');
                if($_POST['dimensions'] == "custom") {
                $wishlistinsert->setWidth(isset($_POST['productWidth'])?$_POST['productWidth']:'');
                $wishlistinsert->setHeight(isset($_POST['productHeight'])?$_POST['productHeight']:'');
                } else {
                    $wishlistinsert->setDimensionId(isset($_POST['dimensions'])?$_POST['dimensions']:'');
                }
                $wishlistinsert->setProductId(isset($_POST['productid'])?$_POST['productid']:'');
                $wishlistinsert->setProductgroupId(isset($_POST['productgroupid'])?$_POST['productgroupid']:'');
                $wishlistinsert->setAccessoryId(isset($_POST['baseaccessories'])?$_POST['baseaccessories']:'');
                $wishlistinsert->setMaterialId(isset($_POST['materialId'])?$_POST['materialId']:'');
                $wishlistinsert->setProcessingId(isset($_POST['baseprocessing'])?$_POST['baseprocessing']:'');
                $wishlistinsert->setShipping(isset($_POST['shipping'])?$_POST['shipping']:'');
                if($_POST['quanitity'] == "") { 
                    $wishlistinsert->setQuanitity(1);  
                } else {
                     $wishlistinsert->setQuanitity(isset($_POST['quanitity'])?$_POST['quanitity']:'');
                }
                $wishlistinsert->setPrice(isset($_POST['bigtotlprice'])?$_POST['bigtotlprice']:'');
                $wishlistinsert->setCreatedDate(time());
		$wishlistinsert->setModifiedDate(time());
                $wishlistinsert->setExtraDefId(isset($_POST['extadefValue'])?$_POST['extadefValue']:'');
                $wishlistinsert->setExtraProcessingTop(isset($_POST['baseisExtraTop'])?$_POST['baseisExtraTop']:''); 
                $wishlistinsert->setExtraProcessingRight(isset($_POST['baseisExtraRight'])?$_POST['baseisExtraRight']:''); 
                $wishlistinsert->setExtraProcessingLeft(isset($_POST['baseisExtraLeft'])?$_POST['baseisExtraLeft']:''); 
                $wishlistinsert->setExtraProcessingBottom(isset($_POST['baseisExtraBottom'])?$_POST['baseisExtraBottom']:''); 
                $wishlistinsert->save(); 
                $whishlist_id = $wishlistinsert->getId();
                
              /***** wishlist images start **********/

                 for($i=1;$i<=$count;$i++) {  
                     if($_FILES['image_'.$i]["name"] !='') { 
                        $wishlistimage = new MP\WishlistImages();
                        $wishlistimage->setUserId($user->uid);
                        $wishlistimage->setImage($_FILES['image_'.$i]["name"]);
                        $wishlistimage->setStatus(1);
                        $wishlistimage->setWishlistId($whishlist_id);
                        $wishlistimage->setCreatedDate(time());
		        $wishlistimage->setModifiedDate(time());
                        $wishlistimage->save(); 
                     }
                 } 
                  /***** wishlist images end **********/
                  /***** wishlist extra processing start **********/
                 if(isset($_POST['extraprocessing']) && $_POST['extraprocessing'] !='') { 
                        $wishlistextra= new MP\WishlistExtraProcessing();
                        $wishlistextra->setWishlistId($whishlist_id);
                        $wishlistextra->setProcessingId($_POST['extraprocessing']);
                        $wishlistextra->setCreatedDate(time());
		        $wishlistextra->setModifiedDate(time());
                        $wishlistextra->save(); 
                     
                 }
                   /***** wishlist extra processing end **********/
                
                 /** end with big format   ***/
                } else if($type == 'small'){  
                    
                $count = $_POST['simagecount'];
                for($i=1;$i<=$count;$i++) {    
                 
                    $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/wishlist/smallformat/' . $_FILES['image_'.$i]["name"];
                    move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                
                } 
                $wishlistinsert = new MP\WishlistItems();
		$wishlistinsert->setUserId($user->uid);
		$wishlistinsert->setProductType('small');
                $wishlistinsert->setProductId($_POST['getId']);
                  $wishlistinsert->setProductgroupId($_POST['sproductgroupid']);
                $wishlistinsert->setDimensionId(isset($_POST['sdimensions'])?$_POST['sdimensions']:'');
                $wishlistinsert->setMaterialId(isset($_POST['smaterialId'])?$_POST['smaterialId']:'');
                $wishlistinsert->setProcessingId(isset($_POST['processing'])?$_POST['processing']:'');
                if($_POST['squanitity'] == "") { 
                    $wishlistinsert->setQuanitity(1); // 
                } else {
                     $wishlistinsert->setQuanitity(isset($_POST['squanitity'])?$_POST['squanitity']:'');
                }
                $wishlistinsert->setShipping(isset($_POST['sshipping'])?$_POST['sshipping']:'');
                $wishlistinsert->setPrice(isset($_POST['smalltotlprice'])?$_POST['smalltotlprice']:'');
                
                 $wishlistinsert->setCreatedDate(time());
		 $wishlistinsert->setModifiedDate(time());
                 $wishlistinsert->save(); 
                 $whishlist_id = $wishlistinsert->getId();
               
                 for($i=1;$i<=$count;$i++) {  
                     if($_FILES['image_'.$i]["name"] !='') { 
                        $wishlistimage = new MP\WishlistImages();
                        $wishlistimage->setWishlistId($whishlist_id);
                        $wishlistimage->setUserId($user->uid);
                        $wishlistimage->setImage($_FILES['image_'.$i]["name"]);
                        $wishlistimage->setStatus(1);
                        $wishlistimage->setCreatedDate(time());
		        $wishlistimage->setModifiedDate(time());
                        $wishlistimage->save(); 
                     }
                 }     
                } 
                }
             $wishurl = $base_url.'/'.$lang_name.'/mywishlist';
              header('Refresh: 2; url='.$wishurl);
    
        
        
     }
 } 
  
  
function orderconfirm() {
               global $base_url;  
               global $user;
               session_start();
              
               $_SESSION['session_id'];
               $cartItems = MP\CartItemsQuery::create()
	                    ->where('CartItems.SessionId =?', $_SESSION['session_id'])
	                    ->find();
              $totalPrice = MP\CartItemsQuery::create()
                        ->withColumn('SUM(Price)')
                        ->where('CartItems.SessionId =?', $_SESSION['session_id'])
                        ->groupBySessionId()
                        ->select(array('price'))
                        ->findOne();//$totalPrice['SUMPrice']
              $cartElements= MP\CartItemsQuery::create()
	                    ->where('CartItems.SessionId =?', $_SESSION['session_id'])
	                    ->find()
                            ->count(); //count 
               
                if($user->uid !=0) {
                    
            
              
              
             /*************** order table start **************/
               $order = new MP\Orders();
               $order->setUserId($user->uid);
               $order->setUserName($user->name);
               $order->setOrderDate(date("Y-m-d"));
               $order->setGrandtotalPrice($totalPrice['SUMPrice']);
               $order->setPayOption('Credit Card');
               $order->setPayStatus('Payment made');
               $order->setPartialCompleteShip($_POST['shipping']);
               $order->setNoofProducts($cartElements);
               $order->setNotes('desc');
               $order->setAnonymousPackage(1); 
               $order->setOrderStatus(1); 
               $order->setCreatedDate(time());
               $order->setModifiedDate(time());
               $order->save(); 
               $orderId = $order->getOrderId();
               
               //grandtotal_price
                 MP\OrdersQuery::create()->filterByGrandtotalPrice(0)->delete();
               
               /*************** order table end **************/
               
               /***** order discount start *****/
                $orderdiscounts = new MP\OrderDiscounts(); 
                $orderdiscounts->setDiscountId(isset($_POST['discount_id'])?$_POST['discount_id']:'');//
                //
                //discount_id
                $orderdiscounts->setCouponCode(isset($_POST['coupon_code'])?$_POST['coupon_code']:'');//
                //coupon_code
                $orderdiscounts->setUsedOnUserid($user->uid);//
                $orderdiscounts->setUsedOnOrderid($orderId);//
                $orderdiscounts->setUsedIndate(time());
                //productprice
                //dicountamount
                $orderdiscounts->setActualPrice(isset($_POST['productprice'])?$_POST['productprice']:'');
                $orderdiscounts->setDiscountPrice(isset($_POST['dicountamount'])?$_POST['dicountamount']:'');
                $orderdiscounts->setCreatedDate(time());
                $orderdiscounts->setModifiedDate(time());
                $orderdiscounts->save(); 
               
                /***** order discount end *****/
               
               
               /*************** order shipping table start **************/
               
                $shipping = new MP\OrderShippingDetails();
		$shipping->setUserId($user->uid);
                $shipping->setOrderId($orderId);
                
		$shipping->setName(isset($_POST['fname'])?$_POST['fname']:'');
                $shipping->setEmail(isset($_POST['email'])?$_POST['email']:'');
                $shipping->setPhone(isset($_POST['phone'])?$_POST['phone']:'');
                $shipping->setAddress(isset($_POST['saddress'])?$_POST['saddress']:'');
                $shipping->setLocation(isset($_POST['slocation'])?$_POST['slocation']:'');
                $shipping->setZipcode(isset($_POST['szipcode'])?$_POST['szipcode']:'');
                $shipping->setProvinance(isset($_POST['sprovience'])?$_POST['sprovience']:'');
                $shipping->setCountry(isset($_POST['scountry'])?$_POST['scountry']:'');
                $shipping->setCreatedDate(time());
                $shipping->setModifiedDate(time());
                $shipping->save();   
               /*************** order shipping table end **************/ 
                
                
                  /************** Production Start date **************/
				date_default_timezone_set('Asia/Kolkata');
                $today =date("Y-m-d");				
                 $todaytime = date("H");
		$kdate = date("l",strtotime($today));				
                if($todaytime >= 12) {  
					if(($kdate == 'Friday') || ($kdate == 'Saturday') || ($kdate =='Sunday')){
						$nextmonday = strtotime("next Monday" , strtotime($today));
						$startdate = date("Y-m-d", $nextmonday);
                    }
					else{
                        $productiondate = strtotime("+1 day" , strtotime($today));
                        $startdate = date("Y-m-d", $productiondate);
					}
                }
				else if($todaytime < 12){				
                    if(($kdate == 'Saturday') || ($kdate =='Sunday')){					
						$nextmonday = strtotime("next Monday" , strtotime($today));
						$startdate = date("Y-m-d", $nextmonday);
                    }
					else{
                         $startdate =date("Y-m-d");
                    }
                }
               
		/********** End Production start date ************/

                
                
                /*************Order Elements start **************/
                
                foreach($cartItems as $values) {  
                    
                     $elements = new MP\OrderElements();
                     $elements->setUserId($user->uid);
                     $elements->setOrderId($orderId);
                     $elements->setProductionStatusId(1);
                     $elements->setNotes('desc');
		     $elements->setNoofElements($values->getQuanitity());
                     $elements->setAnonymousPackaging(1);
                     $elements->setPrice($values->getPrice());
                     $elements->setWidth($values->getWidth());
                     $elements->setHeight($values->getHeight());
                     $elements->setProductionStartDate($startdate);
                     $elements->setFormId($values->getProductId());
                     $elements->setFormType($values->getProductType());
                     $elements->setMaterialId($values->getMaterialId());
                     $elements->setProcessingId($values->getProcessingId());
                     $elements->setAcessoryId($values->getAccessoryId());
                     $elements->setAnonymousPackaging(1);
                     $elements->setCreatedDate(time());
                     $elements->setModifiedDate(time());
                     $elements->save(); 
                     $EID = $elements->getOrderElementId();
                     
                     $cartImagesN = MP\CartImagesQuery::create()
	                    ->where('CartImages.CartId =?', $values->getCartId())
	                    ->find();
                     
                     foreach($cartImagesN as $hj){    
                         $elementfiles = new MP\OrderElementsFiles(); 
                         $elementfiles->setOrderElementId($EID);
                         $elementfiles->setFilename($hj->getImage());
                         $elementfiles->setFileStatus(1);
                         $elementfiles->setCreatedDate(time());
                         $elementfiles->setModifiedDate(time());
                         $elementfiles->save();
                         //order_elements_files
                         
                     }
                     
                     
                   }
                
                 /*************Order Elements end **************/
                    
                    foreach($cartItems as $delete) { 
                       
                        $DELETEIMAGES = MP\CartImagesQuery::create()->filterByCartId($delete->getCartId())->delete();
                        $DELETEEXTRAPROCESSING = MP\CartExtraProcessingQuery::create()->filterByCartId($delete->getCartId())->delete();
                        $DELETECARTITEMS = MP\CartItemsQuery::create()->filterByCartId($delete->getCartId())->delete();
                    }
                       
             
                } else if($user->uid == 0) {  
                     $query = "SELECT max(uid) as idds FROM users";
                     $project = db_query($query)
                                ->fetchObject();
            
                 
                  
                     $name = isset($_POST['fname'])?$_POST['fname']:'';
                     $mail = isset($_POST['email'])?$_POST['email']:'';
                     $phone = isset($_POST['phone'])?$_POST['phone']:'';
                     $address = isset($_POST['address'])?$_POST['address']:'';
                     $zipcode = isset($_POST['zipcode'])?$_POST['zipcode']:'';
                     $provience = isset($_POST['provience'])?$_POST['provience']:'';
                     $location = isset($_POST['location'])?$_POST['location']:'';
                     $insertQuery = db_insert('users')
                    ->fields(array(
                    'uid' => $project->idds+1,
                    'name' => $name,
                    'mail' => $mail,
                    'status' =>1,
                       
                    ));

                    $d = $insertQuery->execute();
                    $LastQuery = "SELECT max(uid) as latest FROM users";
                    $lastRecord = db_query($LastQuery)
                                ->fetchObject();
                    $field_data_field_phone = db_insert('field_data_field_phone')
                                              ->fields(array(
                                                 'entity_type' =>'user',
                                                   'bundle' =>'user',
                                                  'entity_id' => $lastRecord->latest,
                                                   'revision_id' => $lastRecord->latest,
                                                  'delta' => 0,
                                                  'field_phone_value' => $phone
                                                  
                                                  
                                                  
                                              ));
                      
                    $field_data_field_phone->execute();//ok
                    
                     $add = db_insert('field_data_field_address')
                                              ->fields(array(
                                                 'entity_type' =>'user',
                                                   'bundle' =>'user',
                                                  'entity_id' => $lastRecord->latest,
                                                  'revision_id' => $lastRecord->latest,
                                                  'delta' => 0,
                                                  'field_address_value' => $address
                                                  
                                                  
                                                  
                                              ));
                      
                    $add->execute(); //not 
                    
                      $k = db_insert('field_data_field_zip_code')
                                              ->fields(array(
                                                 'entity_type' =>'user',
                                                   'bundle' =>'user',
                                                  'entity_id' => $lastRecord->latest,
                                                   'revision_id' => $lastRecord->latest,
                                                  'delta' => 0,
                                                  'field_zip_code_value'=> $zipcode
                                                  
                                                  
                                                  
                                              ));
                      $k->execute();
                      
                   
                    
                    
                    
                     $field_data_field_province = db_insert('field_data_field_province')
                                              ->fields(array(
                                                 'entity_type' =>'user',
                                                   'bundle' =>'user',
                                                  'entity_id' => $lastRecord->latest,
                                                    'revision_id' => $lastRecord->latest,
                                                  'delta' => 0,
                                                  'field_province_value' => $provience
                                                  
                                                  
                                                  
                                              ));
                      
                    $field_data_field_province->execute();
                    
                    
                      $field_data_field_location = db_insert('field_data_field_location')
                                              ->fields(array(
                                                 'entity_type' => 'user',
                                                   'bundle' => 'user',
                                                  'entity_id' => $lastRecord->latest,
                                                    'revision_id' => $lastRecord->latest,
                                                  'delta' => 0,
                                                  'field_location_value' => $location
                                                  
                                                  
                                                  
                                              )); //ok
                      
                $field_data_field_location->execute(); 
                  
                  /*************** order table start **************/
               $order = new MP\Orders();
               $order->setUserId($lastRecord->latest);
               $order->setUserName(isset($_POST['fname'])?$_POST['fname']:'');
               $order->setOrderDate(date("Y-m-d"));
               $order->setGrandtotalPrice($totalPrice['SUMPrice']);
               $order->setPayOption('Credit Card');
               $order->setPayStatus('Payment made');
               $order->setNoofProducts($cartElements);
               $order->setPartialCompleteShip($_POST['shipping']);
               $order->setNotes('desc');
               $order->setAnonymousPackage(1); 
               $order->setOrderStatus(1); 
               $order->setCreatedDate(time());
               $order->setModifiedDate(time());
               $order->save(); 
               $orderId = $order->getOrderId();
               
               
                 MP\OrdersQuery::create()->filterByGrandtotalPrice(0)->delete();
               /*************** order table end **************/
               
                 /***** order discount start *****/
                $orderdiscounts = new MP\OrderDiscounts(); 
                $orderdiscounts->setDiscountId($_POST['discount_id']);//
                $orderdiscounts->setCouponCode($_POST['coupon_code']);//
                $orderdiscounts->setUsedOnUserid($lastRecord->latest);//
                $orderdiscounts->setUsedOnOrderid($orderId);//
                $orderdiscounts->setUsedIndate(time());
                $orderdiscounts->setActualPrice($_POST['productprice']);
                $orderdiscounts->setDiscountPrice($_POST['dicountamount']);
                $orderdiscounts->setCreatedDate(time());
                $orderdiscounts->setModifiedDate(time());
                $orderdiscounts->save(); 
                /***** order discount end *****/
               /*************** order shipping table start **************/
               
                $shipping = new MP\OrderShippingDetails();
		$shipping->setUserId($lastRecord->latest);
                $shipping->setOrderId($orderId);
                
		$shipping->setName(isset($_POST['fname'])?$_POST['fname']:'');
                $shipping->setEmail(isset($_POST['email'])?$_POST['email']:'');
                $shipping->setPhone(isset($_POST['phone'])?$_POST['phone']:'');
                $shipping->setAddress(isset($_POST['saddress'])?$_POST['saddress']:'');
                $shipping->setLocation(isset($_POST['slocation'])?$_POST['slocation']:'');
                $shipping->setZipcode(isset($_POST['szipcode'])?$_POST['szipcode']:'');
                $shipping->setProvinance(isset($_POST['sprovience'])?$_POST['sprovience']:'');
                $shipping->setCountry(isset($_POST['scountry'])?$_POST['scountry']:'');
                $shipping->setCreatedDate(time());
                $shipping->setModifiedDate(time());
                $shipping->save();   
               /*************** order shipping table end **************/
                
                
                foreach($cartItems as $values) {  
                    
                     $elements = new MP\OrderElements();
                     $elements->setUserId($lastRecord->latest);
                     //$elements->setUserName($lastRecord->latest);
                     //user_name
                     $elements->setOrderId($orderId);
                     $elements->setProductionStatusId(1);
                     $elements->setNotes('desc');
                     $elements->setAnonymousPackaging(1);
                     $elements->setPrice($values->getPrice());
					 $elements->setNoofElements($values->getQuanitity());
                     $elements->setWidth($values->getWidth());
                     $elements->setHeight($values->getHeight());
                     $elements->setProductionStartDate(date("Y-m-d"));
                     $elements->setFormId($values->getProductId());
                     $elements->setFormType($values->getProductType());
                     $elements->setMaterialId($values->getMaterialId());
                     $elements->setProcessingId($values->getProcessingId());
                     $elements->setAcessoryId($values->getAccessoryId());
                     $elements->setAnonymousPackaging(1);
                     $elements->setCreatedDate(time());
                     $elements->setModifiedDate(time());
                     $elements->save(); 
                     $EID = $elements->getOrderElementId();
                     $cartImagesN = MP\CartImagesQuery::create()
	                    ->where('CartImages.CartId =?', $values->getCartId())
	                    ->find();
                     /****** order element files start ************/
                     
                     foreach($cartImagesN as $hj) {    
                         $elementfiles = new MP\OrderElementsFiles(); 
                         $elementfiles->setOrderElementId($EID);
                         $elementfiles->setFilename($hj->getImage());
                         $elementfiles->setFileStatus(1);
                         $elementfiles->setCreatedDate(time());
                         $elementfiles->setModifiedDate(time());
                         $elementfiles->save();
                         //order_elements_files
                         
                     }
                      /****** order element files end ************/
                     
                   }
                   foreach($cartItems as $delete) { 
                       
                        $DELETEIMAGES = MP\CartImagesQuery::create()->filterByCartId($delete->getCartId())->delete();
                        $DELETEEXTRAPROCESSING = MP\CartExtraProcessingQuery::create()->filterByCartId($delete->getCartId())->delete();
                        $DELETECARTITEMS = MP\CartItemsQuery::create()->filterByCartId($delete->getCartId())->delete();
                    }
                
                }
                
             echo '<h4>Order sucessfully created</h4>';   

  
}    

function cartSmallIteams() {
    
   global $base_path;
   global $base_url;
   session_start();
  
   $_SESSION['session_id'] = session_id();
     if(isset($_POST['sbutton']) && $_POST['sbutton'] !='') {
      if(count($_POST) != 0) {  
          
           $purl = explode("?",$_SERVER['HTTP_REFERER']);
           $url = explode("?",$_SERVER["REQUEST_URI"]);
           $purl1 = explode("&type=",$purl['1']);
           $id = $purl1[0];
           $type = $purl1[1];
           $count = $_POST['simagecount'];
           
            for($i=1;$i<=$count;$i++) {    
                  
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);

            }
                 $cartsmall = new MP\CartItems();
                 $cartsmall->setSessionId(session_id());
                 $cartsmall->setProductType('small');
                 
                 $cartsmall->setProductId($_POST['getId']); 
				 $cartsmall->setProductgroupId($_POST['sproductgroupid']); 
                 $cartsmall->setDimensionId(isset($_POST['sdimensions'])?$_POST['sdimensions']:'');
                 $cartsmall->setMaterialId(isset($_POST['smaterialId'])?$_POST['smaterialId']:'');
                 $cartsmall->setProcessingId(isset($_POST['processing'])?$_POST['processing']:'');
                 $cartsmall->setShipping(isset($_POST['sshipping'])?$_POST['sshipping']:'');
                 if($_POST['squanitity'] == "") {
                       $cartsmall->setQuanitity(1); 
                 } else {
                       $cartsmall->setQuanitity(isset($_POST['squanitity'])?$_POST['squanitity']:''); 
                 }
               
                 $cartsmall->setPrice(isset($_POST['smalltotlprice'])?$_POST['smalltotlprice']:'');
                 $cartsmall->setCreatedDate(time());
		 $cartsmall->setModifiedDate(time());
                 $cartsmall->save(); 
                 $idsss =  $cartsmall->getCartId();
                   /******** cart extra processing start************ */
               /* if(isset($_POST['extraprocessing']) && $_POST['extraprocessing'] !='') {
                   
                    $cartExtra = new MP\CartExtraProcessing();
                    $cartExtra->setCartId($idsss);
                    $cartExtra->getProcessingId($_POST['extraprocessing']);
                    $cartExtra->setCreatedDate(time());
		    $cartExtra->setModifiedDate(time()); 
                   $cartExtra->save(); 
                    
                } */
                
                /******** cart extra processing end************ */
                
                
                 /******** cart images start************ */
                for($i=1;$i<=$count;$i++) {   
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setCreatedDate(time());
		        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 } //small format images

               
      }      
      }
   
   
   
                 $sessionid = $_SESSION['session_id'];
                  global $language ;
                  $lang_name = $language->language ;
                  $countS = MP\CartItemsQuery::create()
                                            ->where('CartItems.SessionId =?', $sessionid)
                                            ->count();
                 
                  if($count >0){                        
                     $cartQueryS = MP\CartItemsQuery::create()
                                 ->where('CartItems.SessionId =?', $sessionid);
                  ?>
            
            <?php
                $i =1;
                  foreach($cartQueryS as $result) {  
                      if($result->getProductType() == 'small'){
                       $pquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                       $returnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($result->getProductId())->find();
                       foreach($returnquery as $rquery){
			$url = $base_url.'/smallformat?'.$rquery->getSubcategoryId();
						
                    }
                    foreach($pquery as $presult){
			$sname = $presult->getName();		
                    }
                    	}
                  ?> 
                    <div class="col-md-12 col-sm-12 cart_<?php echo $result->getCartId()?>">
                    <div class="col-md-12 col-sm-12 product_name">
                    <div class="col-md-10 col-sm-10 ">
                        <b><?php echo $sname; ?></b>
                    </div>
                       <?php $editurl = $base_url.'/'.$lang_name.'/editcart?'.$result->getCartId(); ?>
                     <div class="col-md-1 col-sm-1 edit_icon">
                   <a href="<?php echo $editurl; ?>">
                            <i class="icon-edit-sign"></i></a>
                    </div>
                    <div class="col-md-1 col-sm-1  delete_icon">
                          <input type="hidden" name="path" value="<?php echo $base_url; ?>/sites/all/themes/meprint/cart.php">
                        <a  class="deletecart" url="<?php echo $result->getCartId(); ?>">
                            <i class="icon-remove-sign"></i></a>
                    </div>
                    </div>
                          <div class="space-2"></div>
                   <!--<div class="col-md-12 col-sm-12 "> <div class="col-md-4 col-sm-4 ">
                        Offer :
                    </div> <div class="col-md-6 col-sm-6 ">
                        Details :
                    </div></div>-->
                   <div class="col-md-12 col-sm-12 ">
                        <div class="col-md-8 col-sm-8 ">
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        No of Copies :
                    </div><div class="col-md-4 col-sm-4 ">
                        <?php echo $result->getQuanitity() ?>
                    </div></div>
                    <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Dimensions :
                    </div><div class="col-md-4 col-sm-4 ">
                        <?php 
                        $dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
                         foreach($dquery as $mquery){
                             echo $mquery->getWidth() .'*'.$mquery->getHeight();
                         }
                         ?>
                        
                    </div></div>
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Material :
                    </div><div class="col-md-4 col-sm-4 ">
                         <?php 
                         $cquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
                         foreach($cquery as $mquery){
                             echo $mquery->getName();
                         }
                         ?>
                    </div></div>
                     <!--<div class="col-md-12 col-sm-12 "><div class="col-md-4 col-sm-4 ">
                        Accessories :
                    </div><div class="col-md-6 col-sm-6 ">
                        Details :
                    </div></div>-->
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Weight:
                    </div><div class="col-md-4 col-sm-4 ">
                        -NA-
                    </div></div>
                     <div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                        Cart date :
                    </div><div class="col-md-4 col-sm-4 ">
                        <?php 
                       $cBIGDATE  = $result->getCreatedDate(); 
                       echo  $cBIGDATE->format('Y-m-d');
                     
                        
                        ?>
                    </div></div></div>
                     
                     <div class="col-md-4 col-sm-4 ">
                         <?php   
                         $IMAGESS= MP\CartImagesQuery::create()
                        ->withColumn('Max(Id)')
                        ->where('CartImages.CartId =?', $result->getCartId())
                      
                        ->select(array('image'))
                        ->findOne();
                
                    $imageurls = $base_path."/sites/default/files/cart/smallformat/".$IMAGESS['image'];?>
                         <?php if($IMAGESS['image'] !='') { ?>
                         <img src="<?php echo $imageurls; ?>" width="150" heigth="150" />
                         <?php } ?>
                     </div></div>
                        <div class="space-2"></div>
                        <div class="col-md-12 col-sm-12 cart_price"><b><div class="col-md-10 col-sm-10 ">Total Price :
                    </div><div class="col-md-2 col-sm-2 ">
                        <?php 
                        $totlprc = $result->getPrice();
                        echo '&euro;'.number_format((float)$totlprc, 2, '.', '');
                                ?>
                    </div></b></div>
                         <div class="space-2"></div>
                     </div>
                    
                 
                 
                <?php  $i++;  } ?>
               
            <div class="space-2"></div>
               
         
           <input type="hidden" name="type"  value="<?php echo $type; ?>"> 
           <div class="col-md-3 col-sm-6 continue"><a href="<?php echo $url; ?>"><button class="btn "> Continue shopping</button></a></div>
             
            <div class="col-md-3 col-sm-6 payment"><a href="<?php echo $base_url.'/discount'; ?>"><button class="btn " type="submit" >Proceed to Payment</button></a></div>
             
      <?php }else{
          
          echo '<h4> No products available in your cart.</h4>';
      } 
      
   
   
   
}
/************** Cart Update Function start          ***************/
function carteditsuccess() {  
   
     global $user;
     global $base_url;
     global $language ;
     $lang_name = $language->language;
     
     
   if($_POST['type'] =="Big") {
     
   if($_POST['dimensions'] == "custom") {
        $width = $_POST['productWidth'];
        $height = $_POST['productHeight'];
        $dime = '';
        
    } else {
        $dime = $_POST['dimensions'];
         $width = '';
         $height = '';
    }
            $count = $_POST['imagecount'];
            for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);

            }
            $result = MP\CartItemsQuery::create()
                      ->filterByCartId($_POST['cartid'])
                      ->update(array('MaterialId' => $_POST['materialId'],
                            'Quanitity' => $_POST['quanitity'],
                            'Shipping' => $_POST['shipping'],
                            'DimensionId' => $dime,
                            'Width' => $width,
                            'Height' => $height,
                            'ProcessingId' => $_POST['baseprocessing'],
                            'Price' => $_POST['bigtotlprice'],
                            'ModifiedDate' =>time(),
                            'ExtraProcessingTop' => $_POST['baseisExtraTop'],
                            'ExtraProcessingRight' => $_POST['baseisExtraRight'],
                           'ExtraProcessingBottom' => $_POST['baseisExtraBottom'],
                           'ExtraProcessingLeft' => $_POST['baseisExtraLeft'],
                          'ExtraProcessingId' => $_POST['extadefValue'],
                          'AccessoryId'=>$_POST['baseaccessories'],
                          )); 
    
      
            if($_POST['extraprocessing'] && $_POST['extraprocessing'] !='') { 
                
                        $result = MP\CartExtraProcessingQuery::create()
                        ->filterByCartId($_POST['cartid'])
                        ->delete();

                   $cartExtra = new MP\CartExtraProcessing();
                    $cartExtra->setCartId($_POST['cartid']);
                    $cartExtra->setProcessingId($_POST['extraprocessing']);
                    $cartExtra->setCreatedDate(time());
		    $cartExtra->setModifiedDate(time()); 
                   $cartExtra->save(); 
                 
               
            }
       $fdfd =   MP\CartImagesQuery::create()->filterByCartId($_POST['cartid'])->find();
       if(count($fdfd)!=0) {
       foreach($fdfd as $vb)  {  
           if($_FILES['upload_'.$primaryKey]["name"] !='') {
            MP\CartImagesQuery::create()->filterById($vb->getId())->update(array('Status' =>0));
            $primaryKey =  $vb->getId();
            $path =  DRUPAL_ROOT.'/sites/default/files/cart/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($_POST['cartid']);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       }
       
        for($i=1;$i<=$_POST['imagecount'];$i++) {  
             
                $path =  DRUPAL_ROOT.'/sites/default/files/cart/' . $_FILES['image_'.$i]["name"];
              
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage = new MP\CartImages();
                         $cartimage->setCartId($_POST['cartid']);
                         $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 }
       
     } 
     else if($_POST['type'] =="small") {  
         
           $count = $_POST['simagecount'];
            for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);

            }
            $smallresultCart = MP\CartItemsQuery::create()
                      ->filterByCartId($_POST['cartid'])
                      ->update(array('MaterialId' => $_POST['smaterialId'],
                            'Quanitity' => $_POST['squanitity'],
                            'Shipping' => $_POST['sshipping'],
                            'DimensionId' => $_POST['sdimensions'],
                            'ProcessingId' => $_POST['processing'],
                            'ProductType' => $_POST['type'],
                            'Price' => $_POST['smalltotlprice'],
                            'ModifiedDate' =>time()
                             ));  //update cart items
            
            
            
              for($i=1;$i<=$_POST['simagecount'];$i++) {  
              $path =  DRUPAL_ROOT.'/sites/default/files/cart/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($_POST['cartid']);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 } //insert new files upload
                 
                 /***********Existed files upload start ***********/
                 
       $Smallfdfd =   MP\CartImagesQuery::create()->filterByCartId($_POST['cartid'])->find();
       if(count($Smallfdfd)!=0) { 
           if($_FILES['upload_'.$primaryKey]["name"] !='') { 
       foreach($Smallfdfd as $vb)  {   
            MP\CartImagesQuery::create()->filterById($vb->getId())->update(array('Status' =>0));
            $primaryKey =  $vb->getId();
            $path =  DRUPAL_ROOT.'/sites/default/files/cart/smallformat/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($_POST['cartid']);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       }
          /***********Existed files upload end ***********/        
                 
                 
         
     }
     $cartURL = $base_url."/".$lang_name ."/cart";
     echo '<h4> Cart details are sucessfully updated.</h4>';
     header('Refresh: 0; url='.$cartURL); 
  }
  /**************   Cart Update Function end       ***************/
  
 /**************   Wishlist Update Function Start       ******************/
 function wishlistsuccess() {
     global $user;
     global $base_url;
     global $language ;
     $lang_name = $language->language;
    
     if(isset($_POST['proceedc']) && $_POST['proceedc'] !='') {
         addtoCart($_POST);
         
         
         
     }else if(isset($_POST['proceed']) && $_POST['proceed'] !='') {
   if($_POST['type'] =="Big") {
     
   if($_POST['dimensions'] == "custom") {
        $width = $_POST['productWidth'];
        $height = $_POST['productHeight'];
        $dime = '';
        
    } else {
        $dime = $_POST['dimensions'];
         $width = '';
         $height = '';
    }
            $count = $_POST['imagecount'];
            for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/wishlist/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);

            }
            $result = MP\WishlistItemsQuery::create()
                      ->filterById($_POST['wishId'])
                      ->update(array('MaterialId' => $_POST['materialId'],
                            'Quanitity' => $_POST['quanitity'],
                            'Shipping' => $_POST['shipping'],
                            'DimensionId' => $dime,
                            'Width' => $width,
                            'Height' => $height,
                            'ProcessingId' => $_POST['baseprocessing'],
                            'Price' => $_POST['bigtotlprice'],
                            'ModifiedDate' =>time(),
                            'ExtraProcessingTop' => $_POST['baseisExtraTop'],
                            'ExtraProcessingRight' => $_POST['baseisExtraRight'],
                           'ExtraProcessingBottom' => $_POST['baseisExtraBottom'],
                           'ExtraProcessingLeft' => $_POST['baseisExtraLeft'],
                          'ExtraDefId' => $_POST['extadefValue'],
                          'AccessoryId'=>$_POST['baseaccessories'],
                         
                          )); 
    
      
            if($_POST['extraprocessing'] && $_POST['extraprocessing'] !='') { 
                
                        $result = MP\WishlistExtraProcessingQuery::create()
                        ->filterByWishlistId($_POST['wishId'])
                        ->delete();

                   $cartExtra = new MP\WishlistExtraProcessing();
                    $cartExtra->setWishlistId($_POST['wishId']);
                    $cartExtra->setProcessingId($_POST['extraprocessing']);
                    $cartExtra->setCreatedDate(time());
		    $cartExtra->setModifiedDate(time()); 
                   $cartExtra->save(); 
                 
               
            }
       $fdfd =   MP\WishlistImagesQuery::create()->filterByWishlistId($_POST['wishId'])->find();
       if(count($fdfd)!=0) {
       foreach($fdfd as $vb)  {  
            $primaryKey =  $vb->getImageId();
           if($_FILES['upload_'.$primaryKey]["name"] !='') {
           MP\WishlistImagesQuery::create()->filterByImageId($vb->getImageId())->update(array('Status' =>0));
           
             $path =  DRUPAL_ROOT.'/sites/default/files/wishlist/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\WishlistImages();
                        $cartimage->setWishlistId($_POST['wishId']);
                        $cartimage->setUserId($user->uid);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       }
      
        for($i=1;$i<=$_POST['imagecount'];$i++) {  
             
                $path =  DRUPAL_ROOT.'/sites/default/files/wishlist/' . $_FILES['image_'.$i]["name"];
              
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage =  new MP\WishlistImages();
                         $cartimage->setWishlistId($_POST['wishId']);
                          $cartimage->setUserId($user->uid);
                         $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 }
        $cartURL = $base_url."/".$lang_name ."/mywishlist";
        echo '<h4> Wish details are sucessfully updated.</h4>'; 
        header('Refresh:0; url='.$cartURL);
     } 
     else if($_POST['type'] =="small") {  
         
           $count = $_POST['simagecount'];
            for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/wishlist/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);

            }
            $smallresultCart = MP\WishlistItemsQuery::create()
                      ->filterById($_POST['wishId'])
                      ->update(array(
                            'MaterialId' => $_POST['smaterialId'],//
                            'Quanitity' => $_POST['squanitity'],//
                            'Shipping' => $_POST['sshipping'],//
                            'DimensionId' => $_POST['sdimensions'],//
                            'ProcessingId' => $_POST['processing'],//
                            'ProductType' => 'small',//
                            'Price' => $_POST['smalltotlprice'],//
                            'ModifiedDate' =>time()
                             ));  //update cart items
            
            
            
              for($i=1;$i<=$_POST['simagecount'];$i++) {  
              $path =  DRUPAL_ROOT.'/sites/default/files/wishlist/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                    if($_FILES['image_'.$i]["name"] !='') {
                        $cartimage = new MP\WishlistImages();
                        $cartimage->setWishlistId($_POST['wishId']);
                        $cartimage->setUserId($user->uid);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save(); 
                    }
                 } //insert new files upload
                 
                 /***********Existed files upload start ***********/
                 
       $Smallfdfd =   MP\WishlistImagesQuery::create()->filterByWishlistId($_POST['wishId'])->find();
       if(count($Smallfdfd)!=0) { 
           
       foreach($Smallfdfd as $vb)  {   
            $primaryKey =  $vb->getImageId();
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
            MP\WishlistImagesQuery::create()->filterByImageId($vb->getImageId())->update(array('Status' =>0));
            $path =  DRUPAL_ROOT.'/sites/default/files/wishlist/smallformat/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\WishlistImages();
                        $cartimage->setWishlistId($_POST['wishId']);
                        $cartimage->setUserId($user->uid);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       }
          /***********Existed files upload end ***********/        
              
                 
        $cartURL = $base_url."/".$lang_name ."/mywishlist";
        echo '<h4> Wish details are sucessfully updated.</h4>'; 
        header('Refresh:0; url='.$cartURL);  
     }
  }
  
    /* $cartURL = $base_url."/".$lang_name ."/mywishlist";
    
     echo '<h4> Wish details are sucessfully updated.</h4>'; 
     
   
     
       header('Refresh:0; url='.$cartURL); */
  }
  
   /**************   Wishlist Update Function End       ******************/  
  
  function addtoCart($data) { 
      
      global $base_path;
      global $base_url;
      global $language;
      $lang_name = $language->language ;
      session_start();
      $_SESSION['session_id'] = session_id();
      
      
     
      if($data['type'] =="Big") {  
          
                $count = $data['imagecount'];
                for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                
                } 
                $cart = new MP\CartItems();
                $cart->setSessionId(session_id());
                $cart->setProductType("Big");
                $cart->setProductId(isset($data['productid'])?$data['productid']:''); //
                $cart->setProductgroupId(isset($data['productgroupid'])?$data['productgroupid']:'');
                
                $cart->setAccessoryId(isset($data['baseaccessories'])?$data['baseaccessories']:'');//
                if(isset($data['dimensions']) && $data['dimensions'] == "custom" ) {
                $cart->setWidth(isset($data['productWidth'])?$data['productWidth']:'');//
                $cart->setHeight(isset($data['productHeight'])?$data['productHeight']:'');//
                } else  if(isset($data['dimensions']) && $data['dimensions'] != "custom" ) {
                     $cart->setDimensionId(isset($data['dimensions'])?$data['dimensions']:'');//
                }
                $cart->setMaterialId(isset($data['materialId'])?$data['materialId']:'');//
                $cart->setProcessingId(isset($data['baseprocessing'])?$data['baseprocessing']:'');//
             
                $cart->setShipping(isset($data['shipping'])?$data['shipping']:'');//
                if($data['quanitity'] == "") { 
                    $cart->setQuanitity(1); 
                } else {
                     $cart->setQuanitity(isset($data['quanitity'])?$data['quanitity']:''); 
                }
                //extadefValue
                $cart->setExtraProcessingId(isset($data['extadefValue'])?$data['extadefValue']:''); //
                $cart->setExtraProcessingTop(isset($data['baseisExtraTop'])?$data['baseisExtraTop']:''); //
                $cart->setExtraProcessingRight(isset($data['baseisExtraRight'])?$data['baseisExtraRight']:''); //
                $cart->setExtraProcessingLeft(isset($data['baseisExtraLeft'])?$data['baseisExtraLeft']:''); //
                $cart->setExtraProcessingBottom(isset($data['baseisExtraBottom'])?$data['baseisExtraBottom']:''); //
                $cart->setPrice(isset($data['bigtotlprice'])?$data['bigtotlprice']:'');//
                $cart->setCreatedDate(time());
		$cart->setModifiedDate(time());
                $cart->save(); 
                $idsss = $cart->getCartId();
                 /******** cart extra processing start************ */
                if(isset($data['extraprocessing']) && $data['extraprocessing'] !='') {
                   
                    $cartExtra = new MP\CartExtraProcessing();
                    $cartExtra->setCartId($idsss);
                    $cartExtra->setProcessingId($data['extraprocessing']);
                    $cartExtra->setCreatedDate(time());
		    $cartExtra->setModifiedDate(time()); 
                   $cartExtra->save(); 
                    
                }
                
                /******** cart extra processing end************ */
                
      /******** cart images step1************ */         
                
      $fdfd =   MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->find();
       if(count($fdfd)!=0) {
       foreach($fdfd as $vb)  {  
           $primaryKey =  $vb->getImageId();
           if($vb->getImage()!='') { 
               $source = DRUPAL_ROOT.'/sites/default/files/wishlist/' .$vb->getImage();
               $destination = DRUPAL_ROOT.'/sites/default/files/cart/'.$vb->getImage();
               copy($source, $destination);
               unlink($source);
               
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($vb->getImage());
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
           } //moving images wishlist to cart
           
           
           
           if($_FILES['upload_'.$primaryKey]["name"] !='') {
           $path =  DRUPAL_ROOT.'/sites/default/files/cart/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       }
      
       /******** cart images step1 end  ************ */    
       
       
       
        for($i=1;$i<=$count;$i++) {  
                     if($_FILES['image_'.$i]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
                     }
                 } 
       /******** delete images in wishlist start  ************ */    
                 
      MP\WishlistItemsQuery::create()->filterById($data['wishId'])->delete();
      MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->delete();
      MP\WishlistExtraProcessingQuery::create()->filterByWishlistId($data['wishId'])->delete();  
     /******** delete images in wishlist end  ************ */    
              
              $cartURL = $base_url.'/'.$lang_name.'/cart';
              header('Refresh: 0; url='.$cartURL); 
                
        //big end  
      } else if($data['type'] =="small"){
           $count = $data['simagecount'];
                for($i=1;$i<=$count;$i++) {    
                $path =  $_SERVER['DOCUMENT_ROOT'].$base_path.'sites/default/files/cart/smallformat/' . $_FILES['image_'.$i]["name"];
                move_uploaded_file($_FILES['image_'.$i]['tmp_name'],$path);
                
                } 
                
                
                $cart = new MP\CartItems();
                $cart->setSessionId(session_id());
                $cart->setProductType("small");
                $cart->setProductId(isset($data['productid'])?$data['productid']:''); 
                 $cart->setProductgroupId(isset($data['sproductgroupid'])?$data['sproductgroupid']:'');
                $cart->setDimensionId(isset($data['sdimensions'])?$data['sdimensions']:'');
                $cart->setMaterialId(isset($data['smaterialId'])?$data['smaterialId']:'');
                $cart->setProcessingId(isset($data['processing'])?$data['processing']:'');
             
                $cart->setShipping(isset($data['sshipping'])?$data['sshipping']:'');
                if($data['squanitity'] == "") { 
                    $cart->setQuanitity(1); 
                } else {
                     $cart->setQuanitity(isset($data['squanitity'])?$data['squanitity']:''); 
                }
                $cart->setPrice(isset($data['smalltotlprice'])?$data['smalltotlprice']:'');//
                $cart->setCreatedDate(time());
		$cart->setModifiedDate(time());
                $cart->save(); 
                $idsss = $cart->getCartId();
                 for($i=1;$i<=$count;$i++) {  
                     if($_FILES['image_'.$i]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['image_'.$i]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
                     }
                 } 
                 
                 
       $fdfd =   MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->find();
       if(count($fdfd)!=0) {
       foreach($fdfd as $vb)  { 
            if($vb->getImage()!='') { 
               $source = DRUPAL_ROOT.'/sites/default/files/wishlist/smallformat/' .$vb->getImage();
               $destination = DRUPAL_ROOT.'/sites/default/files/cart/smallformat/'.$vb->getImage();
               copy($source, $destination);
               unlink($source);
               
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($vb->getImage());
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
           } //moving images wishlist to cart
           $primaryKey =  $vb->getImageId();
           if($_FILES['upload_'.$primaryKey]["name"] !='') {
           $path =  DRUPAL_ROOT.'/sites/default/files/cart/smallformat/' . $_FILES['upload_'.$primaryKey]["name"];
            move_uploaded_file($_FILES['upload_'.$primaryKey]['tmp_name'],$path);
            if($_FILES['upload_'.$primaryKey]["name"] !='') { 
                        $cartimage = new MP\CartImages();
                        $cartimage->setCartId($idsss);
                        $cartimage->setImage($_FILES['upload_'.$primaryKey]["name"]);
                        $cartimage->setStatus(1);
                        $cartimage->setModifiedDate(time());
                        $cartimage->save();  
             }
       }
       }
       } //inser end
                 
        /******** delete images in wishlist start  ************ */    
                 
      MP\WishlistItemsQuery::create()->filterById($data['wishId'])->delete();
      MP\WishlistImagesQuery::create()->filterByWishlistId($data['wishId'])->delete();
      MP\WishlistExtraProcessingQuery::create()->filterByWishlistId($data['wishId'])->delete();  
     /******** delete images in wishlist end  ************ */    
              
              $cartURL = $base_url.'/'.$lang_name.'/cart';
              header('Refresh: 0; url='.$cartURL);           
                 
                
                
      }
      
      
  }//main funciton 