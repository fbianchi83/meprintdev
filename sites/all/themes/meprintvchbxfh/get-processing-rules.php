<?php
/***************************************************************************************/
/***************************************************************************************/

  /*propel installation*/

  // setup the autoloading
  require_once '/home/meprint/sites/all/libraries/propel/vendor/autoload.php';
  // setup Propel
  require_once '/home/meprint/sites/all/libraries/propel/generated-conf/config.php';

  /**
   * Root directory of Drupal installation.
   */
  define('DRUPAL_ROOT', '/home/meprint/');
  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/***************************************************************************************/

$processing = MP\ProcessingQuery::create()->filterByProcessingKindId(2)->find();

$res = array();

$res['linked'] = array();
$res['forbidden'] = array();
$res['adviced'] = array();

foreach ($processing as $proc){

  $linked = json_decode($proc->getLinked());

  if(isset($linked) && count($linked) > 0){
    foreach ($linked as $lk){
      $res['linked']['extra'.$proc->getProcessingId()][] = $lk;
    }
  }
  
  $forbidden = json_decode($proc->getForbidden());
  
  //dd($forbidden);

  if(isset($forbidden) && count($forbidden) > 0){
    foreach ($forbidden as $fr){
      $res['forbidden']['extra'.$proc->getProcessingId()][] = $fr;
    }
  } 
  
  $adviced = json_decode($proc->getAdviced());

  if(isset($adviced) && count($adviced) > 0){
    foreach ($adviced as $ad){
      $res['adviced']['extra'.$proc->getProcessingId()][] = $ad;
    }
  } 
}

echo json_encode($res);