<?php 
$productdesc = t('Product Description');
$ordertitle = t('ORDER THIS PRODUCT');
$quanitity = t('Quantity:');
$dimension = t('Dimensions:');
$custdimension = t('Custom Dimensions:');
$height = t('Height:');
$width = t('Width:');
$tmaterial = t('Material:');
$processing = t('Processing:');
$accessorie = t('Accessories:');
$Shippingtype = t('Shipping Type:');
$shipdesc = t('Delivery times are valid throughout the national territory. The couriers selected by us, DELIVER normally the day after the SHIPMENT For the islands and some areas are difficult to reach, could serve an additional 2 days. . Print file and confirmation of payment (Credit Card, Bank Policy) sent after the specified time will cause delays in the shipment of the material.');
$summery = t('Summary');
$product = t('Product');
$qty = t('Qty');
$price =t('Price');
$costpfprintingandmaterial = t('Cost of printing and materials');
$processcost = t('Processing Cost Per Copy');
$singleproductcost = t('Complete Single Product Cost');
$totalproductcost = t('Total product cost');
$proceed = t('Proceed');
$wishlist = t('Add to Wishlist');
$download = t('Click to downlaod product template');


$cartpagetittle= t('Cart Details');
$nocopies =t('No of Copies');
$weight = t('Weight');
$cartdate = t('Cart Date');
$totprice =('Total Price');
$conshoping = t('Continue Shopping');
$proceedpay = t('Proceed to Payment');

$summary = t('Summary');
$pname = t('Product Name');
$ordrconf =t('Order sucessfully created');

?>