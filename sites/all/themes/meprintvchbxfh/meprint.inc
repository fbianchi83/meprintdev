<?php

$vars['lbl_welcome'] = t('Welcome');
$vars['lbl_registration'] = t('Registration');
$vars['lbl_login'] = t('Log in');
$vars['lbl_logout'] = t('Log out');
$vars['lbl_cart'] = t('Cart');
$vars['lbl_feature'] = t('FEATURED PRODUCTS');
$vars['lbl_theworld'] = t('THE WORLD');
$vars['lbl_search'] = t('Search');
$vars['fronteretro'] = t('Processing sided');
$vars['item_files'] = t('item files');
$vars['different_files'] = t('different files');
$vars['choose'] = t('choose');

$vars['promo'] = t('Promo');
$vars['bestprice'] = t('Best Price');
$vars['new'] = t('New');
$vars['productdesc'] = t('Product Description');
$vars['ordertitle'] = t('ORDER THIS PRODUCT');
$vars['quanitity'] = t('Quantity');
$vars['dimension'] = t('Dimensions');
$vars['cdimension'] = t('Dimension');
$vars['custdimension'] = t('Custom Dimensions');
$vars['height'] = t('Height');
$vars['width'] = t('Width');
$vars['tmaterial'] = t('Material');
$vars['cmaterial'] = t('Material cart');
$vars['processing'] = t('Processing');
$vars['accessorie'] = t('Accessories');
$vars['front_back'] = t('Front-Back');
$vars['accessorie_base'] = t('Structure');
$vars['Shippingtype'] = t('Shipping Type');
$vars['Shippingdate'] = t('Shipping Date');
$vars['shipprice'] = t('Shipping Price');
$vars['packprice'] = t('Packaging Price');
$vars['deliveryprice'] = t('Cash on Delivery Price');
$vars['shipdesc'] = t('Delivery times are valid throughout the national territory. The couriers selected by us, DELIVER normally the day after the SHIPMENT For the islands and some areas are difficult to reach, could serve an additional 2 days. . Print file and confirmation of payment (Credit Card, Bank Policy) sent after the specified time will cause delays in the shipment of the material.');
$vars['summery'] = t('Summary');
$vars['product'] = t('Product');
$vars['qty'] = t('Qty');
$vars['price'] = t('Price');
$vars['costpfprintingandmaterial'] = t('Cost of printing and materials');
$vars['processcost'] = t('Processing Cost Per Copy');
$vars['singleproductcost'] = t('Complete Single Product Cost');
$vars['totalproductcost'] = t('Total product cost');
$vars['proceed'] = t('Proceed');
$vars['wishlist'] = t('Add to Wishlist');
$vars['download'] = t('Click to download product template');
$vars['bdownload'] = t('Download');
$vars['operatorreview'] = t('Extra operator review for');
$vars['operatorreview_cost'] = t('Operator review');
$vars['beforpromoprice'] = t('Price Before Promo');
$vars['promoval'] = t('Promo Value');
$vars['promoship'] = t('Promo Shipping');
$vars['extraprocessingcost'] = t('Extra Processing Cost');
$vars['wishlistlogin'] = t('Please login to add wishlist');

$vars['cartproduct'] = t('Product');
$vars['file'] = t('File');
$vars['quantity'] = t('Quantity');


//GRANDE FORMATO
$vars['structure_print'] = t('Buy structure and printing');
$vars['only_structure'] = t('Buy only structure');
$vars['only_print'] = t('Buy only printing');
$vars['ldistance'] = t('Choose distance');
$vars['lchoosefr'] = t('Choose loading');
$vars['lchoosetheme'] = t('choose theme');
$vars['leditor'] = t('choose editor');
$vars['lchangetheme'] = t('change theme');
$vars['lchangeditor'] = t('change theme with editor');
$vars['lremoveimage'] = t('remove image');
$vars['prodcorrelati'] = t('related products');



//PICCOLO FORMATO
$vars['lfolding'] = t('Type fold');
$vars['lorientation'] = t('Orientation');
$vars['orientation_h'] = t('Horizontal');
$vars['orientation_v'] = t('Vertical');
$vars['lgrammatura'] = t('Weight product');
$vars['measure_weight'] = t('gr/mq');
$vars['lnumface'] = t('number face');
$vars['ltypecover'] = t('type cover');
$vars['item_page'] = t('internal pages');
$vars['different_page'] = t('specific');
$vars['lcover'] = t('cover');


$vars['cartpagetittle'] = t('Cart Details');
$vars['nocopies'] = t('No of Copies');
$vars['weight'] = t('Weight');
$vars['cartdate'] = t('Cart Date');
$vars['totprice'] = t('Total Price');
$vars['conshoping'] = t('Continue Shopping');
$vars['proceedpay'] = t('Proceed to Payment');
$vars['proceedcart'] = t('Proceed to Cart');
$vars['noproductsavail'] = t('No products available in your cart');






$vars['pdetails'] = t('Personal Details');
$vars['lusername'] = t('Username');
$vars['lname'] = t('Name');
$vars['lsurname'] = t('Surname');
$vars['lamount'] = t('Amount');
$vars['lemail'] = t('Email');
$vars['lemailbis'] = t('Repeat email');
$vars['ptype'] = t('Payment Type');
$vars['lphone'] = t('Phone');
$vars['lmobile'] = t('Cellular');
$vars['lfax'] = t('Fax');
$vars['tnameid'] = t('Transaction Id');
$vars['billingandshipping'] = t('BILLING / SHIPPING ADDRESS');
$vars['billing'] = t('BILLING ADDRESS');
$vars['shipping'] = t('SHIPPING ADDRESS');
$vars['laddress'] = t('ADDRESS');
$vars['lzipcode'] = t('ZIPCODE');
$vars['llocation'] = t('LOCATION');
$vars['lprovince'] = t('PROVIENCE');
$vars['lcontry'] = t('COUNTRY');

$vars['lrecipient'] = t('Recipient');
$vars['lrecipient_name'] = t('Recipient name');
$vars['lrecipient_surname'] = t('Recipient surname');

$vars['laccountholder'] = t('Accountholder');
$vars['laccountholder_name'] = t('Accountholder name');
$vars['laccountholder_surname'] = t('Accountholder surname');

$vars['ltype_customer'] = t('Type of customer');
$vars['lname_rif'] = t('Name of reference person');
$vars['lsurname_rif'] = t('Surname of reference person');

$vars['lcompany_name'] = t('company name');
$vars['lvat'] = t('VAT Code');
$vars['lcf'] = t('Fiscal Code');
$vars['lerrorbill'] = t('Check the data. Account Holder data are mandatory as well as one between VAT and Fiscal Code');
$vars['lerrorbill2'] = t('Check the data. Invalid Fiscal Code');
$vars['productdetails'] = t('Product Details');
$vars['sameasbill'] = t('Same as Billing Address');
$vars['paymentoptions'] = t('Payment Options');
$vars['paymenttype'] = t('Payment Type:');
$vars['spaymenttype'] = t('Select payment type');
$vars['p1'] = t('Bank wire transfer');
$vars['p2'] = t('Cash on Delivery');
$vars['p3'] = t('PayPal');


$vars['finalprice'] = t('Final Price');
$vars['orderconfi'] = t('Order Confirmation Sucess');
$vars['nwishlist'] = t('No products available in your Wishlist');


$vars['summary'] = t('Summary');
$vars['pname'] = t('Product Name');
$vars['ordrconf'] = t('Order sucessfully created');

$vars['extraprocessing'] = t('Extra Processing');

$vars['ExtraProcessingTop'] = t('Extra Processing Top');

$vars['ExtraProcessingRight'] = t('Extra Processing Right');
$vars['ExtraProcessingLeft'] = t('Extra Processing Left');
$vars['ExtraProcessingBottom'] = t('Extra Processing Bottom');

$vars['discounttittle'] = t('Discount');
$vars['sno'] = t('S.No');
$vars['date'] = t('Date');
$vars['DiscountPrice'] = t('Discount Price');
$vars['TotalPrice'] = t('Total Price');
$vars['couponinput'] = t('I have an offer code or cash coupon');
$vars['Couponcode'] = t('Coupon Code');
$vars['invalidcoupon'] = t('Invalid Coupan Code');
$vars['validcoupon'] = t('Coupon code updated successfully');
$vars['update'] = t('Update');
$vars['PartialShipping'] = t('Partial Shipping');
$vars['CompleteShipping'] = t('Complete Shipping');
$vars['Proceedtoship'] = t('Proceed to Shipping');
$vars['uploadimagetitle'] = t('Upload Images');
$vars['uploadimagetitle_prev'] = t('Send Images');
$vars['operatorreviewtitle'] = t('Operator Review');
$vars['from'] = t('from');

$vars['shippinginfo'] = t('SHIPPING INFO');
$vars['orderconfirm'] = t('ORDER CONFIRM');
$vars['productprice'] = t('Product Price');
$vars['ivaprice'] = t('VAT Price');
$vars['producttotalprice'] = t('Product Total Price');
$vars['discountprice'] = t('Discount Price');
$vars['vat4'] = t('Do you have the rights for a facilitated VAT?');
$vars['agevoliva'] = t('Facilitated VAT');

$vars['coupanlabel'] = t('I have an offer code or cash coupon');
$vars['scoupon'] = t('Coupon code updated successfully');
$vars['fcoupon'] = t('Invalid Coupon Code');
$vars['pshipping'] = t('Partial Shipping');
$vars['cshipping'] = t('Complete Shipping');
$vars['Coupon'] = t('Coupon');

$vars['anonoshipingtext'] = t('From address will not be published on the box');
$vars['anonoshiping'] = t('Anonymous Shipping');

$vars['Home'] = t('Home');
$vars['bdiscount'] = t('Discount');
$vars['bcart'] = t('CART DEATILS');
$vars['bshipping'] = t('Shipping Details');
$vars['bordeconfirm'] = t('Order Confirm');
$vars['bordersucess'] = t('Order Sucess');
$vars['orderempty'] = t('your order is EMPTY');

$vars['samecategory'] = t('You might also like');

$vars['addcart'] = t('Add cart');

$vars['side'] = t('Choose side');

$vars['iva_cost'] = t('IVA');

$vars['totalcost'] = t('Total');

$vars['paynow'] = t('Pay now');

$vars['backtocart'] = t('Back to cart');

$vars['back'] = t('Come back');

$vars['buy'] = t('Buy');

$vars['quote_data'] = t('Quote data');

$vars['quote_name'] = t('Name quote');
$vars['note'] = t('Note');
$vars['nonote'] = t('No note inserted');

$vars['print_cart'] = t('Print cart');

$vars['general_info'] = t('General Info');
//rubrica
$vars['lrubrica'] = t('Choose address book');
$vars['lcontact'] = t('Select contact');
$vars['addcontact'] = t('add contact');


// Report
$vars['revenuestats'] = t('Revenues Report');
$vars['topcartstats'] = t('Top Carts');
$vars['lastsellstats'] = t('Last Sells');
$vars['historystats'] = t('Orders History');
$vars['daily'] = t('Daily');
$vars['weekly'] = t('Weekly');
$vars['monthly'] = t('Monthly');
$vars['yearly'] = t('Yearly');
$vars['yearlyc'] = t('Year Comparison');
$vars['datestart'] = t('Date Start');
$vars['dateend'] = t('Date End');
$vars['weekstart'] = t('Week Start');
$vars['weekend'] = t('Week End');
$vars['monthstart'] = t('Month Start');
$vars['monthend'] = t('Month End');
$vars['yearrep'] = t('Select Year');
$vars['filterdate'] = t('Filter');
$vars['daterep'] = t('Dates');
$vars['dayrevenue'] = t('Daily Revenues');
$vars['weekrep'] = t('Weeks');
$vars['weekrevenue'] = t('Weekly Revenues');
$vars['monthrep'] = t('Months');
$vars['monthrevenue'] = t('Monthly Revenues');
$vars['yearrevenue'] = t('Yearly Revenues');
$vars['topcartdates'] = t('Pick the interval');
$vars['lastselldates'] = t('Pick the date');
$vars['lastsellclient'] = t('Pick the customer');
$vars['lastsellqty'] = t('Pick the number');
$vars['showoff'] = t('Show Details');
$vars['lastlast'] = t('Last');
$vars['sellslast'] = t('Sells');
$vars['historyfilter'] = t('Base Filters');
$vars['historyfilter2'] = t('Detailed Filters');
$vars['historyfilter3'] = t('Less Filters');
$vars['noorders'] = t('No Orders Selected');
$vars['daydeadcart'] = t('Abandoned Cart Day By Day');
$vars['weekdeadcart'] = t('Abandoned Cart Week By Week');
$vars['monthdeadcart'] = t('Abandoned Cart Month By Month');
$vars['yeardeadcart'] = t('Abandoned Cart Year');
$vars['yearcompdeadcart'] = t('Abandoned Cart Years Comparison');
$vars['daycartdead'] = t('Day By Day Abandoned Cart');
$vars['weekcartdead'] = t('Week By Week Abandoned Cart');
$vars['monthcartdead'] = t('Month By Month Abandoned Cart');
$vars['yearcartdead'] = t('Year Abandoned Cart');
$vars['deadcartstats'] = t('Abandoned Cart Statistics');
$vars['sendship'] = t('Send Shipment'); 

?>