<?php 

global $language;
$lang_name = $language->language;

if($lang_name == "en"){
  $lbl_welcome = t('Welcome');
  $lbl_registration = t('Registration');
  $lbl_login = t('Log in');
  $lbl_logout = t('Log out');
  $lbl_cart = t('Cart');
  $lbl_feature = t('FEATURED PRODUCTS');
  $lbl_theworld = t('THE WORLD');
  $lbl_search = t('Search...');
}
else if ($lang_name == "fr"){
  $lbl_welcome = t('Bienvenue');
  $lbl_registration = t('Inscription');
  $lbl_login = t('Sidentifier');
  $lbl_logout = t('Deconnexion');
  $lbl_cart = t('Chariot');
  $lbl_feature = t('PRODUITS EN VEDETTE');
  $lbl_theworld = t('LE MONDE');
  $lbl_search = t('Recherche ...');
}
else if($lang_name == "it"){
  $lbl_welcome = t('Benvenuto');
  $lbl_registration = t('Registrazione');
  $lbl_login = t('Login');
  $lbl_logout = t('Esci');
  $lbl_cart = t('Carrello');
  $lbl_feature = t('PRODOTTI IN EVIDENZA');
  $lbl_theworld = t('IL MONDO');
  $lbl_search = t('Ricerca ...');
}

?>