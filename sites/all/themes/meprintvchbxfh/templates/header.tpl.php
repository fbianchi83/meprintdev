<?php global $language; ?>
<header>
    <section class="header-tp">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 top-nav">
            <?php print render($page['header_top']); ?>
          </div>

          <div class="col-md-6 col-sm-6 top-nav hidden-xs">
            <?php print render($page['header_top_left']); ?>
          </div>
        </div>
      </div>
    </section>
    <section class="header-md">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-7 col-xs-12 head2-left">
            <a href="<?php echo $base_url; ?>">
              <div class="logo col-md logo-left"><img src="/sites/default/files/Meprint_logo-New.png"/></div>
            </a>
            <?php if ($user->uid != 1) { ?>
              <div class="logo-right hidden-xs">
                	<span class="fa-stack fa-lg">
                      <a href="https://twitter.com/MePrint_it"
                        target="_blank"><i
                          class="fa fa-twitter fa-stack-1x"></i></a>
                    </span>
                    <span class="fa-stack fa-lg">
                      <a href="http://www.facebook.com/MePrint.it"
                        target="_blank"><i
                          class="fa fa-facebook fa-stack-1x"></i></a>
                    </span>
                    <span class="fa-stack fa-lg">
                      <a href="http://www.youtube.com/user/MePrintWebTV"
                        target="_blank"><i
                          class="fa fa-youtube fa-stack-1x"></i></a>
                    </span>
              </div><?php } ?>
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12 head2-left">
            <div class="mattblacktabs">
                <ul>
                  <li class="selected">
                    <a href="<?php echo $base_url . '/'. $lang_name .'/carrello'; ?>">
                        <i class="fa fa-shopping-cart fa-lg"></i>&nbsp;<span><?php echo $lbl_cart; ?></span>
                        <span class="cartcount">
                          <?php
                            global $user;
                            if($user->uid!=0){
                              $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->findOne();
                            }else{
                              session_start();
                              $sesid = session_id();
                              $cart = MP\CartQuery::create()->filterBySessionId($sesid)->filterByStatus(1)->findOne();
                            }
                            if($cart){
                              $cart_count = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->count();
                              echo $cart_count;
                            }else{
                              echo "0";
                            }
                          ?>
                        </span>
                    </a>
                  </li>
                  <?php
                  if ($logged_in){
                    $loadeduser = user_load($user->uid);
                    $propel_user = MP\UserQuery::create()->filterByUid($user->uid)->findOne();
                    if($propel_user){
                      if($propel_user->getUserType() == "private" || $propel_user->getUserType() == "individual" || $propel_user->getUserType() == "unknown"){
                        $username = $propel_user->getName();
                      } else{
                        $username = $propel_user->getCompanyName();
                      }
                    }  else {
                      $username = $loadeduser->name;
                    }
                    ?> 
                    <script type="text/javascript"> 
                        localStorage.setItem('nomeutente', '<?php echo $username; ?>'); 
                        localStorage.setItem('emailutente', '<?php echo $loadeduser->mail; ?>'); 
                    </script>
                    
                    <li>
                        <a href="/<?php echo $language->language; ?>/user" >
                            <span class="uname" style='color:#fff;'><?php print $lbl_welcome . '  ' . $username; ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $base_url . '/'. $lang_name . '/user/logout'; ?>"><?php echo $lbl_logout; ?></a>
                    </li>
                  <?php }else{
                  ?>
                  <?php if (!$logged_in) { ?>
                    <li>
                        <a href="<?php echo $base_url . '/'. $lang_name ?>/user/register">
                            <i class="fa fa-pencil fa-lg"></i>
                            <span>&nbsp;<?php echo $lbl_registration; ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $base_url . '/'. $lang_name ; ?>/user/login">
                            <i class="fa fa-key fa-lg"></i><span>&nbsp;<?php echo $lbl_login; ?></span>
                        </a>
                    </li>
                  <?php }?>
                    <script type="text/javascript"> 
                        localStorage.setItem('nomeutente', ''); 
                        localStorage.setItem('emailutente', ''); 
                    </script>
                <?php } ?>
                </ul>
                <div class="region region-header-middle">
                  <div class="content">
                    <?php print render($page['header_middle']); ?>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <?php
      if(!drupal_is_front_page()) {
    ?>
        <section class="header-bt">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 head2-left">
                <?php print render($page['frontpage_left']); ?>
              </div>
              <div class="col-sm-6 col-md-6 col-xs-12 head2-left pull-right">
                <form id="searchform" method="post" class="navbar-form"
                  name="advanced-search-form"
                  action="<?php echo $base_url . '/'. $lang_name ; ?>/search-results"
                  enctype="multipart/form-data">
                  <div class="input-group col-md-11 col-sm-11">

                    <input type="text" name="adserach" placeholder="<?php echo $lbl_search; ?>..." class="form-control col-md-4 col-sm-4">

                    <div class="input-group-btn">
                      <button type="submit" id="serach" name="submit"
                        class="btn btn-default"><i class="fa fa-search"></i>
                      </button>
                    </div>
                  </div>
                  <span class="p-title hidden-xs"><img alt="logo"
                      src="<?php echo $base_url; ?>/sites/default/files/meprint-litle.png"></span>

                </form>
              </div>
            </div>
          </div>
        </section>
    <?php
      }else{
    ?>
        <section class="header-bt">
          <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 h3-col">

                  <?php print render($page['header_bottom_thirdsection']); ?>
                </div>
                <div class="col-md-4 col-sm-4 h3-col">
                  <?php print render($page['header_bottom_fourthsection']); ?>
                </div>
                <div class="col-md-4 col-sm-4 h3-col">
                  <?php print render($page['header_bottom_fifthsection']); ?>
                </div>
            </div>
          </div>
        </section>
    <?php
      }
    ?>
  </header>