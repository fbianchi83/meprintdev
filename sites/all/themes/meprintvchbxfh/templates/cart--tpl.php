<?php

/**
 * @file
 * Meprint theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php global $base_path;
global $base_url;
?>
<div id="wrapper">
  <header>
    <section class="header-tp">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 top-nav">
            <?php print render($page['header_top']); ?>
          </div>

          <div class="col-md-6 col-sm-6 top-nav">
            <?php print render($page['header_top_left']); ?>
          </div>
        </div>
      </div>
    </section>
    <section class="header-md">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-7 col-xs-12 head2-left">
            <a href="<?php echo $base_url; ?>">
              <div class="logo col-md logo-left"><img
                  src="/sites/default/files/Meprint_logo-New.png"/></div>
            </a>
            <?php if ($user->uid != 1) { ?>
              <div class="col-md logo-right">
                	<span class="fa-stack fa-lg">
                      <a href="https://twitter.com/MePrint_it"
                        target="_blank"><i
                          class="fa fa-twitter fa-stack-1x"></i></a>
                    </span>
                    <span class="fa-stack fa-lg">
                      <a href="http://www.facebook.com/MePrint.it"
                        target="_blank"><i
                          class="fa fa-facebook fa-stack-1x"></i></a>
                    </span>
                    <span class="fa-stack fa-lg">
                      <a href="http://www.youtube.com/user/MePrintWebTV"
                        target="_blank"><i
                          class="fa fa-youtube fa-stack-1x"></i></a>
                    </span>
              </div><?php } ?>
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12 head2-left">
            <div class="mattblacktabs">
              <ul>
                <li class="selected">
                  <a href="<?php echo $base_url . '/cart'; ?>"><i
                      class="fa fa-shopping-cart fa-lg"></i> &nbsp;<?php echo $lbl_cart; ?> <span
                      class="cartcount">
                     <?php
                     if (count($_POST) != 0) {
                       session_start();
                       $sesid = session_id();
                       $cntSubGroups = MP\CartItemsQuery::create()
                                                        ->where('CartItems.SessionId =?', $sesid)
                                                        ->count();

                       echo $count = $cntSubGroups + 1;
                     }
                     else {
                       session_start();
                       $sesid = session_id();
                       $cntSubGroups = MP\CartItemsQuery::create()
                                                        ->where('CartItems.SessionId =?', $sesid)
                                                        ->count();
                       echo $count = $cntSubGroups;

                     }

                     ?>
                   </span>
                  </a>
                </li>


                <?php
                if ($logged_in){
                  $loadeduser = user_load($user->uid);
                  $username = $loadeduser->name;
                  ?>

                  <li><a><span class="uname"
                        style='color:#fff;'><?php print $lbl_welcome . '  ' . $username; ?></span></a>
                  </li>
                  <!-- <li><a href="<?php print $base_url;?>/user/<?php print $user->uid;?>/edit">Change password</a></li>-->
                  <li><a
                      href="<?php echo $base_path . 'user/logout'; ?>"><?php echo $lbl_logout; ?></a>
                  </li>


                <?php }else{
                ?>


                <?php if (!$logged_in) { ?>
                  <li><a
                      href="<?php echo $GLOBALS['base_url']; ?>/user/register"><i
                        class="fa fa-pencil fa-lg"></i>
                      &nbsp;<?php echo $lbl_registration; ?></a></li>
                  <li><a href="<?php echo $GLOBALS['base_url']; ?>/user/login"><i
                        class="fa fa-key fa-lg"></i>
                      &nbsp;<?php echo $lbl_login; ?></a></li>
                <?php }?>
              </ul>

              <?php } ?>


              <?php print render($page['header_middle']); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="header-bt">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12 head2-left">
            <?php print render($page['frontpage_left']); ?>
          </div>
          <div class="col-sm-6 col-md-6 col-xs-12 head2-left pull-right">
            <form id="searchform" method="post" class="navbar-form"
              name="advanced-search-form"
              action="<?php echo $base_url; ?>/search-results"
              enctype="multipart/form-data">
              <!--<form id="searchform" method="post" class="navbar-form" name="advanced-search-form"
                 action="http://localhost/meprintv3/search-results" enctype= "multipart/form-data">-->
              <div class="input-group col-md-11 col-sm-11">

                <input type="text" name="adserach"
                  placeholder="<?php echo $lbl_search; ?>..."
                  class="form-control col-md-4 col-sm-4">

                <div class="input-group-btn">
                  <button type="submit" id="serach" name="submit"
                    class="btn btn-default"><i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
              <span class="p-title"><img alt="logo"
                  src="<?php echo $base_url; ?>/sites/default/files/meprint-litle.png"></span>

            </form>
            <!--<input type="text" placeholder="Search..." class="serch-panel">-->
            <?php //print render($page['frontpage_right']);?>
          </div>
        </div>
      </div>
    </section>
  </header>
  <!--End of the header--->
<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<aside class="col-md-3 col-sm-3">
  <nav>
    <?php print render($page['left_sidebar']);?>
  </nav>
  <div class="space-2"></div>
</aside>
<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">
  <!--<div class="row">
<?php //if ($breadcrumb): print $breadcrumb; endif;?>
  </div>-->
    <?php //if ($title): ?>
	<!--<h3>&nbsp;<?php //print $title; ?>&nbsp;<span><img src="../sites/all/themes/meprint/images/wave.png" alt="image"></span> </h3><?php //endif; ?>
<!--    <div class="space-3"></div>-->
   
  </div>
  </header>
  <div class="space-3"></div>
  <?php print render($page['content']);
 
  
  ?>
</div>  
  <aside class="col-md-3 col-sm-3">
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <?php print render($page['right_sidebar']);?>
      </div>
    </div>
    <div class="space-2"></div>   
  </aside>
  </article>
  </section>
  <!--End ot the Banner--->
  <section class="blue-bg">
    <div class="container">
      <?php print render($page['first_preface']);?>
    </div>
  </section>
 <section class="lit-gra-bg">
    <div class="container"> 
	<?php global $base_url;?>
	<header class="row">
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
      <?php print render($page['second_preface_header']);?>
		</div>
		<div class="col-sm-2 col-md-2"></div>
	</header>
    <article class="row">
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_left']);?>
	</div>
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_center']);?>
	</div>
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_right']);?>
	</div>	 
    </article>
	</div>
  </section>
  <footer class="footer-bg">
    <div class="container">
      <?php print render($page['footer']);?>
    </div>
  </footer>
</div>