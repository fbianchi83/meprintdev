<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal modal-gallery fade bs-example-modal-lg" id="frameModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width:1000px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Valutazione prodotti</h4>
            </div>
            <div class="modal-body">
                <iframe height="500" width="950" src="<?php print base_path() . path_to_theme() . '/starreviews/admin/'; ?>"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Chiudi</button>
            </div>
        </div>
    </div>
</div>