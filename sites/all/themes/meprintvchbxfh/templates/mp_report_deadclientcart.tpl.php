<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dailyend" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#dailystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    }); 
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    var prevDate = new Date();
    $("#dailystart").datepicker("setDate",currentDate);
    $("#dailyend").datepicker("setDate",prevDate);
    
    filtradead();
});

function filtradead() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    var clientid= jQuery('#clientsel').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadclientcart",
        data:'datastart='+datestart+'&dataend='+dateend+'&client='+clientid,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            if (result) {
                var i=0;
                while (i<result.length) {
                    temp += "<tr>";
                    temp += "<td>"+result[i][0]+"</td>";
                    temp += "<td><strong>"+result[i][2]+"</strong></td>";
                    temp += "<td>"+result[i][3]+" &euro;</td>";
                    temp += "<td>"+result[i][4]+"</td>";
                    temp += "<td class='text-right order-detail'><a href='/<?php echo $language->language; ?>/abandoned_cart/"+result[i][0]+"'><?php echo $showoff; ?></a></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='7'> <?php echo $noorders; ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('N°'); ?></th><th width='50%'><?php echo t('Client'); ?></th><th><?php echo t('Amount'); ?></th><th><?php echo t('Products'); ?></th><th class='text-right'><?php echo t('Display'); ?></th></tr></thead>" + temp;
            jQuery('#mp-list-order').empty().append(temp);
        }
    });
}

</script>

<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $historystats; ?> </h3>
    
    <div id='dailychoice' class="billcentered"> 
        
        <?php echo t('Client'); ?> 
        <select id='clientsel' class="margin10" >
            <option value='-1'> <?php echo t('All Clients'); ?> </option>
            <option value='0'> <?php echo t('Anonymous'); ?> </option>
            <?php 
            $users = MP\UserQuery::create()->find();
            foreach ($users as $user) {
                if ($user->getUid() != 1) {
                    if ($user->getUserType=="association" || $user->getUserType=="public")
                        $name= $user->getCompanyName();
                    else
                        $name= $user->getName() . " " . $user->getSurname();
                    ?>
            <option value="<?php echo $user->getUid(); ?>"> <?php echo $name; ?> </option>
                    <?php
                }
            }
            ?>
        </select>
        <?php echo $datestart; ?> <input type='text' class="margin10" id='dailystart'>
        <?php echo $dateend; ?> <input type='text' class="margin10" id='dailyend'>
        <input type='button' id="filter1" value='<?php echo $filterdate; ?>' onclick='filtradead(); return false;'>
    </div>
    
    <table id="mp-list-order" class="table table-bordered table-striped table-hover"></table>
</div>