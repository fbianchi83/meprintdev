
<?php 
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';
global $base_url;
global $base_path;
global $language;
global $user;
$lang_name = $language->language;
?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">

<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): echo '<a href="'.$base_url.'">'. $Home .'</a>'.' / '. $bordersucess; endif;?>  
  
      <div class="space-2"></div>
     <div class="cartnav">
          <div class="cartlable shiplable">
              <span class="inactive"> <?php echo $cartpagetittle;?></span>
          </div>
          <div class="cartlable shiplable">
              
              <span class="inactive">
              <?php echo $shippinginfo;?></span></div>
      <div class="ordernav shiplable">
              
                   <span class="shiplable active">
              <?php echo $orderconfirm;?></span></div>
      </div>

  </div>
   <div class="space-3"></div>
   
            <?php
            
                session_start();

                if($_POST['accountholder'] != ""){
                  $_SESSION['accountholder'] = $_POST['accountholder'];
                }
                if($_POST['address'] != ""){
                  $_SESSION['address'] = $_POST['address'];
                }
                if($_POST['phone'] != ""){
                  $_SESSION['phone'] = $_POST['phone'];
                }
                if($_POST['email'] != ""){
                  $_SESSION['email'] = $_POST['email'];
                }
                if($_POST['zipcode'] != ""){
                  $_SESSION['zipcode'] = $_POST['zipcode'];
                }
                if($_POST['location'] != ""){
                  $_SESSION['location'] = $_POST['location'];
                }
                if($_POST['province'] != ""){
                  $_SESSION['province'] = $_POST['province'];
                }
                if($_POST['country'] != ""){
                  $_SESSION['country'] = $_POST['country'];
                }

                if($_POST['srecipient'] != ""){
                  $_SESSION['srecipient'] = $_POST['srecipient'];
                }
                if($_POST['saddress'] != ""){
                  $_SESSION['saddress'] = $_POST['saddress'];
                }
                if($_POST['sphone'] != ""){
                  $_SESSION['sphone'] = $_POST['sphone'];
                }
                if($_POST['semail'] != ""){
                  $_SESSION['semail'] = $_POST['semail'];
                }
                if($_POST['szipcode'] != ""){
                  $_SESSION['szipcode'] = $_POST['szipcode'];
                }
                if($_POST['slocation'] != ""){
                  $_SESSION['slocation'] = $_POST['slocation'];
                }
                if($_POST['sprovince'] != ""){
                  $_SESSION['sprovince'] = $_POST['sprovince'];
                }
                if($_POST['scountry'] != ""){
                  $_SESSION['scountry'] = $_POST['scountry'];
                }
                
                if($_POST['note'] != ""){
                  $_SESSION['note'] = $_POST['note'];
                }
                
                if($_POST['Shipping_Confirm'] != ""){
                  $_SESSION['Shipping_Confirm'] = $_POST['Shipping_Confirm'];
                }

                if(isset($_SESSION['Shipping_Confirm']) && $_SESSION['Shipping_Confirm']!='') {

                  if($user->uid == 0){
                    
                    $sessionid = session_id();

                    $cart = MP\CartQuery::create()->filterBySessionId($sessionid)->filterByStatus(1)->findOne();
                  }else{
                    $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->findOne();
                  }
                 
                  if($cart){  
                ?>
                <form method="POST" action="<?php echo $base_url.'/'.$lang_name.'/ordersucess'; ?>" class="confirm-order">
                    
                    <input type="hidden" name="orderconfirm" id="orderconfirm" value="1" />
                    
                     <div class="col-md-12 col-sm-12 product_name">          
                        <div class="col-md-5 col-sm-5"><b><?php echo $cartproduct; ?></b></div>
                        <div class="col-md-3 col-sm-3"><b><?php echo $file; ?></b></div>
                        <div class="col-md-2 col-sm-2"><b><?php echo $quantity; ?></b></div>
                        <div class="col-md-2 col-sm-2"><b><?php echo $price; ?></b></div>
                    </div>
                      
                 <?php     
                 $cart_items = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->find();
                  $i =0;
                  foreach($cart_items as $result){ 
                    $i++;
                    if($result->getProductType() == 'big'){

                    $presult = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->findOne();
                    $returnquery = MP\FormoneBigFormatQuery::create()->filterByFormId($result->getProductId())->findOne();
                    $cartdelid = $result->getCartId();  
                 ?>
		
                <div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $cartdelid; ?>">                                            
                    
                        <div class="clearfix"></div>
                        
                    <div class="col-md-5 col-sm-5 ">
                        <div class="cart_4">
                             <div class="col-md-12 col-sm-12 cartdetails_4">
                                 <b><?php echo $presult->getName(); ?></b><br/></div>
                     <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                      
                        <?php echo $nocopies; ?> 
                    </span><span class="datacart">
                        <?php echo $result->getQuanitity() ?>
                    </span></div>
                    <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                        <?php echo $dimension; ?> 
                    </span><span class="datacart">
                        <?php 
                          if($result->getDimensionId() !=0) {
                                $dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->findOne();
                                echo $dquery->getWidth() .'*'.$dquery->getHeight();
                          } else {
                              echo $result->getWidth()."*".$result->getHeight();
                          }
                         ?>
                    </span></div>
                     <div class="col-md-12 col-sm-12 cartdetails">
                         <span class="labelcart">
                          <?php echo $tmaterial; ?> 
                         </span>
                         <span class="datacart">
                         <?php 
                         
                         $mquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->findOne();
                         echo $mquery->getName();

                         ?>
                        </span>
                     </div>
                     <div class="col-md-12 col-sm-12 cartdetails">
                    <span class="labelcart">
                       <?php echo $Shippingdate;?> 
                    </span><span class="datacart">
                        <?php 
                          $cDATE = $result->getShippingDate();
                          echo $cDATE->format('d-m-Y');
                        ?>
                    </span></div></div>
                        
                    </div>
                        
                         <div class="col-md-3 col-sm-3 ">
                         <?php
                              $images = MP\CartItemFileQuery::create()->filterByCartItemId($cartdelid)->findOne();

                              ?>
                              <?php 
                                if ($images) {
                                  $image = $images->getFid();
                                  $image_uri = file_load($image)->uri;
                              ?>
                                    <img src="<?php echo image_style_url("thumbnail", $image_uri); ?>" />
                              <?php
                              } else {
                                echo '<i class="fa fa-camera"></i>';
                              }
                              ?>
                     </div>
                        <div class="col-md-2 col-sm-2 cartdetailsline">
                        <div class="col-md-6 col-sm-6 cartdetails">
                        <?php echo $result->getQuanitity() ?>
                    </div>
                            
                        </div>
                         <div class="col-md-2 col-sm-2 cartdetailsline">
                        <div class="col-md-10 col-sm-10 cartdetails cartprice"><b>
                        <?php 
                        $totlprc = $result->getTotalPrice();
                        echo '&euro;'.number_format((float)$totlprc, 2, '.', '');
                                ?>
                    </b></div></div>
                         
                       <div class="clearfix"></div>
                    </div>
                 <?php	
                    }else if($result->getProductType() =='small') {
                  
                    }
                 }
                ?>
                       <div class="col-md-12 col-sm-12 discountdiv orderconfirm">
                    
                     <?php 
                    
                     if((isset($_SESSION['dicountamount']) && $_SESSION['dicountamount']!=0) ){?>
                        <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $Coupon; ?></label><input type="text" name="coupon_code" class="form-text" value="<?php echo $_SESSION['coupon_code'];?>" readonly>
                        </div>
                     <?php  }else{?>
                           <div class="col-md-6 col-sm-6"></div>
                   <?php  } ?>
                           <div class="col-md-6 col-sm-6">
                               <?php if((isset($_SESSION['dicountamount']) && $_SESSION['dicountamount']!=0) ){?>
                               <div class="col-md-12 col-sm-12">
                                   <label class="shiplabel"><?php echo $discountprice; ?></label>&euro; <input size="8"  style="border:none;" type="text" name="dicountamount" value="<?php echo $_SESSION['dicountamount'];?>" readonly></span>
                               </div>  
                               <?php }?>
                               <div class="col-md-12 col-sm-12 text-right">
                                   <label class="shiplabel"><?php echo $finalprice; ?></label>&euro; <input size="8" style="border:none;" name="finalamount"  type="text" value="<?php echo $_SESSION['finalamount'];?>" readonly></span></div>
                        </div>
                       </div>
                    <div class="col-md-12 col-sm-12 discountdiv orderconfirm boxed">

                        <div class="clearfix"></div>
                      <div class="space-2"></div>
                         <div class="clearfix"></div>
                         <div class="form-section-title"> <b><?php echo $billing; ?></b></div>
                          <div class="row">
                              <div class="col-sm-6 col-md-6"><label class="shiplabel"><?php echo $laccountholder; ?></label><input type="text" class="form-text" name="accountholder" value="<?php echo $_SESSION['accountholder']; ?>" readonly /></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $laddress;?></label><input type="text" class="form-text" name="address" value="<?php echo $_SESSION['address'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lphone;?></label><input type="text" class="form-text" name="phone" value="<?php echo $_SESSION['phone'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lemail;?></label><input type="text" class="form-text" name="email" value="<?php echo $_SESSION['email'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lzipcode;?></label><input type="text" class="form-text" name="zipcode" value="<?php echo $_SESSION['zipcode'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $llocation;?></label><input type="text" class="form-text" name="location" value="<?php echo $_SESSION['location'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lprovince;?></label><input type="text" class="form-text" name="provience" value="<?php echo $_SESSION['province'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lcontry;?></label><input type="text" class="form-text" name="country" value="<?php echo $_SESSION['country'];?>" readonly>
                            </div>
                          </div>

                        <div class="clearfix"></div>
                          <div class="space-2"></div>
                       <div class="form-section-title"> <b><?php echo $shipping; ?></b></div>
                          <div class="row">
                            <div class="col-sm-6 col-md-6"><label class="shiplabel"><?php echo $lrecipient; ?></label><input type="text" class="form-text" name="srecipient" value="<?php echo $_SESSION['srecipient']; ?>" readonly /></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $laddress;?></label><input type="text" class="form-text" name="saddress"value="<?php echo $_SESSION['saddress'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lphone;?></label><input type="text" class="form-text" name="sphone" value="<?php echo $_SESSION['sphone'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lemail;?></label><input type="text" class="form-text" name="semail" value="<?php echo $_SESSION['semail'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lzipcode;?></label><input type="text" class="form-text" name="szipcode" value="<?php echo $_SESSION['szipcode'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $llocation;?></label><input type="text" class="form-text" name="slocation" value="<?php echo $_SESSION['slocation'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lprovince;?></label><input type="text" name="sprovience" class="form-text" value="<?php echo $_SESSION['sprovince'];?>" readonly></div>
                            <div class="col-md-6 col-sm-6"><label class="shiplabel"><?php echo $lcontry;?></label><input type="text" class="form-text" name="scountry" value="<?php echo $_SESSION['scountry'];?>" readonly></div>
                          </div>
                        <div class="clearfix"></div>
                        
                        <div class="space-2"></div>
               
                        <div class="clearfix"></div>
                        <div class="space-2"></div>
                          <div class="clearfix"></div>
                          <div class="form-section-title"> <b><?php echo $note; ?></b></div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6"><label class="shiplabel"><?php echo $note; ?></label><textarea class="form-text" name="note" id="note" cols="30" rows="2" class="form-control" readonly><?php if ($_SESSION['note'] != '') {echo $_SESSION['note'];} ?></textarea></div>
                        </div>

                            <div class="clearfix"></div>
                        
                    </div>
                         
     <?php           
          $countS = count($cart_items);
          $i=1;
            foreach($cart_items as $paypalvaribles){
               $countnum = $i++;
                $quanitity = $paypalvaribles->getQuanitity();
                $price = $paypalvaribles->getTotalPrice();
                 //$ship = $paypalvaribles->getShipping();
                 $payresult = MP\FormoneLangQuery::create()->filterByFormId($paypalvaribles->getProductId())->filterByLanguageId($lang_name)->findOne();
                 $name  = $payresult->getName();
                   ?>
                        <div id = "item_<?php echo $countnum; ?>" class = "itemwrap">
        <input name = "item_name_<?php echo $countnum; ?>" value = "<?php echo $name; ?>" type = "hidden">        
        <input name = "amount_<?php echo $countnum; ?>" value = "<?php echo $price; ?>" type = "hidden">       
    </div>
                
           <?php }?>
        <input type="hidden" name="cartcount" value="<?php echo $countS; ?>"/>                  
<!--       <input type="hidden" name="productprice" value="<?php //echo $_POST['productprice']; ?>"/>     
       <input type="hidden" name="finalamount" value="<?php //echo $_POST['finalamount']; ?>"/>      
       <input type="hidden" name="discount_id" value="<?php //echo $_POST['discount_id']; ?>"/>
       <input type="hidden" name="shipping" value="<?php //echo $_POST['shipping']; ?>"/>
       <input type="hidden" name="payment" value="<?php //echo $_POST['payment']; ?>"/>-->
       
       <?php
        if(isset($_POST['payment']) && $_POST['payment'] != ''){
          $_SESSION['payment'] = $_POST['payment'];
        }
        if(isset($_POST['shipping']) && $_POST['shipping'] != ''){
          $_SESSION['shipping'] = $_POST['shipping'];
        }
       ?>
                <input type="hidden" name="pay_options" value="<?php echo $_POST['payment']; ?>"/>
<!--          <div class="col-md-3 col-sm-6 continue"></div>-->
            <div class="col-md-3 col-sm-6 continue">
              <a href="<?php echo $base_url . '/' . $lang_name; ?>/spedizione">
                <span class="btn"><?php echo $back; ?></span>
              </a>
            </div>
            <div class="col-md-3 col-sm-6 payment">
                <button class="btn " type="submit" name="orderconfirm" value="confirm"><?php echo $buy; ?></button>
            </div>
   </form>
                 <?php } else{
          
          echo '<h4>'. $noproductsavail .'</h4>';?>
           
        
     <?php 
           }    
       }
     ?>
  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>

<code>
    
  
    
</code>
<?php
  include('footer.tpl.php');
?>