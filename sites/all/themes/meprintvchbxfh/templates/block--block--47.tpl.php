<?php
  global $base_url;
?>     
<header class="row">
    <div class="col-sm-2 col-md-2"></div>
    <div class="col-sm-8 col-md-8">
      <h3 class="text-center section-title">
          <span>
              <img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-right: 15px">
              <strong>MePrint</strong>
          </span>
          &nbsp; <?php echo t('TI OFFRE SEMPRE IL MEGLIO!'); ?>
          <span>
              <img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-left: 15px">
          </span>
      </h3>
    </div>
    <div class="col-sm-2 col-md-2"></div>
  </header>
  <article class="row">
    <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-dropbox fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Imballaggio'); ?></h4>
        <p><?php echo t('I nostri imballaggi costano pochi euro in più ma sono perfetti e non si rompono'); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-magic fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Colori luminosi'); ?></h4>
        <p><?php echo t('Stampe sempre perfette sia in esterno che in interno: colori brillanti per stampe più belle'); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-truck fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Spedizioni'); ?></h4>
        <p><?php echo t('Puoi scegliere quanto è urgente il tuo ordine, spedizioni anche in 24h su tutto il territorio nazionale'); ?></p>
      </div>
    </div>
    <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-print fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Tutti i materiali'); ?></h4>
        <p><?php echo t('Riusciamo a stampare su qualunque tipo di supporto rigido: forex, plexiglass, alluminio...'); ?></p>
      </div>
    </div>
   <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-thumbs-o-up fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Pagamenti'); ?></h4>
        <p>
            <img alt="paypal" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/paypal.png">
            <img alt="master-card" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/master-card.png">
            <img alt="visa" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/visa.png">
            <img alt="master" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/master.png">
            <img width="40px" height="28px" alt="cash" src="/sites/all/themes/meprint/images/check.jpg">
            <img width="35px" height="25px" alt="cash" src="/sites/all/themes/meprint/images/dollars.jpg">
            <img width="43px" height="25px" alt="sofort" src="/sites/all/themes/meprint/images/sofort.png">
        </p>
      </div>
    </div>
    <div class="col-sm-6 col-md-6">
      <span class="blue-bg_img"><i class="fa fa-euro fa-5x"></i></span>
      <div class="blue-bg_cont">
        <h4 class="h4"><?php echo t('Prezzi incredibili'); ?></h4>
        <p><?php echo t('I nostri prezzi sono tra i più bassi del mercato della stampa digitale online'); ?></p>
      </div>
    </div>
  </article>
    