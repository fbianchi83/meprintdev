<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('input[type=radio][name=interval]').change(function() {
        var value = $( 'input[name=interval]:checked' ).val();
        $('#dailychoice').addClass('hidden');
        $('#weeklychoice').addClass('hidden');
        $('#monthlychoice').addClass('hidden');
        $('#yearlychoice').addClass('hidden');
        $('#yearlycompchoice').addClass('hidden');
        $('#'+value).removeClass('hidden');
    });
    
    $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dailyend" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#dailystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $( "#weeklystart" ).datepicker({
      showAnim: "slideDown",
      showWeek: true,
      dateFormat: "dd/mm/yy",
      weekHeader: "W",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#weeklyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#weeklyend" ).datepicker({
      showAnim: "slideDown",
      showWeek: true,
      dateFormat: "dd/mm/yy",
      weekHeader: "W",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#weeklystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    var prevDate = new Date();
    $("#dailystart").datepicker("setDate",currentDate);
    $("#dailyend").datepicker("setDate",prevDate);
    $("#weeklystart").datepicker("setDate",currentDate);
    $("#weeklyend").datepicker("setDate",prevDate);
    filtradaily();
});

    
function filtradaily() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping+'&clientid='+clientid;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0&clientid=-1';
    jQuery.ajax({
        type:'POST',
        url:"/filterday",
        data:'datastart='+datestart+'&dataend='+dateend+morefilters,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $daterep; ?>');
                data.addColumn('number', '<?php echo $dayrevenue; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['revenue'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,dates[2]), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $dayrevenue; ?>',
                        subtitle: datastart[2]+'/'+datastart[1]+'/'+datastart[0]+' - '+dataend[2]+'/'+dataend[1]+'/'+dataend[0]+' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtraweekly() {
    var datestart= formatdate(jQuery('#weeklystart').val());
    var dateend= formatdate(jQuery('#weeklyend').val());
    
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping+'&clientid='+clientid;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0&clientid=-1';
    
    jQuery.ajax({
        type:'POST',
        url:"/filterweek",
        data:'datastart='+datestart+'&dataend='+dateend+morefilters,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $weekrep; ?>');
                data.addColumn('number', '<?php echo $weekrevenue; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['revenue'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,dates[2]), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $weekrevenue; ?>',
                        subtitle: datastart[2]+'/'+datastart[1]+'/'+datastart[0]+' - '+dataend[2]+'/'+dataend[1]+'/'+dataend[0]+' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtramonthly() {
    var datestart= jQuery('#monthlystart').val();
    var dateend= jQuery('#monthlyend').val();
    var monthstart= datestart.substr(4);
    var yearstart= datestart.substr(0,4);
    var monthend= dateend.substr(4);
    var yearend= dateend.substr(0,4);
    
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping+'&clientid='+clientid;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0&clientid=-1';
    
    jQuery.ajax({
        type:'POST',
        url:"/filtermonth",
        data:'monthstart='+monthstart+'&yearstart='+yearstart+'&monthend='+monthend+'&yearend='+yearend+morefilters,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $monthrevenue; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['revenue'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1, ''), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $monthrevenue; ?>',
                        subtitle: datastart[1]+'/'+datastart[0]+' - '+dataend[1]+'/'+dataend[0]+' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtrayearly() {
    var year= jQuery('#year').val();
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping+'&clientid='+clientid;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0&clientid=-1';
    
    jQuery.ajax({
        type:'POST',
        url:"/filteryear",
        data:'year='+year+morefilters,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $yearrevenue; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['revenue'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,''), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var options = {
                    chart: {
                        title: '<?php echo $yearrevenue; ?>',
                        subtitle: year +' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtrayearcomp() {
    var year1= jQuery('#yearstart').val();
    var year2= jQuery('#yearend').val();
    
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping+'&client='+clientid;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0&clientid=-1';
    
    jQuery.ajax({
        type:'POST',
        url:"/filteryear2",
        data:'yearstart='+year1+'&yearend='+year2+morefilters,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $yearrevenue; ?> '+year1);
                data.addColumn('number', '<?php echo $yearrevenue; ?> '+year2);
                i=0; j=12;
                while (j < result.length) {
                    revenue= result[i]['revenue'];
                    revenue2= result[j]['revenue'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date('',dates[1]-1,''), revenue, revenue2]);
                    i++; 
                    j++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var options = {
                    chart: {
                        title: '<?php echo $yearrevenuecomp; ?>',
                        subtitle: year1 + '-' + year2 + ' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function showfilters() {
    if (jQuery('#filters2').css('display') === 'block') 
        jQuery('#filters2').fadeOut("fast");
    else 
        jQuery('#filters2').fadeIn("fast");
}

</script>
<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $revenuestats; ?> </h3>
    <div class="billcentered"> 
        <input checked type='radio' class="margin10" id='daily' name='interval' value="dailychoice"> <?php echo $daily; ?>
        <input type='radio' class="margin10" id='weekly' name='interval' value="weeklychoice"> <?php echo $weekly; ?> 
        <input type='radio' class="margin10" id='monthly' name='interval' value="monthlychoice"> <?php echo $monthly; ?>
        <input type='radio' class="margin10" id='yearly' name='interval' value="yearlychoice"> <?php echo $yearly; ?> 
        <input type='radio' class="margin10" id='yearlyc' name='interval' value="yearlycompchoice"> <?php echo $yearlyc; ?> 
    </div>
    <div id='filterchoice' class="billcentered"> 
        <input type='button' id="shows" value='<?php echo t('Filters'); ?>' onclick='showfilters(); return false;'><br>
    </div>
    <div id="filters2" class="billcentered margin10" style="display:none">
        <?php echo t('Materials'); ?> 
        <select id='materialsel' class="margin10" >
            <option value='0'> <?php echo t('All Materials'); ?> </option>
        <?php 
        $materials = MP\MaterialsQuery::create()->find();
        foreach ($materials as $material) {
            $matname = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->select("name")->findOne();
            ?>
            <option value="<?php echo $material->getMaterialId(); ?>"> <?php echo $matname; ?> </option>
            <?php
        }
        ?>
        </select>
        <?php echo t('Payment Method'); ?> 
        <select id='paymethsel' class="margin10" >
            <option value='nopay'> <?php echo t('All Payment Methods'); ?> </option>
            <option value='paypal'> <?php echo t('Paypal/Credit Card'); ?> </option>
            <option value='banktransfer'> <?php echo t('Bank Transfer'); ?> </option>
            <option value='contrassegno'> <?php echo t('Cash on Delivery'); ?> </option>
            <option value='sofort'> <?php echo t('Sofort'); ?> </option>
        </select>
        <br><?php echo t('Shipping Companies'); ?> 
        <select id='shippingsel' class="margin10" >
            <option value='0'> <?php echo t('All Shipping Companies'); ?> </option>
        <?php 
        $shippings = MP\ShippingCompaniesQuery::create()->filterByStatus(1)->find();
        foreach ($shippings as $shipping) {
            ?>
            <option value="<?php echo $shipping->getShippingId(); ?>"> <?php echo $shipping->getName(); ?> </option>
                <?php
        }
        ?>
        </select>
        <?php echo t('Client'); ?> 
        <select id='clientsel' class="margin10" >
            <option value='-1'> <?php echo t('All Clients'); ?> </option>
            <option value='0'> <?php echo t('Anonymous'); ?> </option>
            <?php 
            $users = MP\UserQuery::create()->find();
            foreach ($users as $user) {
                if ($user->getUid() != 1) {
                    if ($user->getUserType()=="association" || $user->getUserType()=="society")
                        $name= $user->getCompanyName();
                    else
                        $name= $user->getName() . " " . $user->getSurname();
                    ?>
            <option value="<?php echo $user->getUid(); ?>"> <?php echo $name; ?> </option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
        
    
    <div id='dailychoice' class="billcentered"> 
        <?php echo $datestart; ?> <input type='text' class="margin10" id='dailystart'>
        <?php echo $dateend; ?> <input type='text' class="margin10" id='dailyend'> 
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtradaily(); return false;'>
    </div>
    
    <div id='weeklychoice' class="billcentered hidden"> 
        <?php echo $weekstart; ?> <input type='text' class="margin10" id='weeklystart'>
        <?php echo $weekend; ?> <input type='text' class="margin10" id='weeklyend'>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtraweekly(); return false;'>
    </div>
    
    <div id='monthlychoice' class="billcentered hidden"> 
        <?php echo $monthstart; ?> <select id='monthlystart' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $month = date('m', strtotime($startdate));
            $year = date('Y', strtotime($startdate));
            $ym = $year.$month;
            ?> <option value='<?php echo $ym; ?>'> <?php echo $month; ?>-<?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 month", strtotime($startdate)));
        }
        ?>
        </select>
        <?php echo $monthend; ?> <select id='monthlyend' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $month = date('m', strtotime($startdate));
            $year = date('Y', strtotime($startdate));
            $ym = $year.$month;
            ?> <option value='<?php echo $ym; ?>'> <?php echo $month; ?>-<?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 month", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtramonthly(); return false;'>
    </div>
    <div id='yearlychoice' class="billcentered hidden">
        <?php echo $yearrep; ?> <select id='year' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtrayearly(); return false;'>
    </div>
    <div id='yearlycompchoice' class="billcentered hidden">
        <?php echo $yearrepstart; ?> <select id='yearstart' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <?php echo $yearrepend; ?> <select id='yearend' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtrayearcomp(); return false;'>
    </div>
    
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={ 'modules':[{ 'name':'visualization', 'version':'1.1', 'packages':['line'] }] }"></script>
    
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
</div>