<?php
//print_r( $contacts );
global $base_url;
global $language;
?>

<table id="mp-list-contacts" class="table table-mp table-striped table-hover">
  <thead>
    <tr>
      <th class="hidden-xs"><?php echo $lusername; ?></th>
      <th><?php echo $lname; ?></th>
      <th class="hidden-xs"><?php echo $laddress; ?></th>  
      <th><?php echo $llocation; ?></th>
      <th class="hidden-xs"><?php echo $lprovince; ?></th>
      <th><?php echo $lcontry; ?></th>
      <th class="text-right"><?php echo t('Display'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $contacts as $row ): ?>
    <tr class="cnt-<?php echo $row->getId(); ?>">
      <td class="hidden-xs"><?php echo $row->getAddressBookName(); ?></td>
      <td><?php echo $row->getName() . ' ' . $row->getSurname(); ?></td>
      <td class="hidden-xs"><?php echo $row->getAddress(); ?></td>
      <td><?php echo $row->getCity(); ?></td>
      <td class="hidden-xs"><?php echo $row->getProvince(); ?></td>
      <td><?php echo getNationToCode( $row->getNation() ); ?></td>
      <td class="text-right order-detail"><a href="/<?php echo $language->language; ?>/user/rubrica/edit/<?php echo $row->getId() ; ?>"><i class="fa fa-file-text-o"></i></a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<a class="btn btn-orange" href="<?php echo $base_url . '/' . $language->language; ?>/user/rubrica/new"><?php echo $addcontact; ?></a>