<?php
global $base_url;
global $website;
global $language;

$lang_name = $language->language;
$website = $language->language;

$result = MP\FormoneAdditionalInfoQuery::create()
        ->filterByInHomePageFlag(1)
        ->useFormoneBigFormatQuery('a','left join')
            ->filterByStatus(1)
        ->endUse()
        //->filterByWebsiteId($website)
        //->orderByFormId('desc')
        ->orderByModifiedDate("DESC")
        ->limit(9)
        ->find();

foreach ($result as $homepage) {
    $id = $homepage->getFormId();
    $inhome = $homepage->getInHomePageFlag();

    //$beseprice = $fdata['base_price_for_quantity'];
    $promo = $homepage->getPromoFlag();

    $promoprice = $homepage->getPromotionPrice();
    $promotype = $homepage->getPromoPriceType();

    
    if($homepage->getImageListing() != 0 && $homepage->getImageListing() != ""){
      $ImgHome = file_load($homepage->getImageListing());
    }else{
      $ImgHome = file_load($homepage->getImage());
    }
    
    //$ImgHome = file_load($homepage->getImage());
    $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
    
    

    $res = MP\FormoneLangQuery::create()->filterByFormId($homepage->getFormId())->filterByLanguageId($lang_name)->findOne();
    $baseprice_q = MP\FormoneBigFormatQuery::create()->filterByFormId($homepage->getFormId())->findOne();
    $name = substr($res->getName(), 0, 25);
    $pos = strpos($res->getDescription(), " ", 130);
    if (strlen($res->getDescription()) < 130)
        $description = $res->getDescription();
    else {
        $description = substr($res->getDescription(), 0, $pos);
        if (strlen($res->getDescription()) > $pos)
            $description .= "...";
    }
    
    $baseprice = $baseprice_q->getBasePriceForQuantity();
    
    $prod_slug = $res->getSlug();    
    
    $prod_group = MP\ProductsgroupLangQuery::create()->filterByProductgroupId($baseprice_q->getProductgroupId())->filterByLanguageId($lang_name)->findOne();
    $prod_group_slug = $prod_group->getSlug();
    
    $prod_sub_group = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($baseprice_q->getSubcategoryId())->filterByLanguageId($lang_name)->findOne();
    $prod_sub_group_slug = $prod_sub_group->getSlug();
?>

    <div class="col-sm-4 col-md-4">
        <a href="<?php echo $base_url . '/'. $lang_name . '/' . $prod_group_slug . "/" . $prod_sub_group_slug . "/" . $prod_slug . "/pb-" . $id; ?>">
        <div class="products-Thumbnail">
            <div class="item item-type-line">  
              <span class="item-hover">
                <div class="item-info">
                  <div class="title"><b><?php echo $name; ?></b></div>
                  <div class="line"></div>
                  <div class="date"><?php echo $description; ?></div>
                  <!--<div class="date"><?php //echo $date;  ?></div>-->
                </div>
                <div class="mask"></div>
              </span>
              <div class="item-img"><img alt="" src="<?php echo $ImgHomePath; ?>" class="img-responsive"></div>
            </div>
            <?php if ($promo == 'Promo') { ?>
              <div class="products-title"><em><?php echo $promo; ?></em></div>
            <?php } ?>
            <?php if ($promo == 'Best Price') { ?>
              <div class="products-title2"><em><?php echo $bestprice; ?></em></div>
            <?php } ?>
            <?php if ($promo == 'New') { ?>
              <div class="products-title1"><em><?php echo $new; ?></em></div>
            <?php } ?>
            <div class="clr"></div>
            <h2><?php echo $name; ?></h2>
          <?php
            if ($promo == 'Promo') {
                $actualprice = $baseprice;
                if ($promotype == "value") {
                    $baseprice = $baseprice - $promoprice;
                } elseif ($promotype == "percentage") {
                    $promodisc = $promoprice * $baseprice;
                    $promodisc = $promodisc / 100;
                    $baseprice = $baseprice - $promodisc;
                }
                $actualprice= number_format((float)$actualprice, 2, ".","");
                $baseprice= number_format((float)$baseprice, 2, ".","");
          ?>
            <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
          <?php } else { 
              $baseprice= number_format((float)$baseprice, 2, ".","");
                ?>
              <p class="p">&euro; <?php echo $baseprice; ?></p>
          <?php } ?>
          </div>
        </a>
    </div>
<?php
}
$result1 = MP\FormtwoAdditionalInfoQuery::create()
        ->filterByInHomePageFlag(1)
        ->useFormtwoSmallFormatQuery('a','left join')
            ->filterByStatus(1)
        ->endUse()
        //->filterByWebsiteId($website)
        //->orderByFormId('desc')
        ->orderByModifiedDate("DESC")
        ->limit(9)
        ->find();

//$i = 0;



foreach ($result1 as $homepage) {
    $id = $homepage->getFormId();
    $inhome = $homepage->getInHomePageFlag();

    $beseprice = $fdata['base_price_for_quantity'];
    $promo = $homepage->getPromoFlag();

    $promoprice = $homepage->getPromotionPrice();
    $promotype = $homepage->getPromoPriceType();

    $ImgHome = file_load($homepage->getImage());
    $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);

    $res = MP\FormtwoLangQuery::create()->filterByFormId($homepage->getFormId())->filterByLanguageId($lang_name)->findOne();
    $baseprice_q = MP\FormtwoSmallFormatQuery::create()->filterByFormId($homepage->getFormId())->findOne();
    $name = substr($res->getName(), 0, 25);
    $pos = strpos($res->getDescription(), " ", 130);
                if (strlen($res->getDescription()) < 130)
                    $description = $res->getDescription();
                else {
                    $description = substr($res->getDescription(), 0, $pos);
                    if (strlen($res->getDescription()) > $pos)
                        $description .= "...";
                }
                $baseprice = $baseprice_q->getBasePriceForQuantity();
    
    $prod_slug = $res->getSlug();    
    /*
    $prod_group = MP\ProductsgroupLangQuery::create()->filterByProductgroupId($baseprice_q->getProductgroupId())->filterByLanguageId($lang_name)->findOne();
    $prod_group_slug = $prod_group->getSlug();
    
    $prod_sub_group = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($baseprice_q->getSubcategoryId())->filterByLanguageId($lang_name)->findOne();
    $prod_sub_group_slug = $prod_sub_group->getSlug();
     *
     */
?>

    <div class="col-sm-4 col-md-4">
        <a href="<?php echo $base_url . '/' . $lang_name; ?>/piccolo-formato/<?php echo $prod_slug .  "/ps-" . $id; ?>">
        <div class="products-Thumbnail">
            <div class="item item-type-line">  
              <span class="item-hover">
                <div class="item-info">
                  <div class="title"><b><?php echo $name; ?></b></div>
                  <div class="line"></div>
                  <div class="date"><?php echo $description; ?></div>
                  <!--<div class="date"><?php //echo $date;  ?></div>-->
                </div>
                <div class="mask"></div>
              </span>
              <div class="item-img"><img alt="" src="<?php echo $ImgHomePath; ?>" class="img-responsive"></div>
            </div>
            <?php if ($promo == 'Promo') { ?>
              <div class="products-title"><em><?php echo $promo; ?></em></div>
            <?php } ?>
            <?php if ($promo == 'Best Price') { ?>
              <div class="products-title2"><em><?php echo $bestprice; ?></em></div>
            <?php } ?>
            <?php if ($promo == 'New') { ?>
              <div class="products-title1"><em><?php echo $new; ?></em></div>
            <?php } ?>
            <div class="clr"></div>
            <h2><?php echo $name; ?></h2>
          <?php
            if ($promo == 'Promo') {
                $actualprice = $baseprice;
                if ($promotype == "value") {
                    $baseprice = $baseprice - $promoprice;
                } elseif ($promotype == "percentage") {
                    $promodisc = $promoprice * $baseprice;
                    $promodisc = $promodisc / 100;
                    $baseprice = $baseprice - $promodisc;
                }
                $actualprice= number_format((float)$actualprice, 2, ".","");
                $baseprice= number_format((float)$baseprice, 2, ".","");
          ?>
            <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
          <?php } else { 
              $baseprice= number_format((float)$baseprice, 2, ".","");
              ?>
              <p class="p">&euro; <?php echo $baseprice; ?></p>
          <?php } ?>
          </div>
        </a>
    </div>
<?php
}