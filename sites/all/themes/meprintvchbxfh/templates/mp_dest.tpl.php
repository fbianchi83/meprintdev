<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    ?>

    <!--CONTENUTO NODO-->

    <?php  
    
    global $base_url;
    global $language;
    global $website;
    
    $noproduct = t('No Products Avalible');
    $sortbycat = t('Select Sub Category');
    $selsubcat = t('All');
    
    $lang_name = $language->language;
    $website = $language->language;
    ?>
      <section class="row">
        <?php

        $listing_query = MP\FormoneBigFormatQuery::create()
                        ->filterByDestinationId($dest_id)
                         //->joinFormoneAdditionalInfo()
                         //->where('FormoneAdditionalInfo.WebsiteId = ?', $website)
                         //->where('FormoneAdditionalInfo.PromoFlag = ?', "Promo")
                         ->filterByStatus(1)
                         ->find();
        $listing_query2 = MP\FormtwoSmallFormatQuery::create()
                        ->filterByDestinationId($dest_id)
                         //->joinFormoneAdditionalInfo()
                         //->where('FormoneAdditionalInfo.WebsiteId = ?', $website)
                         //->where('FormoneAdditionalInfo.PromoFlag = ?', "Promo")
                         ->filterByStatus(1)
                         ->find();
        
        if(count($listing_query)>0){

            foreach($listing_query as $listing){

                $id = $listing->getFormId();
                $check_lang = MP\FormoneLangQuery::create()
                    ->where('FormoneLang.LanguageId =?', $lang_name)
                    ->where('FormoneLang.FormId =?', $id)
                    ->findOne();

                $name = substr($check_lang->getName(), 0, 25);
                $prod_slug = $check_lang->getSlug();
                $pos = strpos($check_lang->getDescription(), " ", 130);
                if (strlen($check_lang->getDescription()) < 130)
                    $description = $check_lang->getDescription();
                else {
                    $description = substr($check_lang->getDescription(), 0, $pos);
                    if (strlen($check_lang->getDescription()) > $pos)
                        $description .= "...";
                }
                $date = $listing->getCreatedDate();
                $baseprice = $listing->getBasePriceForQuantity();

                $check = MP\FormoneAdditionalInfoQuery::create()
                        //->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                        ->where('FormoneAdditionalInfo.FormId =?', $id)
                        ->findOne();
                
                    $promo = $check->getPromoFlag();
                    $promoprice = $check->getPromotionPrice();
                    $promotype = $check->getPromoPriceType();
                    $ImgHome = file_load($check->getImage());
                    $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
                    
                    
                    $subgrp = MP\ProductsubgroupLangQuery::create()
                              ->filterByLanguageId($lang_name)
                              ->filterByProductsubgroupId($listing->getSubcategoryId())
                              ->filterByStatus(1)
                              ->findOne();
                    $subslug = $subgrp->getSlug();
                    
                    
                    $pgrp = MP\ProductsgroupLangQuery::create()
                               ->filterByLanguageId($lang_name)
                               ->filterByProductgroupId($subgrp->getProductgroupId())
                               ->filterByStatus(1)
                                ->findOne();
                    
                    //$pro_slug = $pgrp->getSlug();
                    
                    $pro_slug = "";
                    if($pgrp){
                        $pro_slug = $pgrp->getSlug();
                    }
                    
                    ?>
                    <div class="col-sm-4 col-md-4 ">
                      <a href="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $id; ?>">
                        <div class=" products-Thumbnail">
                          <div class="item item-type-line">
                            <span class="item-hover">
                              <div class="item-info">
                                <div class="date"><?php echo $name; ?></div>
                                <div class="line"></div>
                                <div class="date"><?php echo $description; ?></div>
                              </div>
                              <div class="mask"></div>
                            </span>

                            <div class="item-img">
                                <img class="img-responsive" alt="" src="<?php echo $ImgHomePath; ?>"></div>
                            </div>
                            <?php if ($promo == 'Promo') { ?>
                              <div class="products-title"><em><?php echo $promo; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'Best Price') { ?>
                              <div class="products-title2"><em><?php echo $bestprice; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'New') { ?>
                              <div class="products-title1"><em><?php echo $new; ?></em></div>
                            <?php } ?>
                            <div class="clr"></div>
                            <p><?php echo $name; ?></p>
                    <?php
                    if ($promo == 'Promo') {
                        $actualprice = $baseprice;
                        if ($promotype == "value") {
                            $baseprice = $baseprice - $promoprice;
                        } elseif ($promotype == "percentage") {
                            $promodisc = $promoprice * $baseprice;
                            $promodisc = $promodisc / 100;
                            $baseprice = $baseprice - $promodisc;
                            $baseprice= number_format((float)$baseprice, 2, ".","");
                            $actualprice= number_format((float)$actualprice, 2, ".","");
                        
                        }
                        ?>
                            <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
                    <?php }
                    else {$baseprice= number_format((float)$baseprice, 2, ".","");
                        
                        ?>
                            <p class="p">&euro; <?php echo $baseprice; ?></p><?php } ?>
                        </div>
                      </a>
                    </div>
                    <?php
                }
                foreach($listing_query2 as $listing2) {
                    $id = $listing2->getFormId();
                    $check_lang = MP\FormtwoLangQuery::create()
                    ->where('FormtwoLang.LanguageId =?', $lang_name)
                    ->where('FormtwoLang.FormId =?', $id)
                    ->findOne();

                    if ($check_lang) {
                        $name = substr($check_lang->getName(), 0, 25);
                        $prod_slug = $check_lang->getSlug();
                        $pos = strpos($check_lang->getDescription(), " ", 130);
                        if (strlen($check_lang->getDescription()) < 130)
                            $description = $check_lang->getDescription();
                        else {
                            $description = substr($check_lang->getDescription(), 0, $pos);
                            if (strlen($check_lang->getDescription()) > $pos)
                                $description .= "...";
                        }
                        $baseprice = $listing2->getBasePriceForQuantity();
                    }
                    $date = $listing2->getCreatedDate();
                            
                    $check = MP\FormtwoAdditionalInfoQuery::create()
                        ->where('FormtwoAdditionalInfo.WebsiteId =?', $website)
                        ->where('FormtwoAdditionalInfo.FormId =?', $id)
                        ->findOne();
                
                    if ($check) {
                        $promo = $check->getPromoFlag();
                        $promoprice = $check->getPromotionPrice();
                        $promotype = $check->getPromoPriceType();
                        $ImgHome = file_load($check->getImage());
                        $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
                    }
                    
                    $subgrp = MP\ProductsubgroupLangQuery::create()
                              ->filterByLanguageId($lang_name)
                              ->filterByProductsubgroupId($listing->getSubcategoryId())
                              ->filterByStatus(1)
                              ->findOne();
                    $subslug = $subgrp->getSlug();
                    
                    
                    $pgrp = MP\ProductsgroupLangQuery::create()
                               ->filterByLanguageId($lang_name)
                               ->filterByProductgroupId($subgrp->getProductgroupId())
                               ->filterByStatus(1)
                                ->findOne();
                    
                    //$pro_slug = $pgrp->getSlug();
                    $pro_slug = "";
                    if($pgrp){
                        $pro_slug = $pgrp->getSlug();
                    }
                    ?>
                    <div class="col-sm-4 col-md-4 ">
                      <a href="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $id; ?>">
                        <div class=" products-Thumbnail">
                          <div class="item item-type-line">
                            <span class="item-hover">
                              <div class="item-info">
                                <div class="date"><?php echo $name; ?></div>
                                <div class="line"></div>
                                <div class="date"><?php echo $description; ?></div>
                              </div>
                              <div class="mask"></div>
                            </span>

                            <div class="item-img">
                                <img class="img-responsive" alt="" src="<?php echo $ImgHomePath; ?>"></div>
                            </div>
                            <?php if ($promo == 'Promo') { ?>
                              <div class="products-title"><em><?php echo $promo; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'Best Price') { ?>
                              <div class="products-title2"><em><?php echo $bestprice; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'New') { ?>
                              <div class="products-title1"><em><?php echo $new; ?></em></div>
                            <?php } ?>
                            <div class="clr"></div>
                            <p><?php echo $name; ?></p>
                    <?php
                    if ($promo == 'Promo') {
                        $actualprice = $baseprice;
                        if ($promotype == "value") {
                            $baseprice = $baseprice - $promoprice;
                        } elseif ($promotype == "percentage") {
                            $promodisc = $promoprice * $baseprice;
                            $promodisc = $promodisc / 100;
                            $baseprice = $baseprice - $promodisc;
                        }
                        $baseprice= number_format((float)$baseprice, 2, ".","");
                        $actualprice= number_format((float)$actualprice, 2, ".","");
                        
                        ?>
                            <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
                    <?php }
                    else {$baseprice= number_format((float)$baseprice, 2, ".","");
                        
                        ?>
                            <p class="p">&euro; <?php echo $baseprice; ?></p><?php } ?>
                        </div>
                      </a>
                    </div>
                    <?php
                }
            ?>
      </section>
  <?php }else{ ?>
            <div class="col-md-12 col-sm-12 well well-lg">
              <?php echo $noproduct;?>
            </div>
  <?php } ?>

  <!--FINE CONTENUTO NODO-->
</div>

</div>

