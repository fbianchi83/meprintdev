
<div class="col-md-12 col-sm-12 col-xs-12 cartdetailspage cart_<?php echo $cart_item_id; ?>">
  <div class="clearfix"></div>
  <div class="col-md-4 col-sm-4 col-xs-7">
    <div class="cart_4">
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails_4">
        <b>
          <span class="fa cartdetails--open fa-plus fa-2x" data-toggle="collapse" href="#cart_<?php echo $cart_item_id; ?>" aria-expanded="false"></span> <?php echo $presult->getName(); ?>
        </b>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 text-center hidden-xs">
    <?php
    $images = MP\CartItemFileQuery::create()->filterByCartItemId($cart_item_id)->findOne();
    ?>
    <?php
    if ($images) {
      $image = $images->getFid();
      $image_uri = file_load($image)->uri;
      ?>
      <img src="<?php echo image_style_url("thumbnail", $image_uri); ?>" />
      <?php
    }
    else {
      echo '<i class="fa fa-camera"></i>';
    }
    ?>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
    <div class="col-md-6 col-sm-6 cartdetails text-center">
      <?php echo $cart_item->getQuanitity() ?>
    </div>
    <div class="col-md-6 col-sm-3 cartdetails text-right">
      <div class="col-md-12 col-sm-12 delete_icon cartdetails">
        <a class="deletecart" url="<?php echo $cart_item_id; ?>">
          <i class="fa fa-trash-o"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 col-xs-3 cartdetailsline">
    <div class="cartdetails cartprice text-right">
      <?php
      $totlprc = $cart_item->getTotalPrice();
      echo number_format((float) $totlprc, 2, '.', '') . ' &euro;';
      ?>
    </div>
  </div>
  
  <div class="col-md-12 col-xs-12">
    <div class="collapse" id="cart_<?php echo $cart_item_id; ?>">

      <!-- NUMERO DI COPIE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $nocopies; ?>
        </span>
        <span class="datacart">
          <?php echo $cart_item->getQuanitity() ?>
        </span>
      </div>

      <!-- DIMENSIONI -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $cdimension; ?>
        </span>
        <span class="datacart">
          <?php
          if ($cart_item->getDimensionId() != "custom") {
            $dquery = MP\DimensionsQuery::create()->filterByDimensionId($cart_item->getDimensionId())->findOne();
            echo $dquery->getWidth() . '*' . $dquery->getHeight();
          }
          else {
            echo $cart_item->getWidth() . "*" . $cart_item->getHeight();
          }
          ?>
        </span>
      </div>

      <!-- MATERIALE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $cmaterial; ?>
        </span>
        <span class="datacart">
          <?php
          $mquery = MP\MaterialsLangQuery::create()->filterByMaterialId($cart_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
          echo $mquery->getName();
          ?>
        </span>
      </div>

      <?php if ($cart_item->getProcessingId() != 11) { ?>
      <!-- LAVORAZIONE BASE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $processing; ?>
        </span>
        <span class="datacart">
          <?php
          $pquery = MP\ProcessingLangQuery::create()->filterByProcessingId($cart_item->getProcessingId())->filterByLanguageId($lang_name)->findOne();

          $base_processing = MP\ProcessingQuery::create()->findPk($cart_item->getProcessingId());

          echo $pquery->getName();

          $is_special = $base_processing->getIsSpecial();

          $is_extra_side_req = $base_processing->getIsExtra();

          if ($is_extra_side_req == 1) {
            if ($cart_item->getProcessingTop() == 1) {
              echo " " . t("Top side") . " ";
            }
            if ($cart_item->getProcessingBottom() == 1) {
              echo " " . t("Bottom side") . " ";
            }
            if ($cart_item->getProcessingLeft() == 1) {
              echo " " . t("Left side") . " ";
            }
            if ($cart_item->getProcessingRight() == 1) {
              echo " " . t("Right side") . " ";
            }
          }

          if ($is_special == 1) {
            $special_type = $base_processing->getSpecialType();
            echo t("Distance") . ": " . $cart_item->getProcessingDistanceSpecial() . " cm";
            echo " " . t("Number") . ": " . $cart_item->getProcessingNumberSpecial();
          }
          ?>
        </span>
      </div>
      <?php } ?>

      <?php
      //EVENTUALI LAVORAZIONI EXTRA
      $cart_item_extra_processings = MP\CartItemExtraProcessingQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
      if (count($cart_item_extra_processings) > 0) {
        ?>

        <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_extra_processings) > 1 ) ? 'cartdetails--more' : ''; ?>" >
          <span class="labelcart">
            <?php echo $extraprocessing; ?>
          </span>

          <?php
          foreach ($cart_item_extra_processings as $cart_item_extra_processing) {
            ?>
            <span class="datacart">
              <?php
              $extra_processing_lang = MP\ProcessingLangQuery::create()->filterByProcessingId($cart_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
              $extra_processing = MP\ProcessingQuery::create()->findPk($cart_item_extra_processing->getProcessingId());

              $is_special = $extra_processing->getIsSpecial();

              $is_extra_side_req = $extra_processing->getIsExtra();

              echo $extra_processing_lang->getName();


              if ($is_extra_side_req == 1) {
                if ($cart_item_extra_processing->getExtraProcessingTop() == 1) {
                  echo ", <em>" . t("Top side") . "</em>";
                }
                if ($cart_item_extra_processing->getExtraProcessingBottom() == 1) {
                  echo ", <em>" . t("Bottom side") . "</em>";
                }
                if ($cart_item_extra_processing->getExtraProcessingLeft() == 1) {
                  echo ", <em>" . t("Left side") . "</em>";
                }
                if ($cart_item_extra_processing->getExtraProcessingRight() == 1) {
                  echo ", <em>" . t("Right side") . "</em>";
                }
              }

              if ($is_special == 1) {
                $special_type = $extra_processing->getSpecialType();
                echo ', <em>' . t("Distance") . ": " . $cart_item_extra_processing->getDistanceSpecial() . " cm</em>";
                echo ", <em>" . t("Number") . ": " . $cart_item_extra_processing->getNumberSpecial() . " </em>";
              }
              ?>
            </span>
            <?php
          }
          ?>     
        </div>
        <?php
      }
      ?>

      <?php
      //EVENTUALE STRUTTURA
      if ($cart_item->getStructureId() != 0) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $accessorie_base; ?>
          </span>
          <span class="datacart">
            <?php
            $structure = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item->getStructureId())->filterByLanguageId($lang_name)->findOne();
            $structure_name = $structure->getName();
            echo $structure_name;
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <?php
      //EVENTUALI ACCESSORI
      $cart_item_accessories = MP\CartItemAccessoryQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
      if (count($cart_item_accessories) > 0) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_accessories) > 1 ) ? 'cartdetails--more' : ''; ?>"">
             <span class="labelcart">
                 <?php echo $accessorie; ?>
          </span>

          <?php
          foreach ($cart_item_accessories as $cart_item_accessory) {
            ?>
            <span class="datacart">
              <?php
              $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
              $accessory_name = $accessory_lang->getName();
              $accessory_quantity = $cart_item_accessory->getQuantity();
              echo $accessory_name . " (" . $accessory_quantity . ")";
              ?>
            </span>
            <?php
          }
          ?>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE VERIFICA FILES -->
      <?php
      if ($cart_item->getOperatorReview() == 1) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $operatorreview . ' &euro;' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?>
          </span>
          <span class="datacart">
            <?php
            echo t("Yes");
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE PACCO ANONIMO -->
      <?php
      if ($cart_item->getAnonymousPack() == 1) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $anonoshiping; ?>
          </span>
          <span class="datacart">
            <?php
            echo t("Yes");
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE NOME PREVETIVO -->
      <?php
      if ($cart_item->getName() != "") {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $lname; ?>
          </span>
          <span class="datacart">
            <?php
            echo $cart_item->getName();
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALI NOTE PREVETIVO -->
      <?php
      if ($cart_item->getNote() != "") {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $note; ?>
          </span>
          <span class="datacart">
            <?php
            echo $cart_item->getNote();
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- DATA DI SPEDIZIONE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $Shippingdate; ?>
        </span>
        <span class="datacart">
          <?php
          $cDATE = $cart_item->getShippingDate();
          echo $cDATE->format('d-m-Y');
          ?>
        </span>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

