
<?php
global $base_url;
global $language;

$public_path = base_path() . variable_get('file_public_path', conf_path() . '/files');

//include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
$lang_name = $language->language;

$id = $prod_id;

$precord = MP\FormoneLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->findOne();


/*
 * INIZIO SEO
 */

//IMPOSTO IL TITOLO SEO
$title_seo = $precord->getTitleSeo();
drupal_set_title($title_seo);

//IMPOSTO LA DESCRIZIONE SEO
$description_seo = $precord->getDescriptionSeo();
$data = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'name' => 'description',
    'content' => $description_seo,
  ),
);
drupal_add_html_head($data, 'big_format');

/*
 * FINE SEO
 */

$bigresult_findone = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->findOne();
$bigid= "big".$id;
$processing_id = $bigresult_findone->getProcessingId();

//$info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->findOne();
$info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id)->findOne();
$product_material = MP\FormoneMaterialsQuery::create()->filterByFormId($id)->find();
$extra_processing = MP\FormoneExtraProcessingQuery::create()->filterByFormId($id)->find();
$formone_dimensions = MP\FormoneDimensionsQuery::create()->filterByFormId($id)->find();
//$formone_accessories = MP\FormoneAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();
$formone_accessories = MP\FormoneAccessoriesQuery::create()->filterByFormId($id)->find();

//$cost_copies = MP\FormoneCostCopiesQuery::create()->filterByFormId($id)->find();
$longtimeprod = MP\FormoneLongtimeProductionQuery::create()->filterByFormId($id)->find();
$countlongtimeprod = MP\FormoneLongtimeProductionQuery::create()->filterByFormId($id)->count();
//$countformone_accessories = MP\FormoneAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->count();
$countformone_accessories = MP\FormoneAccessoriesQuery::create()->filterByFormId($id)->count();
$packingpricedets = MP\ExtrapricePackageQuery::create()->find();

$mystr = "";

foreach ($packingpricedets as $packingdets) {
  $packingvalues = $packingdets->getFromSqm() . "+" . $packingdets->getToSqm() . "+" . $packingdets->getPrice() . "__";
}

$mystr .= '<input type="hidden" value="' . $bigresult_findone->getAdditionalPriceForCopy() . '"  id="additonal_price_copy">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getIncreasePercentage() . '" id="incr_percentage">';
$mystr .= '<input type="hidden" value="1" id="lessthan">';
$mystr .= '<input type="hidden" value="0" id="beforenoon">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getBasePriceForQuantity() . '" id="base_price">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getProductionDelay() . '"  id="production_delay">';


/*
 * Informazioni di setup per l'editor
 */
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getEditorDpi() . '"  id="editor_dpi">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getEditorCuttingGrid() . '"  id="editor_cutting_grid">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getEditorColorsType() . '"  id="editor_colors_type">';
$mystr .= "<input type='hidden' value='" . $bigresult_findone->getEditorColors() . "'  id='editor_colors'>";

$addtnl_prc_cpy = $bigresult_findone->getAdditionalPriceForCopy();
$free_dimesions = $bigresult_findone->getFreeDimensionsAvailable();
$productgroup_id = $bigresult_findone->getProductgroupId();
$dim_wgt = $bigresult_findone->getVolumeWeightPriceCalculation();

$packingpricecalc = $bigresult_findone->getPackagingPriceCalculation();

if($productgroup_id == 14){//prespaziato
  $mystr .= '<input type="hidden" value="0" id="use_external_image">';
}else{
  $mystr .= '<input type="hidden" value="1" id="use_external_image">';
}

$mystr .= '<input type="hidden" value="' . $dim_wgt . '" id="dimwgt">';
$mystr .= '<input type="hidden" value="' . $id . '" id="formid">';
$mystr .= '<input type="hidden" value="' . $productgroup_id . '" id="productgroupid">';
$mystr .= '<input type="hidden" value="' . $lang_name . '" id="languageid">';
$mystr .= '<input type="hidden" value="' . $packingpricecalc . '" id="packingpricecheck">';
$mystr .= '<input type="hidden" value="' . $packingvalues . '" id="packingpricevalues">';
$mystr .= '<input type="hidden" value="0" id="plus_file">';
$mystr .= '<input type="hidden" value="'. $VAT_RATE .'" id="vat_iva">';
$mystr .= '<input type="hidden" value="'. $FILE_REVIEW_COST .'" id="file_review_cost">';

$workload_dets = MP\ProductsGroupQuery::create()->filterByProductgroupId($productgroup_id)->find();
foreach ($workload_dets as $workloadinfo) {
  $workloadtype = $workloadinfo->getWorkloadType();
  $workloadlimit = $workloadinfo->getWorkloadLimit();
  $workloaddaily = $workloadinfo->getWorkloadFirstdate();
  $mystr .= '<input type="hidden" value="' . $workloadtype . '" id="workloadtype">';
  $mystr .= '<input type="hidden" value="' . $workloadlimit . '" id="workloadlimit">';
  $mystr .= '<input type="hidden" value="' . $workloaddaily . '" id="workloaddaily">';
}

$image = $info->getImage();
$my_image = file_load($image)->uri;
$mystr .= '<input type="hidden" value="' . $info->getPromoFlag() . '" id="promotionalFlag">';
$mystr .= '<input type="hidden" value="' . $info->getPromoPriceType() . '" id="promotionalType">';
$mystr .= '<input type="hidden" value="' . $info->getPromotionPrice() . '" id="promotionalValue">';

$images = json_decode($info->getImages(), true);

//ESTRAGGO GALLERIE
$galleries = json_decode($info->getGalleries());

if (count($galleries) > 0) {
  foreach ($galleries as $gallery) {
    $gallery_lang[$gallery] = MP\GalleryImagesLangQuery::create()->filterByGalleryImageId($gallery)->filterByLanguageId($lang_name)->findOne();
    $gallery_imges = MP\GalleryImagesImgQuery::create()->filterByGalleryImageId($gallery)->findOne();
    $gallery_imgs[$gallery] = json_decode($gallery_imges->getImage());
  }
}
//FINE GALLERIE
$notdone= 1;

$materialmodals = Array();
foreach($product_material as $material) {
    $materialmodal = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->findOne();
    
    $uri_image = file_load($materialmodal->getErrorMaxSideImage())->uri;    
    if( $uri_image ){     
      $tmp_path_image = str_replace('public://', '', $uri_image);
      $path_image = $base_url . '/sites/default/files/'.$tmp_path_image;
    } else {
      $path_image = '';
    }
    $materialmodals[] = array(
        'id' => $materialmodal->getMaterialId(),
        'text' => $materialmodal->getErrorMaxSideText(),
        'img' => $path_image
    );
}
include_once('dimensions_modal.tpl.php');
$files = $bigresult_findone->getNumberOfFilesTobeUploaded();    
    
?>
<!--Title -->

<a style="cursor:pointer; display:none" id="modalclick" data-toggle="modal" data-target="#dimensionModal"></a>
<form method="POST" enctype="multipart/form-data" id="product-big-format" action="<?php echo base_path() . $lang_name; ?>/cart" autocomplete="off" >
    
  <input type="hidden" name="productid" value="<?php echo $id; ?>" id="productid">
  <h1 class="title-product">
    <?php print($precord->getName()); ?> &nbsp 
    <span>
        <img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image">
        
          <?php if ($info->getPromoFlag() == 'Promo') { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/promo.png" alt="image">
          <?php } ?>
          <?php if ($info->getPromoFlag() == 'Best Price') { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/best_price.png" alt="image">
          <?php } ?>
    </span>
    <?php if( user_is_logged_in() ) : ?>
    <?php
    $inwish= MP\WishlistQuery::create()->filterByUserId($user->uid)->filterByFormId($id)->filterByFormType("big")->filterByStatus(1)->count();
    if ($inwish == 0) { ?>
    <a id="add-prefer" data-form-type="big" data-form-id="<?php echo $id; ?>" class="add-prefer hidden-xs pull-right">
      <i class="fa fa-heart"></i><span class="hidden-sm"><?php echo t('add prefer');?></span>
    </a>
    <?php } ?>
    
    <?php endif; ?>
  </h1>
  <input type="hidden" id="minQuantity" name="minQuantity" value="<?php echo $bigresult_findone->getMinCopies(); ?>">
  <div class="top-carousel">
    <div><img src="<?php echo image_style_url("product-carousel", $my_image); ?>" /></div>
<?php
if(count($images) > 0){
  foreach ($images as $img) {
    $my_image_gal = file_load($img)->uri;
  ?>
      <div><img src="<?php echo image_style_url("product-carousel", $my_image_gal); ?>" /></div>
      <?php
  }
}
    ?>
  </div>

  <?php if(count($galleries) > 0) { ?>
  <div class="clearfix" >
    <h2 class="title-long-desc"><?php echo $productdesc; ?></h2>
    <a href="pb-<?php echo $id; ?> #gallerymod > *" rel="lightmodal[|height:450px;width:850px]" class="gallerydiv gallerydiv-xs hidden-xs"> 
      <img class="floatedleft" src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/pics.jpg">
      <span class="hidden-sm hidden-xs"><?php echo t('Gallery Available'); ?> <font color="#ed7400"><?php echo "MEPRINT"; ?> <i class="fa fa-info-circle meprintcircle"></i> </font></span>
    </a>
    <?php if( user_is_logged_in() ) : ?>
      <a id="add-prefer" data-form-type="big" data-form-id="<?php echo $id; ?>" class="add-prefer add-prefer-xs pull-right visible-xs">
        <i class="fa fa-heart"></i>
      </a>
    <?php endif; ?>
  </div>
  <div id="gallerymod" style="display: none">
      <img src="<?php echo $public_path;?>/gallerybanner.jpg">
      <p class="gallerytext"><?php echo t("Su questo prodotto puoi usufruire della nostra galleria di immagini, che potrai modificare con il nostro avanzato editor!"); ?></p>
  </div>
   <?php } else { ?>
  <div><h2 class="fullwidth" class="title-long-desc"><?php echo $productdesc; ?></h2></div>
   <?php } ?>
  
  <div class="description-product clearfix">
    <?php echo $precord->getDescription(); ?>
  </div>
  <?php 
  $addinfobig= MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id)->findOne();
  if ($addinfobig->getUrlVideoPage() != "") { ?>
    <a href="<?php echo $addinfobig->getUrlVideoPage(); ?>" target="_blank"><i class="fa fa-youtube" style="font-size:16px; color: black;"></i> <?php echo t('Link to video'); ?></a>
  <?php } ?>
  <div class="space-3"></div>
  <div class="space-3"></div>
  <div class="space-4"></div>
  <div class="order-view-hd"><?php echo $ordertitle; ?> </div>

  <?php
  //controllo che il prodotto sia disponibile con quantità libera
  if ($bigresult_findone->getIsPresetAmount() == 0) {
    ?>

    <!-- QUANTITA' LIBERA -->
    <div class="order-view-top section-quantity">
      <h4 class="order-h"><?php echo $quanitity; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 1"); ?></p>
      <div class="form-group col-md-5 col-sm-5">
          <input type="text" name="quanitity" class="form-control " id="productQuanity" value="1" min="1" >
      </div>
      <div class="form-group col-md-7 col-sm-7">
      </div>
    </div>
    <!-- FINE QUANTITA' LIBERA -->

    <?php
  }
  else {
    $amounts = json_decode($bigresult_findone->getAmounts());
    $amounts_discounts = json_decode($bigresult_findone->getAmountsDiscounts());
    ?>
    <input type="hidden" name="quanitity" class="form-control " id="productQuanity" value="<?php echo $amounts[0]; ?>">
    <?php
  }
  ?>

  <!-- MATERIALI -->  
  <div class="order-view-top section-material">
    <h4 class="order-h"><?php echo $tmaterial; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 2"); ?></p>
        
    <?php
    if( count($product_material) > 1 ) : ?>

      <div class="carousel">
        <?php
        $i_mt = 1;

        foreach ($product_material as $material) {

          $big_material = $material->getMaterialId();
          $material_one = MP\MaterialsLangQuery::create()->filterByMaterialId($big_material)->filterByLanguageId($lang_name)->findOne();

          $material_obj = MP\MaterialsQuery::create()->findPk($big_material);
          $material_img_fid = $material_obj->getImage();
          $material_img_file = file_load($material_img_fid);
          $material_img_uri = $material_img_file->uri;

          $mat_desc = htmlentities($material_one->getDescription());
          ?>
          <div class="text-center">
            <label class="dimdiv dimdiv--circle">
              <input type="radio" name="material" class="material_class" id="material<?php echo $big_material; ?>" value="<?php echo $big_material; ?>" <?php if ($i_mt == 1) { ?> checked="checked" <?php } ?> />
              <img src="<?php echo image_style_url("thumbnail-new", $material_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $material_img_uri); ?>' alt='' /><p><?php echo $mat_desc; ?></p></div>" />
              <p><?php echo $material_one->getName(); ?></p>
              <span id="tooltip-inline" class="visible-sm visible-xs"><?php echo strip_tags($material_one->getDescription()); ?></span>
            </label>
            <?php
            
            $materials_price_values = MP\MaterialsQuery::create()->filterByMaterialId($big_material)->find();
            
            foreach ($materials_price_values as $mat_price) {
              if ($mat_price->getShortSideMaxLength() != 0) {
                $mystr .= '<input type="hidden" name="dbheight1-' . $big_material . '" value="1">';
                $mystr .= '<input type="hidden" name="dbheight2-' . $big_material . '" value="' . round($mat_price->getShortSideMaxLength()) . '">';
              }

              if ($mat_price->getLongSideMaxLength() != 0) {
                $mystr .= '<input type="hidden" name="dbwidth1-' . $big_material . '" value="1">';
                $mystr .= '<input type="hidden" name="dbwidth2-' . $big_material . '" value="' . round($mat_price->getLongSideMaxLength()) . '">';
              }
              
              if ($mat_price->getDeterminationCostCopies() == 2) {
                $cost_copies = MP\MaterialsCostCopiesQuery::create()->filterByMaterialId($big_material)->find();
                foreach ($cost_copies as $crecord) {
                    $cc .= $crecord->getFromCost() . " " . $crecord->getToCost() . " " . $crecord->getPrice() . "+";
                }
                $mystr .= '<input type="hidden" value="' . $cc . '" id="costCopies'.$big_material.'">';
              }
              if ($mat_price->getDeterminationCostCopies() == 3) {
                $cost_copies = MP\MaterialsCostPackageQuery::create()->filterByMaterialId($big_material)->find();
                foreach ($cost_copies as $crecord) {
                    $cc .= $crecord->getFromCost() . " " . $crecord->getToCost() . " " . $crecord->getPrice() . "+";
                }
                $mystr .= '<input type="hidden" value="' . $cc . '" id="costCopies'.$big_material.'">';
              }
              $mystr .= '<input type="hidden" id="det_cost_copy-' . $big_material . '" name="det_cost_copy-' . $big_material . '" value="' . $mat_price->getDeterminationCostCopies() . '">';
              $mystr .= '<input type="hidden" value="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+1+1+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '+' . $mat_price->getLongSideMinLength() . '+' . $mat_price->getShortSideMinLength() . '" id="mtid' . $big_material . '">';
              ?>
            </div>
            <?php
          }
          $i_mt++;
        }
        ?>
      </div>

    <?php else : 
      $material_data = $product_material->getData();
      $material = $material_data[0];      
    
      $big_material = $material->getMaterialId();
      $material_one = MP\MaterialsLangQuery::create()->filterByMaterialId($big_material)->filterByLanguageId($lang_name)->findOne();

      $material_obj = MP\MaterialsQuery::create()->findPk($big_material);
      $material_img_fid = $material_obj->getImage();
      $material_img_file = file_load($material_img_fid);
      $material_img_uri = $material_img_file->uri;

      $mat_desc = htmlentities($material_one->getDescription());
    
      $materials_price_values = MP\MaterialsQuery::create()->filterByMaterialId($big_material)->find();
      foreach ($materials_price_values as $mat_price) {
          //dd($mat_price);
        if ($mat_price->getShortSideMaxLength() != 0) {
          $mystr .= '<input type="hidden" name="dbheight1-' . $big_material . '" value="1">';
          $mystr .= '<input type="hidden" name="dbheight2-' . $big_material . '" value="' . round($mat_price->getShortSideMaxLength()) . '">';
        }

        if ($mat_price->getLongSideMaxLength() != 0) {
          $mystr .= '<input type="hidden" name="dbwidth1-' . $big_material . '" value="1">';
          $mystr .= '<input type="hidden" name="dbwidth2-' . $big_material . '" value="' . round($mat_price->getLongSideMaxLength()) . '">';
        }
        if ($mat_price->getDeterminationCostCopies() == 2) {
                $cost_copies = MP\MaterialsCostCopiesQuery::create()->filterByMaterialId($big_material)->find();
                foreach ($cost_copies as $crecord) {
                    $cc .= $crecord->getFromCost() . " " . $crecord->getToCost() . " " . $crecord->getPrice() . "+";
                }
                $mystr .= '<input type="hidden" value="' . $cc . '" id="costCopies'.$big_material.'">';
              }
              if ($mat_price->getDeterminationCostCopies() == 3) {
                $cost_copies = MP\MaterialsCostPackageQuery::create()->filterByMaterialId($big_material)->find();
                foreach ($cost_copies as $crecord) {
                    $cc .= $crecord->getFromCost() . " " . $crecord->getToCost() . " " . $crecord->getPrice() . "+";
                }
                $mystr .= '<input type="hidden" value="' . $cc . '" id="costCopies'.$big_material.'">';
              }
        $mystr .= '<input type="hidden" value="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+1+1+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '+' . $mat_price->getLongSideMinLength() . '+' . $mat_price->getShortSideMinLength() . '" id="mtid' . $big_material . '">';
        $mystr .= '<input type="hidden" id="det_cost_copy-' . $big_material . '" name="det_cost_copy-' . $big_material . '" value="' . $mat_price->getDeterminationCostCopies() . '">';
       }
      ?>
      
      <div class="dimdiv--only dimdiv--circle-only col-sm-12">
        <input type="radio" name="material" class="material_class" id="material<?php echo $big_material; ?>" value="<?php echo $big_material; ?>" checked="checked" />
        <img src="<?php echo image_style_url("thumbnail-new", $material_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $material_img_uri); ?>' alt='' /><p><?php echo $mat_desc; ?></p></div>" />
        <div class="dimdiv">
          <p class="mat-title"><?php echo $material_one->getName(); ?></p>
          <?php $mat_desc = str_replace('&lt;', '<', $mat_desc); ?>
          <?php $mat_desc = str_replace('&gt;', '>', $mat_desc); ?>
          <span class="mat-desc"><?php echo $mat_desc; ?></span>
        </div>
        
      </div>
      
      
    <?php endif; ?>
  </div>
  <!-- FINE MATERIALI -->
  
  
  <!-- DIMENSIONI -->
  <div class="order-view-top section-dimension">
    <h4 class="order-h"><?php echo $dimension; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 3"); ?></p>
    <div class="form-group">
      <div class="col-md-12 col-sm-12"></div>
      <div id="dimensions-div"><!-- OPEN DIMENSIONS DIV -->
        <input type="hidden" name="fixed_costs" value="0" id="fixed_costs" />
<?php
$i = 1;

foreach ($formone_dimensions as $formonedimensions) {
  $dname = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->findOne();
  $descr = $dname->getDescription();
  $image = $dname->getFile();
  $uuu = file_load($image)->uri;
  //$my_image = explode("://", $uuu);
  if( $uuu != '' ){
      $my_image = image_style_url("dimensioni", $uuu);        
    }
  ?>
          <div class="form-group col-md-3 col-sm-4 col-xs-6 text-center">
          <?php if ($my_image != '') { ?>
              <label class="dimdiv">
                <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
    <?php $mystr .= '<input type ="hidden" value ="' . $dname->getWidth() . '*' . $dname->getHeight() . '" id = "bd' . $dname->getDimensionId() . '">'; ?>
                
                <img src="<?php echo $my_image; ?>"/>
                <p><strong><?php echo $descr; ?></strong></p>
                <p><strong><?php echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>
              </label>
            </div>
  <?php }
  else {
    ?>
            <label class="dimdiv">
              <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
              <?php $mystr .= '<input type ="hidden" value ="' . $dname->getWidth() . '*' . $dname->getHeight() . '" id = "bd' . $dname->getDimensionId() . '">'; ?>
              <img src="<?php echo $base_url . '/sites/default/files/'; ?>camera.jpg"/>
              <p><strong><?php echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>
            </label>

          </div>
          <?php
        }
        $i++;
      }
  ?>
    </div><!-- CLOSE DIMENSIONS DIV -->
    <div class="clearfix"></div>
    <?php if ($free_dimesions == 1) { 
        //dd($dims);
        ?>
      <div class="form-group ">
        <div class="col-md-12 col-sm-12">
          <label class="dimdiv dimdiv--custom-size">
            <input type="radio" <?php if($dims == 0) echo "checked='checked'"; ?> name="dimensions" id="dimensions" value="custom">
            <span class="fa cs-png">
              <img src="<?php echo base_path() . path_to_theme(); ?>/images/transformer.png"/>
            </span>            
            <?php echo $custdimension; ?>
            
            <?php if(count($formone_dimensions)==0): ?>
              <span class="fa fa-check-circle dimdiv__checked"></span>
            <?php endif; ?>
            
          </label>
        </div>
      </div>

      <div class="clearfix"></div>
      <div id="cstdisp" class="form-inline">
        <div class="col-md-12">
          <div class="form-group">
            <h6><?php echo $width; ?></h6>
            <input type="text" name="productWidth" class="form-control " id="productWidth" placeholder="100">
            X
          </div>
          <div class="form-group">
            <h6><?php echo $height; ?></h6>
            <input type="text" name="productHeight" class="form-control" id="productHeight" placeholder="100">
            cm
          </div>
        </div>
      </div> <?php } ?>
    <?php if ($files != 0) { ?>
      <div class="col-sm-12">
      <div class="space-3"></div>
      <a id="template-download" href="#">
        <div class="btn btn-danger" src="<?php //echo $base_url . '/sites/default/files/' . $templateImage[1];  ?>">
          <span class="fa fa-download"></span> <?php echo $bdownload; ?>
        </div>
      </a>
      <div class="space-3"></div>
    </div>
    <?php } ?>
  </div>
  <!-- FINE DIMENSIONI -->
</div>



<!-- LAVORAZIONI BASE -->
<div class="order-view-top section-processing-base" id="basework" <?php if ($processing_id == 11): ?> style="display: none;" <?php endif; ?>  >
  <h4 class="order-h"><?php echo $processing; ?></h4>
  <p class="label-block clearfix"><?php echo t("explain text 4"); ?></p>
  <div class="form-group clearfix">
    <div>
<!--    <div class="carousel">-->
      <input type="hidden" name="productgroupid" value="<?php echo $bigresult_findone->getProductgroupId(); ?>">
      <?php
      $product_processing = MP\ProcessingLangQuery::create()->filterByProcessingId($processing_id)->filterByLanguageId($lang_name)->findOne();
      $precessvalues = MP\ProcessingQuery::create()->filterByProcessingId($processing_id)->findOne();
      $pocess_baseprice = $precessvalues->getBasePrice();
      $base_starting_price = $precessvalues->getStartingPrice();
      $pocess_fucntionid = $precessvalues->getPriceCalcFunctionId();
      $min_sqm = $precessvalues->getMinOneSqm();
      if ($min_sqm == "")
          $min_sqm= 0;
      $basedimminmax = $precessvalues->getIsDimensionMinMaxRequired();
      $ssmin_length = $precessvalues->getShortSideMinLength();
      $lsmin_height = $precessvalues->getLongSideMinLength();
      $extrafieldreq = $precessvalues->getIsExtrafieldRequired();
      $extrafieldname = $precessvalues->getExtraFieldName();
      $baseisExtra = $precessvalues->getIsExtra();
      $baseisExtraTop = $precessvalues->getExtraProcessingTop();
      $baseisExtraRight = $precessvalues->getExtraProcessingRight();
      $baseisExtraLeft = $precessvalues->getExtraProcessingLeft();
      $baseisExtraBottom = $precessvalues->getExtraProcessingBottom();
      $isSpecial = $precessvalues->getIsSpecial();
      $isMandatory = $precessvalues->getIsMandatory();
      $specialType = $precessvalues->getSpecialType();
      $process_img_fid = $precessvalues->getImage();
      $process_img_file = file_load($process_img_fid);
      $process_img_uri = $process_img_file->uri;
      $process_description = $product_processing->getDescription();
      ?>
      
      <div class="dimdiv--only dimdiv--circle-only col-sm-12">
        <input type="radio" name="baseprocessing<?php echo $processing_id;  ?>" id="baseprocessing<?php echo $processing_id; ?>" checked="checked" value="<?php echo $processing_id; ?>" class="baseprocessing_class"/>        
        <img class="img-responsive pull-left" src="<?php echo image_style_url("thumbnail-new", $process_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content' style='z-index:9999999'><img src='<?php echo image_style_url("rectangular-tooltip", $process_img_uri); ?>' alt='' /><p><?php  echo $process_description; ?></p></div>" />                
        <div class="dimdiv">
          <?php if ($isMandatory): ?>
            <p class="mat-title red"><?php echo $product_processing->getName(); ?></p>
          <?php else: ?>
            <p class="mat-title"><?php echo $product_processing->getName(); ?></p>
          <?php endif; ?>
          <span class="mat-desc"><?php echo $process_description; ?></span>
        </div>        
      </div>
      
      <?php
      // to insert hidden variables
      $mystr .= '<input type ="hidden" value ="' . $pocess_baseprice . '+' . $pocess_fucntionid . '+' . $min_sqm . '+' . $ssmin_length . '+' . $lsmin_height . '+' . $basedimminmax . '+' . $base_starting_price . '" id = "bsprcs' . $processing_id . '">';
      ?>

    </div>
  </div>
</div>
<div>
  <div>
    <?php if ($baseisExtra == 1): ?>
      <div class="space-2 col-md-12"></div>
      <div class="lavor-infodiv<?php echo $processing_id; ?> col-md-12">
        <div class="extinfodiv">
<!--          <legend class="legend--toggle"><?php #echo $product_processing->getName(); ?></legend>-->
          <div class="order-view-hd order-view-hd-adviced"><?php echo $product_processing->getName(); ?></div>
          <div class="wrapper-choose-extinfo">
          
              <div class="clearfix">
                <p class="label-block pull-left"><?php echo $side; ?></p>
                <?php if ($extrafieldreq == 1) { ?>
                  <div class="form-group pull-right">
                    <p><strong><?php echo $product_processing->getExtraFieldName(); ?></strong></p>
                    <input type="text" name="extradefValue" id="extradefValue">
                  </div>
                <?php } ?>
                <?php if ($baseisExtra == 1): ?>
                  <div class="wrapper-extra-field pull-right">
                    <?php if ($baseisExtraRight == 1): ?>
                      <div class="form-group extra-check">
                        <input <?php if ($isSpecial == 1): ?> data-base-id="<?php echo $processing_id; ?>" data-special="1" <?php endif; ?> class="base_side" type="checkbox" name="baseisExtraRight" id="baseisExtraRight" value="1">&nbsp<?php echo $ExtraProcessingRight; ?>
                      </div>
                    <?php endif; ?>
                    <?php if ($baseisExtraLeft == 1): ?>
                      <div class="form-group extra-check">
                        <input <?php if ($isSpecial == 1): ?> data-base-id="<?php echo $processing_id; ?>" data-special="1" <?php endif; ?> class="base_side" type="checkbox" name="baseisExtraLeft" id="baseisExtraLeft" value="1">&nbsp<?php echo $ExtraProcessingLeft; ?>
                      </div>
                    <?php endif; ?>
                    <?php if ($baseisExtraTop == 1): ?>
                      <div class="form-group extra-check">
                        <input <?php if ($isSpecial == 1): ?> data-base-id="<?php echo $processing_id; ?>" data-special="1" <?php endif; ?> class="base_side" type="checkbox" name="baseisExtraTop" id="baseisExtraTop" value="1">&nbsp<?php echo $ExtraProcessingTop; ?>
                      </div>
                    <?php endif; ?>
                    <?php if ($baseisExtraBottom == 1): ?>
                      <div class="form-group extra-check">
                        <input <?php if ($isSpecial == 1): ?> data-base-id="<?php echo $processing_id; ?>" data-special="1" <?php endif; ?> class="base_side" type="checkbox" name="baseisExtraBottom" id="baseisExtraBottom" value="1">&nbsp<?php echo $ExtraProcessingBottom; ?>
                      </div>                
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              </div>
              <?php if ($isSpecial == 1): ?>
                <div class="form-group number_special number_special_base clearfix" id="base_distance_special_div_<?php echo $processing_id; ?>"  style="display: none">
                  <p class="label-block pull-left"><?php echo $ldistance; ?></p>
                  <select class="base_distance_special pull-right" name="base_distance_special_<?php echo $processing_id; ?>" id="base_distance_special_<?php echo $processing_id; ?>">
                    <option value="" selected="selected"><?php echo $choose; ?></option>
                    <option value="20">20 cm</option>
                    <option value="25">25 cm</option>
                    <option value="30">30 cm</option>
                    <option value="40">40 cm</option>
                    <option value="50">50 cm</option>
                  </select>
                </div>
                <div class="form-group clearfix" id="base_number_special_div_<?php echo $processing_id; ?>"  style="display: none">
                  <p class="label-block pull-left"><?php echo t('Number of %specialType', array('%specialType' => $specialType)); ?></p>
                  <input class="base_number_special pull-right" data-base-id="<?php echo $processing_id; ?>" type="text" id="base_number_special_<?php echo $processing_id; ?>" name="base_number_special_<?php echo $processing_id; ?>" />
                  <p class="pull-right">                                        
                    <?php echo t('The number of @specialType is recommended <span id="base_number_special_span_@processing_id"></span>. Alternatively you can enter number of @specialType you want.', array('@specialType' => $specialType, '@processing_id' => $processing_id )); ?>
                  </p>
                </div>
              <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
</div>
<!-- FINE LAVORAZIONI BASE -->

<!-- LAVORAZIONI ACCESSORIE -->
<div class="order-view-top section-processing-accessories">
  <?php
  //controllo presenza lavorazioni accessorie
  //in caso contrario nascondo il div
  if (count($extra_processing) > 0) {
    $is_extra = TRUE;
  }
  else {
    $is_extra = FALSE;
  }
  ?>
  <div class="form-group" <?php if (!$is_extra): ?> style="display: none;" <?php endif; ?> >
    <h4 class="order-h"><?php echo $processing; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 5"); ?></p>
    
    <?php if( count($extra_processing) > 1 ): ?>
      <?php
      //$extdefdividlst = "";
      $mand = 0; $change= 0; $first= 0;
      foreach ($extra_processing as $eprocessing) {
        $pocess_id = $eprocessing->getProcessingId();
        $ename = MP\ProcessingLangQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->filterByLanguageId($lang_name)->findOne();

        $valus = MP\ProcessingQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->findOne();
        $pocess_baseprice = $valus->getBasePrice();
        $starting_price = $valus->getStartingPrice();
        $pocess_fucntionid = $valus->getPriceCalcFunctionId();
        $min_sqm = $valus->getMinOneSqm();
        $dimminmax = $valus->getIsDimensionMinMaxRequired();
        $ssmin_length = $valus->getShortSideMinLength();
        $lsmin_height = $valus->getLongSideMinLength();
        $extrafieldreq = $valus->getIsExtrafieldRequired();
        $extrafieldname = $valus->getExtraFieldName();
        $isExtra = $valus->getIsExtra();
        $isExtraTop = $valus->getExtraProcessingTop();
        $isExtraRight = $valus->getExtraProcessingRight();
        $isExtraLeft = $valus->getExtraProcessingLeft();
        $isExtraBottom = $valus->getExtraProcessingBottom();

        $process_img_fid = $valus->getImage();
        $process_img_file = file_load($process_img_fid);
        $process_img_uri = $process_img_file->uri;
        $isMandatory = $valus->getIsMandatory();
        if ($isMandatory == 1) {
            if ($mand == 0) { ?>
            <p class="label-block clearfix red"><strong><?php echo t("explain text 15"); ?></strong></p>
            <div class="carousel">
            
            <?php }
            $mand = 1; $first= 1;
        }   
        else {
            if ($first == 0) { ?>
            <p class="label-block clearfix"><?php echo t("explain text 16"); ?></p>
            <div class="carousel">
            
            <?php 
            $first= 1;
            }
        }
        $process_description = htmlentities( $ename->getDescription() );
        if ($isMandatory == 0 && $mand == 1 && $change == 0) {
        ?>
        </div>
        <p class="label-block clearfix"><?php echo t("explain text 16"); ?></p>
        <div class="carousel section-disable">
        <?php 
            $change= 1;
        } ?>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle" >
            <?php if ($isMandatory == 1) { ?>
              <input type="checkbox" name="extraprocessing<?php echo $pocess_id; ?>" class="extraprocessing_class mand" id="extraprocessing<?php echo $pocess_id; ?>" value="<?php echo $pocess_id; ?>" />
            <?php } else { ?>
              <input type="checkbox" name="extraprocessing<?php echo $pocess_id; ?>" class="extraprocessing_class notmand" id="extraprocessing<?php echo $pocess_id; ?>" value="<?php echo $pocess_id; ?>" />
            <?php } ?>
              <img src="<?php echo image_style_url("thumbnail-new", $process_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'>
                 <img src='<?php echo image_style_url("rectangular-tooltip", $process_img_uri); ?>' />
                 <p><?php echo $process_description ?></p>
                 </div>" />
            <?php if ($isMandatory == 1): ?>
              <p class="red"><?php echo $ename->getName() ?></p>
            <?php else: ?>
              <p><?php echo $ename->getName() ?></p>
            <?php endif; ?>
              <span id="tooltip-inline" class="visible-sm visible-xs"><?php echo strip_tags($ename->getDescription()); ?></span>
          </label>
        </div>
        <?php
        // to insert hidden variables
        $mystr .= '<input type ="hidden" value ="' . $pocess_baseprice . '+' . $pocess_fucntionid . '+' . $min_sqm . '+' . $ssmin_length . '+' . $lsmin_height . '+' . $dimminmax . '+' . $starting_price . '" id = "extprcs' . $pocess_id . '">';
        $mystr .= '<input type ="hidden" value ="" id="extprcs_cost_' . $pocess_id . '">';
        $extdefdividlst .= $pocess_id . "+";
        ?>
      <?php
      }
      $mystr .= '<input type ="hidden" value ="' . $extdefdividlst . '" id="extdefdividlst">';
      ?>
    </div>
    <input type="hidden" id="extra_processing_mandatory" name="extra_processing_mandatory" value="<?php echo $mand;?>" />
    <?php else: 
      
      if(count($extra_processing) == 1){
      
        $eprocessing_data = $extra_processing->getData();
        $eprocessing = $eprocessing_data[0]; 

        $pocess_id = $eprocessing->getProcessingId();
        $ename = MP\ProcessingLangQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->filterByLanguageId($lang_name)->findOne();

        $valus = MP\ProcessingQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->findOne();
        $pocess_baseprice = $valus->getBasePrice();
        $starting_price = $valus->getStartingPrice();
        $pocess_fucntionid = $valus->getPriceCalcFunctionId();
        $min_sqm = $valus->getMinOneSqm();
        $dimminmax = $valus->getIsDimensionMinMaxRequired();
        $ssmin_length = $valus->getShortSideMinLength();
        $lsmin_height = $valus->getLongSideMinLength();
        $extrafieldreq = $valus->getIsExtrafieldRequired();
        $extrafieldname = $valus->getExtraFieldName();
        $isExtra = $valus->getIsExtra();
        $isExtraTop = $valus->getExtraProcessingTop();
        $isExtraRight = $valus->getExtraProcessingRight();
        $isExtraLeft = $valus->getExtraProcessingLeft();
        $isExtraBottom = $valus->getExtraProcessingBottom();

        $process_img_fid = $valus->getImage();
        $process_img_file = file_load($process_img_fid);
        $process_img_uri = $process_img_file->uri;

        $process_description = $ename->getDescription();


        // to insert hidden variables
        $mystr .= '<input type ="hidden" value ="' . $pocess_baseprice . '+' . $pocess_fucntionid . '+' . $min_sqm . '+' . $ssmin_length . '+' . $lsmin_height . '+' . $dimminmax . '+' . $starting_price . '" id = "extprcs' . $pocess_id . '">';
        $mystr .= '<input type ="hidden" value ="" id="extprcs_cost_' . $pocess_id . '">';
        $extdefdividlst .= $pocess_id . "+";
        $mystr .= '<input type ="hidden" value ="' . $extdefdividlst . '" id="extdefdividlst">';

      ?>
        <div class="dimdiv--only dimdiv--circle-only dimdiv--only-border col-sm-12">
          <input type="checkbox" name="extraprocessing<?php echo $pocess_id; ?>" class="extraprocessing_class" id="extraprocessing<?php echo $pocess_id; ?>" value="<?php echo $pocess_id; ?>" />                    
          <img class="img-responsive pull-left" src="<?php echo image_style_url("thumbnail-new", $process_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $process_img_uri); ?>' alt='' /><p><?php  echo $process_description; ?></p></div>" />          
          <div class="dimdiv">
            <p class="mat-title"><?php echo $ename->getName(); ?></p>
            <span class="mat-desc"><?php echo $process_description; ?></span>
          </div>
          <div class="space-2"></div>
        </div>
    <?php
      }
      endif;
    ?>
    
    
    <div id="adviced" class="adviced" style="display: none;"></div>


    <?php
    foreach ($extra_processing as $eprocessing) {
      $pocess_id = $eprocessing->getProcessingId();
      $ename = MP\ProcessingLangQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
      $valus = MP\ProcessingQuery::create()->filterByProcessingId($eprocessing->getProcessingId())->findOne();
      $pocess_baseprice = $valus->getBasePrice();
      $pocess_fucntionid = $valus->getPriceCalcFunctionId();
      $min_sqm = $valus->getMinOneSqm();
      $dimminmax = $valus->getIsDimensionMinMaxRequired();
      $ssmin_length = $valus->getShortSideMinLength();
      $lsmin_height = $valus->getLongSideMinLength();
      $extrafieldreq = $valus->getIsExtrafieldRequired();
      $extrafieldname = $valus->getExtraFieldName();
      $isExtra = $valus->getIsExtra();
      $isExtraTop = $valus->getExtraProcessingTop();
      $isExtraRight = $valus->getExtraProcessingRight();
      $isExtraLeft = $valus->getExtraProcessingLeft();
      $isExtraBottom = $valus->getExtraProcessingBottom();

      $isSpecial = $valus->getIsSpecial();
      $specialType = $valus->getSpecialType();

      $process_img_fid = $valus->getImage();
      $process_img_file = file_load($process_img_fid);
      $process_img_uri = $process_img_file->uri;

      $process_description = $ename->getDescription();
      ?>
      <?php
      if ($isExtra == 1):
        ?>
        <div class="col-md-12">
          <div id="extinfodiv" class="extinfodiv<?php echo $pocess_id; ?>" <?php echo ( count($extra_processing) > 1 )?'style="display:none"':''; ?> >
            <div class="order-view-hd order-view-hd-adviced"><?php echo $ename->getName() ?></div>
            <div class="wrapper-choose-extinfo">
            <?php
            if ($extrafieldreq == 1) {
              ?>
              <div class="form-group">
                <div id="epextradefdiv<?php echo $pocess_id; ?>">
                  <label for="" class="label-block"><?php echo $ename->getExtraFieldName(); ?></label>
                  <input type="text" name="epextadefValue_<?php echo $pocess_id; ?>" class="epextadefValue" id="epextradefValue<?php echo $pocess_id; ?>">
                </div>
              </div>
            <?php
            }
            if ($isExtra == 1):
              ?>
              <div id="epextraprcsdiv<?php echo $pocess_id; ?>" class="epextraprcsdiv clearfix">
                <p class="label-block pull-left"><?php echo $side; ?></p>
                <div class="wrapper-extra-field pull-right">
      <?php if ($isExtraRight == 1): ?>
                  <div class="extra-check">
                    <input <?php if ($isSpecial == 1): ?> data-extra-id="<?php echo $pocess_id; ?>" data-special="1" <?php endif; ?> class="extra_side" type="checkbox" name="isextraright_<?php echo $pocess_id; ?>" id="isextraright_<?php echo $pocess_id; ?>" value="1">&nbsp<?php echo $ExtraProcessingRight; ?>
                  </div>
      <?php endif; ?>
      <?php if ($isExtraLeft == 1): ?>
                  <div class="extra-check">
                    <input <?php if ($isSpecial == 1): ?> data-extra-id="<?php echo $pocess_id; ?>" data-special="1" <?php endif; ?> class="extra_side" type="checkbox" name="isextraleft_<?php echo $pocess_id; ?>" id="isextraleft_<?php echo $pocess_id; ?>" value="1">&nbsp<?php echo $ExtraProcessingLeft; ?>
                  </div>
      <?php endif; ?>
      <?php if ($isExtraTop == 1): ?>
                  <div class="extra-check">
                    <input <?php if ($isSpecial == 1): ?> data-extra-id="<?php echo $pocess_id; ?>" data-special="1" <?php endif; ?> class="extra_side" type="checkbox" name="isextratop_<?php echo $pocess_id; ?>" id="isextratop_<?php echo $pocess_id; ?>" value="1">&nbsp<?php echo $ExtraProcessingTop; ?>
                  </div>
      <?php endif; ?>
      <?php if ($isExtraBottom == 1): ?>
                  <div class="extra-check">
                    <input <?php if ($isSpecial == 1): ?> data-extra-id="<?php echo $pocess_id; ?>" data-special="1" <?php endif; ?> class="extra_side" type="checkbox" name="isextrabottom_<?php echo $pocess_id; ?>" id="isextrabottom_<?php echo $pocess_id; ?>" value="1">&nbsp<?php echo $ExtraProcessingBottom; ?>
                  </div>
              <?php endif; ?>
                </div>
              </div>
            <?php endif; ?>
            <?php if ($isSpecial == 1): ?>
              <div class="form-group number_special number_special_extra clearfix" id="distance_special_div_<?php echo $pocess_id; ?>"  style="display: none">
                <p class="label-block pull-left"><?php echo $ldistance; ?></p>
                <div class="wrapper-extra-field pull-right">
                  <select class="distance_special" name="distance_special_<?php echo $pocess_id; ?>" id="distance_special_<?php echo $pocess_id; ?>">        
                    <option value="" selected="selected"><?php echo $choose; ?></option>
                    <option value="20">20 cm</option>
                    <option value="25">25 cm</option>
                    <option value="30">30 cm</option>
                    <option value="40">40 cm</option>
                    <option value="50">50 cm</option>
                  </select>
                </div>
              </div>
              
              <div class="form-group clearfix" id="number_special_div_<?php echo $pocess_id; ?>"  style="display: none">
                <p class="label-block pull-left"><?php echo t('Number of %specialType', array('%specialType' => $specialType)); ?></p>
                <input class="number_special pull-right" data-extra-id="<?php echo $pocess_id; ?>" type="text" id="number_special_<?php echo $pocess_id; ?>" name="number_special_<?php echo $pocess_id; ?>" />                
                <p class="pull-right">                                        
                  <?php echo t('The number of @specialType is recommended <span id="number_special_span_@$pocess_id"></span>. Alternatively you can enter number of @specialType you want.', array('@specialType' => $specialType, '@$pocess_id' => $pocess_id )); ?>
                </p>
              </div>
        <?php endif; ?>
          
            </div>
          </div>
        </div>
      <?php endif; ?>
      <?php
    }
    ?>
  </div>
</div>
<!-- FINE LAVORAZIONI ACCESSORIE -->
  
<?php
// STRUTTURA /////////////////////////
if ($bigresult_findone->getSellWithStructure() == 1) {  
  ?>
  <div class="order-view-top section-struttura">
    <h4 class="order-h"><?php echo $accessorie_base; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 6"); ?></p>
    <div class="form-group">
      <?php
      $bigresult_findone->getSellWithStructure();
      $accId = $bigresult_findone->getAccessoriesId();
      $accval = MP\AccessoriesQuery::create()->filterByAccessoryId($accId)->findOne();
      $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($bigresult_findone->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
      $baseaccprice = $accval->getPrice();
      $baseaccid = $accval->getAccessoryId();

      $accval_img_fid = $accval->getFile();
      $accval_img_file = file_load($accval_img_fid);
      $accval_img_uri = $accval_img_file->uri;
      
      $accval_img_print_fid = $accval->getFilePrint();
      $accval_img_print_file = file_load($accval_img_print_fid);
      $accval_img_print_uri = $accval_img_print_file->uri;
      
      $accval_img_structure_fid = $accval->getFileStructure();
      $accval_img_structure_file = file_load($accval_img_structure_fid);
      $accval_img_structure_uri = $accval_img_structure_file->uri;

      $accval_description = $aname->getDescription();
      ?>
      <p class="label-block clearfix"><?php echo $accval_description; ?></p>
      <div class="carousel">
  <?php $mystr .= '<input type ="hidden" value ="' . $baseaccprice . '" id="baseacc' . $accId . '">'; ?>      
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle label-structure" >
            <input type="checkbox" name="baseaccessories" class="baseaccessories_class" id="print_structure" value="<?php echo $accId; ?>" checked="checked" />            
            <img class="img-responsive" src="<?php echo image_style_url("thumbnail-new", $accval_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content' style='z-index:9999999'><img src='<?php echo image_style_url("rectangular-tooltip", $accval_img_uri); ?>' alt='' /><p><?php echo $structure_print; ?></p></div>" />
            <p><?php echo $structure_print; ?></p>
          </label>
        </div>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle label-structure" >
            <input type="checkbox" name="baseaccessories" class="baseaccessories_class" id="only_structure" value="<?php echo $accId; ?>" />            
            <img class="img-responsive" src="<?php echo image_style_url("thumbnail-new", $accval_img_structure_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content' style='z-index:9999999'><img src='<?php echo image_style_url("rectangular-tooltip", $accval_img_structure_uri); ?>' alt='' /><p><?php echo $only_structure; ?></p></div>" />
            <p><?php echo $only_structure; ?></p>
          </label>
        </div>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle label-structure" >
            <input type="checkbox" name="baseaccessories" class="baseaccessories_class" id="only_print" value="0" />            
            <img class="img-responsive" src="<?php echo image_style_url("thumbnail-new", $accval_img_print_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $accval_img_print_uri); ?>' alt='' /><p><?php echo $only_print; ?></p></div>" />
            
            <p><?php echo $only_print; ?></p>
          </label>
        </div>
      
      </div>
    </div>
  </div>
  <?php
}
?>
<?php
// LAVORAZIONI FRONTE-RETRO /////////////////////////
// non dipende più dal prodotto ma dal materiale selezionato -> JS!!!
//if ($bigresult_findone->getFrontBack() == 1) {  
 
  $fr_info = node_load(array('tnid' => 63, 'language' => $language->language));
  
  ?>
  <div class="order-view-top section-fronteretro section-only-item" style="display: none;" >
    <h4 class="order-h"><?php echo $fronteretro; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 8"); ?></p>
    <div class="form-group clearfix">
      <div class="col-md-12">
        <label class="dimdiv dimdiv--fr">
          <input type="checkbox" name="fronteretro" id="fronteretro" value="1" class="fronteretro_class" />
          <input type="hidden" name="frontback_calc_type" id="frontback_calc_type" value="" />
          <input type="hidden" name="frontback_price" id="frontback_price" value="" />
          
          <span class="fa fr-png"><img src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/bifacciale.jpg"></span>
          
          <div><?php echo $fr_info->body[LANGUAGE_NONE][0]['value']; ?></div>
        </label>
      </div>
      <div class="adviced-fr col-md-12">
        <div class="extinfodiv">
          <div class="space-2"></div>
          <div class="order-view-hd order-view-hd-adviced"><?php echo $fronteretro; ?></div>
          <div class="wrapper-choose-extinfo">
            <div class="clearfix">
              <p class="label-block pull-left"><?php echo $lchoosefr; ?></p>
              <div class="wrapper-extra-field pull-right">
                <div class="form-group extra-check">
                  <input class="extra_side" type="radio" name="radio_fronteretro" id="radio_fronteretro1" value="1">&nbsp <?php echo $item_files; ?>
                </div>
                <div class="form-group extra-check">
                  <input class="extra_side" type="radio" name="radio_fronteretro" id="radio_fronteretro2" value="2">&nbsp <?php echo $different_files; ?>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
<?php //} ?>

<?php
//  ACCESSORI  ////////////////////////////////////// 
if ($countformone_accessories != 0) {
  ?>
  <div class="order-view-top section-accessories">
    <h4 class="order-h"><?php echo $accessorie; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 7"); ?></p>
      
      <?php if( count( $formone_accessories ) > 1 ): ?>
      <div class="carousel-accessories">
        <?php
        foreach ($formone_accessories as $accsrysitms) {
          $eaccvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->findOne();
          $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->findOne();
          $eaccprice = $eaccvls->getPrice();
          $eaccid = $accsrysitms->getAccessoryId();
          $eacc_img_fid = $eaccvls->getFile();
          $eacc_img_file = file_load($eacc_img_fid);
          $eacc_img_uri = $eacc_img_file->uri;
          $eacc_description = htmlentities( $aname->getDescription() );
          ?>
          <div class=" text-center">
            <label class="dimdiv dimdiv--circle" >
              <input type="checkbox" name="extraaccessories<?php echo $eaccid; ?>" class="extraaccessories_class" id="extraaccessories<?php echo $eaccid; ?>" value="<?php echo $eaccid; ?>" />
              <img src="<?php echo image_style_url("thumbnail-new", $eacc_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $eacc_img_uri); ?>" alt="" /><p><?php echo $eacc_description ?></p></div>' />
              <p><?php echo $aname->getName() ?></p>
              <span id="tooltip-inline" class="visible-sm visible-xs"><?php echo strip_tags($aname->getDescription()); ?></span>
            </label>
            <div id="acc-qnty<?php echo $eaccid; ?>" class="acc-qnty" style="display: none;">
              <div class="form-group">
                <label for="" style="font-weight: normal"><?php echo t('Qty'); ?></label>
                <input type="number" name="extraaccessories-qnty_<?php echo $eaccid; ?>" class="extraaccessories-qnty-Value" id="extraaccessories-qnty_<?php echo $eaccid; ?>" value="1" min="1">
              </div>
            </div>
          </div>
          <?php
          $mystr .= '<input type ="hidden" value ="' . $eaccprice . '" id="extacc' . $eaccid . '">';
        }
        ?>
      </div>
    
      <?php else: 
                
        $accsrysitms_data = $formone_accessories->getData();
        $accsrysitms = $accsrysitms_data[0]; 
        
        $eaccvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->findOne();
        $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->findOne();
        $eaccprice = $eaccvls->getPrice();
        $eaccid = $accsrysitms->getAccessoryId();
        $eacc_img_fid = $eaccvls->getFile();
        $eacc_img_file = file_load($eacc_img_fid);
        $eacc_img_uri = $eacc_img_file->uri;        
        $eacc_description = $aname->getDescription();
        $mystr .= '<input type ="hidden" value ="' . $eaccprice . '" id="extacc' . $eaccid . '">';
        
      ?>
      
      <div class="dimdiv--only dimdiv--circle-only dimdiv--only-border col-xs-12">
        <input type="checkbox" name="extraaccessories<?php echo $eaccid; ?>" class="extraaccessories_class" id="extraaccessories<?php echo $eaccid; ?>" value="<?php echo $eaccid; ?>" />
        
        <img src="<?php echo image_style_url("thumbnail-new", $eacc_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $eacc_img_uri); ?>" alt="" /><p><?php echo $eacc_description ?></p></div>' />
        
        <div class="dimdiv">
          <p class="mat-title"><?php echo $aname->getName(); ?></p>
          <span class="mat-desc"><?php echo $eacc_description; ?></span>
          <div class="space-2"></div>
          <div id="acc-qnty<?php echo $eaccid; ?>" class="acc-qnty">
            <div class="form-group">
              <label for="" style="font-weight: normal"><?php echo t('Qty'); ?></label>
              <input type="number" name="extraaccessories-qnty_<?php echo $eaccid; ?>" class="extraaccessories-qnty-Value" id="extraaccessories-qnty_<?php echo $eaccid; ?>" value="0" min="0">
            </div>
          </div>
        </div>
        
        
      </div>
      
      <?php endif; ?>
  </div>

<?php } ?>


<?php
    if ($files != 0) {     ?>
<!-- file upload for cart -->
<div class="order-view-top section-file">
  <h4 class="order-h"><?php echo $uploadimagetitle_prev ?></h4>
  <div class="form-group hidden-xs hidden-sm">
    <!--TODO UPLOAD IMAGES-->
    <div class="create-graph clearfix <?php echo ( $bigresult_findone->getUseOfExternalDesignerProgram() == 1 )?'':'hidden'; ?>">
      <div class="col-md-6 biggallery">
          <?php echo t("In need of a custom graphic?"); ?>
      </div>
      <!--<p class="col-md-6 smallgallery">
        <?php echo t("Upload your file<br>Or create it with our editor<br>Even using one of the images<br>Available for FREE!"); ?>
      </p>-->
      <p class="col-md-6 smallgallery">
        <?php echo t("Upload your file"); ?>
      </p>
        
      
    </div>
    
     <?php
      echo '<div class="box-messaggi box-messaggi-bigformat"><p>'. t('Product serving <span class="change_number_files">@files</span> files', array('@files' => $files )) . '</p></div>';
      
      for ($i = 0; $i <= $files; $i++) {
        ?>
        <div id="upi-<?php echo $i; ?>" class="uploadproductimage">
          <!--<h4 class="h4 uppercase"><?php echo t("Upload your own file, or choose from our gallery"); ?></h4>-->
          <h4 class="h4 uppercase"><?php echo t("Upload your own file"); ?></h4>
          <?php if ($id == 20) { ?>
          <p><?php echo t("The file must be in one of the following formats: PDF, AI, SVG"); ?></p>
          <?php } else { ?>
          <p><?php echo t("The file must be in one of the following formats: JPG, PNG, PDF"); ?></p>
          <?php } ?>
          <p><?php echo t("If available, upload the file to unlock daily shipping"); ?></p>

          <input type="hidden" name="editor_save_img_data_<?php echo $i; ?>" id="editor_save_img_data_<?php echo $i; ?>" value="" />
          
          <div class="pull-left">            
            <div id="image_<?php echo $i; ?>" class="fileuploader" c="<?php echo $i; ?>"><?php echo t('upload') ?></div>
              <!--input type="file" id="image_<?php echo $i; ?>" class="ImageUploadBig" c="<?php echo $i; ?>"/-->
            <input id="image_fid_<?php echo $i; ?>" class="upload_image_fid" type="hidden" value=""/>
            <input id="gallery_fid_<?php echo $i; ?>" class="gallery_image_fid" type="hidden" value=""/>
            <input id="editor_fid_<?php echo $i; ?>" class="editor_image_fid" type="hidden" value="" data-preview=""/>
            <div class="dvPreview_<?php echo $i; ?>"></div>
          </div>
          
          <!--<a id="custom-file-theme-<?php echo $i; ?>" class="custom-file-theme btn btn-orange <?php echo ( count($galleries) > 0 )?'':'hidden'; ?>" data-toggle="modal" data-target="#exampleModal"><?php echo $lchoosetheme; ?></a>
          
          <a id="custom-file-editor-<?php echo $i; ?>" class="btn btn-success btn-editor custom-file-editor <?php echo ( $bigresult_findone->getUseOfExternalDesignerProgram() == 1 )?'':'hidden'; ?>" data-target="#editorModal" ><i class="fa fa-paint-brush"></i> <?php echo $leditor; ?></a>
          <div class="ImageError_<?php echo $i; ?> imageerror"></div>
          -->
        </div>

      <?php } ?>
      <input type="hidden" id="imagecount" name="imagecount" value="<?php echo $files; ?>"/>
      <input type="hidden" id="loaded" name="loaded" value="0"/>

      <div class="form-group col-md-12 col-sm-12">
        <?php 
          $popup_file = node_load(array('tnid' => 65, 'language' => $language->language));
          $review_desc = node_load(array('tnid' => 99, 'language' => $language->language));
        ?>
        
        <div class="col-md-12">
          <label class="dym-p dimdiv dimdiv--operator-extra">
            <input type="checkbox" name="operatorreview" id="operatorreview" value="1"/>
            <img src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/verifica-file.jpg">
            <div>
              <strong><?php echo $operatorreview . ' &euro; ' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?></strong>
              <br />
              <?php echo $review_desc->body[LANGUAGE_NONE][0]["value"]; ?>
            </div>            
            <span class="fa fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" data-template="<div class='tooltip' role='tooltip'><div class='tooltip-arrow'></div><div class='tooltip-inner tooltip-inner-extra'></div></div>" data-title="<div class='dimdiv__hover-content dimdiv__hover-content-extra text-left'><?php echo htmlentities( $popup_file->body[LANGUAGE_NONE][0]["value"] ); ?></div>'"
            </span>
            
          </label>
        </div>
      </div>
  </div>

  <div class="visible-sm visible-xs">
    <p class="col-xs-12">
      <?php echo t("complete the quote by sending the files to the desktop"); ?>
    </p>
  </div>
    
  </div>
    <?php }
include('gallery_modal.tpl.php');
include('noeditor_modal.tpl.php');
?>

<!-- End file upload  for cart -->

<!-- long time production  -->
<div class="order-view-top section-shipping-date">
  <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingdate; ?></h4>
  <p class="label-block clearfix"><?php echo t("explain text 9"); ?></p>
  <div class="col-md-12 shipping-date-wrapper" >
    <!-- long time production  -->
    

<?php

//colonna in evidenza
$promo_date_column = $bigresult_findone->getPromoDateColumn();

//controllo che il prodotto sia disponibile con quantità libera
if ($bigresult_findone->getIsPresetAmount() == 0) {
  ?>

        <!-- TABELLA CON QUANTITA' LIBERE  -->
        <table id="ship-table" class="table ship-table table-condensed text-center" data-type="free">
          <thead>
            <tr>
              <th class="ship-table__qnty">
                <div class="ship-table__header-item hidden-xs"><?php echo $quantity; ?></div>
                <div class="ship-table__header-item visible-xs"><?php echo t('Qty'); ?></div>
              </th>
              <th id="dx" class="dx" >
                <label class="date-div">
                  <div id="shippingdateprintdivx" class="date-div__date2" data-shippingdate="0"></div>
                </label>
              </th>
              <th id="d0" class="d0 <?php if($promo_date_column == 1){ ?>promo-column<?php } ?>" >
                <?php if($promo_date_column == 1){ ?> <div class="ribbon_price"><span><?php echo $bestprice; ?></span></div> <?php } ?>
                <label class="date-div" id="date0">
                  <input type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="date-q0-d0" checked="checked" value="">
                  <div id="shippingdateprintdiv0" class="date-div__date" data-shippingdate="0"></div>
                </label>
              </th>
          <?php
          if ($countlongtimeprod != 0) {
            $m = 1;
            foreach ($longtimeprod as $longtimprd) {
              if ($m < 4) {
                $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();

                foreach ($longtimprdvls as $lngtmprdvls) {

                  $lngtmdays = $lngtmprdvls->getDays();
                  $lngtmamnt = $lngtmprdvls->getAmount();
                  $lngtmtyp = $lngtmprdvls->getAmountType();
                  $lngtmvals = $lngtmdays . "+" . $lngtmamnt . "+" . $lngtmtyp;
                  $type = "(%)";
                  if ($lngtmtyp == 'Value') {
                    $type = "(€)";
                  }

                  
                  foreach ($longtimprdName as $lngtmprdName) {
                    ?>
                    <th id="d<?php echo $m; ?>" <?php if($promo_date_column == $m+1){ ?> class="promo-column" <?php } ?>>
                      <?php if($promo_date_column == $m+1){ ?> <div class="ribbon_price"><span><?php echo $bestprice; ?></span></div> <?php } ?>
                      <label class="date-div">
                        <!--input type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="date-q0-d<?php echo $m; ?>" value="<?php echo $m; ?>"-->
                        <input type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="date-q0-d<?php echo $m; ?>" value="<?php echo $lngtmprdName->getLongtimeId(); ?>">
                        <div id="shippingdateprintdiv<?php echo $m; ?>" class="date-div__date" data-shippingdate="0"></div>
                      </label>
                    </th>
                    <?php
                    $mystr .= '<input class="lngtmnpt" type ="hidden" value ="' . $lngtmvals . '" id="lngtm' . $lngtmprdName->getLongtimeId() . '">';
                    //$mystr .= '<input class="lngtmnpt" type ="hidden" value ="' . $lngtmvals . '" id="lngtm' . $m . '">';
                    
                  }
                }
              }
              $m++;
            }
          }
          ?>
          </tr>
          </thead>
          <tbody>
            <tr>
              <td id="q0" class="ship-col totalnoofproductbigfrmtdiv"><strong></strong></td>
              <td id="q0-dx" class="dx ship-cell"></td>
              <td id="q0-d0" class="d0 ship-cell <?php if($promo_date_column == 1){ ?>promo-column<?php } ?>">
                  <i class="fa fa-eur"></i><span id="totallngtm0"></span>
              </td>
              <?php
              if ($countlongtimeprod != 0) {
                $m = 1;
                foreach ($longtimeprod as $longtimprd) {
                  if ($m < 4) {
                    $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                    $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();

                    foreach ($longtimprdvls as $lngtmprdvls) {
                        
                      foreach ($longtimprdName as $lngtmprdName) {
                        ?>
                        <td id="q0-d<?php echo $m; ?>" class="ship-cell <?php if($promo_date_column == $m+1){ echo "promo-column"; }?>" >
                            <i class="fa fa-eur"></i><span id="totallngtm<?php echo $lngtmprdName->getLongtimeId(); ?>"></span>
                        </td>
                        <?php
                      }
                    }
                  }
                  $m++;
                }
              }
              ?>
            </tr>
          </tbody>
        </table>
        <!-- FINE TABELLA CON QUANTITA' LIBERE  -->

  <?php
}
else { // TABELLA CON QUANTITA' PREDEFINITE    
  //$amounts = json_decode($bigresult_findone->getAmounts());            
  ?>
        <!-- TABELLA CON QUANTITA' PREDEFINITE  -->
        <table id="ship-table" class="table ship-table table-condensed text-center" data-type="fixed">
          <thead>
            <tr>
              <th class="ship-table__qnty">
                <div class="ship-table__header-item hidden-xs"><?php echo $quantity; ?></div>
                <div class="ship-table__header-item visible-xs"><?php echo t('Qty'); ?></div>
              </th>
              <th id="dx" class="dx" >
                <label class="date-div">
                  <div id="shippingdateprintdivx" class="date-div__date2" data-shippingdate="0"></div>
                </label>
              </th>
              <th id="th-d0" class="d0" <?php if($promo_date_column == 1){ ?> class="promo-column" <?php } ?>>
                <?php if($promo_date_column == 1){ ?> <div class="ribbon_price"><span><?php echo $bestprice; ?></span></div> <?php } ?>
                <label class="date-div">
                  <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d0" value="">
                  <div id="shippingdateprintdiv0" class="date-div__date" data-shippingdate="0"></div>
                </label>
              </th>
          <?php
          if ($countlongtimeprod != 0) {
            $m = 1;
            foreach ($longtimeprod as $longtimprd) {
              if ($m < 4) {
                $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();

                foreach ($longtimprdvls as $lngtmprdvls) {

                  $lngtmdays = $lngtmprdvls->getDays();
                  $lngtmamnt = $lngtmprdvls->getAmount();
                  $lngtmtyp = $lngtmprdvls->getAmountType();
                  $lngtmvals = $lngtmdays . "+" . $lngtmamnt . "+" . $lngtmtyp;
                  $type = "(%)";
                  if ($lngtmtyp == 'Value') {
                    $type = "(€)";
                  }

                  foreach ($longtimprdName as $lngtmprdName) {
                    ?>
                    <th id="th-d<?php echo $m; ?>" <?php if($promo_date_column == $m+1){ ?> class="promo-column" <?php } ?>>
                      <?php if($promo_date_column == $m+1){ ?> <div class="ribbon_price"><span>best price</span></div> <?php } ?>
                      <label class="date-div">
                        <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d<?php echo $m; ?>" value="<?php echo $lngtmprdName->getLongtimeId(); ?>">
                        <div id="shippingdateprintdiv<?php echo $m; ?>" class="date-div__date" data-shippingdate="0"></div>
                      </label>
                    </th>
                    <?php
                    $mystr .= '<input class="lngtmnpt" type ="hidden" value ="' . $lngtmvals . '" id="lngtm' . $lngtmprdName->getLongtimeId() . '">';
                  }
                }
              }
              $m++;
            }
          }
          ?>
          </tr>
          </thead>
          <tbody>
              <?php
              $i_amou = 0;
              foreach ($amounts as $amount) {
                ?>
              <tr>
                <td id="q<?php echo $amount; ?>" class="ship-col fixed-amounts <?php if ($i_amou == 0) echo "quantity-selected"; ?>" data-value="<?php echo $amount; ?>" data-discount="<?php echo $amounts_discounts[$i_amou]; ?>"><strong><?php echo $amount; ?></strong></td>
                <td id="q<?php echo $amount; ?>-dx" class="dx ship-cell"></td>
              
                <td id="q<?php echo $amount; ?>-d0" class="d0 ship-cell-fixed <?php if($promo_date_column == 1){ ?>promo-column<?php } ?>">
                    <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm0"></span>
                </td>
                <?php
                if ($countlongtimeprod != 0) {
                  $m = 1;
                  foreach ($longtimeprod as $longtimprd) {
                    if ($m < 4) {
                      $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                      $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();
                      foreach ($longtimprdvls as $lngtmprdvls) {
                        foreach ($longtimprdName as $lngtmprdName) {
                          ?>
                          <td id="q<?php echo $amount; ?>-d<?php echo $m; ?>" class="ship-cell-fixed <?php if ($m == 1) echo "selected"; ?> <?php if($promo_date_column == $m+1){ ?> promo-column<?php } ?>">
                              <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm<?php echo $lngtmprdName->getLongtimeId(); ?>"></span>
                          </td>
                          <?php
                        }
                      }
                    }
                    $m++;
                  }
                }
                ?>
              </tr>
    <?php
    $i_amou++;
  }
  ?>
          </tbody>
        </table>
        <!-- FINE TABELLA CON QUANTITA' PREDEFINITE  -->
  <?php
}
?>

  </div>
   
  <div class="col-md-12">
    <p> <?php echo $shipdesc; ?> </p>
  </div>
</div>
<!-- FINE long time production  -->

<!-- PREVENTIVO  -->
<div class="order-view-top section-quote">
  <h4 class="order-h"><?php echo $quote_data; ?></h4>

  <div class="col-md-12">
    <div class="col-md-12">
      <div class="form-group">
        <label class="label-block" for=""><?php echo $quote_name; ?></label>
        <input type="text" name="name" id="name" class="form-control"/>
      </div>
      <div class="form-group">
        <label class="label-block" for=""><?php echo $note; ?></label>
        <textarea name="note" id="note" cols="30" rows="2" class="form-control"></textarea>
      </div>
    </div>
  </div>
</div>
<!-- FINE PREVENTIVO  -->
<div class="clearfix"></div>
<div class="space-2 col-sm-12 col-md-12"></div>
<div class="space-2 col-sm-12 col-md-12"></div>

<!-- SOMMARIO  -->
<div class="section-summery" id="pricing">
  <div class="order-view-hd"><?php echo $summery; ?></div>
  <div class="panel panel-default">
    <div class="table-responsive table-summary">
      <table id="sample-table-1" class="table table-hover">
        <tbody>
          <tr>
            <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
            <td class="hidden-480">
              <span class="printigmaterialdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
<!--          <tr>
            <td class="left"><strong><?php //echo $processcost; ?></strong></td>
            <td class="hidden-480">
              <span class="adtnlcostdiv" id="adtnlcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
          <tr class="frontbacktd">
                <td class="left"><strong><?php //echo $front_back; ?></strong></td>
                <td class="hidden-480">
                  <span class="frcostdiv" id="frcostdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
          </tr>
          <tr id="baseaccessoriestd" class="baseaccessoriestd">
            <td class="left"><strong><?php //echo $accessorie_base; ?></strong></td>
            <td class="hidden-480">
              <span class="baseacccstdiv" id="baseacccstdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>-->

          <tr id="extraaccessoriestd" class="extraaccessoriestd">
            <td class="left"><strong><?php echo $accessorie; ?></strong></td>
            <td class="hidden-480">
              <span class="extraacccstdiv" id="extraacccstdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
          
<!--          <tr id="extraprocessingtd" class="extraprocessingtd">
            <td class="left"><strong><?php //echo $extraprocessingcost; ?></strong></td>
            <td class="hidden-480">
              <span class="extraprocessingdiv" id="extraprocessingdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>-->

          <tr>
            <td class="left"><strong><?php echo $quantity; ?></strong></td>
            <td class="hidden-480">
              <span class="totalnoofproductbigfrmtdiv"></span>
            </td>
          </tr>
          
          <tr id="promovaltd" class="promovaltd">
            <td class="left"><strong><?php echo $promoval; ?></strong></td>
            <td class="hidden-480">
              <span class="promovaldiv" id="promovaldiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

<!--          <tr id="promopricetd" class="promopricetd">
            <td class="left"><strong><?php //echo $beforpromoprice; ?></strong></td>
            <td class="hidden-480">
              <span style="text-decoration:line-through" class="promopricediv" id="promopricediv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="promovaltd" class="promovaltd">
            <td class="left"><strong><?php //echo $promoval; ?></strong></td>
            <td class="hidden-480">
              <span class="promovaldiv" id="promovaldiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="operatorreviewtd" class="operatorreviewtd">
            <td class="left"><strong><?php //echo $operatorreview_cost; ?></strong></td>
            <td class="hidden-480">
              <span class="operatorreviewdiv" id="operatorreviewdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>-->

          <tr>
            <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
            <td class="hidden-480">
              <span class="totalproductcostdiv" id="totalproductcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="ivatd" class="ivatd">
            <td class="left"><strong><?php echo $iva_cost; ?></strong></td>
            <td class="hidden-480">
              <span class="ivadiv" id="ivadiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="shippingdatetd" class="shippingdatetd">
            <td class="left red"><strong><?php echo $Shippingdate; ?></strong></td>
            <td class="hidden-480">
              <span class="shippingdatediv" id="shippingdatediv"></span>              
            </td>
          </tr>
          
          <tr>
            <td class="left"><strong><?php echo $totalcost; ?></strong></td>
            <td class="hidden-480">
              <span class="totalcostdiv" id="totalcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <input type="hidden" id="bigtotlprice" name="bigtotlprice" value="">
    <input type="hidden" id="type" name="type" value="big">
    <?php
    $price_path = base_path() . path_to_theme() . "/get-price.php";
    $dates_path = base_path() . path_to_theme() . "/get-dates.php";
    $shipping_path = base_path() . path_to_theme() . "/set-shipping.php";
    $fixed_costs_path = base_path() . path_to_theme() . "/get-fixed-costs.php";
    $processing_rules_path = base_path() . path_to_theme() . "/get-processing-rules.php";
    ?>
    <input type="hidden" name="price_path" value="<?php echo $price_path; ?>" id="price_path">
    <input type="hidden" value="<?php echo $dates_path; ?>" name="dates_path" id="dates_path">
    <input type="hidden" value="<?php echo $fixed_costs_path; ?>" name="fixed_costs_path" id="fixed_costs_path">
    <input type="hidden" value="<?php echo $processing_rules_path; ?>" name="processing_rules_path" id="processing_rules_path">
    <input type="hidden" value="<?php echo $shipping_path; ?>" name="shipping_path" id="shipping_path">
    <input type="hidden" value="" id="extrslct" name="extrslct">
    <input type="hidden" value="" id="extraccslct" name="extraccslct">
    <input type="hidden" value="0" id="dates_count" name="dates_count">

    <!-- input hidden per il carrello -->
    <input type="hidden" id="material_cost" name="material_cost">
    <input type="hidden" id="processing_cost" name="processing_cost">
    <input type="hidden" id="structure_cost" name="structure_cost">
    <input type="hidden" id="total_cost" name="total_cost">
    <input type="hidden" id="shipping_date" name="shipping_date">
    <input type="hidden" id="promo_value" name="promo_value">
    <!-- input hidden per il carrello -->


  </div>
  <div class="btn-add-cart">
    <button class="btn btn-success pull-right" type="submit" name="proceed" value="Proceed" id="bigproceed"><?php echo $addcart; ?></button>
  </div>
</div>

<!-- FINE SOMMARIO  -->  
<?php
//stampa tutti gli input hidden
echo $mystr;
?>
</form>

<div class="clearfix"></div>
<?php
include('publisher-editor.tpl.php');
?>
<div class="clearfix"></div>


<div class="space-2 col-sm-12 col-md-12"></div>
<div class="space-2 col-sm-12 col-md-12"></div>
<div class="space-2 col-sm-12 col-md-12"></div>
<div class="clearfix"></div>
<div class="col-md-12 col-sm-12"></div>

<!-- PRODOTTI CORRELATI -->
<section class="row spacer">
  <div class="col-md-12">
    <?php
      $relresult = MP\FormoneBigFormatQuery::create()
          ->where('FormoneBigFormat.SubcategoryId =?', $bigresult_findone->getSubcategoryId())
          ->where('FormoneBigFormat.FormId !=?', $bigresult_findone->getFormId())
          ->filterByStatus(1)
          ->orderByFormId('desc')
          ->limit(3)->find();
      if (sizeof($relresult) > 0) {
      ?>
    <h3 class="section-title text-center">
      <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image" style="margin-right: 15px"></span>
      <?php echo $prodcorrelati; ?>
      <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image" style="margin-left: 10px"></span>
    </h3>
      <?php } ?>
    <div class="row">
      
      <?php 
      foreach ($relresult as $relres) {
       
        $relproduct = MP\FormoneAdditionalInfoQuery::create()
          ->filterByFormId($relres->getFormid())
          //->filterByWebsiteId($lang_name)
          ->findOne();
        $rellanguage = MP\FormoneLangQuery::create()
          ->where('FormoneLang.FormId =?', $relres->getFormid())
          ->where('FormoneLang.LanguageId =?', $lang_name)
          ->findOne();

        //$name = $rellanguage->getName();
        $name = substr($rellanguage->getTitle(), 0, 22);
        $pos = strpos($rellanguage->getDescription(), " ", 130);
        
        if (strlen($rellanguage->getDescription()) < 130)
            $description = $rellanguage->getDescription();
        else {
            $description = substr($rellanguage->getDescription(), 0, $pos);
            if (strlen($rellanguage->getDescription()) > $pos)
                $description .= "...";
        }
        
        $id = $relproduct->getFormId();
        
        if($relproduct->getImageListing() != 0 && $relproduct->getImageListing() != ""){
          $ImgHome = file_load($relproduct->getImageListing());
        }else{
          $ImgHome = file_load($relproduct->getImage());
        }
        
        //$ImgHome = file_load($relproduct->getImage());
        $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
        $promo_flag = $relproduct->getPromoFlag();
        $promoprice = $relproduct->getPromotionPrice();
        $promotype = $relproduct->getPromoPriceType();
        $baseprice = $relres->getBasePriceForQuantity();
        
                
        $tmp_subslug = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($relres->getSubcategoryId())->filterByLanguageId($lang_name)->findOne();              
        $tmp_proslug = MP\ProductsgroupLangQuery::create()->filterByProductGroupId($relres->getProductgroupId())->filterByLanguageId($lang_name)->findOne();

        $prod_slug = $rellanguage->getSlug();
        $subslug = $tmp_subslug->getSlug();
        $pro_slug = $tmp_proslug->getSlug();

        $url_related = $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $id;

        ?>
  <?php $x = t("vishal"); ?>
        <input type="hidden" value="<?php echo $x; ?>" id="valdate1">
        <div class="col-sm-4 col-md-4 ">
          <a href="<?php echo $url_related; ?>">
            <div class=" products-Thumbnail">
              <div class="item item-type-line item--small">
                <span class="item-hover">
                  <div class="item-info">
                    <div class="date"><?php echo $description; ?></div>
                    <div class="line"></div>
                    <div class="date"><?php //echo $pdate; ?></div>
                  </div>
                  <div class="mask"></div>
                </span>
                <div class="item-img"><img class="img-responsive" src="<?php echo $ImgHomePath; ?>"/></div>
              </div>
              <?php if (strtolower($promo_flag) == 'promo') { ?>
                <div class="products-title"><em><?php echo t('Promo'); ?></em></div>
              <?php } ?>
              <?php if (strtolower($promo_flag) == 'best price') { ?>
                <div class="products-title2"><em><?php echo t('Best Price'); ?></em></div>
              <?php } ?>
              <?php if (strtolower($promo_flag) == 'new') { ?>
                <div class="products-title1"><em><?php echo t('New'); ?></em></div>
              <?php } ?>
              <div class="clr"></div>
              <p><?php echo $name; ?> </p>
              <?php
              if (strtolower($promo_flag) == 'promo') {
                $actualprice = $baseprice;
                if ($promotype == "value") {
                  $baseprice = $baseprice - $promoprice;
                }
                elseif ($promotype == "percentage") {
                  $promodisc = $promoprice * $baseprice;
                  $promodisc = $promodisc / 100;
                  $baseprice = $baseprice - $promodisc;
                }
                $actualprice= number_format((float)$actualprice, 2, ".","");
                $baseprice= number_format((float)$baseprice, 2, ".","");
                ?>
                <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
  <?php
  }
  else {
    ?>
                <p class="p"><?php echo $from; ?> &euro; <?php echo $baseprice; ?></p><?php } ?>
            </div>
          </a>
        </div>
      <?php
}
?>
    </div>
  </div>
</section>
<!-- fine PRODOTTI CORRELATI -->

 <?php include('product_review.tpl.php'); ?>