<?php

global $base_url;

?>


<form action="<?php echo $base_url; ?>/it/tonic/squote-price/update" method="POST" name="small-quote-price" style="overflow: scroll;">
    <input type="hidden" name="id" value="<?php echo $form_id; ?>" />
    
    <h3>Materiali</h3>
    <?php
    foreach ($dimensions as $dimension) {
        $dimension_two = MP\DimensionsTwoQuery::create()->filterByDimensionId($dimension->getDimensionId())->findOne();
        ?>

        <h4><?php echo $name . " " . $dimension_two->getName(); ?></h4>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <?php
                    foreach ($materials as $material) {
                        $material_two = MP\MaterialsTwoQuery::create()->filterByMaterialId($material->getMaterialId())->findOne();
                        $material_two_lang = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId('it')->findOne();
                        $material_two_name = $material_two_lang->getName();
                        foreach ($weights as $weight) {
                            $weight_two = MP\WeightsTwoQuery::create()->filterByWeightId($weight->getWeightId())->findOne();
                            $weight_two_name = $weight_two->getWeight();
                            ?>
                            <th><?php echo $material_two_name . " " . $weight_two_name . " (gr/mq)"; ?></th>
                            <?php
                        }
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($amounts as $amount) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $amount; ?>
                        </td>
                        <?php
                        foreach ($materials as $material) {
                            foreach ($weights as $weight) {
                                ?>
                                <td>
                                    <?php
                                        $price = MP\FormtwoBasePriceQuery::create()
                                                    ->filterByFormId($form_id)
                                                    ->filterByDimensionTwoId($dimension->getDimensionId())
                                                    ->filterByNoCopies($amount)
                                                    ->filterByMaterialTwoId($material->getMaterialId())
                                                    ->filterByWeightTwoId($weight->getWeightId())
                                                    ->findOne();
                                        if($price){
                                            $price_value = $price->getPrice();
                                        } else {
                                            $price_value = 0;
                                        }
                                            
                                    ?>
                                    <input 
                                        data-dimension="<?php echo $dimension->getDimensionId(); ?>"
                                        data-amount="<?php echo $amount; ?>"
                                        data-material="<?php echo $material->getMaterialId(); ?>"
                                        data-weight="<?php echo $weight->getWeightId(); ?>"
                                        name="dimension<?php echo $dimension->getDimensionId(); ?>-amount<?php echo $amount; ?>-material<?php echo $material->getMaterialId(); ?>-weight<?php echo $weight->getWeightId(); ?>"
                                        type="number"
                                        step="0.000001"
                                        value="<?php echo $price_value; ?>"
                                        size="6" />
                                </td>
                                <?php
                            }
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        <?php
    }
    
    if(count($extra_processings)>0 || $is_front_back == 1){
    
      ?>
          <hr />
          <h3>Lavorazioni</h3>
      <?php

      foreach ($dimensions as $dimension) {
          $dimension_two = MP\DimensionsTwoQuery::create()->filterByDimensionId($dimension->getDimensionId())->findOne();
          ?>

          <h4><?php echo $name . " " . $dimension_two->getName(); ?></h4>
          <table>
              <thead>
                  <tr>
                      <th></th>
                      <?php
                      foreach ($extra_processings as $extra_processing) {
                          $extra_processing_two = MP\ProcessingTwoQuery::create()->filterByProcessingId($extra_processing->getProcessingId())->findOne();
                          $extra_processing_two_lang = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($extra_processing->getProcessingId())->filterByLanguageId('it')->findOne();
                          $extra_processing_two_name = $extra_processing_two_lang->getName();
                          ?>                          
                          <th><?php echo $extra_processing_two_name; ?></th>
                          <?php
                      }

                      if($is_front_back == 1){
                      ?>
                          <th>Stampa Fronte Retro</th>
                      <?php
                      }
                      ?>
                  </tr>
              </thead>
              <tbody>
                  <?php
                  foreach ($amounts as $amount) {
                      ?>
                      <tr>
                          <td>
                              <?php echo $amount; ?>
                          </td>
                          <?php
                          foreach ($extra_processings as $extra_processing) {
                              ?>
                              <td>
                                  <?php
                                      $price_proc = MP\FormtwoBasePriceProcessingQuery::create()
                                                  ->filterByFormId($form_id)
                                                  ->filterByDimensionTwoId($dimension->getDimensionId())
                                                  ->filterByNoCopies($amount)
                                                  ->filterByProcessingId($extra_processing->getProcessingId())
                                                  ->findOne();
                                      if($price_proc){
                                          $price_proc_value = $price_proc->getPrice();
                                      } else {
                                          $price_proc_value = 0;
                                      }

                                  ?>
                                  <input
                                      data-dimension="<?php echo $dimension->getDimensionId(); ?>" 
                                      data-amount="<?php echo $amount; ?>" 
                                      data-processing="<?php echo $extra_processing->getProcessingId(); ?>" 
                                      type="number" 
                                      value="<?php echo $price_proc_value; ?>" 
                                      size="6"
                                      step="0.000001"
                                      name="dimension<?php echo $dimension->getDimensionId(); ?>-amount<?php echo $amount; ?>-processing<?php echo $extra_processing->getProcessingId(); ?>"
                                      />
                              </td>
                              <?php
                          }

                          if($is_front_back == 1){
                            ?>
                            <td>
                                  <?php
                                      $price_front_back = MP\FormtwoBasePriceFrontBackQuery::create()
                                                  ->filterByFormId($form_id)
                                                  ->filterByDimensionTwoId($dimension->getDimensionId())
                                                  ->filterByNoCopies($amount)
                                                  ->findOne();
                                      if($price_front_back){
                                          $price_front_back_value = $price_front_back->getPrice();
                                      } else {
                                          $price_front_back_value = 0;
                                      }

                                  ?>
                                  <input
                                      data-dimension="<?php echo $dimension->getDimensionId(); ?>" 
                                      data-amount="<?php echo $amount; ?>"
                                      type="number" 
                                      value="<?php echo $price_front_back_value; ?>" 
                                      size="6"
                                      step="0.000001"
                                      name="dimension<?php echo $dimension->getDimensionId(); ?>-amount<?php echo $amount; ?>-front_back"
                                      />
                              </td>
                            <?php
                          }
                          ?>
                      </tr>
                      <?php
                  }
                  ?>
              </tbody>
          </table>
          <?php
      }
    }
    if($type == "folding"){
      
      ?>
        <hr />
        <h3>Tipologie di piega</h3>
      <?php
      foreach ($dimensions as $dimension) {
        $dimension_two = MP\DimensionsTwoQuery::create()->filterByDimensionId($dimension->getDimensionId())->findOne();
        ?>

        <h4><?php echo $name . " " . $dimension_two->getName(); ?></h4>
        <table>
            <thead>
                <tr>
                    <th></th>
                    <?php
                    foreach ($folding_types as $folding_type) {
                        $folding_type_two = MP\FoldingTypeTwoQuery::create()->filterByFoldingTypeId($folding_type->getFoldingTypeId())->findOne();
                        $folding_type_two_name = $folding_type_two->getName();
                        ?>                          
                        <th><?php echo $folding_type_two_name; ?></th>
                        <?php
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($amounts as $amount) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $amount; ?>
                        </td>
                        <?php
                        foreach ($folding_types as $folding_type) {
                            ?>
                            <td>
                                <?php
                                    $price_fol = MP\FormtwoBasePriceFoldingTypeQuery::create()
                                                ->filterByFormId($form_id)
                                                ->filterByDimensionTwoId($dimension->getDimensionId())
                                                ->filterByNoCopies($amount)
                                                ->filterByFoldingTypeId($folding_type->getFoldingTypeId())
                                                ->findOne();
                                    if($price_fol){
                                        $price_fol_value = $price_fol->getPrice();
                                    } else {
                                        $price_fol_value = 0;
                                    }

                                ?>
                                <input
                                    data-dimension="<?php echo $dimension->getDimensionId(); ?>" 
                                    data-amount="<?php echo $amount; ?>" 
                                    data-folding_type="<?php echo $folding_type->getFoldingTypeId(); ?>" 
                                    type="number" 
                                    value="<?php echo $price_fol_value; ?>" 
                                    size="6"
                                    step="0.000001"
                                    name="dimension<?php echo $dimension->getDimensionId(); ?>-amount<?php echo $amount; ?>-folding_type<?php echo $folding_type->getFoldingTypeId(); ?>"
                                    />
                            </td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    <?php
        }
    }
    ?>
    <input type="submit" value="Salva" />
</form>
