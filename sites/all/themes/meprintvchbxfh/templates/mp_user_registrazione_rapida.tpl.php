<form id="form-registration" class="form-registration spacer" action="/add-user-rapid" method="POST" >

    <fieldset class="boxed">
        <legend><?php echo t('Email and Password');?></legend>            
        <div class="row">
            <div class="form-group col-sm-4">
                <label for=""><?php echo $lemail; ?></label>
                <input type="email" class="form-control" name="email" id="form-registration__email" required />
            </div>
            <div class="form-group col-sm-4">
                <label for=""><?php echo $lemailbis; ?></label>
                <input type="email" class="form-control" name="email_bis" id="form-registration__emailbis" required />
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-4">
                <label for=""><?php echo t('Password');?></label>
                <input type="password" class="form-control" name="password" id="form-registration__pw" required />
            </div>
            <div class="form-group col-sm-4">
                <label for=""><?php echo t('Repeat password');?></label>
                <input type="password" class="form-control" name="password_bis" id="form-registration__pwbis" required />
            </div>
        </div>
    </fieldset>
    <fieldset class="boxed">
        <div class="checkbox">
          <label class="form-registration__privacy">
            <input type="checkbox" name="privacy" id="form-registration__privacy" required > <?php echo t('I accept the regulations for privacy'); ?>
          </label>
        </div>
        <button class="btn btn-red"><?php echo t('Register'); ?></button>
    </fieldset>
</form>

