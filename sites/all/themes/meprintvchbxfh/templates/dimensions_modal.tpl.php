<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal modal-gallery fade bs-example-modal-lg" id="dimensionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Dimensioni</h4>
      </div>
      <div class="modal-body">
        <?php foreach ($materialmodals as $mat) { ?>
          <div class="text-center" id="modalmaterial<?php echo $mat['id']; ?>" style="display:none; margin:auto;">
            <?php if ($mat['img']): ?>  
              <img src="<?php echo $mat['img']; ?>" />
              <div class="space-3"></div>
            <?php endif; ?>                        
            <span class="text-center" style="color:#ED7400; font-size:25px"> <strong><?php echo $mat['text']; ?> </strong></span>
          </div>
        <?php } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal"><?php echo t('close'); ?></button>
      </div>
    </div>
  </div>
</div>