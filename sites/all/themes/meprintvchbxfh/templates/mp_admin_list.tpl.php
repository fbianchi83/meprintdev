<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript">
jQuery(document).ready(function($) {
    filtra();
});

function filtra() {
    var ragsac= jQuery('#ragsoc').val();
    var nome= jQuery('#nome').val();
    var piva= jQuery('#piva').val();
    var cf= jQuery('#cf').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filteradmin",
        data:'ragsac='+ragsac+'&nome='+nome+'&piva='+piva+'&cf='+cf,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            if (result) {
                var i=0;
                while (i<result.length) {
                    temp += "<tr>";
                    temp += "<td>"+result[i][0]+"</td>";
                    temp += "<td>"+result[i][1]+"</td>";
                    temp += "<td>"+result[i][2]+"</td>";
                    temp += "<td>"+result[i][3]+"</td>";
                    temp += "<td><a href='/<?php echo $language->language; ?>/tonic/users/edit/"+result[i][0]+"'><?php echo t('Modifica'); ?></a></td>";
                    temp += "<td><a href='/<?php echo $language->language; ?>/tonic/users/edit-pass/"+result[i][0]+"'><?php echo t('Cambia Password'); ?></a></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='6'> <?php echo t('Nessun utente presente'); ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('S No'); ?></th><th><?php echo t('Nome'); ?></th><th><?php echo t('E-Mail'); ?></th><th><?php echo t('Blacklist'); ?></th><th><?php echo t('Modifica'); ?></th><th><?php echo t('Cambia password'); ?></th></tr></thead>" + temp;
            jQuery('#mp-list-order').empty().append(temp);
        }
    });
}
</script>

<div class="col-sm-12 col-md-12 ">
    <div id='dailychoice' class="billcentered margin10"> 
        <h3> <?php echo t('Filtra Utenti'); ?> </h3>
        <?php echo t('Ragione Sociale'); ?> <input type='text' class="margin10" id='ragsoc'>
        <?php echo t('Nome e Cognome'); ?> <input type='text' class="margin10" id='nome'><br> 
        <?php echo t('Partita IVA'); ?> <input type='text' class="margin10" id='piva'> 
        <?php echo t('Codice Fiscale'); ?> <input type='text' class="margin10" id='cf'> <br>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtra(); return false;'>
    </div>
    
    <table id="mp-list-order" class="table table-bordered table-striped table-hover"></table>
</div>