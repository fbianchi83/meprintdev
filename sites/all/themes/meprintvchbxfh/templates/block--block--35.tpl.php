<?php
  global $base_url;
?>    	
<header class="row">
  <div class="col-sm-2 col-md-2"></div>
  <div class="col-sm-8 col-md-8">
    <h3 class="text-center section-title">
        <span>
            <img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-right: 15px">
            <strong>MePrint</strong>
        </span>
        &nbsp; <?php echo t('TI OFFRE SEMPRE IL MEGLIO!'); ?>
        <span>
            <img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-left: 15px">
        </span>
    </h3>
  </div>
  <div class="col-sm-2 col-md-2"></div>
</header>
<article class="row">
  <div class="col-sm-4 col-md-4">
    <span class="blue-bg_img"><i class="fa fa-magic fa-5x"></i></span>
      <div class="blue-bg_cont">
      <h4 class="h4"><?php echo t('Colori luminosi'); ?></h4>
      <p><?php echo t('Stampe sempre perfette sia in esterno che in interno: colori brillanti per stampe più belle'); ?></p>
      </div>
  </div>
  <div class="col-sm-4 col-md-4">
    <span class="blue-bg_img"><i class="fa fa-print fa-5x"></i></span>
      <div class="blue-bg_cont">
      <h4 class="h4"><?php echo t('Tutti i materiali'); ?></h4>
      <p><?php echo t('Riusciamo a stampare su qualunque tipo di supporto rigido: forex, plexiglass, alluminio...'); ?></p>
      </div>
  </div>
  <div class="col-sm-4 col-md-4">
    <span class="blue-bg_img"><i class="fa fa-euro fa-5x"></i></span>
      <div class="blue-bg_cont">
      <h4 class="h4"><?php echo t('Prezzi incredibili'); ?></h4>
      <p><?php echo t('I nostri prezzi sono tra i più bassi del mercato della stampa digitale online'); ?></p>
      </div>
  </div>
</article>
    