<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
   $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dailyend" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#dailystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    }); 
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    var prevDate = new Date();
    $("#dailystart").datepicker("setDate",currentDate);
    $("#dailyend").datepicker("setDate",prevDate);
    filtragroup();
});

function filtragroup() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadgroupcart",
        data:'datastart='+datestart+'&dataend='+dateend,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            if (result) {
                var i=0;
                while (i<result.length) {
                    //console.log(result[i]['number']);
                    temp += "<tr>";
                    temp += "<td>"+result[i]['uid']+"</td>";
                    temp += "<td><strong>"+result[i]['name']+"</strong></td>";
                    temp += "<td><strong>"+result[i]['number']+"</strong></td>";
                    temp += "<td><strong>"+parseFloat(result[i]['total']/result[i]['number'])+"</strong></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='4'> <?php echo $noorders; ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('ID'); ?></th><th width='60%'><?php echo t('Client'); ?></th><th><?php echo t('Cart Number'); ?></th><th><?php echo t('Avg. Products per Cart'); ?></th></tr></thead>" + temp;
            jQuery('#mp-list-order').empty().append(temp);
        }
    });
}
</script>

<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $topcartstats; ?> </h3>
    
    <div id='dailychoice' class="billcentered"> 
        <h3> <?php echo $topcartdates; ?> </h3>
        <?php echo $datestart; ?> <input type='text' class="margin10" id='dailystart'>
        <?php echo $dateend; ?> <input type='text' class="margin10" id='dailyend'> 
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtragroup(); return false;'>
    </div>
    
    <table id="mp-list-order" class="table table-bordered table-striped table-hover"></table>
</div>