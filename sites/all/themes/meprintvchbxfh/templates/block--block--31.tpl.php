<?php
 global $base_url;
 global $language;
 $lang_name = $language->language;
?>
<div class="row">
  <div class="col-sm-3 col-md-3">
    <h4 class="foooter-h4">CONTATTI</h4>
    <p class="footer-bg-p">MePrint è un'azienda specializzata nella stampa digitale online di grande formato. Per qualsiasi richiesta puoi inviarci una mail a <a href="mailto: info@meprint.it">info@meprint.it</a> o telefonarci al numero 0587-734692.</p>
  </div>
  <div class="col-sm-2 col-md-2">
      <h4 class="foooter-h4">Azienda</h4>
      <ul class="footer-ul">
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/chi-siamo">Chi Siamo</a></li>
                    <li><a href="http://blog.meprint.it/" target="new">Blog</a></li>

        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/iniziative">Iniziative</a></li>
        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/spedizione-rapida">Spedizione Rapida</a></li>

          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/faq">FAQ</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/termini-e-condizioni">Termini e condizioni</a></li>
                    <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/contact">Contatti</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/privacy">Privacy</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/cookie">Cookie Policy</a></li>
      </ul>
  </div>
  <div class="col-sm-2 col-md-2">
      <h4 class="foooter-h4">Focus Prodotti</h4>
      <ul class="footer-ul">
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/stampa-forex">Stampa Forex</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/stampa-plexiglass">Stampa Plexiglass</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/pvc-adesivo">Stampa Adesivi Pvc</a></li>
                    <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/adesivi-prespaziati">Stampa Adesivi Prespaziati</a></li>

          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/stampa-tessuto-bandiere">Stampa Bandiere</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/banner-striscioni">Stampa Banner e Striscioni</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/espositori-rollup">Roll up ed espositori</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/verniciatura-uv-pannelli">Verniciatura UV pannelli</a></li>
      </ul>
  </div>
  <div class="col-sm-2 col-md-2">
      <h4 class="foooter-h4">Supporti</h4>
      <ul class="footer-ul">
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/supporti-rigidi">Stampa su supporti rigidi</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/stampa-pannelli">Stampa Pannelli</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/espositori-avvolgibili">Stampa Espositori avvolgibili</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/manifesti-affissioni">Stampa Manifesti</a></li>
          <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/stampa-carta-fotografica">Stampa su carta Fotografica</a></li>
      </ul>
  </div>
  <div class="col-sm-3 col-md-3">
      <h4 class="foooter-h4">PAGAMENTI</h4>
      <p>
          <span class="pay_img"><img alt="paypal" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/paypal.png"></span>
          <span class="pay_img"><img alt="master-card" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/master-card.png"></span>
          <span class="pay_img"><img alt="visa" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/visa.png"></span>
          <span class="pay_img"><img alt="master" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/master.png"></span>
          <span class="pay_img"><img width="40px" height="28px" alt="cash" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/check.jpg"></span>
          <span class="pay_img"><img width="40px" height="28px" alt="cash" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/dollars.jpg"></span>
          <span class="pay_img"><img alt="sofort" width=43px" height="25px" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/sofort.png"></span>
      </p>
    <div class="space-3 clr"></div>
      <h4 class="foooter-h4">SPEDIZIONI</h4>
      <p>
          <span class="pay_img"><img width=43px" height="27px" alt="brt" src="<?php echo $base_url; ?>/sites/default/files/shipping/bartolini_3.jpg"></span>
          <span class="pay_img"><img width=43px" height="27px" alt="tnt" src="<?php echo $base_url; ?>/sites/default/files/shipping//tnt-1.jpg"></span>
      </p>
  </div>
</div>
<div class="space-3 clr"></div>
  <div class="row">
      <div class="col-xs-3 col-sm-3 col-md-3"><img alt="image" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/wave.png"></div>
      <div class="col-xs-6 col-sm-6 col-md-6"></div>
      <div class="col-xs-3 col-sm-3 col-md-3 text-right"><img alt="image" src="<?php echo $base_url; ?>/sites/all/themes/meprint/images/wave.png"></div>
  </div>
  <div class="row">
      <p class="footer-cright">MePrint è un marchio di Cartoons S.r.l. P.IVA 01460970500 Registro delle Imprese di Pisa Capitale Sociale € 40.000 i.v.</p>
  </div>
