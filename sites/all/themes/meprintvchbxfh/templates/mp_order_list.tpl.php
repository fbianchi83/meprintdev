<?php 
  global $language;
?>

<table id="mp-list-order" class="table table-mp table-striped table-hover">
  <thead>
    <tr>
      <th><?php echo t('N°'); ?></th>
      <th><?php echo t('Order Date'); ?></th>
      <?php echo ( $account->uid == 1 )?"<th>" . t('Client') . "</th>":""; ?>
      <th><?php echo t('Amount'); ?></th>
      <th><?php echo t('Products'); ?></th>
      <th><?php echo t('Status'); ?></th>
      <th class="text-right"><?php echo t('Display'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach( $rows as $row ): ?>
    <tr>
      <td><?php echo $row[0]; ?></td>
      <td><strong><?php echo $row[1]; ?></strong></td>
      <?php echo ( $account->uid == 1 )?"<td><strong>" . $row[2] . "</strong></td>":""; ?>
      <td><?php echo $row[3]; ?> &euro;</td>
      <td><?php echo $row[4]; ?></td>
      <td class="text-uppercase"><strong><?php echo $row[6]; dd("|".$row[6]."|");?></strong></td>
      <td class="text-right order-detail">
          <?php if ($row[6] == "Ordine spedito") { ?> <a href="/sites/default/files/bills/bill<?php echo $row[0]; ?>.pdf" ><i style="color: black;" class="fa fa-download"></i></a> <?php } ?>
          <a href="/<?php echo $language->language; ?>/my-orders/edit/<?php echo $row[0]; ?>"><i class="fa fa-file-text-o"></i></a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>


<?php
  pager_default_initialize($total, $perpage);    
  echo theme('pager');
