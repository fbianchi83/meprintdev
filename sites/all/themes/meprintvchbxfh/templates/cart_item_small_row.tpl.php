
<div class="col-md-12 col-sm-12 col-xs-12 cartdetailspage cart_<?php echo $cart_item_id; ?>">
  <div class="clearfix"></div>
  <div class="col-md-4 col-sm-4 col-xs-7">
    <div class="cart_4">
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails_4">
        <b>
          <span class="fa cartdetails--open fa-plus fa-2x" data-toggle="collapse" href="#cart_<?php echo $cart_item_id; ?>" aria-expanded="false"></span> <?php echo $presult->getName(); ?>
        </b>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 text-center hidden-xs">
    <?php
    $images = MP\CartItemFileQuery::create()->filterByCartItemId($cart_item_id)->findOne();
    ?>
    <?php
    if ($images) {
      $image = $images->getFid();
      $image_uri = file_load($image)->uri;
      ?>
      <img src="<?php echo image_style_url("thumbnail", $image_uri); ?>" />
      <?php
    }
    else {
      echo '<i class="fa fa-camera"></i>';
    }
    ?>
  </div>
  <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
    <div class="col-md-6 col-sm-6 cartdetails text-center">
      <?php echo $cart_item->getQuanitity() ?>
    </div>
    <div class="col-md-6 col-sm-3 cartdetails text-right">
      <div class="col-md-12 col-sm-12 delete_icon cartdetails">
        <a class="deletecart" url="<?php echo $cart_item_id; ?>">
          <i class="fa fa-trash-o"></i>
        </a>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-3 cartdetailsline">
    <div class="cartdetails cartprice text-right">
      <?php
      $totlprc = $cart_item->getTotalPrice();
      echo number_format((float) $totlprc, 2, '.', '') . ' &euro;';
      ?>
    </div>
  </div>
  <div class="col-md-12 col-xs-12">
    <div class="collapse" id="cart_<?php echo $cart_item_id; ?>">

      <!-- NUMERO DI COPIE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $nocopies; ?>
        </span>
        <span class="datacart">
          <?php echo $cart_item->getQuanitity() ?>
        </span>
      </div>

      <?php
      //TIPOLOGIA PIEGA
      $cart_item_folding = MP\FoldingTypeTwoQuery::create()->filterByFoldingTypeId($cart_item->getFoldingTypeId())->findOne();
      if (count( $cart_item_folding ) > 0) {                
        ?>
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart"><?php echo $lfolding; ?></span>
        <span class="datacart">
          <?php echo $cart_item_folding->getName(); ?>
        </span>
      </div>
      <?php } ?>
            
      
      <!-- DIMENSIONI -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $cdimension; ?>
        </span>
        <span class="datacart">
          <?php
          $dquery = MP\DimensionsTwoQuery::create()->filterByDimensionId($cart_item->getDimensionId())->findOne();
           echo $dquery->getName();          
          ?>
        </span>
      </div>

      
      <!-- ORIENTAMENTO -->              
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart"><?php echo $lorientation; ?></span>
        <span class="datacart">
          <?php echo ($cart_item->getOrientation() == 0)?$orientation_h:$orientation_v; ?>
        </span>
      </div>
      
      <!-- MATERIALE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $cmaterial; ?>
        </span>
        <span class="datacart">
          <?php
          $mquery = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($cart_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
          echo $mquery->getName();
          ?>
        </span>
      </div>

      
      <?php
      // GRAMMATURA
      $cart_item_weight = MP\WeightsTwoQuery::create()->filterByWeightId($cart_item->getWeightId())->findOne();
      if (count( $cart_item_weight ) > 0) {                
        ?>
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart"><?php echo $lgrammatura; ?></span>
        <span class="datacart">
          <?php echo $cart_item_weight->getWeight() . ' ' . $measure_weight; ?>
        </span>
      </div>
      <?php } ?>
      
      
      <?php
      // FACCIATE              
      if ( $cart_item->getNfaces() > 0 ) {                
        ?>
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart"><?php echo ucfirst($lnumface); ?></span>
        <span class="datacart">
          <?php echo $cart_item->getNfaces(); ?>
        </span>
      </div>
      <?php } ?>
      
      
      <?php
      // TIPO COPERTINA              
      if ( $cart_item->getCover() > 0 ) {
        $add_cover_str = "";
        if ( $cart_item->getCover() == 2 ) {                  
          //COPERTINA SPECIFICA: materiali e grammatura
          $mcover = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($cart_item->getIdMaterialCover())->filterByLanguageId($lang_name)->findOne();                    
          $wcover = MP\WeightsTwoQuery::create()->filterByWeightId($cart_item->getIdWeightCover())->findOne();

          $add_cover_str = ", <em>". $cmaterial . ": " . $mcover->getName() ."</em>, <em>". $lgrammatura . ": " . $wcover->getWeight() . " " .$measure_weight ."</em>"; 

        }
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart"><?php echo ucfirst($ltypecover); ?></span>
          <span class="datacart">
            <?php echo ( $cart_item->getCover() == 1 )?$item_page:$different_page.$add_cover_str; ?>
          </span>
        </div>

      <?php } ?>
      
      
      
      <?php
      //EVENTUALI LAVORAZIONI EXTRA
      $cart_item_extra_processings = MP\CartItemExtraProcessingQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
      if (count($cart_item_extra_processings) > 0) {
        ?>

        <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_extra_processings) > 1 ) ? 'cartdetails--more' : ''; ?>" >
          <span class="labelcart">
            <?php echo $extraprocessing; ?>
          </span>

          <?php
          foreach ($cart_item_extra_processings as $cart_item_extra_processing) {
            ?>
            <span class="datacart">
              <?php
              $extra_processing_lang = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($cart_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
              $extra_processing = MP\ProcessingTwoQuery::create()->findPk($cart_item_extra_processing->getProcessingId());

              echo $extra_processing_lang->getName();


              ?>
            </span>
            <?php
          }
          ?>     
        </div>
        <?php
      }
      ?>

      <?php
      //EVENTUALI ACCESSORI
      $cart_item_accessories = MP\CartItemAccessoryQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
      //print_r( $cart_item_accessories );
      if (count($cart_item_accessories) > 0) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_accessories) > 1 ) ? 'cartdetails--more' : ''; ?>"">
             <span class="labelcart">
                 <?php echo $accessorie; ?>
          </span>

          <?php
          foreach ($cart_item_accessories as $cart_item_accessory) {
            ?>
            <span class="datacart">
              <?php
              $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
              $accessory_name = $accessory_lang->getName();
              $accessory_quantity = $cart_item_accessory->getQuantity();
              echo $accessory_name . " (" . $accessory_quantity . ")";
              ?>
            </span>
            <?php
          }
          ?>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE VERIFICA FILES -->
      <?php
      if ($cart_item->getOperatorReview() == 1) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $operatorreview . ' &euro;' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?>
          </span>
          <span class="datacart">
            <?php
            echo t("Yes");
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE PACCO ANONIMO -->
      <?php
      if ($cart_item->getAnonymousPack() == 1) {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $anonoshiping; ?>
          </span>
          <span class="datacart">
            <?php
            echo t("Yes");
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALE NOME PREVETIVO -->
      <?php
      if ($cart_item->getName() != "") {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $lname; ?>
          </span>
          <span class="datacart">
            <?php
            echo $cart_item->getName();
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- EVENTUALI NOTE PREVETIVO -->
      <?php
      if ($cart_item->getNote() != "") {
        ?>
        <div class="col-md-12 col-sm-12 cartdetails">
          <span class="labelcart">
            <?php echo $note; ?>
          </span>
          <span class="datacart">
            <?php
            echo $cart_item->getNote();
            ?>
          </span>
        </div>
        <?php
      }
      ?>

      <!-- DATA DI SPEDIZIONE -->
      <div class="col-md-12 col-sm-12 cartdetails">
        <span class="labelcart">
          <?php echo $Shippingdate; ?>
        </span>
        <span class="datacart">
          <?php
          $cDATE = $cart_item->getShippingDate();
          echo $cDATE->format('d-m-Y');
          ?>
        </span>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>

