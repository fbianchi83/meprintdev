<input type="hidden" name="fixed_costs" value="0" id="fixed_costs" />

<?php
$i = 1;

if( count($formone_dimensions) > 1 ){
  foreach ($formone_dimensions as $formonedimensions) {

      $dname = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->findOne();

      $image = $dname->getFile();
      $uuu = file_load($image)->uri;
      //$my_image = explode("://", $uuu);
     
      if( $uuu != '' ){
        $my_image = image_style_url("dimensioni", $uuu);        
      }
      
      ?>
      <div class="form-group col-md-3 col-sm-4 col-xs-6 text-center">
          <?php if ($my_image != '') { ?>
            <label class="dimdiv">
                <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>                
                <img src="<?php echo $my_image; ?>"/>
                <p><strong><?php echo $dname->getDescription(); ?></strong><br>
                <!--<strong><?php //echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>-->
                <input type="hidden" value="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id="bd<?php echo $dname->getDimensionId(); ?>" >
                <input type="hidden" value="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
            </label>
        </div>

      <?php } else {
        ?>
        <label class="dimdiv">
            <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>            
            <img src="<?php echo $base_url . '/sites/default/files/'; ?>camera.jpg"/>
            <p><strong><?php echo $dname->getDescription(); ?></strong><br>
            <!--<strong><?php //echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>-->
            <input type="hidden" value="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id="bd<?php echo $dname->getDimensionId(); ?>" >
            <input type="hidden" value="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
        </label>

        </div>
      <?php
      }
   
    $i++;
  }
} elseif( count($formone_dimensions) == 1 ) { 
  
  $formonedimensions_data = $formone_dimensions->getData();
  $formonedimensions = $formonedimensions_data[0];  
  
  $dname = MP\DimensionsQuery::create()->filterByDimensionId($formonedimensions->getDimensionId())->findOne();

  $image = $dname->getFile();
  $uuu = file_load($image)->uri;
  
?>  
  <div class="dimdiv--only dimdiv--circle-only col-sm-12">
    <input type ="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id = "bd<?php echo $dname->getDimensionId(); ?>" >
    <input type ="hidden" value ="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
    <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" checked="checked" />
    <img class="img-responsive pull-left" src="<?php echo image_style_url("thumbnail-new", $uuu); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $uuu); ?>' alt='' /><p><?php #echo $mat_desc; ?></p></div>" />
    <div class="dimdiv">
      <p class="mat-title"><?php echo $dname->getDescription(); ?><br>        
      <?php// echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></p>      
    </div>
    <div class="space-2"></div>
  </div>
  
<?php }
