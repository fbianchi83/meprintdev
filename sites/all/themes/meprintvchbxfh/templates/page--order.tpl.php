<?php 
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;
?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">

<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): print $breadcrumb; endif;?>  
  
	<h3>&nbsp;Order Confirm <span><img src="/sites/all/themes/meprint/images/wave.png" alt="image"></span> </h3>
   <div class="space-3"></div>
    <?php    print orderconfirm();  ?>
 
  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>