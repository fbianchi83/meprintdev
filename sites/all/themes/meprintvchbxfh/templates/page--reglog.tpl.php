<script type="text/javascript" src="https://parall.ax/parallax/js/jspdf.js"></script>
<?php

global $base_path;
global $base_url;
global $language;
$lang_name = $language->language;

?>

  <?php
    include('header.tpl.php');
  ?>
  <!--Start he Banner--->
  <section class="container">
    <h3 class="page-title"><?php echo t("Access or Register on Meprint.it"); ?><span><img src="/sites/all/themes/meprintvchbxfh/images/wave.png" alt="image" style="margin-left: 15px"></span></h3>
    <div class="space-3"></div>
    <!--End of the search--->
    <article class="row">

      <div class="col-md-12 col-sm-12">
        <header class="row">
          <div class="col-sm-12 col-md-12 ">

            <div class="space-2"></div>
            

            <?php
              print render($page['content']);              
            ?>
          </div>
        </header>
        <div class="space-2"></div>
      </div>

      
    </article>
  </section>
  
  <?php
    include('footer.tpl.php');
  ?>

<script>
  jQuery(document).ready(function($) {
    $('.cartdetails--open').click(function() {
      $(this).toggleClass('fa-plus fa-minus');
    });
  });
</script>