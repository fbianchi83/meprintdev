<?php
    global $base_url;
    global $base_path;
    global $language;
    global $user;
    
    $lang_name = $language->language;
    include DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
    include DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';

    $_SESSION['discount_id'] = "";
    $_SESSION['dicountamount'] = "";
    $_SESSION['note'] = "";
    
    if ($user->uid != 0) {
        $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->update(array('Status'=>2));
    } else {
        $cart = MP\CartQuery::create()->filterBySessionId($session_id)->filterByStatus(1)->update(array('Status'=>2));
    }
    
    $order_id= $_SESSION['orderid'];
    if ($_SESSION['payment'] == "contrassegno")
      $ord= MP\OrdersQuery::create()->filterByOrderId($order_id)->update(array('PayStatus' => 3));
    else if ($_SESSION['payment'] == "sofort" || $_SESSION['payment'] == "paypal")
        $ord= MP\OrdersQuery::create()->filterByOrderId($order_id)->update(array('PayStatus' => 2));

    include( DRUPAL_ROOT . '/sites/all/libraries/PHPMailer/class.phpmailer.php' );
    include( DRUPAL_ROOT . '/sites/all/libraries/PHPMailer/class.smtp.php' );
      
    //INVIO MAIL CONFERMA ORDINE
    mp_mail_conferma_ordine($order_id);
    
    if ($_SESSION['filesok'] == 0)
        mp_mail_file_mancanti($order_id);
    
    $session_id = $_SESSION['session_id'];
    
    // TRACCIAMENTO GOOGLE
    mp_order_track_analytics($order_id);

     /*   
    $cart->getCartId();
    $cart_elements = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->find();

    foreach ($cart_elements as $delete) {
        MP\CartItemFileQuery::create()->filterByCartItemId($delete->getCartItemId())->delete();
        MP\CartItemExtraProcessingQuery::create()->filterByCartItemId($delete->getCartItemId())->delete();
        MP\CartItemAccessoryQuery::create()->filterByCartItemId($delete->getCartItemId())->delete();
    }

    //DELETE CART ITEMS
    MP\CartItemQuery::create()->filterByCartId($cart_id)->delete();

    //DELETE CART
    MP\CartQuery::create()->filterByCartId($cart_id)->delete();
    */
     
  /*if ($_SESSION['ivapercentage'] == $VAT_RATE && $_SESSION['payment'] != "banktransfer" && $_SESSION['payment'] != "contrassegno") {
       mp_order_bill_gen($order_id);
       mp_mail_fattura($order_id);
   }*/
   
    /*require_once(DRUPAL_ROOT . base_path() . 'sites/all/libraries/php-ga-master/src/autoload.php');
    $last_order= MP\OrdersQuery::create()->filterByOrderId($order_id)->findOne();
    
    $order_billing = MP\OrderBillingDetailsQuery::create()->filterByOrderId($order_id)->findOne();
  
    $tracker = new UnitedPrototype\GoogleAnalytics\Tracker("UA-10958509-2", "http://www.meprint.it");
    $visitor = new UnitedPrototype\GoogleAnalytics\Visitor();
    $visitor->setIpAddress($last_order->getUserIp()); // posso tracciare l'indirizzo IP dell'utente - recupero il dato dall'ordine   
    $visitor->setUserAgent($last_order->getUserAgent()); // posso tracciare l'agent dell'utente - recupero il dato dall'ordine
    $session = new UnitedPrototype\GoogleAnalytics\Session();
    $page = new UnitedPrototype\GoogleAnalytics\Page('/ipn_listener.php');
    $page->setTitle('Acquisto mePrint');
    
    //Tracciamento transazione
    $transaction = new UnitedPrototype\GoogleAnalytics\Transaction();
    $transaction->setOrderId($order_id);
    $transaction->setAffiliation("meprint");
    
    $transaction->setTotal( "".number_format($last_order->getFinalPrice(), 2) );
    $transaction->setShipping($last_order->getShipPrice());
    $iva= $last_order->getFinalPrice() * $last_order->getVateRate() / 100;
    $transaction->setTax($iva);
    $transaction->setCity($order_billing->getLocation());
    /*$transaction->setRegion("Tuscany");
    $transaction->setCountry("Italy");
    
    $ordine_dati= MP\OrderElementQuery::create()->filterByOrderId($order_id)->find();
   
    $items = array();
    
    if (count($ordine_dati)) {
        foreach ($ordine_dati as $value) {
            // recupero prodotto
            $product_id = $value->getFormId();
            
            /*if ($product['giftcard'] == 0) {

                $brand_id = $product['brand_id'];
                $brand = $this->MBrand->getBrand($brand_id);
                $titolo_brand = $brand['titolo'];
            } else {
                $titolo_brand = "Giftcard";
            }
            //Tracciamento prodotto
            $item = new UnitedPrototype\GoogleAnalytics\Item();
            $item->setOrderId($order_id);
            $item->setSku($product_id);
            //$item->setCategory($titolo_brand);
            $product= MP\FormoneLangQuery::create()->filterByFormId($product_id)->filterByLanguageId($lang_name)->findOne();
            
            $item->setName($product->getName());
            $item->setQuantity($value->getNoofElements());
            $item->setPrice($value->getPrice()/$value->getNoofElements());
            $items[] = $item;
        }
    }

    //inserisce i prodotti nella transazione
    foreach ($items as $itm) {
        $transaction->addItem($itm);
    }
            
    //inivia i dati a Google
    $tracker->trackTransaction($transaction, $session, $visitor);
    */
?>

  <div class="col-sm-12 col-md-12">
    <?php if ($breadcrumb): echo '<a href="'.$base_url.'">'. $Home .'</a>'.' / '. $bordersucess; endif;?>  
  </div>
  <div class="space-3"></div>
  
  <?php
  
  $order = MP\OrdersQuery::create()->filterByOrderId($order_id)->findOne();
  
  $order_shipping = MP\OrderShippingDetailsQuery::create()->filterByOrderId($order_id)->findOne();
  $order_billing = MP\OrderBillingDetailsQuery::create()->filterByOrderId($order_id)->findOne();
  
  $order_elements = MP\OrderElementQuery::create()->filterByOrderId($order_id)->find();
  //$totalorderPrice = $order->getGrandtotalPrice();
  
  $order_discount = MP\OrderDiscountsQuery::create()->filterByAssignOrderId($order_id)->findOne();
  
  ?>
  <div id="divToPrint" class="clearfix boxed text-center">
      <div class="thankmessage">
        <h3><span class="brand-color"><?php echo $order_billing->getName(); ?></span>, <?php echo t('your order has been successful'); ?>.</h3>
        <h4><?php echo t('Thanks for buying from MePrint'); ?></h4>

        

      </div>
  </div>

  
    <?php if($order->getPayOption() == 'banktransfer'){ ?>
    <div class="boxed text-center">
      <?php echo '<h5>'.t('We will send your order as soon as the payment is received').'</h5>'; ?>
    </div>
  <?php } ?>



