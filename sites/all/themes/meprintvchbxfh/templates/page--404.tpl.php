
<?php
global $base_url;
global $base_path;
global $language;
global $user;
$lang_name = $language->language;

include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';
include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';

include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
  <article class="row">
    <div class="col-sm-12 col-md-12 text-center page-404 page-error">
<!--      <h1 class="title-super">4<span class="brand-color">0</span>4</h1>-->
      <img src="<?php echo base_path() . path_to_theme() ?>/images/404.jpg" title="404" alt="404">
      <p><?php echo t('Sorry, the requested page <br /> not be found on this website.'); ?></p>
      <a class="btn btn-orange" href="<?php echo url('<front>'); ?>"><?php echo t('Back to Homepage.'); ?></a>
    </div>
  </article>
</section>

<?php
include('footer.tpl.php');

