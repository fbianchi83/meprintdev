<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<div id="editorModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <iframe id="editorModalFrame" class="publisher-editor" src="/publishereditor" style="width: 300px; height: 200px" frameborder="0"></iframe>
            </div>
            <div class="modal-footer" style="height: 75px">
                <button id="editor_close" type="button" class="btn btn-white" data-dismiss="modal"><?php echo t('close'); ?></button>
                <button id="editor_save" type="button" class="btn btn-orange"><?php echo t('save'); ?></button>
            </div>
        </div>

    </div>
</div>