<?php
global $language;
?>
<!DOCTYPE html>
<html>
    <head>
        <?php print $head; ?>
        <title><?php print $head_title; ?></title>
        <!--?php print $styles; ?-->
        <!--  <link type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">-->
        <!--?php print $scripts; ?-->

        
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/bootstrap.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/jquery-ui.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/jquery-ui.structure.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/jquery.ui.rotatable.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/colorpicker/jquery.colorpicker.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/css/font-awesome.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/css/uploadfile.css' type='text/css' media='all' />
        
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/ruler.css' type='text/css' media='all' />
        
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/adlib/adlib.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/ballonextrabold/balloon.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/clarendon/clarendon.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/comixheavy/comixheavy.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/compacta/compacta.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/cooperblack/cooprblk.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/dax/dax.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/futura-bold/futura-bold.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/helvetica-bold/helvetica-bold.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/hobo/hobo.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/machine/machine.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/allerta_stencil/allertastencil.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/stardos_stencil/stardosstencil.css' type='text/css' media='all' />
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/sirin_stencil/sirinstencil.css' type='text/css' media='all' />
        <!--link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/couriernew/courier-new.css' type='text/css' media='all' /-->
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/fonts/windsor/windsor.css' type='text/css' media='all' />
        
        <link rel='stylesheet' href='/sites/all/themes/meprintvchbxfh/editor/css/publisher-editor.css' type='text/css' media='all' />
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/locale_<?php echo $language->language; ?>.js"></script>
        
        
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/downloading.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script type="text/javascript" src="/sites/all/modules/jquery_update/replace/jquery/1.11/jquery.min.js?v=1.11.2"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/jquery.ui.rotatable.min.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/scripts/jquery.form.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/scripts/jquery.uploadfile.min.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/underscore-min.js"></script>
        <!--script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/canvas-text-wrapper/canvas-text-wrapper.min.js"></script-->
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/colorpicker/jquery.colorpicker.js"></script>
        
        
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
        
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/jquery.ruler.js"></script>
        
        
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/colorpicker/i18n/jquery.ui.colorpicker-<?php echo $language->language; ?>.js"></script>
        
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/utils.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/entitytabproperties.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/texttabproperties.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/shapetabproperties.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/circletabproperties.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/picturetabproperties.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/layersselector.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/history.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/entity.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/shape.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/circle.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/picture.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/textbox.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/toolbarcontroller.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/page.js"></script>
        <script type="text/javascript" src="/sites/all/themes/meprintvchbxfh/editor/js/main.js"></script>

    </head>
    <body class="<?php print $classes; ?>" <?php print $attributes; ?>>
        
        <!--?php print $page_top; ?-->
        <?php print $page; ?>
        <!--?php print $page_bottom; ?-->
        <span id="measureText" style="display:inline; line-height: 1 !important"></span>
    </body>
</html>