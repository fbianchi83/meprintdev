
<ul class="topnav">
    <?php
    global $base_url;
    global $language;
    $lang_name = $language->language;
    if ($lang_name == 'en') {
      ?>
      <li class="selected"><a>Products</a></li>
      <?php
    } else if ($lang_name == 'it') {
      ?>
      <li class="selected"><a>Grande Formato</a></li>
      <?php
    } else if ($lang_name == 'fr') {
      ?>
      <li class="selected"><a>Produits</a></li>
      <?php
    }
    $result = MP\ProductsgroupLangQuery::create()
              ->filterByLanguageId($lang_name)
              ->filterByStatus(1)
              //->orderById('asc')
              ->orderBySorting("asc")
              ->find();
    foreach ($result as $precord) {
        
      $value = $precord->getName();
      $value_slug = $precord->getSlug();
      $pid = $precord->getProductgroupId();
      $count = MP\FormoneBigFormatQuery::create()
              ->filterByProductgroupId($pid)
              ->filterByStatus(1)
                ->count();
      
      if ($count != 0) {
          $isnew = MP\ProductsGroupQuery::create()->filterByProductGroupId($pid)->findOne()->getIsNew();
          if ($isnew) {
        ?>	
        <li><a href="javascript:void(0);"><?php echo $value; ?><label class="novita">NEW!</label></a> 
            <?php
          }
          else { ?>
          <li><a href="javascript:void(0);"><?php echo $value; ?></a>    
          <?php }
            $pgname_query = MP\ProductsubgroupLangQuery::create()
              ->filterByProductgroupId($precord->getProductgroupId())
              ->filterByStatus(1)
              ->filterByLanguageId($lang_name)
              ->orderByModifiedDate("DESC")
              ->find();
            if (count($pgname_query) > 0) {
              ?>
              <ul>
                  <?php
                  foreach ($pgname_query as $pgname) {
                    $subvalue = $pgname->getName();
                    $subid = $pgname->getProductsubgroupId();
                    $subsid = $pgname->getProductsubgroupId();
                    $sub_slug = $pgname->getSlug();
                    $subcount = MP\FormoneBigFormatQuery::create()
                      ->where('FormoneBigFormat.SubcategoryId=?', $subid)
                      ->filterByStatus(1)
                      ->count();
                    if ($subcount == 1) {
                        $subprod = MP\FormoneBigFormatQuery::create()
                            ->where('FormoneBigFormat.SubcategoryId=?', $subid)
                            ->filterByStatus(1)
                            ->findOne();
                        $slug = MP\FormoneLangQuery::create()
                            ->where('FormoneLang.FormId=?', $subprod->getFormId())
                            ->findOne();
                        $slugtitle = $slug->getSlug();
                        $subsid= $subprod->getFormId();
                    }
                    
                    if ($subcount != 0) {
                      if ($subvalue != '') {
                          
                          $isnew = MP\ProductsubgroupQuery::create()->filterByProductsubgroupId($subid)->findOne()->getIsNew();
                            if ($isnew) {
                        ?>
                        <li>
                            <?php if ($subcount == 1) { ?>
                            <a href="<?php echo $base_url . '/' . $lang_name . '/' . $value_slug . "/" . $sub_slug . "/" . $slugtitle . "/pb-" . $subsid; ?>"><?php echo $subvalue; ?> <label class="novita">NEW!</label> </a>
                            <?php } else { ?>
                            <a href="<?php echo $base_url . '/' . $lang_name . '/' . $value_slug . "/" . $sub_slug . "/l-" . $subsid; ?>"><?php echo $subvalue; ?> <label class="novita">NEW!</label> </a>
                            <?php }
                          } 
                          else { ?>
                          <li>
                            <?php if ($subcount == 1) { ?>
                            <a href="<?php echo $base_url . '/' . $lang_name . '/' . $value_slug . "/" . $sub_slug . "/" . $slugtitle . "/pb-" . $subsid; ?>"><?php echo $subvalue; ?> </a>
                            <?php } else { ?>
                            <a href="<?php echo $base_url . '/' . $lang_name . '/' . $value_slug . "/" . $sub_slug . "/l-" . $subsid; ?>"><?php echo $subvalue; ?> </a>
                            <?php }
                          }
                      }
                          ?>
                      </li>
                      <?php
                    }
                  } ?>
              </ul>
              <?php
                }
              ?>
            
        </li>
        <?php
      }
    }
    ?>
    <?php if ($lang_name == 'en') { ?> 
      <li class="selected"><a>Small Format</a></li> 
    <?php } else if ($lang_name == 'it') { ?>
      <li class="selected"><a>Piccolo Formato</a></li>
    <?php } else if ($lang_name == 'fr') { ?>
      <li class="selected"><a>Petit Format</a></li>
          <?php
        }
        
        /*$spgname_query = MP\FormtwoLangQuery::create()
          //->filterByProductgroupId(9)
          ->filterByLanguageId($lang_name)
          ->find();
          
        echo count($spgname_query);
        if (count($spgname_query) > 0) {
          ?>
          <?php
            
          foreach ($spgname_query as $spgname) {
            $samllvalue = $spgname->getName();
            //$samllid = $spgname->getProductsubgroupId();
            if ($samllvalue != '') {
              $scount = MP\FormtwoSmallFormatQuery::create()
                ->where('FormtwoSmallFormat.Status=1')
                //->where('FormtwoSmallFormat.SubcategoryId=?', $samllid)
                ->count();
              if ($scount > 0) {
                ?>
            <li>
                <a href="smallformat?<?php echo $samllid; ?>"><?php echo $samllvalue; ?> </a>
            </li>
            <?php
          }
        }
      }
    }*/
    
    $small_query = MP\FormtwoLangQuery::create()
          ->filterByLanguageId($lang_name)
          ->filterByStatus(1)
          ->orderByModifiedDate("DESC")
          ->find();
        if (count($small_query) > 0) {
            foreach ($small_query as $smallFormat) {
                $sub_slug = $smallFormat->getSlug();
                $subvalue = $smallFormat->getName();
                $subid = $smallFormat->getFormId();
                
?>
                <li>
                <a href="<?php echo $base_url . '/' . $lang_name; ?>/piccolo-formato/<?php echo $sub_slug .  "/ps-" . $subid; ?>"><?php echo $subvalue ?></a>
            </li>
            <?php 
            }
        }
    
    
    
    ?>
            <li class="selected"><a><?php echo t('Our offers'); ?></a></li>
            <li>
                <a href="<?php echo $base_url . '/' . $lang_name; ?>/offerte"><?php echo t('Offers'); ?></a>
            </li>
</ul>
<br />

