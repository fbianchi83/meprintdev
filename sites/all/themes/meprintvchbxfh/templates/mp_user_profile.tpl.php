<?php
//print_r($account);
include_once '/home/meprint/sites/all/libraries/mailchimp/src/Mailchimp.php';
    
if( (isset($_POST['uid']) && $_POST['uid'] != '') ){
    mp_user_update_profile();
}

$propel_user = MP\UserQuery::create()->filterByUid($account->uid)->findOne();

if($propel_user){
  $customer_type = $propel_user->getUserType();
}

//$nation_user = getNationToCode( $propel_user->getNation() );

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );

$apikey= "a21af34c4b8355346f26afa9330731e6-us2";
$listid= "d540f0cdb0";
$mailchimp = new Mailchimp($apikey);
$maillist = new Mailchimp_Lists( $mailchimp );
    
$subscriber = $maillist->memberInfo($listid, array(0 => array( "email" => $account->mail)));
//dd($subscriber['data'][0]['status']);
if ($subscriber['data'][0]['status'] == "subscribed")
    $subs= 1;
else
    $subs= 0;
?>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $('#form-registration__type').change(function() {
            $('#private').css('display', 'none');
            $('#public').css('display', 'none');
            $('#society').css('display', 'none');
            $('#association').css('display', 'none');
            $('#individual').css('display', 'none');
            $('#'+this.value).css('display', 'block');
        });
    });
</script>
<form id="form-profile" class="form-registration form-profile spacer" method="POST" >
    <input type="hidden" name="uid" value="<?php echo $account->uid; ?>" />
    <input type="hidden" name="user_type" value="<?php echo $customer_type; ?>" />
    <?php if ( $customer_type == 'unknown' ) { ?>
    <fieldset class="boxed">
      <legend><?php echo $ltype_customer; ?></legend>
        <div id="unknown" class="form-profile__optional" >
            <div class="row">
            <label for=""><?php echo $ltype_customer; ?></label>
            <select name="user_type" id="form-registration__type" name="form_type" class="form-control" required >
                <option value="">---</option>
                <option value="private"><?php echo t('Private'); ?></option>
                <option value="society"><?php echo t('Society'); ?></option>
                <option value="public"><?php echo t('Public'); ?></option>
                <option value="association"><?php echo t('Association'); ?></option>
                <option value="individual"><?php echo t('Individual'); ?></option>
            </select>
            </div>
        </div>
    </fieldset>
    <?php } ?>
    <fieldset class="boxed">
    <legend><?php echo t('Billing information and contact details'); ?></legend>
    
<?php ////////////// PRIVATO //////////////   ?>
    <?php if( $customer_type == 'private' || $customer_type == 'unknown') : ?>
    <div id="private" class="form-profile__optional" <?php if ($customer_type == 'unknown') echo "style='display:none;'"; ?>>
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lname; ?></label>
          <input type="text" class="form-control" name="private_name" id="form-profile__name" value="<?php echo $propel_user->getName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lsurname; ?></label>
          <input type="text" class="form-control" name="private_surname" id="form-profile__surname" value="<?php echo $propel_user->getSurname(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" name="private_fiscal_code" id="form-profile__cf" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="private_province" id="form-profile__prov" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="private_city" id="form-profile__location" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="private_address" id="form-profile__address" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="private_zip_code" id="form-profile__zip" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcontry; ?></label> 
          <select name="private_nation" id="form-profile__country" class="form-control">                 
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE PRIVATO //////////////   ?>

<?php ////////////// SOCIETA //////////////  ?>
    <?php if( $customer_type == 'society' || $customer_type == 'unknown') : ?>
    <div id="society" class="form-profile__optional" <?php if ($customer_type == 'unknown') echo "style='display:none;'"; ?>>
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcompany_name; ?></label>
          <input type="text" class="form-control" name="society_company_name" id="form-profile__business_name" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo t('type'); ?></label>
          <input type="text" class="form-control" name="society_company_type" id="form-profile__business_type" value="<?php echo $propel_user->getCompanyType(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lvat; ?></label>
          <input type="text" class="form-control" name="society_vat" id="form-profile__vat" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" class="form-control" name="society_fiscal_code" id="form-profile__cf_company" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="society_province" id="form-profile__type_company" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="society_city" id="form-profile__location_company" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="society_address" id="form-profile__address_company" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="society_zip_code" id="form-profile__zip_company" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcountry; ?></label>
          <select name="society_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE SOCIETA //////////////   ?>

<?php ////////////// ASSOCIAZIONE //////////////  ?>
    <?php if( $customer_type == 'association' || $customer_type == 'unknown') : ?>
    <div id="association" class="form-profile__optional" <?php if ($customer_type == 'unknown') echo "style='display:none;'"; ?>>
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcompany_name; ?></label>
          <input type="text" class="form-control" name="association_name" id="form-profile__bussiness_name_club" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" name="association_fiscal_code" id="form-profile__cf_club" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lvat; ?></label>
          <input type="text" class="form-control" name="association_vat" id="form-profile__vat_club" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="association_province" id="form-profile__prov_club" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="association_city" id="form-profile__location_club" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="association_address" id="form-profile__address_club" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="association_zip_code" id="form-profile__zip_club" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcountry; ?></label>
          <select name="association_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE ASSOCIAZIONE //////////////   ?>

<?php ////////////// INDIVIDUALE //////////////  ?>
    <?php if( $customer_type == 'individual' || $customer_type == 'unknown') : ?>
    <div id="individual" class="form-profile__optional" <?php if ($customer_type == 'unknown') echo "style='display:none;'"; ?>>
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcompany_name; ?></label>
          <input type="text" class="form-control" name="individual_company_name" id="form-profile__denomination_single" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <div class="intermediate_particle">
            di
          </div>  
          <label for=""><?php echo $lname; ?></label>
          <input type="text" class="form-control" name="individual_name" id="form-profile__name_single" value="<?php echo $propel_user->getName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lsurname; ?></label>
          <input type="text" class="form-control" name="individual_surname" id="form-profile__surname_single" value="<?php echo $propel_user->getSurname(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lvat; ?></label>
          <input type="text" class="form-control" name="individual_vat" id="form-profile__vat_single" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" name="individual_fiscal_code" id="form-profile__cf_single" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="individual_province" id="form-profile__prov_single" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="individual_city" id="form-profile__location_single" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="individual_address" id="form-profile__address_single" value="<?php $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="individual_zip_code" id="form-profile__zip_single" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcountry; ?></label>
          <select name="individual_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE INDIVIDUALE //////////////   ?>

<?php ////////////// ENTE PUBBLICO //////////////  ?>
    <?php if( $customer_type == 'public' || $customer_type == 'unknown') : ?>
    <div id="public" class="form-profile__optional" <?php if ($customer_type == 'unknown') echo "style='display:none;'"; ?>>
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo t('Name institution'); ?></label>
          <input type="text" class="form-control" name="public_company_name" id="form-profile__name_ente" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lvat; ?></label>
          <input type="text" class="form-control" name="public_vat" id="form-profile__vat_ente" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" name="public_fiscal_code" id="form-profile__cf_ente" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="public_province" id="form-profile__prov_ente" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="public_city" id="form-profile__location_ente" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="public_address" id="form-profile__address_ente" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="public_zip_code" id="form-profile__zip_ente" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcountry; ?></label>
          <select name="public_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE ENTE PUBBLICO //////////////   ?>

    <div class="row">
      <div class="form-group col-md-4 col-sm-6 col-xs-12 namerif">
        <label for=""><?php echo $lname_rif; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input type="text" class="form-control" name="reference_person_name" id="form-profile__user_rif_name" value="<?php echo $propel_user->getReferencePersonName(); ?>" />
        </div>
      </div>
      <div class="form-group col-md-4 col-sm-6 col-xs-12 surnamerif">
        <label for=""><?php echo $lsurname_rif; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input type="text" class="form-control" name="reference_person_surname" id="form-profile__user_rif_name" value="<?php echo $propel_user->getReferencePersonSurname(); ?>" />
        </div>
      </div>
      <div class="form-group col-md-4 col-sm-6 col-xs-12">
        <label for=""><?php echo $lphone; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-phone fa-lg"></i></span>
          <input type="tel" class="form-control" name="phone" id="form-profile__phone_rif" value="<?php echo $propel_user->getPhone(); ?>" />
        </div>
      </div>
      <div class="form-group col-md-4 col-sm-6 col-xs-12">
        <label for=""><?php echo $lmobile; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-mobile fa-lg"></i></span>
          <input type="tel" class="form-control" name="mobile_phone" id="form-profile__cell_rif" value="<?php echo $propel_user->getMobilePhone(); ?>" />
        </div>
      </div>
      <div class="form-group col-md-4 col-sm-6 col-xs-12">
        <label for=""><?php echo $lfax; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-fax"></i></span>
          <input type="tel" class="form-control" name="fax" id="form-profile__fax_rif" value="<?php echo $propel_user->getFax(); ?>" />
        </div>
      </div>
      <div class="form-group col-md-4 col-sm-6 col-xs-12">
        <label for=""><?php echo $lemail; ?></label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
          <input type="email" class="form-control" name="email" id="form-profile__email_rif" value="<?php echo (!empty($account->mail ))?$account->mail:""; ?>" />
        </div>
      </div>
      
    </div>
  </fieldset>

  <?php 
  
    //ESTRAGGO I DATI DI SPEDIZIONE
    $user_shipping = MP\UserShippingQuery::create()->filterByUid($account->uid)->findOne();
    if( $user_shipping ){
        $refcompany= $user_shipping->getCompanyName();
        $refpername= $user_shipping->getReferencePersonName();
        $refpersurname= $user_shipping->getReferencePersonSurname();
        $refcf= $user_shipping->getFiscalCode();
        $refvat= $user_shipping->getVat();
        $shipaddress= $user_shipping->getAddress();
        $shipphone= $user_shipping->getPhone();
        $shipzip= $user_shipping->getZipCode();
        $shipcity= $user_shipping->getCity();
        $shipprovince= $user_shipping->getProvince();
        $shipnation= $user_shipping->getNation();
    }
  ?>
  <fieldset class="boxed">
    <legend><?php echo $shipping; ?></legend>
    <div class="box-shipping">
      <div class="row">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lrecipient_name; ?></label>
          <input type="text" class="form-control" name="shipping_reference_person_name" id="form-profile__shipping_dest_name" value="<?php echo $refpername; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lrecipient_surname; ?></label>
          <input type="text" class="form-control" name="shipping_reference_person_surname" id="form-profile__shipping_dest_surname" value="<?php echo $refpersurname; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lrecipient; ?></label>
          <input type="text" class="form-control" name="shipping_company_name" id="form-profile__shipping_company" value="<?php echo $refcompany; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcf; ?></label>
          <input type="text" class="form-control" name="shipping_cf" id="form-profile__shipping_cf" value="<?php echo $refcf; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lvat; ?></label>
          <input type="text" class="form-control" name="shipping_vat" id="form-profile__shipping_vat" value="<?php echo $refvat; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $laddress; ?></label>
          <input type="text" class="form-control" name="shipping_address" id="form-profile__shipping_address" value="<?php echo $shipaddress; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lphone; ?></label>
          <input type="text" class="form-control" name="shipping_phone" id="form-profile__shipping_phone" value="<?php echo $shipphone; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lzipcode; ?></label>
          <input type="text" class="form-control" name="shipping_zip_code" id="form-profile__shipping_zip_code" value="<?php echo $shipzip; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $llocation; ?></label>
          <input type="text" class="form-control" name="shipping_city" id="form-profile__shipping_location" value="<?php echo $shipcity; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lprovince; ?></label>
          <input type="text" class="form-control" name="shipping_province" id="form-profile__shipping_prov" value="<?php echo $shipprovince; ?>" />
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
          <label for=""><?php echo $lcontry; ?></label>
          <select name="shipping_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $shipnation ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>    
  </fieldset>
  
  <fieldset class="boxed">
    <legend><?php echo t('Newsletter'); ?></legend>
    <div class="checkbox">
      <label class="form-profile__privacy">
          <input type="checkbox" <?php if($propel_user->getNewsletter() == 1 || $subs){ echo "checked='checked'"; } ?> name="newsletter" id="form-profile__newsletter" > <?php echo t('Subscribe to newsletter'); ?></label>
    </div>    
  </fieldset>
  <input type="submit" class="btn btn-red btn-update" value="<?php echo t('Update'); ?>" />
</form>






