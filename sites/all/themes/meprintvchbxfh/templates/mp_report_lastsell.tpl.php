<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
   $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    $("#dailystart").datepicker("setDate",currentDate);
    
    filtralast();
});

function filtralast() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    var clientid= jQuery('#clientsel').val();
    
    var morefilters= '&materials=0&paytype=nopay&shipping=0';
    
    jQuery.ajax({
        type:'POST',
        url:"/filterhistory",
        data:'datastart='+datestart+'&dataend='+dateend+'&client='+clientid+morefilters,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            var temp2 = '';
            if (result) {
                var i=0;
                var j=0;
                var totals=0;
                while (j<result.length) {
                    totals += parseFloat(result[j][3]) + (parseFloat(result[j][3]*result[j][7]/100));
                    j++;
                }  
                while (i<result.length) {
                    var total= parseFloat(result[i][3]) + (parseFloat(result[i][3]*result[i][7]/100)); 
                    temp += "<tr>";
                    temp += "<td>"+result[i][0]+"</td>";
                    temp += "<td><strong>"+result[i][1]+"</strong></td>";
                    temp += "<td><strong>"+result[i][2]+"</strong></td>";
                    temp += "<td>"+total.toFixed(2)+" &euro;</td>";
                    temp += "<td>"+result[i][4]+"</td>";
                    temp += "<td class='text-uppercase'><strong>"+result[i][5]+"</strong></td>";
                    temp += "<td class='text-right order-detail'><a href='/<?php echo $language->language; ?>/orders/orders_edit/"+result[i][0]+"'><?php echo $showoff; ?></a></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='7'> <?php echo $noorders; ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('N°'); ?></th><th><?php echo t('Order Date'); ?></th><th><?php echo t('Client'); ?></th><th><?php echo t('Amount'); ?></th><th><?php echo t('Products'); ?></th><th><?php echo $ptype; ?></th><th class='text-right'><?php echo t('Display'); ?></th></tr></thead>" + temp;
            temp2 = "<th width='35%' class='text-uppercase text-center'><?php echo t('Order Numbers'); ?></th><td width='15%' class='text-center'>"+j+"</td><th class='text-uppercase text-center' width='35%'><?php echo t('Total Amount'); ?></th><td class='text-center' width='15%'>"+totals.toFixed(2)+" &euro;</td>"
            jQuery('#mp-list-order').empty().append(temp);
            jQuery('#mp-list-order2').empty().append(temp2);
        }
    });
}
function filtralastnum() {
    var num= jQuery('#numberof').val();
    var clientid= jQuery('#clientsel').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterlastnum",
        data:'numof='+num+'&client='+clientid,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            var temp2 = '';
            if (result) {
                var i=0;
                var j=0;
                var totals=0;
                while (j<result.length) {
                    totals += parseFloat(result[j][3]) + (parseFloat(result[j][3]*result[j][7]/100));
                    j++;
                }  
                while (i<result.length) {
                    temp += "<tr>";
                    temp += "<td>"+result[i][0]+"</td>";
                    temp += "<td><strong>"+result[i][1]+"</strong></td>";
                    temp += "<td><strong>"+result[i][2]+"</strong></td>";
                    temp += "<td>"+result[i][3]+" &euro;</td>";
                    temp += "<td>"+result[i][4]+"</td>";
                    temp += "<td class='text-uppercase'><strong>"+result[i][5]+"</strong></td>";
                    temp += "<td class='text-right order-detail'><a href='/<?php echo $language->language; ?>/orders/orders_edit/"+result[i][0]+"'><?php echo $showoff; ?></a></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='7'> <?php echo $noorders; ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('N°'); ?></th><th><?php echo t('Order Date'); ?></th><th><?php echo t('Client'); ?></th><th><?php echo t('Amount'); ?></th><th><?php echo t('Products'); ?></th><th><?php echo $ptype; ?></th><th class='text-right'><?php echo t('Display'); ?></th></tr></thead>" + temp;
            temp2 = "<th width='35%' class='text-uppercase text-center'><?php echo t('Order Numbers'); ?></th><td width='15%' class='text-center'>"+j+"</td><th class='text-uppercase text-center' width='35%'><?php echo t('Total Amount'); ?></th><td class='text-center' width='15%'>"+totals.toFixed(2)+" &euro;</td>"
            jQuery('#mp-list-order').empty().append(temp);
            jQuery('#mp-list-order2').empty().append(temp2);
        }
    });
}
</script>

<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $lastsellstats; ?> </h3>
    <div id='dailychoice' class="billcentered"> 
        <h4> <?php echo $lastsellclient; ?> </h4>
        <?php echo t('Client'); ?> 
        <select id='clientsel' class="margin10" >
            <option value='0'> <?php echo t('All Clients'); ?> </option>
            <?php 
            $users = MP\UserQuery::create()->find();
            foreach ($users as $user) {
                if ($user->getUid() != 1) {
                    ?>
            <option value="<?php echo $user->getUid(); ?>"> <?php echo $user->getName() . " " . $user->getSurname(); ?> </option>
                    <?php
                }
            }
            ?>
        </select>
    </div>
    <div class='dailychoice2' class="billcentered"> 
        <h4> <?php echo $lastselldates; ?> </h4>
        <input type='text' class="margin10" id='dailystart'>
        <input type='hidden' class="margin10" id='dailyend' readonly value="<?php echo date('d/m/Y'); ?>">
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtralast(); return false;'>
    </div>
    <div class='dailychoice2' class="billcentered"> 
        <h4> <?php echo $lastsellqty; ?> </h4>
        <select class="margin10" id='numberof'>
            <option value="0">---</option>
            <?php for ($i=1; $i<=30; $i++) { ?>
            <option value="<?php echo $i; ?>"> <?php echo $lastlast." ".$i." ".$sellslast; ?></option> 
            <?php } ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtralastnum(); return false;'>
    </div>
    
    <table id="mp-list-order2" class="table table-bordered table-striped table-hover"></table>
    <table id="mp-list-order" class="table table-bordered table-striped table-hover"></table>
</div>