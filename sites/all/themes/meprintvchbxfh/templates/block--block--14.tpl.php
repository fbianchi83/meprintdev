<?php
$fire = t('events and fairs');
$events = t('business promotion');
$furnish = t('window dressing');
$enterprises = t('signs and plaques');
$newyear = t('gift ideas');
$posters = t('ideas for home');
$everythingfor = t('everything for');

global $base_url;
global $language;
$lang_name = $language->language;
?>
        <a href="<?php echo $base_url."/".$lang_name; ?>/eventi-fiere">
          <div class="media ico-banner">
            <div class="media-left ico-banner--events">
              <i class="fa fa-flag ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--events fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $fire; ?></p>
            </div>
          </div>
        </a>

        <a href="<?php echo $base_url."/".$lang_name; ?>/promozione-aziendale">
          <div class="media ico-banner">
            <div class="media-left ico-banner--promotions">
              <i class="fa fa-group ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--promotions fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $events; ?></p>
            </div>
          </div>
        </a>

        <a href="<?php echo $base_url."/".$lang_name; ?>/allestimento-vetrine">
          <div class="media ico-banner">
            <div class="media-left ico-banner--showcase">
              <i class="fa fa-star ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--showcase fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $furnish; ?></p>
            </div>
          </div>
        </a>

        <a href="<?php echo $base_url."/".$lang_name; ?>/insegne-targhe">
          <div class="media ico-banner">
            <div class="media-left ico-banner--writings">
              <i class="fa fa-bullseye ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--writings fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $enterprises; ?></p>
            </div>
          </div>
        </a>

        <a href="<?php echo $base_url."/".$lang_name; ?>/idee-regalo">
          <div class="media ico-banner">
            <div class="media-left ico-banner--gift">
              <i class="fa fa-gift ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--gift fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $newyear; ?></p>
            </div>
          </div>
        </a>

        <a href="<?php echo $base_url."/".$lang_name; ?>/idee-casa">
          <div class="media ico-banner">
            <div class="media-left ico-banner--home">
              <i class="fa fa-lightbulb-o ico-banner__icon fa-lg"></i>
            </div>
            <div class="media-body">
              <i class="fa fa-chevron-circle-right ico-banner__arrow ico-banner__arrow--home fa-lg pull-right"></i>
              <p class="ico-banner__text"><span><?php echo $everythingfor; ?></span><br /><?php echo $posters; ?></p>
            </div>
          </div>
        </a>
