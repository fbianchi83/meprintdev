<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php global $base_path;
global $language;
$lang_name = $language->language;
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
global $base_url;?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<!--<aside class="col-md-3 col-sm-3">
  <nav class="main-menu">
     <?php print render($page['left_sidebar']);?>
  </nav>
  <div class="space-2"></div>
  
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add1.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
</aside>-->
<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): print $breadcrumb; endif;?>  
  
	<h3>&nbsp;Order Confirm <span><img src="/sites/all/themes/meprint/images/wave.png" alt="image"></span> </h3>
   <div class="space-3"></div>
   
            <?php 
           global $base_url;  
	  print orderconfirm();?>
          
           <form id = "paypal_checkout" action = "https://www.paypal.com/cgi-bin/webscr" method = "post">
    <input name = "cmd" value = "_cart" type = "hidden">
    <input name = "upload" value = "1" type = "hidden">
    <input name = "no_note" value = "0" type = "hidden">
    <input name = "bn" value = "PP-BuyNowBF" type = "hidden">
    <input name = "tax" value = "0" type = "hidden">
    <input name = "rm" value = "2" type = "hidden"> 
    <input name = "business" value = "jeremy@jdmweb.com" type = "hidden">
    <input name = "handling_cart" value = "0" type = "hidden">
    <input name = "currency_code" value = "EUR" type = "hidden">
    <input name = "lc" value = "GB" type = "hidden">
    <input name = "return" value = "http://mysite/myreturnpage" type = "hidden">
    <input name = "cbt" value = "Return to My Site" type = "hidden">
    <input name = "cancel_return" value = "http://mysite/mycancelpage" type = "hidden">
    <input name = "custom" value = "" type = "hidden">
 
            <?php 

         global $base_url;  
         global $user;
         session_start();
          $sessionid = session_id();
	  global $language ;
          $lang_name = $language->language ;
          $query = MP\CartItemsQuery::create()
                                 ->where('CartItems.SessionId =?', $sessionid);
          $i=1;
            foreach($query as $paypalvaribles){
               echo $countnum = $i++;
                //echo "<pre>";
                //print_r($paypalvaribles);
                $quanitity = $paypalvaribles->getQuanitity();
                $price = $paypalvaribles->getPrice();
                 $ship = $paypalvaribles->getShipping();
                 $payquery = MP\FormoneLangQuery::create()->filterByFormId($paypalvaribles->getProductId())->filterByLanguageId($lang_name)->find();
                 foreach($payquery as $payresult){ 
                   $name  = $payresult->getName();
                 }?>
    
    <div id = "item_<?php echo $countnum; ?>" class = "itemwrap">
        <input name = "item_name_<?php echo $countnum; ?>" value = "<?php echo $name; ?>" type = "hidden">        
        <input name = "amount_<?php echo $countnum; ?>" value = "<?php echo $price; ?>" type = "hidden">       
    </div>
                
           <?php }?>
   
   
    
 
 <!--<input id = "ppcheckoutbtn" value = "Checkout" class = "button" type = "submit">-->
</form>
           
	
 

  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>