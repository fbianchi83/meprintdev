<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php 
global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

drupal_add_js(base_path() . path_to_theme() . '/scripts/smallformat-validations.js');
drupal_add_js('/sites/all/themes/meprintvchbxfh/scripts/page-small-format.js');
drupal_add_js('/sites/all/themes/meprintvchbxfh/scripts/jquery-scrolltofixed-min.js');
drupal_add_js('/sites/all/themes/meprintvchbxfh/scripts/price_small.js');



?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
  <aside class="col-md-3 col-sm-3 col-xs-12">
    <a class="menu-mobile btn btn-success visible-xs">Menu</a>
    <?php print render($page['left_sidebar']);?>
    <div class="space-2 mm"></div>

    <?php print render($page['left_sidebar_prod']); ?>
    <div class="space-2 mm"></div>
  </aside>
  <div class="col-md-7 col-sm-7 col-xs-12">
    <header class="row">
      <?php print render($page['content']);?>
    </header>
  </div>
  <aside class="col-md-2 col-sm-2 col-xs-12">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="panel panel-default summary-cart">
          <div class="panel-heading"><?php echo $cartpagetittle; ?></div>
          <div class="">
            
              <?php
            if ($cart) {

              $cart_items_sum = MP\CartItemQuery::create()
                ->filterByCartId($cart->getCartId())
                ->withColumn('SUM(total_price)', 'Sum')
                ->findOne();

              $cart_items = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->limit(3)->find();
              ?>
                <table class="table table-condensed">
                  <tr>
                    <th class="grid_list"><?php echo $pname; ?></th>
                    <th class="grid_list"><?php echo $price; ?></th>
          <!--                    <th class="col-md-2 col-sm-2 grid_list">--><?php //echo $qty;  ?><!--</th>-->
                  </tr>
                  <?php
                  foreach ($cart_items as $item) {
                    /* @var $item MP\CartItem */
                    $dateship= $item->getShippingDate('d-m-Y');
                    if ($item->getProductType() == "big")
                        $pquery = MP\FormoneLangQuery::create()->filterByFormId($item->getProductId())->filterByLanguageId($lang_name)->findOne();
                    else
                        $pquery = MP\FormtwoLangQuery::create()->filterByFormId($item->getProductId())->filterByLanguageId($lang_name)->findOne();
                    $m = substr($pquery->getName(), 0, 15);
                    ?>
                    <tr>
                      <td class="">
                        <strong><?php echo $m; ?></strong><br />
                        <strong><?php echo t('Qty'); ?>. </strong> <?php echo $item->getQuanitity(); ?>
                      </td>
                      <td class="">
                        <strong><?php echo '&euro;' . number_format((float) $item->getTotalPrice(), 2, '.', '');
                ?></strong>
                      </td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td class=""><strong><?php echo $Shippingdate; ?></strong><br /></td>
                    <td class=""><?php echo $dateship; ?></td>
                  </tr>  
                  <tr>
                    <td class="text-center" colspan="2">
                      <p class="summary-cart__total"><?php echo $totalcost . ' &euro; ' . number_format((float) $cart_items_sum->getSum(), 2, '.', ''); ?></p>
                      <button class="btn btn-success" onclick="location.href='/carrello';"><?php echo $paynow; ?></button>
                    </td>

                  </tr>
                </table>
              <?php
                  }else{
                    echo '<h5 style="padding:5px;">'.$noproductsavail.'</h5>';
                  } ?>
              </div>

              
            <div class="space-2"></div>
            <!--            <p class="text-center">
              <button type="button" class="btn btn-success">Success</button>
            </p>-->
        </div>
      </div>
    </div>
    <div class="space-2"></div>

    
    
    
      <!--    carrello che segue-->
      <div class="followcart">
        <div id="summary-cart--product" class="panel panel-default summary-cart summary-cart--product">
        <div class="panel-heading"><?php echo $summary; ?></div>
        <div class="">
          <table id="sample-table-2"  class="table table-condensed">
            <tbody>
              
              <tr>
                <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="productcostdiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="extraaccessoriestd">
                <td class="left"><strong><?php echo $accessorie; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="extraacccstdiv" id="extraacccstdiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
<!--                <tr>
                <td class="left"><strong><?php //echo $singleproductcost;  ?></strong></td>
                <td>
                  <div class="fa fa-eur singleproductcostdiv"></div>
                </td>
              </tr>-->
              <tr class="extraprocessingtd">
                <td class="left"><strong><?php echo $extraprocessingcost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="extraprocessingdiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr>
                <td class="left"><strong><?php echo $quantity; ?></strong></td>
                <td class="text-right">
                  <div class="totalnoofproductbigfrmtdiv">0</div>
                </td>
              </tr>
              <tr class="promopricetd">
                <td class="left"><strong><?php echo $beforpromoprice; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span style="text-decoration:line-through" class="promopricediv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="promovaltd">
                <td class="left"><strong><?php echo $promoval; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="promovaldiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="promoshiptd">
                <td class="left"><strong><?php echo $promoship; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="promoshipdiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="operatorreviewtd">
                <td class="left"><strong><?php echo $operatorreview_cost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="operatorreviewdiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="shippingdatetd">
                <td class="left"><strong><?php echo $Shippingdate; ?></strong></td>
                <td class="hidden-480">
                  <div class="shippingdatediv">0</div>
                </td>
              </tr>
              <tr>
                <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="totalproductcostdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="ivatd">
                <td class="left"><strong><?php echo $iva_cost . ' (' . $VAT_RATE . '%)'; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="ivadiv">0</span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>              
              <tr>
                <td colspan="2" class="text-center">
                  <div class="summary-cart__total">
<?php echo $totalcost; ?> &nbsp; <span class="totalcostdiv"></span> <i class="fa fa-eur"></i>
                  </div>
                </td>
              </tr>

              <tr>
                <td colspan="2" class="text-center">
                  <div class="btn-add-cart">
                    <button class="btn btn-success" type="submit" name="proceed" value="Proceed" id="smallproceed2"><?php echo $addcart; ?></button>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="space-2"></div>
      </div>
      <!--fine carrello che segue-->
      <?php
          
      $id = arg($id);
      $this_prod = MP\FormtwoSmallFormatQuery::create()->filterByFormId($id)->findOne();

      $listing_query = MP\FormtwoSmallFormatQuery::create()
                      ->filterByStatus(1)
                      ->where("form_id != $id")
                      ->find();
      if(count($listing_query ) >0 ){
  
    ?>
    
    <div id="relatedpanel" class="panel panel-default panel--mini">
      <h3 class="panel-heading"><?php echo $samecategory; ?></h3>
      <div class="panel-body">
        <div class="carousel-related">
          <?php
                    
            foreach($listing_query as $listing){
              $id_same = $listing->getFormId();
              if($id_same == $id){
                continue;
              }
              $check_lang = MP\FormtwoLangQuery::create()
                                ->where('FormtwoLang.LanguageId =?', $lang_name)
                                ->where('FormtwoLang.FormId =?', $id_same)
                                ->findOne();
              
              $name = substr($check_lang->getName(), 0, 25);
              $name_slug = $check_lang->getSlug();

              $add_info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id_same)->findOne();

              $img = file_load($add_info->getImage());
              $img_path_small = image_style_url("same-category-small", $img->uri);
              $img_path = image_style_url("same-category", $img->uri);

              $url_small = $base_url . '/' . $lang_name .'/piccolo-formato/'. $name_slug .  "/ps-" . $id_same;


                ?>
                  <div class="media media--mini-listing">
                    <div class="media-left hidden-sm">
                      <a href="<?php echo $url_small; ?>">
                        <img class="media-object" src="<?php echo $img_path_small; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo $img_path; ?>" alt="<?php echo $name; ?>" /><p></p></div>'>
                      </a>
                    </div>
                    <div class="media-body">
                      <h4 class="media-mini-listing__title"><?php echo $name; ?></h4>
                    </div>
                  </div>
                <?php
            }
         
          ?>
        </div>
      </div>
    </div>

    <div class="space-2"></div>
    <div class="space-2"></div>
    
    <?php } ?>
      </div>
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>