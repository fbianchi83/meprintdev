<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
    $('input[type=radio][name=interval]').change(function() {
        var value = $( 'input[name=interval]:checked' ).val();
        $('#dailychoice').addClass('hidden');
        $('#weeklychoice').addClass('hidden');
        $('#monthlychoice').addClass('hidden');
        $('#yearlychoice').addClass('hidden');
        $('#yearlycompchoice').addClass('hidden');
        $('#'+value).removeClass('hidden');
        switch (value) {
            case 'dailychoice': $('#period').val("day"); break;
            case 'weeklychoice': $('#period').val("week"); break;
            case 'monthlychoice': $('#period').val("month"); break;
            case 'yearlychoice': $('#period').val("year"); break;
            case 'yearlycompchoice': $('#period').val("yearcomp"); break;
        }
    });
    
    $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dailyend" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#dailystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    $( "#weeklystart" ).datepicker({
      showAnim: "slideDown",
      showWeek: true,
      dateFormat: "dd/mm/yy",
      weekHeader: "W",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#weeklyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#weeklyend" ).datepicker({
      showAnim: "slideDown",
      showWeek: true,
      dateFormat: "dd/mm/yy",
      weekHeader: "W",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#weeklystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    var prevDate = new Date();
    $("#dailystart").datepicker("setDate",currentDate);
    $("#dailyend").datepicker("setDate",prevDate);
    $("#weeklystart").datepicker("setDate",currentDate);
    $("#weeklyend").datepicker("setDate",prevDate);
    filtradaily();
});

    
function filtradaily() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    
    var period= jQuery('#period').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadcart",
        data:'datastart='+datestart+'&dataend='+dateend+'&period='+period,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $daterep; ?>');
                data.addColumn('number', '<?php echo $daydeadcart; ?>');
                i=0;
                while (i < result.length) {
                    orders= result[i]['orders'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,dates[2]), orders]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $daydeadcart; ?>',
                        subtitle: datastart[2]+'/'+datastart[1]+'/'+datastart[0]+' - '+dataend[2]+'/'+dataend[1]+'/'+dataend[0]
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtraweekly() {
    var datestart= formatdate(jQuery('#weeklystart').val());
    var dateend= formatdate(jQuery('#weeklyend').val());
    
    var period= jQuery('#period').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadcart",
        data:'datastart='+datestart+'&dataend='+dateend+'&period='+period,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $weekrep; ?>');
                data.addColumn('number', '<?php echo $weekrevenue; ?>');
                i=0;
                while (i < result.length) {
                    orders= result[i]['orders'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,dates[2]), orders]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $weekrevenue; ?>',
                        subtitle: datastart[2]+'/'+datastart[1]+'/'+datastart[0]+' - '+dataend[2]+'/'+dataend[1]+'/'+dataend[0]
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtramonthly() {
    var datestart= jQuery('#monthlystart').val();
    var dateend= jQuery('#monthlyend').val();
    var monthstart= datestart.substr(4);
    var yearstart= datestart.substr(0,4);
    var monthend= dateend.substr(4);
    var yearend= dateend.substr(0,4);
    
    var period= jQuery('#period').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadcart",
        data:'monthstart='+monthstart+'&yearstart='+yearstart+'&monthend='+monthend+'&yearend='+yearend+'&period='+period,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $monthdeadcart; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['orders'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1, ''), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var datastart= result[0]['data'].split('-');
                var dataend= result[result.length-1]['data'].split('-');
                var options = {
                    chart: {
                        title: '<?php echo $monthdeadcart; ?>',
                        subtitle: datastart[1]+'/'+datastart[0]+' - '+dataend[1]+'/'+dataend[0]
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtrayearly() {
    var year= jQuery('#year').val();
    
    var period= jQuery('#period').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadcart",
        data:'year='+year+'&period='+period,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $yeardeadcart; ?>');
                i=0;
                while (i < result.length) {
                    revenue= result[i]['orders'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date(dates[0],dates[1]-1,''), revenue]);
                    i++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var options = {
                    chart: {
                        title: '<?php echo $yeardeadcart; ?>',
                        subtitle: year
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function filtrayearcomp() {
    var year1= jQuery('#yearstart').val();
    var year2= jQuery('#yearend').val();
    
    var period= jQuery('#period').val();
    
    jQuery.ajax({
        type:'POST',
        url:"/filterdeadcart",
        data:'yearstart='+year1+'&yearend='+year2+'&period='+period,
        dataType:'json',
        success: function(result) {  
            if (result) {
                var data = new google.visualization.DataTable()
                
                data.addColumn('date', '<?php echo $monthrep; ?>');
                data.addColumn('number', '<?php echo $yearrevenue; ?> '+year1);
                data.addColumn('number', '<?php echo $yearrevenue; ?> '+year2);
                i=0; j=12;
                while (j < result.length) {
                    revenue= result[i]['orders'];
                    revenue2= result[j]['orders'];
                    var dates= result[i]['data'].split('-');
                    data.addRow([new Date('',dates[1]-1,''), revenue, revenue2]);
                    i++; 
                    j++;
                }
                var chart = new google.charts.Line(document.getElementById('curve_chart'));
                
                var options = {
                    chart: {
                        title: '<?php echo $yearrevenuecomp; ?>',
                        subtitle: year1 + '-' + year2 + ' in €'
                    },
                    width: 800,
                    height: 500
                };
                chart.draw(data, options);
            }
        }
    });
}

function showfilters() {
    if (jQuery('#filters2').css('display') === 'block') 
        jQuery('#filters2').fadeOut("fast");
    else 
        jQuery('#filters2').fadeIn("fast");
}

</script>
<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $deadcartstats; ?> </h3>
    <div class="billcentered"> 
        <input checked type='radio' class="margin10" id='daily' name='interval' value="dailychoice"> <?php echo $daily; ?>
        <input type='radio' class="margin10" id='weekly' name='interval' value="weeklychoice"> <?php echo $weekly; ?> 
        <input type='radio' class="margin10" id='monthly' name='interval' value="monthlychoice"> <?php echo $monthly; ?>
        <input type='radio' class="margin10" id='yearly' name='interval' value="yearlychoice"> <?php echo $yearly; ?> 
        <input type='radio' class="margin10" id='yearlyc' name='interval' value="yearlycompchoice"> <?php echo $yearlyc; ?> 
    </div>
        
    <input type="hidden" name="period" id="period" value="day" />
    <div id='dailychoice' class="billcentered"> 
        <?php echo $datestart; ?> <input type='text' class="margin10" id='dailystart'>
        <?php echo $dateend; ?> <input type='text' class="margin10" id='dailyend'> 
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtradaily(); return false;'>
    </div>
    
    <div id='weeklychoice' class="billcentered hidden"> 
        <?php echo $weekstart; ?> <input type='text' class="margin10" id='weeklystart'>
        <?php echo $weekend; ?> <input type='text' class="margin10" id='weeklyend'>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtraweekly(); return false;'>
    </div>
    
    <div id='monthlychoice' class="billcentered hidden"> 
        <?php echo $monthstart; ?> <select id='monthlystart' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $month = date('m', strtotime($startdate));
            $year = date('Y', strtotime($startdate));
            $ym = $year.$month;
            ?> <option value='<?php echo $ym; ?>'> <?php echo $month; ?>-<?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 month", strtotime($startdate)));
        }
        ?>
        </select>
        <?php echo $monthend; ?> <select id='monthlyend' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $month = date('m', strtotime($startdate));
            $year = date('Y', strtotime($startdate));
            $ym = $year.$month;
            ?> <option value='<?php echo $ym; ?>'> <?php echo $month; ?>-<?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 month", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtramonthly(); return false;'>
    </div>
    <div id='yearlychoice' class="billcentered hidden">
        <?php echo $yearrep; ?> <select id='year' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtrayearly(); return false;'>
    </div>
    <div id='yearlycompchoice' class="billcentered hidden">
        <?php echo $yearrepstart; ?> <select id='yearstart' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <?php echo $yearrepend; ?> <select id='yearend' class="selectrep">
        <?php $startdate= date('Y-m-d', strtotime('2015-08-01'));
    
        while (date('Y-m-d',strtotime($startdate)) <  date('Y-m-d')) {
            $year = date('Y', strtotime($startdate));
            ?> <option value='<?php echo $year; ?>'> <?php echo $year; ?> </option>
            <?php
            $startdate = date('d-m-Y', strtotime("+1 year", strtotime($startdate)));
        }
        ?>
        </select>
        <input type='button' value='<?php echo $filterdate; ?>' onclick='filtrayearcomp(); return false;'>
    </div>
    
    <script type="text/javascript" src="https://www.google.com/jsapi?autoload={ 'modules':[{ 'name':'visualization', 'version':'1.1', 'packages':['line'] }] }"></script>
    
    <div id="curve_chart" style="width: 900px; height: 500px"></div>
</div>