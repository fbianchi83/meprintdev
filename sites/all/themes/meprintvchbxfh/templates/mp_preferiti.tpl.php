<script type="text/javascript">
jQuery(document).ready(function($) {  
    //elimino dai preferiti
  $('.del-prefer').click(function() {
      var form_id = $(this).data("form-id");
      var form_type = $(this).data("form-type");
      
      $.ajax({
        url: "/del-wishlist",
        data: {
            form_id: form_id,
            form_type: form_type
        },
        dataType: "html",
        type: "POST",
        success: function( response ) {            
            showModal("alertModal", Drupal.t("remove prefer"), Drupal.t("product removed from prefer"));
            window.location.replace("/preferiti");
        }
      });
      
  });
 
              
  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function showModal(id,title,content){
    $('#'+id+' .modal-title').html(title);
    $('#'+id+' .modal-body').html( capitalizeFirstLetter(content) );
    $('#'+id).modal('show');
  }
  
  
  
  
});

</script>

<?php

$sno = t('Num.');
$Productname = t('Product name');
$Productdesc = t('Product description');
$delete= t('Delete');
global $base_url;
global $user; 
global $language;
$lang_name = $language->language;

//$user->uid;
$result = MP\WishlistQuery::create()->filterByUserId($user->uid)->orderByFormId('desc')->find();
$count= MP\WishlistQuery::create()->filterByUserId($user->uid)->count();
if($count > 0){ 

?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <table class="row">
      <tr>
          <th class="col-xs-1 col-sm-1 col-md-1 grid_list text-center"><?php echo $sno; ?></th>
          <th class="col-xs-10 col-sm-5 col-md-5 grid_list "><?php echo $Productname; ?></th>
          <th class="hidden-xs col-sm-5 col-md-5 grid_list "><?php echo $Productdesc; ?></th>
          <th class="col-xs-1 col-sm-1 col-md-1 grid_list text-center"><?php echo $delete; ?></th>
      </tr> 
      <?php 
        $i=0;
        foreach ($result as $wishlist){
          $i++;  
          $id = $wishlist->getFormId();
          
          $page = isset($_GET['page'])?$_GET['page']:0;
          $limit = 10;
          if($wishlist->getFormType() == 'big'){
            
            $productname = MP\FormoneLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->findOne();
            $formone = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->findOne(); 
            
            $tmp_subslug = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($formone->getSubcategoryId())->filterByLanguageId($lang_name)->findOne();              
            $tmp_proslug = MP\ProductsgroupLangQuery::create()->filterByProductGroupId($formone->getProductgroupId())->filterByLanguageId($lang_name)->findOne();
              
            $prod_slug = $productname->getSlug();
            $subslug = $tmp_subslug->getSlug();
            $pro_slug = $tmp_proslug->getSlug();
                        
            $url = $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $wishlist->getFormId();
            
            $name =  $productname->getName();
            $description = TagliaStringa($productname->getDescription(), 50, true);
            
          }else if($wishlist->getFormType() == 'small'){
            $productname = MP\FormtwoLangQuery::create()->filterByFormId($wishlist->getFormId())->filterByLanguageId($lang_name)->findOne();
            $name_slug = $productname->getSlug();
            
            $url = $base_url.'/'.$lang_name.'/piccolo-formato/'. $name_slug .  "/ps-" . $wishlist->getFormId();
            $name = $productname->getName();
            $description = TagliaStringa($productname->getDescription(), 50, true);
          }


      ?>
          <tr class="deleteTable_<?php echo $id; ?>">
              <td class="col-xs-1 col-sm-1 col-md-1 grid_list text-center"><?php echo $i; ?></td>
              <td class="col-xs-10 col-sm-5 col-md-5 grid_list"> <a href="<?php echo $url; ?>" ><?php echo $name; ?></a></td>
              <td class="hidden-xs col-sm-5 col-md-5 grid_list"><?php echo $description; ?></td>
              <td class="col-xs-1 col-sm-1 col-md-1 grid_list text-center">
                  <a class="del-prefer" data-form-type="<?php echo $wishlist->getFormType(); ?>" data-form-id="<?php echo $id; ?>" style="cursor: pointer">
                    <i class="fa fa-trash-o fa-2x"></i>
                  </a>
              </td>
          </tr>
<?php } ?>
  </table>
</div>
<?php  
}else{
  ?>
    <p><?php echo t('At the moment there are no products to your favorite'); ?></p>
<?php
}
?>
