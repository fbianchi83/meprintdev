
<div class="cartdetailspage">
    <!-- nome prodotto --> 
    <div class="col-md-8 col-sm-8 col-xs-7"> 
        <div class="cartdetails_4">
            <strong class="cartdetails--open" data-toggle="collapse" href="#cart_<?php echo $order_item_id; ?>" aria-expanded="false">
                <i class="fa fa-1x fa-plus collapsed" ></i>
                <span><?php echo $presult->getName(); ?></span>
            </strong>
        </div>    
    </div>
    <!-- quantita -->
    <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
        <div class="cartdetails text-center">
            <span><?php echo $order_item->getNoofElements(); ?></span>
        </div>
    </div>
    <!-- prezzo prodotto -->
    <div class="col-md-2 col-sm-2 col-xs-3 cartdetailsline">
        <div class="cartdetails cartprice text-right">
            <?php
            $totlprc = $order_item->getPrice();
            $prc = number_format((float) $totlprc, 2, '.', '') . ' &euro;';
            ?>

            <span><?php echo $prc; ?></span>
        </div>
    </div>

    <!-- dettaglio collapse con files già presenti -->
    <div class="col-sm-12 col-xs-12 detail-collapsed">
        <div style="height: 0px;" aria-expanded="false" class="collapse" id="cart_<?php echo $order_item_id; ?>">

            <!-- NUMERO DI COPIE -->
            <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                <span class="labelcart"><?php echo $nocopies; ?></span>
                <span class="datacart"><?php echo $order_item->getNoofElements(); ?></span>
            </div>


            <?php
            //TIPOLOGIA PIEGA
            $order_item_folding = MP\FoldingTypeTwoQuery::create()->filterByFoldingTypeId($order_item->getFoldingTypeId())->findOne();
            if (count($order_item_folding) > 0) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo $lfolding; ?></span>
                    <span class="datacart">
                        <?php echo $order_item_folding->getName(); ?>
                    </span>
                </div>
            <?php } ?>



            <!-- DIMENSIONI -->      
            <?php
            if ($order_item->getDimensionId() != "custom") {
                $dquery = MP\DimensionsTwoQuery::create()->filterByDimensionId($order_item->getDimensionId())->findOne();
                $size = $dquery->getWidth() . '*' . $dquery->getHeight();
                
                $productHeight = $dquery->getHeight();
                $productWidth = $dquery->getWidth();
                
            } else {
                $size = $order_item->getWidth() . "*" . $order_item->getHeight();
                
                $productHeight = $order_item->getHeight();
                $productWidth = $order_item->getWidth();
                
            }
            ?>
            <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                <span class="labelcart"><?php echo $cdimension; ?></span>
                <span class="datacart"><?php echo $size; ?></span>
                
                <input id="productHeight_<?php echo $order_item_id; ?>" type="hidden" value="<?php echo $productHeight ?>">
                <input id="productWidth_<?php echo $order_item_id; ?>" type="hidden" value="<?php echo $productWidth ?>">
                
            </div>


            <!-- ORIENTAMENTO -->              
            <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                <span class="labelcart"><?php echo $lorientation; ?></span>
                <span class="datacart">
                    <?php echo ($order_item->getOrientation() == 0) ? $orientation_h : $orientation_v; ?>
                </span>
            </div>


            <!-- MATERIALE -->
            <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                <span class="labelcart"><?php echo $cmaterial; ?></span>
                <span class="datacart">
                    <?php
                    $mquery = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($order_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
                    echo $mquery->getName();
                    ?>
                </span>
            </div>


            <?php
            // GRAMMATURA
            $order_item_weight = MP\WeightsTwoQuery::create()->filterByWeightId($order_item->getWeightId())->findOne();
            if (count($order_item_weight) > 0) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo $lgrammatura; ?></span>
                    <span class="datacart">
                        <?php echo $order_item_weight->getWeight() . ' ' . $measure_weight; ?>
                    </span>
                </div>
            <?php } ?>


            <?php
            // FACCIATE              
            if ($order_item->getNfaces() > 0) {
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo ucfirst($lnumface); ?></span>
                    <span class="datacart">
                        <?php echo $order_item->getNfaces(); ?>
                    </span>
                </div>
            <?php } ?>


            <?php
            // TIPO COPERTINA              
            if ($order_item->getCover() > 0) {
                $add_cover_str = "";
                if ($order_item->getCover() == 2) {
                    //COPERTINA SPECIFICA: materiali e grammatura
                    $mcover = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($order_item->getIdMaterialCover())->filterByLanguageId($lang_name)->findOne();
                    $wcover = MP\WeightsTwoQuery::create()->filterByWeightId($order_item->getIdWeightCover())->findOne();

                    $add_cover_str = ", <em>" . $cmaterial . ": " . $mcover->getName() . "</em>, <em>" . $lgrammatura . ": " . $wcover->getWeight() . " " . $measure_weight . "</em>";
                }
                ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo ucfirst($ltypecover); ?></span>
                    <span class="datacart">
    <?php echo ( $order_item->getCover() == 1 ) ? $item_page : $different_page . $add_cover_str; ?>
                    </span>
                </div>

<?php } ?>


            <!-- LAVORAZIONE EXTRA -->
<?php
$order_item_extra_processings = MP\OrderElementExtraProcessingQuery::create()->filterByOrderElementId($order_item_id)->find();
if (count($order_item_extra_processings) > 0) {
    ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo $extraprocessing; ?></span>


    <?php
    foreach ($order_item_extra_processings as $order_item_extra_processing) {
        ?>
                        <span class="datacart">
                        <?php
                        $extra_processing_lang = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($order_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
                        $extra_processing = MP\ProcessingTwoQuery::create()->findPk($order_item_extra_processing->getProcessingId());

                        echo $extra_processing_lang->getName();

                        /*
                          $is_special = $extra_processing->getIsSpecial();

                          $is_extra_side_req = $extra_processing->getIsExtra();



                          if ($is_extra_side_req == 1) {
                          if ($order_item_extra_processing->getExtraProcessingTop() == 1) {
                          echo ", <em>" . t("Top side") . "</em>";
                          }
                          if ($order_item_extra_processing->getExtraProcessingBottom() == 1) {
                          echo ", <em>" . t("Bottom side") . "</em>";
                          }
                          if ($order_item_extra_processing->getExtraProcessingLeft() == 1) {
                          echo ", <em>" . t("Left side") . "</em>";
                          }
                          if ($order_item_extra_processing->getExtraProcessingRight() == 1) {
                          echo ", <em>" . t("Right side") . "</em>";
                          }
                          }

                          if ($is_special == 1) {
                          $special_type = $extra_processing->getSpecialType();
                          echo ', <em>' . t("Distance") . ": " . $order_item_extra_processing->getDistanceSpecial() . " cm</em>";
                          echo ", <em>" . t("Number") . ": " . $order_item_extra_processing->getNumberSpecial() . " </em>";
                          }
                         */
                        ?>
                        </span>
                            <?php
                        }
                        ?>
                </div>
                <?php } ?>



            <!-- EVENTUALE ACCESSORI -->
<?php
$order_item_accessories = MP\OrderElementAccessoryQuery::create()->filterByOrderElementId($order_item_id)->find();
if (count($order_item_accessories) > 0) {
    ?>
                <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                    <span class="labelcart"><?php echo $accessorie; ?></span>

                <?php
                foreach ($order_item_accessories as $order_item_accessory) {
                    ?>
                        <span class="datacart">
                        <?php
                        $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($order_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
                        $accessory_name = $accessory_lang->getName();
                        $accessory_quantity = $order_item_accessory->getQuantity();
                        echo $accessory_name . " (" . $accessory_quantity . ")";
                        ?>
                        </span>
                            <?php
                        }
                        ?>       
                </div>
                <?php } ?>

            <!-- FILES TEMA -->
            <?php
            $form_type = $order_item->getFormType();
            $how_form = MP\FormtwoSmallFormatQuery::create()->filterByFormId($order_item->getFormId())->findOne();


            $files = 1 + $order_item->getPlusFile();
            
            $images = MP\OrderElementsFilesQuery::create()->filterByOrderElementId($order_item_id)->find();
            $images_count = count($images);

            $img = array();
            $e = 0;
            foreach ($images as $m => $image) {
                $img_url = file_load($image->getFileId())->uri;
                $img[$m + 1]['filename'] = str_replace('public://cart/', '', $image->getFilename());
                $img[$m + 1]['path'] = image_style_url('gallery-thumb', $img_url);
                $img[$m + 1]['status'] = $image->getFileStatus();
                $img[$m + 1]['message'] = $image->getErrorMessage();
                $img[$m + 1]['fid'] = $image->getFileId();
                $img[$m + 1]['source'] = $image->getSource();
                $img[$m + 1]['previewfid'] = $image->getPreviewFid();
                
                if ($image->getFileStatus() == 0)
                    $e++;
            }
            
            $lost_files = $files - $images_count;
            ?>


            <div class="datacart--files hidden-xs hidden-sm">

            <?php
            if ($files != 0) {
                //echo '<div class="box-messaggi box-messaggi-bigformat"><p>Per questo prodotto servono <span class="change_number_files">'. $files .'</span> files</p></div>';
                for ($i = 0; $i < $files; $i++) {
                    ?>
                        <div id="upi-<?php echo "$order_item_id$i"; ?>" class="uploadproductimage 
                             <?php if ($i < $images_count) { ?>
                             uploadproductimage--selected
                             <?php } ?>">
                            <input type="hidden" name="editor_save_img_data_<?php echo "$order_item_id$i"; ?>" id="editor_save_img_data_<?php echo "$order_item_id$i"; ?>" value="" />

                            <div class="pull-left">            
                                <div <?php if ($i < $images_count) { ?> style="display: none" <?php } ?>  id="image_<?php echo "$order_item_id$i"; ?>" class="fileuploader" c="<?php echo "$order_item_id$i"; ?>"><?php echo t('upload') ?></div>
                                <input id="image_fid_<?php echo "$order_item_id$i"; ?>" class="upload_image_fid upload_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 1) {
                                        echo $img[$i + 1]['fid'];

                                       } ?>"/>
                                <input id="gallery_fid_<?php echo "$order_item_id$i"; ?>" class="gallery_image_fid gallery_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 0) {
                                        echo $img[$i + 1]['fid'];

                                       } ?>"/>
                                <input id="editor_fid_<?php echo "$order_item_id$i"; ?>" class="editor_image_fid editor_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 2) {
                                        echo $img[$i + 1]['fid'];

                                       } ?>" data-preview="<?php if ($i < $images_count && $img[$i + 1]['source'] == 2) {
                                        echo $img[$i + 1]['previewfid'];

                                       } ?>"/>
                                <div class="dvPreview_<?php echo "$order_item_id$i"; ?>"></div>
                            </div>
                            <?php if ($order_item->getProductionStatusId() < 4) { ?>
                            <!--<a id="custom-file-theme-<?php echo "$order_item_id$i"; ?>" class="custom-file-theme btn btn-orange" data-toggle="modal" data-target="#exampleModal">
                                <?php if ($i < $images_count) { echo t('change theme'); } else { echo t('change theme'); } ?>
                            </a>
                            <a id="custom-file-editor-<?php echo "$order_item_id$i"; ?>" class="btn btn-success btn-editor custom-file-editor" data-toggle="modal" data-target="#editorModal" data-orderid="<?php echo "$order_item_id"; ?>"><i class="fa fa-paint-brush"></i>
                                <?php if ($i < $images_count) { echo t('change theme with editor'); } else { echo $leditor; } ?>
                            </a>-->
                            <?php } ?>
                            <div class="ImageError_<?php echo "$order_item_id$i"; ?> imageerror"></div>
                            <?php if ($i < $images_count) { ?>
                                <img id="load-img-<?php echo "$order_item_id$i"?>-<?php echo $img[$m + 1]['fid'] ?>" class="galleryimage--choose" src="<?php echo $img[$i + 1]['path'] ?>">
                                
                                <?php if ($order_item->getProductionStatusId() < 4) { ?>
                                    
                                    
                                        <?php if ($img[$i + 1]['source'] == 0) { ?>
                                        <a class="uploadproductimage__remove">
                                        <?php } else if ($img[$i + 1]['source'] == 1) { ?>
                                        <a class="uploadproductimage__remove2">
                                        <?php } else { ?>
                                        <a class="uploadproductimage__remove3">
                                        <?php } ?>
                                        <span class="fa fa-trash"></span>
                                        <?php echo t('remove image'); ?>
                                        </a>
                                        
                                <?php } ?>
                            <?php } ?>
                        </div>

                <?php
                }
            }
            ?>
            </div>

            <div class="hidden-xs hidden-sm">
                <div class="btn-add-cart">
                    <button class="orderproceed btn btn-success pull-right" data-nfiles="<?php echo $files; ?>" type="button" name="orderproceed" value="Proceed" id="orderproceed_<?php echo $order_item_id; ?>" c="<?php echo $order_item_id; ?>"><?php echo t('Invia File'); ?></button>
                </div>
            </div>
            
            
            
        </div>
    </div>

    <!-- alert file non caricati -->
<div class="col-md-12 col-sm-12 col-xs-12 box-messaggi box-messaggi<?php echo "$order_item_id"; ?>">
    <?php if( $lost_files > 0 ) :?>
      <p>
      <?php echo t('You have not entered all the required files.'); ?>
    <?php if( $e > 0 ) : ?>
      <br /><?php echo t('There are files incorrect.'); ?>
    <?php endif; ?>
    </p>
    <?php endif; ?>
  </div>

    <div class="clearfix"></div>
</div>

