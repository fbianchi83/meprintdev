<?php
session_start();
$sessionid = session_id();
global $language;
global $user;
$lang_name = $language->language;
ini_set('max_execution_time', 3000);
//include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';

$count = MP\CartItemsQuery::create()
  ->where('CartItems.SessionId =?', $sessionid)
  ->count();

if($user->uid != 0){
  $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->findOne();
  $sendto= "spedizione";
}else{
  $cart = MP\CartQuery::create()->filterBySessionId($sessionid)->filterByStatus(1)->findOne();
  $sendto= "reglog";
}
if ($cart) {
  ?>
  <form class="cart-form clearfix" method="POST" action="<?php echo $base_url . '/' . $lang_name . '/' . $sendto; ?>">
    <div class="col-md-12 col-sm-12 col-xs-12 product_name">
      <div class="col-md-4 col-sm-4 col-xs-7"><b><?php echo $cartproduct; ?></b></div>
      <div class="col-md-3 col-sm-3 text-center hidden-print hidden-xs"><b><?php echo $file; ?></b></div>
      <div class="col-md-2 col-sm-2 col-xs-2"><b class="hidden-xs"><?php echo $quantity; ?></b><b class="visible-xs"><?php echo t('Qty'); ?></b></div>
      <div class="col-md-3 col-sm-3 text-right col-xs-3"><b><?php echo $price; ?></b></div>
    </div>

    <?php
    $cart_items = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->find();
    $i = 0;
    $istopack= 0;
    foreach ($cart_items as $cart_item) {
      $i++;
      $dateship = $cart_item->getShippingDate('d-m-Y');
      // GRANDE FORMATO
      if ($cart_item->getProductType() == 'big') {
        $presult = MP\FormoneLangQuery::create()->filterByFormId($cart_item->getProductId())->filterByLanguageId($lang_name)->findOne();

        $returnquery = MP\FormoneBigFormatQuery::create()->filterByFormId($cart_item->getProductId())->findOne();
        if ($returnquery->getVolumeWeightPriceCalculation() == 1) {
            $istopack= 1;
        }
        ?>
        <?php $cartdelid = $cart_item->getCartItemId(); ?>

        <div class="col-md-12 col-sm-12 col-xs-12 cartdetailspage cart_<?php echo $cartdelid; ?>">
          <div class="clearfix"></div>
          <div class="col-md-4 col-sm-4 col-xs-7">
            <div class="cart_4">
              <div class="col-md-12 col-sm-12 col-xs-12 cartdetails_4">
                <b>
                  <span class="fa cartdetails--open fa-plus fa-2x hidden-print" data-toggle="collapse" href="#cart_<?php echo $cartdelid; ?>" aria-expanded="false"></span> <?php echo $presult->getName(); ?>
                </b>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 text-center hidden-xs hidden-print ">
            <?php
            $images = MP\CartItemFileQuery::create()->filterByCartItemId($cartdelid)->findOne();
            ?>
            <?php
            if ($images) {
              $image = $images->getFid();
              $image_uri = file_load($image)->uri;
              ?>
              <img src="<?php echo image_style_url("thumbnail", $image_uri); ?>" />
              <?php
            }
            else {
              echo '<i class="fa fa-camera"></i>';
            }
            ?>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
            <div class="col-md-6 col-sm-6 cartdetails text-center">
              <?php echo $cart_item->getQuanitity() ?>
            </div>
            <div class="col-md-6 col-sm-3 cartdetails text-right hidden-print">
              <div class="col-md-12 col-sm-12 delete_icon cartdetails">
                <a class="deletecart" url="<?php echo $cartdelid; ?>">
                  <i class="fa fa-trash-o"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3 cartdetailsline">
            <div class="cartdetails cartprice text-right">
              <?php
              $totlprc = $cart_item->getTotalPrice();
              echo number_format((float) $totlprc, 2, '.', '') . ' &euro;';
              ?>
            </div>
          </div>
          
          <div class="col-md-12 col-xs-12">
            <div class="collapse" id="cart_<?php echo $cartdelid; ?>">

              <!-- NUMERO DI COPIE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $nocopies; ?>
                </span>
                <span class="datacart">
                  <?php echo $cart_item->getQuanitity() ?>
                </span>
              </div>

              <!-- DIMENSIONI -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $cdimension; ?>
                </span>
                <span class="datacart">
                  <?php
                  if ($cart_item->getDimensionId() != "custom") {
                    $dquery = MP\DimensionsQuery::create()->filterByDimensionId($cart_item->getDimensionId())->findOne();
                    echo $dquery->getWidth() . '*' . $dquery->getHeight();
                  }
                  else {
                    echo $cart_item->getWidth() . "*" . $cart_item->getHeight();
                  }
                  ?>
                </span>
              </div>

              <!-- MATERIALE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $cmaterial; ?>
                </span>
                <span class="datacart">
                  <?php
                  $mquery = MP\MaterialsLangQuery::create()->filterByMaterialId($cart_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
                  echo $mquery->getName();
                  ?>
                </span>
              </div>

              <?php if ($cart_item->getProcessingId() != 11) { ?>
              <!-- LAVORAZIONE BASE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $processing; ?>
                </span>
                <span class="datacart">
                  <?php
                  $pquery = MP\ProcessingLangQuery::create()->filterByProcessingId($cart_item->getProcessingId())->filterByLanguageId($lang_name)->findOne();

                  $base_processing = MP\ProcessingQuery::create()->findPk($cart_item->getProcessingId());

                  echo $pquery->getName();

                  $is_special = $base_processing->getIsSpecial();

                  $is_extra_side_req = $base_processing->getIsExtra();

                  if ($is_extra_side_req == 1) {
                    if ($cart_item->getProcessingTop() == 1) {
                      echo " <em>" . t("Top side") . "</em>";
                    }
                    if ($cart_item->getProcessingBottom() == 1) {
                      echo " <em>" . t("Bottom side") . "</em>";
                    }
                    if ($cart_item->getProcessingLeft() == 1) {
                      echo " <em>" . t("Left side") . "</em>";
                    }
                    if ($cart_item->getProcessingRight() == 1) {
                      echo " <em>" . t("Right side") . "</em>";
                    }
                  }

                  if ($is_special == 1) {
                    $special_type = $base_processing->getSpecialType();
                    echo ", <em>" . t("Distance") . ": " . $cart_item->getProcessingDistanceSpecial() . " cm</em>";
                    echo ", <em>" . t("Number") . ": " . $cart_item->getProcessingNumberSpecial(). "</em>";
                  }
                  ?>
                </span>
              </div>
              <?php } ?>

              <?php
              //EVENTUALI LAVORAZIONI EXTRA
              $cart_item_extra_processings = MP\CartItemExtraProcessingQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
              if (count($cart_item_extra_processings) > 0) {
                ?>

                <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_extra_processings) > 1 )?'cartdetails--more':'';?>" >
                  <span class="labelcart">
                    <?php echo $extraprocessing; ?>
                  </span>

                  <?php
                  foreach ($cart_item_extra_processings as $cart_item_extra_processing) {
                    ?>
                    <span class="datacart">
                    <?php
                    $extra_processing_lang = MP\ProcessingLangQuery::create()->filterByProcessingId($cart_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
                    $extra_processing = MP\ProcessingQuery::create()->findPk($cart_item_extra_processing->getProcessingId());

                    $is_special = $extra_processing->getIsSpecial();

                    $is_extra_side_req = $extra_processing->getIsExtra();

                    echo $extra_processing_lang->getName();


                    if ($is_extra_side_req == 1) {
                      if ($cart_item_extra_processing->getExtraProcessingTop() == 1) {
                        echo ", <em>" . t("Top side") . "</em>";
                      }
                      if ($cart_item_extra_processing->getExtraProcessingBottom() == 1) {
                        echo ", <em>" . t("Bottom side") . "</em>";
                      }
                      if ($cart_item_extra_processing->getExtraProcessingLeft() == 1) {
                        echo ", <em>" . t("Left side") . "</em>";
                      }
                      if ($cart_item_extra_processing->getExtraProcessingRight() == 1) {
                        echo ", <em>" . t("Right side") . "</em>";
                      }
                    }

                    if ($is_special == 1) {
                      $special_type = $extra_processing->getSpecialType();
                      echo ', <em>' . t("Distance") . ": " . $cart_item_extra_processing->getDistanceSpecial() . " cm</em>";
                      echo ", <em>" . t("Number") . ": " . $cart_item_extra_processing->getNumberSpecial() . " </em>";
                    }
                    ?>
                    </span>
                      <?php
                    }
                    ?>     
                </div>
                  <?php
                }
                ?>

              <?php
              //EVENTUALE STRUTTURA
              if ($cart_item->getStructureId() != 0) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $accessorie_base; ?>
                  </span>
                  <span class="datacart">
                    <?php
                    $structure = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item->getStructureId())->filterByLanguageId($lang_name)->findOne();
                    $structure_name = $structure->getName();
                    echo $structure_name;
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <?php
              //EVENTUALI ACCESSORI
              $cart_item_accessories = MP\CartItemAccessoryQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
              if (count($cart_item_accessories) > 0) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_accessories) > 1 )?'cartdetails--more':'';?>"">
                  <span class="labelcart">
                <?php echo $accessorie; ?>
                  </span>

                    <?php
                    foreach ($cart_item_accessories as $cart_item_accessory) {
                      ?>
                    <span class="datacart">
                    <?php
                    $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
                    $accessory_name = $accessory_lang->getName();
                    $accessory_quantity = $cart_item_accessory->getQuantity();
                    echo $accessory_name . " (" . $accessory_quantity . ")";
                    ?>
                    </span>
                      <?php
                    }
                    ?>
                </div>
                  <?php
                }
                ?>

              <!-- EVENTUALE VERIFICA FILES -->
              <?php
              if ($cart_item->getOperatorReview() == 1) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $operatorreview . ' &euro;' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo t("Yes");
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALE PACCO ANONIMO -->
              <?php
              if ($cart_item->getAnonymousPack() == 1) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $anonoshiping; ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo t("Yes");
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALE NOME PREVETIVO -->
              <?php
              if ($cart_item->getName() != "") {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo ucfirst($quote_name); ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo $cart_item->getName();
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALI NOTE PREVETIVO -->
              <?php
              if ($cart_item->getNote() != "") {
                $_SESSION["note"] = $cart_item->getNote();
                
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $note; ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo $cart_item->getNote();
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- DATA DI SPEDIZIONE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
      <?php echo $Shippingdate; ?>
                </span>
                <span class="datacart">
                  <?php
                  $cDATE = $cart_item->getShippingDate();
                  echo $cDATE->format('d-m-Y');
                  ?>
                </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>


      <?php
    }
    else if ($cart_item->getProductType() == 'small') {
      // PICCOLO FORMATO
        $presult = MP\FormtwoLangQuery::create()->filterByFormId($cart_item->getProductId())->filterByLanguageId($lang_name)->findOne();

        $returnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($cart_item->getProductId())->findOne();
        ?>
        <?php $cartdelid = $cart_item->getCartItemId(); ?>

        <div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $cartdelid; ?>">
          <div class="clearfix"></div>
          <div class="col-md-4 col-sm-4 col-xs-4">
            <div class="cart_4">
              <div class="col-md-12 col-sm-12 cartdetails_4">
                <b>
                  <span class="fa cartdetails--open fa-plus fa-2x hidden-print" data-toggle="collapse" href="#cart_<?php echo $cartdelid; ?>" aria-expanded="false"></span> <?php echo $presult->getName(); ?>
                </b>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 text-center hidden-print">
            <?php
            $images = MP\CartItemFileQuery::create()->filterByCartItemId($cartdelid)->findOne();
            ?>
            <?php
            if ($images) {
              $image = $images->getFid();
              $image_uri = file_load($image)->uri;
              ?>
              <img src="<?php echo image_style_url("thumbnail", $image_uri); ?>" />
              <?php
            }
            else {
              echo '<i class="fa fa-camera"></i>';
            }
            ?>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
            <div class="col-md-6 col-sm-6 cartdetails text-center">
              <?php echo $cart_item->getQuanitity() ?>
            </div>
            <div class="col-md-6 col-sm-3 cartdetails text-right hidden-print">
              <div class="col-md-12 col-sm-12 delete_icon cartdetails">
                <a class="deletecart" url="<?php echo $cartdelid; ?>">
                  <i class="fa fa-trash-o"></i>
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3 cartdetailsline">
            <div class="cartdetails cartprice text-right">
              <?php
              $totlprc = $cart_item->getTotalPrice();
              echo number_format((float) $totlprc, 2, '.', '') . ' &euro;';
              ?>
            </div>
          </div>
          
          <div class="col-md-12 col-xs-12">
            <div class="collapse" id="cart_<?php echo $cartdelid; ?>">

              <!-- NUMERO DI COPIE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $nocopies; ?>
                </span>
                <span class="datacart">
                  <?php echo $cart_item->getQuanitity() ?>
                </span>
              </div>

              <?php
              //TIPOLOGIA PIEGA
              $cart_item_folding = MP\FoldingTypeTwoQuery::create()->filterByFoldingTypeId($cart_item->getFoldingTypeId())->findOne();
              if (count( $cart_item_folding ) > 0) {                
                ?>
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart"><?php echo $lfolding; ?></span>
                <span class="datacart">
                  <?php echo $cart_item_folding->getName(); ?>
                </span>
              </div>
              <?php } ?>
              
              <!-- DIMENSIONI -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $cdimension; ?>
                </span>
                <span class="datacart">
                  <?php
                  $dquery = MP\DimensionsTwoQuery::create()->filterByDimensionId($cart_item->getDimensionId())->findOne();
                  echo $dquery->getName();
                  ?>
                </span>
              </div>
              
              <!-- ORIENTAMENTO -->              
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart"><?php echo $lorientation; ?></span>
                <span class="datacart">
                  <?php echo ($cart_item->getOrientation() == 0)?$orientation_h:$orientation_v; ?>
                </span>
              </div>
              
                            
              <!-- MATERIALE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
                  <?php echo $cmaterial; ?>
                </span>
                <span class="datacart">
                  <?php
                  $mquery = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($cart_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
                  echo $mquery->getName();
                  ?>
                </span>
              </div>

              
              <?php
              // GRAMMATURA
              $cart_item_weight = MP\WeightsTwoQuery::create()->filterByWeightId($cart_item->getWeightId())->findOne();
              if (count( $cart_item_weight ) > 0) {                
                ?>
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart"><?php echo $lgrammatura; ?></span>
                <span class="datacart">
                  <?php echo $cart_item_weight->getWeight() . ' ' . $measure_weight; ?>
                </span>
              </div>
              <?php } ?>
              
              
              <?php
              // FACCIATE              
              if ( $cart_item->getNfaces() > 0 ) {                
                ?>
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart"><?php echo ucfirst($lnumface); ?></span>
                <span class="datacart">
                  <?php echo $cart_item->getNfaces(); ?>
                </span>
              </div>
              <?php } ?>
              
              <?php
              // TIPO COPERTINA              
              if ( $cart_item->getCover() > 0 ) {
                $add_cover_str = "";
                if ( $cart_item->getCover() == 2 ) {                  
                  //COPERTINA SPECIFICA: materiali e grammatura
                  $mcover = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($cart_item->getIdMaterialCover())->filterByLanguageId($lang_name)->findOne();                    
                  $wcover = MP\WeightsTwoQuery::create()->filterByWeightId($cart_item->getIdWeightCover())->findOne();

                  $add_cover_str = ", <em>". $cmaterial . ": " . $mcover->getName() ."</em>, <em>". $lgrammatura . ": " . $wcover->getWeight() . " " .$measure_weight ."</em>"; 
                    
                }
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart"><?php echo ucfirst($ltypecover); ?></span>
                  <span class="datacart">
                    <?php echo ( $cart_item->getCover() == 1 )?$item_page:$different_page.$add_cover_str; ?>
                  </span>
                </div>

              <?php } ?>
              
              
              <?php
              //EVENTUALI LAVORAZIONI EXTRA
              $cart_item_extra_processings = MP\CartItemExtraProcessingQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
              if (count($cart_item_extra_processings) > 0) {
                ?>

                <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_extra_processings) > 1 )?'cartdetails--more':'';?>" >
                  <span class="labelcart">
                    <?php echo $extraprocessing; ?>
                  </span>

                  <?php
                  foreach ($cart_item_extra_processings as $cart_item_extra_processing) {
                    ?>
                    <span class="datacart">
                    <?php
                    $extra_processing_lang = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($cart_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
                    $extra_processing = MP\ProcessingTwoQuery::create()->findPk($cart_item_extra_processing->getProcessingId());


                    echo $extra_processing_lang->getName();

                    
                    ?>
                    </span>
                      <?php
                    }
                    ?>     
                </div>
                  <?php
                }
              ?>

              

              <?php
              //EVENTUALI ACCESSORI
              $cart_item_accessories = MP\CartItemAccessoryQuery::create()->filterByCartItemId($cart_item->getCartItemId())->find();
              if (count($cart_item_accessories) > 0) {
              ?>
                <div class="col-md-12 col-sm-12 cartdetails <?php echo (count($cart_item_accessories) > 1 )?'cartdetails--more':'';?>"">
                  <span class="labelcart">
                <?php echo $accessorie; ?>
                  </span>

                    <?php
                    foreach ($cart_item_accessories as $cart_item_accessory) {
                      ?>
                    <span class="datacart">
                    <?php
                    $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($cart_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
                    $accessory_name = $accessory_lang->getName();
                    $accessory_quantity = $cart_item_accessory->getQuantity();
                    echo $accessory_name . " (" . $accessory_quantity . ")";
                    ?>
                    </span>
                      <?php
                    }
                    ?>
                </div>
                  <?php
                }
              ?>

              <?php
              // LAVORAZIONE FRONTE-RETRO              
              if ( $cart_item->getFrontBack() == 1 ) {                
                ?>
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart"><?php echo $fronteretro; ?></span>
                <span class="datacart">
                  <?php echo ( $cart_item->getPlusFile() == 1 )?$different_files:$item_files; ?>
                </span>
              </div>
              <?php } ?>
              
              
              <!-- EVENTUALE VERIFICA FILES -->
              <?php
              if ($cart_item->getOperatorReview() == 1) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $operatorreview . ' &euro;' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo t("Yes");
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALE PACCO ANONIMO -->
              <?php
              if ($cart_item->getAnonymousPack() == 1) {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $anonoshiping; ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo t("Yes");
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALE NOME PREVETIVO -->
              <?php
              if ($cart_item->getName() != "") {
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo ucfirst($quote_name); ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo $cart_item->getName();
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- EVENTUALI NOTE PREVETIVO -->
              <?php
              if ($cart_item->getNote() != "") {
                $_SESSION["note"] = $cart_item->getNote();
                
                ?>
                <div class="col-md-12 col-sm-12 cartdetails">
                  <span class="labelcart">
                <?php echo $note; ?>
                  </span>
                  <span class="datacart">
                    <?php
                    echo $cart_item->getNote();
                    ?>
                  </span>
                </div>
                    <?php
                  }
                  ?>

              <!-- DATA DI SPEDIZIONE -->
              <div class="col-md-12 col-sm-12 cartdetails">
                <span class="labelcart">
      <?php echo $Shippingdate; ?>
                </span>
                <span class="datacart">
                  <?php
                  $cDATE = $cart_item->getShippingDate();
                  echo $cDATE->format('d-m-Y');
                  ?>
                </span>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>


      <?php
    }
  }

  $cart_items_sum = MP\CartItemQuery::create()
    ->filterByCartId($cart->getCartId())
    ->withColumn('SUM(total_price)', 'Sum')
    ->findOne();
  
  //dd($user);
    /*$bigpackages= 0;
    $packcost= 0;
    if ($istopack) {
        $packages= package_generation($cart->getCartId(), 2);
        $bigpackages = bigpack_count($packages);
        
        $sets= MP\ShippingCostSettingsQuery::create()->filterByStatus(1)->findOne();
        $weight= $sets->getMaximumWeightPackage();
        $packcost= $sets->getPackageCost();
    }
    $ships= MP\ShippingCompaniesQuery::create()->filterByName('BRT')->findOne();
        
    $cost= $ships->getCoefficient();
    
        //dd($packages);
    $totweight= mp_shipping_calculate_shipweight($packages, $cost, $weight);
    $prices = MP\ShippingWeightRangeQuery::create()->filterByShippingId($ships->getShippingId())->find();
    if ($totweight == 0)
        $totweight = 0.000001;
    
    foreach($prices as $price) {
        if ($totweight > $price->getFromRange() && $totweight <= $price->getToRange())
            $_SESSION['ship_cost']= number_format((float) $price->getPrice(), 2, '.', '');
    }
    //dd($_SESSION['ship_cost']);*/
    $packages= package_generation($cart->getCartId(), 2);
    $bigpackages = bigpack_count($packages);
        
    $sets= MP\ShippingCostSettingsQuery::create()->filterByStatus(1)->findOne();
    $weight= $sets->getMaximumWeightPackage();
    $ships= MP\ShippingCompaniesQuery::create()->filterByName('BRT')->findOne();
    
    $cost= $ships->getCoefficient();
    
    $totweight= mp_shipping_calculate_shipweight($packages, $cost, $weight);
    $prices = MP\ShippingWeightRangeQuery::create()->filterByShippingId(1)->find();
    foreach($prices as $price) {
        if ($totweight > $price->getFromRange() && $totweight <= $price->getToRange())
            $_SESSION['ship_cost']= number_format((float) $price->getPrice(), 2, '.', '');
        $bigprice = number_format((float) $price->getPrice(), 2, '.', '');
        $bigweight = $price->getToRange();
    }
    
    if ($totweight > $bigweight)
        $_SESSION['ship_cost'] = $bigprice;
    if ($_SESSION['ship_cost'] == 0)
        $_SESSION['ship_cost'] = MP\ShippingWeightRangeQuery::create()->filterByShippingId(1)->findOne()->getPrice();
    
    $_SESSION['package_cost'] = number_format((float) $bigpackages * $sets->getPackageCost(), 2, '.', '');
      
  $usert = MP\UserQuery::create()->filterByUid($user->uid)->findOne();
  if (isset($usert))
      $typet= $usert->getUserType();
  else
      $typet= "";
  
  if ($user->uid != 0 && $typet == 'public') {
      $_SESSION['ivapercentage'] = 0;
      $_SESSION['iva'] = 0;
  }
  else {
      $_SESSION['ivapercentage'] = $VAT_RATE;
      $_SESSION['iva']= number_format((float) round($_SESSION['ship_cost']+$_SESSION['package_cost']+$cart_items_sum->getSum(), 2)*$VAT_RATE/100, 2, '.', '');
  }  
  ?>

    <div class="col-md-12 col-sm-12 col-xs-12 discountdiv">
      <div class="col-md-12 col-sm-12 col-xs-12">
  <!--                        <input type="checkbox" name="discountcheck" id="discountcheck"> <span class="coupanlabel"><?php #echo $coupanlabel;  ?></span>-->

        <div class="discountyes">
          <p class="label-block"><?php echo $Couponcode; ?></p>
          <input type="text" name="coupancode" value="" placeholder="inserisci il tuo coupon">
          <input type="hidden" name="cart_id" value="<?php echo $cart->getCartId(); ?>"/>
          <input type="hidden" name="session_id" value="<?php echo $_SESSION['session_id']; ?>"/>
          <input type="hidden" name="totalprice" value="<?php echo round($cart_items_sum->getSum(), 2); ?>"/>
          <input type="hidden" name="dicountamount" value=""/>
          <input type="hidden" name="finalamount" value="<?php echo round($cart_items_sum->getSum(), 2); ?>"/>
          <input type="hidden" name="ship" value="<?php echo $_SESSION['ship_cost']; ?>" />
          <input type="hidden" name="pack" value="<?php echo $_SESSION['package_cost']; ?>" />
          <input type="hidden" name="iva" value="<?php echo $_SESSION['iva']; ?>" />
          <input type="hidden" name="ivapercentage" value="<?php echo $_SESSION['ivapercentage']; ?>" />
          <input type="hidden" name="productprice" value=""/>
          <input type="hidden" name="discount_id" value=""/>
          <input type="hidden" name="couponpath" readonly="readonly" value="<?php echo $base_url; ?>/sites/all/themes/meprint/discount.php"/>
          <button type="button" name="discountbutton" id="discountbutton" value="Submit"><i class="fa fa-arrow-down"></i></button>
          <div class="valid" style="display:none;"><span><?php echo $scoupon; ?> </span></div>
          <div class="invalid" style="display:none;"><span><?php echo $fcoupon; ?> </span></div>
        </div>
      </div>
      
        <div class="col-md-12 col-sm-12 col-xs-12 price-summary text-right">
        <div class="disshow ivarow row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice"><?php echo $Shippingdate; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">        
            <span class="hiddp text-right" ><?php echo $dateship; ?></span>
          </div>
        </div>
        <div class="disshow row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice"><?php echo $productprice; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">
            <span class="hiddp text-right" ><?php echo number_format((float) round($cart_items_sum->getSum(), 2),2,".",""); ?></span> &euro;
          </div>
        </div>
        <div class="dishide row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
          <span class="discountlabel"><?php echo $discountprice; ?></span>
          </div> 
          <div class="col-md-2 col-sm-2 col-xs-6 discount text-right">
<!--           <input size="6" type="text" name="dicountamount" readonly="readonly" value="0" class="text-right"/> &euro;-->
            - <span class="dicountamount text-right" >0</span> &euro;
          </div>
        </div>    
        <div class="disshow row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice"><?php echo $shipprice; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">        
            <span class="hiddp text-right" ><?php echo number_format((float) round($_SESSION['ship_cost'], 2),2,".",""); ?></span> &euro;
          </div>
        </div>
        <div class="disshow row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice"><?php echo $packprice; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">        
            <span class="hiddp text-right" ><?php echo number_format((float) round($_SESSION['package_cost'], 2),2,".",""); ?></span> &euro;
          </div>
        </div>
        <div class="disshow row row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice ivaprice"><?php echo t("Taxable"); ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">        
            <span class="hiddp text-right taxableprice" ><?php echo number_format((float) round($cart_items_sum->getSum()+$_SESSION['ship_cost']+$_SESSION['package_cost'], 2),2,".",""); ?></span> &euro;
          </div>
        </div> 
        <div class="disshow ivarow row clearfix">
          <div class="col-md-10 col-sm-10 col-xs-6">
            <span class="producttotalprice ivaprice"><?php echo $ivaprice; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-6 text-right">        
            <span class="hiddp text-right ivapriceval" ><?php echo number_format((float) round($_SESSION['iva'], 2),2,".",""); ?></span> &euro;
          </div>
        </div>
          
        
        <div class="disshow row clearfix bg-total">
          <div class="col-md-10 col-sm-10 col-xs-12">
            <span class="producttotalprice"><?php echo $producttotalprice; ?></span>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-12 text-right">        
            <span class="hiddp text-right finalamount" ><?php echo number_format((float) round($cart_items_sum->getSum()+$_SESSION['iva']+$_SESSION['ship_cost']+$_SESSION['package_cost'], 2),2,".",""); ?></span> &euro;
          </div>
        </div>
          
        
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="space-2"></div>

    <div class="clearfix"></div>


    <div class="col-md-3 col-sm-6 col-xs-12 continue">
      <a href="<?php echo $base_url . '/' . $lang_name; ?>/">
        <span class="btn"><?php echo $conshoping; ?></span>
      </a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12 payment">
      <button class="btn btn-success" type="submit" name="proceedtoshipping" value="proceedtoshipping"><?php echo $Proceedtoship; ?></button>
    </div>
    <div class="col-sm-12 print-cart hidden-sm hidden-xs">
      <a href="javascript:print();"><i class="fa fa-print"></i><strong><?php echo $print_cart; ?></strong></a>
    </div>
  </form>
  <?php
  
}
else {
  echo '<h4>' . $noproductsavail . '</h4>';
}

