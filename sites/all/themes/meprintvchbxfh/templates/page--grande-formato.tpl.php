<?php

global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

drupal_add_js(base_path() . path_to_theme() . '/scripts/bigformat-validations.js');
drupal_add_js(base_path() . path_to_theme() . '/scripts/page-product.js');
drupal_add_js(base_path() . path_to_theme() . '/scripts/price.js');
drupal_add_js(base_path() . path_to_theme() . '/scripts/jquery-scrolltofixed-min.js');

$id = arg($id);

?>

<?php
include('header.tpl.php');
?>
<!--Start he Banner--->
<section class="container">
  <div class="space-3"></div>
  <!--End of the search--->
  <article class="row">
    <aside class="col-md-3 col-sm-3 col-xs-12">      
      <a class="menu-mobile btn btn-success visible-xs">Menu</a>            
      <?php print render($page['left_sidebar']); ?>      
      <div class="space-2 mm"></div>
      <?php print render($page['left_sidebar_prod']); ?>
      <div class="space-2 mm"></div>
    </aside>
    <div class="col-md-7 col-sm-7 col-xs-12">      
      <header class="row">
        <?php print render($page['content']); ?>
      </header>      
    </div>
    <aside class="col-md-2 col-sm-2 col-xs-12">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="panel panel-default summary-cart">
            <div class="panel-heading"><?php echo $cartpagetittle; ?></div>
            <div class="">

            <?php
            if ($cart && $cart->getStatus() == 1) {

              $cart_items_sum = MP\CartItemQuery::create()
                ->filterByCartId($cart->getCartId())
                ->withColumn('SUM(total_price)', 'Sum')
                ->findOne();

              $cart_items = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->limit(3)->find();
              ?>
                <table class="table table-condensed">
                  <tr>
                    <th class="grid_list"><?php echo $pname; ?></th>
                    <th class="grid_list"><?php echo $price; ?></th>
          <!--                    <th class="col-md-2 col-sm-2 grid_list">--><?php //echo $qty;  ?><!--</th>-->
                  </tr>
                  <?php
                  foreach ($cart_items as $item) {
                    /* @var $item MP\CartItem */
                    $dateship= $item->getShippingDate('d-m-Y');
                    if ($item->getProductType() == "big")
                        $pquery = MP\FormoneLangQuery::create()->filterByFormId($item->getProductId())->filterByLanguageId($lang_name)->findOne();
                    else
                        $pquery = MP\FormtwoLangQuery::create()->filterByFormId($item->getProductId())->filterByLanguageId($lang_name)->findOne();
                    $m = substr($pquery->getName(), 0, 15);
                    ?>
                    <tr>
                      <td class="">
                        <strong><?php echo $m; ?></strong><br />
                        <strong><?php echo t('Qty'); ?>. </strong> <?php echo $item->getQuanitity(); ?>
                      </td>
                      <td class="">
                        <strong><?php echo '&euro;' . number_format((float) $item->getTotalPrice(), 2, '.', '');
                ?></strong>
                      </td>
                    </tr>
                  <?php } ?>
                  <tr>
                    <td class=""><strong><?php echo $Shippingdate; ?></strong><br /></td>
                    <td class=""><?php echo $dateship; ?></td>
                  </tr>  
                  <tr>
                    <td class="text-center" colspan="2">
                      <p class="summary-cart__total"><?php echo $totalcost . ' &euro; ' . number_format((float) $cart_items_sum->getSum(), 2, '.', ''); ?></p>
                      <button class="btn btn-success" onclick="location.href='/carrello';"><?php echo $paynow; ?></button>
                    </td>

                  </tr>
                </table>
              <?php
              }
              else {
                echo '<h5 style="padding:5px;">' . $noproductsavail . '</h5>';
              }
              ?>
            </div>


            <div class="space-2"></div>
            <!--            <p class="text-center">
              <button type="button" class="btn btn-success">Success</button>
            </p>-->
          </div>
        </div>
      </div>
      <div class="space-2"></div>
      
      
      <!-- carrello che segue -->
      
      <div class="followcart">
       <div id="summary-cart--product" class="panel panel-default summary-cart summary-cart--product">
        <div class="panel-heading"><?php echo $summary; ?></div>
        <div class="">
          <table id="sample-table-2"  class="table table-condensed">
            <tbody>
              <tr>
                <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="printigmaterialdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
<!--              <tr>
                <td class="left"><strong><?php //echo $processcost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="adtnlcostdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="frontbacktd">
                <td class="left"><strong><?php //echo $front_back; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="frcostdiv" id="frcostdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="baseaccessoriestd">
                <td class="left"><strong><?php //echo $accessorie_base; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="baseacccstdiv" id="baseacccstdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>-->
              <tr class="extraaccessoriestd">
                <td class="left"><strong><?php echo $accessorie; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="extraacccstdiv" id="extraacccstdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
<!--                <tr>
                <td class="left"><strong><?php //echo $singleproductcost;  ?></strong></td>
                <td>
                  <div class="fa fa-eur singleproductcostdiv"></div>
                </td>
              </tr>-->
<!--              <tr class="extraprocessingtd">
                <td class="left"><strong><?php //echo $extraprocessingcost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="extraprocessingdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>-->
              <tr>
                <td class="left"><strong><?php echo $quantity; ?></strong></td>
                <td class="text-right">
                  <div class="totalnoofproductbigfrmtdiv"></div>
                </td>
              </tr>
              <tr class="promovaltd">
                <td class="left"><strong><?php echo $promoval; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="promovaldiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
<!--              <tr class="promopricetd">
                <td class="left"><strong><?php //echo $beforpromoprice; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span style="text-decoration:line-through" class="promopricediv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="promovaltd">
                <td class="left"><strong><?php //echo $promoval; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="promovaldiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="operatorreviewtd">
                <td class="left"><strong><?php //echo $operatorreview_cost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="operatorreviewdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>-->
              <tr class="shippingdatetd">
                <td class="left red"><strong><?php echo $Shippingdate; ?></strong></td>
                <td class="hidden-480">
                  <div class="shippingdatediv"></div>
                </td>
              </tr>
              <tr>
                <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="totalproductcostdiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              <tr class="ivatd">
                <td class="left"><strong><?php echo $iva_cost . ' (' . $VAT_RATE . '%)' ?></strong></td>
                <td class="hidden-480 text-right">
                  <span class="ivadiv"></span>
                  <i class="fa fa-eur"></i>
                </td>
              </tr>
              
              <tr>
                <td colspan="2" class="text-center">
                  <div class="summary-cart__total">
<?php echo $totalcost; ?> &nbsp; <span class="totalcostdiv"></span> <i class="fa fa-eur"></i>
                  </div>
                </td>
              </tr>
              
              <tr>
                <td colspan="2" class="text-center">
                  <div class="btn-add-cart">
                    <button class="btn btn-success" type="submit" name="proceed" value="Proceed" id="bigproceed2"><?php echo $addcart; ?></button>
                  </div>
                </td>
              </tr>

            </tbody>
          </table>
        </div>
        <?php
        //$id = arg($id);
        $this_prod = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->findOne();

        $instructions_file = $this_prod->getInstructionsFile();

        $listing_query = MP\FormoneBigFormatQuery::create()
          ->filterBySubcategoryId($this_prod->getSubcategoryId())
          ->filterByStatus(1)
          ->where("form_id != $id")
          ->find();
        
        ?>
        <div class="space-2"></div>
      </div>
      <!--fine carrello che segue-->
      <?php if (count($listing_query) > 0) {
      ?>
          <div id="relatedpanel" class="panel panel-default panel--mini">
            <h3 class="panel-heading"><?php echo $samecategory; ?></h3>
            <div class="panel-body">
              <div class="carousel-related">
              <?php

                foreach ($listing_query as $listing) {
                  //print_r( $listing );
                  $id_same = $listing->getFormId();
                  if ($id_same == $id) {
                    continue;
                  }
                  $check_lang = MP\FormoneLangQuery::create()
                    ->where('FormoneLang.LanguageId =?', $lang_name)
                    ->where('FormoneLang.FormId =?', $id_same)
                    ->findOne();
                  $name = substr($check_lang->getName(), 0, 25);

                  //$add_info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id_same)->filterByWebsiteId($lang_name)->findOne();

                  $add_info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id_same)->findOne();
                  
                  if($add_info->getImageListing() != 0 && $add_info->getImageListing() != ""){
                    $img = file_load($add_info->getImageListing());
                  }else{
                    $img = file_load($add_info->getImage());
                  }


                  //$img = file_load($ImgHome);
                  $img_path_small = image_style_url("same-category-small", $img->uri);
                  $img_path = image_style_url("same-category", $img->uri);
                  
                  

                  $tmp_subslug = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($listing->getSubcategoryId())->filterByLanguageId($lang_name)->findOne();              
                  $tmp_proslug = MP\ProductsgroupLangQuery::create()->filterByProductGroupId($listing->getProductgroupId())->filterByLanguageId($lang_name)->findOne();

                  $prod_slug = $check_lang->getSlug();
                  $subslug = $tmp_subslug->getSlug();
                  $pro_slug = $tmp_proslug->getSlug();


                  $url_big = $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $id_same;

                ?>
                    <div class="media media--mini-listing">
                      <div class="media-left hidden-sm">
                        <a href="<?php echo $url_big; ?>">
                          <img class="media-object" src="<?php echo $img_path_small; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo $img_path; ?>" alt="<?php echo $name; ?>" /><p></p></div>'>
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-mini-listing__title"><?php echo $name; ?></h4>
                      </div>
                    </div>
                <?php
              }

            ?>
              </div>
            </div>
          </div>

          <div class="space-2"></div>
      <?php } ?>
          <?php 
      
      $materials = MP\FormoneMaterialsQuery::create()->filterByFormId($id)->find(); ?>

      <div id="panel-file" class="panel panel-default panel-file summary-cart">
        <div class="panel-heading"><?php echo $uploadimagetitle; ?></div>
        <div class="panel-body">
          <?php 
          $i=0;
          foreach($materials as $material) {
              $cert_file = MP\MaterialsQuery::create()->filterByMaterialId($material->getMaterialId())->findOne();
              $certification_file = $cert_file->getFireProofPdf();
              
              if(isset($certification_file) && $certification_file != ""  && $certification_file != 0){
                $certification_file_uri = file_load($certification_file)->uri;
           ?>
          <div id="cert<?php echo $material->getMaterialId(); ?>">
            <i class="fa fa-download fa-2x hidden-sm"></i>          
            <a href="<?php echo file_create_url($certification_file_uri); ?>" target="_blank" class="dld material_certification"><span><?php echo t('Download certification'); ?></span></a>
          </div>
            <?php
          }
          }
              $prod = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->findOne();
              $instructions_file = $prod->getInstructionsFile();
              if(isset($instructions_file) && $instructions_file != ""  && $instructions_file != 0){
                $instructions_file_uri = file_load($instructions_file)->uri;
            ?>
                <div>
                  <i class="fa fa-book fa-2x hidden-sm"></i>
                  <a target="_blank" href="<?php echo file_create_url($instructions_file_uri); ?>" class="dld"><span><?php echo t('Download instructions'); ?></span></a>
                </div>
            <?php
              }
            ?>
        </div>
      </div>
      </div>
    </aside>
  </article>
</section>

<?php
include('footer.tpl.php');
?>
