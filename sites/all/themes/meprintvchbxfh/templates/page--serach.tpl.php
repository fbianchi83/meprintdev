
<?php
 include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
 global $base_path;
 global $base_url;
 ?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<aside class="col-md-3 col-sm-3">
  <a class="menu-mobile btn btn-success visible-xs">Menu</a>
  <?php print render($page['left_sidebar']);?>
  <div class="space-2"></div>
</aside>
<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">
  <div class="row">
	<?php if ($breadcrumb): print $breadcrumb; endif;?>
  </div>
    <?php if ($title): ?><h3>&nbsp;<?php //print $title; ?>&nbsp;<span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image"></span> </h3><?php endif; ?>
    <div class="space-3"></div>
   
  </div>
  </header>
  <div class="space-3"></div>
  <?php print render($page['content']);?>
 <?php //advanced_search($adv);?>

</div>  
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>