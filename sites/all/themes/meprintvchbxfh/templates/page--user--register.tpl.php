<?php

/**
 * @file
 * Meprint theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<?php
//include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
//include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/meprint.inc';
global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;
drupal_add_js('/sites/all/themes/meprintvchbxfh/scripts/registration.js');

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
include_once DRUPAL_ROOT . '/sites/default/files/listaprovince.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );
$lista_province_json = listaprovince();
$province = drupal_json_decode( $lista_province_json );

//dd( $nazioni );

?>

<?php
  include('header.tpl.php');
?>
  <!--Start he Banner--->
  <section class="container">
    <!--<div class="space-3"></div>-->
    <!--End of the search--->
    <article class="row">
      <div class="col-md-12 col-sm-12">
        <header class="row">
          <div class="col-sm-12 col-md-12 ">
            <!--<div class="row breadcrumb">
              <?php /*//if ($breadcrumb): print $breadcrumb.drupal_get_title(); endif;*/?>
              <?php /*if ($breadcrumb): echo '<a href="' . $base_url . '">' . $Home . '</a>' . ' / ' . drupal_get_title(); endif; */?>
            </div>-->
            <?php if ($title): ?>

              <h3 class="page-title"><?php echo t("Register to MePrint.it"); ?><span><img
                    src="<?php print base_path() . path_to_theme(); ?>/images/wave.png"
                    alt="image" style="margin-left: 15px"></span></h3>

            <?php endif; ?>
            <div class="space-3"></div>

          </div>
        </header>
        <!-- For Error Messages Start  -->
        <?php if ($messages): ?>
          <div id="messages">
            <div class="section clearfix">
              <?php print $messages; ?>
            </div>
          </div> <!-- /.section, /#messages -->
        <?php endif; ?>
        <!-- For Error Messages End  -->
        <div class="space-3"></div>
        <?php // print render($page['content']); ?>



        <?php
        ////////////////////////////////////////////////////////
        ///// FORM REGISTRAZIONE //////////////////////////////
        ?>

        <form id="form-registration" class="form-registration spacer" action="/add-user" method="POST" >
          <fieldset class="boxed">
            <legend><?php echo t('Country and type of customer'); ?></legend>
            <div class="row">              
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lcontry; ?></label>
                <select name="nation" id="form-registration__country" class="form-control">
                  
                  <?php 
                    foreach( $nazioni as $k => $nazione ){
                      $selected = "";
                      if( $nazione["countryCode"] == "IT" ) { //$selected = 'selected="selected"';
                      
                        echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
                      }
                    }

                  ?>

                </select>
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo $ltype_customer; ?></label>
                <select name="user_type" id="form-registration__type" class="form-control" required >
                  <option value="">---</option>
                  <option value="private"><?php echo t('Private'); ?></option>
                  <option value="society"><?php echo t('Society'); ?></option>
                  <option value="public"><?php echo t('Public'); ?></option>
                  <option value="association"><?php echo t('Association'); ?></option>
                  <option value="individual"><?php echo t('Individual'); ?></option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 box-messaggi">
                <p><?php echo t("Choose type of customer");  ?></p>
              </div>
            </div>
          </fieldset>
          <fieldset class="boxed">
            <legend><?php echo t('Billing information and contact details'); ?></legend>
            <?php ////////////// PRIVATO //////////////  ?>
            <div id="private" class="form-registration__optional" style="display: none">
              <div class="row">
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lname; ?></label>
                  <input type="text" class="form-control" name="private_name" id="form-registration__name" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lsurname; ?></label>
                  <input type="text" class="form-control" name="private_surname" id="form-registration__surname" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcf; ?></label>
                  <input type="text" class="form-control cfcontrol" name="private_fiscal_code" id="form-registration__cf" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lprovince; ?></label>
                  <select name="private_province" id="form-registration__prov" class="form-control">
                  
                  <?php 
                    foreach( $province as $k => $provincia ){
                        echo '<option value="' . $provincia["provinceCode"] . '" >' . $provincia["provinceName"] . '</option>';
                    }

                  ?>

                  </select>
                  <!--<input style="display:none" type="text" class="form-control" name="private_province" id="form-registration__prov" />-->
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $llocation; ?></label>
                  <input type="text" class="form-control" name="private_city" id="form-registration__location" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $laddress; ?></label>
                  <input type="text" class="form-control" name="private_address" id="form-registration__address" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lzipcode; ?></label>
                  <input type="text" class="form-control" name="private_zip_code" id="form-registration__zip" />
                </div>
              </div>
            </div>
            <?php ////////////// FINE PRIVATO //////////////  ?>

            <?php ////////////// SOCIETA //////////////  ?>
            <div id="society" class="form-registration__optional" style="display: none;">
              <div class="row">
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcompany_name; ?></label>
                  <input type="text" class="form-control" name="society_company_name" id="form-registration__business_name" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo t('type'); ?></label>
                  <select name="society_company_type" id="form-registration__type_company" class="form-control">
                    <option value="s.s">s.s</option>
                    <option value="sas">sas</option>
                    <option value="snc">snc</option>
                    <option value="spa">spa</option>
                    <option value="srl">srl</option>
                    <option value="srlu">srlu</option>
                    <option value="srls">srls</option>
                    <option value="sapa">sapa</option>
                    <option value="scarl">scarl</option>
                    <option value="_">altro</option>
                  </select>
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lvat; ?></label>
                  <input type="text" class="form-control" name="society_vat" id="form-registration__vat" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcf; ?></label>
                  <input type="text" class="form-control" class="form-control cfcontrol" name="society_fiscal_code" id="form-registration__cf_company" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lprovince; ?></label>
                  <select name="society_province" id="form-registration__prov_company" class="form-control">
                  
                  <?php 
                    foreach( $province as $k => $provincia ){
                        echo '<option value="' . $provincia["provinceCode"] . '" >' . $provincia["provinceName"] . '</option>';
                    }

                  ?>
                  </select>
                  <!--</select><input type="text" class="form-control" name="society_province" id="form-registration__prov_company" />-->
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $llocation; ?></label>
                  <input type="text" class="form-control" name="society_city" id="form-registration__location_company" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $laddress; ?></label>
                  <input type="text" class="form-control" name="society_address" id="form-registration__address_company" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lzipcode; ?></label>
                  <input type="text" class="form-control" name="society_zip_code" id="form-registration__zip_company" />
                </div>
              </div>
            </div>
            <?php ////////////// FINE SOCIETA //////////////  ?>

            <?php ////////////// ASSOCIAZIONE //////////////  ?>
            <div id="association" class="form-registration__optional" style="display: none">
              <div class="row">
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcompany_name; ?></label>
                  <input type="text" class="form-control" name="association_company_name" id="form-registration__bussiness_name_club"/>
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcf; ?></label>
                  <input type="text" class="form-control" name="association_fiscal_code" id="form-registration__cf_club"/>
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lvat; ?></label>
                  <input type="text" class="form-control" name="association_vat" id="form-registration__vat_club"/>
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lprovince; ?></label>
                  <select name="association_province" id="form-registration__prov_club" class="form-control">
                  
                  <?php 
                    foreach( $province as $k => $provincia ){
                        echo '<option value="' . $provincia["provinceCode"] . '" >' . $provincia["provinceName"] . '</option>';
                    }

                  ?>
                  </select>
                  <!--<input type="text" class="form-control" name="association_province" id="form-registration__prov_club" />-->
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $llocation; ?></label>
                  <input type="text" class="form-control" name="association_city" id="form-registration__location_club" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $laddress; ?></label>
                  <input type="text" class="form-control" name="association_address" id="form-registration__address_club" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lzipcode; ?></label>
                  <input type="text" class="form-control" name="association_zip_code" id="form-registration__zip_club" />
                </div>
              </div>
            </div>
            <?php ////////////// FINE ASSOCIAZIONE //////////////  ?>

            <?php ////////////// INDIVIDUALE //////////////  ?>
            <div id="individual" class="form-registration__optional" style="display: none">
              <div class="row">
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcompany_name; ?></label>
                  <input type="text" class="form-control" name="individual_company_name" id="form-registration__denomination_single" />
                </div>
                <div class="form-group col-sm-4">
                    <div class="intermediate_particle">
                        di
                    </div>  
                  <label for=""><?php echo $lname; ?></label>
                  <input type="text" class="form-control" name="individual_name" id="form-registration__name_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lsurname; ?></label>
                  <input type="text" class="form-control" name="individual_surname" id="form-registration__surname_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lvat; ?></label>
                  <input type="text" class="form-control" name="individual_vat" id="form-registration__vat_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcf; ?></label>
                  <input type="text" class="form-control cfcontrol" name="individual_fiscal_code" id="form-registration__cf_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lprovince; ?></label>
                  <select name="individual_province" id="form-registration__prov_single" class="form-control">
                  
                  <?php 
                    foreach( $province as $k => $provincia ){
                        echo '<option value="' . $provincia["provinceCode"] . '" >' . $provincia["provinceName"] . '</option>';
                    }

                  ?>
                  </select>
                  <!--<input type="text" class="form-control" name="individual_province" id="form-registration__prov_single" />-->
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $llocation; ?></label>
                  <input type="text" class="form-control" name="individual_city" id="form-registration__location_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $laddress; ?></label>
                  <input type="text" class="form-control" name="individual_address" id="form-registration__address_single" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lzipcode; ?></label>
                  <input type="text" class="form-control" name="individual_zip_code" id="form-registration__zip_single" />
                </div>
              </div>
            </div>
            <?php ////////////// FINE INDIVIDUALE //////////////  ?>

            <?php ////////////// ENTE PUBBLICO //////////////  ?>
            <div id="public" class="form-registration__optional" style="display: none;">
              <div class="row">
                <div class="form-group col-sm-4">
                  <label for=""><?php echo t('Name institution'); ?></label>
                  <input type="text" class="form-control" name="public_company_name" id="form-registration__name_ente" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lvat; ?></label>
                  <input type="text" class="form-control" name="public_vat" id="form-registration__vat_ente" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lcf; ?></label>
                  <input type="text" class="form-control cfcontrol" name="public_fiscal_code" id="form-registration__cf_ente" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lprovince; ?></label>
                  <select name="public_province" id="form-registration__prov_ente" class="form-control">
                  
                  <?php 
                    foreach( $province as $k => $provincia ){
                        echo '<option value="' . $provincia["provinceCode"] . '" >' . $provincia["provinceName"] . '</option>';
                    }

                  ?>
                  </select>
                  <!--<input type="text" class="form-control" name="public_province" id="form-registration__prov_ente" />-->
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $llocation; ?></label>
                  <input type="text" class="form-control" name="public_city" id="form-registration__location_ente" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $laddress; ?></label>
                  <input type="text" class="form-control" name="public_address" id="form-registration__address_ente" />
                </div>
                <div class="form-group col-sm-4">
                  <label for=""><?php echo $lzipcode; ?></label>
                  <input type="text" class="form-control" name="public_zip_code" id="form-registration__zip_ente" />
                </div>
              </div>
            </div>
            <?php ////////////// FINE ENTE PUBBLICO //////////////  ?>

            <div class="row">
              <div class="form-group col-sm-4 namerif">
                <label for=""><?php echo $lname_rif; ?></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" name="reference_person_name" id="form-registration__user_rif_name" required />
                </div>
              </div>
              <div class="form-group col-sm-4 surnamerif">
                <label for=""><?php echo $lsurname_rif; ?></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" name="reference_person_surname" id="form-registration__user_rif_surname" required />
                </div>
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lphone; ?></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-phone fa-lg"></i></span>
                  <input type="tel" class="form-control phones" name="phone" id="form-registration__phone_rif" />
                </div>
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lmobile; ?></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-mobile fa-lg"></i></span>
                  <input type="tel" class="form-control phones" name="mobile_phone" id="form-registration__cell_rif" />
                </div>
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lfax; ?></label>
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-fax"></i></span>
                  <input type="tel" class="form-control" name="fax" id="form-registration__fax_rif" />
                </div>
              </div>              
            </div>
          </fieldset>
          <fieldset class="boxed">
            <legend><?php echo t('Email and Password');?></legend>            
            <div class="row">
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lemail; ?></label>
                <input type="email" class="form-control" name="email" id="form-registration__email" required />
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo $lemailbis; ?></label>
                <input type="email" class="form-control" name="email_bis" id="form-registration__emailbis" required />
              </div>
            </div>
            
            <div class="row">
              <div class="form-group col-sm-4">
                <label for=""><?php echo t('Password');?></label>
                <input type="password" class="form-control" name="password" id="form-registration__pw" required />
              </div>
              <div class="form-group col-sm-4">
                <label for=""><?php echo t('Repeat password');?></label>
                <input type="password" class="form-control" name="password_bis" id="form-registration__pwbis" required />
              </div>
            </div>
          </fieldset>
          <fieldset class="boxed">
            <div class="checkbox">
              <label class="form-registration__privacy">
                <input type="checkbox" name="privacy" id="form-registration__privacy" required > <?php echo t('I accept the regulations for privacy'); ?>
              </label>
            </div>
            <div class="checkbox">
              <label class="form-registration__privacy">
                <input type="checkbox" name="newsletter" id="form-registration__newsletter" checked > <?php echo t('Subscribe to newsletter'); ?>
              </label>
            </div>
            <button class="btn btn-red"><?php echo t('Register'); ?></button>
          </fieldset>

        </form>





        <?php
        ////////////////////////////////////////////////////////
        ///// FINE FORM REGISTRAZIONE //////////////////////////////
        ?>

      </div>
    </article>
  </section>
  
  <?php
    include('footer.tpl.php');
  ?>