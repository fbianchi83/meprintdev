<table cellpadding="0" cellspacing="0" border="0" width="100%">
  <tr>
    <td colspan="2" valign="top" style="vertical-align: top; border-top: 3px solid #f0efeb; padding: 20px 0;">
      <h2 style="text-transform: uppercase; font-weight: bold; margin-bottom: 10px; display: block;font-size: 12px">I nostri punti di forza</h2>
      <img src="<?php echo $merge_url; ?>/images/email/punti-forza.png" alt="I nostri punti di forza" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" width="600" />
    </td>
  </tr>
  <tr>
    <td valign="top">
      <h2 style="text-transform: uppercase; font-weight: bold; margin-bottom: 10px; display: block;font-size: 12px">Metodi di pagamento</h2>
      <img src="<?php echo $merge_url; ?>/images/email/pagamento.png" alt="Metodi di pagamento" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" />
    </td>
    <td valign="top" style="padding-bottom: 30px">
      <h2 style="text-transform: uppercase; font-weight: bold; margin-bottom: 10px; display: block;font-size: 12px">Spedizioni</h2>
      <img src="<?php echo $merge_url; ?>/images/email/spedizioni.png" alt="Spedizioni" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" />
    </td>
  </tr>
</table>
<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-top: 3px solid #f0efeb;">
  <tr>
    <td colspan="3" valign="top" style="padding-top: 15px" ></td>
  </tr>
  <tr>
    <td valign="top" style="text-align: center">
      <a href="#" style="border: none"><img src="<?php echo $merge_url; ?>/images/email/social-fb.png" alt="fb" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /> </a>
    </td>
    <td valign="top" style="text-align: center">
      <a href="#" style="border: none"><img src="<?php echo $merge_url; ?>/images/email/social-twitter.png" alt="fb" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /> </a>
    </td>
    <td valign="top" style="text-align: center">
      <a href="#" style="border: none"><img src="<?php echo $merge_url; ?>/images/email/social-google.png" alt="fb" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" /> </a>
    </td>
  </tr>
  <tr>
    <td></td>
    <td><p style="font-size: 10px; line-height: 1.3; text-align: center; margin: 15px 0">MePrint &egrave; un marchio di Cartoons S.r.l.<br />P.IVA 01460970500 registro imprese di Pisa<br />Capitale Sociale 10.000 &euro; i.v.</p></td>
    <td></td>
  </tr>
</table>