
<div class="row">
  <div class="col-sm-12">
    <div class="boxed spacer">
      <div class="row">
        <div class="col-sm-6">
          <?php print drupal_render_children($form) ?>
        </div>
        <div class="col-sm-6 login-border-left">
          <div class="fb-login text-center">
            <a href="/user/simple-fb-connect" class="fb-login__btn"><?php echo t('Login with facebook'); ?></a>
            <p><?php echo t('Improve your experience <strong> MePrint.it </ strong> and connected with your <strong> Facebook account </ strong>.'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

