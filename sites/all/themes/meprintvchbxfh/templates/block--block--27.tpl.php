<?php
  global $user;
  global $base_url;
  global $language;
  $lang_name = $language->language;
?>
<?php
include('frame_modal.tpl.php');
?>

<ul class="topnav" id="main-menu-user">
    <li class="selected"><a href="<?php echo $base_url. '/' .$lang_name; ?>/user">Il mio MePrint</a></li>
        <?php
        global $user;
        global $base_url;
        $rid = db_query('SELECT rid FROM {users_roles} where uid=' . $user->uid)->fetchField();
        if ($user->uid != 0) {
          ?>
      <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/user/my-profile">Il mio profilo</a></li>
      <!--<li> <a href="<?#php echo $base_url. '/' .$lang_name; ?>/user/<?#php echo $user->uid; ?>/change-password">Cambia password</a></li>-->
      <li> <a href="<?php echo $base_url. '/' .$lang_name; ?>/user/change-password">Cambia password</a></li>
          <?php if ($user->uid == 1) { ?>
        <li> <a href="<?php echo $base_url. '/' .$lang_name; ?>/tonic/productgroup/view">Gestione Prodotti</a></li> 
      <?php }if ($user->uid == 1 || $rid == 4 || $rid == 6 || $rid == 7 ) { ?>
        <li><a href="#">Gestione Ordini</a>
            <ul>
       <?php if ($user->uid == 1) { ?>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/all">Tutti gli ordini</a></li>
       <?php }if ($user->uid == 1 || $rid == 4 || $rid == 6 || $rid == 7) { ?>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/open">Ordini in corso</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/toship">Ordini da spedire</a></li>
       <?php }if ($user->uid == 1) { ?>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/closed">Ordini evasi</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/bordero">Lista spedizioni giornaliere</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/bills">Fatture</a></li>
       <?php }if ($user->uid == 1 || $rid == 4 || $rid == 6 || $rid == 7) { ?>          
                <li><a href="<?php echo $base_url; ?>/it/orders/order_elements2/order">Prodotti Ordinati</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/order_elements2/build">Prodotti da produrre</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/order_elements/ship">Prodotti da spedire</a></li>
       <?php }if ($user->uid == 1) { ?>
                <li><a href="<?php echo $base_url; ?>/it/orders/order_discounts">Sconti utilizzati</a></li>
                <li><a href="<?php echo $base_url; ?>/it/orders/view/cancel">Ordini annullati</a></li>
       <?php } ?>
            </ul>
        </li>
        
        <?php if ($user->uid == 1) { ?>
        <li><a href="#">Statistiche</a>
            <ul>
                <li><a href="<?php echo $base_url; ?>/it/report/revenue">Fatturato</a></li>
                <li><a href="<?php echo $base_url; ?>/it/report/topcart">Top Carrelli</a></li>
                <li><a href="<?php echo $base_url; ?>/it/report/lastsell">Ultimi Acquisti</a></li>
                <li><a href="<?php echo $base_url; ?>/it/report/history">Storico Acquisti</a></li>
                <li><a href="#">Carrelli Abbandonati</a>
                    <ul>
                        <li><a href="<?php echo $base_url; ?>/it/report/abandonedcart">Grafici per periodi</a></li>
                        <li><a href="<?php echo $base_url; ?>/it/report/abandonedgroupcart">Raggruppamento per cliente</a></li>
                        <li><a href="<?php echo $base_url; ?>/it/report/abandonedclientcart">Filtri per cliente</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <?php } ?>
        <li> <a href="<?php echo $base_url. '/' .$lang_name; ?>/preferiti">I miei preferiti</a></li>
        <li> <a style="cursor:pointer" data-toggle="modal" data-target="#frameModal">Gestione Review Prodotti</a></li>
  <?php } else { ?>
        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/preferiti">I miei preferiti</a></li>
        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/my-orders/view">I miei ordini</a></li>
        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/carrello">Il mio carrello</a></li>
        <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/user/rubrica">La mia rubrica</a></li>
  <?php } } ?>
</ul>
<br>