<?php
/**
 * @file
 * Meprint theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['triptych_first']: Items for the first triptych.
 * - $page['triptych_middle']: Items for the middle triptych.
 * - $page['triptych_last']: Items for the last triptych.
 * - $page['footer_firstcolumn']: Items for the first footer column.
 * - $page['footer_secondcolumn']: Items for the second footer column.
 * - $page['footer_thirdcolumn']: Items for the third footer column.
 * - $page['footer_fourthcolumn']: Items for the fourth footer column.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bartik_process_page()
 * @see html.tpl.php
 */
?>
<?php
global $base_url;
global $base_path;
global $language;
global $user;
$lang_name = $language->language;
?>

<?php
include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
  <!--<div class="space-3"></div>-->
  <!--End of the search--->
  <article class="row">
    <aside class="col-md-3 col-sm-3">
      <a class="menu-mobile btn btn-success visible-xs">Menu</a>
      <?php print render($page['left_sidebar']); ?>
           
    </aside>
    <div class="col-md-9 col-sm-9">
      <header class="row">
        <div class="col-sm-12 col-md-12 ">
          <!--<div class="row breadcrumb">
          <?php /* //if ($breadcrumb): print $breadcrumb.drupal_get_title(); endif; */ ?>
          <?php /* if ($breadcrumb): echo '<a href="' . $base_url . '">' . $Home . '</a>' . ' / ' . drupal_get_title(); endif; */ ?>
          </div>-->
          <?php if ($title && arg(0) != 'listing'): ?>

            <h1 class="page-title"><?php print $title; ?><span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image" style="margin-left: 15px"></span></h1>
            <div class="space-3"></div>
          <?php endif; ?>


        </div>
      </header>
      <!-- For Error Messages Start  -->
      <?php if ($messages): ?>
        <div id="messages">
          <div class="section clearfix">
            <?php print $messages; ?>
          </div>
        </div> <!-- /.section, /#messages -->
        <div class="space-3"></div>
      <?php endif; ?>
      <!-- For Error Messages End  -->      
      <?php print render($page['content']); ?>

      <?php
      /*
       * Search functionality
       */
      if (isset($_POST['submit'])) {
        $adv = $_POST['adserach'];
        advanced_search($adv);
      }

      function advanced_search($adv) {
        global $base_url;
        /*         * Search in groups */
        $pinfo = array();
        $pinfo_small = array();
        $pbigfo = array();
        $catinfos = array();
        $new_big = array();
        $new_bigs = array();
        $category_id = array();
        $subcategory_id = array();
        $category_fr_id = array();
        $pg_ids = array();
        $lang_name = 'it';
        /* Search My BigFormat Material */
        $matinfo = MP\MaterialsLangQuery::create()
          ->filterByLanguageId($lang_name)
          ->filterByName('%' . $adv . '%')
          ->_or()
          ->filterByDescription('%' . $adv . '%')
          ->select(array('MaterialId'))
          ->find();
        if (count($matinfo) > 0) {
          foreach ($matinfo as $mat_res) {
            $mat_pr_list = MP\FormoneMaterialsQuery::create()
              ->filterByMaterialId($mat_res)
              ->select(array('FormId'))
              ->find();
            foreach ($mat_pr_list as $mat_pr_data) {
              if (!in_array($mat_pr_data, $pinfo)) {
                $pinfo[] = $mat_pr_data;
              }
            }
          }
        }
        /* Search My BigFormat ProductName */
        $myresults_pr = MP\FormoneLangQuery::create()
          ->filterByName('%' . $adv . '%')
          ->_or()
          ->filterByTitle('%' . $adv . '%')
          ->_or()
          ->filterByDescription('%' . $adv . '%')
          ->select(array('FormId'))
          ->find();
        if (count($myresults_pr) > 0) {
          foreach ($myresults_pr as $myresults_data) {
            if (!in_array($myresults_data, $pbigfo)) {
              $pbigfo[] = $myresults_data;
            }
          }
        }
        if (count($pbigfo) > 0 && count($pinfo) > 0) {
          $new_big = array_unique(array_merge($pbigfo, $pinfo));
        }
        else if (count($pbigfo) > 0) {
          $new_big = $pbigfo;
        }
        else if (count($pinfo) > 0) {
          $new_big = $pinfo;
        }
        /* Search My BigFormat Category */
        $catinfo = MP\ProductsgroupLangQuery::create()
          ->filterByLanguageId($lang_name)
          ->filterByName('%' . $adv . '%')
          ->_or()
          ->filterByDescription('%' . $adv . '%')
          ->select(array('ProductgroupId'))
          ->find();
        if (count($catinfo) > 0) {
          foreach ($catinfo as $cat_res) {
            if (!in_array($cat_res, $category_id)) {
              $category_id[] = $cat_res;
            }
          }
        }
        /* Search My BigFormat SubCategory */
        $catinfos = MP\ProductsubgroupLangQuery::create()
          ->filterByLanguageId($lang_name)
          ->filterByName('%' . $adv . '%')
          ->_or()
          ->filterByDescription('%' . $adv . '%')
          ->select(array('ProductgroupId'))
          ->find();
        if (count($catinfos) > 0) {
          foreach ($catinfos as $cat_ress) {
            if (!in_array($cat_ress, $subcategory_id)) {
              $subcategory_id[] = $cat_ress;
            }
          }
        }
        if (count($category_id) > 0 && count($subcategory_id) > 0) {
          $pg_ids = array_unique(array_merge($category_id, $subcategory_id));
        }
        else if (count($category_id) > 0) {
          $pg_ids = $category_id;
        }
        else if (count($subcategory_id) > 0) {
          $pg_ids = $subcategory_id;
        }
        if (count($pg_ids) > 0) {
          foreach ($pg_ids as $pg_id) {
            $cat_list = MP\FormoneBigFormatQuery::create()
              ->filterByProductgroupId($pg_id)
              ->select(array('FormId'))
              ->find();
            foreach ($cat_list as $cat_data) {
              if (!in_array($cat_data, $category_fr_id)) {
                $category_fr_id[] = $cat_data;
              }
            }
          }
        }
        if (count($category_fr_id) > 0 && count($new_big) > 0) {
          $new_bigs = array_unique(array_merge($category_fr_id, $new_big));
        }
        else if (count($category_fr_id) > 0) {
          $new_bigs = $category_fr_id;
        }
        else if (count($new_big) > 0) {
          $new_bigs = $new_big;
        }
        if (count($new_bigs) > 0) {
          foreach ($new_bigs as $forms) {
            $myresults = MP\FormoneBigFormatQuery::create()
              ->filterByFormId($forms)
              ->filterByStatus(1)
              ->useFormoneLangQuery('fl')
              ->filterByLanguageId($lang_name)
              ->filterByFormId($forms)
              ->orderByFormId()
              ->endUse()
              ->useFormoneAdditionalInfoQuery('fbf')
              ->filterByFormId($forms)
              //->filterByWebsiteId($lang_name)
              ->orderByFormId()
              ->endUse()
              ->select(array(
                'FormId',
                'ProductgroupId',
                'SubcategoryId',
                'fbf.InHomePageFlag',
                'fbf.PromoFlag',
                'fbf.CreatedDate',
                'fbf.Image',
                'fbf.ImageListing',
                'BasePriceForQuantity',
                'fl.Title',
                'fl.Description',
                'fl.Name',
                'fl.Slug'
              ))
              ->find();
            if (count($myresults) > 0) {
              foreach ($myresults as $resultpage) {
                
                if($resultpage['fbf.ImageListing'] != 0 && $resultpage['fbf.ImageListing'] != ""){
                  $ImgHome = file_load($resultpage['fbf.ImageListing'])->uri;
                }else{
                  $ImgHome = file_load($resultpage['fbf.Image'])->uri;
                }
                                
                //$image = $resultpage['fbf.Image'];
                //$uuu = file_load($image)->uri;
                $image_path = image_style_url("products-listing", $ImgHome);
                
                $promo_flag = $resultpage['fbf.PromoFlag'];                
                
                $tmp_subslug = MP\ProductsubgroupLangQuery::create()->filterByProductsubgroupId($resultpage['SubcategoryId'])->filterByLanguageId($lang_name)->findOne();              
              $tmp_proslug = MP\ProductsgroupLangQuery::create()->filterByProductGroupId($resultpage['ProductgroupId'])->filterByLanguageId($lang_name)->findOne();
              
                $prod_slug = $resultpage['fl.Slug'];
                $subslug = $tmp_subslug->getSlug();
                $pro_slug = $tmp_proslug->getSlug();

                
                $url_product = $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $resultpage['FormId'] ;
                
                //$name = $resultpage['fl.Name'];
                $name = $resultpage['fl.Title'];
                $description = TagliaStringa($resultpage['fl.Description'], 125, true);
                
                
                ?>
                
      
                <div class="col-sm-4 col-md-4 ">
                      <a href="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $resultpage['FormId']; ?>">
                        <div class=" products-Thumbnail">
                          <div class="item item-type-line">
                            <span class="item-hover">
                              <div class="item-info">
                                <div class="date"><?php echo $name; ?></div>
                                <div class="line"></div>
                                <div class="date"><?php echo $description; ?></div>
                              </div>
                              <div class="mask"></div>
                            </span>

                            <div class="item-img">
                                <img class="img-responsive" alt="" src="<?php echo $image_path; ?>"></div>
                            </div>
                            <?php if ($promo_flag == 'Promo') { ?>
                              <div class="products-title"><em><?php echo $promo_flag; ?></em></div>
                            <?php } ?>
                            <?php if ($promo_flag == 'Best Price') { ?>
                              <div class="products-title2"><em><?php echo $promo_flag; ?></em></div>
                            <?php } ?>
                            <?php if ($promo_flag == 'New') { ?>
                              <div class="products-title1"><em><?php echo $promo_flag; ?></em></div>
                            <?php } ?>
                            <div class="clr"></div>
                            <h2><?php echo $name; ?></h2>
                        </div>
                      </a>
                    </div>
                <?php
              }
            }
          }
        }
        /* End Big Format */
        /* Search By SmallFormat Material */
        /*if (count($matinfo) > 0) {
          foreach ($matinfo as $mat_res) {
            $mat_pr_list = MP\FormtwoMaterialsQuery::create()
              ->filterByMaterialId($mat_res)
              ->select(array('FormId'))
              ->find();
            foreach ($mat_pr_list as $mat_pr_data) {
              if (!in_array($mat_pr_data, $pinfo_small)) {
                $pinfo_small[] = $mat_pr_data;
              }
            }
          }
        }
        /* Search By SmallFormat ProductName */
        /*$pinfo_small = array();
        $psmallfo = array();
        $new_small = array();
        $myresults_pr_small = MP\FormtwoLangQuery::create()
          ->filterByName('%' . $adv . '%')
          ->_or()
          ->filterByTitle('%' . $adv . '%')
          ->_or()
          ->filterByDescription('%' . $adv . '%')
          ->select(array('FormId'))
          ->filterByStatus(1)
          ->find();
        if (count($myresults_pr_small) > 0) {
          foreach ($myresults_pr_small as $myresults_small_data) {
            if (!in_array($myresults_small_data, $psmallfo)) {
              $psmallfo[] = $myresults_small_data;
            }
          }
        }
        if (count($psmallfo) > 0 && count($pinfo_small) > 0) {
          $new_small = array_unique(array_merge($psmallfo, $pinfo_small));
        }
        else if (count($psmallfo) > 0) {
          $new_small = $psmallfo;
        }
        else if (count($pinfo_small) > 0) {
          $new_small = $pinfo_small;
        }
        $category_fr_id_small = array();
        $new_smalls = array();
        /*if (count($subcategory_id) > 0) {
          foreach ($subcategory_id as $pg_ids) {
            $cat_list_small = MP\FormtwoSmallFormatQuery::create()
              //->filterBySubcategoryId($pg_ids)
              ->filterByStatus(1)
              ->select(array('FormId'))
              ->find();
            foreach ($cat_list_small as $cat_data_small) {
              if (!in_array($cat_data_small, $category_fr_id_small)) {
                $category_fr_id_small[] = $cat_data_small;
              }
            }
          }
        }*/
        //if (count($category_fr_id_small) > 0 && count($new_small) > 0) {
          //$new_smalls = array_unique(array_merge($category_fr_id_small, $new_small));
        //}
        //else if (count($category_fr_id_small) > 0) {
          //$new_smalls = $category_fr_id_small;
        //}
        //else if (count($new_small) > 0) {
          //$new_smalls = $new_small;
        //}
        //if (count($new_smalls) > 0) {
          //foreach ($new_smalls as $formsm) {
            $myresultss = MP\FormtwoSmallFormatQuery::create()
              //->filterByFormId($formsm)
              ->filterByStatus(1)
              ->useFormtwoLangQuery('fl')
              ->filterByName('%' . $adv . '%')
              ->_or()
              ->filterByTitle('%' . $adv . '%')
              ->_or()
              ->filterByDescription('%' . $adv . '%')
              
              ->filterByLanguageId($lang_name)
              //->filterByFormId($formsm)
              //->orderByFormId()
              ->endUse()
              ->useFormtwoAdditionalInfoQuery('fbf')
              //->filterByFormId($formsm)
              //->filterByWebsiteId($lang_name)
              //->orderByFormId()
              ->endUse()
              ->select(array(
                'FormId',
                'fbf.InHomePageFlag',
                'fbf.PromoFlag',
                'fbf.CreatedDate',
                'fbf.Image',
                'BasePriceForQuantity',
                'fl.Title',
                'fl.Description',
                'fl.Name',
                'fl.Slug'
              ))
              ->find();
            if (count($myresultss) > 0) {
              foreach ($myresultss as $resultpages) {
                //print_r( $resultpages );
                $image = $resultpages['fbf.Image'];
                $uuu = file_load($image)->uri;
                $image_path = image_style_url("products-listing", $uuu);
                
                    
                $name_slug = $resultpages['fl.Slug'];

                $url_product = $base_url . '/' . $lang_name .'/piccolo-formato/'. $name_slug .  "/ps-" . $resultpages['FormId'];
                
                $name = $resultpages['fl.Name'];
                $description = TagliaStringa($resultpages['fl.Description'], 125, true);
                
                $promo_flag = $resultpages['fbf.PromoFlag'];
               
                ?>
                <div class="col-sm-4 col-md-4 single-search">
                  <div class="products-Thumbnail">
                    <div class="item item-type-line">
                      <a href="<?php echo $url_product; ?>" class="item-hover">
                        <div class="item-info">
                          <div class="title"><?php echo $name; ?></div>
                          <div class="line"></div>
<!--                          <div class="description"><?php #echo $resultpages['fl.Description']; ?></div>-->
                          <div class="date"><?php echo $description; ?></div>
                        </div>
                        <div class="mask"></div>
                      </a>

                      <div class="item-img"><img class="img-responsive" src="<?php echo $image_path; ?>"></div>
                    </div>
                    <?php if (strtolower($promo_flag) == 'promo') { ?>
                      <div class="products-title"><em><?php echo t('Promo'); ?></em></div>
                    <?php } ?>
                    <?php if (strtolower($promo_flag) == 'best price') { ?>
                      <div class="products-title2"><em><?php echo t('Best Price'); ?></em></div>
                    <?php } ?>
                    <?php if (strtolower($promo_flag) == 'new') { ?>
                      <div class="products-title1"><em><?php echo t('New'); ?></em></div>
                    <?php } ?>
                    <div class="clr"></div>
                    <p><?php echo $resultpages['fl.Name']; ?></p>

                    <p class="p">
                      <small>&euro; </small><?php echo $resultpages['BasePriceForQuantity']; ?>
                    </p>
                  </div>
                </div>
                <?php
              }
            }
          //}
        //}
        ?>
      <?php } ?>
    </div>
  </article>
</section>

<?php
include('footer.tpl.php');
?>
<script>
  /*$(document).ready(function(){
   alert("gfdgfdg");
   $("#search").keyup(function() {

   });
   }); */

</script>