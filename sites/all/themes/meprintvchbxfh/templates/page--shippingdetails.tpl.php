<?php global $base_url; ?>
<script type="text/javascript" src="<?php  echo $base_url;  ?>/sites/all/themes/meprint/scripts/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function() { 
    
     $("#shipping-form").validate({
           rules: { 
               fnames:{
                    required: true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/namevalidation.php", async: false }
               },
               emails:{
                    required: true,
                    email :true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/emailvalidation.php", async: false }
                    
               },
               phone:{
                   required: true,
                   number :true,
               },
               address:{
                    required: true
               },
               zipcode:{
                    required: true
            
               },
               location:{
                    required: true
               },
               provience :{
                    required: true
               },
               country : {
                   required: true
               },
               saddress : {
                   required: true
               },
               szipcode:{
                   required: true
               },slocation:{
                   required: true
               },sprovience :{
                   required: true
               },
               scountry : {
                   required: true
               },
                       pay_options:{ 
                           required: true
                       }
             
           },
            messages: {
               fnames:{
                    required: "Please Enter Name",
                    remote: "Sorry Username Already Existed",
               },
               emails:{
                    required: "Please Enter Email",
                    email :"Please Enter valid Email",
                    remote :"Sorry Email Already Existed"
               },
               phone:{
                   required: "Please Enter Phone",
                   number :"Please Enter Numbers only"
               },
               address:{
                    required: "Please Enter Addresss"
               },
               zipcode:{
                    required: "Please Enter Zipcode"
               },
               location:{
                    required: "Please Enter Location"
               },
               provience :{
                    required: "Please Enter Provience"
               },
               country : {
                   required: "Please Enter Country"
               },
               saddress : {
                   required: "Please Enter Address"
               },
               szipcode:{
                   required: "Please Enter Zipcode"
               },
               slocation:{
                   required: "Please Enter Location"
               },
               sprovience :{
                   required: "Please Enter Provience"
               },
               scountry : {
                   required: "Please Enter Country"
               },
                pay_options:{ 
                           required: "Please Select Payment Type"
                       }
            }
     });
     
});
function Fillshipping(f) {
  if(f.billingtoo.checked === true) {
      
    f.saddress.value =f.address.value; 
    f.szipcode.value  = f.zipcode.value; 
    f.slocation.value = f.location.value;
    f.sprovience.value = f.provience.value;
    f.scountry.value = f.country.value;
   
  }
    if(f.billingtoo.checked === false) {
       
    f.saddress.value = '';
    f.szipcode.value = '';
    f.slocation.value = '';
    f.sprovience.value = '';
    f.scountry.value = '';
     
  }
  }
 
</script>

<?php 

global $base_path;
global $language;
$lang_name = $language->language;
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';

?>

<?php
  include('header.tpl.php');
?>


<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">

<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): echo '<a href="'.$base_url.'">'. $Home .'</a>'.' / '. $bshipping; endif;?>  
  
      <div class="space-2"></div>
     <div class="cartnav">
          <div class="cartlable shiplable">
              <span class="inactive"> <?php echo $cartpagetittle;?></span>
          </div>
          <div class="cartlable shiplable">
              <span class="shiplable active">
              <?php echo $shippinginfo;?></span></div>
      <div class="ordernav">
              <span class="shiplable inactive">
              <?php echo $orderconfirm;?></span></div>
      </div>

  </div>
   <div class="space-3"></div>
   
   
            <?php 

         global $user;
          session_start();
          $sessionid = session_id();
          
         $name = '';
         $email = '';
         if($user->uid !=0) { 
           
            
            $query = "SELECT * FROM {users} WHERE uid = :uid ";
            $project = db_query($query, array(':uid' => $user->uid))
                       ->fetchObject();
             
             $name = $project->name;
             $mail = $project->mail; 
             
             
            $phone = db_query("SELECT field_phone_value FROM  field_data_field_phone WHERE entity_id =  $user->uid ")
                     ->fetchField();
            $zipcode = db_query("SELECT  field_zip_code_value  FROM  field_data_field_zip_code WHERE entity_id =  $user->uid ")
                     ->fetchField();
            $province = db_query("SELECT  field_province_value  FROM  field_data_field_province WHERE entity_id =  $user->uid ")
                     ->fetchField(); 
            $location = db_query("SELECT  field_location_value  FROM  field_data_field_location WHERE entity_id =  $user->uid ")
                     ->fetchField();
         }
            
           if(isset($_POST['proceedtoshipping']) && $_POST['proceedtoshipping']!='') {
         ?>
   <form method="POST" id="shipping-form" action="<?php echo $base_url.'/'.$lang_name;?>/orderconfirm" autocomplete="on" class="boxed">
        <div class="legend"> <span><?php echo $pdetails; ?></span></div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-md-6">
            
            <lable class="shiplabel"><?php echo $lname; ?></lable> 
            <?php if($name !='') { ?>
            <input type="text" name="fname" id="fname" class="form-text shipform" value="<?php  echo $name; ?>" readonly="readonly"/>
            <?php } else { ?>
             <input type="text" name="fnames" id="fname" class="form-text shipform" value=""/>
           <?php
                
            } ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <lable class="shiplabel"><?php echo $lemail; ?> </lable>
           <?php  if($mail !='') { ?>
            <input type="text" name="email" id="email" class="form-text shipform" value="<?php  echo  $mail;  ?>" readonly="readonly"/>
           <?php } else {  ?>
              <input type="text" name="emails" id="email" class="form-text shipform"/>
           <?php } ?>
        </div>
        <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo  $lphone; ?></lable><input type="text" name="phone" id="phone" class="form-text shipform" value="<?php if($phone !='') { echo  $phone; } ?>"/></div>
        <div class="clearfix"></div>
             <div class="space-2"></div>
        
        <div class="legend "><span><?php echo $billing; ?></span></div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $laddress;?></lable>  <textarea name="address" id="address" class="form-text shipform"><?php if($address !='') { echo $address; } ?></textarea></div>
            <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lzipcode;?></lable> <input type="text" name="zipcode" id="zipcode" class="form-text shipform" value="<?php if($zipcode !='') { echo $zipcode; } ?>"/></div>
       
                <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $llocation;?></lable> <input type="text" name="location" id="location" class="form-text shipform" value="<?php if($location !='') { echo $location; } ?>" /></div>
                   
                <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lprovince;?></lable>  <input type="text" name="provience" id="provience" class="form-text shipform" value="<?php if($province !='') { echo $province; } ?>"/></div>
     <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lcontry;?></lable>  <input type="text" name="country" id="country" class="form-text shipform" value=""/></div> 
                <div class="clearfix"></div>
             <div class="space-2"></div>
                <div id="shipdetailsform" style="overflow: hidden">
                    <div class="legend "><span ><?php $shipping; ?></span> </div> 
                    <div class="col-sm-12 col-md-12">
                         <input type = "checkbox" name="billingtoo"   onclick="Fillshipping(this.form)" /> <span ><?php echo $sameasbill; ?></span></div>
                       <div class="clearfix"></div>
                    <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $laddress;?></lable>  <textarea name="saddress" id="saddress" class="form-text shipform"><?php if($address !='') { echo $address; } ?></textarea></div>
            <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lzipcode;?></lable> <input type="text" name="szipcode" id="szipcode" class="form-text shipform" value="<?php if($zipcode !='') { echo $zipcode; } ?>"/></div>
       
                <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $llocation;?></lable> <input type="text" name="slocation" id="slocation" class="form-text shipform" value="<?php if($location !='') { echo $location; } ?>" /></div>
                    <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lprovince;?></lable>  <input type="text" name="sprovience" id="sprovience"  class="form-text shipform" value="<?php if($province !='') { echo $province; } ?>"/></div>
                    
                       <div class="col-sm-6 col-md-6"><lable class="shiplabel"><?php echo $lcontry;?></lable>  <input type="text" name="scountry" id="scountry" class="form-text shipform" value=""/></div> 
                </div>
                       <div class="clearfix"></div>
             <div class="space-2"></div>
                         <div class="legend "> <span><?php echo $paymentoptions; ?></span></div>
                         <div class="col-sm-12 col-md-12"><div class="col-sm-2 col-md-2">
                                 <lable ><b><?php echo  $paymenttype; ?></b></lable></div>
                                     <div class="col-sm-5 col-md-5"><select name="pay_options" class="form-select" id="pay_options">
                                          <option value=""><?php echo  $paymenttype; ?></option>
                                         <option value="wire"><?php echo  $p1; ?></option>
                                          <option value="cash"><?php echo  $p2; ?></option>
                                           <option value="paypal"><?php echo $p3; ?></option>
                                         </select></div> </div>
                         <div class="space-2"></div>
                         <div class="clearfix"></div>
                         
                                
       <input type="hidden" name="productprice" value="<?php echo $_POST['productprice']; ?>"/>
       <input type="hidden" name="dicountamount" value="<?php echo $_POST['dicountamount']; ?>"/>
       <input type="hidden" name="finalamount" value="<?php echo $_POST['finalamount']; ?>"/>
       <input type="hidden" name="coupon_code" value="<?php echo $_POST['coupancode']; ?>"/>
       <input type="hidden" name="discount_id" value="<?php echo $_POST['discount_id']; ?>"/>
       <input type="hidden" name="shipping" value="<?php echo $_POST['shipping']; ?>"/>
         <div class="col-sm-12 col-md-12 shipbutton">
             <input class="form-submit" type="submit" name="Shipping_Confirm"  value="Proceed"></div>
       
           </form><?php } else {
               echo '<h4>'. $noproductsavail .'</h4>';
           }?>
             
  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>
