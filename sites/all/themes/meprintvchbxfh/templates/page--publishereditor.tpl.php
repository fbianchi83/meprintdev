<?php
?>
<!-- input type="file" id="fileupload" style="display:none"/ -->


<div id="history_trick" style="display:none"></div>
<div id="container" class="row">
    <div id="layerBrowser" class="col-sm-3 nopadding-right">
        <ul>
            <li class="fieldTabControl" title='<?php echo t("tools"); ?>'>
                <a href="#tabs-pages">
                    <span class="fa-stack">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                        <i class="fa fa-edit fa-stack-1x"></i>
                    </span>
                </a>
            </li>
            <!--li class="imageLibraryTabControl">
                <a href="#tabs-lbImageLibrary" title='<?php echo t("pictures"); ?>'>
                    <span class="fa-stack">
                        <i class="photostack"></i>
                        <i class="photostack two"></i>
                        <i class="fa fa-picture-o fa-stack-2x"></i>

                    </span>
                </a>
            </li-->
        </ul>
        <div id="tabs-pages">
            <div id="layers">
            </div>
        </div>
        <!--div id="tabs-lbImageLibrary">
        </div-->
    </div>
    <div id="mainSection" class="col-sm-9">
        <div id="controlBar" class="row">
            <div id="controlBarIn" class="btn-toolbar col-sm-12" role="toolbar">
                <div class="btn-group" role="group">
                    <button class="button tooltip-label" type="button" id="add_image" data-tooltip="<?php echo t("Load image"); ?>">
                        <span class="fa fa-plus fa-1x"></span>
                        <span class="fa fa-camera fa-2x"></span>
                    </button>
                    <button class="button tooltip-label" type="button" id="add_text" data-tooltip="<?php echo t("insert text"); ?>">
                        <span class="fa fa-plus fa-1x"></span>
                        <span class="fa fa-font fa-2x"></span>
                    </button>
                    <button class="button tooltip-label" type="button" id="add_shape" data-tooltip="<?php echo t("insert shape"); ?>">
                        <span class="fa fa-plus fa-1x"></span>
                        <span class="fa fa-square-o fa-2x"></span>
                    </button>
                    <button class="button tooltip-label" type="button" id="add_circle" data-tooltip="<?php echo t("insert circle"); ?>">
                        <span class="fa fa-plus fa-1x"></span>
                        <span class="fa fa-circle-thin fa-2x"></span>
                    </button>
                </div>
                <div class="btn-group" role="group">    
                    <button class="button tooltip-label" type="button" id="undo" data-tooltip='<?php echo t("undo"); ?>'>
                        <span class="fa fa-reply fa-2x"></span>
                    </button>
                    <button class="button tooltip-label" type="button" id="redo" data-tooltip='<?php echo t("redo"); ?>'>
                        <span class="fa fa-share fa-2x"></span>
                    </button>
                </div>
                <div class="btn-group" role="group">    
                    <button class="button tooltip-label" type="button" id="preview_immagine" data-tooltip="<?php echo t("image preview"); ?>">
                        <span class="fa fa-picture-o fa-2x"></span>
                    </button>   
                    <input type="checkbox" id="resize_ratio">
                    <label id="resize_ratio_lbl" class="tooltip-label" for="resize_ratio" data-tooltip="<?php echo t("deform selected object"); ?>">
                        <span class="resize-image-icon"></span>
                    </label>
                    <button class="button tooltip-label" type="button" id="zorder_down" data-tooltip="<?php echo t("move layer behind"); ?>">
                        <span class="fa fa-long-arrow-down fa-2x"></span>
                    </button>
                    <button class="button tooltip-label" type="button" id="zorder_up" data-tooltip="<?php echo t("move layer forward"); ?>">
                        <span class="fa fa-long-arrow-up fa-2x"></span>
                    </button>
                    <input type="checkbox" id="grid_object" />
                    <label class="tooltip-label" id="grid_object_lbl" for="grid_object" data-tooltip="<?php echo t("view cut grid"); ?>">
                        <span class="cutting-image-icon"></span>
                    </label>
                    
                    <input type="checkbox" id="ruler_object" />
                    <label class="tooltip-label" id="ruler_object_lbl" for="ruler_object" data-tooltip="<?php echo t("view ruler"); ?>">
                        <span class="ruler-image-icon"></span>
                    </label>
                    
                    <!--button class="button" type="button" id="grid_object" title="<?php echo t("view cut grid"); ?>">
                        <span class="fa fa-map-o fa-2x"></span>
                    </button-->
                    <button class="button tooltip-label" type="button" id="remove_object" data-tooltip="<?php echo t("remove selected object"); ?>">
                        <span class="fa fa-trash-o fa-2x"></span>
                    </button>
                    <!--button class="button disabled" type="button" id="crop_object" title="Crop dell'immagine">
                        <span class="fa fa-crop fa-2x"></span>
                    </button-->
                </div>
                <!--div id="zoom-group" class="btn-group" role="group">
                    <!--span id="zoom-icon" class="fa fa-search fa-2x"></span-->
                    <!--strong class="zoom-label" >Zoom: </strong>
                    <select name="zoom" id="zoom" title="Zoom">
                        <option value="fitPage" selected="selected"><?php echo t('fit page'); ?></option>
                        <option value="fitHeight"><?php echo t('fit height'); ?></option>
                        <option value="fitWidth"><?php echo t('fit width'); ?></option>
                        <option value="0.01">1%</option>
                        <option value="0.03">3%</option>
                        <option value="0.05">5%</option>
                        <option value="0.1">10%</option>
                        <option value="0.25">25%</option>
                        <option value="0.5">50%</option>
                        <option value="0.75">75%</option>
                        <option value="1">100%</option>
                        <option value="1.25">125%</option>
                        <option value="1.5">150%</option>
                        <option value="1.75">175%</option>
                        <option value="2">200%</option>
                    </select>
                    
                    <input id="zoom_spinner" name="zoom_spinner" value="">
                </div-->
                <!--div class="btn-group" role="group">
                    <button class="button button-ok" aria-haspopup="true" type="button">
                        <span class="fa fa-fw fa-check fa-2x"></span>
                        <strong>Approve / Checkout</strong>
                    </button>
                </div-->
                <!--div class="btn-group" role="group">
                    <button id="button-cancel" class="button button-cancel" aria-haspopup="true" type="button">
                        <span class="fa fa-times fa-2x"></span>
                    </button>
                </div-->
            </div>
        </div>
        <div id="workcanvas">
            <ul id="pageList">
                <li id="liPage-1" class="liPage current-page" style="display: block;">
                    <div id="selection_box" style="display:none"></div>
                    <div id="crop_box" style="display:none">
                        <div id="crop_selection"></div>
                        <div id="cropper-n" class="cropper-overlay"></div>
                        <div id="cropper-s" class="cropper-overlay"></div>
                        <div id="cropper-w" class="cropper-overlay"></div>
                        <div id="cropper-e" class="cropper-overlay"></div>
                    </div>
                    <div id="cutting_box">
                        
                    </div>
                    <div id="page-ruler">
                        
                    </div>
                    <div id="page-1" class="page">
                        <div class="pagina-taglio">
                            <div class="pagina-sicurezza">
                            </div>
                        </div>
                    </div>
                </li>
                <li id="liPage-2" class="liPage" style="display: none;">
                    <div id="page-2" class="page">
                        <div class="pagina-taglio">
                            <div class="pagina-sicurezza">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div id="pageBrowser" class="row">
            <div id="pageBrowserIn" class="col-sm-12">
                <div id="pageBrowserControlBar" class="row">
                    <div id="pageBrowserBarLeft" class="col-sm-6">
                        <strong class="dimension-label" ><?php echo t('Page sizes'); ?>: </strong>
                        <span id="mesurePage">
                            0 cmm x 0 cmm
                        </span>
                    </div>
                    <div id="pageBrowserBarRight" class="col-sm-6">
                        <div id="zoom-group" class="btn-group" role="group">
                            <!--span id="zoom-icon" class="fa fa-search fa-2x"></span-->
                            <strong class="zoom-label" >Zoom: </strong>
                            <select name="zoom" id="zoom" title="Zoom">
                                <option value="fitPage" selected="selected"><?php echo t('fit page'); ?></option>
                                <option value="fitHeight"><?php echo t('fit height'); ?></option>
                                <option value="fitWidth"><?php echo t('fit width'); ?></option>
                                <option value="0.01">1%</option>
                                <option value="0.03">3%</option>
                                <option value="0.05">5%</option>
                                <option value="0.1">10%</option>
                                <option value="0.25">25%</option>
                                <option value="0.5">50%</option>
                                <option value="0.75">75%</option>
                                <option value="1">100%</option>
                                <option value="1.25">125%</option>
                                <option value="1.5">150%</option>
                                <option value="1.75">175%</option>
                                <option value="2">200%</option>
                            </select>

                            <input id="zoom_spinner" name="zoom_spinner" value="">
                        </div>
                    </div>
                </div>
                <div id="pageBrowserPagerAreaWrap" class="row">
                    <div id="pageBrowserPagerAreaIn" class="col-sm-12" style="overflow-x: hidden;">
                        <ul id="pageBrowserPagerAreaList" style="width: 100%;">
                            <li class="pbPager pbPager-1 current-pbpage">
                                <div id="pageBrowserPager-1" class="pager" data-page-number="1">
                                    <div class="pbImageWrap">
                                        <img height="53.32178896663268" width="75" data-pagetext="Page" alt="Page 1" title="Page 1" src="">
                                    </div>
                                    <p>Page 1</p>
                                </div>
                            </li>
                            <li class="pbPager pbPager-2">
                                <div id="pageBrowserPager-2" class="pager" data-page-number="2">
                                    <div class="pbImageWrap">
                                        <img height="53.32178896663268" width="75" data-pagetext="Page" alt="Page 2" title="Page 2" src="">
                                    </div>
                                    <p>Page 2</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<canvas id="template" style="display: none"></canvas>
<div id="fileupload" class="fileuploader" style="display: none"></div>
<div id="progress_bar_screen" style='bottom: 0px;left: 0px;outline: 0 none;overflow: hidden;position: fixed;right: 0px;top: 0px;z-index: 9999999990;overflow-x: hidden;overflow-y: auto;background-color: rgba(0,0,0,0.2);'>&nbsp;</div>
<div id="progress_bar_editor" style="z-index: 999999999999999;margin: 0px; padding: 0px; list-style-type: none;border: 2px solid #fff;position: fixed;background-color: rgba(255,255,255,0.8);"></div>