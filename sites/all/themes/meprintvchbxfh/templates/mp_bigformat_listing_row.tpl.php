<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if (isset($display_submitted)): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    ?>

    <!--CONTENUTO NODO-->

    <?php  
    
    global $base_url;
    global $language;
    global $website;
    
    $noproduct = t('No Products Avalible');
    $sortbycat = t('Select Sub Category');
    $selsubcat = t('All');
    $noproduct = t('No products Available');
    
    $count=0;
    $page = isset($_GET['page'])?$_GET['page']:0;
    
    $grid = isset($_GET['grid'])?$_GET['grid']:0;
    
    $prod_id = $id_sub;
    $lang_name = $language->language;
    $website = $language->language;
    $subgrp = MP\ProductsubgroupLangQuery::create()
                    ->where('ProductsubgroupLang.LanguageId =?', $lang_name)
                    ->where('ProductsubgroupLang.ProductsubgroupId =?', $prod_id)
                    ->where('ProductsubgroupLang.Status =?', 1)
                    ->findOne();
    if($subgrp){
        $subname = $subgrp->getName();
        $subdesc = $subgrp->getDescription();
        $subslug = $subgrp->getSlug();
        
        /*
         * INIZIO SEO
         */
        //IMPOSTO IL TITOLO SEO
        $title_seo = $subgrp->getTitleSeo();
        drupal_set_title($title_seo);

        //IMPOSTO LA DESCRIZIONE SEO
        $description_seo = $subgrp->getDescriptionSeo();
        $data = array(
          '#tag' => 'meta',
          '#attributes' => array(
             'name' => 'description',
             'content' => $description_seo,
          ),
        );
        drupal_add_html_head($data, 'big_format');
        /*
         * FINE SEO
         */
        
        
        $subpgrp = MP\ProductsgroupLangQuery::create()
                        ->where('ProductsgroupLang.LanguageId =?', $lang_name)
                        ->where('ProductsgroupLang.ProductgroupId =?', $subgrp->getProductgroupId())
                        ->where('ProductsgroupLang.Status =?', 1)
                        ->findOne();
          $pro_name=$subpgrp->getName();
          $pro_slug = $subpgrp->getSlug();
          $pro_description=$subpgrp->getDescription();
    }
    ?>
    <header>

        <?php if(isset($subname) && isset($subdesc)){?>
          <h1 class="section-title"><?php echo $subname; ?>
              <span>
                <img alt="image" src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" style="margin-left: 15px">
              </span>
          </h1>
          <p><?php echo $subdesc; ?></p>
        <?php }?>
    </header>
    <div class="well well-lg list-filter">
      <div class="pull-right list-selector">
        <!--<span><a href="allproductslist?<?php echo $prod_id; ?>"><i class="fa fa-align-justify fa-2x list-fa-th-list"></i></a></span>&nbsp;-->
        <span class="hidden-xs"><a href="?layout=block"><i class="fa fa-th fa-2x list-fa-th-list"></i></a></span>&nbsp;
      </div>
      <form role="form" class="form-inline">
        <div class=" form-group">
          <label for="formGroupInputSmall" class="list-filter__label control-label"><?php echo $sortbycat; ?></label>

          <select class="form-control input-sm" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
            <option value=""><?php echo $subname; ?></option>
            <?php
            if ($subgrp) {
              $subcategories = MP\ProductsubgroupLangQuery::create()
                                ->filterByProductgroupId($subgrp->getProductgroupId())
                                ->filterByLanguageId($lang_name)
                                ->filterByProductsubgroupId($subgrp->getProductsubgroupId(), \Propel\Runtime\ActiveQuery\Criteria::NOT_EQUAL)
                                ->find();
              foreach ($subcategories as $subcat) {
                  $this_sub_slug = $subcat->getSlug();
                  
                  $count = MP\FormoneBigFormatQuery::create()
                            ->filterBySubcategoryId($subcat->getProductsubgroupId())
                            ->filterByStatus(1)
                            ->count();
                  
                  if($count>0){
                  
                ?>
                    <option value="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . '/' . $this_sub_slug . '/l-' . $subcat->getProductsubgroupId(); ?>"><?php echo $subcat->getName(); ?></option>
                <?php
                  }
              }
            } 
            ?>
          </select>
        </div>
      </form>
    </div>
      <section id="listing-layout-row" class="">
        <?php  
        $limit=9;

        $listing_query = MP\FormoneBigFormatQuery::create()
                            ->filterBySubcategoryId($prod_id)
                            ->filterByStatus(1)
                            ->orderByModifiedDate("DESC")
                            ->find();
        
        if(count($listing_query )>0){
            
            $r = 0;
            $p = 0;

            foreach($listing_query as $listing){

                $id = $listing->getFormId();
                $check_lang = MP\FormoneLangQuery::create()
                    ->where('FormoneLang.LanguageId =?', $lang_name)
                    ->where('FormoneLang.FormId =?', $id)
                    ->findOne();

                //$name = substr($check_lang->getName(), 0, 20);
                $name = $check_lang->getName();
                $prod_slug = $check_lang->getSlug();
                $description = substr($check_lang->getDescription(), 0, 125);
                $date = $listing->getCreatedDate();
                $baseprice = $listing->getBasePriceForQuantity();

                $check = MP\FormoneAdditionalInfoQuery::create()
                        //->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                        ->where('FormoneAdditionalInfo.FormId =?', $id)
                        ->findOne();

                $count++;
                $r++;
                
                $mypage = $page * $limit;
                $lastrec = $mypage + $limit;
                if ($r >= $mypage && $r <= ($mypage + $limit)) {
                    $p++;
                    $promo = $check->getPromoFlag();
                    $promoprice = $check->getPromotionPrice();
                    $promotype = $check->getPromoPriceType();
                    
                    if($check->getImageListing() != 0 && $check->getImageListing() != ""){
                      $ImgHome = file_load($check->getImageListing());
                    }else{
                      $ImgHome = file_load($check->getImage());
                    }
                    
                    //$ImgHome = file_load($check->getImage());
                    $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
                    
                    
                    ?>
          
                    <?php
                        if($grid == 0){
                          $link_detail = $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/" . $prod_slug . "/pb-" . $id ;
                    ?>
                    <div class="single-row clearfix">
                      <a href="<?php echo $link_detail; ?>">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                            <?php if ($promo == 'Promo') { ?>
                              <div class="products-title"><em><?php echo $promo; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'Best Price') { ?>
                              <div class="products-title2"><em><?php echo $bestprice; ?></em></div>
                            <?php } ?>
                            <?php if ($promo == 'New') { ?>
                              <div class="products-title1"><em><?php echo $new; ?></em></div>
                            <?php } ?>
                              
                              <img class="img-responsive" alt="" src="<?php echo $ImgHomePath; ?>" />                            
                          </div>
                          
                          <div class="col-sm-12 col-md-7">
                            <p class="products-title-row"><?php echo $name; ?></p>
                            <div class="line"></div>
                            <div class="date"><?php echo $description; ?></div>
                          </div>
                            
                          <?php
                          if ($promo == 'Promo') {
                              $actualprice = $baseprice;
                              if ($promotype == "value") {
                                  $baseprice = $baseprice - $promoprice;
                              } elseif ($promotype == "percentage") {
                                  $promodisc = $promoprice * $baseprice;
                                  $promodisc = $promodisc / 100;
                                  $baseprice = $baseprice - $promodisc;
                              }
                          ?>
                            <p class="product_price col-sm-12 col-md-2 text-center"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
                          <?php }
                          else {
                          ?>
                            <p class="product_price col-sm-12 col-md-2 text-center">&euro; <?php echo $baseprice; ?></p>
                          <?php } ?>
                        </div>
                      </a>
                    </div>
                    
                    <?php
                        }elseif ($grid == 1) {
                            $price = $listing->getBasePriceForQuantity();
                            $image = $check->getImage();
                            $uuu=file_load($image)->uri;
                            $my_image=explode("://",$uuu);
                    ?>
                            <div class="col-md-12 col-sm-12 ">
                                <tr>
                                    <td class="col-sm-1 col-md-1  grid_list"><?php echo $r; ?></td>
                                    <td class="col-sm-4 col-md-4  grid_list"><a href="<?php echo $base_url.'/'.$lang_name.'/product-three-column?'.$id.'&type=Big';?>" ><?php echo $name; ?></a></td>
                                    <td class="col-sm-4 col-md-4  grid_list"><?php echo $description; ?></td>
                                    <td class="col-sm-4 col-md-4  grid_list"><a target="_blank" href="<?php echo $base_url.'/'.$lang_name.'/product-three-column?'.$id.'&type=Big';?>" ><img alt="" src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="50px" height="50px"></a></td>
                                    <td class="col-sm-4 col-md-4  grid_list">&euro; <?php echo $price; ?></td>
                                </tr>
                            </div> 
                    <?php
                        }
                    ?>
          
                    <?php
                }
            }  
            
            if($p<=0){?>
                <div class="col-md-12 col-sm-12 well well-lg">
                  <?php echo $noproduct;?>
                </div>
            <?php
            }
            ?>
      </section>
  <?php }else{ ?>
            <div class="col-md-12 col-sm-12 well well-lg">
              <?php echo $noproduct;?>
            </div>
  <?php } ?>
    <footer class="row">
      <div class="col-sm-12 col-md-12">
        <div class="col-sm-3 col-md-3">
          <nav>
          </nav>
        </div>
        <div class="col-sm-9 col-md-9 text-center">
          <nav>
            <?php 
                $pagcnt = round($count/$limit);
            ?>
            <ul class="pagination">
              <?php 
                for($g=1;$g<=$pagcnt;$g++){
                    $f = $g -1;
                    $class='';
                    if($f==$page){
                        $class='active';
                    }
                    if($g==1){?>
                      <li class="<?php echo $class;?>"><a href="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/l-" . $prod_id; ?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                    <?php
                    }else{
                      ?>
                      <li class="<?php echo $class;?>"><a href="<?php echo $base_url . '/'. $lang_name . '/' . $pro_slug . "/" . $subslug . "/l-" . $prod_id; ?>?page=<?php echo $f;?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                    <?php        
                    } 
                }
              ?>
            </ul>
          </nav>
        </div>
        <nav>
        </nav>
      </div>
  </footer>

  <!--FINE CONTENUTO NODO-->
</div>

<?php
// Remove the "Add new comment" link on the teaser page or if the comment
// form is being displayed on the same page.
if ($teaser || !empty($content['comments']['comment_form'])) {
  unset($content['links']['comment']['#links']['comment-add']);
}
// Only display the wrapper div if there are links.
$links = render($content['links']);
if ($links):
  ?>
  <div class="link-wrapper">
    <?php print $links; ?>
  </div>
<?php endif; ?>

<?php print render($content['comments']); ?>

</div>