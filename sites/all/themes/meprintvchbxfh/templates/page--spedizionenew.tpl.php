<?php 

global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

drupal_add_js(base_path() . path_to_theme() . '/scripts/jquery.validate.js');
drupal_add_js(base_path() . path_to_theme() . '/scripts/page-spedizione.js');

?>
<?php
  include('header.tpl.php');
?>
<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">

<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): echo '<a href="'.$base_url.'">'. $Home .'</a>'.' / '. $bshipping; endif;?>  
  
      <div class="space-2"></div>
      
      <div class="cartnav clearfix">
        <div class="cartlable hidden-xs">
            <span class="shiplable inactive"> <?php echo $cartpagetittle;?></span>
        </div>
        <div class="shippingnav shiplable">
          <span class="active"><?php echo $shippinginfo;?></span>
        </div>
        <div class="ordernav hidden-xs">
          <span class="shiplable inactive">
          <?php echo $orderconfirm;?></span>
        </div>
      </div>

  </div>
   <div class="space-3"></div>
   
   
            <?php 
              print render($page['content']);
            ?>
             
  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
<code>


</code>
  </section>

<?php
  include('footer.tpl.php');
?>
