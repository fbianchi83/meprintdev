<?php global $base_url; ?>
<ul class="topnav">	
<!--    <li class="selected"><a href="#" >Gestione Ordini</a></li>
    <li ><a href="#" >Gestione Ordini</a>
        <ul>
            <li><a href="<?php //echo $base_url; ?>/it/orders/view">Tutti gli ordini</a></li>  	
            <li><a href="<?php //echo $base_url; ?>/it/orders/current">Ordini aperti</a></li> 
            <li><a href="<?php //echo $base_url; ?>/it/orders/order_discounts">Ordine Sconti</a></li>  	
            <li><a href="<?php //echo $base_url; ?>/it/orders/order_elements">Prodotti Ordinati</a></li>
            <li><a href="<?php //echo $base_url; ?>/it/orders/cancel">Ordini annullati</a></li>  	
        </ul> </li>-->
    <li class="selected"><a href="#">Gestione prodotti</a></li>  	
    <li><a href="#">Categorie</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/productgroup/view">Gestisci categorie</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/productgroup/add">Aggiungi Categoria</a></li>			 
        </ul>
    </li>
    <li><a href="#">Sotto categorie</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/productsubgroup/view">Gestisci Sotto Categorie</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/productsubgroup/add">Aggiungi Sotto Categoria</a></li>			 
        </ul>
    </li>  
    <li><a href="#">Accessori</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/accessories/view">Gestisci Accessori</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/accessories/add">Aggiungi Accessorio</a></li>			 
        </ul>
    </li>  	
    <li><a href="#">Strutture</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/structures/view">Gestisci Strutture</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/structure/add">Aggiungi Struttura</a></li>			 
        </ul>
    </li>  	
<!--    <li class="selected"><a href="#">Gestione Materiali</a></li>  -->
<!--    <li><a href="#">Gruppi di Materiali</a>
        <ul>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/materialgroup/view">Gestisci Gruppi di Materiali</a></li>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/materialgroup/add">Aggiungi Gruppo di Materiali</a></li>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/material/grouping">Raggruppamento Materiali</a></li>			 
        </ul>
    </li>-->
    <li class="selected"><a href="#">Gestione Carico di Lavoro</a></li>        
<!--    <li><a href="#">Tipi di Lavorazione</a>
        <ul>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/processingkind/view">Gestisci Tipi di Lavorazione</a></li>
                  <li><a href="<?php //echo $base_url; ?>/it/tonic/processingkind/add">Aggiungi Tipo di Lavorazione</a></li>			 
        </ul>
    </li>-->
    <li><a href="#">Funzioni Carico di Lavoro</a>
        <ul>
         <!-- <li><a href="<?php //echo $base_url; ?>/it/tonic/weightCalc/view">Gestisci Calcoli peso</a></li>
         <li><a href="<?php //echo $base_url; ?>/it/tonic/weightCalc/add">Aggiungi Calcolo Peso</a></li>	
         <li><a href="<?php //echo $base_url; ?>/it/tonic/priceCalc/view">Gestisci Calcoli Prezzo</a></li>
         <li><a href="<?php //echo $base_url; ?>/it/tonic/priceCalc/add">Aggiungi Calcolo Prezzo</a></li>-->			
            <li><a href="<?php echo $base_url; ?>/it/tonic/holidays/view">Gestisci Vacanze</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/holidays/add">Aggiungi Vacanza</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/longproduct/view">Gestisci Prod. Dilazionate</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/longproduct/add">Aggiungi Prod. Dilazionata</a></li>
        </ul>
    </li>
    <li class="selected"><a href="#">Gestione Gallerie</a></li>  
<!--    <li><a href="#">Templates</a>
        <ul>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/template/view">Gestisci Templates</a></li>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/template/add">Aggiungi Template</a></li>			 
        </ul>
    </li>
    <li><a href="#">Siti Web</a>
        <ul>
            <li><a href="<?php //echo $base_url; ?>/it/tonic/website/view">Gestisci Siti Web</a></li>
                  <li><a href="<?php //echo $base_url; ?>/it/tonic/website/add">Aggiungi Sito Web</a></li>			 
        </ul>
    </li>-->
    <li><a href="#">Gallerie immagini</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/gallery/view">Gestisci Gallerie</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/gallery/add">Aggiungi Galleria</a></li>
        </ul>
    </li>
    <li class="selected"><a href="#">Grande Formato</a></li>        
    <li><a href="#">Lavorazioni</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/processing/view">Gestisci Lavorazioni</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/processing/add">Aggiungi Lavorazione</a></li>								
        </ul>
    </li>
    <li><a href="#">Dimensioni</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/dimensions/view">Gestisci Dimensioni</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/dimensions/add">Aggiungi Dimensione</a></li>							
        </ul>
    </li>
    <li><a href="#">Materiali Grande Formato</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/material/view">Gestisci materiali</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/material/add">Aggiungi Materiale</a></li>			 
        </ul>
    </li>
    <!--<li><a href="#">Modifica Costo imballi</a>
        <ul>

            <li><a href="<? php echo $base_url; ?>/it/tonic/extrapakage/add">Costo imballi</a></li>		 
        </ul>
    </li>-->
    <li><a href="#">Preventivi</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/quote/view">Gestisci Preventivi</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/quote/add">Aggiungi Preventivo</a></li>		 
        </ul>
    </li>	
    <li class="selected"><a href="#">Piccolo Formato</a></li>
    <li><a href="#">Materiali Piccolo Formato</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/material_two/view">Gestisci materiali</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/material_two/add">Aggiungi Materiale</a></li>			 
        </ul>
    </li>
    <li><a href="#">Lavorazioni</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/processing_two/view">Gestisci Lavorazioni</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/processing_two/add">Aggiungi Lavorazione</a></li>								
        </ul>
    </li>
    <li><a href="#">Dimensioni</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/dimensions_two/view">Gestisci Dimensioni</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/dimensions_two/add">Aggiungi Dimensione</a></li>							
        </ul>
    </li>
    <li><a href="#">Grammature</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/weights_two/view">Gestisci Grammature</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/weights_two/add">Aggiungi Grammatura</a></li>							
        </ul>
    </li>
    <li><a href="#">Tipologie di piega</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/folding_type_two/view">Gestisci Tipologie di piega</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/folding_type_two/add">Aggiungi Tipologia di piega</a></li>							
        </ul>
    </li>
    <li><a href="#">Preventivi</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/squote/view">Gestisci Preventivi</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/squote/add">Aggiungi Preventivo</a></li>            		 
        </ul>
    </li>
    <li class="selected"><a href="#">Spedizione</a></li>  
    <li><a href="#">Compagnia Di Spedizione</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/shipping/view">Gestire Shipping Company Api</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/shipping/addapi">Aggiungere Shipping Company Api</a></li>            		 
            <li><a href="<?php echo $base_url; ?>/it/tonic/shipping/addsetting">Impostazioni costo di trasporto</a></li> 
        </ul>
    </li> 	
    <li class="selected"><a href="#">Gestione Promozioni</a></li>        
    <li><a href="#">Sconti</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/discounts/view">Gestisci Sconti</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/discounts/add">Aggiungi Sconto</a></li>								
        </ul>
    <!--<li><a href="#">Tipi di Promozione</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/promotionkind/view">Gestisci Tipi di Promozione</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/promotionkind/add">Aggiungi Tipo di Promozione</a></li>		 
        </ul>
    </li>-->
    <li class="selected"><a href="#">Operatori e gruppi</a></li>        
    <li><a href="#">Operatori</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/operator/view">Gestisci Operatori</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/operator/add">Aggiungi Operatore</a></li>								
        </ul>
    </li>
    <li><a href="#">Gruppi</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/group/view">Gestisci Gruppi</a></li>
            <li><a href="<?php echo $base_url; ?>/it/tonic/group/add">Aggiungi Gruppo</a></li>			
        </ul>
    </li>
    <li class="selected"><a href="#">Utenti</a></li>        
    <li><a href="#">Utenti</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/users/view">Gestisci Utenti</a></li>						
        </ul>
    </li>
    <li><a href="#">Blacklist</a>
        <ul>
            <li><a href="<?php echo $base_url; ?>/it/tonic/users/blacklist">Gestisci Blacklist</a></li>	
        </ul>
    </li>
</ul>