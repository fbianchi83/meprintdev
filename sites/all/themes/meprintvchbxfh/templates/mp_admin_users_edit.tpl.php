<?php

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );

$customer_type = $propel_user->getUserType();

$account = user_load($propel_user->getUid());
?>


<form action="<?php echo $base_url; ?>/it/tonic/users/update" id="form-profile" class="form-registration form-profile spacer" method="POST" >
    <input type="hidden" name="uid" value="<?php echo $propel_user->getUid(); ?>" />
  <fieldset class="boxed">
    <legend>Dati di fatturazione e recapiti</legend>
<?php ////////////// PRIVATO //////////////   ?>
    <?php if( $customer_type == 'private' ) : ?>
    <div id="private" class="form-profile__optional">
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="">Nome</label>
          <input type="text" class="form-control" name="private_name" id="form-profile__name" value="<?php echo $propel_user->getName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cognome</label>
          <input type="text" class="form-control" name="private_surname" id="form-profile__surname" value="<?php echo $propel_user->getSurname(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Codice fiscale</label>
          <input type="text" class="form-control" name="private_fiscal_code" id="form-profile__cf" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Provincia</label>
          <input type="text" class="form-control" name="private_province" id="form-profile__prov" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Località</label>
          <input type="text" class="form-control" name="private_city" id="form-profile__location" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Indirizzo</label>
          <input type="text" class="form-control" name="private_address" id="form-profile__address" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cap</label>
          <input type="text" class="form-control" name="private_zip_code" id="form-profile__zip" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Paese</label>
<!--          <input type="text" class="form-control" name="private_nation" id="form-profile__country" value="<?php #echo $nation_user ; ?>" />-->
          
          <select name="private_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE PRIVATO //////////////   ?>

<?php ////////////// SOCIETA //////////////  ?>
    <?php if( $customer_type == 'society' ) : ?>
    <div id="society" class="form-profile__optional" >
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="">Ragione sociale</label>
          <input type="text" class="form-control" name="society_company_name" id="form-profile__business_name" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Tipo</label>
          <input type="text" class="form-control" name="society_company_type" id="form-profile__business_type" value="<?php echo $propel_user->getCompanyType(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Partita IVA</label>
          <input type="text" class="form-control" name="society_vat" id="form-profile__vat" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Codice fiscale</label>
          <input type="text" class="form-control" class="form-control" name="society_fiscal_code" id="form-profile__cf_company" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Provincia</label>
          <input type="text" class="form-control" name="society_province" id="form-profile__type_company" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Località</label>
          <input type="text" class="form-control" name="society_city" id="form-profile__location_company" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Indirizzo</label>
          <input type="text" class="form-control" name="society_address" id="form-profile__address_company" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cap</label>
          <input type="text" class="form-control" name="society_zip_code" id="form-profile__zip_company" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Paese</label>
<!--          <input type="text" class="form-control" name="society_nation" id="form-profile__country" value="<?php #echo $propel_user->getNation(); ?>" />-->
          <select name="society_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE SOCIETA //////////////   ?>

<?php ////////////// ASSOCIAZIONE //////////////  ?>
    <?php if( $customer_type == 'association' ) : ?>
    <div id="association" class="form-profile__optional" >
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="">Denominazione</label>
          <input type="text" class="form-control" name="association_name" id="form-profile__bussiness_name_club" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Codice fiscale</label>
          <input type="text" class="form-control" name="association_fiscal_code" id="form-profile__cf_club" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Eventuale Partita IVA</label>
          <input type="text" class="form-control" name="association_vat" id="form-profile__vat_club" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Provincia</label>
          <input type="text" class="form-control" name="association_province" id="form-profile__prov_club" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Località</label>
          <input type="text" class="form-control" name="association_city" id="form-profile__location_club" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Indirizzo</label>
          <input type="text" class="form-control" name="association_address" id="form-profile__address_club" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cap</label>
          <input type="text" class="form-control" name="association_zip_code" id="form-profile__zip_club" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Paese</label>
<!--          <input type="text" class="form-control" name="association_nation" id="form-profile__country" value="<?php #echo $propel_user->getNation(); ?>" />-->
          <select name="association_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE ASSOCIAZIONE //////////////   ?>

<?php ////////////// INDIVIDUALE //////////////  ?>
    <?php if( $customer_type == 'individual' ) : ?>
    <div id="individual" class="form-profile__optional">
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="">Nome</label>
          <input type="text" class="form-control" name="individual_name" id="form-profile__name_single" value="<?php echo $propel_user->getName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cognome</label>
          <input type="text" class="form-control" name="individual_surname" id="form-profile__surname_single" value="<?php echo $propel_user->getSurname(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Denominazione</label>
          <input type="text" class="form-control" name="individual_company_name" id="form-profile__denomination_single" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Partita Iva</label>
          <input type="text" class="form-control" name="individual_vat" id="form-profile__vat_single" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Codice fiscale</label>
          <input type="text" class="form-control" name="individual_fiscal_code" id="form-profile__cf_single" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Provincia</label>
          <input type="text" class="form-control" name="individual_province" id="form-profile__prov_single" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Località</label>
          <input type="text" class="form-control" name="individual_city" id="form-profile__location_single" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Indirizzo</label>
          <input type="text" class="form-control" name="individual_address" id="form-profile__address_single" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cap</label>
          <input type="text" class="form-control" name="individual_zip_code" id="form-profile__zip_single" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Paese</label>
<!--          <input type="text" class="form-control" name="individual_nation" id="form-profile__country" value="<?php #echo $propel_user->getNation(); ?>" />-->
          <select name="individual_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE INDIVIDUALE //////////////   ?>

<?php ////////////// ENTE PUBBLICO //////////////  ?>
    <?php if( $customer_type == 'public' ) : ?>
    <div id="public" class="form-profile__optional" >
      <div class="row">
        <div class="form-group col-sm-4">
          <label for="">Nome Ente</label>
          <input type="text" class="form-control" name="public_company_name" id="form-profile__name_ente" value="<?php echo $propel_user->getCompanyName(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Partita Iva</label>
          <input type="text" class="form-control" name="public_vat" id="form-profile__vat_ente" value="<?php echo $propel_user->getVat(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Codice fiscale</label>
          <input type="text" class="form-control" name="public_fiscal_code" id="form-profile__cf_ente" value="<?php echo $propel_user->getFiscalCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Provincia</label>
          <input type="text" class="form-control" name="public_province" id="form-profile__prov_ente" value="<?php echo $propel_user->getProvince(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Località</label>
          <input type="text" class="form-control" name="public_city" id="form-profile__location_ente" value="<?php echo $propel_user->getCity(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Indirizzo</label>
          <input type="text" class="form-control" name="public_address" id="form-profile__address_ente" value="<?php echo $propel_user->getAddress(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Cap</label>
          <input type="text" class="form-control" name="public_zip_code" id="form-profile__zip_ente" value="<?php echo $propel_user->getZipCode(); ?>" />
        </div>
        <div class="form-group col-sm-4">
          <label for="">Paese</label>
<!--          <input type="text" class="form-control" name="public_nation" id="form-profile__country" value="<?php #echo $propel_user->getNation(); ?>" />-->
          <select name="public_nation" id="form-profile__country" class="form-control">
                  
            <?php 
              foreach( $nazioni as $k => $nazione ){
                $selected = "";
                if( $nazione["countryCode"] == $propel_user->getNation() ) $selected = 'selected="selected"';

                echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
              }

            ?>

          </select>
        </div>
      </div>
    </div>
    <?php endif; ?>
<?php ////////////// FINE ENTE PUBBLICO //////////////   ?>

    <div class="row">
      <div class="form-group col-sm-4">
        <label for="">Nome della persona riferimento</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input type="text" class="form-control" name="reference_person_name" id="form-profile__user_rif_name" value="<?php echo $propel_user->getReferencePersonName(); ?>" />
        </div>
      </div>
      <div class="form-group col-sm-4">
        <label for="">Cognome della persona riferimento</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input type="text" class="form-control" name="reference_person_surname" id="form-profile__user_rif_name" value="<?php echo $propel_user->getReferencePersonSurname(); ?>" />
        </div>
      </div>
      <div class="form-group col-sm-4">
        <label for="">Telefono</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-phone fa-lg"></i></span>
          <input type="tel" class="form-control" name="phone" id="form-profile__phone_rif" value="<?php echo $propel_user->getPhone(); ?>" />
        </div>
      </div>
      <div class="form-group col-sm-4">
        <label for="">Cellulare</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-mobile fa-lg"></i></span>
          <input type="tel" class="form-control" name="mobile_phone" id="form-profile__cell_rif" value="<?php echo $propel_user->getMobilePhone(); ?>" />
        </div>
      </div>
      <div class="form-group col-sm-4">
        <label for="">Fax</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-fax"></i></span>
          <input type="tel" class="form-control" name="fax" id="form-profile__fax_rif" value="<?php echo $propel_user->getFax(); ?>" />
        </div>
      </div>
      <div class="form-group col-sm-4">
        <label for="">Email</label>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
          <input type="email" class="form-control" name="email" id="form-profile__email_rif" value="<?php echo (!empty($account->mail ))?$account->mail:""; ?>" />
        </div>
      </div>
      
    </div>
  </fieldset>

  <?php 
  
    //ESTRAGGO I DATI DI SPEDIZIONE
    $user_shipping = MP\UserShippingQuery::create()->filterByUid($account->uid)->findOne();
    if($user_shipping){
  ?>
      <fieldset class="boxed">
        <legend>Dati di spedizione</legend>
        <div class="box-shipping">
          <div class="row">
            <div class="form-group col-sm-4">
              <label for="">Nome Destinatario</label>
              <input type="text" class="form-control" name="shipping_reference_person_name" id="form-profile__shipping_dest_name" value="<?php echo $user_shipping->getReferencePersonName(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">Cognome Destinatario</label>
              <input type="text" class="form-control" name="shipping_reference_person_surname" id="form-profile__shipping_dest_surname" value="<?php echo $user_shipping->getReferencePersonSurname(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">indirizzo</label>
              <input type="text" class="form-control" name="shipping_address" id="form-profile__shipping_address" value="<?php echo $user_shipping->getAddress(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">telefono</label>
              <input type="text" class="form-control" name="shipping_phone" id="form-profile__shipping_phone" value="<?php echo $user_shipping->getPhone(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">cap</label>
              <input type="text" class="form-control" name="shipping_zip_code" id="form-profile__shipping_zip_code" value="<?php echo $user_shipping->getZipCode(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">città</label>
              <input type="text" class="form-control" name="shipping_city" id="form-profile__shipping_location" value="<?php echo $user_shipping->getCity(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">provincia</label>
              <input type="text" class="form-control" name="shipping_province" id="form-profile__shipping_prov" value="<?php echo $user_shipping->getProvince(); ?>" />
            </div>
            <div class="form-group col-sm-4">
              <label for="">paese</label>
    <!--          <input type="text" class="form-control" name="shipping_nation" id="form-profile__shipping_country" value="<?php #echo $user_shipping->getNation(); ?>" />-->
              <select name="shipping_nation" id="form-profile__country" class="form-control">

                <?php 
                  foreach( $nazioni as $k => $nazione ){
                    $selected = "";
                    if( $nazione["countryCode"] == $user_shipping->getNation() ) $selected = 'selected="selected"';

                    echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
                  }

                ?>

              </select>
            </div>
          </div>
        </div>    
      </fieldset>
    
    <?php
    }
    ?>
  <fieldset class="boxed">
    <legend>Newsletter</legend>
    <div class="checkbox">
      <label class="form-profile__privacy">
          <input type="checkbox" <?php if($propel_user->getNewsletter() == 1){ echo "checked='checked'"; } ?> name="newsletter" id="form-profile__newsletter" > Iscrizione <strong>Newsletter</strong></label>
    </div>    
  </fieldset>
    
  <fieldset class="boxed">
    <legend>Iva Agevolata</legend>
    <div class="checkbox">
      <label class="form-profile__privacy">
          <input type="checkbox" <?php if($propel_user->getVatEasy() == 1){ echo "checked='checked'"; } ?> name="vateasy" id="form-profile__privacy" > Iva <strong>Agevolata</strong></label>
    </div>    
  </fieldset>
  <fieldset class="boxed">
    <legend>Blacklist</legend>
    <div class="checkbox">
      <label class="form-profile__privacy">
          <input type="checkbox" <?php if($propel_user->getBlacklist() == 1){ echo "checked='checked'"; } ?> name="blacklist" id="form-profile__blacklist" > Aggiungi alla <strong>Blacklist</strong></label>
    </div>    
  </fieldset>
  <input type="submit" class="btn btn-red btn-update" value="Aggiorna" />
</form>






