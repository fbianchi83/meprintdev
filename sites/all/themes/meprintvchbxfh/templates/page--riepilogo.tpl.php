
<?php
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';

global $base_url;
global $base_path;
global $language;
global $user;
$lang_name = $language->language;
?>

<?php
include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
  <div class="space-3"></div>
  <!--End of the search--->
  <article class="row">

    <div class="col-md-9 col-sm-9">
      <header class="row">
        <div class="col-sm-12 col-md-12 ">

          <?php if ($breadcrumb): echo '<a href="' . $base_url . '">' . $Home . '</a>' . ' / ' . $bordersucess; endif; ?>  

          <div class="space-2"></div>
          <div class="cartnav clearfix">
            <div class="cartlable hidden-xs">
              <span class="shiplable inactive"> <?php echo $cartpagetittle; ?></span>
            </div>
            <div class="shippingnav hidden-xs">
              <span class="shiplable inactive"><?php echo $shippinginfo; ?></span>
            </div>
            <div class="ordernav shiplable">
              <span class="active"><?php echo $orderconfirm; ?></span>
            </div>
          </div>

        
          <div class="space-3"></div>

          <?php
          print render($page['content']);
          ?>
        </div>
      </header>
      <div class="space-2"></div> 
    </div>

    <aside class="col-md-3 col-sm-3">

      <div class="space-2"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
      </div>
      <div class="space-2"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
      </div>
      <div class="space-2"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
      </div>
    </aside>
  </article>
</section>

<code>



</code>
<?php
include('footer.tpl.php');
?>

<script>
  jQuery(document).ready(function($) {
    $('.cartdetails--open').click(function() {
      $(this).toggleClass('fa-plus fa-minus');
    });
  });
</script>