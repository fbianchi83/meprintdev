<?php

global $base_url;
global $language;
$lang_name = $language->language;

?>

<ul>
  <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/chi-siamo">Chi Siamo</a></li>
   <li><a href="http://blog.meprint.it/" target="new">Blog</a></li>
   <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/iniziative">Iniziative</a></li>
  <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/faq">FAQ </a></li>
  <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/termini-e-condizioni">Termini e condizioni</a></li>
    <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/contact">Contatti</a></li>
  <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/privacy">Privacy</a></li>
  <li><a href="<?php echo $base_url. '/' .$lang_name; ?>/cookie">Cookie Policy</a></li>
</ul>