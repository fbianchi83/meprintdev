<script type="text/javascript">
jQuery(document).ready(function($) {
  
  // al cambio di rubrica ricarico i dati di fatturazione e spedizione
  $( "#rubrica-user-fatt" ).change(function() {    
    var contact = $('#rubrica-user-fatt').val();
    //console.log( contact);
    $.ajax({
      url: "/loadRubryByValue",
      type: "POST",
      dataType: "json",
      data: {contact: contact}
    })
    .success(function(response){
       //console.log(response);
      if( response == false ) {
        $('#accountholder_name').val('');
        $('#accountholder_surname').val('');
        $('#accountholder').val('');
        $('#cf').val('');
        $('#vat').val('');
        $('#address').val('');
        $('#phone').val('');
        $('#email').val('');      
        $('#zipcode').val('');
        $('#location').val('');
        $('#province').val('');
        $('#country').val('');
      } else {
        var json = JSON.parse( response );
        //console.log(json);
        $('#accountholder_name').val(json.Name);
        $('#accountholder_surname').val(json.Surname);
        $('#accountholder').val(json.CompanyName);
        $('#cf').val(json.FiscalCode);
        $('#vat').val(json.Vat);
        $('#address').val(json.Address);
        $('#phone').val(json.Phone);
        $('#email').val(json.Email);      
        $('#zipcode').val(json.ZipCode);
        $('#location').val(json.City);
        $('#province').val(json.Province);
        $('#country').val(json.Nation);
      }
    });
    
  });
  
  
  $( "#rubrica-user-shipping" ).change(function() {    
    var contact = $('#rubrica-user-shipping').val();    
    $.ajax({
      url: "/loadRubryByValue",
      type: "POST",
      dataType: "json",
      data: {contact: contact}
    })
    .success(function(response){
       //console.log(response);
      if( response == false ) {
        $('#srecipient_name').val('');
        $('#srecipient_surname').val('');
        $('#srecipient').val('');
        $('#scf').val('');
        $('#svat').val('');
        $('#saddress').val('');
        $('#sphone').val('');
        $('#semail').val('');      
        $('#szipcode').val('');
        $('#slocation').val('');
        $('#sprovince').val('');
        $('#scountry').val('');
      } else {
        var json = JSON.parse( response );
        //console.log(json);
        $('#srecipient_name').val(json.Name);
        $('#srecipient_surname').val(json.Surname);
        $('#srecipient').val(json.CompanyName);
        $('#scf').val(json.FiscalCode);
        $('#svat').val(json.Vat);
        $('#saddress').val(json.Address);
        $('#sphone').val(json.Phone);
        $('#semail').val(json.Email);      
        $('#szipcode').val(json.ZipCode);
        $('#slocation').val(json.City);
        $('#sprovince').val(json.Province);
        $('#scountry').val(json.Nation);
      }
    });
    
  });
  
  $('#shipping-form').submit(function(e) {
        if (jQuery('#vat').val() != "") {
            if (jQuery('#accountholder').val() == "") {
                jQuery('#errorbill').css('display', 'block');
                var topPosition = jQuery('#accountholder_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#accountholder_name').val() != "") {
                if (jQuery('#accountholder_surname').val() == "" || jQuery('#cf').val() == "") {
                    jQuery('#errorbill').css('display', 'block');
                    var topPosition = jQuery('#accountholder_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
            else {
                jQuery('#errorbill').css('display', 'block');
                var topPosition = jQuery('#accountholder_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        
        if (jQuery('#svat').val() != "") {
            if (jQuery('#srecipient').val() == "") {
                jQuery('#errorship').css('display', 'block');
                var topPosition = jQuery('#srecipeint_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#srecipient_name').val() != "") {
                if (jQuery('#srecipient_surname').val() == "" || jQuery('#scf').val() == "") {
                    jQuery('#errorship').css('display', 'block');
                    var topPosition = jQuery('#srecipient_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
            else {
                jQuery('#errorship').css('display', 'block');
                var topPosition = jQuery('#srecipient_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        
        if (jQuery('#vat').val() == "") {
            if (jQuery('#cf').val() != "") {
                if (!checkcf(jQuery('#cf').val())){
                    jQuery('#errorbill2').css('display', 'block');
                    var topPosition = jQuery('#accountholder_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
            if (jQuery('#scf').val() != "") {
                if (!checkcf(jQuery('#scf').val())){
                    jQuery('#errorship2').css('display', 'block');
                    var topPosition = jQuery('#srecipient_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
        }
    });
});

function checkcf(val) {
    var regex = /[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]/;
    var v = val.match(regex);
    //console.log( 'codice fiscale valido: ' + v );
    if ( v ) { return true; }
    else { 
        regex = /[\d]{11}/;
        var v = val.match(regex);
        //console.log( 'codice fiscale valido: ' + v );
        if ( v ) { return true; }
        else { 
            return false; 
        }
    }
}




    /*function checkformship() {
        if (jQuery('#vat').val() != "") {
            if (jQuery('#accountholder').val() == "") {
                jQuery('#errorbill').css('display', 'block');
                var topPosition = jQuery('#accountholder_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#accountholder_name').val() != "") {
                if (jQuery('#accountholder_surname').val() == "" || jQuery('#cf').val() == "") {
                    jQuery('#errorbill').css('display', 'block');
                    var topPosition = jQuery('#accountholder_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
        }
        
        if (jQuery('#svat') != "") {
            if (jQuery('#saccountholder').val() == "") {
                jQuery('#serrorbill').css('display', 'block');
                var topPosition = jQuery('#saccountholder_name').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#saccountholder_name').val() != "") {
                if (jQuery('#saccountholder_surname').val() == "" || jQuery('#scf').val() == "") {
                    jQuery('#serrorbill').css('display', 'block');
                    var topPosition = jQuery('#saccountholder_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
        }
        
        if (jQuery('#vat').val() == "") {
            if (jQuery('#cf').val() != "") {
                if (!checkcf(jQuery('#cf').val())){
                    jQuery('#errorbill2').css('display', 'block');
                    var topPosition = jQuery('#accountholder_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
            if (jQuery('#scf').val() != "") {
                if (!checkcf(jQuery('#scf').val())){
                    jQuery('#errorship2').css('display', 'block');
                    var topPosition = jQuery('#srecipient_name').offset().top;
                    jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                    return false;
                }
            }
        }
        
        jQuery('#shipping-form').submit();
    }*/

</script>

<?php

global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );


global $user;
session_start();
$sessionid = session_id();

$name = '';
$surname = '';
$email = '';
if ($user->uid != 0) {
  
  $propel_user = MP\UserQuery::create()->filterByUid($user->uid)->findOne();
  $customer_type = $propel_user->getUserType();
  if ($customer_type == "private" || $customer_type == "unknown") {
    $name = $propel_user->getName();
    $surname = $propel_user->getSurname();
    $cf = $propel_user->getFiscalCode();
  } else {
    $recipient = $propel_user->getCompanyName();
    $vat = $propel_user->getVat();
  }
  
  $address = $propel_user->getAddress();
  $phone = $propel_user->getPhone();
  $zipcode = $propel_user->getZipCode();
  $province = $propel_user->getProvince();
  $location = $propel_user->getCity();
  $country = $propel_user->getNation();
  $email = $user->mail;
  $reducedvat = $propel_user->getVatEasy();
  
  $shipping_user = MP\UserShippingQuery::create()->filterByUid($user->uid)->findOne();
  if( $shipping_user ){
    $shipping_recipient_name = $shipping_user->getReferencePersonName();
    $shipping_recipient_surname = $shipping_user->getReferencePersonSurname();
    $shipping_cf = $shipping_user->getFiscalCode();
    $shipping_recipient = $shipping_user->getCompanyName();
    $shipping_vat = $shipping_user->getVat();

    $shipping_address = $shipping_user->getAddress();
    $shipping_phone = $shipping_user->getPhone();
    $shipping_zipcode = $shipping_user->getZipCode();
    $shipping_province = $shipping_user->getProvince();
    $shipping_location = $shipping_user->getCity();
    $shipping_country = $shipping_user->getNation();
  }
  $shipping_email = $user->mail;

  if($_SESSION['accountholder'] == ""){
    $_SESSION['accountholder'] = $recipient;
    if( $name != '' && $surname != '' ){
      $_SESSION['accountholder_name'] = $name;
      $_SESSION['accountholder_surname'] = $surname;
    }
  }
  if($_SESSION['cf'] == ""){
    $_SESSION['cf'] = $cf;
  }
  if($_SESSION['vat'] == ""){
    $_SESSION['vat'] = $vat;
  }  
  if($_SESSION['address'] == ""){
    $_SESSION['address'] = $address;
  }
  if($_SESSION['phone'] == ""){
    $_SESSION['phone'] = $phone;
  }
  if($_SESSION['zipcode'] == ""){
    $_SESSION['zipcode'] = $zipcode;
  }
  if($_SESSION['location'] == ""){
    $_SESSION['location'] = $location;
  }
  if($_SESSION['province'] == ""){
    $_SESSION['province'] = $province;
  }
  if($_SESSION['country'] == ""){
    $_SESSION['country'] = $country;
  }
  if($_SESSION['email'] == ""){
    $_SESSION['email'] = $email;
  }
  
  if($_SESSION['srecipient'] == ""){
    $_SESSION['srecipient'] = $shipping_recipient;
    if( $shipping_recipient_name != '' && $shipping_recipient_surname != '' ){
      $_SESSION['srecipient_name'] = $shipping_recipient_name;
      $_SESSION['srecipient_surname'] = $shipping_recipient_surname;
    }
  }
  //dd($shipping_recipient_name);
  if($_SESSION['scf'] == ""){
    $_SESSION['scf'] = $shipping_cf;
  }
  if($_SESSION['svat'] == ""){
    $_SESSION['svat'] = $shipping_vat;
  } 
  if($_SESSION['saddress'] == ""){
    $_SESSION['saddress'] = $shipping_address;
  }
  if($_SESSION['sphone'] == ""){
    $_SESSION['sphone'] = $shipping_phone;
  }
  if($_SESSION['szipcode'] == ""){
    $_SESSION['szipcode'] = $shipping_zipcode;
  }
  if($_SESSION['slocation'] == ""){
    $_SESSION['slocation'] = $shipping_location;
  }
  if($_SESSION['sprovince'] == ""){
    $_SESSION['sprovince'] = $shipping_province;
  }
  if($_SESSION['scountry'] == ""){
    $_SESSION['scountry'] = $shipping_country;
  }
  if($_SESSION['semail'] == ""){
    $_SESSION['semail'] = $email;
  }
}

  if(isset($_POST['productprice']) && $_POST['productprice'] != ''){
    $_SESSION['productprice'] = $_POST['productprice'];
  }
  if(isset($_POST['dicountamount']) && $_POST['dicountamount'] != ''){
    $_SESSION['dicountamount'] = $_POST['dicountamount'];
  }
  if(isset($_POST['finalamount']) && $_POST['finalamount'] != ''){
    $_SESSION['finalamount'] = $_POST['finalamount'];
  }
  if(isset($_POST['coupon_code']) && $_POST['coupon_code'] != ''){
    $_SESSION['coupon_code'] = $_POST['coupon_code'];
  }
  if(isset($_POST['discount_id']) && $_POST['discount_id'] != ''){
    $_SESSION['discount_id'] = $_POST['discount_id'];
  }
  if(isset($_POST['totalprice']) && $_POST['totalprice'] != ''){
    $_SESSION['totalprice'] = $_POST['totalprice'];
  }
  if(isset($_POST['note']) && $_POST['note'] != ''){
    $_SESSION['note'] = $_POST['note'];
  }
if($user->uid == 0){

  $cart = MP\CartQuery::create()->filterBySessionId($sessionid)->filterByStatus(1)->findOne();
}else{
  $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->findOne();
}

if ($cart) {
  
  $contacts = MP\UserAddressBookQuery::create()->filterByUid($user->uid)->find();
  
  if ($user->uid != 0) {
    $user_blacklist = $propel_user->getBlacklist();
  }else{
    $user_blacklist = 0;
  }
  
  
  ?>

  <form method="POST" id="shipping-form" action="<?php echo $base_url . '/' . $lang_name; ?>/riepilogo" autocomplete="on" class="boxed">
      
    <input type="hidden" name="Shipping_Confirm" id="Shipping_Confirm" value="1" />

    <?php if( user_is_logged_in() ): ?>
      <div class="legend "><span><?php echo $lrubrica; ?></span></div>
      <div class="col-sm-6 col-xs-12">
        <select id="rubrica-user-fatt" class="rubrica-user form-control shipform" name="rubrica_user_fatt">
        <option value="" ><?php echo $lcontact; ?></option>
        <?php foreach($contacts as $contact): ?>
          <option value="<?php echo $contact->getId(); ?>"><?php echo $contact->getAddressBookName(); ?></option>
        <?php endforeach; ?>
      </select>
      </div>

      <div class="clearfix"></div>
    <?php endif; ?>
    
    
    <div class="legend "><span><?php echo $billing; ?></span></div>
    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $laccountholder_name; ?></label>  <input type="text" name="accountholder_name" id="accountholder_name" class="form-text shipform" value="<?php if ($_SESSION['accountholder_name'] != '') {echo $_SESSION['accountholder_name'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $laccountholder_surname; ?></label>  <input type="text" name="accountholder_surname" id="accountholder_surname" class="form-text shipform" value="<?php if ($_SESSION['accountholder_surname'] != '') {echo $_SESSION['accountholder_surname'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $laccountholder; ?></label>  <input type="text" name="accountholder" id="accountholder" class="form-text shipform" value="<?php if ($_SESSION['accountholder'] != '') {echo $_SESSION['accountholder'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lvat; ?></label>  <input type="text" name="vat" id="vat" class="form-text shipform" value="<?php if ($_SESSION['vat'] != '') {echo $_SESSION['vat'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lcf; ?></label>  <input type="text" name="cf" id="cf" class="form-text shipform" value="<?php if ($_SESSION['cf'] != '') {echo $_SESSION['cf'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $laddress; ?></label>  <input type="text" name="address" id="address" class="form-text shipform" value="<?php if ($_SESSION['address'] != '') {echo $_SESSION['address'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lphone; ?></label>  <input type="text" name="phone" id="phone" class="form-text shipform" value="<?php if ($_SESSION['phone'] != '') {echo $_SESSION['phone'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lemail; ?></label>  <input type="text" name="email" id="email" class="form-text shipform" value="<?php if ($_SESSION['email'] != '') {echo $_SESSION['email'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lzipcode; ?></label> <input type="text" name="zipcode" id="zipcode" class="form-text shipform" value="<?php if ($_SESSION['zipcode'] != '') {echo $_SESSION['zipcode'];} ?>"/></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $llocation; ?></label> <input type="text" name="location" id="location" class="form-text shipform" value="<?php if ($_SESSION['location'] != '') {echo $_SESSION['location'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lprovince; ?></label>  <input type="text" name="province" id="province" class="form-text shipform" value="<?php if ($_SESSION['province'] != '') {echo $_SESSION['province'];} ?>"/></div>
    <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lcontry; ?></label>  <!--<input type="text" name="country" id="country" class="form-text shipform" value="<?php #if ($_SESSION['country'] != '') {echo $_SESSION['country'];} ?>"/>-->
      <select name="country" id="country" class="form-control shipform">                  
        <?php
          foreach( $nazioni as $k => $nazione ){
            $selected = "";
            if( $nazione["countryCode"] == $_SESSION['country'] ) $selected = 'selected="selected"';

            echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
          }
        ?>
      </select>
    </div> 
    <div class="error-msg" id="errorbill" style="display:none; color:red"> <?php echo $lerrorbill; ?> </div>
    <div class="error-msg" id="errorbill2" style="display:none; color:red"> <?php echo $lerrorbill2; ?> </div>
    <div class="error-msg" id="errorbill3" style="display:none; color:red"> <?php echo $lerrorbill3; ?> </div>
    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div id="shipdetailsform" style="overflow: hidden">
      
      <?php if( user_is_logged_in() ): ?>
      <div class="legend "><span><?php echo $lrubrica; ?></span></div>
      <div class="col-sm-6 col-xs-12">
        <select id="rubrica-user-shipping" class="rubrica-user form-control shipform" name="rubrica_user_shipping">
        <option value="" ><?php echo $lcontact; ?></option>
        <?php foreach($contacts as $contact): ?>
          <option value="<?php echo $contact->getId(); ?>"><?php echo $contact->getAddressBookName(); ?></option>
        <?php endforeach; ?>
      </select>
      </div>

      <div class="clearfix"></div>
    <?php endif; ?>
      
      
      <div class="legend "><span ><?php echo $shipping; ?></span> </div> 
      <div class="col-sm-12 col-md-12 col-xs-12 hidden">
        <input type="checkbox" name="billingtoo" onclick="Fillshipping(this.form)" /> <span ><?php echo $sameasbill; ?></span>
      </div>
      <div class="clearfix"></div>
      
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lrecipient_name; ?></label>  <input type="text" name="srecipient_name" id="srecipient_name" class="form-text shipform" value="<?php if ($_SESSION['srecipient_name'] != '') {echo $_SESSION['srecipient_name'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lrecipient_surname; ?></label>  <input type="text" name="srecipient_surname" id="srecipient_surname" class="form-text shipform" value="<?php if ($_SESSION['srecipient_surname'] != '') {echo $_SESSION['srecipient_surname'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lrecipient; ?></label>  <input type="text" name="srecipient" id="srecipient" class="form-text shipform" value="<?php if ($_SESSION['srecipient'] != '') {echo $_SESSION['srecipient'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lvat; ?></label>  <input type="text" name="svat" id="svat" class="form-text shipform" value="<?php if ($_SESSION['svat'] != '') {echo $_SESSION['svat'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lcf; ?></label>  <input type="text" name="scf" id="scf" class="form-text shipform" value="<?php if ($_SESSION['scf'] != '') {echo $_SESSION['scf'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $laddress; ?></label>  <input type="text" name="saddress" id="saddress" class="form-text shipform" value="<?php if ($_SESSION['saddress'] != '') {echo $_SESSION['saddress'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lphone; ?></label>  <input type="text" name="sphone" id="sphone" class="form-text shipform" value="<?php if ($_SESSION['sphone'] != '') {echo $_SESSION['sphone'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lemail; ?></label>  <input type="text" name="semail" id="semail" class="form-text shipform" value="<?php if ($_SESSION['semail'] != '') {echo $_SESSION['semail'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lzipcode; ?></label> <input type="text" name="szipcode" id="szipcode" class="form-text shipform" value="<?php if ($_SESSION['szipcode'] != '') {echo $_SESSION['szipcode'];} ?>"/></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $llocation; ?></label> <input type="text" name="slocation" id="slocation" class="form-text shipform" value="<?php if ($_SESSION['slocation'] != '') {echo $_SESSION['slocation'];} ?>" /></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lprovince; ?></label>  <input type="text" name="sprovince" id="sprovince"  class="form-text shipform" value="<?php if ($_SESSION['sprovince'] != '') {echo $_SESSION['sprovince'];} ?>"/></div>
      <div class="col-sm-6 col-md-6 col-xs-12"><label class="shiplabel"><?php echo $lcontry; ?></label>  <!--<input type="text" name="scountry" id="scountry" class="form-text shipform" value="<?php #if ($_SESSION['scountry'] != '') {echo $_SESSION['scountry'];} ?>"/>-->
      
      <select name="scountry" id="scountry" class="form-control shipform">                  
        <?php 
          foreach( $nazioni as $k => $nazione ){
            $selected = "";
            if( $nazione["countryCode"] == $_SESSION['scountry'] ) $selected = 'selected="selected"';

            echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
          }
        ?>
      </select>
      </div> 
        <div class="error-msg" id="errorship" style="display:none; color:red"> <?php echo $lerrorbill; ?> </div>
        <div class="error-msg" id="errorship2" style="display:none; color:red"> <?php echo $lerrorbill2; ?> </div>
        <div class="error-msg" id="errorship3" style="display:none; color:red"> <?php echo $lerrorbill3; ?> </div>
    </div>
    
    <div id="noteform" style="overflow: hidden">
      <div class="legend "><span ><?php echo $note; ?></span> </div>
      <div class="clearfix"></div>
      <div class="col-sm-12 col-md-12 col-xs-12"><textarea name="note" id="note" rows="2" cols="95"><?php if ($_SESSION['note'] != '') {echo $_SESSION['note'];} ?></textarea></div>
    </div>
    <?php if ($reducedvat == 1) { ?>
    <div id="vatform" style="overflow: hidden">
      <div class="legend "><span ><?php echo $vat4; ?> <input name="vat4" id="vat4" type="checkbox"/></span> </div>
     </div>
    <?php } ?>
    <div class="clearfix"></div>
    <div class="space-2"></div>
      
    <!-- Shipping Company Details --->
    <div class="legend "><span><?php echo $Shippingtype; ?></span></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

      <?php
      $shippingName = MP\ShippingCompaniesQuery::create()->filterByStatus(1)->find();
      $s = 1;
      foreach ($shippingName as $shipname) {
        $ship_img_fid = $shipname->getFile();
        $ship_img_file = file_load($ship_img_fid);
        $ship_img_url = file_create_url($ship_img_file->uri);
        ?>
            <div class="shipzone">
              <label class="dym-p dimdiv dimdiv--shipping">
                <input type="radio" name="shipping" id="shipping_<?php echo $shipname->getShippingId(); ?>" <?php if ($s == 1): ?>checked="checked"<?php endif; ?> value="<?php echo $shipname->getShippingId(); ?>" />
                <img src="<?php echo $ship_img_url; ?>" alt=""/>
                <p><?php echo $shipname->getName(); ?></p>
              </label>
            </div>
        <?php
        $s++;
      }
      ?>

    </div>
     
    <!-- Anonymous Shipping Start  -->
    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div class="legend "><span><?php echo $anonoshiping; ?></span></div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <label class="dym-p dimdiv dimdiv--anonimous">
        <input type="checkbox" name="anonshipping" id="annonshipping" value="1">
        <span class="fa fa-eye-slash fa-4x"></span><p class="dimdiv--anonimous__label"><?php echo $anonoshiping; ?><br /><span><?php echo $anonoshipingtext; ?></span></p>
      </label>     
    </div>
    <!-- Anonymous Shipping Start End  -->

    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div class="legend"> <span><?php echo $paymentoptions; ?></span></div>
    
    <div id="payment-wrapper" >        
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="paypal" value="paypal" checked="checked" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/paypal_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo base_path() . path_to_theme() ?>/images/paypal_icon.png" alt="" /><p><?php echo t('If you have chosen a formula of advance payment (Credit Card, Paypal, etc.) you can cancel your order until you have not made the payment. For all other types of payment order can not be undone.'); ?></p></div>' />
            <p><?php echo t('credit card and paypal'); ?></p>
          </label>
        </div>
        
        <?php
         $shipcost= MP\ShippingCostSettingsQuery::create()->filterByStatus(1)->findOne();
          /* CONTROLLO BLACKLIST */ 
          // se l'utente è in BLACKLIST non può pagare in contrassegno
          if(!$user_blacklist && $_SESSION['iva']+$_SESSION['totalprice']<$shipcost->getMaxLimitOrderCashDelivery()){
        ?>
            <div class="text-center">
              <label class="dimdiv dimdiv--circle">
                <input type="radio" name="payment" class="payment_class" id="contrassegno" value="contrassegno" />
                <img src="<?php echo base_path() . path_to_theme() ?>/images/contrassegno_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo base_path() . path_to_theme() ?>/images/contrassegno_icon.png' alt='' /><p><?php echo t('If you have chosen to pay for their delivery, you can cancel the order as long as you have not uploaded at least one file.'); ?></p></div>" />
                <p><?php echo t('cash'); ?></p>
              </label>
            </div>
        <?php
          }
        ?>
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="banktransfer" value="banktransfer" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/banktransfer_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo base_path() . path_to_theme() ?>/images/banktransfer_icon.png' alt='' /><p><?php echo t('For payments by bank transfer printing it will start only after the amount has been credited, regardless of sending the cro or payment slip. Therefore the delivery slip of 2-3 days, plus the time for printing and shipping. Also remember that you must enter the order number in the description of payment to speed up the transaction.'); ?></p></div>" />
            <p><?php echo t('bank transfer'); ?></p>
          </label>
        </div>
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="sofort" value="sofort" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/sofort_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo base_path() . path_to_theme() ?>/images/sofort_icon.png" alt="" /><p><?php echo t('If you have chosen a formula of advance payment (Credit Card, Paypal, etc.) you can cancel your order until you have not made the payment. For all other types of payment order can not be undone.'); ?></p></div>' />
            <p>sofort</p>
          </label>
        </div>
        
      </div>
    
    <div class="space-2"></div>
    <div class="clearfix"></div>
    <div class="space-2"></div>
    
    <div class="col-md-6 col-sm-6 continue">
      <a href="<?php echo $base_url . '/' . $lang_name; ?>/carrello">
        <span class="btn"><?php echo $backtocart; ?></span>
      </a>
    </div>
    <div class="col-md-6 col-sm-6 payment">
      <button class="btn btn-success" type="submit" name="proceedtoshipping" value="proceedtoshipping"><?php echo $Proceedtoship; ?></button>
    </div>

  </form><?php
} else {
  echo '<h4>' . $noproductsavail . '</h4>';
}?>