<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    ?>

    <!--CONTENUTO NODO-->

    <?php  global $base_url;
    $noproduct = t('No Products Avalible ');
    $sortbycat = t('Select Sub Category');
    $selsubcat = t('All');
    global $website;
    if($_SERVER['SERVER_NAME'] == 'www.meprint.it' || $_SERVER['SERVER_NAME'] == 'new.meprint.it'){
      $website = 'it';
    }
    else if ($_SERVER['SERVER_NAME'] == 'www.meprint.fr'){
      $website = 'fr';
    }
    $count=0;
    $page = isset($_GET['page'])?$_GET['page']:0;
    $noproduct = t('No products Available');
    $purl = explode("?",$_SERVER["REQUEST_URI"]);
    //$prod_id = $purl[1];  $subname="";$pro_name=""; $subdesc ="";
    $prod_id = $id;
    global $language ;
    $lang_name = $language->language ;
    $sub_query = MP\ProductsubgroupLangQuery::create()
                                            ->where('ProductsubgroupLang.LanguageId =?', $lang_name)
                                            ->where('ProductsubgroupLang.ProductsubgroupId =?', $prod_id)
                                            ->where('ProductsubgroupLang.Status =?', 1)
                                            ->find();
    if(count($sub_query)>0){
      foreach($sub_query as $subgrp){
        $subname = $subgrp->getName();
        $subdesc = $subgrp->getDescription();
        $subp_query = MP\ProductsgroupLangQuery::create()
                                               ->where('ProductsgroupLang.LanguageId =?', $lang_name)
                                               ->where('ProductsgroupLang.ProductgroupId =?', $subgrp->getProductgroupId())
                                               ->where('ProductsgroupLang.Status =?', 1)
                                               ->find();
        foreach($subp_query as $subpgrp){
          $pro_name=$subpgrp->getName();
          $pro_description=$subpgrp->getDescription();
        }
      }
    }
    ?>
    <header>
        <?php if(isset($subname) && isset($subdesc)){?>
          <h1 class="section-title"><?php echo $subname; ?><span>
			<img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-left: 15px"></span> </h1>
          <p><?php echo $subdesc; ?></p>
        <?php }?>
    </header>
    <div class="well well-lg list-filter">
      <div class="pull-right list-selector">
        <span><a href="allproductslist?<?php echo $prod_id; ?>"><i class="fa fa-align-justify fa-2x list-fa-th-list"></i></a></span>&nbsp;
      </div>
      <form role="form" class="form-inline">
        <div class=" form-group">
          <label for="formGroupInputSmall" class="list-filter__label control-label"><?php echo $sortbycat; ?></label>

          <select class="form-control input-sm" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
            <option value=""><?php echo $selsubcat; ?></option>
            <?php
            if (count($sub_query) > 0) {
              $subcategories = MP\ProductsubgroupLangQuery::create()
                                                          ->filterByProductgroupId($subgrp->getProductgroupId())
                                                          ->filterByLanguageId($lang_name)
                                                          ->find();
              foreach ($subcategories as $subcat) {
                ?>
                <option value="<?php echo $base_url . '/' . $lang_name . '/productgridlist?' . $subcat->getproductSubgroupId(); ?>"><?php echo $subcat->getName(); ?></option>
              <?php }
            } ?>

          </select>
        </div>
      </form>
    </div>
      <section class="row">
        <?php  $limit=10;
        $purl = explode("?",$_SERVER["REQUEST_URI"]);
        $list_lang = explode("/",$purl[0]);
        $prsid = explode("&page",$purl[1]);
        $prod_id = $purl[1];
        if(count($prsid)>0){
          $prod_id = $prsid[0];
        }
        $listing_query = MP\FormoneBigFormatQuery::create()
                                                 ->filterBySubcategoryId($prod_id)
                                                 ->filterByStatus(1)
                                                 ->find();
        if(count($listing_query )>0){$r=0;$p=0;
        foreach($listing_query as $listing){
          $id = $listing->getFormId();
          $result_lang = MP\FormoneLangQuery::create()
                                            ->where('FormoneLang.LanguageId =?', $lang_name)
                                            ->where('FormoneLang.FormId =?', $id)
                                            ->find();
          foreach($result_lang as $check_lang){
            $name = substr($check_lang->getName(), 0, 25);
            $pos = strpos($check_lang->getDescription(), " ", 130);
                if (strlen($check_lang->getDescription()) < 130)
                    $description = $check_lang->getDescription();
                else {
                    $description = substr($check_lang->getDescription(), 0, $pos);
                    if (strlen($check_lang->getDescription()) > $pos)
                        $description .= "...";
                }
                $date = $listing->getCreatedDate();
            $baseprice = $listing->getBasePriceForQuantity();
            $check_query = MP\FormoneAdditionalInfoQuery::create()
                                                        ->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                                                        ->where('FormoneAdditionalInfo.FormId =?', $id)
                                                        ->find();
            $count+= MP\FormoneAdditionalInfoQuery::create()
                                                  ->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                                                  ->where('FormoneAdditionalInfo.FormId =?', $id)
                                                  ->count();
            foreach($check_query as $check){
              $r++;
              $mypage = $page * $limit;
              $lastrec = $mypage +10;
              if($r>=$mypage && $r<=($mypage+10)){
                $p++;
                $promo = $check->getPromoFlag();
                $promoprice = $check->getPromotionPrice();
                $promoprice = $check->getPromoPriceType();
                $ImgHome = file_load($check->getImage());
                $ImgHomePath = image_style_url("products-listing", $ImgHome->uri);
                ?>
                <div class="col-sm-4 col-md-4 ">
                  <a href="<?php echo $base_url . '/' . $lang_name . '/product-three-column?' . $id . '&type=Big'; ?>">
                    <div class=" products-Thumbnail">
                      <div class="item item-type-line">
                        <span class="item-hover">
                          <div class="item-info">
                            <div class="date"><?php echo $name; ?></div>
                            <div class="line"></div>
                            <div class="date"><?php echo $description; ?></div>
                          </div>
                          <div class="mask"></div>
                        </span>

                        <div class="item-img">

                          <img class="img-responsive" alt="" src="<?php echo $ImgHomePath; ?>"></div>
                      </div>
                      <?php if ($promo == 'Promo') { ?>
                        <div class="products-title"><em><?php echo $lpromo; ?></em></div>
                      <?php } ?>
                      <?php if ($promo == 'Best price') { ?>
                        <div class="products-title1"><em><?php echo $lbestprice; ?></em></div>
                      <?php } ?>
                      <div class="clr"></div>
                      <p><?php echo $name; ?></p>
                      <?php if ($promo == 'Promo') {
                        $actualprice = $baseprice;
                        if ($promotype == "value") {
                          $baseprice = $baseprice - $promoprice;
                        }
                        elseif ($promotype == "percentage") {
                          $promodisc = $promoprice * $baseprice;
                          $promodisc = $promodisc / 100;
                          $baseprice = $baseprice - $promodisc;
                        }
                        $baseprice= number_format((float)$baseprice, 2, ".","");
                        $actualprice= number_format((float)$actualprice, 2, ".","");
                        
                        ?>

                        <p class="p"><?php if ($baseprice != $actualprice): ?>&euro; <span style="text-decoration:line-through"><?php echo $actualprice; ?></span>/<?php endif; ?> &euro; <?php echo $baseprice; ?></p>
                      <?php }
                      else { $baseprice= number_format((float)$baseprice, 2, ".","");
                        ?>
                        <p class="p">&euro; <?php echo $baseprice; ?></p><?php } ?>
                    </div>
                  </a>
                </div>
              <?php     }
            } }    }  if($p<=0){?>
          <div class="col-md-12 col-sm-12 well well-lg">
            <?php echo $noproduct;?>
          </div>
        <?php }?>
      </section>
  <?php }else{ ?>
    <div class="col-md-12 col-sm-12 well well-lg">
      <?php echo $noproduct;?>
    </div>
  <?php }?>
    <footer class="row">
      <div class="col-sm-12 col-md-12">
        <div class="col-sm-3 col-md-3">
          <nav>
          </nav>
        </div>
        <div class="col-sm-9 col-md-9 text-center">
          <nav>
            <?php $pagcnt = round($count/10);?>
            <ul class="pagination">
              <?php for($g=1;$g<=$pagcnt;$g++){
                $f = $g -1;
                $class='';
                if($f==$page){ $class='active';}
                if($g==1){?>
                  <li class="<?php echo $class;?>"><a href="productgridlist?<?php echo $prod_id;?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                <?php }else{
                  ?>
                  <li class="<?php echo $class;?>"><a href="productgridlist?<?php echo $prod_id;?>&page=<?php echo $f;?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                <?php } }?>
            </ul>
          </nav>
        </div>
        <nav>
        </nav>
      </div>
  </footer>

  <!--FINE CONTENUTO NODO-->
</div>

<?php
// Remove the "Add new comment" link on the teaser page or if the comment
// form is being displayed on the same page.
if ($teaser || !empty($content['comments']['comment_form'])) {
  unset($content['links']['comment']['#links']['comment-add']);
}
// Only display the wrapper div if there are links.
$links = render($content['links']);
if ($links):
  ?>
  <div class="link-wrapper">
    <?php print $links; ?>
  </div>
<?php endif; ?>

<?php print render($content['comments']); ?>

</div>