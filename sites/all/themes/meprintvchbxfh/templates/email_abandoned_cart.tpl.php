<?php

global $base_url, $theme_path;
$merge_url = $base_url . '/' . $theme_path;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title><?php echo $subject; ?></title>

  <style type="text/css">

    /* reset */
    #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
    .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Forces Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
    p {margin: 0; padding: 0; font-size: 0px; line-height: 0px;} /* squash Exact Target injected paragraphs */
    table td {border-collapse: collapse;} /* Outlook 07, 10 padding issue fix */
    table {border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; } /* remove spacing around Outlook 07, 10 tables */
    body {
      font-family: arial, helvetica, sans-serif;
    }

  </style>
  <!--[if gte mso 9]>
  <style>
    /* Target Outlook 2007 and 2010 */
  </style>
  <![endif]-->
</head>
<body style="width:100%; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">

    
<!-- body wrapper -->
<table cellpadding="0" cellspacing="0" border="0" style="margin:0; padding:0; width:100%; line-height: 100% !important;">
  <tr>
    <td valign="top">
      <!-- edge wrapper -->
      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="">
        <tr>
          <td valign="top">
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="">
              <tr>
                <td valign="middle" height="70" style="border-top: 3px solid #eb7a1a; background: #f0efeb">
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="">
                    <tr>
                      <td valign="top">
                        <img src="<?php echo $merge_url; ?>/images/email/logo.png" alt="MePrint" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" />
                      </td>
                      <td valign="top">
                        <h1 style="font-size: 14px; font-weight: bold; text-transform: uppercase; text-align: right">Carrello Meprint</h1>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- content wrapper -->
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="">
              <tr>
                <td valign="top" style="vertical-align: top;">
                  <!-- ///////////////////////////////////////////////////// -->

                  <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                      <td valign="top" style="vertical-align: top; padding-bottom: 20px">
                        <p style="font-size: 16px; margin-bottom: 20px; line-height:1.5;margin-top: 15px">
                          <?php echo $message; ?>
                        </p>
                        
                        
                        <p style="font-size: 16px; line-height:1.5; font-style: italic; font-weight: bold;">
                          Lo staff di MePrint.it
                        </p>
                      </td>
                    </tr>
                  </table>
                  <?php include('email_footer.tpl.php'); ?>
                  <!-- //////////// -->
                </td>
              </tr>
            </table>
            <!-- / content wrapper -->
          </td>
        </tr>
      </table>
      <!-- / edge wrapper -->
    </td>
  </tr>
</table>
<!-- / page wrapper -->
</body>
</html>
