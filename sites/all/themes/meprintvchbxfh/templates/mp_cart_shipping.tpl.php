<?php

global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
include_once DRUPAL_ROOT . '/sites/default/files/listaprovince.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );
$lista_province_json = listaprovince();
$provinces = drupal_json_decode( $lista_province_json );

global $user;
session_start();
$sessionid = session_id();

$name = '';
$surname = '';
$email = '';
$customer_type = '';
if ($user->uid != 0) {
    $propel_user = MP\UserQuery::create()->filterByUid($user->uid)->findOne();
    $customer_type = $propel_user->getUserType();
    $name = $propel_user->getName();
    $surname = $propel_user->getSurname();
    $cf = $propel_user->getFiscalCode();
    $society = $propel_user->getCompanyName();
    $type= $propel_user->getCompanyType();
    $vat = $propel_user->getVat();
    $address = $propel_user->getAddress();
    $phone = $propel_user->getPhone();
    $zipcode = $propel_user->getZipCode();
    $province = $propel_user->getProvince();
    $location = $propel_user->getCity();
    $country = $propel_user->getNation();
    $email = $user->mail;
    $reducedvat = $propel_user->getVatEasy();
  
    $shipping_user = MP\UserShippingQuery::create()->filterByUid($user->uid)->findOne();
    if( $shipping_user ){
        $shipping_recipient_name = $shipping_user->getReferencePersonName();
        $shipping_recipient_surname = $shipping_user->getReferencePersonSurname();
        $shipping_cf = $shipping_user->getFiscalCode();
        $shipping_recipient = $shipping_user->getCompanyName();
        $shipping_type = $shipping_user->getCompanyType();
        $shipping_vat = $shipping_user->getVat();

        $shipping_address = $shipping_user->getAddress();
        $shipping_phone = $shipping_user->getPhone();
        $shipping_zipcode = $shipping_user->getZipCode();
        $shipping_province = $shipping_user->getProvince();
        $shipping_location = $shipping_user->getCity();
        $shipping_country = $shipping_user->getNation();
    }
    $shipping_email = $user->mail;

    if($_SESSION['accountholder'] == ""){
        $_SESSION['accountholder'] = $recipient;
    }
    if($_SESSION['companytype'] == ""){
        $_SESSION['companytype'] = $type;
    }
    if ($_SESSION['accountholder_name'] == ""){
        $_SESSION['accountholder_name'] = $name;
    }
    if ($_SESSION['accountholder_surname'] == ""){
        $_SESSION['accountholder_surname'] = $surname;
    }
    if($_SESSION['cf'] == ""){
        $_SESSION['cf'] = $cf;
    }
    if($_SESSION['vat'] == ""){
        $_SESSION['vat'] = $vat;
    }  
    if($_SESSION['address'] == ""){
        $_SESSION['address'] = $address;
    }
    if($_SESSION['phone'] == ""){
        $_SESSION['phone'] = $phone;
    }
    if($_SESSION['zipcode'] == ""){
        $_SESSION['zipcode'] = $zipcode;
    }
    if($_SESSION['location'] == ""){
        $_SESSION['location'] = $location;
    }
    if($_SESSION['province'] == ""){
        $_SESSION['province'] = mb_strtoupper($province);
    }
    if($_SESSION['country'] == ""){
        $_SESSION['country'] = $country;
    }
    if($_SESSION['email'] == ""){
        $_SESSION['email'] = $email;
    }
  
    if($_SESSION['srecipient'] == ""){
        $_SESSION['srecipient'] = $shipping_recipient;
    }
    if($_SESSION['scompany_type'] == ""){
        $_SESSION['scompany_type'] = $shipping_type;
    }
    if ($_SESSION['srecipient_name'] == ""){
        $_SESSION['srecipient_name'] = $shipping_name;
    }
    if ($_SESSION['srecipient_surname'] == ""){
        $_SESSION['srecipient_surname'] = $shipping_surname;
    }
  //dd($shipping_recipient_name);
    if($_SESSION['scf'] == ""){
        $_SESSION['scf'] = $shipping_cf;
    }
    if($_SESSION['svat'] == ""){
        $_SESSION['svat'] = $shipping_vat;
    } 
    if($_SESSION['saddress'] == ""){
        $_SESSION['saddress'] = $shipping_address;
    }
    if($_SESSION['sphone'] == ""){
        $_SESSION['sphone'] = $shipping_phone;
    }
    if($_SESSION['szipcode'] == ""){
        $_SESSION['szipcode'] = $shipping_zipcode;
    }
    if($_SESSION['slocation'] == ""){
        $_SESSION['slocation'] = $shipping_location;
    }
    if($_SESSION['sprovince'] == ""){
        $_SESSION['sprovince'] = mb_strtoupper($shipping_province);
    }
    if($_SESSION['scountry'] == ""){
        $_SESSION['scountry'] = $shipping_country;
    }
    if($_SESSION['semail'] == ""){
        $_SESSION['semail'] = $email;
    }
    
    $_SESSION['province']= mb_strtoupper($_SESSION['province']);
    $_SESSION['sprovince']= mb_strtoupper($_SESSION['sprovince']);
}
/*else {
    drupal_goto("reglog");
}*/

  if(isset($_POST['productprice']) && $_POST['productprice'] != ''){
    $_SESSION['productprice'] = $_POST['productprice'];
  }
  if(isset($_POST['dicountamount']) && $_POST['dicountamount'] != ''){
    $_SESSION['dicountamount'] = $_POST['dicountamount'];
  }
  if(isset($_POST['finalamount']) && $_POST['finalamount'] != ''){
    $_SESSION['finalamount'] = $_POST['finalamount'];
  }
  if(isset($_POST['coupon_code']) && $_POST['coupon_code'] != ''){
    $_SESSION['coupon_code'] = $_POST['coupon_code'];
  }
  if(isset($_POST['discount_id']) && $_POST['discount_id'] != ''){
    $_SESSION['discount_id'] = $_POST['discount_id'];
  }
  if(isset($_POST['totalprice']) && $_POST['totalprice'] != ''){
    $_SESSION['totalprice'] = $_POST['totalprice'];
  }
  if(isset($_POST['ship']) && $_POST['ship'] != ''){
    $_SESSION['ship'] = $_POST['ship'];
  }
  if(isset($_POST['pack']) && $_POST['pack'] != ''){
    $_SESSION['pack'] = $_POST['pack'];
  }
  if(isset($_POST['note']) && $_POST['note'] != ''){
    $_SESSION['note'] = $_POST['note'];
  }
  if(isset($_POST['shipnote']) && $_POST['shipnote'] != ''){
    $_SESSION['shipnote'] = $_POST['shipnote'];
  }
if($user->uid == 0){

  $cart = MP\CartQuery::create()->filterBySessionId($sessionid)->filterByStatus(1)->findOne();
}else{
  $cart = MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->findOne();
}

if ($cart) {
  
  $contacts = MP\UserAddressBookQuery::create()->filterByUid($user->uid)->find();
  
  if ($user->uid != 0) {
    $user_blacklist = $propel_user->getBlacklist();
  }else{
    $user_blacklist = 0;
  }
  
if ($_SESSION['country'] == "")
    $_SESSION['country'] = "IT";
if ($_SESSION['scountry'] == "")
    $_SESSION['scountry'] = "IT";

  ?>

<script type="text/javascript">
jQuery(document).ready(function($) {
  $( "#rubrica-user-shipping" ).change(function() {    
    var contact = $('#rubrica-user-shipping').val();    
    $.ajax({
      url: "/loadRubryByValue",
      type: "POST",
      dataType: "json",
      data: {contact: contact}
    })
    .success(function(response){
       //console.log(response);
      if( response == false ) {
        $('#form-shipping__stype').val("private");
        updateshipping();
        $('#srecipient_name').val('');
        $('#srecipient_surname').val('');
        $('#srecipient').val('');
        $('#saddress').val('');
        $('#sphone').val('');
        $('#semail').val('');      
        $('#szipcode').val('');
        $('#slocation').val('');
        $('#sprovince').val('');
        $('#scountry').val('');
      } else {
        var json = JSON.parse( response );
        //console.log(json);
        $('#form-shipping__stype').val(json.Type);
        updateshipping();
        $('#srecipient_name').val(json.Name);
        $('#srecipient_surname').val(json.Surname);
        $('#srecipient').val(json.CompanyName);
        $('#form-registration__type_scompany').val(json.CompanyType);
        $('#saddress').val(json.Address);
        $('#sphone').val(json.Phone);
        $('#semail').val(json.Email);      
        $('#szipcode').val(json.ZipCode);
        $('#slocation').val(json.City);
        $('#sprovince').val(json.Province);
        $('#scountry').val(json.Nation);
      }
    });
    
  });
  
  $('#form-shipping__stype').change(function(e) {
      updateshipping();
  });
  
  $('#form-shipping__type').change(function(e) {
      if ($(this).val() == "private") {
          $('#billname').css('display','block');
          $('#billsurname').css('display','block');
          $('#billsociety').css('display','none');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','none');
      }
      if ($(this).val() == "society") {
          $('#billname').css('display','none');
          $('#billsurname').css('display','none');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','block');
          $('#billvat').css('display','block');
      }
      if ($(this).val() == "public" || $(this).val() == "association") {
          $('#billname').css('display','none');
          $('#billsurname').css('display','none');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','block');
      }
      if ($(this).val() == "individual") {
          $('#billname').css('display','block');
          $('#billsurname').css('display','block');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','block');
          $('.intermediate_particle').css('display','block');
      }
  });
  
    $('#shipping-form').submit(function(e) {
        jQuery('#errorbill').css('display', 'none');
        jQuery('#errorbill2').css('display', 'none');
        jQuery('#errorbill3').css('display', 'none');
        jQuery('#errorbill4').css('display', 'none');
        jQuery('#errorship').css('display', 'none');
        jQuery('#errorship2').css('display', 'none');
        jQuery('#errorship3').css('display', 'none');
        jQuery('#errorship4').css('display', 'none');  
        jQuery('#errorship5').css('display', 'none');       
        
        if (jQuery('#form-shipping__type').val() == "private" || jQuery('#form-shipping__type').val() == "individual") {
            if (jQuery('#accountholder_name').val() == "" || jQuery('#accountholder_surname').val() == "") {
                jQuery('#errorbill').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (!checkcf(jQuery('#cf').val())) {
                jQuery('#errorbill2').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#accountholder').val() == "") {
                jQuery('#errorbill3').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (jQuery('#vat').val() == "" || !checkcf2(jQuery('#vat').val())) {
                jQuery('#errorbill4').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (!checkcf2(jQuery('#cf').val())) {
                jQuery('#errorbill2').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        
        if (jQuery('#form-shipping__stype').val() == "private" || jQuery('#form-shipping__stype').val() == "individual") {
            if (jQuery('#srecipient_name').val() == "" || jQuery('#srecipient_surname').val() == "") {
                jQuery('#errorship').css('display', 'block');
                var topPosition = jQuery('#form-shipping__stype').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#srecipient').val() == "") {
                jQuery('#errorship3').css('display', 'block');
                var topPosition = jQuery('#form-shipping__stype').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        
        if (jQuery('#scountry').val() != "IT"){
            jQuery('#errorship5').css('display', 'block');
            var topPosition = jQuery('#form-shipping__stype').offset().top;
            jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
            return false;
        }
    });
});

function checkcf(val) {
    var regex = /[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]/;
    var v = val.match(regex);
    //console.log( 'codice fiscale valido: ' + v );
    if ( v ) { return true; }
    else { return false; }
}

function checkcf2(val) {
    var regex = /^\d+$/;
    var v = val.match(regex);
    //console.log( 'codice fiscale valido: ' + v );
    if ( v ) { 
        if (val.length == 11)
            return true; 
        else
            return false;
    }
    else { return false; }
}

function updateshipping() {
    if (jQuery('#form-shipping__stype').val() == "private") {
          jQuery('#billsname').css('display','block');
          jQuery('#billssurname').css('display','block');
          jQuery('#billssociety').css('display','none');
          jQuery('#billstypesociety').css('display','none');
      }
      if (jQuery('#form-shipping__stype').val() == "society") {
          jQuery('#billsname').css('display','none');
          jQuery('#billssurname').css('display','none');
          jQuery('#billssociety').css('display','block');
          jQuery('#billstypesociety').css('display','block');
      }
      if (jQuery('#form-shipping__stype').val() == "public" || jQuery('#form-shipping__stype').val() == "association") {
          jQuery('#billsname').css('display','none');
          jQuery('#billssurname').css('display','none');
          jQuery('#billssociety').css('display','block');
          jQuery('#billstypesociety').css('display','none');
      }
      if (jQuery('#form-shipping__stype').val() == "individual") {
          jQuery('#billsname').css('display','block');
          jQuery('#billssurname').css('display','block');
          jQuery('#billssociety').css('display','block');
          jQuery('#billstypesociety').css('display','none');
          jQuery('.intermediate_particle2').css('display','block'); 
      }
}

function fillshipping() {
    jQuery('#form-shipping__stype').val(jQuery('#form-shipping__type').val());
    updateshipping();
    jQuery('#srecipient_name').val(jQuery('#accountholder_name').val());
    jQuery('#srecipient_surname').val(jQuery('#accountholder_surname').val());
    jQuery('#srecipient').val(jQuery('#accountholder').val());
    jQuery('#form-registration__type_scompany').val(jQuery('#form-registration__type_company').val());
    jQuery('#saddress').val(jQuery('#address').val());
    jQuery('#sphone').val(jQuery('#phone').val());
    jQuery('#semail').val(jQuery('#email').val());
    jQuery('#szipcode').val(jQuery('#zipcode').val());
    jQuery('#slocation').val(jQuery('#location').val());
    jQuery('#sprovince').val(jQuery('#province').val());
    jQuery('#scountry').val(jQuery('#country').val());
    
    if (jQuery('#form-shipping__stype').val() == "private") {
        jQuery('#srecipient').val('');
    }
    else {
        if (jQuery('#form-shipping__stype').val() != "individual") {
            jQuery('#srecipient_name').val('');
            jQuery('#srecipient_surname').val('');
        }
    }
}
</script>


  <form method="POST" id="shipping-form" action="<?php echo $base_url . '/' . $lang_name; ?>/riepilogo" autocomplete="on" class="boxed">
      
    <input type="hidden" name="Shipping_Confirm" id="Shipping_Confirm" value="1" />

    <div class="clearfix"></div>
    
    <div class="legend "><span><?php echo $billing; ?></span></div>
    <div class="form-group col-sm-6">
        <label for="form-shipping__type"><?php echo $ltype_customer; ?></label>
        <select name="user_type" id="form-shipping__type" class="form-control shipform" required >
            <option value="">---</option>
            <option <?php if ($customer_type == "private") echo "selected"; ?> value="private"><?php echo t('Private'); ?></option>
            <option <?php if ($customer_type == "society") echo "selected"; ?> value="society"><?php echo t('Society'); ?></option>
            <option <?php if ($customer_type == "public") echo "selected"; ?> value="public"><?php echo t('Public'); ?></option>
            <option <?php if ($customer_type == "association") echo "selected"; ?> value="association"><?php echo t('Association'); ?></option>
            <option <?php if ($customer_type == "individual") echo "selected"; ?> value="individual"><?php echo t('Individual'); ?></option>
        </select>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type == "private") { ?> style="display: none" <?php } ?> id="billsociety"><label class="shiplabel"><?php echo $laccountholder; ?> *</label>  <input type="text" name="accountholder" id="accountholder" class="form-text shipform" value="<?php if ($_SESSION['accountholder'] != '') {echo $_SESSION['accountholder'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "society") { ?> style="display: none" <?php } ?> id="billtypesociety"><label for=""><?php echo t('type'); ?> *</label>
        <select name="society_company_type" id="form-registration__type_company" class="form-control shipform">
            <option <?php if($_SESSION['type']=="s.s") echo "selected"; ?> value="s.s">s.s</option>
            <option <?php if($_SESSION['type']=="sas") echo "selected"; ?> value="sas">sas</option>
            <option <?php if($_SESSION['type']=="snc") echo "selected"; ?> value="snc">snc</option>
            <option <?php if($_SESSION['type']=="spa") echo "selected"; ?> value="spa">spa</option>
            <option <?php if($_SESSION['type']=="srl") echo "selected"; ?> value="srl">srl</option>
            <option <?php if($_SESSION['type']=="srlu") echo "selected"; ?> value="srlu">srlu</option>
            <option <?php if($_SESSION['type']=="srls") echo "selected"; ?> value="srls">srls</option>
            <option <?php if($_SESSION['type']=="sapa") echo "selected"; ?> value="sapa">sapa</option>
            <option <?php if($_SESSION['type']=="scarl") echo "selected"; ?> value="scarl">scarl</option>
            <option value="_">altro</option>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billname">
        <div <?php if ($customer_type != "individual") { ?> style="display:none"<?php } ?>class="intermediate_particle">
            di
        </div> 
        <label class="shiplabel"><?php echo $laccountholder_name; ?> *</label>  <input type="text" name="accountholder_name" id="accountholder_name" class="form-text shipform" value="<?php if ($_SESSION['accountholder_name'] != '') {echo $_SESSION['accountholder_name'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billsurname"><label class="shiplabel"><?php echo $laccountholder_surname; ?> *</label>  <input type="text" name="accountholder_surname" id="accountholder_surname" class="form-text shipform" value="<?php if ($_SESSION['accountholder_surname'] != '') {echo $_SESSION['accountholder_surname'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type == "private") { ?> style="display: none" <?php } ?> id="billvat"><label class="shiplabel"><?php echo $lvat; ?> *</label>  <input type="text" name="vat" id="vat" class="form-text shipform" value="<?php if ($_SESSION['vat'] != '') {echo $_SESSION['vat'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billfisc"><label class="shiplabel"><?php echo $lcf; ?> *</label>  <input required type="text" name="cf" id="cf" class="form-text shipform" value="<?php if ($_SESSION['cf'] != '') {echo $_SESSION['cf'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billaddress"><label class="shiplabel"><?php echo $laddress; ?> *</label>  <input required type="text" name="address" id="address" class="form-text shipform" value="<?php if ($_SESSION['address'] != '') {echo $_SESSION['address'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billphone"><label class="shiplabel"><?php echo $lphone; ?> *</label>  <input required type="text" name="phone" id="phone" class="form-text shipform" value="<?php if ($_SESSION['phone'] != '') {echo $_SESSION['phone'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billemail"><label class="shiplabel"><?php echo $lemail; ?> *</label>  <input required type="text" name="email" id="email" class="form-text shipform" value="<?php if ($_SESSION['email'] != '') {echo $_SESSION['email'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billzip"><label class="shiplabel"><?php echo $lzipcode; ?> *</label> <input required type="text" name="zipcode" id="zipcode" class="form-text shipform" value="<?php if ($_SESSION['zipcode'] != '') {echo $_SESSION['zipcode'];} ?>"/></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billlocation"><label class="shiplabel"><?php echo $llocation; ?> *</label> <input required type="text" name="location" id="location" class="form-text shipform" value="<?php if ($_SESSION['location'] != '') {echo $_SESSION['location'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billprovince"><label class="shiplabel"><?php echo $lprovince; ?> *</label>  <!--<input type="text" name="province" id="province" class="form-text shipform" value="<?php #if ($_SESSION['province'] != '') {echo $_SESSION['province'];} ?>"/></div>-->
        <select name="province" id="province" class="form-control shipform">
        <?php 
        foreach( $provinces as $k => $provincia ){
            $selected = "";
            if( $provincia["provinceCode"] == $_SESSION['province'] ) $selected = 'selected="selected"';
            
            echo '<option value="' . $provincia["provinceCode"] . '" ' . $selected . ' >' . $provincia["provinceName"] . '</option>';
        }

        ?>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billcountry"><label class="shiplabel"><?php echo $lcontry; ?> *</label>  <!--<input type="text" name="country" id="country" class="form-text shipform" value="<?php #if ($_SESSION['country'] != '') {echo $_SESSION['country'];} ?>"/>-->
      <select name="country" id="country" class="form-control shipform">                  
        <?php
          foreach( $nazioni as $k => $nazione ){
            $selected = "";
            if( $nazione["countryCode"] == $_SESSION['country'] ) $selected = 'selected="selected"';

            echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
          }
        ?>
      </select>
    </div> 
    <div class="error-msg" id="errorbill" style="display:none; color:red"> <?php echo t('Name and Surname are mandatory. Please complete your data.'); ?> </div>
    <div class="error-msg" id="errorbill2" style="display:none; color:red"> <?php echo t('Fiscal Code Format Error. Please review your data.'); ?> </div>
    <div class="error-msg" id="errorbill3" style="display:none; color:red"> <?php echo t('Society Name is mandatory. Please complete your data'); ?> </div>
    <div class="error-msg" id="errorbill4" style="display:none; color:red"> <?php echo t('Vat is mandatory and must be well formed. Please complete your data'); ?> </div>
    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div id="shipdetailsform" style="overflow: hidden">
      
      <!--<div class="col-sm-12 col-xs-12 legend">
          <span onclick="copyaddress()" style="cursor:pointer" ><?php echo t("Use billing address for shipping"); ?></span>
      </div>-->
    <?php if( user_is_logged_in() ): ?>
      <div class="legend "><span><?php echo $lrubrica; ?></span></div>
      <div class="col-sm-6 col-xs-12">
        <select id="rubrica-user-shipping" class="rubrica-user form-control shipform" name="rubrica_user_shipping">
        <option value="" ><?php echo $lcontact; ?></option>
        <?php foreach($contacts as $contact): ?>
          <option value="<?php echo $contact->getId(); ?>"><?php echo $contact->getAddressBookName(); ?></option>
        <?php endforeach; ?>
      </select>
      </div> 

    <?php endif; ?>
        <div class="clearfix"></div>
      <div class="legend "><span ><?php echo $shipping; ?></span> </div> 
      <div class="col-sm-12 col-md-12 col-xs-12">
        <span onclick="fillshipping()" style="cursor:pointer; text-decoration:underline;" ><?php echo $sameasbill; ?></span>
      </div>
      <div class="form-group col-sm-6">
        <label for="form-shipping__type"><?php echo $ltype_customer; ?></label>
        <select name="suser_type" id="form-shipping__stype" class="form-control shipform" required >
            <option value="">---</option>
            <option <?php if ($customer_type == "private") echo "selected"; ?> value="private"><?php echo t('Private'); ?></option>
            <option <?php if ($customer_type == "society") echo "selected"; ?> value="society"><?php echo t('Society'); ?></option>
            <option <?php if ($customer_type == "public") echo "selected"; ?> value="public"><?php echo t('Public'); ?></option>
            <option <?php if ($customer_type == "association") echo "selected"; ?> value="association"><?php echo t('Association'); ?></option>
            <option <?php if ($customer_type == "individual") echo "selected"; ?> value="individual"><?php echo t('Individual'); ?></option>
        </select>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type == "private") { ?> style="display: none" <?php } ?> id="billssociety"><label class="shiplabel"><?php echo $laccountholder; ?></label>  <input type="text" name="srecipient" id="srecipient" class="form-text shipform" value="<?php if ($_SESSION['srecipient'] != '') {echo $_SESSION['srecipient'];} ?>" /></div>
        <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "society") { ?> style="display: none" <?php } ?> id="billstypesociety"><label for=""><?php echo t('type'); ?></label>
        <select name="scompany_type" id="form-registration__type_scompany" class="form-control shipform">
            <option <?php if($_SESSION['stype']=="s.s") echo "selected"; ?> value="s.s">s.s</option>
            <option <?php if($_SESSION['stype']=="sas") echo "selected"; ?> value="sas">sas</option>
            <option <?php if($_SESSION['stype']=="snc") echo "selected"; ?> value="snc">snc</option>
            <option <?php if($_SESSION['stype']=="spa") echo "selected"; ?> value="spa">spa</option>
            <option <?php if($_SESSION['stype']=="srl") echo "selected"; ?> value="srl">srl</option>
            <option <?php if($_SESSION['stype']=="srlu") echo "selected"; ?> value="srlu">srlu</option>
            <option <?php if($_SESSION['stype']=="srls") echo "selected"; ?> value="srls">srls</option>
            <option <?php if($_SESSION['stype']=="sapa") echo "selected"; ?> value="sapa">sapa</option>
            <option <?php if($_SESSION['stype']=="scarl") echo "selected"; ?> value="scarl">scarl</option>
            <option value="_">altro</option>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billsname">
        <div <?php if ($customer_type != "individual") { ?> style="display:none"<?php } ?>class="intermediate_particle2">
            di
        </div> <label class="shiplabel"><?php echo $laccountholder_name; ?></label>  <input type="text" name="srecipient_name" id="srecipient_name" class="form-text shipform" value="<?php if ($_SESSION['srecipient_name'] != '') {echo $_SESSION['srecipient_name'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billssurname"><label class="shiplabel"><?php echo $laccountholder_surname; ?></label>  <input type="text" name="srecipient_surname" id="srecipient_surname" class="form-text shipform" value="<?php if ($_SESSION['srecipient_surname'] != '') {echo $_SESSION['srecipient_surname'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billsaddress"><label class="shiplabel"><?php echo $laddress; ?> *</label>  <input required type="text" name="saddress" id="saddress" class="form-text shipform" value="<?php if ($_SESSION['saddress'] != '') {echo $_SESSION['saddress'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billsphone"><label class="shiplabel"><?php echo $lphone; ?> *</label>  <input required type="text" name="sphone" id="sphone" class="form-text shipform" value="<?php if ($_SESSION['sphone'] != '') {echo $_SESSION['sphone'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billsemail"><label class="shiplabel"><?php echo $lemail; ?> *</label>  <input required type="text" name="semail" id="semail" class="form-text shipform" value="<?php if ($_SESSION['semail'] != '') {echo $_SESSION['semail'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billszip"><label class="shiplabel"><?php echo $lzipcode; ?> *</label> <input required type="text" name="szipcode" id="szipcode" class="form-text shipform" value="<?php if ($_SESSION['szipcode'] != '') {echo $_SESSION['szipcode'];} ?>"/></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billslocation"><label class="shiplabel"><?php echo $llocation; ?> *</label> <input required type="text" name="slocation" id="slocation" class="form-text shipform" value="<?php if ($_SESSION['slocation'] != '') {echo $_SESSION['slocation'];} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billsprovince"><label class="shiplabel"><?php echo $lprovince; ?> *</label>  <!--<input type="text" name="province" id="province" class="form-text shipform" value="<?php #if ($_SESSION['province'] != '') {echo $_SESSION['province'];} ?>"/></div>-->
        <select name="sprovince" id="sprovince" class="form-control shipform">
        <?php 
        foreach( $provinces as $k => $provincia ){
            $selected = "";
            if( $provincia["provinceCode"] == $_SESSION['sprovince'] ) $selected = 'selected="selected"';
            
            echo '<option value="' . $provincia["provinceCode"] . '" ' . $selected . ' >' . $provincia["provinceName"] . '</option>';
        }

        ?>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billcountry"><label class="shiplabel"><?php echo $lcontry; ?> *</label>  <!--<input type="text" name="country" id="country" class="form-text shipform" value="<?php #if ($_SESSION['country'] != '') {echo $_SESSION['country'];} ?>"/>-->
      <select name="scountry" id="scountry" class="form-control shipform">                  
        <?php
          foreach( $nazioni as $k => $nazione ){
            $selected = "";
            if( $nazione["countryCode"] == $_SESSION['scountry'] ) $selected = 'selected="selected"';

            echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
          }
        ?>
      </select>
    </div> 
    <div class="error-msg" id="errorship" style="display:none; color:red"> <?php echo t('Name and Surname are mandatory. Please complete your data.'); ?> </div>
    <div class="error-msg" id="errorship2" style="display:none; color:red"> <?php echo t('Fiscal Code Format Error. Please review your data.'); ?> </div>
    <div class="error-msg" id="errorship3" style="display:none; color:red"> <?php echo t('Society Name is mandatory. Please complete your data'); ?> </div>
    <div class="error-msg" id="errorship4" style="display:none; color:red"> <?php echo t('Vat is mandatory and must be well formed. Please complete your data'); ?> </div>
    <div class="error-msg" id="errorship5" style="display:none; color:red"> <?php echo t('Shippings outside Italy are momentarily forbidden'); ?> </div>
    <div class="clearfix"></div>
    </div>
    
    <div id="noteform" style="overflow: hidden">
      <div class="legend "><span ><?php echo $note; ?></span> </div>
      <div class="clearfix"></div>
      <div class="col-sm-12 col-md-12 col-xs-12"><textarea name="note" id="note" rows="2" cols="95"><?php if ($_SESSION['note'] != '') {echo $_SESSION['note'];} ?></textarea></div>
      <div class="legend "><span ><?php echo t('Note Shipping'); ?></span> </div>
      <div class="clearfix"></div>
      <div class="col-sm-12 col-md-12 col-xs-12"><textarea name="shipnote" id="shipnote" rows="2" cols="95"><?php if ($_SESSION['shipnote'] != '') {echo $_SESSION['shipnote'];} ?></textarea></div>
    </div>
    <?php if ($reducedvat == 1) { ?>
    <div id="vatform" style="overflow: hidden">
      <div class="legend "><span ><?php echo $vat4; ?> <input name="vat4" id="vat4" type="checkbox"/></span> </div>
     </div>
    <?php } ?>
    <div class="clearfix"></div>
    <div class="space-2"></div>
      
    <!-- Shipping Company Details --->
    <div class="legend "><span><?php echo $Shippingtype; ?></span></div>
    <div class="col-md-12 col-sm-12 col-xs-12">

      <?php
      $shippingName = MP\ShippingCompaniesQuery::create()->filterByStatus(1)->find();
      $s = 1;
      foreach ($shippingName as $shipname) {
        $ship_img_fid = $shipname->getFile();
        $ship_img_file = file_load($ship_img_fid);
        $ship_img_url = file_create_url($ship_img_file->uri);
        ?>
            <div class="shipzone">
              <label class="dym-p dimdiv dimdiv--shipping">
                <input type="radio" name="shipping" id="shipping_<?php echo $shipname->getShippingId(); ?>" <?php if ($s == 1): ?>checked="checked"<?php endif; ?> value="<?php echo $shipname->getShippingId(); ?>" />
                <img src="<?php echo $ship_img_url; ?>" alt=""/>
                <p><?php echo $shipname->getName(); ?></p>
              </label>
            </div>
        <?php
        $s++;
      }
      ?>

    </div>
    <div id="noteform" style="overflow: hidden">
      
    </div>
    <!-- Anonymous Shipping Start  -->
    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div class="legend "><span><?php echo $anonoshiping; ?></span></div>

    <div class="col-md-12 col-sm-12 col-xs-12">
      <label class="dym-p dimdiv dimdiv--anonimous">
        <input type="checkbox" name="anonshipping" id="annonshipping" value="1">
        <span class="fa fa-eye-slash fa-4x"></span><p class="dimdiv--anonimous__label"><?php echo $anonoshiping; ?><br /><span><?php echo $anonoshipingtext; ?></span></p>
      </label>     
    </div>
    <!-- Anonymous Shipping Start End  -->

    <div class="clearfix"></div>
    <div class="space-2"></div>
    <div class="legend"> <span><?php echo $paymentoptions; ?></span></div>
    
    <div id="payment-wrapper" >        
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="paypal" value="paypal" checked="checked" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/paypal_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo base_path() . path_to_theme() ?>/images/paypal_icon.png" alt="" /><p><?php echo t('If you have chosen a formula of advance payment (Credit Card, Paypal, etc.) you can cancel your order until you have not made the payment. For all other types of payment order can not be undone.'); ?></p></div>' />
            <p><?php echo t('credit card and paypal'); ?></p>
          </label>
        </div>
        
        <?php
         $shipcost= MP\ShippingCostSettingsQuery::create()->filterByStatus(1)->findOne();
          /* CONTROLLO BLACKLIST */ 
          // se l'utente è in BLACKLIST non può pagare in contrassegno
         //dd(($_SESSION['iva']+$_SESSION['totalprice']+$_SESSION['ship']+$_SESSION['pack']) . " - " . $shipcost->getMaxLimitOrderCashDelivery());
          if(!$user_blacklist && $_SESSION['iva']+$_SESSION['totalprice']+$_SESSION['ship']+$_SESSION['pack']<$shipcost->getMaxLimitOrderCashDelivery()){
        ?>
            <div class="text-center">
              <label class="dimdiv dimdiv--circle">
                <input type="radio" name="payment" class="payment_class" id="contrassegno" value="contrassegno" />
                <img src="<?php echo base_path() . path_to_theme() ?>/images/contrassegno_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo base_path() . path_to_theme() ?>/images/contrassegno_icon.png' alt='' /><p><?php echo t('If you have chosen to pay for their delivery, you can cancel the order as long as you have not uploaded at least one file.'); ?></p></div>" />
                <p><?php echo t('cash'); ?></p>
              </label>
            </div>
        <?php
          }
        ?>
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="banktransfer" value="banktransfer" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/banktransfer_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo base_path() . path_to_theme() ?>/images/banktransfer_icon.png' alt='' /><p><?php echo t('For payments by bank transfer printing it will start only after the amount has been credited, regardless of sending the cro or payment slip. Therefore the delivery slip of 2-3 days, plus the time for printing and shipping. Also remember that you must enter the order number in the description of payment to speed up the transaction.'); ?></p></div>" />
            <p><?php echo t('bank transfer'); ?></p>
          </label>
        </div>
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="payment" class="payment_class" id="sofort" value="sofort" />
            <img src="<?php echo base_path() . path_to_theme() ?>/images/sofort_icon.png" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo base_path() . path_to_theme() ?>/images/sofort_icon.png" alt="" /><p><?php echo t('If you have chosen a formula of advance payment (Credit Card, Paypal, etc.) you can cancel your order until you have not made the payment. For all other types of payment order can not be undone.'); ?></p></div>' />
            <p>sofort</p>
          </label>
        </div>
        
      </div>
    
    <div class="space-2"></div>
    <div class="clearfix"></div>
    <div class="space-2"></div>
    
    <div class="col-md-6 col-sm-6 continue">
      <a href="<?php echo $base_url . '/' . $lang_name; ?>/carrello">
        <span class="btn"><?php echo $backtocart; ?></span>
      </a>
    </div>
    <div class="col-md-6 col-sm-6 payment">
      <button class="btn btn-success" type="submit" name="proceedtoshipping" value="proceedtoshipping"><?php echo $Proceedtoship; ?></button>
    </div>

  </form><?php
} else {
  echo '<h4>' . $noproductsavail . '</h4>';
}?>