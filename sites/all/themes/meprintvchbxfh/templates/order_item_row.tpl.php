<?php
/*
 * Informazioni di setup per l'editor
 */

$form_id = $order_item->getFormId();
$bigresult_findone = MP\FormoneBigFormatQuery::create()->findPk($form_id);

?>

<input type="hidden" value="<?php echo $bigresult_findone->getEditorDpi(); ?>"  id="editor_dpi_<?php echo $order_item_id; ?>">
<input type="hidden" value="<?php echo $bigresult_findone->getEditorCuttingGrid(); ?>"  id="editor_cutting_grid_<?php echo $order_item_id; ?>">
<input type="hidden" value="<?php echo $bigresult_findone->getEditorColorsType(); ?>"  id="editor_colors_type_<?php echo $order_item_id; ?>">
<input type="hidden" value="<?php echo $bigresult_findone->getEditorColors(); ?>"  id="editor_colors_<?php echo $order_item_id; ?>">

<div class="cartdetailspage">
  <!-- nome prodotto --> 
  <div class="col-md-8 col-sm-8 col-xs-7"> 
    <div class="cartdetails_4">
      <strong class="cartdetails--open" data-toggle="collapse" href="#cart_<?php echo $order_item_id; ?>" aria-expanded="false">
        <i class="fa fa-1x fa-plus collapsed" ></i>
        <span><?php echo $presult->getName(); ?></span>
      </strong>
    </div>    
  </div>
  <!-- quantita -->
  <div class="col-md-2 col-sm-2 col-xs-2 cartdetailsline">
    <div class="cartdetails text-center">
      <span><?php echo $order_item->getNoofElements(); ?></span>
    </div>
  </div>
  <!-- prezzo prodotto -->
  <div class="col-md-2 col-sm-2 col-xs-3 cartdetailsline">
    <div class="cartdetails cartprice text-right">
      <?php
        $totlprc = $order_item->getPrice();
        $prc =  number_format((float) $totlprc, 2, '.', '') . ' &euro;';
      ?>
      
      <span><?php echo $prc; ?></span>
    </div>
  </div>
  
  <!-- dettaglio collapse con files già presenti -->
  <div class="col-sm-12 col-xs-12 detail-collapsed">
    <div style="height: 0px;" aria-expanded="false" class="collapse" id="cart_<?php echo $order_item_id; ?>">

      <!-- NUMERO DI COPIE -->
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
        <span class="labelcart"><?php echo $nocopies; ?></span>
        <span class="datacart"><?php echo $order_item->getNoofElements(); ?></span>
      </div>

      <!-- DIMENSIONI -->      
      <?php
        if ($order_item->getDimensionId() != "custom") {
          $dquery = MP\DimensionsQuery::create()->filterByDimensionId($order_item->getDimensionId())->findOne();
          $size = $dquery->getWidth() . '*' . $dquery->getHeight();
          
          $productHeight = $dquery->getHeight();
          $productWidth = $dquery->getWidth();
        }
        else {
          $size = $order_item->getWidth() . "*" . $order_item->getHeight();
          
          $productHeight = $order_item->getHeight();
          $productWidth = $order_item->getWidth();
                
        }
      ?>
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
        <span class="labelcart"><?php echo $cdimension; ?></span>
        <span class="datacart"><?php echo $size; ?></span>
        
        <input id="productHeight_<?php echo $order_item_id; ?>" type="hidden" value="<?php echo $productHeight ?>">
        <input id="productWidth_<?php echo $order_item_id; ?>" type="hidden" value="<?php echo $productWidth ?>">
        
      </div>

      <!-- MATERIALE -->
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
        <span class="labelcart"><?php echo $cmaterial; ?></span>
        <span class="datacart">
          <?php
          $mquery = MP\MaterialsLangQuery::create()->filterByMaterialId($order_item->getMaterialId())->filterByLanguageId($lang_name)->findOne();
          echo $mquery->getName();
          ?>
        </span>
      </div>
      
      <?php if ($order_item->getProcessingId() != 11) { ?>
      <!-- LAVORAZIONE BASE -->
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
        <span class="labelcart"><?php echo $processing; ?></span>
        <span class="datacart">
          <?php
          $pquery = MP\ProcessingLangQuery::create()->filterByProcessingId($order_item->getProcessingId())->filterByLanguageId($lang_name)->findOne();

          $base_processing = MP\ProcessingQuery::create()->findPk($order_item->getProcessingId());

          echo $pquery->getName();

          $is_special = $base_processing->getIsSpecial();
          $is_extra_side_req = $base_processing->getIsExtra();
            
          if ($is_extra_side_req == 1) {
            if ($order_item->getProcessingTop() == 1) {
              echo " " . t("Top side") . " ";
            }
            if ($order_item->getProcessingBottom() == 1) {
              echo " " . t("Bottom side") . " ";
            }
            if ($order_item->getProcessingLeft() == 1) {
              echo " " . t("Left side") . " ";
            }
            if ($order_item->getProcessingRight() == 1) {
              echo " " . t("Right side") . " ";
            }
          }
          
          if ($is_special == 1) {
            $special_type = $base_processing->getSpecialType();            
            echo t("Distance") . ": " . $order_item->getProcessingDistanceSpecial() . " cm";            
            echo " " . t("Number") . ": " . $order_item->getProcessingNumberSpecial();
          }
          
          
          ?>
           
        </span>
      </div>
      <?php } ?>
      
      <?php if ( $order_item->getCheckUploadedFiles() >= 1) { ?>
          <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
          <span class="labelcart"><?php echo t('Verifica File'); ?></span><span class="datacart"><?php 
          if ($order_item->getCheckUploadedFiles() == 1) {   
            echo t("Verifica in corso"); 
          }
          else if ($order_item->getCheckUploadedFiles() == 2) {   
            echo t("File verificato e corretto"); 
          } 
          else {
              echo t("File verificato e segnalato: ") . "<br>".$order_item->getOperatorNotes();
          }
        ?></span></div>
        <?php } ?>  
      
      <!-- LAVORAZIONE EXTRA -->
      <?php 
        $order_item_extra_processings = MP\OrderElementExtraProcessingQuery::create()->filterByOrderElementId( $order_item_id )->find();
        if (count($order_item_extra_processings) > 0) {
      ?>
        <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
          <span class="labelcart"><?php echo $extraprocessing; ?></span>
          
          
          <?php
          foreach ($order_item_extra_processings as $order_item_extra_processing) {
            ?>
            <span class="datacart">
              <?php
              $extra_processing_lang = MP\ProcessingLangQuery::create()->filterByProcessingId($order_item_extra_processing->getProcessingId())->filterByLanguageId($lang_name)->findOne();
              $extra_processing = MP\ProcessingQuery::create()->findPk($order_item_extra_processing->getProcessingId());

              $is_special = $extra_processing->getIsSpecial();

              $is_extra_side_req = $extra_processing->getIsExtra();

              echo $extra_processing_lang->getName();

              if ($is_extra_side_req == 1) {
                if ($order_item_extra_processing->getExtraProcessingTop() == 1) {
                  echo ", <em>" . t("Top side") . "</em>";
                }
                if ($order_item_extra_processing->getExtraProcessingBottom() == 1) {
                  echo ", <em>" . t("Bottom side") . "</em>";
                }
                if ($order_item_extra_processing->getExtraProcessingLeft() == 1) {
                  echo ", <em>" . t("Left side") . "</em>";
                }
                if ($order_item_extra_processing->getExtraProcessingRight() == 1) {
                  echo ", <em>" . t("Right side") . "</em>";
                }
              }

              if ($is_special == 1) {
                $special_type = $extra_processing->getSpecialType();
                echo ', <em>' . t("Distance") . ": " . $order_item_extra_processing->getDistanceSpecial() . " cm</em>";
                echo ", <em>" . t("Number") . ": " . $order_item_extra_processing->getNumberSpecial() . " </em>";
              }
              
              ?>
            </span>
            <?php
          }
          ?>
        </div>
      <?php } ?>
        
      <!-- EVENTUALE STRUTTURA -->
      <?php      
      if ($order_item->getStructureId() != 0) {
        ?>        
        <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
          <span class="labelcart"><?php echo $accessorie_base; ?></span>
          <span class="datacart">
            <?php
            $structure = MP\AccessoriesLangQuery::create()->filterByAccessoryId($order_item->getStructureId())->filterByLanguageId($lang_name)->findOne();
            $structure_name = $structure->getName();
            echo $structure_name;
            ?>
 
          </span>
        </div>
      <?php } ?>
        
      <!-- EVENTUALE ACCESSORI -->
      <?php
      
      $order_item_accessories = MP\OrderElementAccessoryQuery::create()->filterByOrderElementId( $order_item_id )->find();
      if (count($order_item_accessories) > 0) {
      
        ?>
      <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
        <span class="labelcart"><?php echo $accessorie; ?></span>
        
        <?php
          foreach ($order_item_accessories as $order_item_accessory) {
            ?>
            <span class="datacart">
              <?php
              $accessory_lang = MP\AccessoriesLangQuery::create()->filterByAccessoryId($order_item_accessory->getAccessoriesId())->filterByLanguageId($lang_name)->findOne();
              $accessory_name = $accessory_lang->getName();
              $accessory_quantity = $order_item_accessory->getQuantity();
              echo $accessory_name . " (" . $accessory_quantity . ")";
              ?>
            </span>
            <?php
          }
          ?>       
      </div>
      <?php } ?>
      
      <!-- FILES TEMA -->
      <?php
      
      $how_form = MP\FormoneBigFormatQuery::create()->filterByFormId( $order_item->getFormId() )->findOne();
      
      $files = $how_form->getNumberOfFilesTobeUploaded();
      if ($order_item->getMaterialId() == 43)
          $files = 0;
      
      $images = MP\OrderElementsFilesQuery::create()->filterByOrderElementId( $order_item_id )->find();
      $images_count = count( $images );
      
      $img = array();
      $e = 0;
      foreach ( $images as $m => $image ){
        $img_url = file_load($image->getFileId())->uri;
        $img[$m + 1]['filename'] = str_replace('public://cart/', '', $image->getFilename());
                $img[$m + 1]['path'] = image_style_url('gallery-thumb', $img_url);
                $img[$m + 1]['status'] = $image->getFileStatus();
                $img[$m + 1]['message'] = $image->getErrorMessage();
                $img[$m + 1]['fid'] = $image->getFileId();
                $img[$m + 1]['previewfid'] = $image->getPreviewFid();
                
                $img[$m + 1]['source'] = $image->getSource();
          
        if( $image->getFileStatus() == 0) $e++;
      }
      //print_r( $img ) ;
      //echo 'error: ' . $e;
      $lost_files = $files - $images_count;

      ?>
      
      
              <div class="datacart--files">

            <?php
            if ($files != 0) {
                //echo '<div class="box-messaggi box-messaggi-bigformat"><p>Per questo prodotto servono <span class="change_number_files">'. $files .'</span> files</p></div>';
                for ($i = 0; $i < $files; $i++) {
                    ?>
                
            <div id="upi-<?php echo "$order_item_id$i"; ?>" class="uploadproductimage 
                 <?php if ($i < $images_count) { ?>
                 uploadproductimage--selected
                 <?php } ?>">
                <?php if ($img[$i + 1]['status'] == 1 && $img[$i + 1]['message'] != "" && $img[$i + 1]['subject'] != "") { ?> 
                    <div class="col-md-12 col-sm-12 col-xs-12 cartdetails">
                        <span class="labelcart red"><?php echo t("File Segnalato"); ?></span>
                        <span class="datacart red"><?php echo $img[$i + 1]['subject'] . ": ". $img[$i + 1]['message']; ?></span>
                    </div>
                    <?php 
                    $errorfile++;
                } ?> 
                <input type="hidden" name="editor_save_img_data_<?php echo "$order_item_id$i"; ?>" id="editor_save_img_data_<?php echo "$order_item_id$i"; ?>" value="" />

                <div class="pull-left">            
                    <div <?php if ($i < $images_count) { ?> style="display: none" <?php } ?>  id="image_<?php echo "$order_item_id$i"; ?>" class="fileuploader" c="<?php echo "$order_item_id$i"; ?>"><?php echo t('upload') ?></div>
                    <input id="image_fid_<?php echo "$order_item_id$i"; ?>" class="upload_image_fid upload_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 1) {
                            echo $img[$i + 1]['fid'];

                           } ?>"/>
                    <input id="gallery_fid_<?php echo "$order_item_id$i"; ?>" class="gallery_image_fid gallery_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 0) {
                            echo $img[$i + 1]['fid'];

                           } ?>"/>
                    <input id="editor_fid_<?php echo "$order_item_id$i"; ?>" class="editor_image_fid editor_image_fid<?php echo "$order_item_id"; ?>" type="hidden" value="<?php if ($i < $images_count && $img[$i + 1]['source'] == 2) {
                            echo $img[$i + 1]['fid'];

                           } ?>" data-preview="<?php if ($i < $images_count && $img[$i + 1]['source'] == 2) {
                            echo $img[$i + 1]['previewfid'];

                           } ?>"/>
                    <div class="dvPreview_<?php echo "$order_item_id$i"; ?>"></div>
                </div>
                <?php if ($order_item->getProductionStatusId() < 4) { ?>
                <!--<a id="custom-file-theme-<?php echo "$order_item_id$i"; ?>" class="custom-file-theme btn btn-orange" data-toggle="modal" data-target="#exampleModal">
                    <?php if ($i < $images_count) { echo t('change theme'); } else { echo $lchoosetheme; } ?>
                </a>
                <a id="custom-file-editor-<?php echo "$order_item_id$i"; ?>" class="btn btn-success btn-editor custom-file-editor" data-toggle="modal" data-target="#editorModal" data-orderid="<?php echo "$order_item_id"; ?>"><i class="fa fa-paint-brush"></i>
                    <?php if ($i < $images_count) { echo t('change theme with editor'); } else { echo $leditor; } ?>
                </a>-->
                <?php } ?>
                <div class="ImageError_<?php echo "$order_item_id$i"; ?> imageerror"></div>
                <?php if ($i < $images_count) { ?>
                    <img id="load-img-<?php echo "$order_item_id$i"?>-<?php echo $img[$m + 1]['fid'] ?>" class="galleryimage--choose" src="<?php echo $img[$i + 1]['path'] ?>">
                    
                    <?php if ($order_item->getProductionStatusId() < 4) { ?>
                        
                            <?php if ($img[$i + 1]['source'] == 0) { ?>
                            <a class="uploadproductimage__remove">
                            <?php } else if ($img[$i + 1]['source'] == 1) { ?>
                            <a class="uploadproductimage__remove2">
                            <?php } else { ?>
                            <a class="uploadproductimage__remove3">
                            <?php } ?>
                                <span class="fa fa-trash"></span>
                                <?php echo t('remove image'); ?>
                            </a>
                    <?php } ?>
                <?php } ?>
            </div>

    <?php
    }
}
?>

            </div>
      
            <div>
        <div class="btn-add-cart">
            <?php echo t('Clicca su invia file per completare l\'operazione'); ?>
            <button class="orderproceed btn btn-success pull-right" data-nfiles="<?php echo $files; ?>" type="submit" name="orderproceed" value="Proceed" id="orderproceed_<?php echo $order_item_id; ?>" c="<?php echo $order_item_id; ?>"><?php echo t('Invia File'); ?></button>
        </div>
    </div>
      
    </div>
      
  </div>
  
  <!-- alert file non caricati -->
  
  <div class="col-md-12 col-sm-12 col-xs-12 box-messaggi box-messaggi<?php echo "$order_item_id"; ?>">
    <?php if( $lost_files > 0 ) :?>
      <p>
      <?php echo t('You have not entered all the required files.'); ?>
    <?php if( $e > 0 ) : ?>
      <br /><?php echo t('There are files incorrect.'); ?>
    <?php endif; ?>
    </p>
    <?php endif; ?>
  </div>
  
  
  <div class="clearfix"></div>
  
  
  
</div>

