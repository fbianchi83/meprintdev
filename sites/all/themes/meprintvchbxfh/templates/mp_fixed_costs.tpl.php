<input type="hidden" name="fixed_costs" value="1" id="fixed_costs" />
<?php
$i = 1;
if( count($fixed_costs) > 1 ){
  foreach ($fixed_costs as $fixed_cost) {
    /* @var $fixed_cost MP\MaterialsFixedCosts */
    $dname = MP\DimensionsQuery::create()->findPk($fixed_cost->getDimensionId());
    $price = $fixed_cost->getPrice();
    $image = $dname->getFile();
    $uuu = file_load($image)->uri;
    //$my_image = explode("://", $uuu);
    if( $uuu != '' ){
        $my_image = image_style_url("dimensioni", $uuu);        
      }
    ?>
    <div class="form-group col-md-3 col-sm-4 col-xs-6 text-center">
        <?php if ($my_image != '') { ?>
          <label class="dimdiv">
              <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>              
              <img src="<?php echo $my_image; ?>" />
              <p><strong><?php echo $dname->getDescription(); ?></strong><br>
            <!--<strong><?php //echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>-->
              <input type="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id = "bd<?php echo $dname->getDimensionId(); ?>">
              <input type="hidden" value="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
          </label>
      </div>
      <input type="hidden" name="fc_price_<?php echo $dname->getDimensionId(); ?>" id="fc_price_<?php echo $dname->getDimensionId(); ?>" value="<?php echo $price; ?>" />
    <?php } else {
      ?>
      <label class="dimdiv">
          <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>          
          <img src="/sites/default/files/camera.jpg"/>
          <p><strong><?php echo $dname->getDescription(); ?></strong><br>
          <!--<strong><?php //echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>-->
          <input type ="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id = "bd<?php echo $dname->getDimensionId(); ?>">
          <input type="hidden" value="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
      </label>
      </div>
      <input type="hidden" name="fc_price_<?php echo $dname->getDimensionId(); ?>" id="fc_price_<?php echo $dname->getDimensionId(); ?>" value="<?php echo $price; ?>" />
    <?php
    }

    $i++;
  }

} else {
    
  $fixed_costs_data = $fixed_costs->getData();
  $fixed_cost = $fixed_costs_data[0];
  
  $dname = MP\DimensionsQuery::create()->findPk($fixed_cost->getDimensionId());
  $image = $dname->getFile();
  $price = $fixed_cost->getPrice();
    
  $uuu = file_load($image)->uri;
  //$my_image = explode("://", $uuu);

?>  
  <div class="dimdiv--only dimdiv--circle-only col-sm-12">
    <input type="hidden" name="fc_price_<?php echo $dname->getDimensionId(); ?>" id="fc_price_<?php echo $dname->getDimensionId(); ?>" value="<?php echo $price; ?>" />    
    <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" checked="checked" />
    <img class="img-responsive pull-left" src="<?php echo image_style_url("thumbnail-new", $uuu); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $uuu); ?>' alt='' /><p><?php  echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></p></div>" />
    <input type="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight(); ?>" id = "bd<?php echo $dname->getDimensionId(); ?>">
    <input type="hidden" value="<?php echo $dname->getMaskFile(); ?>" id="bd_mask_file_<?php echo $dname->getDimensionId(); ?>" >
    <div class="dimdiv">
      <p class="mat-title"><?php echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></p>      
    </div>
    
  </div>
  
<?php }
