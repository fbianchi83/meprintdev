<?php
global $user;
global $base_url;
global $language;
$lang_name = $language->language;
?>
<?php
include('frame_modal.tpl.php');
?>

<ul class="topnav" id="main-menu-user">
  <li class="selected"><a href="<?php echo $base_url . '/' . $lang_name; ?>/user">Mon MePrint</a></li>
  <?php
    global $user;
    global $base_url;
    //$rid = db_query('SELECT rid FROM {users_roles} where uid=' . $user->uid)->fetchField();
  
  ?>
    <li><a href="<?php echo $base_url . '/' . $lang_name; ?>/user/my-profile">Mon Compte</a></li>
    <!--<li> <a href="<?#php echo $base_url . '/' . $lang_name; ?>/user/<?#php echo $user->uid; ?>/change-password">Changer le mot de passe</a></li>      -->
    <li> <a href="<?php echo $base_url . '/' . $lang_name; ?>/user/change-password">Changer le mot de passe</a></li>
    <li><a href="<?php echo $base_url . '/' . $lang_name; ?>/preferiti">Ma liste d'envies</a></li>
    <li><a href="<?php echo $base_url . '/' . $lang_name; ?>/my-orders/view">Mes Commandes</a></li>
    <li><a href="<?php echo $base_url . '/' . $lang_name; ?>/carrello">Mon panier</a></li>
    <li><a href="<?php echo $base_url . '/' . $lang_name; ?>/user/rubrica">Mon carnet d'adresses</a></li>
  
</ul>
<br>