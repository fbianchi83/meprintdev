
<?php
//print_r( $contacts );
global $language;
include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
include_once DRUPAL_ROOT . '/sites/default/files/listaprovince.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );
$lista_province_json = listaprovince();
$provinces = drupal_json_decode( $lista_province_json );

?>
<script type="text/javascript">
    
jQuery(document).ready(function($) {
    $('#form-shipping__type').change(function(e) {
      if ($(this).val() == "private") {
          $('#billname').css('display','block');
          $('#billsurname').css('display','block');
          $('#billsociety').css('display','none');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','none');
      }
      if ($(this).val() == "society") {
          $('#billname').css('display','none');
          $('#billsurname').css('display','none');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','block');
          $('#billvat').css('display','block');
      }
      if ($(this).val() == "public" || $(this).val() == "association") {
          $('#billname').css('display','none');
          $('#billsurname').css('display','none');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','block');
      }
      if ($(this).val() == "individual") {
          $('#billname').css('display','block');
          $('#billsurname').css('display','block');
          $('#billsociety').css('display','block');
          $('#billtypesociety').css('display','none');
          $('#billvat').css('display','block');
      }
  });
  
  $('#form-contacts-edit').submit(function(e) {
        jQuery('#errorbill').css('display', 'none');
        jQuery('#errorbill2').css('display', 'none');
        jQuery('#errorbill3').css('display', 'none');
        jQuery('#errorbill4').css('display', 'none');
        
        if (jQuery('#form-shipping__type').val() == "private" || jQuery('#form-shipping__type').val() == "individual") {
            if (jQuery('#accountholder_name').val() == "" || jQuery('#accountholder_surname').val() == "") {
                jQuery('#errorbill').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (!checkcf(jQuery('#cf').val())) {
                jQuery('#errorbill2').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        else {
            if (jQuery('#accountholder').val() == "") {
                jQuery('#errorbill3').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (jQuery('#vat').val() == "" || !checkcf2(jQuery('#vat').val())) {
                jQuery('#errorbill4').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
            if (!checkcf2(jQuery('#cf').val())) {
                jQuery('#errorbill2').css('display', 'block');
                var topPosition = jQuery('#form-shipping__type').offset().top;
                jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
                return false;
            }
        }
        
    });
});

    function checkcf(val) {
        var regex = /[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]/;
        var v = val.match(regex);
        //console.log( 'codice fiscale valido: ' + v );
        if ( v ) { return true; }
        else { return false; }
    }

    function checkcf2(val) {
        var regex = /^\d+$/;
        var v = val.match(regex);
        //console.log( 'codice fiscale valido: ' + v );
        if ( v ) { 
            if (val.length == 11)
                return true; 
            else
                return false;
            }
        else { return false; }
    }
    
</script>    
<?php
    $customer_type= "private";
    $nation= "IT";
    if ($contacts) {
        $customer_type= $contacts->getType();
        $name= $contacts->getName();
        $surname= $contacts->getSurname();
        $society= $contacts->getCompanyName();
        $societytype= $contacts->getCompanyType();
        $vat= $contacts->getVat();
        $cf= $contacts->getFiscalCode();
        $address= $contacts->getAddress();
        $zipcode= $contacts->getZipCode();
        $province= $contacts->getProvince();
        $location= $contacts->getCity();
        $phone= $contacts->getPhone();
        $email= $contacts->getEmail();
        $nation= $contacts->getNation();
    }
?>
<form id="form-contacts-edit" class="form-registration form-profile spacer" method="POST" action="/<?php echo $language->language; ?>/user/rubrica" >
  <input type="hidden" value="<?php echo (arg(2)=='edit')?'update':'new'; ?>" name="action" />
  <input type="hidden" value="<?php echo $user->uid; ?>" name="uid" />
  <input type="hidden" value="<?php echo (arg(2)=='edit')?$contacts->getId():''; ?>" name="idc" />
  <div class="form-profile__optional">
    <fieldset class="boxed">
      <legend><?php echo (arg(2)=='edit')?t('edit contact'):t('enter contact'); ?></legend>   
      <div class="form-group col-sm-6">
        <label for="form-shipping__type"><?php echo $ltype_customer; ?></label>
        <select name="user_type" id="form-shipping__type" class="form-control shipform" required >
            <option value="">---</option>
            <option <?php if ($customer_type == "private") echo "selected"; ?> value="private"><?php echo t('Private'); ?></option>
            <option <?php if ($customer_type == "society") echo "selected"; ?> value="society"><?php echo t('Society'); ?></option>
            <option <?php if ($customer_type == "public") echo "selected"; ?> value="public"><?php echo t('Public'); ?></option>
            <option <?php if ($customer_type == "association") echo "selected"; ?> value="association"><?php echo t('Association'); ?></option>
            <option <?php if ($customer_type == "individual") echo "selected"; ?> value="individual"><?php echo t('Individual'); ?></option>
        </select>
    </div>
    <div class="clearfix"></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billname"><label class="shiplabel" style="width:40%"><?php echo $laccountholder_name; ?></label>  <input type="text" name="accountholder_name" id="accountholder_name" class="form-text shipform" value="<?php if ($name != '') {echo $name;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "private" && $customer_type != "individual") { ?> style="display: none" <?php } ?> id="billsurname"><label class="shiplabel" style="width:40%"><?php echo $laccountholder_surname; ?></label>  <input type="text" name="accountholder_surname" id="accountholder_surname" class="form-text shipform" value="<?php if ($surname != '') {echo $surname;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type == "private") { ?> style="display: none" <?php } ?> id="billsociety"><label class="shiplabel" style="width:40%"><?php echo $laccountholder; ?></label>  <input type="text" name="accountholder" id="accountholder" class="form-text shipform" value="<?php if ($society != '') {echo $society;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type != "society") { ?> style="display: none" <?php } ?> id="billtypesociety"><label for="" style="width:40%"><?php echo t('type'); ?></label>
        <select name="society_company_type" id="form-registration__type_company" class="form-control shipform">
            <option <?php if($societytype=="s.s") echo "selected"; ?> value="s.s">s.s</option>
            <option <?php if($societytype=="sas") echo "selected"; ?> value="sas">sas</option>
            <option <?php if($societytype=="snc") echo "selected"; ?> value="snc">snc</option>
            <option <?php if($societytype=="spa") echo "selected"; ?> value="spa">spa</option>
            <option <?php if($societytype=="srl") echo "selected"; ?> value="srl">srl</option>
            <option <?php if($societytype=="srlu") echo "selected"; ?> value="srlu">srlu</option>
            <option <?php if($societytype=="srls") echo "selected"; ?> value="srls">srls</option>
            <option <?php if($societytype=="sapa") echo "selected"; ?> value="sapa">sapa</option>
            <option <?php if($societytype=="scarl") echo "selected"; ?> value="scarl">scarl</option>
            <option value="_">altro</option>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" <?php if($customer_type == "private") { ?> style="display: none" <?php } ?> id="billvat"><label class="shiplabel" style="width:40%"><?php echo $lvat; ?></label>  <input type="text" name="vat" id="vat" class="form-text shipform" value="<?php if ($vat != '') {echo $vat;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billfisc"><label class="shiplabel" style="width:40%"><?php echo $lcf; ?> *</label>  <input required type="text" name="cf" id="cf" class="form-text shipform" value="<?php if ($cf != '') {echo $cf;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billaddress"><label class="shiplabel" style="width:40%"><?php echo $laddress; ?> *</label>  <input required type="text" name="address" id="address" class="form-text shipform" value="<?php if ($address != '') {echo $address;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billphone"><label class="shiplabel" style="width:40%"><?php echo $lphone; ?> *</label>  <input required type="text" name="phone" id="phone" class="form-text shipform" value="<?php if ($phone != '') {echo $phone;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billemail"><label class="shiplabel" style="width:40%"><?php echo $lemail; ?> *</label>  <input required type="text" name="email" id="email" class="form-text shipform" value="<?php if ($email != '') {echo $email;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billzip"><label class="shiplabel" style="width:40%"><?php echo $lzipcode; ?> *</label> <input required type="text" name="zipcode" id="zipcode" class="form-text shipform" value="<?php if ($zipcode != '') {echo $zipcode;} ?>"/></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billlocation"><label class="shiplabel" style="width:40%"><?php echo $llocation; ?> *</label> <input required type="text" name="location" id="location" class="form-text shipform" value="<?php if ($location != '') {echo $location;} ?>" /></div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billprovince"><label class="shiplabel" style="width:40%"><?php echo $lprovince; ?> *</label>  <!--<input type="text" name="province" id="province" class="form-text shipform" value="<?php #if ($_SESSION['province'] != '') {echo $_SESSION['province'];} ?>"/></div>-->
        <select name="province" id="province" class="form-control shipform">
        <?php 
        foreach( $provinces as $k => $provincia ){
            $selected = "";
            if( $provincia["provinceCode"] == $province ) $selected = 'selected="selected"';
            
            echo '<option value="' . $provincia["provinceCode"] . '" ' . $selected . ' >' . $provincia["provinceName"] . '</option>';
        }

        ?>
        </select>
    </div>
    <div class="col-sm-6 col-md-6 col-xs-12" id="billcountry"><label class="shiplabel" style="width:40%"><?php echo $lcontry; ?> *</label>  <!--<input type="text" name="country" id="country" class="form-text shipform" value="<?php #if ($_SESSION['country'] != '') {echo $_SESSION['country'];} ?>"/>-->
      <select name="country" id="country" class="form-control shipform">                  
        <?php
          foreach( $nazioni as $k => $nazione ){
            $selected = "";
            if( $nazione["countryCode"] == $nation ) $selected = 'selected="selected"';

            echo '<option value="' . $nazione["countryCode"] . '" ' . $selected . ' >' . $nazione["countryName"] . '</option>';
          }
        ?>
      </select>
    </div> 
    <div class="error-msg" id="errorbill" style="display:none; color:red"> <?php echo t('Name and Surname are mandatory. Please complete your data.'); ?> </div>
    <div class="error-msg" id="errorbill2" style="display:none; color:red"> <?php echo t('Fiscal Code Format Error. Please review your data.'); ?> </div>
    <div class="error-msg" id="errorbill3" style="display:none; color:red"> <?php echo t('Society Name is mandatory. Please complete your data'); ?> </div>
    <div class="error-msg" id="errorbill4" style="display:none; color:red"> <?php echo t('Vat is mandatory and must be well formed. Please complete your data'); ?> </div>
    <div class="clearfix"></div>

    
    <input type="submit" class="btn btn-red btn-update" value="<?php echo (arg(2)=='edit')?t('update'):t('insert'); ?>" />
</form>






