<?php

global $base_url, $theme_path;
$merge_url = $base_url . '/' . $theme_path;

$order = MP\OrdersQuery::create()->filterByOrderId($order_id)->findOne();

if($order){
  $order_elements = MP\OrderElementQuery::create()->filterByOrderId($order_id)->find();
  $order_shipping = MP\OrderShippingDetailsQuery::create()->filterByOrderId($order_id)->findOne();
  $order_billing = MP\OrderBillingDetailsQuery::create()->filterByOrderId($order_id)->findOne();
  $shipping_company_id = $order->getShippingCompany();
  $shipping_company = MP\ShippingCompaniesQuery::create()->filterByShippingId($shipping_company_id)->findOne();
  $shipping_company_name = $shipping_company->getName();
  foreach ($order_elements as $order_element) {
    $shipping_date = $order_element->getEstimatedShippingDate("d/m/Y");
  }
}

switch ($order->getPayOption()) {
    case "paypal":
        $pay_option = "Paypal";
        break;
    case "sofort":
        $pay_option = "Sofort";
        break;
    case "banktransfer":
        $pay_option = "Bonifico";
        break;
    case "contrassegno":
        $pay_option = "Contrassegno";
        break;
    default:
        $pay_option = "Sconosciuto";
        break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title><?php echo $subject; ?></title>

  <style type="text/css">

    /* reset */
    #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
    .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Forces Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
    p {margin: 0; padding: 0; font-size: 0px; line-height: 0px;} /* squash Exact Target injected paragraphs */
    table td {border-collapse: collapse;} /* Outlook 07, 10 padding issue fix */
    table {border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; } /* remove spacing around Outlook 07, 10 tables */
    body {
      font-family: arial, helvetica, sans-serif;
    }

  </style>
  <!--[if gte mso 9]>
  <style>
    /* Target Outlook 2007 and 2010 */
  </style>
  <![endif]-->
</head>
<body style="width:100%; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;">

<!-- body wrapper -->
<table cellpadding="0" cellspacing="0" border="0" style="margin:0; padding:0; width:100%; line-height: 100% !important;">
  <tr>
    <td valign="top">
      <!-- edge wrapper -->
      <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="">
        <tr>
          <td valign="top">
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="">
              <tr>
                <td valign="middle" height="70" style="border-top: 3px solid #eb7a1a; background: #f0efeb">
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="">
                    <tr>
                      <td valign="top">
                        <img src="<?php echo $merge_url; ?>/images/email/logo.png" alt="MePrint" style="display: block; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;" />
                      </td>
                      <td valign="top">
                        <h1 style="font-size: 14px; font-weight: bold; text-transform: uppercase; text-align: right">Conferma Ordine</h1>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
            <!-- content wrapper -->
            <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="">
              <tr>
                <td valign="top" style="vertical-align: top;">
                  <!-- ///////////////////////////////////////////////////// -->

                  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-bottom: 30px">
                    <tr>
                      <td valign="top" style="vertical-align: top; padding-bottom: 50px">
                        <p style="font-size: 16px; margin-bottom: 20px; line-height:1.5;margin-top: 15px">
                          <?php echo $message; ?>
                         
                        </p>
                        <p style="font-size: 12px; line-height:1.3; font-weight: bold; border: 1px solid #f0efeb; padding: 10px">Data Partenza Materiale: <?php echo $shipping_date; ?></p>

                        <table cellpadding="0" cellspacing="0" border="0" align="left" width="600" style="font-size: 11px">
                          <thead valign="bottom"  style="background: #333; color: #fff; text-align: left">
                          <tr>
                            <th style="padding: 10px">Id</th>
                            <th style="padding: 10px">Prodotto</th>
                            <th style="padding: 10px">Qnt</th>
                            <th style="padding: 10px">Lavorazioni & Accessori</th>
                            <th style="padding: 10px; text-align: right;">Prezzo</th>
                          </tr>
                          </thead>
                          <tbody>
                              <?php
                              foreach ($order_elements as $element) {
                                if($element->getFormType() == "big"){
                                  $product_lang = MP\FormoneLangQuery::create()->filterByFormId($element->getFormId())->filterByLanguageId('it')->findOne();
                                }elseif($element->getFormType() == "small"){
                                  $product_lang = MP\FormtwoLangQuery::create()->filterByFormId($element->getFormId())->filterByLanguageId('it')->findOne();
                                }
                                $product_name = $product_lang->getName();
                                
                                if ($element->getFormType() == 'big') {
                                    //dd($element->getMaterialId());
                                    $material = MP\MaterialsLangQuery::create()->filterByMaterialId($element->getMaterialId())
                                                    ->filterByLanguageId('it')->select(array('name'))->findOne();
                                    //dd($material);
                                    if ($element->getDimensionId() == 0)
                                        $dimension = $element->getWidth() . " x " . $element->getHeight();
                                    else
                                        $dimension = MP\DimensionsQuery::create()->filterByDimensionId($element->getDimensionId())
                                                        ->select(array('name'))->findOne();
                                }
                                else {
                                    $material = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($element->getMaterialId())
                                                    ->filterByLanguageId('it')->select(array('name'))->findOne();
                                    if ($element->getDimensionId() == 0)
                                        $dimension = $element->getWidth() . " x " . $element->getHeight();
                                    else
                                        $dimension = MP\DimensionsTwoQuery::create()->filterByDimensionId($element->getDimensionId())
                                                        ->select(array('name'))->findOne();
                                }

                                $processing = MP\ProcessingLangQuery::create()->filterByProcessingId($element->getProcessingId())
                                                ->filterByLanguageId('it')
                                                ->select(array('name'))
                                                ->findOne();

                                $sides = "";
                                if ($element->getProcessingTop() == 1)
                                    $sides = "Alto";
                                if ($element->getProcessingBottom() == 1) {
                                    if ($sides != "")
                                        $sides .= ", Basso";
                                    else
                                        $sides = "Basso";
                                }
                                if ($element->getProcessingLeft() == 1) {
                                    if ($sides != "")
                                        $sides .= ", Sx";
                                    else
                                        $sides = "Sx";
                                }
                                if ($element->getProcessingRight() == 1) {
                                    if ($sides != "")
                                        $sides .= ", Dx";
                                    else
                                        $sides = "Dx";
                                }
                                if ($element->getProcessingDistanceSpecial() != 0) {
                                    $sides .= " ".$element->getProcessingDistanceSpecial()." cm ";
                                } 
                                if ($element->getProcessingNumberSpecial() != 0) {
                                    $sides .= " #".$element->getProcessingNumberSpecial()."";
                                } 

                                if ($sides != "")
                                    $processing .= "\n".$sides;

                                if ($processing == "" || $processing == "Lavorazione nulla") {
                                    $processing= "-";
                                }

                                if ($element->getFrontback() == 1) {
                                    if ($processing == "-")
                                        $processing= "Bifacciale - ";
                                    else
                                        $processing.= " - Bifacciale - ";
                                }

                                $element2 = MP\OrderElementExtraProcessingQuery::create()->filterByOrderElementId($element->getOrderElementId())->find();
                                $index = 0;
                                foreach ($element2 as $elementextra) {
                                    if ($processing == "-")
                                        $processing = "";
                                    else 
                                        $processing .= " - ";

                                    $processing .= MP\ProcessingLangQuery::create()->filterByProcessingId($elementextra->getProcessingId())
                                                    ->filterByLanguageId('it')
                                                    ->select(array('name'))
                                                    ->findOne();
                                    $sides = "";
                                    if ($elementextra->getExtraProcessingTop() == 1)
                                        $sides = "Alto";
                                    if ($elementextra->getExtraProcessingBottom() == 1)
                                        if ($sides != "")
                                            $sides .= ", Basso";
                                        else
                                            $sides = "Basso";

                                    if ($elementextra->getExtraProcessingLeft() == 1)
                                        if ($sides != "")
                                            $sides .= ", Sx";
                                        else
                                            $sides = "Sx";

                                    if ($elementextra->getExtraProcessingRight() == 1)
                                        if ($sides != "")
                                            $sides .= ", Dx";
                                        else
                                            $sides = "Dx";

                                    if ($elementextra->getDistanceSpecial() != NULL) {
                                        $sides .= " ".$elementextra->getDistanceSpecial()." cm ";
                                    } 
                                    if ($elementextra->getNumberSpecial() != NULL) {
                                        $sides .= $elementextra->getNumberSpecial()."occh.";
                                    }     
                                    if ($sides != "")
                                        $processing .= "\n".$sides;
                                }

                                $element3 = MP\OrderElementAccessoryQuery::create()->filterByOrderElementId($element->getOrderElementId())->find();
                                $accessories= "";
                                if ($element->getStructureId() != 0)
                                    $accessories.= "Struttura\n";
                                foreach ($element3 as $elementacc) {
                                    $accessories .= MP\AccessoriesLangQuery::create()->filterByAccessoryId($elementacc->getAccessoriesId())
                                                    ->filterByLanguageId('it')
                                                    ->select(array('name'))
                                                    ->findOne();
                                    $accessories .= " (" . $elementacc->getQuantity() . ")";
                                    $accessories .= "<br/>";
                                }

                                if ($accessories == "")
                                    $accessories= "-";
                                //$shipping_date = $order_element->getEstimatedShippingDate("d/m/Y");
                              ?>
                                <tr style="border-bottom: 1px solid #f0efeb">
                                  <td valign="top" style="padding: 10px"><?php echo $element->getOrderElementId(); ?></td>
                                  <td valign="top" style="padding: 10px"><?php echo $product_name; ?><br><?php echo $material . " - " . $dimension; ?></td>
                                  <td valign="top" style="padding: 10px"><?php echo $element->getNoofElements(); ?></td>
                                  <td valign="top" style="padding: 10px"><?php echo $processing; ?><br><?php echo $accessories; ?></td>
                                  <td valign="top" style="padding: 10px; text-align: right;"><strong><?php echo number_format((float)$element->getPrice(),2,".",""); ?> &euro;</strong></td>
                                </tr>
                              <?php
                              }
                              ?>
                          <tr style="border-bottom: 1px solid #f0efeb">
                              <td colspan="5" style="padding: 10px 0"><strong style="font-weight: bold">Note inserite per la spedizione:</strong> <?php echo $order->getNotes(); ?> </td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Costo di spedizione</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getShipPrice(),2,".",""); ?> &euro;</td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Costo di imballo</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getPackPrice(),2,".",""); ?> &euro;</td>
                          </tr>
                          <?php if ($order->getCashOnDeliveryPrice() > 0) { ?>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Pagamento alla consegna</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getCashOnDeliveryPrice(),2,".",""); ?> &euro;</td>
                          </tr>
                          <?php } ?>
                          <?php if ($order->getDiscountPrice() > 0) { ?>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Sconto</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getDiscountPrice(),2,".",""); ?> &euro;</td>
                          </tr>
                          <?php } ?>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Totale Imponibile</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getGrandtotalPrice()+$order->getCashOnDeliveryPrice()+$order->getShipPrice()+$order->getPackPrice()-$order->getDiscountPrice(),2,",",""); ?> &euro;</td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">IVA</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php $iva= ($order->getFinalPrice()-$order->getDiscountPrice())*$VAT_RATE/100; echo number_format((float)$iva,2,',',''); ?> &euro;</td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Data di spedizione scelta</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo $shipping_date; ?></td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Corriere scelto</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo $shipping_company_name; ?></td>
                          </tr>
                          <tr>
                            <td colspan="4" style="text-align: right; font-weight: bold; padding: 5px 10px">Il totale complessivo del tuo ordine n. <?php echo $order_element->getOrderId(); ?> &egrave; pari a</td>
                            <td style="font-weight: bold; padding: 5px 10px; text-align: right"><?php echo number_format((float)$order->getGrandtotalPrice()+$order->getCashOnDeliveryPrice()+$iva+$order->getShipPrice()+$order->getPackPrice()-$order->getDiscountPrice(),2,",",""); ?> &euro;</td>
                          </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>

                  <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="font-size: 11px; margin-bottom: 30px">
                    <thead valign="bottom"  style="background: #333; color: #fff; text-align: left">
                    <tr>
                      <th style="padding: 10px">Indirizzo Spedizione</th>
                      <th style="padding: 10px">Indirizzo Fatturazione</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td style="padding: 10px">
                        <strong style="font-weight: bold"><?php echo $order_shipping->getName(); ?></strong><br />
                        <br />
                        <?php echo $order_shipping->getPhone(); ?><br /><br/>
                        <?php echo $order_shipping->getAddress(); ?><br />
                        <?php $zip= $order_shipping->getZipCode(); if (strlen($zip) == 4) { $zip= "0".$zip; } echo $zip ." ".$order_shipping->getLocation(); ?><br />
                        <?php echo $order_shipping->getCountry(); ?>
                      </td>
                      <td style="padding: 10px">
                        <strong style="font-weight: bold"><?php echo $order_billing->getName(); ?></strong><br />
                        <br />
                        <?php echo $order_billing->getPhone(); ?><br /><br/>
                        <?php echo $order_billing->getAddress(); ?><br />
                        <?php $zip= $order_billing->getZipCode(); if (strlen($zip) == 4) { $zip= "0".$zip; } echo $zip ." ".$order_billing->getLocation(); ?><br />
                        <?php echo $order_billing->getCountry(); ?>
                      </td>
                    </tr>
                    <tr>
                      
                        <td colspan="2" style="padding: 10px; font-weight: bold">Modalit&agrave; di pagamento: <?php echo $pay_option; ?></td>
                    </tr>
                    </tbody>
                  </table>
                  
                  <br />

                  <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="font-size: 11px; margin-bottom: 30px">
                    <tr>
                      <td valign="top"><h3 style="font-weight: bold; text-transform: uppercase; margin-bottom: 15px">Note</h3></td>
                    </tr>
                    <tr>
                      <td valign="top" style="padding: 10px; background: #f0efeb;">
                        <p style="font-size: 12px; line-height:1.3; margin-bottom: 10px">Nel caso in cui tu abbia scelto il pagamento con <strong style="font-weight: bold">bonifico bancario</strong> le nostre coordinate bancarie sono:</p>
                        <p style="font-size: 11px; line-height:1.3; margin-bottom: 10px">
                          <strong style="font-weight: bold">Cartoons s.r.l</strong><br />
                          Via Torino 6A/6B<br />
                          56038 - Ponsacco (PI)<br /><br />
                          <strong style="font-weight: bold">IBAN: IT73J0856271120000010180495</strong><br />
                          <strong style="font-weight: bold">Banca di Pisa e Fornacette</strong><br />
                        </p>
                      </td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    <tr>
                      <td valign="top" style="padding: 10px; background: #f0efeb; margin-bottom: 10px">
                        <p style="font-size: 12px; line-height:1.3; margin-bottom: 10px">Se hai scelto il pagamento con <strong style="font-weight: bold">Contrassegno</strong> potrai pagare direttamente al corriere tramite:</p>
                        <ul style="list-style-type: none; margin: 0; padding: 0">
                          <li style="margin-bottom: 5px; font-weight: bold">- Contanti</li>
                          <li style="margin-bottom: 5px; font-weight: bold">- Assegno Bancario intestato a Cartoons srl</li>
                        </ul>
                      </td>
                    </tr>
                  </table>
                  
                  <br />
                  <?php if ($pay_option == "paypal" || $pay_option == "sofort") { ?>
                  <table cellpadding="0" cellspacing="0" border="0" align="left" width="100%" style="font-size: 11px; margin-bottom: 30px">
                    <tr>
                      <td valign="top">
                          <h3 style="font-size:16px; font-weight: bold; margin-bottom: 15px">Il tuo pagamento &egrave; stato registrato correttamente</h3>
                        <p style="font-size: 12px; line-height:1.3; margin-bottom: 30px">Riceverai la <strong style="font-weight: bold">Fattura</strong> via email oppure potrai scaricarla direttamente dalla tua area personale su sito MePrint.it</p>
                        
                        <p style="font-size: 16px; line-height:1.5;margin-top: 15px; font-style: italic; font-weight: bold">
                          Lo staff di MePrint.it
                        </p>
                      </td>
                    </tr>
                  </table>
                  <?php } ?>
                  <?php include('email_footer.tpl.php'); ?>
           
                  <!-- //////////// -->
                </td>
              </tr>
            </table>
            <!-- / content wrapper -->
          </td>
        </tr>
      </table>
      <!-- / edge wrapper -->
    </td>
  </tr>
</table>
<!-- / page wrapper -->
</body>
</html>
