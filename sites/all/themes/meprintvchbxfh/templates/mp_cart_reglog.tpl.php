<?php

global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;

include_once DRUPAL_ROOT . '/sites/default/files/listanazioni.inc';
include_once DRUPAL_ROOT . '/sites/default/files/listaprovince.inc';
$lista_nazioni_json = listanazioni();
$nazioni = drupal_json_decode( $lista_nazioni_json );
$lista_province_json = listaprovince();
$provinces = drupal_json_decode( $lista_province_json );

global $user;
session_start();
$sessionid = session_id();

if(isset($_POST['productprice']) && $_POST['productprice'] != ''){
    $_SESSION['productprice'] = $_POST['productprice'];
  }
  if(isset($_POST['dicountamount']) && $_POST['dicountamount'] != ''){
    $_SESSION['dicountamount'] = $_POST['dicountamount'];
  }
  if(isset($_POST['finalamount']) && $_POST['finalamount'] != ''){
    $_SESSION['finalamount'] = $_POST['finalamount'];
  }
  if(isset($_POST['coupon_code']) && $_POST['coupon_code'] != ''){
    $_SESSION['coupon_code'] = $_POST['coupon_code'];
  }
  if(isset($_POST['discount_id']) && $_POST['discount_id'] != ''){
    $_SESSION['discount_id'] = $_POST['discount_id'];
  }
  if(isset($_POST['totalprice']) && $_POST['totalprice'] != ''){
    $_SESSION['totalprice'] = $_POST['totalprice'];
  }
  if(isset($_POST['ship']) && $_POST['ship'] != ''){
    $_SESSION['ship'] = $_POST['ship'];
  }
  if(isset($_POST['pack']) && $_POST['pack'] != ''){
    $_SESSION['pack'] = $_POST['pack'];
}

?>


<div class="row">
  <div class="col-sm-12">
    <div class="boxed spacer">
      <div class="row">
        <div class="col-sm-6 text-center">
            <h4><?php echo t("Are you already a MePrint user?"); ?></h4>
            <a class="btn btn-success" href="/user/login"><i class="fa fa-key fa-lg"></i> <?php echo $lbl_login; ?></a>
        </div>
        <div class="col-sm-6 text-center login-border-left">
            <h4><?php echo t("Are you not a MePrint user yet?"); ?></h4>
            <a class="btn btn-success" href="/user/register"><i class="fa fa-pencil fa-lg"></i> <?php echo t("Register"); ?></a>
        </div>
      </div>
    </div>
  </div>
    <div class="col-sm-12">
        <p class="pull-right"><?php echo t('Or go on as'); ?> <a href="spedizione"><?php echo t('anonymous user');?> </a></p>
    </div>  
</div>
