<section class="blue-bg">
    <div class="container">
      <?php print render($page['first_preface']); ?>
    </div>
  </section>
  <section class="lit-gra-bg">
    <div class="container">
      <?php global $base_url; ?>
      <header class="row">
        <div class="col-sm-2 col-md-2"></div>
        <div class="col-sm-8 col-md-8">
          <?php print render($page['second_preface_header']); ?>
        </div>
        <div class="col-sm-2 col-md-2"></div>
      </header>
      <article class="row">
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_left']); ?>
        </div>
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_center']); ?>
        </div>
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_right']); ?>
        </div>
      </article>
    </div>
  </section>
  <footer class="footer-bg">
    <div class="container">
      <?php print render($page['footer']); ?>
    </div>
  </footer>