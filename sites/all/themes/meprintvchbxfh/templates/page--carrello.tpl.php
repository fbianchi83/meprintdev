<script type="text/javascript" src="https://parall.ax/parallax/js/jspdf.js"></script>
<?php

global $base_path;
global $base_url;
global $language;
$lang_name = $language->language;

?>

  <?php
    include('header.tpl.php');
  ?>
  <!--Start he Banner--->
  <section class="container">
    <div class="space-3"></div>
    <!--End of the search--->
    <article class="row">

      <div class="col-md-9 col-sm-9">
        <header class="row">
          <div class="col-sm-12 col-md-12 ">

            <?php if ($breadcrumb): echo '<a href="' . $base_url . '">' . $Home . '</a>' . ' / ' . $bcart; endif; ?>
            <div class="space-2"></div>
            <div class="cartnav clearfix">
              <div class="cartlable shiplable">
                <span class="active"><?php echo $cartpagetittle; ?></span>
              </div>
              <div class="shippingnav hidden-xs">
                <span class="shiplable inactive"><?php echo $shippinginfo; ?></span>
              </div>
              <div class="ordernav hidden-xs">
                <span class="shiplable inactive"><?php echo $orderconfirm; ?></span>
              </div>
            </div>
            <div class="space-3"></div>

            <?php
              print render($page['content']);              
            ?>
          </div>
        </header>
        <div class="space-2"></div>
      </div>

      <aside class="col-md-3 col-sm-3">
        <?php
          print render($page['right_sidebar_cart']);
        ?>
               
      </aside>
    </article>
  </section>
  
  <?php
    include('footer.tpl.php');
  ?>

<script>
  jQuery(document).ready(function($) {
    $('.cartdetails--open').click(function() {
      $(this).toggleClass('fa-plus fa-minus');
    });
  });
</script>