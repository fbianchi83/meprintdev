<!DOCTYPE html>
<html>
  <head><!-- 20160913 -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php
    if (drupal_is_front_page()) {
    ?>
        <meta name="description" content="MePrint è un'azienda specializzata nella stampa digitale di grande formato online. Personalizza e stampa i tuoi prodotti a prezzi bassi senza rinunciare alla qualità" />
    <?php
    }
    ?>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>

    <?php if (arg(0) == 'carrello'): ?>
      <link rel="stylesheet" type="text/css" media="print" href="<?php echo base_path() . path_to_theme(); ?>/css/print.css">
    <?php endif; ?>

    <?php print $scripts; ?>
        <script type="text/javascript" src="http://www.meprint.it/sites/all/modules/jquery_cookie/jquery.cookie.js"></script>  
    <script>
      jQuery(function(){
        jQuery(".topnav").accordion({
          accordion:false,
          speed: 500,
          closedSign: '+',
          openedSign: '-'
        });
        
        if(jQuery.cookie('policy') != '1'){jQuery("#cookie_banner").css('display','block'); }

        if(jQuery.cookie('policy') == '1'){
           google();
        }

        jQuery("#cookie_banner a").click(function(){
           cookie();
        });
      });
    </script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-10958509-2', 'auto');
   ga('set', 'anonymizeIp', true);
  ga('require', 'ecommerce', 'ecommerce.js');

    <?php
    global $language;
  $lang_name = $language->language;
  
    if($_SERVER['SCRIPT_URL']=="/it/conferma-ordine/") {
        $last_order= MP\OrdersQuery::create()->filterByOrderId($_SESSION['orderid'])->findOne();
        $iva= $last_order->getFinalPrice() * $last_order->getVateRate() / 100;
    ?>

    ga('ecommerce:addTransaction', {
        'id': '<?php echo $_SESSION['orderid']; ?>',
        'affiliation': 'meprint',
        'revenue': '<?php echo $last_order->getGrandTotalPrice(); ?>',
        'shipping': '<?php echo $last_order->getShipPrice(); ?>',
        'tax': '<?php echo $iva; ?>'
    });
    <?php

    $prods= MP\OrderElementQuery::create()->filterByOrderId($_SESSION['orderid'])->find();
    foreach($prods as $product) {
        $product_id= $product->getFormId();
        $productn= MP\FormoneLangQuery::create()->filterByFormId($product_id)->filterByLanguageId($lang_name)->findOne();
        $categoryn= MP\ProductsGroupLangQuery::create()->filterByProductgroupId($product->getProductgroupId())->filterByLanguageId($lang_name)->findOne();
            
    ?>

    ga('ecommerce:addItem', {
        'id': '<?php echo $_SESSION['orderid']; ?>',
        'name': '<?php echo $productn->getName(); ?>',
        'sku': '<?php echo $product_id; ?>',
        'category': '<?php echo $categoryn->getName(); ?>',
        'price': '<?php echo $product->getPrice(); ?>',
        'quantity': '<?php echo $product->getNoofElements(); ?>',
    });
    
    <?php 
    }
    ?>

    ga('ecommerce:send');
        <?php } ?>
  ga('send', 'pageview');

</script>
    
<script>
    function google() {
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1706477366303650');
        fbq('track', "PageView");
        
        var $mcGoal = {'settings':{'uuid':'8446e4d10d7c4bcc2eab594a6','dc':'us2'}};
	(function() {
		var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true; sp.defer = true;
		sp.src = ('https:' == document.location.protocol ? 'https://s3.amazonaws.com/downloads.mailchimp.com' : 'http://downloads.mailchimp.com') + 

'/js/goal.min.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
	})(); 
    }
    <?php 
    global $user;
    if ($user->uid != 0 && isset($_SESSION['cartid']) && $_SESSION['cartid'] != "") {
        MP\CartQuery::create()->filterByUserId($user->uid)->filterByStatus(1)->update(array('Status' => 0));
        MP\CartQuery::create()->filterByCartId($_SESSION['cartid'])->update(array('UserId' => $user->uid));
        unset($_SESSION['cartid']);
        drupal_goto("spedizione");
    }
    
    ?>
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1706477366303650&ev=PageView&noscript=1"
/></noscript>
<script>
    function cookie(){
        jQuery.cookie('policy', '1', { expires: 30, path: '/' });
        google();
        jQuery("#cookie_banner").hide();
   }
</script>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">
    window.smartlook||(function(d) {
    var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
    var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
    c.charset='utf-8';c.src='//rec.getsmartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', '6219e3ae9798aaeb9ae8d3e3872e6f5a133b6497');
</script>

<!-- zoorate feedaty merchant widget start -->
<div id="zoorate_widget_f6f40a5a78da88ca"></div>
<script src="//widget.zoorate.com/js/feed_v3.js" type="text/javascript"></script>
<script type="text/javascript">
var zoorate_params = {
"zoorate_site_key": "fa2c8c6619f7d412c9d517212c5c1c0b",
"zoorate_widget_css": "merchant_float_top.css",
"zoorate_widget_type": "merchant_float_top",
"zoorate_widget_id": "f6f40a5a78da88ca"+"|"+document.URL,
"zoorate_widget_server": "widget.zoorate.com"
};
do_widget(zoorate_params);
</script>
<!-- zoorate widget end --> 

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?2OoODAlx3FrmsxzggEBaRfmTDMEPXNo7";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes; ?>>
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1011861371;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1011861371/?value=0&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>
      <div id="wrapper">

      <!-- --------- PAGE CONTENT ---------- -->
      <?php print $page; ?>
      <!-- --------- PAGE CONTENT ---------- -->


      <!-- modale riempita tramite js per visualizzazione alert -->
      <div class="modal modal-alert fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo t('close'); ?>"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">                    
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-white" data-dismiss="modal"><?php echo t('close'); ?></button>
            </div>
          </div>
        </div>
      </div>


    </div><!-- end wrapper -->
    <script>
      // TIENE IL MENU APERTO SU URL CON STESSA CATEGORIA
      jQuery(document).ready(function($) {
        var curUrlComp = window.location.href;
        var curUrl = curUrlComp.replace("http://"+window.location.hostname, "");
        var curSplit = curUrl.split('/');
        var urlSplit = curSplit[2];
        
        $('.region-left-sidebar .topnav ul').hide();
        $.each($('.region-left-sidebar .topnav ul a'), function() {
          var urlLink = $(this).attr('href');
          var curUrlLink = urlLink.replace("http://"+window.location.hostname, "");
          var splitLink = curUrlLink.split('/');
          var urlSplitLink = splitLink[2];
          
          // escludo quello di admin
          if( urlSplit != 'tonic' && urlSplitLink == urlSplit ) {
            $(this).parents('ul').css('display', 'block');
          }

        });
        
        
        $('ul.topnav li a').hover(function() {
          $(this).find('.novita').addClass("white");
        }, function() {
          $(this).find('.novita').removeClass("white");
        });
        
        
        /* MOBILE RESPONSIVE */
        var wWindow = $(window).width();        
        if( wWindow < 768 ){
          
          //nascondo le voci di menu tranne quelle selezionate
          $(".topnav li").addClass("mm");
          $(".region-left-sidebar-prod").addClass("mm");
          $(".mm").hide();
          //al click di una quelle selezionate apre le voci sottostanti
          $(".menu-mobile").on('click', function(e){ 
            var mm = jQuery(this);
            e.preventDefault();
            $(".topnav li.mm").slideToggle('slow', function() {
                if ($(this).is(':visible')) {
                    mm.text('close menu');                
                } else {
                    mm.text('open menu');                
                }        
            });       
            
          });
          
          //quando sono in MOBILE calcolo il padding left dell'immagini del listing
          var wBoxThumb = $('.products-Thumbnail').width();          
          var ml = ( wBoxThumb - 260 ) / 2; 
          var hImgThumb = ml + ml/2 + 260; 
          
          $('.item-img').css({
            "padding-top" : ml + "px",
            "padding-left" : ml + "px"
          });
          
          $('.item, .item-img').css({"height": hImgThumb + "px" });
                    
          
          
          
        }
        
        
        
        
        
        
      });

    </script>
    <div id="cookie_banner" style="display: none;">
    <div class="row">
        <div class="col-xs-9">
            <p>Questo sito utilizza cookie propri e di altri siti per fornire i propri servizi e per assicurare la migliore esperienza di navigazione. <br> Continuando la navigazione o accedendo a qualunque elemento del sito, accetti l'utilizzo dei cookie da parte di Meprint.</p>
        </div>
        <div class="col-xs-3 text-right">
            <a class="btn btn-default" href="" onclick="cookie(); return false;">Accetta</a>
            <a class="btn btn-default" target="_blank" href="/cookie">Informazioni</a>
        </div>
    </div>
</div>
  </body>
</html>
