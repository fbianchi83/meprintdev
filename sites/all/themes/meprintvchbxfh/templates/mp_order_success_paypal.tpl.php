<?php
    global $base_url;
    global $base_path;
    global $language;
    $lang_name = $language->language;
    include DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
    include DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';

    $raw_post_data = file_get_contents('php://input');
    $raw_post_array = explode('&', $raw_post_data);
    $myPost = array();
    foreach ($raw_post_array as $keyval) {
        $keyval = explode ('=', $keyval);
        if (count($keyval) == 2)
            $myPost[$keyval[0]] = urldecode($keyval[1]);
    }
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
    $req = 'cmd=_notify-validate';
    if(function_exists('get_magic_quotes_gpc')) {
        $get_magic_quotes_exists = true;
    } 
    
    foreach ($myPost as $key => $value) {        
        if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
            $value = urlencode(stripslashes($value)); 
        } else {
            $value = urlencode($value);
        }
        $req .= "&$key=$value";
    }

    // Step 2: POST IPN data back to PayPal to validate
    $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
    // In wamp-like environments that do not come bundled with root authority certificates,
    // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set 
    // the directory path of the certificate as shown below:
    // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
    if( !($res = curl_exec($ch)) ) {
        // error_log("Got " . curl_error($ch) . " when processing IPN data");
        curl_close($ch);
        exit;
    }
    curl_close($ch);
    
    if ($user->uid == 139) {
        foreach($_POST as $key => $value) {
            echo $key." = ". $value."";
        }
    } 
    
    if (strcmp ($res, "VERIFIED") == 0) {
        // The IPN is verified, process it:
        // check whether the payment_status is Completed
        // check that txn_id has not been previously processed
        // check that receiver_email is your Primary PayPal email
        // check that payment_amount/payment_currency are correct
        // process the notification
        // assign posted variables to local variables
        $payment_status = $_POST['payment_status'];
        $txn_id = $_POST['txn_id'];
        $receiver_email = $_POST['receiver_email'];
        $payer_email = $_POST['payer_email'];
        $orderid= $_POST['custom'];
        MP\OrdersQuery::create()->filterByOrderId($orderid)->update(array('PayStatus' => 2));
        // IPN message values depend upon the type of notification sent.
        // To loop through the &_POST array and print the NV pairs to the screen:
        if ($user->uid == 139) {
            foreach($_POST as $key => $value) {
                echo $key." = ". $value."";
            }
        } 
        include( DRUPAL_ROOT . '/sites/all/libraries/PHPMailer/class.phpmailer.php' );
        include( DRUPAL_ROOT . '/sites/all/libraries/PHPMailer/class.smtp.php' );
      
        //INVIO MAIL CONFERMA ORDINE
        mp_mail_conferma_ordine($orderid);
        
        $cart= MP\OrdersQuery::create()->filterByOrderId($orderid)->findOne();
        $cart_elements = MP\CartItemQuery::create()->filterByCartId($cart->getCartId())->find();

        $filesok= 1;
        foreach ($cart_elements as $cart_element) {
            
            if ($cart_element->getProductType() == 'small') {
                $product = MP\FormtwoSmallFormatQuery::create()->findPk($cart_element->getProductId());
                $product_group_id = 34;
                $nfiles= 1;
            } else {
                $product = MP\FormoneBigFormatQuery::create()->findPk($cart_element->getProductId());
                $product_group_id = $product->getproductgroupId();
                $product_group_id2 = $product->getsubcategoryId();
                $nfiles= $product->getNumberOfFilesTobeUploaded();
            }
            if ($cart_element->getMaterialId() == 43)
                $nfiles= 0;
        
            $cart_plus_file = $cart_element->getPlusFile();            

            $cart_item_files = MP\CartItemFileQuery::create()->filterByCartItemId($cart_element->getCartItemId())->find();
            $cart_item_filesn = MP\CartItemFileQuery::create()->filterByCartItemId($cart_element->getCartItemId())->count();


            //dd($nfiles . " " . $cart_item_filesn);
            if (($nfiles + $cart_plus_file) != $cart_item_filesn)
                $filesok= 0;
        }
        if ($filesok == 0) {
            mp_mail_file_mancanti($orderid);
        }
        
        // TRACCIAMENTO GOOGLE
        mp_order_track_analytics($order_id);
        
    } else if (strcmp ($res, "INVALID") == 0) {
        // IPN invalid, log for manual investigation
        echo "The response from IPN was: " .$res ."";
    }
?>