
<?php 

global $user;
global $language;
$lang_name = $language->language;

$status = MP\OrderStatusQuery::create()->filterByOrderStatusId($orders->getOrderStatus())->select(array('name'))->findOne();

$orderel = MP\OrderElementQuery::create()->filterByOrderId($orders->getOrderId())->findOne();
$tmp_date = $orderel->getEstimatedShippingDate("Y-m-d");
$orderdt = explode("-", $tmp_date);
$shipping_date = $orderdt[2] . "-" . $orderdt[1] . "-" . $orderdt[0];

$shipping_companie = MP\ShippingCompaniesQuery::create()->filterByShippingId($orders->getShippingCompany())->findOne();

$ship_img_url = "http://placehold.it/70x70" ;
if( isset( $shipping_companie ) ){
  $ship_img_fid = $shipping_companie->getFile();
  $ship_img_file = file_load($ship_img_fid);
  $ship_img_url = file_create_url($ship_img_file->uri);
}

drupal_add_js(base_path() . path_to_theme() . '/scripts/users.js');

//print_r( $orders );
?> 

<form id="form-edit-order" class="form-registration form-edit-order spacer" method="POST" >
  <div class="product_name">
    <div class="col-md-8 col-sm-8 col-xs-7"><strong><?php echo $cartproduct; ?></strong></div>
    <div class="col-md-2 col-sm-2 col-xs-2 text-center"><strong class="hidden-xs"><?php echo $quantity; ?></strong><strong class="visible-xs"><?php echo t('Qty'); ?></strong></div>
    <div class="col-md-2 col-sm-2 col-xs-3 text-right"><strong><?php echo $price; ?></strong></div>
  </div>
  <div class="wrapper-cartdetails">    
    <?php

    foreach( $order_items as $order_item ){
      
   
        if ($order_item->getFormType() == 'big') {
            //print_r( $order_item );
            $presult = MP\FormoneLangQuery::create()->filterByFormId($order_item->getFormId())->filterByLanguageId($lang_name)->findOne();
            //print_r( $presult );
            $order_item_id = $order_item->getOrderElementId();
      
            echo theme('order_item_row', array('order_item_id' => $order_item_id, 'order_item' => $order_item , 'presult' => $presult, 'lang_name' => $lang_name )); 
      
        } else {
            //print_r( $order_item );
            $presult = MP\FormtwoLangQuery::create()->filterByFormId($order_item->getFormId())->filterByLanguageId($lang_name)->findOne();
            //print_r( $presult );
            $order_item_id = $order_item->getOrderElementId();
      
            echo theme('order_item_row_small', array('order_item_id' => $order_item_id, 'order_item' => $order_item , 'presult' => $presult, 'lang_name' => $lang_name )); 
      
        }
    }
    $iva= number_format((float) round(($orders->getGrandtotalPrice()-$orders->getDiscountPrice())+$orders->getShipPrice()+$orders->getPackPrice()+$orders->getCashOnDeliveryPrice(), 2)*$orders->getVateRate()/100, 2, '.', '');
    ?>     
  </div>  
    
  <!-- calcolo del prezzo finale -->
  <div class="wrapper-price-cart clearfix">
    <div class="clearfix totalamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right text-uppercase"><?php echo $productprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right"><?php echo number_format((float)$orders->getGrandtotalPrice(),2,'.',''); ?> &euro;</span></div>
    <?php if( $orders->getDiscountPrice() > 0 ) : ?>    
    <div class="clearfix discountamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right"><?php echo $discountprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right">-<?php echo number_format((float)$orders->getDiscountPrice(),2,'.',''); ?> &euro;</span></div>
    <?php endif; ?>       
    <div class="clearfix discountamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right"><?php echo $shipprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right"><?php echo number_format((float)$orders->getShipPrice(),2,'.',''); ?> &euro;</span></div> 
    <div class="clearfix discountamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right"><?php echo $packprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right"><?php echo number_format((float)$orders->getPackPrice(),2,'.',''); ?> &euro;</span></div> 
    <div class="clearfix discountamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right"><?php echo $deliveryprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right"><?php echo number_format((float)$orders->getCashOnDeliveryPrice(),2,'.',''); ?> &euro;</span></div> 
    <div class="clearfix discountamount"><strong class="col-md-10 col-sm-10 col-xs-8 text-right"><?php echo $ivaprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-4 text-right"><?php echo $iva ?> &euro;</span></div> 
    <div class="clearfix finalamount bg-total"><strong class="col-md-10 col-sm-10 col-xs-12 text-right text-uppercase"><?php echo $finalprice; ?></strong><span class="col-md-2 col-sm-2 col-xs-12 text-right"><?php echo ($orders->getFinalPrice()-$orders->getDiscountPrice())+$iva; ?> &euro;</span></div>
  </div>
  
  <!-- RIEPILOGO ORDINE -->
  <fieldset class="boxed">
    <legend><?php echo t('Order Summary'); ?></legend>    
    <div class="order-summary">
      <div class="row">
        <div class="form-group col-sm-6"><label><?php echo t('Order Number'); ?></label><p><?php echo $orders->getOrderId(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo t('Status'); ?></label><p><?php echo $status; ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $Shippingdate; ?></label><p><?php echo $shipping_date; ?></p>
        <?php if ($orders->getShippingDateEdit() > 0) { ?>
            <p class="red"><?php echo t('Your shipping date has shifted by ') . $orders->getShippingDateEdit() . t(' workdays'); ?></p>
        <?php } ?>    
        </div>
        <div class="form-group col-sm-6 "><label><?php echo $Shippingtype; ?></label><p><img src="<?php echo $ship_img_url; ?>" alt="" /></p></div>
        <div class="form-group col-sm-6 "><label><?php echo $paymentoptions; ?></label><p><img src="<?php echo base_path() . path_to_theme() ?>/images/<?php echo $orders->getPayOption();  ?>_icon.png" alt="" width="70" /></p></div>
        <?php if ( $orders->getNotes() != "") { ?>
          <div class="form-group col-sm-6"><label><?php echo $note; ?></label><p><?php echo $orders->getNotes(); ?></p></div>
        <?php } ?>
        
        <!-- EVENTUALE PACCO ANONIMO -->
        <?php if ( $orders->getAnonymousPackage() == 1 ) { ?>
          <div class="form-group col-sm-6"><label><?php echo $anonoshiping; ?></label><p><?php echo t("Yes"); ?></p></div>
        <?php } ?>

      </div>
    </div>       
  </fieldset>
  <fieldset class="boxed">
    <legend><?php echo $billing; ?></legend>    
    <div class="order-summary">
      <div class="row">
        <div class="form-group col-sm-6"><label><?php echo $lname; ?></label><p><?php echo $order_billing->getName(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $laddress; ?></label><p><?php echo $order_billing->getAddress(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lphone; ?></label><p><?php echo $order_billing->getPhone(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lemail; ?></label><p><?php echo $order_billing->getEmail(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lzipcode; ?></label><p><?php echo $order_billing->getZipcode(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $llocation; ?></label><p><?php echo $order_billing->getLocation(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lprovince; ?></label><p><?php echo $order_billing->getProvince(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lcontry; ?></label><p><?php echo getNationToCode( $order_billing->getCountry() ); ?></p></div>
      </div>
    </div>
  </fieldset>
  <fieldset class="boxed">
    <legend><?php echo $shipping; ?></legend>    
    <div class="order-summary">
      <div class="row">
        <div class="form-group col-sm-6"><label><?php echo $lname; ?></label><p><?php echo $order_shipping->getName(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $laddress; ?></label><p><?php echo $order_shipping->getAddress(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lphone; ?></label><p><?php echo $order_shipping->getPhone(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lemail; ?></label><p><?php echo $order_shipping->getEmail(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lzipcode; ?></label><p><?php echo $order_shipping->getZipcode(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $llocation; ?></label><p><?php echo $order_shipping->getLocation(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lprovince; ?></label><p><?php echo $order_shipping->getProvinance(); ?></p></div>
        <div class="form-group col-sm-6"><label><?php echo $lcontry; ?></label><p><?php echo getNationToCode($order_shipping->getCountry() ); ?></p></div>
      </div>
    </div>
    
    
  </fieldset>
</form>


<?php
/////////// MODAL CONTENT //////////////////////////////
//ESTRAGGO GALLERIE
$galleries = array(1,2,3);

if (count($galleries) > 0) {
  foreach ($galleries as $gallery) {
    $gallery_lang[$gallery] = MP\GalleryImagesLangQuery::create()->filterByGalleryImageId($gallery)->filterByLanguageId($lang_name)->findOne();
    $gallery_imges = MP\GalleryImagesImgQuery::create()->filterByGalleryImageId($gallery)->findOne();
    $gallery_imgs[$gallery] = json_decode($gallery_imges->getImage());
  }
}
//FINE GALLERIE
?>

<?php
  include('gallery_modal.tpl.php');
  include('publisher-editor.tpl.php');
  ?>


