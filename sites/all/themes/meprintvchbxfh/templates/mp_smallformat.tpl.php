<?php
global $base_url;
global $language;

$theme_path = base_path() . path_to_theme();
$public_path = base_path() . variable_get('file_public_path', conf_path() . '/files');

//include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
$lang_name = $language->language;

$id = $prod_id;

$bigid = "small" . $id;
$precord = MP\FormtwoLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->findOne();


/*
 * INIZIO SEO
 */

//IMPOSTO IL TITOLO SEO
$title_seo = $precord->getTitleSeo();
drupal_set_title($title_seo);

//IMPOSTO LA DESCRIZIONE SEO
$description_seo = $precord->getDescriptionSeo();
$data = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'name' => 'description',
    'content' => $description_seo,
  ),
);
drupal_add_html_head($data, 'small_format');

/*
 * FINE SEO
 */

$bigresult_findone = MP\FormtwoSmallFormatQuery::create()->filterByFormId($id)->findOne();
$longtimeprod = MP\FormtwoLongtimeProductionQuery::create()->filterByFormId($id)->find();
$countlongtimeprod = MP\FormtwoLongtimeProductionQuery::create()->filterByFormId($id)->count();

//$info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->findOne();
$info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id)->findOne();

if ($bigresult_findone->getFolding()) {

  $formtwo_foldings = MP\FoldingTypeTwoQuery::create()->find();
  $formtwo_dimensions = $formtwo_foldings[0]->getDimensions();
  $formtwo_dimensions = json_decode($formtwo_dimensions, false);

  $product_weights = $formtwo_foldings[0]->getWeights();
  $product_weights = json_decode($product_weights, false);
}
else {
  $formtwo_dimensions = MP\FormtwoDimensionsQuery::create()->filterByFormId($id)->find();
  $product_weights = MP\FormtwoWeightsQuery::create()->filterByFormId($id)->find();
}

//print_r( $product_weights );


$amounts = json_decode($bigresult_findone->getAmounts());
$amounts_discounts = json_decode($bigresult_findone->getAmountsDiscounts());

$product_material = MP\FormtwoMaterialsQuery::create()->filterByFormId($id)->find();
$extra_processings = MP\FormtwoExtraProcessingQuery::create()->filterByFormId($id)->find();

$prezzo_di_avvio = 0;
if ($bigresult_findone->getBookPrinting()) {
    $formtwo_book_weights = MP\FormtwoBookWeightsQuery::create()->filterByFormId($id)->find();
    $formtwo_book_materials = MP\FormtwoBookMaterialsQuery::create()->filterByFormId($id)->find();
    
    $prezzo_di_avvio = $bigresult_findone->getBookPrintingStartupCost();
    
    
    
}

//$countformtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->count();
//$formtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();

$countformtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->count();
$formtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->find();

$image = $info->getImage();
$my_image = file_load($image)->uri;
$mystr = '<input type="hidden" value="' . $info->getPromoFlag() . '" id="promotionalFlag">';
if($info->getPromoFlag() == 'Promo') {
    $mystr .= '<input type="hidden" value="' . $info->getPromoPriceType() . '" id="promotionalType">';
    $mystr .= '<input type="hidden" value="' . $info->getPromotionPrice() . '" id="promotionalValue">';
}
else {
    $mystr .= '<input type="hidden" value="" id="promotionalType">';
    $mystr .= '<input type="hidden" value="" id="promotionalValue">';
}
$mystr .= '<input type="hidden" value="' . $prezzo_di_avvio . '" id="bookPrintingStartupCost">';
$mystr .= '<input type="hidden" value="' . $id . '" id="formid">';
$mystr .= '<input type="hidden" value="' . $lang_name . '" id="languageid">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getFolding() . '" id="isFolding">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getBookPrinting() . '" id="isBook">';
$mystr .= '<input type="hidden" value="' . $bigresult_findone->getIncreasePercentage() . '" id="incr_percentage">';
$mystr .= '<input type="hidden" value="0" id="plus_file">';
$mystr .= '<input type="hidden" value="1" id="lessthan">';
$mystr .= '<input type="hidden" value="0" id="beforenoon">';
$mystr .= '<input type="hidden" value="'. $VAT_RATE .'" id="vat_iva">';
$mystr .= '<input type="hidden" value="'. $FILE_REVIEW_COST .'" id="file_review_cost">';

$images = json_decode($info->getImages(), true);

//ESTRAGGO GALLERIE
$galleries = json_decode($info->getGalleries());

if (count($galleries) > 0) {
  foreach ($galleries as $gallery) {
    $gallery_lang[$gallery] = MP\GalleryImagesLangQuery::create()->filterByGalleryImageId($gallery)->filterByLanguageId($lang_name)->findOne();
    $gallery_imges = MP\GalleryImagesImgQuery::create()->filterByGalleryImageId($gallery)->findOne();
    $gallery_imgs[$gallery] = json_decode($gallery_imges->getImage());
  }
}
//FINE GALLERIE
?>

<form method="POST" enctype="multipart/form-data" id="product-small-format" action="<?php echo base_path() . $lang_name; ?>/cart" autocomplete="off" >

  <input type="hidden" name="productid" value="<?php echo $id; ?>" id="productid">
  <h1 class="title-product"><?php print($precord->getName()); ?>
      &nbsp 
      <span>
          <img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image">
          <?php if ($info->getPromoFlag() == 'Promo') { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/promo.png" alt="image">
          <?php } ?>
          <?php if ($info->getPromoFlag() == 'Best Price') { ?>
            <img src="<?php print base_path() . path_to_theme(); ?>/images/best_price.png" alt="image">
          <?php } ?>
      </span>
<?php if (user_is_logged_in()) : ?>
      <?php
    $inwish= MP\WishlistQuery::create()->filterByUserId($user->uid)->filterByFormId($id)->filterByFormType("small")->filterByStatus(1)->count();
    if ($inwish == 0) { ?>
    <a id="add-prefer" data-form-type="small" data-form-id="<?php echo $id; ?>" class="add-prefer hidden-xs pull-right">
      <i class="fa fa-heart"></i><span class="hidden-sm"><?php echo t('add prefer');?></span>
    </a>
    <?php } ?>
<?php endif; ?>
  </h1>
  <div class="space-3"></div>

  <div class="top-carousel">
<!--    <div><img src="<?php //echo image_style_url("product-carousel", $my_image); ?>" /></div>-->
    <?php
    foreach ($images as $img) {
      $my_image_gal = file_load($img)->uri;
      ?>
      <div><img src="<?php echo image_style_url("product-carousel", $my_image_gal); ?>" /></div>
      <?php
    }
    ?>
  </div>
  <?php if(count($galleries) > 0) { ?>
  <div class="clearfix">
    <h2 class="title-long-desc"><?php echo $productdesc; ?></h2>
    <a href="ps-<?php echo $id; ?> #gallerymod > *" rel="lightmodal[|height:450px;width:850px]" class="gallerydiv hidden-xs">
      <img class="floatedleft" src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/pics.jpg">
      <span class="hidden-sm hidden-xs"><?php echo t('Gallery Available'); ?> <font color="#ed7400"><?php echo "MEPRINT"; ?> <i class="fa fa-info-circle meprintcircle"></i> </font></span>
    </a>
    <?php if( user_is_logged_in() ) : ?>
      <a id="add-prefer" data-form-type="big" data-form-id="<?php echo $id; ?>" class="add-prefer add-prefer-xs pull-right visible-xs">
        <i class="fa fa-heart"></i>
      </a>
    <?php endif; ?>
  </div>
  <div id="gallerymod" style="display: none">
      <img src="<?php echo $public_path;?>/gallerybanner.jpg">
      <p class="gallerytext"><?php echo t("Su questo prodotto puoi usufruire della nostra galleria di immagini, che potrai modificare con il nostro avanzato editor!"); ?></p>
  </div>
   <?php } else { ?>
  <div><h2 class="fullwidth" class="title-long-desc"><?php echo $productdesc; ?></h2></div>
   <?php } ?>
  
  <div class="description-product clearfix">
    <?php echo $precord->getDescription(); ?>
  </div>
  
  <div class="space-3"></div>
  <div class="space-3"></div>
  <div class="space-4"></div>
  <div class="order-view-hd"><?php echo $ordertitle; ?> </div>
  <input type="hidden" name="fixed_costs" value="0" id="fixed_costs" />
<?php if ($bigresult_findone->getFolding()) { ?>
    <!-- TIPOLOGIA PIEGA -->
    <div class="order-view-top order-view-section section-piega">
      <h4 class="order-h"><?php echo $lfolding; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 10"); ?></p>
      <div class="carousel">
        <?php
        $i = 1;
        foreach ($formtwo_foldings as $formtwofolding) {

          $image = $formtwofolding->getFile();

          if ($image != '') {
            $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
          }
          else {
            $my_image = $base_url . '/sites/default/files/camera.jpg';
          }
          ?>
          <div class="text-center">
            <label class="dimdiv dimdiv--circle">
              <input name="piega" class="piega-input" id="piega-<?php echo $formtwofolding->getFoldingTypeId(); ?>" value="<?php echo $formtwofolding->getFoldingTypeId(); ?>" type="radio" <?php if ($i == 1) { ?> checked="checked" <?php } ?> >
              <img src="<?php echo $my_image; ?>">
              <p><strong><?php echo $formtwofolding->getName() ?></strong></p>
            </label>
          </div>

          <?php
          $i++;
        }
        ?>
      </div>
    </div>
    <!-- END TIPOLOGIA PIEGA -->

<?php } ?>

  <!-- DIMENSIONI -->
  <div class="order-view-top order-view-section section-dimension">
    <h4 class="order-h"><?php echo $dimension; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 3"); ?></p>
    <div class="form-group">
      <div class="col-md-12 col-sm-12"></div>
      <div id="dimensions-div"><!-- OPEN DIMENSIONS DIV -->

        <?php
        $i = 1;
        foreach ($formtwo_dimensions as $formtwodimensions) {

          if ($bigresult_findone->getFolding()) {
            $dname = MP\DimensionsTwoQuery::create()->filterByDimensionId($formtwodimensions)->findOne();
          }
          else {
            $dname = MP\DimensionsTwoQuery::create()->filterByDimensionId($formtwodimensions->getDimensionId())->findOne();
          }

          $image = $dname->getFile();

          if ($image != '') {
            $my_image = image_style_url("dimensioni", file_load($image)->uri);
          }
          else {
            $my_image = $base_url . '/sites/default/files/camera.jpg';
          }


          //$my_image = image_style_url("thumbnail-new", file_load($image)->uri);  //file_create_url(file_load($image)->uri);
          ?>
          <div class="form-group col-md-3 col-sm-4 col-xs-6 text-center">
            <label class="dimdiv">
              <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
              <img  src="<?php echo $my_image; ?>"/>
              <p><strong><?php echo $dname->getDescription(); ?></strong><br>
                <!--<strong><?php// echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>-->
            </label>
            <input type ="hidden" value ="<?php echo $dname->getVertical() . '*' . $dname->getHorizontal() ?>" id = "oo<?php echo $dname->getDimensionId() ?>">
            <input type ="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight() ?>" id = "bd<?php echo $dname->getDimensionId() ?>">  
          </div>

  <?php
  $i++;
}
?>
      </div><!-- CLOSE DIMENSIONS DIV -->
      <div class="clearfix"></div>
      <div class="col-sm-12">
        <div class="space-3"></div>
        <a id="template-download" href="#">
          <div class="btn btn-danger" src="<?php //echo $base_url . '/sites/default/files/' . $templateImage[1];     ?>">
            <span class="fa fa-download"></span> <?php echo $bdownload; ?>
          </div>
        </a>
        <div class="space-3"></div>
      </div>
    </div>
  </div>
  <!-- END DIMENSIONI -->

  <!-- ORIENTAMENTO -->
  <div class="order-view-top order-view-section section-order">
    <h4 class="order-h"><?php echo $lorientation; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 11"); ?></p>
    <div class="form-group">
      <div id="f-dimensions-order-h" class="form-group col-md-3 col-sm-4 col-xs-6 text-center dimensions-order">
        <label class="dimdiv dimdiv--circle">
          <input name="order" class="order-input" id="dimensions-order-h" value="0" type="radio" checked="checked" >
          <?php
            if($id == 6 || $id == 2){
          ?>
            <img src="<?php echo $public_path; ?>/orizzontale_new.jpg">
          <?php
            }else{
          ?>
            <img src="<?php echo $public_path; ?>/orizzontale.jpg">
          <?php
            }
          ?>
          <p><strong><?php echo $orientation_h; ?></strong></p>
        </label>
      </div>
      <div id="f-dimensions-order-v" class="form-group col-md-3 col-sm-4 col-xs-6 text-center dimensions-order">
        <label class="dimdiv dimdiv--circle">
          <input name="order" class="order-input" id="dimensions-order-v" value="1" type="radio">
          <?php
            if($id == 6 || $id == 2){
          ?>
            <img src="<?php echo $public_path; ?>/verticale_new.jpg">
          <?php
            }else{
          ?>
            <img src="<?php echo $public_path; ?>/verticale.jpg">
          <?php
            }
          ?>
          <p><strong><?php echo $orientation_v; ?></strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END ORDINAMENTO -->

  <!-- MATERIALI -->
  <div class="order-view-top">
    <h4 class="order-h"><?php echo $tmaterial; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 2"); ?></p>
    
    <?php if( count($product_material) > 1 ) : ?>
    <div class="carousel">
      <?php
      $i_mt = 1;

      foreach ($product_material as $material) {

        $big_material = $material->getMaterialId();
        $material_one = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($big_material)->filterByLanguageId($lang_name)->findOne();

        $material_obj = MP\MaterialsTwoQuery::create()->findPk($big_material);
        $material_img_fid = $material_obj->getImage();
        $material_img_file = file_load($material_img_fid);
        $material_img_uri = $material_img_file->uri;

        $mat_desc = htmlentities($material_one->getDescription());
        ?>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="material" class="material_class" id="material<?php echo $big_material; ?>" value="<?php echo $big_material; ?>" <?php if ($i_mt == 1) { ?> checked="checked" <?php } ?> />
            <img src="<?php echo image_style_url("thumbnail-new", $material_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $material_img_uri); ?>" alt="" /><p><?php echo $mat_desc; ?></p></div>' />
            <p><?php echo $material_one->getName(); ?></p>
            <span id="tooltip-inline" class="visible-sm visible-xs"><?php echo strip_tags($material_one->getDescription()); ?></span>
          </label>
          <?php
          $materials_price_values = MP\MaterialsTwoQuery::create()->filterByMaterialId($big_material)->find();
          foreach ($materials_price_values as $mat_price) {
            if ($mat_price->getShortSideMinLength() != 0 && $mat_price->getShortSideMaxLength() != 0) {
              $mystr .= '<input type="hidden" name="dbheight1-' . $big_material . '" value="' . round($mat_price->getShortSideMinLength()) . '">';
              $mystr .= '<input type="hidden" name="dbheight2-' . $big_material . '" value="' . round($mat_price->getShortSideMaxLength()) . '">';
            }

            if ($mat_price->getLongSideMinLength() != 0 && $mat_price->getLongSideMaxLength() != 0) {
              $mystr .= '<input type="hidden" name="dbwidth1-' . $big_material . '" value="' . round($mat_price->getLongSideMinLength()) . '">';
              $mystr .= '<input type="hidden" name="dbwidth2-' . $big_material . '" value="' . round($mat_price->getLongSideMaxLength()) . '">';
            }
            $mystr .= '<input type="hidden" value="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+' . $mat_price->getShortSideMinLength() . '+' . $mat_price->getLongSideMinLength() . '+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '" id="mtid' . $big_material . '">';
            ?>
          </div>
          <?php
        }
        $i_mt++;
      }
      ?>
    </div>
    
    <?php else : 
      $material_data = $product_material->getData();
      $material = $material_data[0];      
    
      $big_material = $material->getMaterialId();
      $material_one = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($big_material)->filterByLanguageId($lang_name)->findOne();

      $material_obj = MP\MaterialsTwoQuery::create()->findPk($big_material);
      $material_img_fid = $material_obj->getImage();
      $material_img_file = file_load($material_img_fid);
      $material_img_uri = $material_img_file->uri;

      $mat_desc = htmlentities($material_one->getDescription());
    
      $materials_price_values = MP\MaterialsTwoQuery::create()->filterByMaterialId($big_material)->find();
      foreach ($materials_price_values as $mat_price) {
        if ($mat_price->getShortSideMinLength() != 0 && $mat_price->getShortSideMaxLength() != 0) {
          $mystr .= '<input type="hidden" name="dbheight1-' . $big_material . '" value="1">';
          $mystr .= '<input type="hidden" name="dbheight2-' . $big_material . '" value="' . round($mat_price->getShortSideMaxLength()) . '">';
        }

        if ($mat_price->getLongSideMinLength() != 0 && $mat_price->getLongSideMaxLength() != 0) {
          $mystr .= '<input type="hidden" name="dbwidth1-' . $big_material . '" value="1">';
          $mystr .= '<input type="hidden" name="dbwidth2-' . $big_material . '" value="' . round($mat_price->getLongSideMaxLength()) . '">';
        }
        $mystr .= '<input type="hidden" value="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+1+1+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '" id="mtid' . $big_material . '">';
      }
      
      ?>
      
      <div class="dimdiv--only dimdiv--circle-only col-sm-12">
        <input type="radio" name="material" class="material_class" id="material<?php echo $big_material; ?>" value="<?php echo $big_material; ?>" checked="checked" />        
        <img class="img-responsive pull-left" src="<?php echo image_style_url("gallery-thumb", $material_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo image_style_url("rectangular-tooltip", $material_img_uri); ?>' alt='' /><p><?php echo $mat_desc; ?></p></div>" />        
        <div class="dimdiv">
          <p class="mat-title"><?php echo $material_one->getName(); ?></p>
          <span class="mat-desc"><?php echo $mat_desc; ?></span>
        </div>
        
      </div>
      
      
    <?php endif; ?>
  
  </div>
  <!-- FINE MATERIALI -->

  <!-- GRAMMATURA -->
  <div class="order-view-top order-view-section section-grammatura">
    <h4 class="order-h"><?php echo $lgrammatura ;?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 12"); ?></p>
    
      <?php
      $i = 1;
      if( count( $product_weights ) > 1 ){
        echo '<div id="grammatura-div" class="carousel">';
        foreach ($product_weights as $productweight) {
          if ($bigresult_findone->getFolding() == 1) {          
            $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight)->findOne();
          }
          else {                    
            $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight->getWeightId())->findOne();
          }

          if ($weight_one) {
            $image = $weight_one->getFile();

            if ($image != '') {
              $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
            }
            else {
              $my_image = $base_url . '/sites/default/files/camera.jpg';
            }
            ?>
            <div class="text-center">
              <label class="dimdiv dimdiv--circle">
                <input name="grammatura" class="grammatura-input" id="grammatura-<?php echo $weight_one->getWeightId(); ?>" value="<?php echo $weight_one->getWeightId(); ?>" type="radio" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
                <img src="<?php echo $my_image; ?>">
                <p><strong><?php echo $weight_one->getWeight() . ' ' . $measure_weight ?></strong></p>
              </label>
              <input type="hidden" name="grammatura_maxf" id="grammatura-<?php echo $weight_one->getWeightId(); ?>-maxf" value="<?php echo $weight_one->getBookPrintingMaxFace(); ?>" >
            </div>
            
          <?php
          if ($i == 1) $maxnum= $weight_one->getBookPrintingMaxFace();
          $i++;
        }
      }
    } else { 
      echo '<div id="grammatura-div" class="form-group">';
      $product_weights_data = $product_weights->getData();
      $productweight = $product_weights_data[0];      
    
      if ($bigresult_findone->getFolding() == 1) {          
            $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight)->findOne();
          }
          else {                    
            $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight->getWeightId())->findOne();
          }

          if ($weight_one) {
            $image = $weight_one->getFile();

            if ($image != '') {
              $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
              $my_image_tooltip = image_style_url("rectangular-tooltip", file_load($image)->uri);
            }
            else {
              $my_image = $base_url . '/sites/default/files/camera.jpg';
              $my_image_tooltip = $base_url . '/sites/default/files/camera.jpg';
            }
      
          ?>

          <div class="dimdiv--only dimdiv--circle-only col-sm-12 col-xs-12">
            <input name="grammatura" class="grammatura-input" id="grammatura-<?php echo $weight_one->getWeightId(); ?>" value="<?php echo $weight_one->getWeightId(); ?>" type="radio" checked="checked" />       
            <img class="img-responsive pull-left" src="<?php echo $my_image; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title="<div class='dimdiv__hover-content'><img src='<?php echo $my_image_tooltip; ?>' alt='' /><p><?php echo $weight_one->getWeight() . ' ' . $measure_weight; ?></p></div>" />        
            <div class="dimdiv">
              <p class="mat-title"><?php echo $weight_one->getWeight() . ' ' . $measure_weight; ?></p>                
            </div>
            <input type="hidden" name="grammatura_maxf" id="grammatura-<?php echo $weight_one->getWeightId(); ?>-maxf" value="<?php echo $weight_one->getBookPrintingMaxFace(); ?>" >
          </div>
            
      <?php $maxnum= $weight_one->getBookPrintingMaxFace();} } ?>

    </div>
  </div>
  <!-- END GRAMMATURA -->


  <?php if ($bigresult_findone->getBookPrinting()) { ?>

    <!-- NUMERO FACCIATE -->
    <div class="order-view-top order-view-section section-face">
      <h4 class="order-h"><?php echo $lnumface; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 13"); ?></p>
      <div class="form-group col-md-5 col-sm-12">
          <input type="text" name="facciate" class="faceQuantity form-control required" id="faceQuantity" placeholder="8" value="8" min="8" max="<?php echo $maxnum; ?>">
      </div>

    </div>
    <!-- END NUMERO FACCIATE -->

    <!-- TIPO COPERTINA -->
    <div class="order-view-top order-view-section section-cover">
      <h4 class="order-h"><?php echo ucfirst($ltypecover); ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 14"); ?></p>
      <div class="carousel">
          
      <div class="form-group col-md-3 col-sm-3 col-xs-3 text-center">
        <label class="dimdiv dimdiv--circle">
          <input name="copertina" class="cover-input" id="cover1" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path; ?>/normale.jpg">
          <p><strong><?php echo $item_page; ?></strong></p>
        </label>
      </div>
      <div class="form-group col-md-3 col-sm-3 text-center">
        <label class="dimdiv dimdiv--circle">
          <input name="copertina" class="cover-input" id="cover2" value="2" type="radio">
          <img src="<?php echo $public_path; ?>/specifica.jpg">
          <p><strong><?php echo $different_page; ?></strong></p>
        </label>
      </div>

    </div>
      <div class="adviced-cover col-md-12">
        <?php if( count( $formtwo_book_materials ) > 0 ): ?>
        <div class="extinfodiv">
          <div class="order-view-hd order-view-hd-adviced"><?php echo t('cover material'); ?></div>
          <div class="wrapper-choose-extinfo">
            <div class="clearfix">
              <p class="label-block pull-left"><?php echo t('Choose cover'); ?></p>
              <div class="wrapper-extra-field pull-right">
                <?php
                foreach ($formtwo_book_materials as $formtwo_book_material) {                   
                  $material_one = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($formtwo_book_material->getMaterialId())->findOne();
              ?>
                
                <div class="form-group extra-check">
                  <input class="custom_cover" name="custom_cover" id="cover_<?php echo $formtwo_book_material->getMaterialId(); ?>" value="<?php echo $formtwo_book_material->getMaterialId(); ?>" type="radio">&nbsp; <?php echo $material_one->getName() ?>
                </div>
                
              <?php } ?>
              </div>
            </div>
          </div>

        </div>
        <div class="space-2"></div>
        <?php endif; ?>
        
        
        <?php if( count( $formtwo_book_weights ) > 0 ): ?>
        
        <div class="extinfodiv">
          <div class="order-view-hd order-view-hd-adviced"><?php echo t('cover weight'); ?></div>
          <div class="wrapper-choose-extinfo">
            <div class="clearfix">
              <p class="label-block pull-left"><?php echo t('choose weight'); ?></p>
              <div class="wrapper-extra-field pull-right">
              <?php
                foreach ($formtwo_book_weights as $formtwo_book_weight) { 

                  $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($formtwo_book_weight->getWeightId())->findOne();
              ?>
                  <div class="form-group extra-check">
                    <input class="extra_cover" name="custom_grammatura" id="custom_grammatura_<?php echo $formtwo_book_weight->getWeightId() ?>" value="<?php echo $formtwo_book_weight->getWeightId() ?>" type="radio" >&nbsp;<?php echo $weight_one->getWeight() . ' ' . $measure_weight; ?>
                  </div>
              <?php } ?>
                  
              </div>
            </div>
          </div>

        </div>
        <?php endif; ?>
      </div>

    </div>
    <!-- END TIPO COPERTINA -->

  <?php } ?>

  <!-- LAVORAZIONI -->
  <?php if (count($extra_processings) > 0) { ?>
    <div class="order-view-top order-view-section">
      <h4 class="order-h"><?php echo $processing; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 4"); ?></p>
      
      <?php if (count($extra_processings) > 1) : ?>
      
      <div class="carousel-lavorazioni">
        <?php
        foreach ($extra_processings as $extra_processing) {

          $processId = $extra_processing->getProcessingId();
          $process_one = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($processId)->filterByLanguageId($lang_name)->findOne();
          $process_obj = MP\ProcessingTwoQuery::create()->findPk($processId);
          $image = $process_obj->getImage();

          if ($image != '') {
            $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
            $my_image_tooltip = image_style_url("rectangular-tooltip", file_load($image)->uri);
          }
          else {
            $my_image = $base_url . '/sites/default/files/camera.jpg';
            $my_image_tooltip = $base_url . '/sites/default/files/camera.jpg';
          }
          ?>
          <div class=" text-center">
            <label class="dimdiv dimdiv--circle">
              <input type="checkbox" name="lavorazioni-input" class="lavorazioni_class" id="lavorazioni<?php echo $processId; ?>" value="<?php echo $processId; ?>" data-price="0" />
              <img src="<?php echo $my_image; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo $my_image_tooltip; ?>" alt="" /><p><?php echo htmlentities($process_one->getDescription()); ?></p></div>' />
              <p><?php echo $process_one->getName(); ?></p>
              <span id="tooltip-inline" class="visible-sm visible-xs"><?php echo strip_tags($process_one->getDescription()); ?></span>
            </label>
          </div>
        <?php } ?>
      </div>
    
      <?php else: 
        
          $extra_processings_data = $extra_processings->getData();
          $extra_processing = $extra_processings_data[0]; 
        
          $processId = $extra_processing->getProcessingId();
          $process_one = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($processId)->filterByLanguageId($lang_name)->findOne();
          $process_obj = MP\ProcessingTwoQuery::create()->findPk($processId);
          $image = $process_obj->getImage();

          if ($image != '') {
            $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
            $my_image_tooltip = image_style_url("rectangular-tooltip", file_load($image)->uri);
          }
          else {
            $my_image = $base_url . '/sites/default/files/camera.jpg';
            $my_image_tooltip = $base_url . '/sites/default/files/camera.jpg';
          }    
        
        ?>
      
      <div class="dimdiv--only dimdiv--circle-only col-sm-12">
        <input type="checkbox" name="lavorazioni-input" class="lavorazioni_class" id="lavorazioni<?php echo $processId; ?>" value="<?php echo $processId; ?>" data-price="0" checked="checked" />
        <img src="<?php echo $my_image; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo $my_image_tooltip; ?>" alt="" /><p><?php echo htmlentities($process_one->getDescription()); ?></p></div>' />
        <div class="dimdiv">
          <p class="mat-title"><?php echo $process_one->getName(); ?></p>
          <span class="mat-desc"><?php echo $process_one->getDescription(); ?></span>
        </div>
        <div class="space-2"></div>
      </div>
      <?php endif; ?>
    
    </div>
<?php } ?>
  <!-- END LAVORAZIONI -->

  <?php
//  ACCESSORI  ////////////////////////////////////// 
    if ($countformtwo_accessories != 0) {
      ?>
    <div class="order-view-top order-view-section">
      <h4 class="order-h"><?php echo $accessorie; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 7"); ?></p>
      <div class="form-group col-md-12 col-sm-12">
        
        <?php if( count( $formtwo_accessories ) > 1 ): ?>
        <div class="carousel-accessories">
          <?php
          foreach ($formtwo_accessories as $accsrysitms) {
            $eaccvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->findOne();
            $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->findOne();
            $eaccprice = $eaccvls->getPrice();
            $eaccid = $accsrysitms->getAccessoryId();
            $eacc_img_fid = $eaccvls->getFile();
            $eacc_img_file = file_load($eacc_img_fid);
            $eacc_img_uri = $eacc_img_file->uri;
            $eacc_description = htmlentities($aname->getDescription());
            ?>
            <div class=" text-center">
              <label class="dimdiv dimdiv--circle" >
                <input type="checkbox" name="extraaccessories<?php echo $eaccid; ?>" class="extraaccessories_class" id="extraaccessories<?php echo $eaccid; ?>" value="<?php echo $eaccid; ?>" />
                <img src="<?php echo image_style_url("thumbnail-new", $eacc_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $eacc_img_uri); ?>" alt="" /><p><?php echo $eacc_description ?></p></div>' />
                <p><?php echo $aname->getName() ?></p>
              </label>
              <div id="acc-qnty<?php echo $eaccid; ?>" class="acc-qnty" style="display: none;">
                <div class="form-group">
                  <label for="" style="font-weight: normal"><?php echo t('Qty'); ?></label>
                  <input type="number" name="extraaccessories-qnty_<?php echo $eaccid; ?>" class="extraaccessories-qnty-Value" id="extraaccessories-qnty_<?php echo $eaccid; ?>" data-price="<?php echo $eaccprice ?>" data-id="<?php echo $eaccid ?>" value="1" min="1">
                </div>
              </div>
            </div>
      <?php
      //$mystr .= '<input type ="hidden" value ="' . $eaccprice . '" id="extacc' . $eaccid . '">';
    }
    ?>
        </div>
      
        <?php else: 
          
          $accsrysitms_data = $formtwo_accessories->getData();
          $accsrysitms = $accsrysitms_data[0];           
          
          $eaccvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->findOne();
          $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->findOne();
          $eaccprice = $eaccvls->getPrice();
          $eaccid = $accsrysitms->getAccessoryId();
          $eacc_img_fid = $eaccvls->getFile();
          $eacc_img_file = file_load($eacc_img_fid);
          $eacc_img_uri = $eacc_img_file->uri;
          $eacc_description = htmlentities($aname->getDescription());
                    
          
          ?>
          <div class="dimdiv--only dimdiv--circle-only">
            <input type="checkbox" name="extraaccessories<?php echo $eaccid; ?>" class="extraaccessories_class" id="extraaccessories<?php echo $eaccid; ?>" value="<?php echo $eaccid; ?>" />
            <img src="<?php echo image_style_url("thumbnail-new", $eacc_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $eacc_img_uri); ?>" alt="" /><p><?php echo $eacc_description ?></p></div>' />
            <div class="dimdiv">
              <p class="mat-title"><?php echo $aname->getName(); ?></p>
              <span class="mat-desc"><?php echo $eacc_description; ?></span>
            </div>
            <div id="acc-qnty<?php echo $eaccid; ?>" class="acc-qnty" >
              <div class="form-group">
                <label for="" style="font-weight: normal"><?php echo $qty; ?></label>
                <input type="number" name="extraaccessories-qnty_<?php echo $eaccid; ?>" class="extraaccessories-qnty-Value" id="extraaccessories-qnty_<?php echo $eaccid; ?>" data-price="<?php echo $eaccprice ?>" data-id="<?php echo $eaccid ?>" value="0" min="0">
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>

  <?php } ?>



<?php
// LAVORAZIONI FRONTE-RETRO /////////////////////////
if ($bigresult_findone->getFrontBack() == 1) {
  $fr_info = node_load(array('tnid' => 63, 'language' => $language->language));
  ?>
    <div class="order-view-top section-fronteretro section-only-item">
      <h4 class="order-h"><?php echo $fronteretro; ?></h4>
      <p class="label-block clearfix"><?php echo t("explain text 8"); ?></p>
      
      <div class="form-group">
        
<!--        <div class="col-md-12">
          <label class="dimdiv--only">
            <input type="checkbox" name="fronteretro" id="fronteretro" value="1" class="fronteretro_class"/>              
            <span>se selezioni questa lavorazione devi scegliere la modalità di caricamento dei files</span>
          </label>
        </div>-->
        
        <div class="col-md-12">
          <label class="dimdiv dimdiv--fr">
            <input type="checkbox" name="fronteretro" id="fronteretro" value="1" class="fronteretro_class"/>
            <span class="fa fr-png"><img src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/bifacciale.jpg"></span>
            <div><?php echo $fr_info->body[LANGUAGE_NONE][0]['value']; ?></div>
          </label>
        </div>
        
        <input type="hidden" id="loaded" name="loaded" value="0"/>
        <div class="adviced-fr col-md-12">
          <div class="extinfodiv">
            <div class="space-2"></div>
            <div class="order-view-hd order-view-hd-adviced"><?php echo $fronteretro; ?></div>
            <div class="wrapper-choose-extinfo">
              <div class="clearfix">
                <p class="label-block pull-left"><?php echo $lchoosefr; ?></p>
                <div class="wrapper-extra-field pull-right">
                  <div class="form-group extra-check">
                    <input class="extra_side" type="radio" name="radio_fronteretro" id="radio_fronteretro1" value="1">&nbsp <?php echo $item_files; ?>
                  </div>
                  <div class="form-group extra-check">
                    <input class="extra_side" type="radio" name="radio_fronteretro" id="radio_fronteretro2" value="2">&nbsp <?php echo $different_files; ?>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
<?php } ?>


  <!-- è la parte riguardante "INFORMAZIONI SUL FILE" -->
  <div class="order-view-top order-view-section section-file">
    <h4 class="order-h"><?php echo $uploadimagetitle_prev; ?></h4>
    <div class="form-group hidden-xs hidden-sm">
      <!--TODO UPLOAD IMAGES-->
      <div class="create-graph clearfix">
        <div class="col-md-6 biggallery">
          <?php echo t("In need of a custom graphic?"); ?>
      </div>
      
      <!--<p class="col-md-6 smallgallery">
        <?php echo t("Upload your file<br>Or create it with our editor<br>Even using one of the images<br>Available for FREE!"); ?>
      </p>-->
      <p class="col-md-6 smallgallery">
        <?php echo t("Upload your file"); ?>
      </p>
      </div>
<?php
for ($i = 0; $i <= 1; $i++) {
  ?>
        <div id="upi-<?php echo $i; ?>" class="uploadproductimage">
          <!--<h4><?php echo t("Upload your own file, or choose from our gallery"); ?></h4>-->
          <h4><?php echo t("Upload your own file"); ?></h4>
          <p><?php echo t("The file must be in one of the following formats: JPG, PNG, PDF"); ?></p>
          <p><?php echo t("If available, upload the file to unlock daily shipping"); ?></p>

          <input type="hidden" name="editor_save_img_data_<?php echo $i; ?>" id="editor_save_img_data_<?php echo $i; ?>" value="" />

          <div class="pull-left">            
            <div id="image_<?php echo $i; ?>" class="fileuploader" c="<?php echo $i; ?>"><?php echo t('upload') ?></div>
             
             <!--input type="file" id="image_<?php echo $i; ?>" class="ImageUploadBig" c="<?php echo $i; ?>"/-->
            <input id="image_fid_<?php echo $i; ?>" class="upload_image_fid" type="hidden" value=""/>
            <input id="gallery_fid_<?php echo $i; ?>" class="gallery_image_fid" type="hidden" value=""/>
            <input id="editor_fid_<?php echo $i; ?>" class="editor_image_fid" type="hidden" value="" data-preview=""/>
            <div class="dvPreview_<?php echo $i; ?>"></div>
          </div>
          
            <!--<a id="custom-file-theme-<?php echo $i; ?>" class="custom-file-theme btn btn-orange" data-toggle="modal" data-target="#exampleModal"><?php echo $lchoosetheme; ?></a>
            <a id="custom-file-editor-<?php echo $i; ?>" class="btn btn-success btn-editor custom-file-editor" data-toggle="modal" data-target="#editorModal" ><i class="fa fa-paint-brush"></i> <?php echo $leditor; ?></a>
          -->
          <div class="ImageError_<?php echo $i; ?> imageerror"></div>
        </div>

<?php } ?>

      <div class="form-group col-md-12 col-sm-12">
        <?php 
          $popup_file = node_load(array('tnid' => 65, 'language' => $language->language));
          $review_desc = node_load(array('tnid' => 99, 'language' => $language->language));
        ?>
        <label class="dym-p dimdiv dimdiv--operator-extra">
          <input name="operatorreview" id="operatorreview" value="1" type="checkbox">
          <img src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/verifica-file.jpg">
          <div>
            <strong><?php echo $operatorreview . ' &euro; ' . number_format((float)$FILE_REVIEW_COST, 2, '.', ''); ?></strong>
            <br />
            <?php echo $review_desc->body[LANGUAGE_NONE][0]["value"]; ?>
          </div>
          <span class="fa fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" data-template="<div class='tooltip' role='tooltip'><div class='tooltip-arrow'></div><div class='tooltip-inner tooltip-inner-extra'></div></div>" data-title="<div class='dimdiv__hover-content dimdiv__hover-content-extra text-left'><?php echo htmlentities($popup_file->body[LANGUAGE_NONE][0]["value"]); ?></div>">
          </span>                         
        </label>       
      </div>

    </div>
  
    <div class="visible-sm visible-xs">
      <p class="col-xs-12">
        <?php echo t("complete the quote by sending the files to the desktop"); ?>
      </p>
    </div>
  
  
  </div>
  <?php
  include('gallery_modal.tpl.php');
  ?>

  <!-- End file upload  for cart -->

  <div class="clearfix"></div>
<?php
include('publisher-editor.tpl.php');
?>


  <!-- long time production  -->
  <div class="order-view-top order-view-section">
    <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingdate; ?></h4>
    <p class="label-block clearfix"><?php echo t("explain text 9"); ?></p>
    <div class="col-md-12">
      <!-- long time production  -->
      <div class="">

        <?php
//colonna in evidenza
        $promo_date_column = $bigresult_findone->getPromoDateColumn();

//controllo che il prodotto sia disponibile con quantità libera

        $amounts = json_decode($bigresult_findone->getAmounts());
        ?>
        <!-- TABELLA CON QUANTITA' PREDEFINITE  -->
        <table id="ship-table" class="table ship-table table-condensed text-center" data-type="fixed">
          <thead>
            <tr>
              <th class="ship-table__qnty">              
              <div class="ship-table__header-item hidden-xs"><?php echo $quantity; ?></div>
              <div class="ship-table__header-item visible-xs">qtn</div>
          </th>
          <th id="dx" class="dx" >
                <label class="date-div">
                  <div id="shippingdateprintdivx" class="date-div__date2" data-shippingdate="0"></div>
                </label>
           </th>
           <th id="th-d0" class="d0 <?php if($promo_date_column == 1){ ?>promo-column<?php } ?>">
          <?php if ($promo_date_column == 1) { ?> <div class="ribbon_price"><span><?php echo $bestprice; ?></span></div> <?php } ?>
          <label class="date-div">
            <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d0" value="">
            <div id="shippingdateprintdiv0" class="date-div__date" data-shippingdate="0"></div>
          </label>
          </th>
          <?php
          if ($countlongtimeprod != 0) {
            $m = 1;
            foreach ($longtimeprod as $longtimprd) {
              if ($m < 4) {
                $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();

                foreach ($longtimprdvls as $lngtmprdvls) {

                  $lngtmdays = $lngtmprdvls->getDays();
                  $lngtmamnt = $lngtmprdvls->getAmount();
                  $lngtmtyp = $lngtmprdvls->getAmountType();
                  $lngtmvals = $lngtmdays . "+" . $lngtmamnt . "+" . $lngtmtyp;
                  $type = "(%)";
                  if ($lngtmtyp == 'Value') {
                    $type = "(€)";
                  }

                  foreach ($longtimprdName as $lngtmprdName) {
                    ?>
                    <th id="th-d<?php echo $lngtmprdName->getLongtimeId(); ?>" <?php if ($promo_date_column == $m + 1) { ?> class="promo-column" <?php } ?>>
                    <?php if ($promo_date_column == $m + 1) { ?> <div class="ribbon_price"><span><?php echo $bestprice; ?></span></div> <?php } ?>
                    <label class="date-div">
                      <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d<?php echo $lngtmprdName->getLongtimeId(); ?>" value="<?php echo $lngtmprdName->getLongtimeId(); ?>">
                      <div id="shippingdateprintdiv<?php echo $m; ?>" class="date-div__date" data-shippingdate="0"></div>
                    </label>
                    </th>
                    <?php
                    $mystr .= '<input class="lngtmnpt" type ="hidden" value ="' . $lngtmvals . '" id="lngtm' . $lngtmprdName->getLongtimeId() . '" data-id="' . $lngtmprdName->getLongtimeId() . '">';
                  }
                }
              }
              $m++;
            }
          }
          ?>
          </tr>
          </thead>
          <tbody>
            <?php
            $i_amou = 0;
            foreach ($amounts as $amount) {

              if ($i_amou == 0) {
                $mystr .= '<input type ="hidden" value ="' . $amount . '" id="productQuanity">';
                $mystr .= '<input type ="hidden" value ="' . $amounts_discounts[$i_amou] . '" id="day_discount">';
              }
              ?>
              <tr>
                <td id="q<?php echo $amount; ?>" class="ship-col fixed-amounts <?php if ($i_amou == 0) echo "quantity-selected"; ?>" data-value="<?php echo $amount; ?>" data-discount="<?php echo $amounts_discounts[$i_amou]; ?>"><strong><?php echo $amount; ?></strong></td>
                <td id="q<?php echo $amount; ?>-dx" class="dx ship-cell"></td>
                <td id="q<?php echo $amount; ?>-d0" class="d0 ship-cell-fixed <?php if ($promo_date_column == 1) { ?>promo-column<?php } ?>">
                  <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm0"></span>
                </td>
                <?php
                if ($countlongtimeprod != 0) {
                  $m = 1;
                  foreach ($longtimeprod as $longtimprd) {
                    if ($m < 4) {
                      $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                      $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();
                      foreach ($longtimprdvls as $lngtmprdvls) {
                        foreach ($longtimprdName as $lngtmprdName) {
                          ?>
                          <td id="q<?php echo $amount; ?>-d<?php echo $lngtmprdName->getLongtimeId(); ?>" class="<?php if ($i_amou == 0 && $lngtmprdName->getLongtimeId() == 2) {?>dsel<?php } ?> ship-cell-fixed <?php if ($m == 1) echo "selected"; ?> <?php if ($promo_date_column == $m + 1) { ?> promo-column<?php } ?>">
                            <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm<?php echo $lngtmprdName->getLongtimeId(); ?>"></span>
                          </td>
                          <?php
                        }
                      }
                    }
                    $m++;
                  }
                }
                ?>
              </tr>
  <?php
  $i_amou++;
}
?>
          </tbody>
        </table>
        <!-- FINE TABELLA CON QUANTITA' PREDEFINITE  -->

      </div>
    </div> 
    <div class="col-md-12">
      <p> <?php echo $shipdesc; ?> </p>
    </div>
  </div>
  <!-- FINE long time production  -->
  <!-- END NUMERO DI COPIE -->
  <!-- PREVENTIVO  -->
  <div class="order-view-top order-view-section">
    <h4 class="order-h"><?php echo $quote_data; ?></h4>

    <div class="col-md-12">
      <div class="col-md-12">
        <div class="form-group">
          <label class="label-block" for=""><?php echo $quote_name; ?></label>
          <input type="text" name="name" id="name" class="form-control"/>
        </div>
        <div class="form-group">
          <label class="label-block" for=""><?php echo $note; ?></label>
          <textarea name="note" id="note" cols="30" rows="2" class="form-control"></textarea>
        </div>
      </div>
    </div>
  </div>
  <!-- FINE PREVENTIVO  -->
  <div class="clearfix"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>


  <!-- SOMMARIO  -->
  <div class="" id="pricing">
    <div class="order-view-hd"><?php echo $summery; ?></div>
    <div class="panel panel-default">
      <div class="table-responsive table-summary">
        <table id="sample-table-1" class="table table-hover">
          <tbody>
            <tr>
              <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
              <td class="hidden-480 ">
                <span class="productcostdiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <!--<tr class="extraaccessoriestd">
              <td class="left"><strong><?php //echo $accessorie; ?></strong></td>
              <td class="hidden-480 ">
                <span class="extraacccstdiv" id="extraacccstdiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
                <tr>
              <td class="left"><strong><?php //echo $singleproductcost;    ?></strong></td>
              <td>
                <div class="fa fa-eur singleproductcostdiv"></div>
              </td>
            </tr>
            <tr class="extraprocessingtd">
              <td class="left"><strong><?php //echo $extraprocessingcost; ?></strong></td>
              <td class="hidden-480 ">
                <span class="extraprocessingdiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>-->
            <tr>
              <td class="left"><strong><?php echo $quantity; ?></strong></td>
              <td class="">
                <div class="totalnoofproductbigfrmtdiv">0</div>
              </td>
            </tr>
<!--            <tr class="promopricetd">
              <td class="left"><strong><?php //echo $beforpromoprice; ?></strong></td>
              <td class="hidden-480 ">
                <span style="text-decoration:line-through" class="promopricediv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <tr class="promovaltd">
              <td class="left"><strong><?php //echo $promoval; ?></strong></td>
              <td class="hidden-480 ">
                <span class="promovaldiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <tr class="promoshiptd">
              <td class="left"><strong><?php //echo $promoship; ?></strong></td>
              <td class="hidden-480 ">
                <span class="promoshipdiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <tr class="operatorreviewtd">
              <td class="left"><strong><?php //echo $operatorreview_cost; ?></strong></td>
              <td class="hidden-480 ">
                <span class="operatorreviewdiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>-->
            <tr class="shippingdatetd">
              <td class="left red"><strong><?php echo $Shippingdate; ?></strong></td>
              <td class="hidden-480">
                <div class="shippingdatediv">0</div>
              </td>
            </tr>
            <tr>
              <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
              <td class="hidden-480 ">
                <span class="totalproductcostdiv"></span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <tr class="ivatd">
              <td class="left"><strong><?php echo $iva_cost . ' (' . $VAT_RATE . '%)'; ?></strong></td>
              <td class="hidden-480 ">
                <span class="ivadiv">0</span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
            <tr>
              <td class="left"><strong><?php echo $totalcost; ?></strong></td>
              <td class="hidden-480">
                <span class="totalcostdiv" id="totalcostdiv"></span>
                <i class="fa fa-eur"></i>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <input type="hidden" id="bigtotlprice" name="bigtotlprice" value="">
      <input type="hidden" id="type" name="type" value="small">
      <input type="hidden" value="0" id="dates_count" name="dates_count">
      <input type="hidden" id="total_cost" name="total_cost">
      <input type="hidden" id="shipping_date" name="shipping_date">
      <input type="hidden" id="promo_value" name="promo_value">
<?php
$dates_path = base_path() . path_to_theme() . "/get-dates-small.php";
?>

      <input type="hidden" value="<?php echo $dates_path; ?>" name="dates_path" id="dates_path">


    </div>
    <div class="btn-add-cart">
      <button class="btn btn-success pull-right" type="submit" name="proceed" value="Proceed" id="smallproceed"><?php echo $addcart; ?></button>
    </div>
  </div>

  <!-- FINE SOMMARIO  -->  

  <div class="clearfix"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="clearfix"></div>
  <div class="col-md-12 col-sm-12"></div>

<?php
//stampa tutti gli input hidden
echo $mystr;
?>

</form>

<?php include('product_review.tpl.php'); ?>