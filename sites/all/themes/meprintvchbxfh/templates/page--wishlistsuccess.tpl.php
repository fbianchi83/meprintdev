
<?php 
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/meprint.inc';
global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;
?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<aside class="col-md-3 col-sm-3">
  <a class="menu-mobile btn btn-success visible-xs">Menu</a>
  <?php print render($page['left_sidebar']);?>
  <div class="space-2"></div>
  
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add1.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
</aside>
<div class="col-md-6 col-sm-6">
 <header class="row"> 
  <?php  print wishlistsuccess(); ?>
   <div class="clearfix"></div>
    </header>
  </div>
    
  <aside class="col-md-3 col-sm-3">
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading"><?php echo $summary; ?></div>
          <div class="table-responsive">
            
              <?php session_start();
                  $sessionid = session_id();
                  global $language ;
                  $lang_name = $language->language ;
                   $cntSubGroups = MP\CartItemsQuery::create()
                                   ->where('CartItems.SessionId =?', $sessionid)
                                   ->count();
                  $scart = MP\CartItemsQuery::create()
                                   ->where('CartItems.SessionId =?', $sessionid)->limit(3)->find();
                  
                  if($cntSubGroups  >0){
                  ?>
              <div class="col-md-12 col-sm-12" style="padding:0px;">
                <table>
                  <tr>
                    <th class="col-md-4 col-sm-4 grid_list"><?php echo $pname; ?></th>
                    <th class="col-md-2 col-sm-2 grid_list"><?php echo $price; ?></th>
                    <th class="col-md-2 col-sm-2 grid_list"><?php echo $qty; ?></th>
                  </tr>
              </div>
              <?php
                   $i =1;
                  foreach($scart as $result){  
                      
                  $pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                    foreach($pquery as $presult){                    
                    }  ?>
              <div class="col-md-12 col-sm-12 ">
                <tr>
                    
                  <td class="col-md-6 col-sm-6">
                    <?php echo substr($presult->getName(), 0, 15); ?>
                  </td>
                  <td class="col-md-3 col-sm-3">
                    <?php echo '&euro;'.number_format((float)$result->getPrice(), 2, '.', ''); 
                    ?>
                  </td>
                  <td class="col-md-2 col-sm-2">
                    <?php echo $result->getQuanitity(); ?>
                  </td>
                </tr>
                <?php $i++; } ?>
                </table>
              </div>
              <?php }else{
          echo '<h5 style="padding:5px;">'.$nwishlist .'</h5>';
      } ?>
              
            <div class="space-2"></div>
<!--            <p class="text-center">
              <button type="button" class="btn btn-success">Success</button>
            </p>-->
          </div>
        </div>
      </div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>