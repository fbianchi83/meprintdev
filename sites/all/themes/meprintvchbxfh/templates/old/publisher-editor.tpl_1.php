<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal modal-editor fade bs-example-modal-lg" id="editorModal" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
        <div id="publisher-editor" class="modal-content">
            <input type="file" id="fileupload" style="display:none"/>
            
            <input type="hidden" id="h-id_canvas" value="page-1"/>
            <input type="hidden" id="h-z_index" value="100"/>
            <input type="hidden" id="h-sel_element" value=""/>
            <input type="hidden" id="h-object_id" value="0"/>
            <input type="hidden" id="h-objects" value=""/>
            <input type="hidden" id="h-font_size" value="8"/>
            <input type="hidden" id="h-shape_color" value="rgba(0,205,0,0.7)"/>
            
            <div data-role="page" style="width: 100%;display: block;">
                <div id="layerBrowser">
                    <ul class="nav nav-pills nav-justified">
                        <li role="presentation" class="fieldTabControl active" title='Fields'>
                            <a href="#tabs-pages">
                                <span class="fa-stack">
                                    <i class="fa fa-square-o fa-stack-2x"></i>
                                    <i class="fa fa-edit fa-stack-1x"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="imageLibraryTabControl" title='My Images'>
                            <a href="#tabs-lbImageLibrary" title='My Images'>
                                <span class="fa-stack">
                                    <i class="photostack"></i>
                                    <i class="photostack two"></i>
                                    <i class="fa fa-picture-o fa-stack-2x"></i>

                                </span>
                            </a>
                        </li>
                    </ul>
                    
                    <div class="tab-content">
                        <div id="tabs-pages" role="tabpanel" class="tab-pane active">
                            
                        </div>
                        <div id="tabs-lbImageLibrary" role="tabpanel" class="tab-pane">
                            
                        </div>
                    </div>
                    <div id="tabs-pages">
                        <div class="panel-group" id="layers" role="tablist" aria-multiselectable="true">
                        </div>
                    </div>
                    <div id="tabs-lbImageLibrary">
                    </div>
                </div>
                <div id="mainSection">
                    <div id="controlBar">
                        <div id="controlBarIn" class="btn-toolbar" role="toolbar">
                            <div class="btn-group" role="group">
                                <button class="button" type="button" id="add_image">
                                    <span class="fa fa-plus fa-1x"></span>
                                    <span class="fa fa-camera fa-2x"></span>
                                </button>
                                <button class="button" type="button" id="add_text">
                                    <span class="fa fa-plus fa-1x"></span>
                                    <span class="fa fa-font fa-2x"></span>
                                </button>
                                <button class="button" type="button" id="add_shape">
                                    <span class="fa fa-plus fa-1x"></span>
                                    <span class="fa fa-square-o fa-2x"></span>
                                </button>
                            </div>
                            <div class="btn-group" role="group">    
                                <button class="button disabled" type="button" id="undo">
                                    <span class="fa fa-reply fa-2x"></span>
                                </button>
                                <button class="button disabled" type="button" id="redo">
                                    <span class="fa fa-share fa-2x"></span>
                                </button>
                            </div>
                            <div class="btn-group" role="group">
                                <button class="button button-ok" aria-haspopup="true" type="button">
                                    <span class="fa fa-fw fa-check fa-2x"></span>
                                    <strong>Approve / Checkout</strong>
                                </button>
                            </div>
                            <div class="btn-group" role="group">
                                <button class="button button-cancel" aria-haspopup="true" type="button">
                                    <span class="fa fa-times fa-2x"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="workcanvas">
                        <ul id="pageList">
                            <li id="liPage-1" class="liPage current-page" style="display: block;">
                                <div id="page-1" class="page">
                                    <div class="pagina-taglio">
                                        <div class="pagina-sicurezza">
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li id="liPage-2" class="liPage" style="display: none;">
                                <div id="page-2" class="page">
                                    <div class="pagina-taglio">
                                        <div class="pagina-sicurezza">
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="pageBrowser">
                        <div id="pageBrowserIn">
                            <div id="pageBrowserControlBar">
                                <div id="pageBrowserBarLeft">
                                    <span id="immersiveUiToggle">
                                        <i id="collapse_left" class="fa fa-sign-out fa-flip-horizontal"></i>
                                        <i id="collapse_right" class="fa fa-sign-in" style="display: none"></i>
                                    </span>
                                    <span id="pageBrowserToggle">
                                        <i id="collapse_down" class="fa fa-caret-square-o-down"></i>
                                        <i id="collapse_up" class="fa fa-caret-square-o-up" style="display: none"></i>
                                    </span>
                                </div>
                                <div id="pageBrowserInfoArea">
                                    <div id="pageBrowserInfoAreaControls">
                                        <div id="prevPage" title="Previous Page">
                                            <span><</span>
                                        </div>
                                        <ul id="pageBrowserInfoAreaList">
                                            <li class="pbPageInfo pbPageInfo-1" style="display: inline;">
                                                <div class="pageBrowserNumbers">
                                                    <span>Page</span>
                                                    1 of 2
                                                </div>
                                            </li>
                                            <li class="pbPageInfo pbPageInfo-2" style="display: none;">
                                                <div class="pageBrowserNumbers">
                                                    <span>Page</span>
                                                    2 of 2
                                                </div>
                                            </li>
                                        </ul>
                                        <div id="nextPage" title="Next Page">
                                            <span>></span>
                                        </div>
                                    </div>
                                </div>
                                <div id="pageBrowserBarRight">
                                    <span id="fullscreenToggle">
                                        <i class="fa fa-compress"></i>
                                    </span>
                                    <div id="pageBrowserZoom" class="disabled">
                                        <div id="pageBrowserZoomControl" class="icon-magnifier-zoom fugueicon"></div>
                                        <span id="pageBrowserZoomText">
                                            <span>Zoom</span>
                                            <span>100%</span>
                                        </span>
                                        <div id="pageBrowserZoomControlSelect" data-inline="true" data-mini="true">
                                            <div id="zoom-fitHeight" class="zoom-choice" data-zoom-value="fitHeight"> Fit Height </div>
                                            <div id="zoom-fitWidth" class="zoom-choice" data-zoom-value="fitWidth"> Fit Width </div>
                                            <div id="zoom-100" class="zoom-choice" data-zoom-value="1">100%</div>
                                            <div id="zoom-125" class="zoom-choice" data-zoom-value="1.25">125%</div>
                                            <div id="zoom-150" class="zoom-choice" data-zoom-value="1.5">150%</div>
                                            <div id="zoom-175" class="zoom-choice" data-zoom-value="1.75">175%</div>
                                            <div id="zoom-200" class="zoom-choice" data-zoom-value="2">200%</div>
                                            <div id="zoom-225" class="zoom-choice" data-zoom-value="2.25">225%</div>
                                            <div id="zoom-250" class="zoom-choice" data-zoom-value="2.50">250%</div>
                                            <div id="zoom-300" class="zoom-choice" data-zoom-value="3">300%</div>
                                            <div id="zoom-400" class="zoom-choice" data-zoom-value="4">400%</div>
                                            <div class="subnav-current zoom-choice" data-zoom-value="current"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pageBrowserPagerAreaWrap">
                                <div id="pageBrowserPagerAreaIn" style="overflow-x: hidden;">
                                    <ul id="pageBrowserPagerAreaList" style="width: 100%;">
                                        <li class="pbPager pbPager-1 current-pbpage">
                                            <div id="pageBrowserPager-1" class="pager" data-page-number="1">
                                                <div class="pbImageWrap">
                                                    <img height="53.32178896663268" width="75" data-pagetext="Page" alt="Page 1" title="Page 1" src="">
                                                </div>
                                                <p>Page 1</p>
                                            </div>
                                        </li>
                                        <li class="pbPager pbPager-2">
                                            <div id="pageBrowserPager-2" class="pager" data-page-number="2">
                                                <div class="pbImageWrap">
                                                    <img height="53.32178896663268" width="75" data-pagetext="Page" alt="Page 2" title="Page 2" src="">
                                                </div>
                                                <p>Page 2</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>