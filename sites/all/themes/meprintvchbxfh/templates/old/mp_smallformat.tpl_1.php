<?php
global $base_url;
global $language;

$theme_path = base_path() . path_to_theme();
$public_path = base_path() . variable_get('file_public_path', conf_path() . '/files');

//include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';
$lang_name = $language->language;

$id = $prod_id;
$bigid= "small".$id;
$precord = MP\FormtwoLangQuery::create()->filterByFormId($id)->filterByLanguageId($lang_name)->findOne();


/*
 * INIZIO SEO
 */

//IMPOSTO IL TITOLO SEO
$title_seo = $precord->getTitleSeo();
drupal_set_title($title_seo);

//IMPOSTO LA DESCRIZIONE SEO
$description_seo = $precord->getDescriptionSeo();
$data = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'name' => 'description',
    'content' => $description_seo,
  ),
);
drupal_add_html_head($data, 'small_format');

/*
 * FINE SEO
 */

$bigresult_findone = MP\FormtwoSmallFormatQuery::create()->filterByFormId($id)->findOne();
$longtimeprod = MP\FormtwoLongtimeProductionQuery::create()->filterByFormId($id)->find();
$countlongtimeprod = MP\FormtwoLongtimeProductionQuery::create()->filterByFormId($id)->count();

$info = MP\FormtwoAdditionalInfoQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->findOne();

if ($bigresult_findone->getFolding()) {
    
    $formtwo_foldings = MP\FoldingTypeTwoQuery::create()->find(); 
    $formtwo_dimensions = $formtwo_foldings[0]->getDimensions();
    $formtwo_dimensions = json_decode($formtwo_dimensions, false);
    
    $product_weights = $formtwo_foldings[0]->getWeights();
    $product_weights = json_decode($product_weights, false);
    
} else {
    $formtwo_dimensions = MP\FormtwoDimensionsQuery::create()->filterByFormId($id)->find();
    $product_weights = MP\FormtwoWeightsQuery::create()->filterByFormId($id)->find();
}

$product_material = MP\FormtwoMaterialsQuery::create()->filterByFormId($id)->find();
$extra_processings = MP\FormtwoExtraProcessingQuery::create()->filterByFormId($id)->find();

$countformtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->count();
$formtwo_accessories = MP\FormtwoAccessoriesQuery::create()->filterByFormId($id)->filterByWebsiteId($lang_name)->find();

$image = $info->getImage();
$my_image = file_load($image)->uri;
$mystr .= '<input type="hidden" value="' . $info->getPromoFlag() . '" id="promotionalFlag">';
$mystr .= '<input type="hidden" value="' . $info->getPromoPriceType() . '" id="promotionalType">';
$mystr .= '<input type="hidden" value="' . $info->getPromotionPrice() . '" id="promotionalValue">';

$images = json_decode($info->getImages(), true);

//ESTRAGGO GALLERIE
$galleries = json_decode($info->getGalleries());

if (count($galleries) > 0) {
  foreach ($galleries as $gallery) {
    $gallery_lang[$gallery] = MP\GalleryImagesLangQuery::create()->filterByGalleryImageId($gallery)->filterByLanguageId($lang_name)->findOne();
    $gallery_imges = MP\GalleryImagesImgQuery::create()->filterByGalleryImageId($gallery)->findOne();
    $gallery_imgs[$gallery] = json_decode($gallery_imges->getImage());
  }
}
//FINE GALLERIE

?>

<form method="POST" enctype="multipart/form-data" id="product-small-format" action="<?php echo base_path() . $lang_name; ?>/cart" autocomplete="off" >

  <input type="hidden" name="productid" value="<?php echo $id; ?>" id="productid">
  <h3><?php print($precord->getName()); ?> &nbsp <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image"></span>
    <?php if( user_is_logged_in() ) : ?>
    <a id="add-prefer" data-form-type="small" data-form-id="<?php echo $id; ?>" class="add-prefer pull-right">
      <i class="fa fa-heart"></i>aggiungi ai preferiti
    </a>
    <?php endif; ?>
  </h3>
  <div class="space-3"></div>
  
  <div class="top-carousel">
    <div><img src="<?php echo image_style_url("product-carousel", $my_image); ?>" /></div>
<?php
foreach ($images as $img) {
  $my_image_gal = file_load($img)->uri;
  ?>
      <div><img src="<?php echo image_style_url("product-carousel", $my_image_gal); ?>" /></div>
      <?php
    }
    ?>
  </div>
  <div><h4><?php echo $productdesc; ?></h4></div>
  
  <?php
echo $precord->getDescription();
'<br>';
?>
  <div class="space-3"></div>
  <div class="space-3"></div>
  <div class="space-4"></div>
  <div class="order-view-hd"><?php echo $ordertitle; ?> </div>
  <input type="hidden" name="fixed_costs" value="0" id="fixed_costs" />
  <?php if ($bigresult_findone->getFolding()) { ?>
  <!-- TIPOLOGIA PIEGA -->
  <div class="order-view-top order-view-section section-piega">
    <h4 class="order-h">tipologia piega</h4>
    <div class="form-group">
        <?php
        $i = 1;
        foreach ($formtwo_foldings as $formtwofolding) { 
            
            $image = $formtwofolding->getFile();
            
            if ($image != '') {
                $my_image = image_style_url("dimensions_thumb", file_load($image)->uri);
            } else {
                $my_image = $base_url . '/sites/default/files/camera.jpg';
            }

            ?>
            <div class="form-group col-md-2 col-sm-2 text-center">
                <label class="dimdiv">
                  <input name="piega" class="piega-input" id="piega-<?php echo $formtwofolding->getFoldingTypeId(); ?>" value="<?php echo $formtwofolding->getFoldingTypeId(); ?>" type="radio" <?php if ($i == 1) { ?> checked="checked" <?php } ?> >
                  <img src="<?php echo $my_image; ?>">
                  <p><strong><?php echo $formtwofolding->getName() ?></strong></p>
                </label>
              </div>
        
        <?php 
        $i++;
            } ?>
    </div>
  </div>
  <!-- END TIPOLOGIA PIEGA -->
  
  <?php } ?>
  
  <!-- DIMENSIONI -->
  <div class="order-view-top section-dimension">
    <h4 class="order-h"><?php echo $dimension; ?></h4>
    <div class="form-group">
      <div class="col-md-12 col-sm-12"></div>
      <div id="dimensions-div"><!-- OPEN DIMENSIONS DIV -->
        
<?php
$i = 1;
foreach ($formtwo_dimensions as $formtwodimensions) {
  
    if ($bigresult_findone->getFolding()) {
        $dname = MP\DimensionsTwoQuery::create()->filterByDimensionId($formtwodimensions)->findOne();
    } else {
        $dname = MP\DimensionsTwoQuery::create()->filterByDimensionId($formtwodimensions->getDimensionId())->findOne();
    }
    
  $image = $dname->getFile();
  
  if ($image != '') {
      $my_image = image_style_url("dimensions_thumb", file_load($image)->uri);
  } else {
      $my_image = $base_url . '/sites/default/files/camera.jpg';
  }
  
  
  $my_image = image_style_url("dimensions_thumb", file_load($image)->uri);  //file_create_url(file_load($image)->uri);
  
  ?>
          <div class="form-group col-md-2 col-sm-2 text-center">
              <label class="dimdiv">
                <input type="radio" name="dimensions" class="dimension-input" id="dimensions<?php echo $dname->getDimensionId(); ?>" value="<?php echo $dname->getDimensionId(); ?>" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
                <img src="<?php echo $my_image; ?>"/>
                <p><strong><?php echo $dname->getWidth() . 'x' . $dname->getHeight(); ?></strong></p>
              </label>
              <input type ="hidden" value ="<?php echo $dname->getVertical() . '*' . $dname->getHorizontal() ?>" id = "oo<?php echo $dname->getDimensionId() ?>">
              <input type ="hidden" value ="<?php echo $dname->getWidth() . '*' . $dname->getHeight()?>" id = "bd<?php echo $dname->getDimensionId() ?>">  
            </div>
  
            <?php
        $i++;
      }
      ?>
    </div><!-- CLOSE DIMENSIONS DIV -->
    <div class="clearfix"></div>
    <div class="col-sm-12">
      <div class="space-3"></div>
      <a id="template-download" href="#">
        <div class="btn btn-danger" src="<?php //echo $base_url . '/sites/default/files/' . $templateImage[1];  ?>">
          <span class="fa fa-download"></span> <?php echo $bdownload; ?>
        </div>
      </a>
      <div class="space-3"></div>
    </div>
  </div>
</div>
  <!-- END DIMENSIONI -->

  <!-- ORIENTAMENTO -->
  <div class="order-view-top order-view-section section-order">
    <h4 class="order-h">Orientamento</h4>
    <div class="form-group">
      <div id="f-dimensions-order-h" class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="order" class="order-input" id="dimensions-order-h" value="orizzontale" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/ori.png">
          <p><strong>orizzontale</strong></p>
        </label>
      </div>
      <div id="f-dimensions-order-v" class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="order" class="order-input" id="dimensions-order-v" value="verticale" type="radio">
          <img src="<?php echo $public_path ; ?>/vert.png">
          <p><strong>verticale</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END ORDINAMENTO -->
  
  <!-- Indicazioni stampa -->
  <div class="order-view-top order-view-section section-print">
    <h4 class="order-h">Indicazioni stampa</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="print" class="print-input" id="print1" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>stampa solo fronte</strong></p>
        </label>
      </div>
      <?php if ($bigresult_findone->getFrontBack()) { ?>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="print" class="print-input" id="print2" value="2" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>stampa fronte retro</strong></p>
        </label>
      </div>
      <?php } ?>
    </div>
  </div>
  <!-- END STAMPA -->
  
  <!-- MATERIALI -->
  <div class="order-view-top">
    <h4 class="order-h"><?php echo $tmaterial; ?></h4>
    <div class="carousel-material">
      <?php
      $i_mt = 1;

      foreach ($product_material as $material) {

        $big_material = $material->getMaterialId();
        $material_one = MP\MaterialsTwoLangQuery::create()->filterByMaterialId($big_material)->filterByLanguageId($lang_name)->findOne();

        $material_obj = MP\MaterialsTwoQuery::create()->findPk($big_material);
        $material_img_fid = $material_obj->getImage();
        $material_img_file = file_load($material_img_fid);
        $material_img_uri = $material_img_file->uri;

        $mat_desc = $material_one->getDescription();
        ?>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="material" class="material_class" id="material<?php echo $big_material; ?>" value="<?php echo $big_material; ?>" <?php if ($i_mt == 1) { ?> checked="checked" <?php } ?> />
            <img src="<?php echo image_style_url("thumbnail-new", $material_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $material_img_uri); ?>" alt="" /><p><?php echo $mat_desc; ?></p></div>' />
            <p><?php echo $material_one->getName(); ?></p>
          </label>
          <?php
          $materials_price_values = MP\MaterialsTwoQuery::create()->filterByMaterialId($big_material)->find();
          foreach ($materials_price_values as $mat_price) {
            if ($mat_price->getShortSideMinLength() != 0 && $mat_price->getShortSideMaxLength() != 0) {
              $mystr .= '<input type="hidden" name="dbheight1-' . $big_material . '" value="' . round($mat_price->getShortSideMinLength()) . '">';
              $mystr .= '<input type="hidden" name="dbheight2-' . $big_material . '" value="' . round($mat_price->getShortSideMaxLength()) . '">';
            }

            if ($mat_price->getLongSideMinLength() != 0 && $mat_price->getLongSideMaxLength() != 0) {
              $mystr .= '<input type="hidden" name="dbwidth1-' . $big_material . '" value="' . round($mat_price->getLongSideMinLength()) . '">';
              $mystr .= '<input type="hidden" name="dbwidth2-' . $big_material . '" value="' . round($mat_price->getLongSideMaxLength()) . '">';
            }
            $mystr .= '<input type="hidden" value="' . $mat_price->getPrice() . '+' . $mat_price->getMinSize() . '+' . $mat_price->getShortSideMinLength() . '+' . $mat_price->getLongSideMinLength() . '+' . $mat_price->getMinimumProductionTime() . '+' . $mat_price->getWeight() . '+' . $mat_price->getThickness() . '" id="mtid' . $big_material . '">';
            ?>
          </div>
          <?php
        }
        $i_mt++;
      }
      ?>
    </div>
  </div>
  <!-- FINE MATERIALI -->
  
  <!-- GRAMMATURA -->
  <div class="order-view-top order-view-section section-grammatura">
    <h4 class="order-h">grammatura</h4>
    <div id="grammatura-div" class="form-group">
    <?php
        $i = 1;
        foreach ($product_weights as $productweight) { 
            
            if ($bigresult_findone->getFolding() == 1) {
                $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight)->findOne();
            } else {
                
                //dd($productweight->getId());
                
                $weight_one = MP\WeightsTwoQuery::create()->filterByWeightId($productweight->getId())->findOne();
            }
            if ($weight_one) {
                $image = $weight_one->getFile();

                if ($image != '') {
                    $my_image = image_style_url("dimensions_thumb", file_load($image)->uri);
                } else {
                    $my_image = $base_url . '/sites/default/files/camera.jpg';
                }
            
            ?>
            <div class="form-group col-md-2 col-sm-2 text-center">
                <label class="dimdiv">
                  <input name="grammatura" class="grammatura-input" id="grammatura-<?php echo $weight_one->getWeightId(); ?>" value="<?php echo $weight_one->getWeightId(); ?>" type="radio" <?php if ($i == 1) { ?> checked="checked" <?php } ?>/>
                  <img src="<?php echo $my_image; ?>">
                  <p><strong><?php echo $weight_one->getWeight() ?> gr/mq</strong></p>
                </label>
              </div>
        
    <?php 
            $i++;}
        
            } ?>
        
    </div>
  </div>
  <!-- END GRAMMATURA -->
  
  
  <?php if ($bigresult_findone->getBookPrinting()) { ?>
  
  
  <!-- NUMERO FACCIATE -->
  <div class="order-view-top section-face">
    <h4 class="order-h">numero facciate</h4>
    <div class="form-group col-md-5 col-sm-12">
      <input type="text" name="facciate" class="form-control " id="faceQuantity" placeholder="8" >
    </div>
    
  </div>
  <!-- END NUMERO FACCIATE -->
  
  <!-- TIPO COPERTINA -->
  <div class="order-view-top order-view-section section-cover">
    <h4 class="order-h">tipo copertina</h4>
    <div class="form-group col-md-2 col-sm-2 text-center">
      <label class="dimdiv">
        <input name="copertina" class="cover-input" id="cover1" value="1" type="radio" checked="checked" >
        <img src="<?php echo $public_path ; ?>/camera.jpg">
        <p><strong>come pagine interne</strong></p>
      </label>
    </div>
    <div class="form-group col-md-2 col-sm-2 text-center">
      <label class="dimdiv">
        <input name="copertina" class="cover-input" id="cover2" value="2" type="radio">
        <img src="<?php echo $public_path ; ?>/camera.jpg">
        <p><strong>specifica</strong></p>
      </label>
    </div>
    
    <div id="adviced-2">
      <div class="col-md-12">
        <fieldset class="extinfodiv1" style="">
          <legend class="legend--toggle">materiale copertina</legend>
          <div id="custom_cover_material" class="clearfix">
            <div class="extra-check">
              <input class="extra_side" name="custom_cover" id="cover_patinata_lucida" value="1" type="radio" checked="checked">&nbsp;copertina patinata lucida
            </div>
            <div class="extra-check">
              <input class="extra_side" name="custom_cover" id="cover_patinata_opaca" value="2" type="radio">&nbsp; copertina patinata opaca
            </div>
            
          </div>
        </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="extinfodiv1" style="">
          <legend class="legend--toggle">grammatura copertina</legend>
          <div id="custom_grammatura_material" class="clearfix">
            <div class="extra-check">
              <input class="extra_side" name="custom_grammatura" id="custom_grammatura_170" value="1" type="radio" checked="checked">&nbsp;170.00 gr/mq
            </div>
            <div class="extra-check">
              <input class="extra_side" name="custom_grammatura" id="custom_grammatura_250" value="2" type="radio">&nbsp;250.00 gr/mq
            </div>
            <div class="extra-check">
              <input class="extra_side" name="custom_grammatura" id="custom_grammatura_300" value="3" type="radio">&nbsp;300.00 gr/mq
            </div>
            
          </div>
        </fieldset>
      </div>
    </div>
    
  </div>
  <!-- END NUMERO FACCIATE -->
  
  
  
  <?php } ?>
  
  <!-- LAVORAZIONI -->
  <?php if (count($extra_processings)>0) { ?>
  <div class="order-view-top">
    <h4 class="order-h">lavorazioni</h4>
    <div class="carousel-lavorazioni">
    <?php
      foreach ($extra_processings as $extra_processing) { 
            
            $processId = $extra_processing->getProcessingId();
            $process_one = MP\ProcessingTwoLangQuery::create()->filterByProcessingId($processId)->filterByLanguageId($lang_name)->findOne();
            $process_obj = MP\ProcessingTwoQuery::create()->findPk($processId);
            $image = $process_obj->getImage();
            
            if ($image != '') {
                $my_image = image_style_url("thumbnail-new", file_load($image)->uri);
            } else {
                $my_image = $base_url . '/sites/default/files/camera.jpg';
            }
            
            ?>
        <div class=" text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="checkbox" name="lavorazioni-input" class="lavorazioni_class" id="lavorazioni<?php echo $processId; ?>" value="<?php echo $processId; ?>" />
            <img src="<?php echo image_style_url("thumbnail-new", file_load($image)->uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", file_load($image)->uri); ?>" alt="" /><p><?php echo $process_one->getDescription(); ?></p></div>' />
            <p><?php echo $process_one->getName(); ?></p>
          </label>
          </div>
          <?php
      }
      ?>
    </div>
  </div>
  <?php } ?>
  <!-- END LAVORAZIONI -->
  
  <?php
//  ACCESSORI  ////////////////////////////////////// 
if ($countformtwo_accessories != 0) {
  ?>
  <div class="order-view-top">
    <h4 class="order-h"><?php echo $accessorie; ?></h4>
    <div class="form-group col-md-12 col-sm-12">
      <div class="carousel-accessories">
        <?php
        foreach ($formtwo_accessories as $accsrysitms) {
          $eaccvls = MP\AccessoriesQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->findOne();
          $aname = MP\AccessoriesLangQuery::create()->filterByAccessoryId($accsrysitms->getAccessoryId())->filterByLanguageId($lang_name)->findOne();
          $eaccprice = $eaccvls->getPrice();
          $eaccid = $accsrysitms->getAccessoryId();
          $eacc_img_fid = $eaccvls->getFile();
          $eacc_img_file = file_load($eacc_img_fid);
          $eacc_img_uri = $eacc_img_file->uri;
          $eacc_description = $aname->getDescription();
          ?>
          <div class=" text-center">
            <label class="dimdiv dimdiv--circle" >
              <input type="checkbox" name="extraaccessories<?php echo $eaccid; ?>" class="extraaccessories_class" id="extraaccessories<?php echo $eaccid; ?>" value="<?php echo $eaccid; ?>" />
              <img src="<?php echo image_style_url("thumbnail-new", $eacc_img_uri); ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo image_style_url("rectangular-tooltip", $eacc_img_uri); ?>" alt="" /><p><?php echo $eacc_description ?></p></div>' />
              <p><?php echo $aname->getName() ?></p>
            </label>
            <div id="acc-qnty<?php echo $eaccid; ?>" class="acc-qnty" style="display: none;">
              <div class="form-group">
                <label for="" style="font-weight: normal">Qtà</label>
                <input type="number" name="extraaccessories-qnty_<?php echo $eaccid; ?>" class="extraaccessories-qnty-Value" id="extraaccessories-qnty_<?php echo $eaccid; ?>" data-price="<?php echo $eaccprice ?>" value="0">
              </div>
            </div>
          </div>
          <?php
          //$mystr .= '<input type ="hidden" value ="' . $eaccprice . '" id="extacc' . $eaccid . '">';
        }
        ?>
      </div>
    </div>
  </div>

<?php } ?>
  
  
  <!-- NUMERO DI COPIE -->
  <!-- long time production  -->
<div class="order-view-top">
  <h4 class="order-h col-md-12 col-sm-12"><?php echo $Shippingdate; ?></h4>

  <div class="col-md-12">
    <!-- long time production  -->
    <div class="col-md-12">

<?php

//colonna in evidenza
$promo_date_column = $bigresult_findone->getPromoDateColumn();

//controllo che il prodotto sia disponibile con quantità libera

$amounts = json_decode($bigresult_findone->getAmounts());            
  ?>
        <!-- TABELLA CON QUANTITA' PREDEFINITE  -->
        <table id="ship-table" class="table ship-table table-condensed text-center" data-type="fixed">
          <thead>
            <tr>
              <th class="ship-table__qnty">
                <div class="ship-table__header-item"><?php echo $quantity; ?></div>
              </th>
              <th id="th-d0" <?php if($promo_date_column == 1){ ?> class="promo-column" <?php } ?>>
                <?php if($promo_date_column == 1){ ?> <div class="ribbon_price"><span>best price</span></div> <?php } ?>
                <label class="date-div">
                  <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d0" value="">
                  <div id="shippingdateprintdiv0" class="date-div__date" data-shippingdate="0"></div>
                </label>
              </th>
          <?php
          if ($countlongtimeprod != 0) {
            $m = 1;
            foreach ($longtimeprod as $longtimprd) {
              if ($m < 4) {
                $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();

                foreach ($longtimprdvls as $lngtmprdvls) {

                  $lngtmdays = $lngtmprdvls->getDays();
                  $lngtmamnt = $lngtmprdvls->getAmount();
                  $lngtmtyp = $lngtmprdvls->getAmountType();
                  $lngtmvals = $lngtmdays . "+" . $lngtmamnt . "+" . $lngtmtyp;
                  $type = "(%)";
                  if ($lngtmtyp == 'Value') {
                    $type = "(€)";
                  }

                  foreach ($longtimprdName as $lngtmprdName) {
                    ?>
                    <th id="th-d<?php echo $lngtmprdName->getLongtimeId(); ?>" <?php if($promo_date_column == $m+1){ ?> class="promo-column" <?php } ?>>
                      <?php if($promo_date_column == $m+1){ ?> <div class="ribbon_price"><span>best price</span></div> <?php } ?>
                      <label class="date-div">
                        <input disabled="disabled" type="radio" name="lngtmprdctn" class="lngtmprdctnclass" id="d<?php echo $lngtmprdName->getLongtimeId(); ?>" value="<?php echo $lngtmprdName->getLongtimeId(); ?>">
                        <div id="shippingdateprintdiv<?php echo $m; ?>" class="date-div__date" data-shippingdate="0"></div>
                      </label>
                    </th>
                    <?php
                    $mystr .= '<input class="lngtmnpt" type ="hidden" value ="' . $lngtmvals . '" id="lngtm' . $lngtmprdName->getLongtimeId() . '" data-id="' . $lngtmprdName->getLongtimeId() . '">';
                  }
                }
              }
              $m++;
            }
          }
          ?>
          </tr>
          </thead>
          <tbody>
              <?php
              $i_amou = 0;
              foreach ($amounts as $amount) {
                  
                  if ($i_amou == 0) {
                      $mystr .= '<input type ="hidden" value ="' . $amount . '" id="productQuanity">';
                      $mystr .= '<input type ="hidden" value ="0" id="selected_day">';
                  }
                  
                  
                ?>
              <tr>
                <td id="q<?php echo $amount; ?>" class="ship-col fixed-amounts <?php if ($i_amou == 0) echo "quantity-selected"; ?>" data-value="<?php echo $amount; ?>" data-discount="<?php echo $amounts_discounts[$i_amou]; ?>"><strong><?php echo $amount; ?></strong></td>
                <td id="q<?php echo $amount; ?>-d0" class="ship-cell-fixed <?php if($promo_date_column == 1){ ?>promo-column<?php } ?>">
                    <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm0"></span>
                </td>
                <?php
                if ($countlongtimeprod != 0) {
                  $m = 1;
                  foreach ($longtimeprod as $longtimprd) {
                    if ($m < 4) {
                      $longtimprdvls = MP\LongtimeProductionQuery::create()->filterById($longtimprd->getLongtermId())->find();
                      $longtimprdName = MP\LongtimeProductionLangQuery::create()->filterByLongTimeId($longtimprd->getLongtermId())->filterByLanguageId($lang_name)->find();
                      foreach ($longtimprdvls as $lngtmprdvls) {
                        foreach ($longtimprdName as $lngtmprdName) {
                          ?>
                          <td id="q<?php echo $amount; ?>-d<?php echo $lngtmprdName->getLongtimeId(); ?>" class="ship-cell-fixed <?php if ($m == 1) echo "selected"; ?> <?php if($promo_date_column == $m+1){ ?> promo-column<?php } ?>">
                              <i class="fa fa-eur"></i><span id="total-q<?php echo $amount; ?>-lngtm<?php echo $lngtmprdName->getLongtimeId(); ?>"></span>
                          </td>
                          <?php
                        }
                      }
                    }
                    $m++;
                  }
                }
                ?>
              </tr>
    <?php
    $i_amou++;
  }
  ?>
          </tbody>
        </table>
        <!-- FINE TABELLA CON QUANTITA' PREDEFINITE  -->

    </div>
  </div> 
  <div class="col-md-12">
    <p> <?php echo $shipdesc; ?> </p>
  </div>
</div>
<!-- FINE long time production  -->
  <!-- END NUMERO DI COPIE -->
  
  <!-- è la parte riguardante "INFORMAZIONI SUL FILE" -->
  <div class="order-view-top section-file">
    <h4 class="order-h">Invio file</h4>
    <div class="form-group">
      <!--<div class="dvPreview"></div>-->
      <!--TODO UPLOAD IMAGES (VA ANCHE TRADOTTO)-->
      <div class="create-graph clearfix">
        <p class="col-md-7">Utilizza il nostro sistema per creare la tua grafica personalizzata online! <br> <span>E' semplice e veloce!</span></p>
        <div class="col-md-5">
          <a href="#" class="btn btn-success btn-block"><i class="fa fa-paint-brush fa-3x"></i> Crea la tua grafica</a>
        </div>

      </div>
      <div class="uploadproductimage">
        <h4 class="h4 uppercase">Carica un tuo file oppure scegli dalla nostra galleria </h4>
        <p>Il file deve essere in uno dei seguenti formati: JPG, PNG, PDF</p>
        <div class="pull-left">
          <input name="imagecount" value="1" type="hidden">
          <input name="image_1" id="image_1" class="ImageUploadsmall" type="file">
          <div class="dvPreviewSmall_1"></div>
        </div>
        <a class="btn btn-orange" id="custom-file-theme" data-toggle="modal" data-target="#exampleModal">Scegli tema</a>
        <a class="btn btn-success" id="custom-file-editor" data-toggle="modal" data-target="#editorModal" ><i class="fa fa-paint-brush"></i> Editor</a>

        <div class="ImageError_1 imageerror"></div>
      </div>


      <div class="form-group col-md-5 col-sm-12">        
        <label class="dym-p dimdiv dimdiv--operator-extra">
          <input name="operatorreview" id="operatorreview" value="1" type="checkbox">
          <span class="fa fa-wrench fa-4x"></span>&nbsp; Verifica file 5 €                          
        </label>       
      </div>

    </div>
  </div>
  <?php
include('gallery_modal.tpl.php');
?>
 
  <!-- End file upload  for cart -->
  
  <div class="clearfix"></div>
  <?php
    include('publisher-editor.tpl.php');
  ?>
  <div class="clearfix"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>

  
  <!-- SOMMARIO  -->
<div class="" id="pricing">
  <div class="order-view-hd"><?php echo $summery; ?></div>
  <div class="panel panel-default">
    <div class="table-responsive table-summary">
      <table id="sample-table-1" class="table table-hover">
        <tbody>
          <tr>
            <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
            <td class="hidden-480">
              <span class="printigmaterialdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
          <tr>
            <td class="left"><strong><?php echo $processcost; ?></strong></td>
            <td class="hidden-480">
              <span class="adtnlcostdiv" id="adtnlcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="baseaccessoriestd" class="baseaccessoriestd">
            <td class="left"><strong><?php echo $accessorie_base; ?></strong></td>
            <td class="hidden-480">
              <span class="baseacccstdiv" id="baseacccstdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="extraaccessoriestd" class="extraaccessoriestd">
            <td class="left"><strong><?php echo $accessorie; ?></strong></td>
            <td class="hidden-480">
              <span class="extraacccstdiv" id="extraacccstdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
          
          <tr id="extraprocessingtd" class="extraprocessingtd">
            <td class="left"><strong><?php echo $extraprocessingcost; ?></strong></td>
            <td class="hidden-480">
              <span class="extraprocessingdiv" id="extraprocessingdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr>
            <td class="left"><strong><?php echo $quantity; ?></strong></td>
            <td class="hidden-480">
              <span class="totalnoofproductbigfrmtdiv"></span>
            </td>
          </tr>

          <tr id="promopricetd" class="promopricetd">
            <td class="left"><strong><?php echo $beforpromoprice; ?></strong></td>
            <td class="hidden-480">
              <span style="text-decoration:line-through" class="promopricediv" id="promopricediv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="promovaltd" class="promovaltd">
            <td class="left"><strong><?php echo $promoval; ?></strong></td>
            <td class="hidden-480">
              <span class="promovaldiv" id="promovaldiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="operatorreviewtd" class="operatorreviewtd">
            <td class="left"><strong><?php echo $operatorreview_cost; ?></strong></td>
            <td class="hidden-480">
              <span class="operatorreviewdiv" id="operatorreviewdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr>
            <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
            <td class="hidden-480">
              <span class="totalproductcostdiv" id="totalproductcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="ivatd" class="ivatd">
            <td class="left"><strong><?php echo $iva_cost; ?></strong></td>
            <td class="hidden-480">
              <span class="ivadiv" id="ivadiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>

          <tr id="shippingdatetd" class="shippingdatetd">
            <td class="left"><strong><?php echo $Shippingdate; ?></strong></td>
            <td class="hidden-480">
              <span class="shippingdatediv" id="shippingdatediv"></span>              
            </td>
          </tr>
          
          <tr>
            <td class="left"><strong><?php echo $totalcost; ?></strong></td>
            <td class="hidden-480">
              <span class="totalcostdiv" id="totalcostdiv"></span>
              <i class="fa fa-eur"></i>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <input type="hidden" id="bigtotlprice" name="bigtotlprice" value="">
    <input type="hidden" id="type" name="type" value="big">
    <?php
    $price_path = base_path() . path_to_theme() . "/get-price.php";
    $dates_path = base_path() . path_to_theme() . "/get-dates.php";
    $shipping_path = base_path() . path_to_theme() . "/set-shipping.php";
    $fixed_costs_path = base_path() . path_to_theme() . "/get-fixed-costs.php";
    $processing_rules_path = base_path() . path_to_theme() . "/get-processing-rules.php";
    ?>
    <input type="hidden" name="price_path" value="<?php echo $price_path; ?>" id="price_path">
    <input type="hidden" value="<?php echo $dates_path; ?>" name="dates_path" id="dates_path">
    <input type="hidden" value="<?php echo $fixed_costs_path; ?>" name="fixed_costs_path" id="fixed_costs_path">
    <input type="hidden" value="<?php echo $processing_rules_path; ?>" name="processing_rules_path" id="processing_rules_path">
    <input type="hidden" value="<?php echo $shipping_path; ?>" name="shipping_path" id="shipping_path">
    <input type="hidden" value="" id="extrslct" name="extrslct">
    <input type="hidden" value="" id="extraccslct" name="extraccslct">
    <input type="hidden" value="0" id="dates_count" name="dates_count">

    <!-- input hidden per il carrello -->
    <input type="hidden" id="material_cost" name="material_cost">
    <input type="hidden" id="processing_cost" name="processing_cost">
    <input type="hidden" id="structure_cost" name="structure_cost">
    <input type="hidden" id="total_cost" name="total_cost">
    <input type="hidden" id="shipping_date" name="shipping_date">
    <input type="hidden" id="promo_value" name="promo_value">
    <!-- input hidden per il carrello -->


  </div>
  <div class="btn-add-cart">
    <button class="btn btn-success pull-right" type="submit" name="proceed" value="Proceed" id="bigproceed"><?php echo $addcart; ?></button>
  </div>
</div>

<!-- FINE SOMMARIO  -->  

  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>

  <?php
//stampa tutti gli input hidden
echo $mystr;
?>
  
</form>

 <?php include('product_review.tpl.php'); ?>