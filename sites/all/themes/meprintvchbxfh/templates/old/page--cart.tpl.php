<script type="text/javascript" src="https://parall.ax/parallax/js/jspdf.js"></script>
<?php
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/meprint.inc';
global $base_path;
global $base_url;
global $language;
$lang_name = $language->language;

?>
<div id="wrapper">
  <header>
    <section class="header-tp">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 top-nav"><?php print render($page['header_top']); ?> </div>
          <div class="col-md-6 col-sm-6 top-nav"><?php print render($page['header_top_left']); ?></div>
        </div>
      </div>
    </section>
    <section class="header-md">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-7 col-xs-12 head2-left">
            <a href="<?php echo $base_url; ?>">
              <div class="logo col-md logo-left"><img src="/sites/default/files/Meprint_logo-New.png"/></div>
            </a>
            <?php if ($user->uid != 1) { ?>
              <div class="col-md logo-right">
                <span class="fa-stack fa-lg">
                      <a href="https://twitter.com/MePrint_it" target="_blank"><i class="fa fa-twitter fa-stack-1x"></i></a>
                </span>
                <span class="fa-stack fa-lg">
                      <a href="http://www.facebook.com/MePrint.it" target="_blank"><i class="fa fa-facebook fa-stack-1x"></i></a>
                </span>
                <span class="fa-stack fa-lg">
                      <a href="http://www.youtube.com/user/MePrintWebTV" target="_blank"><i class="fa fa-youtube fa-stack-1x"></i></a>
                </span>
              </div><?php } ?>
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12 head2-left">
            <div class="mattblacktabs">
              <ul>
                <li class="selected">
                  <a href="<?php echo $base_path . 'cart'; ?>"><i class="fa fa-shopping-cart fa-lg"></i><span class="cartcount">
                        <?php
                        if (count($_POST) != 0) {
                          session_start();
                          $sesid = session_id();
                          $cntSubGroups = MP\CartItemsQuery::create()
                                                           ->where('CartItems.SessionId =?', $sesid)
                                                           ->count();

                          echo $count = $cntSubGroups + 1;
                        }
                        else {
                          session_start();
                          $sesid = session_id();
                          $cntSubGroups = MP\CartItemsQuery::create()
                                                           ->where('CartItems.SessionId =?', $sesid)
                                                           ->count();
                          echo $count = $cntSubGroups;

                        }

                        ?>
                      </span> &nbsp;<?php echo $lbl_cart; ?>
                  </a>
                </li>

                <?php
                if ($logged_in){
                  $loadeduser = user_load($user->uid);
                  $username = $loadeduser->name;
                  ?>

                  <li><a><span class="uname" style='color:#fff;'><?php print $lbl_welcome . '  ' . $username; ?></span></a></li>
                  <!-- <li><a href="<?php print $base_url;?>/user/<?php print $user->uid;?>/edit">Change password</a></li>-->
                  <li><a href="<?php echo $base_path . 'user/logout'; ?>"><?php echo $lbl_logout; ?></a></li>



                <?php }else{
                ?>


                <?php if (!$logged_in) { ?>
                  <li><a href="<?php echo $GLOBALS['base_url']; ?>/user/register"><i class="fa fa-pencil fa-lg"></i> &nbsp;<?php echo $lbl_registration; ?></a></li>
                  <li><a href="<?php echo $GLOBALS['base_url']; ?>/user/login"><i class="fa fa-key fa-lg"></i> &nbsp;<?php echo $lbl_login; ?></a></li>
                <?php }?>
              </ul>

              <?php } ?>


              <?php print render($page['header_middle']); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="header-bt">
      <div class="container">
        <div class="row col-md-12 col-sm-12">
          <div class="col-md-6 col-sm-6 head2-left">
            <?php print render($page['frontpage_left']); ?>
          </div>
          <div class="col-sm-6 col-md-6 head2-left pull-right">

            <form class="navbar-form" role="search">
              <div class="input-group col-md-11 col-sm-11">
                <?php

                if ($lang_name == 'en') { ?>
                  <input type="text" name="adserach" placeholder="Search..." class="form-control col-md-4 col-sm-4"><?php }
                else if ($lang_name == 'it') { ?>
                  <input type="text" name="adserach" placeholder="Ricerca ..." class="form-control col-md-4 col-sm-4"><?php }
                else if ($lang_name == 'fr') { ?>
                  <input type="text" name="adserach" placeholder="Recherche ..." class="form-control col-md-4 col-sm-4"><?php } ?>
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                </div>
              </div>
              <span><img src="<?php print base_path() . path_to_theme(); ?>/images/meprint-litle.png" alt="logo"></span>
            </form>
          </div>

        </div>
      </div>
    </section>
  </header>
  <!--End of the header--->
  <!--Start he Banner--->
  <section class="container">
    <div class="space-3"></div>
    <!--End of the search--->
    <article class="row">

      <div class="col-md-9 col-sm-9">
        <header class="row">
          <div class="col-sm-12 col-md-12 ">

            <?php if ($breadcrumb): echo '<a href="' . $base_url . '">' . $Home . '</a>' . ' / ' . $bcart; endif; ?>
            <div class="space-2"></div>
            <div class="cartnav">
              <div class="cartlable shiplable">
                <span class="active"> <?php echo $cartpagetittle; ?></span>
              </div>
              <div class="shippingnav">
              <span class="shiplable inactive">
              <?php echo $shippinginfo; ?></span></div>
              <div class="ordernav">
              <span class="shiplable inactive">
              <?php echo $orderconfirm; ?></span></div>
            </div>
            <div class="space-3"></div>

            <?php
            include_once DRUPAL_ROOT . '/sites/all/themes/meprint/meprint.inc';

            if (isset($_POST['wishlist']) && $_POST['wishlist'] != '') {
              print whishlist();
            }
            else if (isset($_POST['proceed']) && $_POST['type'] == "Big") {
              print cartBigIteams();
            }
            else if (isset($_POST['sbutton']) && $_POST['type'] == "small") {
              print cartSmallIteams();
            }
            else {

              session_start();
              $sessionid = session_id();
              global $language;
              $lang_name = $language->language;
              $count = MP\CartItemsQuery::create()
                                        ->where('CartItems.SessionId =?', $sessionid)
                                        ->count();

              if ($count > 0) {
                ?>
                <form class="cart-form clearfix" method="POST" action="<?php echo $base_url . '/' . $lang_name . '/shippingdetails'; ?>">
                  <div class="col-md-12 col-sm-12 product_name">
                    <div class="col-md-5 col-sm-5"><b><?php echo $cartproduct; ?></b></div>
                    <div class="col-md-3 col-sm-3 text-center"><b><?php echo $file; ?></b></div>
                    <div class="col-md-2 col-sm-2"><b><?php echo $quantity; ?></b></div>
                    <div class="col-md-2 col-sm-2 text-right" style="padding-right: 28px"><b><?php echo $price; ?></b></div>
                  </div>

                  <?php $query = MP\CartItemsQuery::create()
                                                  ->where('CartItems.SessionId =?', $sessionid)
                                                  ->orderByCartId('desc');


                  $i = 0;


                  foreach ($query as $result) {
                    $i++;
                    // $result->getProductType();
                    if ($result->getProductType() == 'Big') {
                      $pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();

                      $returnquery = MP\FormoneBigFormatQuery::create()->filterByFormId($result->getProductId())->find();
                      foreach ($pquery as $presult) { ?>
                        <?php $cartdelid = $result->getCartId() ?>

                        <div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $cartdelid; ?>">

                          <div class="clearfix"></div>

                          <div class="col-md-5 col-sm-5 ">
                            <div class="cart_4">
                              <div class="col-md-12 col-sm-12 cartdetails_4">
                                <b><span class="fa cartdetails--open fa-plus fa-2x" data-toggle="collapse" href="#cart_<?php echo $cartdelid; ?>" aria-expanded="false"></span> <?php echo $presult->getName(); ?></b></div>

                            </div>

                          </div>

                          <div class="col-md-3 col-sm-3 text-center">
                            <?php
                            // echo $result->getCartId();
                            $IMAGES = MP\CartImagesQuery::create()
                                                        ->withColumn('Max(Id)')
                                                        ->where('CartImages.CartId =?', $result->getCartId())
                                                        ->where('CartImages.Status =?', 1)
                                                        ->select(array('image'))
                                                        ->findOne();

                            $imageurl = $base_url . "/sites/default/files/cart/" . $IMAGES['image'];?>
                            <?php if ($IMAGES['image'] != '') { ?>
                              <img src="<?php echo $imageurl; ?>" width="75" heigth="75"/>
                            <?php }
                            else {
                              echo '<i class="fa fa-camera"></i>';
                            } ?>
                          </div>
                          <div class="col-md-2 col-sm-2 cartdetailsline">
                            <div class="col-md-6 col-sm-6 cartdetails">
                              <?php echo $result->getQuanitity() ?>
                            </div>
                            <div class="col-md-3 col-sm-3 cartdetails ">
                              <?php $editurl = $base_url . '/' . $lang_name . '/editcart?' . $result->getCartId(); ?>
                              <div class="col-md-12 col-sm-12 edit_icon cartdetails">
                                <a href="<?php echo $editurl; ?>">
                                  <i class="fa fa-pencil pencil"></i>

                                </a>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 cartdetails ">
                              <div class="col-md-12 col-sm-12  delete_icon cartdetails">
                                <input type="hidden" name="path" value="<?php echo $base_url; ?>/sites/all/themes/meprint/cart.php"/>
                                <a class="deletecart" url="<?php echo $result->getCartId(); ?>">
                                  <i class="fa fa-trash-o"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 cartdetailsline">
                            <div class="col-md-10 col-sm-10 cartdetails cartprice text-right">
                                <?php
                                $totlprc = $result->getPrice();
                                echo '&euro;' . number_format((float) $totlprc, 2, '.', '');
                                ?>
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="collapse" id="cart_<?php echo $cartdelid; ?>">
                              <div class="col-md-12 col-sm-12 cartdetails">
                                <span class="labelcart">
                                  <?php echo $nocopies; ?>
                                </span>
                                <span class="datacart">
                                  <?php echo $result->getQuanitity() ?>
                                </span>
                              </div>
                              <div class="col-md-12 col-sm-12 cartdetails">
                                <span class="labelcart">
                                  <?php echo $dimension; ?>
                                </span>
                                <span class="datacart">
                                  <?php
                                  // echo $result->getDimensionId();
                                  if ($result->getDimensionId() != 0) {
                                    $dquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
                                    foreach ($dquery as $mquery) {
                                      echo $mquery->getWidth() . '*' . $mquery->getHeight();
                                    }
                                  }
                                  else {
                                    echo $result->getWidth() . "*" . $result->getHeight();
                                  }
                                  ?>
                                </span>
                              </div>

                              <div class="col-md-12 col-sm-12 cartdetails">
                                <span class="labelcart">
                                  <?php echo $tmaterial; ?>
                                </span>
                                <span class="datacart">
                                 <?php
                                 $cquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
                                 foreach ($cquery as $mquery) {
                                   echo $mquery->getName();
                                 }
                                 ?>
                                </span>
                              </div>

                              <!--<div class="col-md-8 col-sm-8 "><div class="col-md-4 col-sm-4 ">
                              <?php //echo $weight ?> :
                              </div><div class="col-md-4 col-sm-4 ">
                                  -NA-
                              </div></div>-->
                              <div class="col-md-12 col-sm-12 cartdetails">
                                <span class="labelcart">
                                  <?php echo $cartdate;?>
                                </span>
                                <span class="datacart">
                                  <?php
                                  $cDATE = $result->getCreatedDate();
                                  echo $cDATE->format('Y-m-d');
                                  ?>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>


                      <?php }


                    }
                    else if ($result->getProductType() == 'small') {

                      $spquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();

                      $sreturnquery = MP\FormtwoSmallFormatQuery::create()->filterByFormId($result->getProductId())->find();

                      foreach ($spquery as $spresult) { ?>
                        <?php $cartdelid = $result->getCartId();

                        ?>
                        <div class="col-md-12 col-sm-12 cartdetailspage cart_<?php echo $cartdelid; ?>">
                          <div class="col-md-5 col-sm-5 ">
                            <div class="cart_4">
                              <div class="col-md-12 col-sm-12 cartdetails_4">
                                <b><?php echo $spresult->getName(); ?></b></div>

                              <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                      
                        <?php echo $nocopies; ?> 
                   </span><span class="datacart">
                         <?php echo $result->getQuanitity(); ?>
                    </span></div>
                              <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                        <?php echo $dimension; ?> 
                    </span><span class="datacart">
                        <?php
                        // echo $result->getDimensionId();

                        $sdquery = MP\DimensionsQuery::create()->filterByDimensionId($result->getDimensionId())->find();
                        foreach ($sdquery as $smquery) {
                          echo $smquery->getWidth() . '*' . $smquery->getHeight();
                        }

                        ?>
                    </span></div>
                              <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                        <?php echo $tmaterial; ?> 
                    </span><span class="datacart">
                         <?php
                         $scquery = MP\MaterialsLangQuery::create()->filterByMaterialId($result->getMaterialId())->filterByLanguageId($lang_name)->find();
                         foreach ($scquery as $smquery) {
                           echo $smquery->getName();
                         }
                         ?>
                    </span></div>
                              <div class="col-md-12 col-sm-12 cartdetails"><span class="labelcart">
                       <?php echo $cartdate;?> 
                    </span><span class="datacart">
                        <?php
                        $cDATE = $result->getCreatedDate();
                        echo $cDATE->format('Y-m-d');
                        ?>
                    </span></div>
                            </div>
                          </div>
                          <div class="col-md-3 col-sm-3">
                            <?php
                            $IMAGES = MP\CartImagesQuery::create()
                                                        ->withColumn('Max(Id)')
                                                        ->where('CartImages.CartId =?', $result->getCartId())
                                                        ->where('CartImages.Status =?', 1)
                                                        ->select(array('image'))
                                                        ->findOne();

                            $imageurl = $base_url . "/sites/default/files/cart/smallformat/" . $IMAGES['image'];?>
                            <?php if ($IMAGES['image'] != '') { ?>
                              <img src="<?php echo $imageurl; ?>" width="75" heigth="75"/>
                            <?php }
                            else {
                              echo '<i class="fa fa-camera"></i>';
                            } ?>
                          </div>
                          <div class="col-md-2 col-sm-2 cartdetailsline">
                            <div class="col-md-6 col-sm-6 cartdetails">
                              <?php echo $result->getQuanitity() ?>
                            </div>
                            <div class="col-md-3 col-sm-3 cartdetails ">
                              <?php $editurl = $base_url . '/' . $lang_name . '/editcart?' . $result->getCartId(); ?>
                              <div class="col-md-12 col-sm-12 edit_icon cartdetails">
                                <a href="<?php echo $editurl; ?>">
                                  <i class="fa fa-pencil pencil"></i></a>
                              </div>
                            </div>
                            <div class="col-md-3 col-sm-3 cartdetails ">
                              <div class="col-md-12 col-sm-12  delete_icon cartdetails">
                                <input type="hidden" name="path" value="<?php echo $base_url; ?>/sites/all/themes/meprint/cart.php"/>
                                <a class="deletecart" url="<?php echo $result->getCartId(); ?>">
                                  <i class="fa fa-trash-o"></i></a>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 col-sm-2 cartdetailsline">
                            <div class="col-md-10 col-sm-10 cartdetails cartprice"><b>
                                <?php
                                $totlprc = $result->getPrice();
                                echo '&euro;' . number_format((float) $totlprc, 2, '.', '');
                                ?>
                              </b></div>
                          </div>

                        </div>
                        <div class="clearfix"></div>


                      <?php }
                    }

                  }

                  $totalPrice = MP\CartItemsQuery::create()
                                                 ->withColumn('SUM(Price)')
                                                 ->where('CartItems.SessionId =?', $_SESSION['session_id'])
                                                 ->groupBySessionId()
                                                 ->select(array('price'))
                                                 ->findOne();//$totalPrice['SUMPrice']
                  ?>

                  <div class="col-md-12 col-sm-12 discountdiv">
                    <div class="col-md-12 col-sm-12">
                      <input type="checkbox" name="discountcheck" id="discountcheck"> <span class="coupanlabel"><?php echo $coupanlabel; ?></span>

                      <div class="discountyes">Coupon Code : <input type="text" name="coupancode" value="">
                        <input type="hidden" name="session_id" value="<?php echo $_SESSION['session_id']; ?>"/>
                        <input type="hidden" name="totalprice" value="<?php echo $totalPrice['SUMPrice']; ?>"/>
                        <input type="hidden" name="productprice" value=""/>
                        <input type="hidden" name="discount_id" value=""/>
                        <input type="hidden" name="couponpath" readonly="readonly" value="<?php echo $base_url; ?>/sites/all/themes/meprint/discount.php"/>
                        <button type="button" name="discountbutton" id="discountbutton" value="Submit">Update</button>
                        <div class="valid" style="display:none;"><span><?php echo $scoupon; ?> </span></div>
                        <div class="invalid" style="display:none;"><span><?php echo $fcoupon; ?> </span></div>
                      </div>
                    </div>
                    <div class="col-md-12 col-sm-12 price-summary text-right">
                        <span class=""><?php echo $producttotalprice; ?></span>
                      <span class="text-right" style="margin-right: 16px">
                         <input size="7" type="text" name="finalprice" value="<?php echo round($totalPrice['SUMPrice'], 2)?>" readonly="readonly" class="hiddp"/> &euro;
                      </span>
                      <div class="dishide">
                        <div class="col-md-7 col-sm-7">
                          <span class="discountlabel"><?php echo $discountprice; ?></span>
                        </div>
                        <div class="col-md-5 col-sm-5 discount">
                          &euro; <input size="7" type="text" name="dicountamount" readonly="readonly" value="0"/>
                        </div>

                        <div class="col-md-7 col-sm-7">
                          <span class="discountlabel"><?php echo $finalprice; ?></span>
                        </div>
                        <div class="col-md-5 col-sm-5 discount">
                          &euro; <input size="7" type="text" name="finalamount" readonly="readonly" value="<?php echo round($totalPrice['SUMPrice'], 2)?>"/>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="space-2"></div>

                  <div class="clearfix"></div>
                  <?php if ($count > 1) { ?>
                    <div class="col-md-12 col-sm-12">
                      <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="partial" checked="checked">&nbsp; <?php echo $pshipping; ?></div>
                      <div class="col-md-6 col-sm-6 pshipping"><input type="radio" name="shipping" value="complete">&nbsp; <?php echo $cshipping; ?></div>

                    </div>
                    <div class="space-2"></div>
                    <div class="space-2"></div>
                    <div class="space-2"></div>
                  <?php } ?>

                  <div class="col-md-3 col-sm-6 continue"><a href="<?php echo $base_url; ?>"><span class="btn"><?php echo $conshoping;?></span></a></div>
                  <div class="col-md-3 col-sm-6 payment">
                    <button class="btn btn-success" type="submit" name="proceedtoshipping" value="proceedtoshipping"><?php echo $proceed; ?></button>
                  </div>
                </form>
              <?php }
              else {

                echo '<h4>' . $noproductsavail . '</h4>'; ?>


              <?php }
            }
            ?>
          </div>
        </header>
        <div class="space-2"></div>
      </div>

      <aside class="col-md-3 col-sm-3">

        <div class="space-2"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
        </div>
        <div class="space-2"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
        </div>
        <div class="space-2"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
        </div>
      </aside>
    </article>
  </section>
  <section class="blue-bg">
    <div class="container">
      <?php print render($page['first_preface']); ?>
    </div>
  </section>

  <section class="lit-gra-bg">
    <div class="container">
      <?php global $base_url; ?>
      <header class="row">
        <div class="col-sm-2 col-md-2"></div>
        <div class="col-sm-8 col-md-8">
          <?php print render($page['second_preface_header']); ?>
        </div>
        <div class="col-sm-2 col-md-2"></div>
      </header>
      <article class="row">
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_left']); ?>
        </div>
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_center']); ?>
        </div>
        <div class="col-sm-4 col-md-4">
          <?php print render($page['second_preface_right']); ?>
        </div>
      </article>
    </div>
  </section>
  <footer class="footer-bg">
    <div class="container">
      <?php print render($page['footer']); ?>
    </div>
  </footer>
</div>
<script>
  jQuery(document).ready(function($) {
    $('.cartdetails--open').click(function() {
      $(this).toggleClass('fa-plus fa-minus');
    });
  });
</script>