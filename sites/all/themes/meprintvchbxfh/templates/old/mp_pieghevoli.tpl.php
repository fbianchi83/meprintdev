<?php
/* 
*   TEMPLATE FORMATO PICCOLO: PIEGHEVOLI
*/

$theme_path = base_path() . path_to_theme();
$public_path = base_path() . variable_get('file_public_path', conf_path() . '/files');

?>

<form method="POST" enctype="multipart/form-data" id="product-small-format" action="#" autocomplete="off">

  <input name="productid" value="1=" type="hidden">
  <h3>
    &nbsp pieghevoli &nbsp 
    <span><img src="<?php print base_path() . path_to_theme(); ?>/images/wave.png" alt="image"></span>
  </h3>
  <div class="space-3"></div>
  <input id="minQuantity" name="minQuantity" value="0" type="hidden">
  <div class="top-carousel">
    <div><img src="http://dummyimage.com/652x235/999999/ffffff.jpg&text=piega-1" /></div>
    <div><img src="http://dummyimage.com/652x235/999999/ffffff.jpg&text=piega-2" /></div>
    <div><img src="http://dummyimage.com/652x235/999999/ffffff.jpg&text=piega-3" /></div>
    <div><img src="http://dummyimage.com/652x235/999999/ffffff.jpg&text=piega-4" /></div>
  </div>


  <div><h4>descrizione del prodotto</h4></div>
  <div>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sem non urna fringilla iaculis. Vestibulum porta augue turpis, at accumsan risus aliquam vitae. Aenean volutpat urna vel turpis varius, a tempus leo dictum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam faucibus tortor id erat tristique venenatis.
  </div>
  <div class="space-3"></div>
 
  <div class="order-view-hd">Ordinare questo prodotto </div>
  
  <!-- TIPOLOGIA PIEGA -->
  <div class="order-view-top order-view-section section-piega">
    <h4 class="order-h">tipologia piega</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="piega" class="piega-input" id="piega-v" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/piega_v.png">
          <p><strong>piega a v</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="piega" class="piega-input" id="piega-finestra" value="2" type="radio" >
          <img src="<?php echo $public_path ; ?>/piega_fin.png">
          <p><strong>piega a finestra</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="piega" class="piega-input" id="piega-z" value="3" type="radio" >
          <img src="<?php echo $public_path ; ?>/piega_z.png">
          <p><strong>piega a z</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="piega" class="piega-input" id="piega-portafoglio" value="4" type="radio" >
          <img src="<?php echo $public_path ; ?>/piega_porta.png">
          <p><strong>piega a portafoglio</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END TIPOLOGIA PIEGA -->
  
  
  
  <!-- DIMENSIONI -->
  <div class="order-view-top order-view-section section-dimension">
    <h4 class="order-h">Dimensioni (cm)</h4>
    <div class="form-group">
      <div id="dimensions-div-small">
                
        <div class="form-group col-md-2 col-sm-2 text-center">
          <label class="dimdiv">
            <input name="dimensions" class="dimension-input" id="dimensions1" value="1" type="radio" checked="checked" >
            <img src="<?php echo $public_path ; ?>/dimensions//radio.jpg">
            <p><strong>14.80x21.00</strong></p>
          </label>
        </div>
        
        <div class="form-group col-md-2 col-sm-2 text-center">
          <label class="dimdiv">
            <input name="dimensions" class="dimension-input" id="dimensions2" value="2" type="radio">
            <img src="<?php echo $public_path ; ?>/camera.jpg">
            <p><strong>21.00x29.70</strong></p>
          </label>
        </div>
        
        <div class="form-group col-md-2 col-sm-2 text-center">
          <label class="dimdiv">
            <input name="dimensions" class="dimension-input" id="dimensions3" value="3" type="radio">
            <img src="<?php echo $public_path ; ?>/camera.jpg">
            <p><strong>29.70x42.00</strong></p>
          </label>
        </div>
            
        <input type="hidden" value="14.80*21.00" id="bd1">
        <input type="hidden" value="21.00*29.70" id="bd2">
        <input type="hidden" value="29.70*42.00" id="bd3">
        
      </div>
      <!-- CLOSE DIMENSIONS DIV -->

      <!-- DIMENSIONS CUSTOM -->
      <div class="clearfix"></div>
      <div class="form-group ">
        <div class="col-md-12 col-sm-12">
          <label><input name="dimensions" id="dimensions" value="custom" type="radio">
            Dimensioni personalizzate</label>
        </div>
      </div>

      <div class="clearfix"></div>
      <div style="display: none;" id="cstdisp" class="form-inline">
        <div class="col-md-12">
          <div class="form-group">
            <h6>Altezza</h6>
            <input name="productHeight" class="form-control" id="productHeight" placeholder="10.50" type="text">
            X
          </div>
          <div class="form-group">
            <h6>Larghezza</h6>
            <input name="productWidth" class="form-control " id="productWidth" placeholder="14.80" type="text">
            cm
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="space-3"></div>
        <div class="btn btn-danger" src="<?php echo $public_path ; ?>"><span class="fa fa-download"></span> Scarica il template</div>
      </div>
      <!-- END DIMENSIONS CUSTOM -->
    </div>
  </div>
  <!-- END DIMENSIONI -->

  <!-- ORDINAMENTO -->
  <div class="order-view-top order-view-section section-order">
    <h4 class="order-h">Ordinamento</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label id="dimdiv--order-h" class="dimdiv">
          <input name="order" class="order-input" id="dimensions-order-h" value="orizzontale" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/ori_piega.png">
          <p><strong>orizzontale</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label id="dimdiv--order-v" class="dimdiv">
          <input name="order" class="order-input" id="dimensions-order-v" value="verticale" type="radio">
          <img src="<?php echo $public_path ; ?>/vert_piega.png">
          <p><strong>verticale</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END ORDINAMENTO -->
  
  <!-- Indicazioni stampa -->
  <div class="order-view-top order-view-section section-print">
    <h4 class="order-h">Indicazioni stampa</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="print" class="print-input" id="print1" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>stampa solo fronte</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="print" class="print-input" id="print2" value="2" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>stampa fronte retro</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END STAMPA -->
  
  <!-- MATERIALI -->
  <div class="order-view-top section-material">
    <h4 class="order-h">Materiali</h4>
    <div class="form-group"> 
      <div id="material-div-small" class="carousel">
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="material" class="material_class" id="material1" value="1" checked="checked" />
            <img src="http://dummyimage.com/100x100/999999/ffffff.jpg&text=mtl1" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="http://dummyimage.com/100x100/999999/ffffff.jpg&text=mtl1" alt="" /><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec cursus sem non urna fringilla iaculis</p></div>' />
            <p>carta patinata opaca</p>
          </label>
        </div>
        <div class="text-center">
          <label class="dimdiv dimdiv--circle">
            <input type="radio" name="material" class="material_class" id="material2" value="2" />
            <img src="http://dummyimage.com/100x100/999999/ffffff.jpg&text=mtl2" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="http://dummyimage.com/100x100/999999/ffffff.jpg&text=mtl2" alt="" /><p>Integer iaculis justo quis magna vulputate dictum.</p></div>' />
            <p>carta patinata lucida</p>
          </label>
        </div>
        
        
        
      </div>
    </div>
  </div>
  <!-- END STAMPA -->
  
  <!-- GRAMMATURA -->
  <div class="order-view-top order-view-section section-grammatura">
    <h4 class="order-h">grammatura</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="grammatura" class="grammatura-input" id="grammatura1" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>115.00 gr/mq</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="grammatura" class="grammatura-input" id="grammatura1" value="1" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>170.00 gr/mq</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="grammatura" class="grammatura-input" id="grammatura1" value="1" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>300.00 gr/mq</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END GRAMMATURA -->
  
  <!-- PLASTIFICAZIONE -->
  <div class="order-view-top order-view-section section-plastificazione">
    <h4 class="order-h">plastificazione</h4>
    <div class="form-group">
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="plastificazione" class="plastificazione-input" id="plastificazione1" value="1" type="radio" checked="checked" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>nessuna plastificazione</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="plastificazione" class="plastificazione-input" id="plastificazione2" value="2" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>plastificazione opaca fronte</strong></p>
        </label>
      </div>
      <div class="form-group col-md-2 col-sm-2 text-center">
        <label class="dimdiv">
          <input name="plastificazione" class="plastificazione-input" id="plastificazione3" value="3" type="radio" >
          <img src="<?php echo $public_path ; ?>/camera.jpg">
          <p><strong>plastificazione opaca fronte retro</strong></p>
        </label>
      </div>
    </div>
  </div>
  <!-- END PLASTIFICAZIONE -->
  
  <!-- NUMERO DI COPIE -->
  <div class="order-view-top section-copie">
    <h4 class="order-h">numero copie (stesso soggetto)</h4>
    <div class="form-group">
      
      <table id="ship-table" class="table ship-table table-condensed text-center">
        <thead>
          <tr>
            <th class="ship-table__qnty">
              <div class="ship-table__header-item">Quantity</div>
            </th>
            <th id="d1">
              <div class="date-div">
                <div id="" class="date-div__date"><div class="date-div__day">Martedì</div><div class="date-div__day-n">09</div><div class="date-div__month">Giugno</div></div>
              </div>
            </th>
            <th id="d2">
              <div class="date-div">
                <div id="" class="date-div__date"><div class="date-div__day">Martedì</div><div class="date-div__day-n">09</div><div class="date-div__month">Giugno</div></div>
              </div>
            </th>
            <th id="d3">
              <div class="date-div">
                <div id="" class="date-div__date"><div class="date-div__day">Martedì</div><div class="date-div__day-n">09</div><div class="date-div__month">Giugno</div></div>
              </div>
            </th>
            <th id="d4">
              <div class="date-div">
                <div id="" class="date-div__date"><div class="date-div__day">Martedì</div><div class="date-div__day-n">09</div><div class="date-div__month">Giugno</div></div>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td id="q1" class="ship-col"><strong>250</strong></td>
            <td id="q1-d1" class="ship-cell">15,39 €</td>
            <td id="q1-d2" class="ship-cell">15,39 €</td>
            <td id="q1-d3" class="ship-cell">15,39 €</td>
            <td id="q1-d4" class="ship-cell">15,39 €</td>
          </tr>
          <tr>
            <td id="q2" class="ship-col"><strong>500</strong></td>
            <td id="q2-d1" class="ship-cell">15,39 €</td>
            <td id="q2-d2" class="ship-cell">15,39 €</td>
            <td id="q2-d3" class="ship-cell">15,39 €</td>
            <td id="q2-d4" class="ship-cell">15,39 €</td>
          </tr>
        </tbody>
      </table>
  
    </div>
  </div>
  <!-- END NUMERO DI COPIE -->
  
  <!-- è la parte riguardante "INFORMAZIONI SUL FILE" -->
  <div class="order-view-top section-file">
    <h4 class="order-h">Invio file</h4>
    <div class="form-group">
      <!--<div class="dvPreview"></div>-->
      <!--TODO UPLOAD IMAGES (VA ANCHE TRADOTTO)-->
      <div class="create-graph clearfix">
        <p class="col-md-7">Utilizza il nostro sistema per creare la tua grafica personalizzata online! <br> <span>E' semplice e veloce!</span></p>
        <div class="col-md-5">
          <a href="#" class="btn btn-success btn-block"><i class="fa fa-paint-brush fa-3x"></i> Crea la tua grafica</a>
        </div>

      </div>
      <div class="uploadproductimage">
        <h4 class="h4 uppercase">Carica un tuo file oppure scegli dalla nostra galleria </h4>
        <p>Il file deve essere in uno dei seguenti formati: JPG, PNG, PDF</p>
        <div class="pull-left">
          <input name="imagecount" value="1" type="hidden">
          <input name="image_1" id="image_1" class="ImageUploadsmall" type="file">
          <div class="dvPreviewSmall_1"></div>
        </div>
        <a class="btn btn-orange" id="custom-file-theme" data-toggle="modal" data-target="#exampleModal">Scegli tema</a>
        <a href="#" class="btn btn-success" id="custom-file-editor" data-toggle="modal" data-target="#editorModal" ><i class="fa fa-paint-brush"></i> Editor</a>

        <div class="ImageError_1 imageerror"></div>
      </div>


      <div class="form-group col-md-5 col-sm-12">        
        <label class="dym-p dimdiv dimdiv--operator-extra">
          <input name="operatorreview" id="operatorreview" value="1" type="checkbox">
          <img style="width: 100px" src="<?php echo $base_url;?>/sites/all/themes/meprintvchbxfh/images/filecheck.jpg">&nbsp; Verifica file 5 €                          
        </label>       
      </div>

    </div>
  </div>
  <div class="modal modal-gallery fade bs-example-modal-lg" id="exampleModal" role="dialog" tabindex="-1" aria-labelledby="myModalLabel" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="col-md-3">
              <ul class="nav nav-pills" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="pill">Home</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="pill">Profile</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="pill">Messages</a></li>
                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="pill">Settings</a></li>
              </ul>
            </div>
            <div class="col-md-9">
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                  <div class="row">
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-1" class="theme-input" id="theme-1" value="1" type="radio">
                        <img id="theme_img-1" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-2" class="theme-input" id="theme-2" value="2" type="radio">
                        <img id="theme_img-2" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-3" class="theme-input" id="theme-3" value="3" type="radio">
                        <img id="theme_img-3" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-4" class="theme-input" id="theme-4" value="4" type="radio">
                        <img id="theme_img-4" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-5" class="theme-input" id="theme-5" value="5" type="radio">
                        <img id="theme_img-5" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-6" class="theme-input" id="theme-6" value="6" type="radio">
                        <img id="theme_img-6" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-7" class="theme-input" id="theme-7" value="7" type="radio">
                        <img id="theme_img-7" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-8" class="theme-input" id="theme-8" value="8" type="radio">
                        <img id="theme_img-8" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-9" class="theme-input" id="theme-9" value="9" type="radio">
                        <img id="theme_img-9" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-10" class="theme-input" id="theme-10" value="10 " type="radio">
                        <img id="theme_img-10" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-1" class="theme-input" id="theme-1" value="1" type="radio">
                        <img id="theme_img-11" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                  <div class="row">
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-1" class="theme-input" id="theme-1" value="1" type="radio">
                        <img id="theme_img-1" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-2" class="theme-input" id="theme-2" value="2" type="radio">
                        <img id="theme_img-2" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-3" class="theme-input" id="theme-3" value="3" type="radio">
                        <img id="theme_img-3" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                    <div class="form-group col-md-4 col-sm-4 text-center">
                      <label class="dimdiv dimdiv--theme">
                        <input name="theme-4" class="theme-input" id="theme-4" value="4" type="radio">
                        <img id="theme_img-4" src="http://placehold.it/200x120" class="img-responsive">
                      </label></div>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages">...</div>
                <div role="tabpanel" class="tab-pane" id="settings">...</div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
          <button id="modal-save" type="button" class="btn btn-orange">Save changes</button>
        </div>
      </div>
    </div>
  </div>
 
  <!-- End file upload  for cart -->
  
  <div class="clearfix"></div>
  <?php
    include('publisher-editor.tpl.php');
  ?>
  <div class="clearfix"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>

  
  <!-- SOMMARIO -->
  <div id="pricing">
    <div class="order-view-hd">Sommario</div>
    <div class="panel panel-default">

      <div class="table-responsive table-summary">
        <table id="sample-table-1" class="table table-hover">
        
          <tbody>
            <tr>
              <td class="left"><strong>quantità</strong></td>
              <td class="hidden-480">
                <div class="totalnoofproductbigfrmtdiv">1</div>
              </td>
            </tr>
            <tr>
              <td class="left"><strong>Costi di stampa e materiali</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur printigmaterialdiv" id="printigmaterialdiv">4.00</div>
              </td>
            </tr>
            <tr>
              <td class="left"><strong>Costo per copia lavorazione base</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur adtnlcostdiv" id="adtnlcostdiv">1.00</div>
              </td>
            </tr>

            <tr style="display: none;" id="baseaccessoriestd" class="baseaccessoriestd">
              <td class="left"><strong>Accesory Cost</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur baseacccstdiv" id="baseacccstdiv"></div>
              </td>
            </tr>

            <tr>
              <td class="left"><strong>Completa Singolo costi del prodotto</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur singleproductcostdiv" id="singleproductcostdiv">5.00</div>
              </td>
            </tr>

            <tr style="display: none;" id="extraprocessingtd" class="extraprocessingtd">
              <td class="left"><strong>Costo lavorazioni accessorie</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur extraprocessingdiv" id="extraprocessingdiv"></div>
              </td>
            </tr>

            <tr style="display: none;" id="promopricetd" class="promopricetd">
              <td class="left"><strong>Price Before Promo</strong></td>
              <td class="hidden-480">
                <div style="text-decoration:line-through" class="fa fa-eur promopricediv" id="promopricediv"></div>
              </td>
            </tr>

            <tr>

              <td class="left"><strong>Costo totale del prodotto</strong></td>
              <td class="hidden-480">
                <div class="fa fa-eur totalproductcostdiv" id="totalproductcostdiv">5.00</div>
              </td>

            </tr>
          </tbody>
        </table>
      </div>
      <!--<input id="bigtotlprice" name="bigtotlprice" value="" type="hidden">
      <input name="type" value="small" type="hidden">
      <input name="price_path" value="http://www.meprint.it/sites/all/themes/meprint/get-price.php" id="price_path" type="hidden">
      <input value="http://www.meprint.it/sites/all/themes/meprint/get-dates.php" name="dates_path" id="dates_path" type="hidden">
      <input value="http://www.meprint.it/sites/all/themes/meprint/get-fixed-costs.php" name="fixed_costs_path" id="fixed_costs_path" type="hidden">
      <input value="http://www.meprint.it/sites/all/themes/meprint/get-processing-rules.php" name="processing_rules_path" id="processing_rules_path" type="hidden">
      <input value="http://www.meprint.it/sites/all/themes/meprint/set-shipping.php" name="shipping_path" id="shipping_path" type="hidden">
      <input value="" id="extrslct" name="extrslct" type="hidden">-->
    </div>
    <div class="col-md-12 col-sm-12 text-center">
      <div class="row">
        <button id="add-wishlist" class="btn btn-orange" type="submit" name="wishlist" value="wishlist">Aggiungi alla lista dei desideri</button>
        <button id="add-cart" class="btn btn-success" type="submit" name="proceed" value="Proceed" id="bigproceed">Aggiungi al carrello</button>
      </div>
      
    </div>

  </div>
  <!-- END SOMMARIO -->

  <div class="space-2 col-sm-12 col-md-12"></div>
  <div class="space-2 col-sm-12 col-md-12"></div>

</form>