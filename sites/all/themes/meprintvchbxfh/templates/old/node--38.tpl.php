<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    ?>

    <!--CONTENUTO NODO-->

    <?php  global $base_url;
    $noproduct = t('No products Avalible ');
    $sortbycat = t('Select Sub Category');
    $selsubcat = t('All');
    $sno = t('S.no');
    $Productname = t('Product name');
    $Productdesc = t('Product description');
    $media = t('Media');
    $price = t('Price');
    $noproduct = t('No products Avalible ');

    global $website;
    if($_SERVER['SERVER_NAME'] == 'www.meprint.it' || $_SERVER['SERVER_NAME'] == 'new.meprint.it'){
      $website = 'it';
    } else if ($_SERVER['SERVER_NAME'] == 'www.meprint.fr'){
      $website = 'fr';
    }
    $count=0;
    $page = isset($_GET['page'])?$_GET['page']:0;
    $noproduct = t('No products Available');
    $limit=10;
    $purl = explode("?",$_SERVER["REQUEST_URI"]);
    $list_lang = explode("/",$purl[0]);
    $prsid = explode("&page",$purl[1]);
    $prod_id = $purl[1];
    if(count($prsid)>0){
      $prod_id = $prsid[0];
    }
    $subname=""; $pro_name=""; $subdesc ="";
    global $language ;
    $lang_name = $language->language ;
    $sub_query = MP\ProductsgroupLangQuery::create()
                                          ->where('ProductsgroupLang.LanguageId =?', $lang_name)
                                          ->where('ProductsgroupLang.ProductgroupId =?', $prod_id)
                                          ->where('ProductsgroupLang.Status =?', 1)
                                          ->find();
    if(count($sub_query)>0){
      foreach($sub_query as $subgrp){
        $subname = $subgrp->getName();
        $subdesc = $subgrp->getDescription();
      }
    }
    ?>
    <header>
        <!--<div class="row">
          <ol class="breadcrumb">
            <li><a href="<?php /*echo $base_url; */?>">Home</a></li>
            <?php /*if(isset($subname) && $subname!=''){*/?>
              <li><a href="#"  class="active"><?php /*echo $subname;*/?></a></li>
            <?php /*}*/?>
          </ol>
        </div>-->
        <h4>CIAO</h4>
        <?php if(isset($subname) && isset($subdesc)){?>
          <h3><?php echo $subname; ?><span>
                  <h4>CIAO</h4>
		<img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png" style="margin-left: 15px"></span> </h3>
          <p class="description"><?php echo $subdesc; ?></p>
        <?php }?>
    </header>

    <div class="well well-lg list-filter">
      <div class="pull-right list-selector">
        <span><a href="allproducts?<?php echo $prod_id; ?>"><i class="fa fa-th fa-2x list-fa-align-justify"></i></a></span>&nbsp;
      </div>
      <form role="form" class="form-inline">
        <div class=" form-group">
          <label for="formGroupInputSmall" class="list-filter__label control-label"><?php echo $sortbycat; ?></label>

          <select class="form-control input-sm" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
            <option value=""><?php echo $selsubcat; ?></option>
            <?php
            if (count($sub_query) > 0) {
              $subcategories = MP\ProductsubgroupLangQuery::create()
                                                          ->filterByProductgroupId($subgrp->getProductgroupId())
                                                          ->filterByLanguageId($lang_name)
                                                          ->find();
              foreach ($subcategories as $subcat) {
                ?>
                <option value="<?php echo $base_url . '/' . $lang_name . '/productgridlist?' . $subcat->getproductSubgroupId(); ?>"><?php echo $subcat->getName(); ?></option>
              <?php }
            } ?>

          </select>
        </div>
      </form>
    </div>

    <!--<div class="col-md-12 col-sm-12 well well-lg">
      <div class="col-md-9 col-sm-9">
        <form role="form" class="form-horizontal">
          <div class=" form-group-sm">
            <label for="formGroupInputSmall" class="col-sm-4 control-label"><?php /*echo $sortbycat; */?></label>
            <div class="col-sm-7">
              <select class="form-control input-sm" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                <option value=""><?php /*echo $selsubcat; */?></option>
                <?php
/*                $subcategories= MP\ProductsubgroupLangQuery::create()
                                                           ->filterByProductgroupId($subgrp->getProductgroupId())
                                                           ->filterByLanguageId($lang_name)
                                                           ->find();
                foreach($subcategories as $subcat){*/?>
                  <option value="<?php /*echo $base_url.'/'.$lang_name.'/productlisting?'. $subcat->getproductSubgroupId(); */?>"><?php /*echo $subcat->getName(); */?></option>
                <?php /*} */?>

              </select>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-2 col-sm-2 pull-right">
        <span><a href="allproducts?<?php /*echo $prod_id; */?>"><i class="fa fa-th fa-2x list-fa-align-justify"></i></a></span>&nbsp;
      </div>
    </div>-->
      <section class="">
        <?php

        $listing_query = MP\FormoneBigFormatQuery::create()
                                                 ->filterByProductgroupId($prod_id)
                                                 ->filterByStatus(1)
                                                 ->find();
        if(count($listing_query )>0){?>

        <table><tr><th  class="col-sm-1 col-md-1 grid_list"><?php echo $sno; ?></th>
            <th class="col-sm-4 col-md-4 grid_list "><?php echo $Productname; ?></th>
            <th class="col-sm-4 col-md-4 grid_list "><?php echo $Productdesc; ?></th>
            <th class="col-sm-4 col-md-4 grid_list "><?php echo $media; ?></th>
            <th class="col-sm-4 col-md-4  grid_list"><?php echo $price; ?></th></tr>

          <?php
          $i=1;
          foreach($listing_query as $listing){
            $id = $listing->getFormId();
            $result_lang = MP\FormoneLangQuery::create()
                                              ->where('FormoneLang.LanguageId =?', $lang_name)
                                              ->where('FormoneLang.FormId =?', $id)
                                              ->find();
            foreach($result_lang as $check_lang){
              $name = substr($check_lang->getName(), 0, 25);
              $description = substr($check_lang->getDescription(), 0, 125);
              $price = $listing->getBasePriceForQuantity();
              $check_query = MP\FormoneAdditionalInfoQuery::create()
                                                          ->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                                                          ->where('FormoneAdditionalInfo.FormId =?', $id)
                                                          ->find();
              $count+=  MP\FormoneAdditionalInfoQuery::create()
                                                     ->where('FormoneAdditionalInfo.WebsiteId =?', $website)
                                                     ->where('FormoneAdditionalInfo.FormId =?', $id)
                                                     ->count();
              foreach($check_query as $check){
                $r++;
                $mypage = $page * $limit;
                $lastrec = $mypage +10;
                if($r>=$mypage && $r<=($mypage+10)){
                  $p++;
                  $promo = $check->getPromoFlag();
                  $image = $check->getImage();
                  $uuu=file_load($image)->uri;
                  $my_image=explode("://",$uuu); ?>
                  <div class="col-md-12 col-sm-12 ">
                    <tr><td class="col-sm-1 col-md-1  grid_list"><?php echo $r; ?></td>
                      <td class="col-sm-4 col-md-4  grid_list"> <a href="<?php echo $base_url.'/product-three-column?'.$id.'&type=Big';?>" ><?php echo $name; ?></a></td>
                      <td class="col-sm-4 col-md-4  grid_list"><?php echo $description; ?></td>
                      <td class="col-sm-4 col-md-4  grid_list"> <a href="<?php echo $base_url.'/product-three-column?'.$id.'&type=Big';?>" ><img alt="" src="<?php echo $base_url.'/sites/default/files/'.$my_image[1];?>" width="50px" height="50px"></a></td>
                      <td class="col-sm-4 col-md-4  grid_list">&euro; <?php echo $price; ?></td></tr></div>

                <?php   } } }    }  if($p<=0){  ?>
            <div class="col-md-12 col-sm-12 well well-lg" align="center">
              <span style="font-weight:bold;color:red;"><?php echo $noproduct;?></span>
            </div>
          <?php }?>
        </table>
      </section>

  <?php }else{ ?>
    <div class="col-md-12 col-sm-12 well well-lg" align="center">
      <span style="font-weight:bold;color:red;"><?php echo $noproduct;?></span>
    </div>
  <?php }?>
    <footer class="row">
      <div class="col-sm-12 col-md-12">
        <div class="col-sm-3 col-md-3">
          <nav>
            <!--<ul class="pager">
            <li class="previous"><a href="#">← Older</a></li>
            </ul>-->
          </nav>
        </div>
        <div class="col-sm-9 col-md-9 text-center">
          <nav>
            <?php $pagcnt = round($count/10); if($count >10){?>
              <ul class="pagination">
                <?php for($g=1;$g<=$pagcnt;$g++){
                  $f = $g -1;
                  $class='';
                  if($f==$page){ $class='active';}
                  if($g==1){?>
                    <li class="<?php echo $class;?>"><a href="allproductslist?<?php echo $prod_id;?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                  <?php }else{
                    ?>
                    <li class="<?php echo $class;?>"><a href="allproductslist?<?php echo $prod_id;?>&page=<?php echo $f;?>"> <?php echo $g;?> <span class="sr-only">(current)</span></a></li>
                  <?php } }?>
              </ul>
            <?php }?>
          </nav>
        </div>
        <nav>
          <!--<ul class="pager">
          <li class="next"><a href="#">Newer →</a></li>
          </ul>-->
        </nav>
      </div>
  </footer>

  <!--FINE CONTENUTO NODO-->
</div>

<?php
// Remove the "Add new comment" link on the teaser page or if the comment
// form is being displayed on the same page.
if ($teaser || !empty($content['comments']['comment_form'])) {
  unset($content['links']['comment']['#links']['comment-add']);
}
// Only display the wrapper div if there are links.
$links = render($content['links']);
if ($links):
  ?>
  <div class="link-wrapper">
    <?php print $links; ?>
  </div>
<?php endif; ?>

<?php print render($content['comments']); ?>

</div>
