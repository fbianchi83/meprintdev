<?php global $base_url; ?>
<script type="text/javascript" src="<?php  echo $base_url;  ?>/sites/all/themes/meprint/scripts/jquery.validate.js"></script>
<script type="text/javascript">
$(document).ready(function() { 
    $('#paybutton').click(function(){ 
     $("#shipping-form").validate({
           rules: { 
               fnames:{
                    required: true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/namevalidation.php", async: false }
               },
               emails:{
                    required: true,
                    email :true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/emailvalidation.php", async: false }
                    
               },
               phone:{
                   required: true,
                   number :true,
               },
               address:{
                    required: true
               },
               zipcode:{
                    required: true
            
               },
               location:{
                    required: true
               },
               provience :{
                    required: true
               },
               country : {
                   required: true
               },
               saddress : {
                   required: true
               },
               szipcode:{
                   required: true
               },slocation:{
                   required: true
               },sprovience :{
                   required: true
               },
               scountry : {
                   required: true
               },
                       pay_options:{ 
                           required: true
                       }
             
           },
            messages: {
                 fnames:{
                    required: "Please Enter Name",
                    remote: "Sorry Username Already Existed",
               },
                 emails:{
                    required: "Please Enter Email",
                    email :"Please Enter valid Email",
                    remote :"Sorry Email Already Existed"
               },
                 phone:{
                   required: "Please Enter Phone",
                   number :"Please Enter Numbers only"
               },
                address:{
                    required: "Please Enter Addresss"
               },
               zipcode:{
                    required: "Please Enter Zipcode"
               },
               location:{
                    required: "Please Enter Location"
               },
               provience :{
                    required: "Please Enter Provience"
               },
               country : {
                   required: "Please Enter Country"
               },
               saddress : {
                   required: "Please Enter Address"
               },
               szipcode:{
                   required: "Please Enter Zipcode"
               },slocation:{
                   required: "Please Enter Location"
               },sprovience :{
                   required: "Please Enter Provience"
               },
               scountry : {
                   required: "Please Enter Country"
               },
                pay_options:{ 
                           required: "Please Select Payment Type"
                       }
            }
     });
     });
});

</script>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php global $base_path;
global $language;
$lang_name = $language->language;
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/language_theme.inc';
include_once DRUPAL_ROOT . '/sites/all/themes/meprintvchbxfh/meprint.inc';
global $base_url;?>
<div id="wrapper">
  <header>
    <section class="header-tp">
      <div class="container">
        <div class="row">
          <!--<div class="col-md-12">-->
            <div class="col-md-6 col-sm-6 top-nav">
				<?php print render($page['header_top']); ?>
            </div>
          <div class="col-md-6 col-sm-6 top-nav">
				<?php print render($page['header_top_left']); ?>
			</div>
      </div>
      </div>
    </section>
    <section class="header-md">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-7 col-xs-12 head2-left">
            <a href="http://www.meprint.it">
              <div class="logo col-md logo-left">
                <img src="/sites/default/files/Meprint_logo-New.png"/>
              </div>
            </a>
            <?php if($user->uid != 1){?>
            <div class="col-md logo-right">
              <span class="fa-stack fa-lg">
                <a href="https://twitter.com/MePrint_it" target="_blank">
                  <i class="fa fa-twitter fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack fa-lg">
                <a href="http://www.facebook.com/MePrint.it" target="_blank">
                  <i class="fa fa-facebook fa-stack-1x"></i>
                </a>
              </span>
              <span class="fa-stack fa-lg">
                <a href="http://www.youtube.com/user/MePrintWebTV" target="_blank">
                  <i class="fa fa-youtube fa-stack-1x"></i>
                </a>
              </span>
            </div>
            <?php }?>
          </div>
          <div class="col-md-5 col-sm-5 col-xs-12 head2-left">
            <div class="mattblacktabs">
              <ul>
                <li class="selected">
                  <a href="<?php echo $base_url .'/cart'; ?>"><i class="fa fa-shopping-cart fa-lg"></i><span class="cartcount">
                      <?php 
                                   if(count($_POST) != 0){   
                                    session_start();
                                    $sesid = session_id(); 
                                    $cntSubGroups = MP\CartItemsQuery::create()
                                            ->where('CartItems.SessionId =?', $sesid)
                                            ->count();
                                
                                    echo $count = $cntSubGroups;
                                   }else{
                                    session_start();
                                    $sesid = session_id();                                
                                    $cntSubGroups = MP\CartItemsQuery::create()
                                           ->where('CartItems.SessionId =?', $sesid)
                                           ->count();
                                    echo $count = $cntSubGroups;
                                    
                                   }
                                     
                                   ?>
                    </span> &nbsp;<?php echo $lbl_cart; ?>
                  </a>
                </li>


                <?php 
						if($logged_in){
						$loadeduser = user_load($user->uid);
						$username = $loadeduser->name;
				  ?>

                <li>
                  <a >
                    <span class="uname" style='color:#fff;'>
                      <?php  print $lbl_welcome.'  '.$username; ?>
                    </span>
                  </a>
                </li>
                <!-- <li><a href="<?php print $base_url;?>/user/<?php print $user->uid;?>/edit">Change password</a></li>-->
                <li>
                  <a href=""
                    <?php echo $base_path .'user/logout'; ?>><?php echo $lbl_logout; ?>
                  </a>
                </li>


                <?php }else{?>


                <?php if(!$logged_in){?>
                <li>
                  <a href=""
                    <?php echo $GLOBALS['base_url'];?>/user/register><i class="fa fa-pencil fa-lg"></i> &nbsp;<?php echo $lbl_registration; ?>
                  </a>
                </li>
                <li>
                  <a href=""
                    <?php echo $GLOBALS['base_url'];?>/user/"><i class="fa fa-key fa-lg"></i> &nbsp;<?php echo $lbl_login; ?>
                  </a>
                </li>
                <?php  }?>
              </ul>

              <?php } ?>


              <?php print render($page['header_middle']); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="header-bt">
      <div class="container">
        <div class="row col-md-12 col-sm-12">
           <div class="col-md-6 col-sm-6 head2-left">
            	<?php print render($page['frontpage_left']);?>
            </div>
            <div class="col-sm-6 col-md-6 head2-left pull-right">
  <!--<input type="text" placeholder="Search..." class="serch-panel">-->
  <form class="navbar-form" role="search">
    <div class="input-group col-md-11 col-sm-11">
     <?php 

if($lang_name=='en'){ ?>
      <input type="text" name="adserach" placeholder="Search..." class="form-control col-md-4 col-sm-4"><?php } else if($lang_name=='it'){?>
	  <input type="text" name="adserach" placeholder="Ricerca ..." class="form-control col-md-4 col-sm-4"><?php } else if($lang_name=='fr'){?>
	   <input type="text" name="adserach" placeholder="Recherche ..." class="form-control col-md-4 col-sm-4"><?php } ?>
      <div class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
      </div>
    </div>
    <span><img src="<?php print base_path() . path_to_theme(); ?>/images/meprint-litle.png" alt="logo"></span>
  </form>
</div>
            
        </div>
      </div>
    </section>
  </header>
  <!--End of the header--->
<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<!--<aside class="col-md-3 col-sm-3">
  <nav class="main-menu">
     <?php print render($page['left_sidebar']);?>
  </nav>
  <div class="space-2"></div>
  
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add1.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
</aside>-->
<div class="col-md-9 col-sm-9">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">

<?php if ($breadcrumb): echo '<a href="'.$base_url.'">'. $Home .'</a>'.' / '. $bshipping; endif;?>  
  
      <h3>&nbsp;Shipping Details <span><img src="/sites/all/themes/meprint/images/wave.png" alt="image"></span> </h3></div>
   <div class="space-3"></div>
   
   
            <?php 

         global $base_url;  
         global $user;
          session_start();
          $sessionid = session_id();
	  global $language ;
          $lang_name = $language->language ;
       
        
         
    
         $name = '';
         $email = '';
         if($user->uid !=0) { 
           
            
            $query = "SELECT * FROM {users} WHERE uid = :uid ";
            $project = db_query($query, array(':uid' => $user->uid))
                       ->fetchObject();
             
             $name = $project->name;
             $mail = $project->mail; 
             
             
            $phone = db_query("SELECT field_phone_value FROM  field_data_field_phone WHERE entity_id =  $user->uid ")
                     ->fetchField(); //getting partidular coloumn value
            //$address = db_query("SELECT field_address_value  FROM  field_data_field_address WHERE entity_id =  $user->uid ")
                  //   ->fetchField(); 
            $zipcode = db_query("SELECT  field_zip_code_value  FROM  field_data_field_zip_code WHERE entity_id =  $user->uid ")
                     ->fetchField(); 
            
            $province = db_query("SELECT  field_province_value  FROM  field_data_field_province WHERE entity_id =  $user->uid ")
                     ->fetchField(); 
            $location = db_query("SELECT  field_location_value  FROM  field_data_field_location WHERE entity_id =  $user->uid ")
                     ->fetchField(); 
            /*$country = db_query("SELECT  field_location_value  FROM  field_data_field_location WHERE entity_id =  $user->uid ")
                     ->fetchField();  */
             
        // echo  $_SESSION['session_id'];
         }
         
         ?>
   <form method="POST" id="shipping-form" action="<?php echo base_path().$lang_name;?>/orderconfirm" autocomplete="off">
        <div class="legend"> <span>Personal Details</span></div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-md-6">
            
            <lable class="shiplabel">Name :</lable> 
            <?php if($name !='') { ?>
            <input type="text" name="fname" id="fname" class="form-text shipform" value="<?php  echo $name; ?>" readonly="readonly"/>
            <?php } else { ?>
             <input type="text" name="fnames" id="fname" class="form-text shipform" value=""/>
           <?php
                
            } ?>
        </div>
        <div class="col-sm-6 col-md-6">
            <lable class="shiplabel">Email: </lable>
           <?php  if($mail !='') { ?>
            <input type="text" name="email" id="email" class="form-text shipform" value="<?php  echo  $mail;  ?>" readonly="readonly"/>
           <?php } else {  ?>
              <input type="text" name="emails" id="email" class="form-text shipform"/>
           <?php } ?>
        </div>
        <div class="col-sm-6 col-md-6"><lable class="shiplabel">Phone:  </lable><input type="text" name="phone" id="phone" class="form-text shipform" value="<?php if($phone !='') { echo  $phone; } ?>"/></div>
        <div class="clearfix"></div>
        
        <div class="legend "> <span>Contact Details</span></div>
        <div class="clearfix"></div>
        <div class="col-sm-6 col-md-6"><lable class="shiplabel">Address:</lable>  <textarea name="address" id="address" class="form-text shipform"><?php if($address !='') { echo $address; } ?></textarea></div>
            <div class="col-sm-6 col-md-6"><lable class="shiplabel">Zipcode: </lable> <input type="text" name="zipcode" id="zipcode" class="form-text shipform" value="<?php if($zipcode !='') { echo $zipcode; } ?>"/></div>
       
                <div class="col-sm-6 col-md-6"><lable class="shiplabel">Location: </lable> <input type="text" name="location" id="location" class="form-text shipform" value="<?php if($location !='') { echo $location; } ?>" /></div>
                   
                <div class="col-sm-6 col-md-6"><lable class="shiplabel">Province:</lable>  <input type="text" name="provience" id="provience" class="form-text shipform" value="<?php if($province !='') { echo $province; } ?>"/></div>
     <div class="col-sm-6 col-md-6"><lable class="shiplabel">Country:</lable>  <input type="text" name="country" id="country" class="form-text shipform" value=""/></div> 
                <div class="clearfix"></div>
                    <div class="legend ">  <span >Shipping details</span></div> 
                     <div class="clearfix"></div>
                      <div class="col-sm-6 col-md-6"><lable class="shiplabel">Address:</lable>  <textarea name="saddress" id="saddress" class="form-text shipform"><?php if($address !='') { echo $address; } ?></textarea></div>
            <div class="col-sm-6 col-md-6"><lable class="shiplabel">Zipcode: </lable> <input type="text" name="szipcode" id="szipcode" class="form-text shipform" value="<?php if($zipcode !='') { echo $zipcode; } ?>"/></div>
       
                <div class="col-sm-6 col-md-6"><lable class="shiplabel">Location: </lable> <input type="text" name="slocation" id="slocation" class="form-text shipform" value="<?php if($location !='') { echo $location; } ?>" /></div>
                    <div class="col-sm-6 col-md-6"><lable class="shiplabel">Province:</lable>  <input type="text" name="sprovience" id="sprovience"  class="form-text shipform" value="<?php if($province !='') { echo $province; } ?>"/></div>
                    
                       <div class="col-sm-6 col-md-6"><lable class="shiplabel">Country:</lable>  <input type="text" name="scountry" id="scountry" class="form-text shipform" value=""/></div> 
                       
                       <div class="clearfix"></div>
                         <div class="legend "> <span>Payment Options</span></div>
                         <div class="col-sm-12 col-md-12"><div class="col-sm-2 col-md-2">
                                 <lable ><b>Payment Type:</b></lable></div>
                                     <div class="col-sm-5 col-md-5"><select name="pay_options" class="form-select" id="pay_options">
                                          <option value="">select payment type</option>
                                         <option value="wire">Bank wire transfer</option>
                                          <option value="cash">Cash on Delivery</option>
                                           <option value="paypal">PayPal</option>
                                         </select></div> </div>
                         <div class="space-2"></div>
                         <div class="clearfix"></div>
                         
                       <?php 
                       $countS = MP\CartItemsQuery::create()
                                            ->where('CartItems.SessionId =?', $sessionid)
                                            ->count();
                       $query = MP\CartItemsQuery::create()
                                 ->where('CartItems.SessionId =?', $sessionid);
          $i=1;
            foreach($query as $paypalvaribles){
               $countnum = $i++;
                //echo "<pre>";
                //print_r($paypalvaribles);
                $quanitity = $paypalvaribles->getQuanitity();
                $price = $paypalvaribles->getPrice();
                 $ship = $paypalvaribles->getShipping();
                 $payquery = MP\FormoneLangQuery::create()->filterByFormId($paypalvaribles->getProductId())->filterByLanguageId($lang_name)->find();
                 foreach($payquery as $payresult){ 
                   $name  = $payresult->getName();
                 }?>
                        <div id = "item_<?php echo $countnum; ?>" class = "itemwrap">
        <input name = "item_name_<?php echo $countnum; ?>" value = "<?php echo $name; ?>" type = "hidden">        
        <input name = "amount_<?php echo $countnum; ?>" value = "<?php echo $price; ?>" type = "hidden">       
    </div>
                
           <?php }?>
        <input type="hidden" name="cartcount" value="<?php echo $countS; ?>"/>               
       <input type="hidden" name="productprice" value="<?php echo $_POST['productprice']; ?>"/>
       <input type="hidden" name="dicountamount" value="<?php echo $_POST['dicountamount']; ?>"/>
       <input type="hidden" name="coupon_code" value="<?php echo $_POST['coupancode']; ?>"/>
       <input type="hidden" name="discount_id" value="<?php echo $_POST['discount_id']; ?>"/>
         <input type="hidden" name="shipping" value="<?php echo $_POST['shipping']; ?>"/>
        
       <input class="form-submit" type="submit" name="Shipping_Confirm" id="paybutton" value="Proceed">
       <!--<button type="submit" name="submit" class="form-submit" value="Submit"></button>-->
       
        
       
   </form>
             
             <?php
	//print orderconfirm();
	  ?>
  </header>
    <div class="space-2"></div> 
  </div>
   
  <aside class="col-md-3 col-sm-3">
    
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add2.png" width="100%;" alt="Add-Image"></div>
    </div>
    <div class="space-2"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/add3.png" width="100%;" alt="Add-Image"></div>
    </div>
  </aside>
  </article>
  </section>
    <section class="blue-bg">
    <div class="container">
      <?php print render($page['first_preface']);?>
    </div>
  </section>
 
  <section class="lit-gra-bg">
    <div class="container"> 
	<?php global $base_url;?>
	<header class="row">
		<div class="col-sm-2 col-md-2"></div>
		<div class="col-sm-8 col-md-8">
		  <h3 class="text-center"> <span><img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png"></span>&nbsp;THE WORLD &nbsp;<span><strong>MEPRINT</strong><img alt="image" src="<?php echo $base_url;?>/sites/all/themes/meprint/images/wave.png"></span> </h3>
		</div>
		<div class="col-sm-2 col-md-2"></div>
	</header>
    <article class="row">
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_left']);?>
	</div>
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_center']);?>
	</div>
	<div class="col-sm-4 col-md-4">
    	<?php print render($page['second_preface_right']);?>
	</div>	 
    </article>
	</div>
  </section>
    <footer class="footer-bg">
    <div class="container">
      <?php print render($page['footer']);?>
    </div>
  </footer>
</div>
</body>
