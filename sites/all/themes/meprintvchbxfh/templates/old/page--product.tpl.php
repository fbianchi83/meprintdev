<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php 
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/language_theme.inc';
include_once DRUPAL_ROOT . base_path() . path_to_theme() . '/meprint.inc';
global $base_url;
global $base_path;
global $language;
$lang_name = $language->language;
drupal_add_js(base_path() . path_to_theme() . '/page-product.js');
drupal_add_js(base_path() . path_to_theme() . '/jquery-scrolltofixed-min.js');

?>

<?php
  include('header.tpl.php');
?>

<!--Start he Banner--->
<section class="container">
<div class="space-3"></div>
<!--End of the search--->
<article class="row">
<aside class="col-md-3 col-sm-3">
  <nav class="main-menu">
     <?php print render($page['left_sidebar']);?>
  </nav>
  <div class="space-2"></div>
  
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12"><img src="<?php print base_path() . path_to_theme(); ?>/images/left-add1.png" width="100%;" alt="Add-Image"></div>
  </div>
  <div class="space-2"></div>
</aside>
<div class="col-md-7 col-sm-7">
  <header class="row">
  <div class="col-sm-12 col-md-12 ">
 
       <?php 
     global $base_url;
     $purl = explode("?",$_SERVER["REQUEST_URI"]);
	      $purl1 = explode("&type=",$purl['1']);
		$id = $purl1[0];
		$type = $purl1[1];
      
    if($type == "Big"){
       print productthreecolum($id,$type);
       
       ?>
        
    <?php }else if($type == "small"){
      print productthreecolumSmall($id,$type);  
    }
    ?>
     
   
    </header>
  </div>
  <aside class="col-md-2 col-sm-2">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="panel panel-default summary-cart">
          <div class="panel-heading"><?php echo $cartpagetittle; ?></div>
          <div class="">
            
              <?php session_start();
                  $sessionid = session_id();
                  global $language ;
                  $lang_name = $language->language ;
                   $cntSubGroups = MP\CartItemsQuery::create()
                                   ->where('CartItems.SessionId =?', $sessionid)
                                   ->count();
                  $scart = MP\CartItemsQuery::create()
                                   ->where('CartItems.SessionId =?', $sessionid)->limit(3)->find();
                  
                  if($cntSubGroups  >0){
                  ?>
                <table class="table table-condensed">
                  <tr>
                    <th class="grid_list"><?php echo $pname; ?></th>
                    <th class="grid_list"><?php echo $price; ?></th>
          <!--                    <th class="col-md-2 col-sm-2 grid_list">--><?php //echo $qty; ?><!--</th>-->
                  </tr>
              <?php
                   $i =1;
                  foreach($scart as $result){  
                      if($result->getProductType() == "Big") {
                  $pquery = MP\FormoneLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                    foreach($pquery as $presult){   
                        $m = substr($presult->getName(), 0, 15); 
                    }  
                      } else { 
                          $pquery = MP\FormtwoLangQuery::create()->filterByFormId($result->getProductId())->filterByLanguageId($lang_name)->find();
                    foreach($pquery as $presult){   
                        $m = substr($presult->getName(), 0, 15); 
                    }
                          
                      }
                    
                    ?>
                <tr>
                    
                  <td class="">
                    <strong><?php echo $m; ?></strong><br />
                    <strong>Qnt.</strong> <?php echo $result->getQuanitity(); ?>
                  </td>
                  <td class="">
                    <strong><?php echo '&euro;'.number_format((float)$result->getPrice(), 2, '.', '');
                    ?></strong>
                  </td>
                </tr>
                <?php $i++; } ?>
                  <tr>
                    <td class="text-center" colspan="2">
                      <p class="summary-cart__total">Totale 245.000 €</p>
                      <button class="btn btn-success">Paga Adesso</button>
                    </td>

                  </tr>
                </table>
                  <?php }else{
                    echo '<h5 style="padding:5px;">'.$noproductsavail.'</h5>';
                  } ?>
              </div>

              
            <div class="space-2"></div>
            <!--            <p class="text-center">
              <button type="button" class="btn btn-success">Success</button>
            </p>-->
        </div>
      </div>
    </div>
    <div class="space-2"></div>

    <div class="panel panel-default panel--mini">
      <h3 class="panel-heading"><?php echo $samecategory; ?></h3>
      <div class="panel-body">
        <div class="carousel-related">
          <?php
          
          $this_prod = MP\FormoneBigFormatQuery::create()->filterByFormId($id)->findOne();
          
          $listing_query = MP\FormoneBigFormatQuery::create()
                          ->filterBySubcategoryId($this_prod->getSubcategoryId())
                          ->filterByStatus(1)
                          ->find();
          if(count($listing_query ) >0 ){
            foreach($listing_query as $listing){
              $id_same = $listing->getFormId();
              if($id_same == $id){
                continue;
              }
              $check_lang = MP\FormoneLangQuery::create()
                                ->where('FormoneLang.LanguageId =?', $lang_name)
                                ->where('FormoneLang.FormId =?', $id_same)
                                ->findOne();
                $name = substr($check_lang->getName(), 0, 25);
                
                $add_info = MP\FormoneAdditionalInfoQuery::create()->filterByFormId($id_same)->filterByWebsiteId($lang_name)->findOne();
                
                $img = file_load($add_info->getImage());
                $img_path_small = image_style_url("same-category-small", $img->uri);
                $img_path = image_style_url("same-category", $img->uri);
                ?>
                  <div class="media media--mini-listing">
                    <div class="media-left">
                      <a href="<?php echo $base_url . '/' . $lang_name . '/product-three-column?' . $id_same . '&type=Big'; ?>">
                        <img class="media-object" src="<?php echo $img_path_small; ?>" data-toggle="tooltip" data-html="true" data-placement="top" data-container="body" title='<div class="dimdiv__hover-content"><img src="<?php echo $img_path; ?>" alt="<?php echo $name; ?>" /><p></p></div>'>
                      </a>
                    </div>
                    <div class="media-body">
                      <h4 class="media-mini-listing__title"><?php echo $name; ?></h4>
                    </div>
                  </div>
                <?php
            }
          }
          ?>
        </div>
      </div>
    </div>

    <div class="space-2"></div>
    <div class="space-2"></div>
    
    <div id="summary-cart--file" class="panel panel-default panel-file summary-cart">
      <div class="panel-heading">Invio file</div>
      <div class="">
        <div>
          <i class="fa fa-download fa-1x"></i>
          <span>scarica il template</span>
        </div>
        <div>
          <i class="fa fa-book fa-1x"></i>
          <span>scarica le istruzioni</span>
        </div>
      </div>
    </div>
    
    <div class="space-2"></div>
    <div class="space-2"></div>
        
<!--    carrello che segue-->
    <div id="summary-cart--product" class="panel panel-default summary-cart summary-cart--product">
        <div class="panel-heading">Riepilogo</div>
        <div class="">
<!--          <table class="table table-condensed">
            <tr>
              <th class="grid_list"><?php //echo $pname; ?></th>
              <th class="grid_list"><?php //echo $price; ?></th>
            </tr>

            <tr>
              <td class="">
                <strong><?php //echo $costpfprintingandmaterial; ?></strong><br />
                <strong>Qnt.</strong> 1
              </td>
              <td class="">
                € 36
              </td>
            </tr>
            <tr>
              <td class="">
                <strong><?php //echo $processcost; ?></strong><br />
                <strong>Qnt.</strong> 1
              </td>
              <td class="">
                € 15
              </td>
            </tr>
            <tr>
              <td class="text-center" colspan="2">
                <p class="summary-cart__total">
                  <?php //echo $totalproductcost; ?> €
                  <span id="totalproductcostdiv_sum"></span>
                </p>
              </td>

            </tr>
          </table>-->
            <table id="sample-table-2"  class="table table-condensed">
              <tbody>
                <tr>
                  <td class="left"><strong><?php echo $costpfprintingandmaterial; ?></strong></td>
                  <td>
                    <div class="fa fa-eur printigmaterialdiv"></div>
                  </td>
                </tr>
                <tr>
                  <td class="left"><strong><?php echo $processcost; ?></strong></td>
                  <td>
                    <div class="fa fa-eur adtnlcostdiv"></div>
                  </td>
                </tr>
                
                <tr id="baseaccessoriestd" class="baseaccessoriestd">
                  <td class="left"><strong><?php echo $accessorie_base; ?></strong></td>
                  <td class="hidden-480">
                      <div class="fa fa-eur baseacccstdiv" id="baseacccstdiv"></div>
                  </td>
                </tr>
                <tr id="extraaccessoriestd" class="extraaccessoriestd">
                    <td class="left"><strong><?php echo $accessorie; ?></strong></td>
                    <td class="hidden-480">
                        <div class="fa fa-eur extraacccstdiv" id="extraacccstdiv"></div>
                    </td>
                </tr>
<!--                <tr>
                  <td class="left"><strong><?php //echo $singleproductcost; ?></strong></td>
                  <td>
                    <div class="fa fa-eur singleproductcostdiv"></div>
                  </td>
                </tr>-->
                <tr class="extraprocessingtd">
                  <td class="left"><strong><?php echo $extraprocessingcost; ?></strong></td>
                  <td>
                    <div class="fa fa-eur extraprocessingdiv"></div>
                  </td>
                </tr>
                <tr>
                  <td class="left"><strong><?php echo $quantity; ?></strong></td>
                  <td>
                    <div class="totalnoofproductbigfrmtdiv"></div>
                  </td>
                </tr>
                <tr class="promopricetd">
                  <td class="left"><strong><?php echo $beforpromoprice; ?></strong></td>
                  <td>
                    <div style='text-decoration:line-through' class="fa fa-eur promopricediv"></div>
                  </td>
                </tr>
                <tr id="promovaltd" class="promovaltd">
                  <td class="left"><strong><?php echo $promoval; ?></strong></td>
                  <td class="hidden-480">
                      <div class="fa fa-eur promovaldiv"></div>
                  </td>
                </tr>
                <tr id="operatorreviewtd" class="operatorreviewtd">
                  <td class="left"><strong><?php echo $operatorreview_cost; ?></strong></td>
                  <td class="hidden-480">
                      <div class="fa fa-eur operatorreviewdiv"></div>
                  </td>
                </tr>
                <tr>
                  <td class="left"><strong><?php echo $totalproductcost; ?></strong></td>
                  <td class="hidden-480">
                    <div class="fa fa-eur totalproductcostdiv"></div>
                  </td>
                </tr>
              </tbody>
          </table>
        </div>


        <div class="space-2"></div>
      </div>
<!--fine carrello che segue-->
  </aside>
  </article>
  </section>

<?php
  include('footer.tpl.php');
?>