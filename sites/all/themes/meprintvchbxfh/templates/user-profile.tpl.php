<?php

/**
 * @file
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * Use render($user_profile) to print all profile items, or print a subset
 * such as render($user_profile['user_picture']). Always call
 * render($user_profile) at the end in order to print all remaining items. If
 * the item is a category, it will contain all its profile items. By default,
 * $user_profile['summary'] is provided, which contains data on the user's
 * history. Other data can be included by modules. $user_profile['user_picture']
 * is available for showing the account picture.
 *
 * Available variables:
 *   - $user_profile: An array of profile items. Use render() to print them.
 *   - Field variables: for each field instance attached to the user a
 *     corresponding variable is defined; e.g., $account->field_example has a
 *     variable $field_example defined. When needing to access a field's raw
 *     values, developers/themers are strongly encouraged to use these
 *     variables. Otherwise they will have to explicitly specify the desired
 *     field language, e.g. $account->field_example['en'], thus overriding any
 *     language negotiation rule that was previously applied.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 *
 * @ingroup themeable
 */
global $user;
global $language;

$propel_user = MP\UserQuery::create()->filterByUid($user->uid)->findOne();

if($propel_user){
  
  $user_type = $propel_user->getUserType();
  if($user_type == "private" || $user_type == "individual"){
    $user_name = $propel_user->getName()." ".$propel_user->getSurname();
  }  else {
    $user_name = $propel_user->getCompanyName();
  }
  
  drupal_set_title($user_name);
}

?>
<div class="profile"<?php print $attributes; ?>>
  <ul class="menu-profile row">
    <li class="col-sm-4 menu-profile__item">
      <a href="/<?php print $language->language ?>/user/my-profile"><span class="fa fa-user fa-5x menu-profile__icon"></span> <?php echo t('My profile'); ?></a>
    </li>
    <li class="col-sm-4 menu-profile__item">
      <a href="/<?php print $language->language ?>/user/change-password"><span class="fa fa-key fa-5x menu-profile__icon"></span> <?php echo t('Change password'); ?></a>
    </li>
    <li class="col-sm-4 menu-profile__item">
      <a href="/<?php print $language->language ?>/preferiti"><span class="fa fa-heart fa-5x menu-profile__icon"></span> <?php echo t('My prefers'); ?></a>
    </li>
    <li class="col-sm-4 menu-profile__item">
      <?php if( $user->uid == 1 ) : ?>
      <a href="/<?php print $language->language ?>/orders/view"><span class="fa fa-pencil-square-o fa-5x menu-profile__icon"></span> <?php echo t('My orders'); ?></a>
      <?php else : ?>
      <a href="/<?php print $language->language ?>/my-orders/view"><span class="fa fa-pencil-square-o fa-5x menu-profile__icon"></span> <?php echo t('My orders'); ?></a>
      <?php endif; ?>
    </li>
    <li class="col-sm-4 menu-profile__item">
      <a href="/<?php print $language->language ?>/carrello"><span class="fa fa-shopping-cart fa-5x menu-profile__icon"></span> <?php echo t('My cart'); ?></a>
    </li>
    <li class="col-sm-4 menu-profile__item">
      <a href="/<?php print $language->language ?>/user/rubrica"><span class="fa fa-users fa-5x menu-profile__icon"></span> <?php echo t('My address book'); ?></a>
    </li>
  </ul>
  <!--<div class="user-profile boxed clearfix"><?php #print render($user_profile);?></div>-->
  
</div>