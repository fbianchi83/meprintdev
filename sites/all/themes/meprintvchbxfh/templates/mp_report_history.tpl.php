<?php
  global $base_url;
  global $base_path;
  global $language;
  $lang_name = $language->language;
?>

<script type="text/javascript" src="<?php echo base_path(); ?>sites/all/modules/mp_report/scripts/report.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
   $( "#dailystart" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
          //console.log(selectedDate);
        $( "#dailyend" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#dailyend" ).datepicker({
      showAnim: "slideDown",
      dateFormat: "dd/mm/yy",
      minDate: new Date(2015, 8 - 1, 1),
      onClose: function( selectedDate ) {
        $( "#dailystart" ).datepicker( "option", "maxDate", selectedDate );
      }
    }); 
    
    var currentDate = new Date();  
    currentDate.setDate(currentDate.getDate()-15);
    var prevDate = new Date();
    $("#dailystart").datepicker("setDate",currentDate);
    $("#dailyend").datepicker("setDate",prevDate);
    
    filtralast();
});

function filtralast() {
    var datestart= formatdate(jQuery('#dailystart').val());
    var dateend= formatdate(jQuery('#dailyend').val());
    var clientid= jQuery('#clientsel').val();
    var materialid= jQuery('#materialsel').val();
    var paytype= jQuery('#paymethsel').val();
    var shipping= jQuery('#shippingsel').val();
    
    var morefilters = "";
    if (jQuery('#filters2').css('display') === 'block')
        morefilters= '&materials='+materialid+'&paytype='+paytype+'&shipping='+shipping;
    else
        morefilters= '&materials=0&paytype=nopay&shipping=0';
    jQuery.ajax({
        type:'POST',
        url:"/filterhistory",
        data:'datastart='+datestart+'&dataend='+dateend+'&client='+clientid+morefilters,
        dataType:'json',
        success: function(result) {  
            var temp = '';
            var temp2 = '';
            if (result) {
                var i=0;
                var j=0;
                var totals=0;
                while (j<result.length) {
                    totals += parseFloat(result[j][3]) + (parseFloat(result[j][3]*result[j][7]/100));
                    j++;
                }  
                while (i<result.length) {
                    var total= parseFloat(result[i][3]) + (parseFloat(result[i][3]*result[i][7]/100)); 
                    temp += "<tr>";
                    temp += "<td>"+result[i][0]+"</td>";
                    temp += "<td><strong>"+result[i][1]+"</strong></td>";
                    temp += "<td><strong>"+result[i][2]+"</strong></td>";
                    temp += "<td>"+total.toFixed(2)+" &euro;</td>";
                    temp += "<td>"+result[i][4]+"</td>";
                    temp += "<td class='text-uppercase'><strong>"+result[i][5]+"</strong></td>";
                    temp += "<td class='text-right order-detail'><a href='/<?php echo $language->language; ?>/orders/orders_edit/"+result[i][0]+"'><?php echo $showoff; ?></a></td>";
                    temp += "</tr>";
                    i++;
                }
            }
            else {
                temp = "<tr><td colspan='7'> <?php echo $noorders; ?></td></tr>";
            }
            temp = "<thead><tr><th><?php echo t('N°'); ?></th><th><?php echo t('Order Date'); ?></th><th><?php echo t('Client'); ?></th><th><?php echo t('Amount'); ?></th><th><?php echo t('Products'); ?></th><th><?php echo $ptype; ?></th><th class='text-right'><?php echo t('Display'); ?></th></tr></thead>" + temp;
            temp2 = "<th width='35%' class='text-uppercase text-center'><?php echo t('Order Numbers'); ?></th><td width='15%' class='text-center'>"+j+"</td><th class='text-uppercase text-center' width='35%'><?php echo t('Total Amount'); ?></th><td class='text-center' width='15%'>"+totals.toFixed(2)+" &euro;</td>"
            jQuery('#mp-list-order').empty().append(temp);
            jQuery('#mp-list-order2').empty().append(temp2);
        }
    });
}

function showfilters() {
    if (jQuery('#filters2').css('display') === 'block') 
        jQuery('#filters2').fadeOut("fast");
    else 
        jQuery('#filters2').fadeIn("fast");
}

</script>

<div class="col-sm-12 col-md-12 ">
    <h3> <?php echo $historystats; ?> </h3>
    
    <div id='dailychoice' class="billcentered"> 
        <h4> <?php echo $historyfilter; ?> </h4>
        <input type='button' id="shows" value='<?php echo t('Filters'); ?>' onclick='showfilters(); return false;'><br>
        <div id="filters2" class="billcentered margin10" style="display:none">
            <?php echo t('Materials'); ?> 
            <select id='materialsel' class="margin10" >
                <option value='0'> <?php echo t('All Materials'); ?> </option>
            <?php 
            $materials = MP\MaterialsQuery::create()->find();
            foreach ($materials as $material) {
                $matname = MP\MaterialsLangQuery::create()->filterByMaterialId($material->getMaterialId())->filterByLanguageId($lang_name)->select("name")->findOne();
                ?>
                <option value="<?php echo $material->getMaterialId(); ?>"> <?php echo $matname; ?> </option>
                    <?php
            }
            ?>
            </select>
            <br><?php echo t('Payment Method'); ?> 
            <select id='paymethsel' class="margin10" >
                <option value='nopay'> <?php echo t('All Payment Methods'); ?> </option>
                <option value='paypal'> <?php echo t('Paypal/Credit Card'); ?> </option>
                <option value='banktransfer'> <?php echo t('Bank Transfer'); ?> </option>
                <option value='contrassegno'> <?php echo t('Cash on Delivery'); ?> </option>
                <option value='sofort'> <?php echo t('Sofort'); ?> </option>
            </select>
            <?php echo t('Shipping Companies'); ?> 
            <select id='shippingsel' class="margin10" >
                <option value='0'> <?php echo t('All Shipping Companies'); ?> </option>
            <?php 
            $shippings = MP\ShippingCompaniesQuery::create()->filterByStatus(1)->find();
            foreach ($shippings as $shipping) {
               ?>
                <option value="<?php echo $shipping->getShippingId(); ?>"> <?php echo $shipping->getName(); ?> </option>
                    <?php
            }
            ?>
            </select>
        </div>
        <?php //echo t('Client'); ?> 
<!--        <select id='clientsel' class="margin10" >
            <option value='0'> <?php //echo t('All Clients'); ?> </option>
            <?php 
            /*$users = MP\UserQuery::create()->find();
            foreach ($users as $user) {
                if ($user->getUid() != 1) {
                    if ($user->getUserType=="association" || $user->getUserType=="public")
                        $name= $user->getCompanyName();
                    else
                        $name= $user->getName() . " " . $user->getSurname();*/
                    ?>
            <option value="<?php //echo $user->getUid(); ?>"> <?php //echo $name; ?> </option>
                    <?php
                //}
            //}
            ?>
        </select>-->
        <?php echo $datestart; ?> <input type='text' class="margin10" id='dailystart'>
        <?php echo $dateend; ?> <input type='text' class="margin10" id='dailyend'>
        <input type='button' id="filter1" value='<?php echo $filterdate; ?>' onclick='filtralast(); return false;'>
    </div>
    
    </div>
    <table id="mp-list-order2" class="table table-bordered table-striped table-hover"></table>
    <table id="mp-list-order" class="table table-bordered table-striped table-hover"></table>
</div>