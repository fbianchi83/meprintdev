
<link href="<?php print base_path() . path_to_theme(); ?>/starreviews/assets/css/starReviews.css" rel="stylesheet" type="text/css"/>
<!-- jQuery Form Validator -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.34/jquery.form-validator.min.js"></script>
<!-- jQuery Barrating plugin -->
<script src="<?php print base_path() . path_to_theme(); ?>/starreviews/assets/js/jquery.barrating.js"></script>
<!-- jQuery starReviews -->
<script src="<?php print base_path() . path_to_theme(); ?>/starreviews/assets/js/starReviews.js"></script>
 
<script type="text/javascript">
    jQuery(document).ready(function($) {
        /* Activate our reviews */
        $().reviews('.starReviews');
    });
</script>


<!-- **** Start reviews **** -->
<div class="starReviews col-xs-12">
    <input type="hidden" id="basepath" value="<?php print base_path() . path_to_theme(); ?>" />
    <!-- Show average-rating -->
    <div class="average-rating"></div>
    <!-- Add new review -->
    <div class="add-review"></div>
    <!-- This is where your product ID goes -->
    <div id="review-productId" class="review-productId" style=""><?php echo $bigid; ?></div> 
    <hr>
    <!-- Show current reviews -->
    <div class="show-reviews"></div>
</div> 
