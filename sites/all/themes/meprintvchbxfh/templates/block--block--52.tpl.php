<div>
  <h4>MePrint<span class="lit-gra-span"> Web TV</span></h4>
  <div class="">
      <?php
        //$yt_playlist = get_field('canale_youtube');

        $yt_playlist = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=PLW5bKonykiAqDNpnV9c3qB10tz5Y6OrCe&part=snippet&maxResults=1&order=date&key=AIzaSyBsC7X9Z0Rjq5Q3JGOhA0uEt0ILcWfAOrU';
        
        //$yt_channel = 'https://www.googleapis.com/youtube/v3/channels?id=UCtd7ZkH3w1qqoXBCGPVk3Ig&part=snippet&maxResults=1&order=date&key=AIzaSyBsC7X9Z0Rjq5Q3JGOhA0uEt0ILcWfAOrU';
        
        //$yt_channel = 'https://www.googleapis.com/youtube/v3/channelSections?channelId=UCtd7ZkH3w1qqoXBCGPVk3Ig&part=snippet&key=AIzaSyBsC7X9Z0Rjq5Q3JGOhA0uEt0ILcWfAOrU';
        
        $context = stream_context_create(array(
         'http' => array(
            'ignore_errors'=>false,
            'method'=>'GET'
          )
        ));
        $json = file_get_contents($yt_playlist, false, $context);

        $data = json_decode($json, TRUE);
        
        foreach($data['items'] as $vid){
            #print_r($vid);

            $id           = $vid['snippet']['resourceId']['videoId'];
            $title        = $vid['snippet']['title'];
            $description  = $vid['snippet']['description'];
            $thumb        = $vid['snippet']['thumbnails']['high']['url'];

            $videourl     = 'http://www.youtube.com/watch?v='.$id;

            ?>

            <iframe width="340" height="250" src="https://www.youtube.com/embed/<?php echo $id; ?>" frameborder="0" allowfullscreen></iframe>

        <?php } ?>
  </div>       
</div>