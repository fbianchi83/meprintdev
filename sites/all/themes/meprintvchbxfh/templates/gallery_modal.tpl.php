<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="modal modal-gallery fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="<?php echo t('close');?>"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo t("our themes"); ?></h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-2">
                        <ul class="nav nav-pills" role="tablist">
                            <?php
                            $i = 0;
                            foreach ($galleries as $gallery) {
                                $name = $gallery_lang[$gallery]->getName();
                                ?>
                                <li role="presentation" <?php
                            if ($i == 0) {
                                echo "class=\"active\"";
                            }
                                ?>>
                                    <a href="#gal-<?php echo $gallery; ?>" aria-controls="gal-<?php echo $gallery; ?>" role="tab" data-toggle="pill"><?php echo $name; ?></a>
                                </li>
                                <?php
                                $i++;
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        <div class="tab-content">
                            <?php
                            $i = 0;
                            foreach ($galleries as $gallery) {
                                $gal_imgs = $gallery_imgs[$gallery];
                                ?>

                                <div role="tabpanel" class="tab-pane <?php
                                     if ($i == 0) {
                                         echo "active";
                                     }
                                     ?>" id="gal-<?php echo $gallery; ?>">
                                    <div class="row">
                                        <?php
                                        foreach ($gal_imgs as $img) {
                                            $image_gal = file_load($img)->uri;
                                            ?>
                                            <div class="form-group col-md-4 col-sm-4 text-center">
                                                <label class="dimdiv dimdiv--theme">
                                                    <input type="radio" name="gal-<?php echo $gallery; ?>-img" class="theme-input" id="gal-<?php echo $gallery; ?>-img-<?php echo $img; ?>" value="<?php echo $img; ?>" >
                                                    <img id="theme_img-<?php echo $img; ?>" src="<?php echo image_style_url("gallery-thumb", $image_gal); ?>" class="img-responsive">
                                                    <button id="zoom-button-<?php echo $img; ?>" class="zoom-button" data-src="<?php echo file_create_url($image_gal); ?>"><i class="fa fa-search"></i></button>
                                                </label>
                                            </div>
        <?php
    }
    ?>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"><?php echo t('close');?></button>
                <button id="modal-save" type="button" class="btn btn-orange"><?php echo t('choose');?></button>
            </div>
        </div>
    </div>
</div>