jQuery(document).ready(function($) {

  $('#form-registration__type').on('change', function() {
    var value = $(this).val();
    $('.form-registration__optional').hide();
    $('.form-registration__optional input, .form-registration__optional select').prop('required',false);
    $('#' + value).show();
    $('#' + value + ' input, #'+ value + ' select').prop('required',true);
    $('#form_registration__vat_club').prop('required', false);
    $('.namerif').show();
    $('.surnamerif').show();
    if (value == "private") {
        $('.namerif').hide();
        $('.surnamerif').hide();
    }
    
  });
  
  
  $.validator.addMethod("cfcontrol", function (val, ele, arg) {
    //console.log( val );
    var regex = /[A-Za-z]{6}[\d]{2}[A-Za-z][\d]{2}[A-Za-z][\d]{3}[A-Za-z]/;
    var v = val.match(regex);
    //console.log( 'codice fiscale valido: ' + v );
    if ( v ) { return true; }
    else { return false; }
  } , "Inserire Codice fiscale valido.");
    
  $.validator.addClassRules("cfcontrol", { cfcontrol: true });
  
   /*$.validator.addMethod('phones', function(val, el) { 
       console.log(el);
       var $module = $(el).parents('div.input'); 
       console.log($module.find('.phones:filled').length);
       return $module.find('.phones:filled').length; 
   });
   
   $.validator.addClassRules('phones', { phones : true });
   $.validator.messages.phones = 'Please fill out at least one of these fields.';
   */
  $('#form-registration').validate({
    rules: {      
      mobile_phone: {
        required: function(element){
            return $("#form-registration__phone_rif").val()=="";
        }
      },
      phone: {
        required: function(element){
            return $("#form-registration__cell_rif").val()=="";
        }
      },
      email: { 
        required: true, 
        email: true,                    
        remote:{
          url: "/ctrlEmailFront"
        }
      },
      cf: { cfcontrol: true },
      /*cf: {
        remote:{
          url: "/ctrlCfFront"
        }
      },
      private_fiscal_code: {
        remote:{
          url: "/ctrlCfFront"
        }
      },*/
      scf: { cfcontrol: true },
      email_bis: { equalTo: "#form-registration__email" },
      password_bis: { equalTo: "#form-registration__pw" }
    },
    messages: {
      email: { remote: "Email già presente nel nostro sistema" },
      email_bis: { equalTo: "Il valore non corrisponde al campo Email" },
      password_bis: { equalTo: "Il valore non corrisponde al campo Password" }
    },
    errorPlacement: function (error, element) {      
      if ( element.attr("name") == "privacy"){
        error.appendTo(element.parents('.checkbox'));
      } else {
        error.appendTo(element.parents('.form-group'));
      }
    }
  });

});
