jQuery(document).ready(function($) {
  
  if( $(window).width() > 1024 ){
    $("[data-toggle='tooltip']").tooltip();
  } else {
    $("[data-toggle='tooltip']").tooltip('disable');
  }
  
  $("#shipping-form").validate({
    rules: { 
      /*fnames:{
                    required: true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/namevalidation.php", async: false }
               },
               emails:{
                    required: true,
                    email :true,
                    remote: { type: "post", url: "<?php echo $base_url; ?>/sites/all/themes/meprint/emailvalidation.php", async: false }
                    
               },*/
      phone:{
        required: true,
        number :true
      },
      address:{
        required: true
      },
      zipcode:{
        required: true 
      },
      location:{
        required: true
      },
      province :{
        required: true
      },
      email :{
        required: true
      },
      country : {
        required: true
      },
      saddress : {
        required: true
      },
      szipcode:{
        required: true
      },
      slocation:{
        required: true
      },
      sphone: {
        required: true,
        number :true
      },
      sprovince :{
        required: true
      },
      semail :{
        required: true
      },
      scountry : {
        required: true
      },
      pay_options:{ 
        required: true
      }
             
    },
    messages: {
      /*fnames:{
                    required: "Please Enter Name",
                    remote: "Sorry Username Already Existed",
               },
               emails:{
                    required: "Please Enter Email",
                    email :"Please Enter valid Email",
                    remote :"Sorry Email Already Existed"
               },*/
      phone:{
        required: Drupal.t("Please Enter Phone"),
        number : Drupal.t("Please Enter Numbers only")
      },
      address:{
        required: Drupal.t("Please Enter Address")
      },
      zipcode:{
        required: Drupal.t("Please Enter Zipcode")
      },
      location:{
        required: Drupal.t("Please Enter Location")
      },
      province :{
        required: Drupal.t("Please Enter Province")
      },
      country : {
        required: Drupal.t("Please Enter Country")
      },
      saddress : {
        required: Drupal.t("Please Enter Address")
      },
      szipcode:{
        required: Drupal.t("Please Enter Zipcode")
      },
      slocation:{
        required: Drupal.t("Please Enter Location")
      },
      sprovince :{
        required: Drupal.t("Please Enter Provience")
      },
      scountry : {
        required: Drupal.t("Please Enter Country")
      },
      pay_options:{ 
        required: Drupal.t("Please Select Payment Type")
      }
    }
  });
     
});


