jQuery(document).ready(function($) {
  /****** small Format Form ************/
  
  //validation checkbox
  
  $('.section-cover .dimdiv').on('click', function(){
    //console.log('val:' + $(this).children('.cover-input').val() );
    if( $(this).children('.cover-input').val() == 2 ){ //copertina specifica      
      $('.adviced-cover .extra-check').parent().each(function(key, value){
        $(this).find('input').addClass('scheck'+key);
        $(this).find('input').addClass('scheckrequired');

        var name = 'scheck'+key;

        $.validator.addClassRules(name,{
            require_from_group: [1, '.'+name]
        });
      });
    } else {
      $('.adviced-cover .extra-check').find('input').removeClass('scheckrequired');
      $('.adviced-cover').find('span.error').remove();
      $('.adviced-cover').find('.error').removeClass('error');
    }
  });
  
  $('input.fronteretro_class').on('click', function(){
    if($(this).is(':checked') == true){
      $('.adviced-fr .extra-check').parent().each(function(key, value){
        $(this).find('input').addClass('frcheck'+key);
        $(this).find('input').addClass('frcheckrequired');

        var name = 'frcheck'+key;

        $.validator.addClassRules(name,{
            require_from_group: [1, '.'+name]
        });
      });
    } else {
      $('.adviced-fr .extra-check').find('input').removeClass('frcheckrequired');
      $('.adviced-fr').find('span.error').remove();
      $('.adviced-fr').find('.error').removeClass('error');
    }
    
  });
  

  $.validator.addMethod("ctrlFace", function (value, element, param) {      
    if( value == "" || value % 4 != 0 ) return false;
    else return true;
  }, Drupal.t("Enter a number greater than 8 and a multiple of 4"));
  $.validator.addClassRules("faceQuantity", {ctrlFace: true});
  
  
  
  $("#product-small-format").validate({
    /*rules: {
      facciate: {
        required: true,
        number: true       
      }
    },*/
    /*messages: {  
      facciate: {
        number: "Inserire un numero"
      }
    },*/
    errorElement: 'span',
    errorPlacement: function (error, element) {
      if( element.hasClass('extra_side') || element.hasClass('extra_cover') ){
        error.appendTo(element.parents('.extinfodiv'));

      } else {
        error.appendTo(element.parents('.order-view-top'));
      }

    }
  });
  
  
  
  
});
