(function($) {
    Drupal.behaviors.myBehavior = {   
        attach: function (context, settings) {

            $('input[type=radio][name=sdimensions]').change(function() {
         var count = $('input[name=simagecount]').val();
          for(var i=1;i<=count;i++) {
                 $(".dvPreviewSmall_"+i).html('');
                 $("#image_"+i).val(null);
                 $(".ImageError_"+i).html("");
          }
           $('input[name^="jssmall"]').each(function() { 
              var k = $(this).val(); 
              $(".dbimagesmall_"+k).hide();
              $(".dvPreviewExistedSmall_"+k).html('');
              $("#upload_"+k).val(null);
           
          });
          
       }); //end
    $(".existedSmallN").change(function() {
        var id = $(this).attr('name');      
        var strnumber = id.split("upload_");
        var F = this.files;
        if(F && F[0]){					
            for(var i=0;i<F.length;i++){existedImageSmall(F[i],strnumber[1],id); }
        }
    });
        
function  existedImageSmall(file,divID,id) { 
    var productwidth = 0;
    var productheight = 0;
    var displaywidth = 100;
    var displayheight = 100;
    var dimensionFinder ;
    dimensionFinder = $('input[name=sdimensions]:checked').val();
    var hashedid = '#'+id;
    if((dimensionFinder != null)) {
            var dim_dets = "#bd"+dimensionFinder;
            var  dimension_details = $(dim_dets).val(); 
            var res = dimension_details.split("*");
            productwidth = res[0];
            productheight = res[1];
    }
    else if(dimensionFinder == ""){
            productheight = 0;   
            productwidth = 0;
    } 
    var reader = new FileReader();
    var image  = new Image();
    reader.readAsDataURL(file); 
    reader.onload = function(_file) {
    image.src    = _file.target.result;              // url.createObjectURL(file);
    image.onload = function() {
    var w = this.width,
    h = this.height,
    t = file.type,                           // ext only: // file.type.split('/')[1],
    n = file.name,
    s = ~~(file.size/1024) +'KB';
    /*********************** dimension calculation start************/ 
    var productarea = parseFloat(productheight) * parseFloat(productwidth);
    var productno = parseFloat(productarea)/ 10000;
    productno = Math.ceil(productno);
    if(productno == 0){
            productno = 1;
    }
    var minwdth = 2400 * parseFloat(productno);
    var maxwdth = 7500 * parseFloat(productno);
    if (w >= minwdth && w <= maxwdth) {
            $(".dbimagesmall_"+divID).show();
            $(".ImageError_"+divID).html("Please choose image with widht above 2400px");
            $(".dvPreviewExistedSmall_"+divID).html('');
            $("#upload_"+divID).val(null);
            return;
    }
    else {
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);	
            if(provideimgaspect == dbimgaspect) {
                    $(".dbimagesmall_"+divID).hide();
                    $(".ImageError_"+divID).html("");
                    $(".dvPreviewExistedSmall_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" >');
            }
            else { 
                    $(".dbimagesmall_"+divID).show();
                    $(".ImageError_"+divID).html("Image ratios are not same");
                    $("#upload_"+divID).val(null);
                    $(".dvPreviewExistedSmall_"+divID).html('');
            }
    }
    };
    image.onerror= function() {
            $(".ImageError_"+divID).html('Upload .jpg,.gif.png only');
            $("#upload_"+divID).val(null);
            return;
    };      
    };
}

        
}  
};    
})(jQuery);