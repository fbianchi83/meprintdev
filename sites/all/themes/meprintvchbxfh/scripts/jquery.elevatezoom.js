
if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function( $, window, document, undefined ) {
    var ElevateZoom = {
        init: function( options, elem ) {
                var self = this;

                

                self.elem = elem;
                self.$elem = $( elem );

                self.imageSrc = options.url;

                //self.refresh( 1 );
                
                self.fetch(self.imageSrc);
                

        },

        refresh: function( length ) {
                var self = this;

                setTimeout(function() {
                        self.fetch(self.imageSrc);

                }, length );
        },

        fetch: function(imgsrc) {
                //get the image
                var self = this;
                var newImg = new Image();
                newImg.onload = function() {
                        //set the large image dimensions - used to calculte ratio's
                        self.largeWidth = newImg.width;
                        self.largeHeight = newImg.height;
                        //once image is loaded start the calls

                        self.startZoom();
                }
                newImg.src = imgsrc; // this must be done AFTER setting onload

                return;

        },

        startZoom: function( ) {
                var self = this;
                
                var imgW = self.largeWidth;
                var imgH = self.largeHeight;
                
                var windowW = $(window).width(); // Max width for the image
                var windowH = $(window).height();    // Max height for the image
                
                var maxWidth = $(window).width() * 0.6; // Max width for the image
                var maxHeight = $(window).height() * 0.6;    // Max height for the image
                
                
                var ratio = 0;  // Used for aspect ratio
                var width = self.largeWidth;    // Current image width
                var height = self.largeHeight;  // Current image height

                
                // Check if the current width is larger than the max
                if(width > maxWidth){
                    ratio = maxWidth / width;   // get ratio for scaling image
                    imgW = maxWidth; // Set new width
                    imgH = height * ratio;  // Scale height based on ratio
                    height = height * ratio;    // Reset height to match scaled image
                    width = width * ratio;    // Reset width to match scaled image
                }

                // Check if current height is larger than max
                if(height > maxHeight){
                    ratio = maxHeight / height; // get ratio for scaling image
                    imgH = maxHeight;   // Set new height
                    imgW =  width * ratio;    // Scale width based on ratio
                    width = width * ratio;    // Reset width to match scaled image
                    height = height * ratio;    // Reset height to match scaled image
                }
                
                
                
                self.backimage = $("<div style='bottom: 0px;left: 0px;outline: 0 none;overflow: hidden;"+
                                    "position: fixed;right: 0px;top: 0px;"+
                                    "z-index: 999998;overflow-x: hidden;overflow-y: auto;background-color: #000;opacity: 0.5'>&nbsp;</div>")
                        .appendTo('body')
                        .click(function () {
                                $(this).remove();
                                self.zoomWindow.remove();
                        });
    
                self.zoomWindowStyle = "left:" + ((windowW / 2) - (imgW / 2)) + "px;"
                                + "top:" + ((windowH / 2) - (imgH / 2)) + "px;"
                                + "overflow: hidden;"
                                + "background-image: url("+ self.imageSrc +");"
                                + "background-position: 0px 0px;text-align:center;"  
                                + "background-color: #fff;"            
                                + "width: " + imgW + "px;"
                                + "height: " + imgH + "px;"
                                + "background-size: 100% 100%;"
                                + "z-index: 999999;"
                                + "border: 2px solid #fff;" 
                                + "background-repeat: no-repeat;"
                                + "position: fixed;";

                self.zoomWindow = $("<div style='" + self.zoomWindowStyle + "' class='zoomWindow'>&nbsp;</div>")
                        .appendTo('body')
                        .click(function () {
                                $(this).remove();
                                self.backimage.remove();
                        });
        }


    };




	$.fn.elevateZoom = function( options ) {
		return this.each(function() {
			var elevate = Object.create( ElevateZoom );

			elevate.init( options, this );

			

		});
	};

})( jQuery, window, document );