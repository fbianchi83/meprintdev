var editorval = {
  'save_file_id': 0,
  'product_width': '1000',
  'product_height': '1020',
  'template_id': 1,
  'gallery_fid': 609,
  'modal_id': 'editorModal',
  'font_size': 100,
  'font_max': 3000,
  'font_min': 4,
  'font_step': 2,
  'colors_type': 0,  //0 -> tutti colori ,1 -> cmyk ,2 -> solo elenco
  'font_color': {
    a: 1, 
    rgb: {
      r: 0,
      g: 0,
      b: 0
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 100
    }
  },
  'font_style': 'normal', /* normal, italic*/
  'font_weight': 'normal', /* normal, bold*/
  'text_decoration': 'none',
  'text_align': 'left',
  'shape_fill_color' : {
    a: 0.7,
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'shape_border_color': {
    a: 0.7,
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'circle_fill_color' : {
        rgb: {
            r: 42,
            g: 139,
            b: 200
        },
        cmyk: {
            c: 94,
            m: 32,
            y: 11,
            k: 0
        }
    },
    'circle_border_color': {
        rgb: {
            r: 9,
            g: 111,
            b: 176
        },
        cmyk: {
            c: 100,
            m: 48,
            y: 13,
            k: 1
        }
    },
  'text_content': 'Testo',
  'text_fill_color': {
    a: 0.0,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'text_border_color': {
    a: 0.0,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'dpi': 39.370,
  'min_image_dpi_y': 72,
  'min_image_dpi_x': 72,
  'z_index': 11,
  'cutting_grid': 0
};

var iRefreshEditor = undefined;
            
            
(function ($) {

    Drupal.behaviors.page_small_format = {
        
        attach: function (context, settings) {
            
              //previene il submit del form alla pressione del tasto 'invio' da tastiera
              $(window).keydown(function(event){
                if(event.keyCode == 13) {
                  event.preventDefault();
                  return false;
                }
               });
            
            function changeDimensions() {

                var dimension = $('#dimensions-div .form-group:eq(0) .dimension-input').val();

                var dim_dets = "#bd" + dimension;
                var dimension_details = $(dim_dets).val();
                var res = dimension_details.split("*");
                var productwidth = res[0];
                var productheight = res[1];

                $.ajax({
                    url: "/get_custom_template",
                    data: {
                        dimfind: dimension,
                        width: productwidth,
                        height: productheight,
                        type: 2
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (data) {
                        //IMPOSTO URL PER IL DOWNLOAD DEL TEMPLATE
                        $("#template-download").attr("href", data);
                    }
                });

                var dim_orient = "#oo" + dimension;
                var orients_details = $(dim_orient).val();
                var res_orient = orients_details.split("*");
                var vertical = res_orient[0];
                var horizontal = res_orient[1];

                if (vertical == 1){
                  $("#f-dimensions-order-v").show();          
                } else {
                  $("#f-dimensions-order-v").hide();
                }


                if (horizontal == 1){
                  $("#f-dimensions-order-h").show();
                } else {
                  $("#f-dimensions-order-h").hide();       
                }

                if (vertical != 1 && horizontal != 1)
                    $(".section-order").hide();    
                    
                //nei vari cambi di piega e dimensione resetto tutto e setto correttamente l'ordinamento
                $('.dimensions-order span.dimdiv__checked').remove();
                $('.dimensions-order .dimdiv .order-input').prop("checked",false);          
                $('.dimensions-order:visible:eq(0) .dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $('.dimensions-order:visible:eq(0) .dimdiv .order-input').prop("checked",true);


                $('input[name="dimensions"]').on("change", function () {

                    dimension = $(this).val();

                    $('#fixed_costs').val(1).trigger('change');

                    dim_dets = "#bd" + dimension;
                    dimension_details = $(dim_dets).val();
                    res = dimension_details.split("*");
                    productwidth = res[0];
                    productheight = res[1];

                    dim_orient = "#oo" + dimension;
                    orients_details = $(dim_orient).val();
                    res_orient = orients_details.split("*");
                    vertical = res_orient[0];
                    horizontal = res_orient[1];

                    $("#template-download").attr("href", "/sites/all/libraries/tcpdf/template.php?w=" + productwidth + "&h=" + productheight);

                    if (vertical == 1){
                      $("#f-dimensions-order-v").show();
                    }else{
                      $("#f-dimensions-order-v").hide();
                    }


                    if (horizontal == 1){
                      $("#f-dimensions-order-h").show();
                    }else{
                      $("#f-dimensions-order-h").hide();
                    }

                    $('.dimensions-order span.dimdiv__checked').remove();
                    $('.dimensions-order .dimdiv .order-input').prop("checked",false);  
                    $('.dimensions-order:visible:eq(0) .dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                    $('.dimensions-order:visible:eq(0) .dimdiv .order-input').prop("checked",true);


                });
            }

            function updateDimensions() {
                //var fixed_costs_path = $('input[name=fixed_costs_path]').val();
                var piega_id = $(".piega-input:checked").val();
                var form_id = $('#formid').val();

                $.ajax({
                    url: "/get-dimensions",
                    data: {
                        piega_id: piega_id,
                        form_id: form_id
                    },
                    dataType: "html",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {

                        $("#dimensions-div").html(data);
                        $('.section-dimension .dimension-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                        changeDimensions();
                        $('#fixed_costs').val(1).trigger('change');

                    },
                    error: function (data) {
                        //console.log("Error!");
                    }
                });
            }

            function changeWeights() {

                $('input[name="grammatura"]').on("change", function () {
                    $('#fixed_costs').val(1).trigger('change');
                });
            }

            function updateWeights() {
                //var fixed_costs_path = $('input[name=fixed_costs_path]').val();
                var piega_id = $(".piega-input:checked").val();
                var form_id = $('#formid').val();

                $.ajax({
                    url: "/get-weights",
                    data: {
                        piega_id: piega_id,
                        form_id: form_id
                    },
                    dataType: "html",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {

                        $("#grammatura-div").html(data);
                        //$('.section-grammatura .grammatura-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                        //changeWeights();
                        //$('#fixed_costs').val(1).trigger('change');
                        //$('.section-grammatura .grammatura-input').prop('checked', false);
                        //$('.section-grammatura span.dimdiv__checked').remove();
                        $('.section-grammatura .grammatura-input:checked').parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                        $('.section-grammatura .grammatura-input:checked').prop('checked', true);
                    },
                    error: function (data) {
                        //console.log("Error!");
                    }
                });
            }

            changeDimensions();
            changeWeights();


            $('.section-piega:not(".section-disable") .piega-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
            $('.section-piega:not(".section-disable")').on("click", 'input[type="radio"]', function () {
                $('.section-piega .piega-input').prop('checked', false);
                $('.section-piega span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $(this).prop('checked', true);

                //alert('update piega');
                updateDimensions();
                updateWeights();


            });


            // CAROUSEL

            $('.carousel').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 4,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: false                
                    }
                  },            
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      infinite: false
                    }
                  }
              ]
            });

            $('.carousel-theme').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 4,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>"
            });

            $('.carousel-material').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 4,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: false                
                    }
                  },            
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      infinite: false
                    }
                  }
              ]
            });

            $('.carousel-lavorazioni').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 4,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: false                
                    }
                  },            
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      infinite: false
                    }
                  }
              ]
            });

            $('.top-carousel').slick({
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
                dots: true,
                autoplay: true
            });

            $('.carousel-related').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 3,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-down fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-up fa-2x'></span>",
                vertical: true
            });

            $('.carousel-accessories').slick({
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 4,
                pauseOnHover: false,
                nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
                prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3,
                      infinite: false                
                    }
                  },            
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      infinite: false
                    }
                  }
              ]
            });

            // TOOLTIP
            if( $(window).width() > 1024 ){
              $("[data-toggle='tooltip']").tooltip();
            } else {
              $("[data-toggle='tooltip']").tooltip('disable');
            }

            //aggiungio ai preferiti
            $('#add-prefer').click(function () {
                var form_id = $(this).data("form-id");
                var form_type = $(this).data("form-type");

                $.ajax({
                    url: "/add-wishlist",
                    data: {
                        form_id: form_id,
                        form_type: form_type
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (response) {
                        $('#add-prefer').css("display", "none");
                        //alert("Prodotto aggiunto ai preferiti");
                        showModal("alertModal", Drupal.t("add prefer"),Drupal.t("product added to favorites") );
                    }
                });

            });


            // mi serve sapere quale upload image è stato selezionato
            $('.custom-file-theme').on('click', function () {
                var id = $(this).attr('id').replace('custom-file-theme-', '');
                //al click si apre una modale (che è unica)...per sapere a quale upload image corrisponde inserisco un campo con l'id del chiamante
                $('#exampleModal').attr('data-upi', id);
            });

            // IMAGE GALLERY che appende l'immagine scelta in zona upload
            $('#modal-save').click(function () {
                var imgId = $('.theme-input:checked').val();
                var img = $('#theme_img-' + imgId).attr('src');
                var upi = $('#exampleModal').data('upi');
                //console.log( 'save upi: ' + upi );
                $('#exampleModal').modal('hide');
                $('#upi-' + upi + ' .galleryimage--choose, #upi-' + upi + ' .uploadproductimage__remove').remove();//nel caso di modifica elimino img caricata precedentemente
                $('#upi-' + upi).append('<img id="load-img-' + upi + '-' + imgId + '" class="galleryimage--choose" src="' + img + '" /><a class="uploadproductimage__remove"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');
                $('#upi-' + upi + ' .fileuploader').hide();
                $('#upi-' + upi + ' .gallery_image_fid').val(imgId);
                $('.dimdiv--gallery__checked').remove();
                //var elemID = $(this).parents('.modal').attr('id');
                //$('a[data-target="#' + elemID + '"]').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--gallery__checked"></span>');

                //nascondo il bottone EDITOR e modifico il testo del bottone tema
                //$('#upi-'+upi+' .custom-file-editor, #upi-'+upi+' input[type="file"]').hide();
                //$('#upi-' + upi + ' input[type="file"]').hide();
                $('#upi-' + upi + ' .custom-file-theme').text(Drupal.t('change theme'));
                $('#upi-' + upi + ' .custom-file-editor').text(Drupal.t('change theme with editor'));
                //rimuovo il data-upi per la successiva modale
                $('#exampleModal').removeData("upi");
                $('#custom-file-editor-'+upi).click();
            });

            $('#editor_close').on('click', function() {
                var id = $('.uploadproductimage').attr('id').replace('upi-', '');
                var galFid = $('#gallery_fid_'+id).val();
                
                if (galFid > 0) {
                    $('.uploadproductimage__remove').click();
                }
              });
              
            $('.uploadproductimage').on('click', '.uploadproductimage__remove', function () {
                var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');
                //console.log( 'remove id: ' + id );
                $('#upi-' + id + ' .fileuploader').show();
                $('#upi-'+id+' img').remove();
                $('#upi-'+id).removeClass('uploadproductimage--selected');
                $('#upi-'+id+' .uploadproductimage__remove').remove();
                $('.dimdiv--theme__checked').remove();
                $('#upi-' + id + ' .gallery_image_fid').val('');
                $('.dimdiv--theme .theme-input').prop('checked',false);
                //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
                //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
            
            });

            $('.uploadproductimage').on('click', '.uploadproductimage__remove2', function () {
              var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

              var fid = jQuery("#image_fid_"+id).val();

              $.post( '/remove_image', {'fid': fid}, function() {
                  $('#upi-' + id + ' .fileuploader').show();
                  $('#upi-'+id+' img').remove();
                  $('#upi-'+id).removeClass('uploadproductimage--selected');
                  $('#upi-'+id+' .uploadproductimage__remove2').remove();
                  $('.dimdiv--theme__checked').remove();
                  $('.dimdiv--theme .theme-input').prop('checked',false);
                  $('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                  $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                  $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
                  //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
                  var loaded= $('#loaded').val();
                  loaded--;

                  $('#loaded').val(loaded);
                  
                  if ($('#beforenoon').val() == 1) {
                      $('.d0').hide();
                      $('.dx').show();
                      if ($('#q'+$('#productQuanity').val()+'-d0').hasClass("selected")) {
                            $('#q'+$('#productQuanity').val()+'-d1').click();
                      }
                  }
              })
              .fail(function() {
                  $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
              });

            });
            
            $('.uploadproductimage').on('click', '.uploadproductimage__remove3', function () {
                var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

                var fid = jQuery("#editor_fid_"+id).val();
                var previewfid = jQuery("#editor_fid_"+id).data('preview');

                $.post( '/remove_image', {
                    'fid': fid,
                    'previewfid': previewfid
                }, function() {
                  $('#upi-' + id + ' .fileuploader').show();
                  $('#upi-'+id+' img').remove();
                  $("#editor_fid_"+id).val('');
                  $("#editor_fid_"+id).attr('data-preview','');
                  $('#upi-'+id).removeClass('uploadproductimage--selected');
                  $('#upi-'+id+' .uploadproductimage__remove3').remove();
                  $('.dimdiv--theme__checked').remove();
                  $('.dimdiv--theme .theme-input').prop('checked',false);
                  //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                  $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                  $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
                  //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
                  $('#image_'+id).show();
                  var loaded= $('#loaded').val();
                  loaded--;

                    $('#loaded').val(loaded);
                    if ($('#beforenoon').val() == 1) {
                        $('.d0').hide();
                        $('.dx').show();
                        if ($('#q'+$('#productQuanity').val()+'-d0').hasClass("selected")) {
                            $('#q'+$('#productQuanity').val()+'-d1').click();
                        }
                    }
                })
                .fail(function() {
                  $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
                });

              });
            
            
            // RADIO BUTTON MODAL GALLERY

            $('.modal-gallery .theme-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
            $('.modal-gallery .theme-input[type="radio"]').click(function () {
                $('.modal-gallery .theme-input').prop('checked', false);
                $('.modal-gallery span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
                $(this).prop('checked', true);
            });


            $("button.zoom-button").on('click', function (ev) {
                ev.stopPropagation();

                //alert('puppa');
                var src = $(this).data('src');
                var el = $(this).closest("label.dimdiv--theme").find("img.img-responsive");

                el.elevateZoom({url: src});

                return false;

            });


            var wWindow = $(window).width();        
            if( wWindow > 767 ){

              $('#relatedpanel').scrollToFixed({
                  marginTop: $('#summary-cart--product').height(), 
                  limit: $($('.blue-bg')).offset().top
              });
              $('#summary-cart--product').scrollToFixed({
                  marginTop: 30, 
                  limit: $($('.blue-bg')).offset().top - $('#summary-cart--product').height(),
                  preFixed: function() { $('#relatedpanel').css('top', $('#summary-cart--product').height() + 120); }
              });


            }
            $('input[name="material"]').on("change", function () {
                $('#fixed_costs').val(1).trigger('change');
            });


            // RADIO BUTTON DIMENSIONI

            $('.section-dimension:not(".section-disable") .dimension-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
            $('.section-dimension:not(".section-disable")').on("click", 'input[type="radio"]', function () {
                $('.section-dimension .dimension-input').prop('checked', false);
                $('.section-dimension span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $(this).prop('checked', true);

            });

            // RADIO BUTTON GRAMMATURA
            $('.section-grammatura:not(".section-disable") .grammatura-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
            $('.section-grammatura:not(".section-disable")').on("click", 'input[type="radio"]', function () {
                $('.section-grammatura .grammatura-input').prop('checked', false);
                $('.section-grammatura span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $(this).prop('checked', true);
                maxfac= $('#grammatura-'+$(this).val()+'-maxf').val();
                
                $('#faceQuantity').attr('max',maxfac);
            });

            // RADIO DATA
            $('.date-div input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>');
            $('.date-div input').click(function () {
                $('.date-div input').prop('checked', false);
                $('.date-div span.dimdiv__checked').remove();
                $(this).parents('.date-div').append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>');
                $(this).prop('checked', true);

                var splittedResult = $(this).attr('id').split('-');
                var day = splittedResult[2];
                var day_n = day.substring(1, 2);
                var date_txt = $("#shippingdateprintdiv" + day_n).data("shippingdate");
                $("#shipping_date").val(date_txt);
                $(".shippingdatediv").html(date_txt);

            });


            var ship_table_type = $("#ship-table").data("type");

            if (ship_table_type === "fixed") { //CALCOLO PER LA TABELLA CON DIMENSIONI PREDEFINITE

                $('.date-div').css('cursor', 'default');

                var amount_first = $('.fixed-amounts').first().data("value");
                $('#ship-table .fa-check-circle').remove();
                $('.ship-cell-fixed').removeClass('selected');
                $("#q" + amount_first + "-d0").append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>').addClass('selected');
                $('#ship-table th').removeClass('day-selected');
                $('#d0').addClass('day-selected');
                $('.ship-col').removeClass('quantity-selected');
                $('#q' + amount_first).addClass('quantity-selected');

                $('.date-div input').prop('checked', false);
                $('.date-div span.dimdiv__checked').remove();
                $('#d0').prop('checked', true);
            }


            // accessories checkbox
            $(".extraaccessories_class").on('click', function () {
                if ($(this).prop('checked')) {
                    $(this).closest('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--accessories__checked"></span>');
                    $(this).closest('.text-center').find('.acc-qnty').show();
                    $(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(1).trigger('change');

                } else {

                    $(this).closest('.dimdiv--circle').find('span.dimdiv--accessories__checked').remove();
                    $(this).closest('.text-center').find('.acc-qnty').hide();
                    $(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(0).trigger('change');

                }
            });

            // RADIO LAVORAZIONI
            $('.lavorazioni_class').click(function () {
                if ($(this).prop('checked')) {
                    if ($('.lavorazioni_class:checked').length == 1) {
                        $(this).closest('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--accessories__checked"></span>');
                        //$(this).closest('.text-center').find('.acc-qnty').show();
                        //$(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(1);
                    }
                    else {
                        $(this).prop('checked',false);
                    }
                 
                } else {
                    $(this).closest('.dimdiv--circle').find('span.dimdiv--accessories__checked').remove();
                    //$(this).closest('.text-center').find('.acc-qnty').hide();
                    //$(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(0);

                }

            });

            // RADIO shipping
            $('.dimdiv--shipping input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--shipping__checked"></span>');
            $('.dimdiv--shipping input').click(function () {
                $('.dimdiv--shipping input').prop('checked', false);
                $('.dimdiv--shipping span.dimdiv__checked').remove();
                $(this).parents('.dimdiv--shipping').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--shipping__checked"></span>');
                $(this).prop('checked', true);
            });

            // RADIO orientamento
            $('.section-order:not(".section-disable") .order-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
            $('.section-order:not(".section-disable")').on("click", 'input[type="radio"]', function () {
                $('.section-order .order-input').prop('checked', false);
                $('.section-order span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $(this).prop('checked', true);

            });

            // MATERIALI RADIO
            $('.dimdiv--circle .material_class:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--material__checked"></span>');
            $('.dimdiv--circle .material_class').click(function () {
                $('.dimdiv--circle .material_class').prop('checked', false);
                $('.dimdiv--circle span.dimdiv--material__checked').remove();
                $(this).parents('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--material__checked"></span>');
                $(this).prop('checked', true);

            });


            // COPERTINA RADIO
            $('.adviced-cover').hide();
            $('.section-cover:not(".section-disable") .cover-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
            $('.section-cover:not(".section-disable") .cover-input').on("click", function () {
                $('.section-cover .order-input').prop('checked', false);
                $('.section-cover span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                $(this).prop('checked', true);

                if ($(this).val() == 2) {
                  $('.adviced-cover').show();
                }
                else {
                  $('.adviced-cover').hide();
                }


            });


          // FRONTE RETRO RADIO
          $('.adviced-fr').hide();
      $('#upi-0').hide();
      var nf = parseInt($('.change_number_files').html());
      $('.section-fronteretro .fr-png').on("click",function() {
        //console.log( $(this).parents('.dimdiv--fr').attr('class') );
        if( $(this).parents('.dimdiv--fr').hasClass('dimdiv--checked') == true ){
          $(this).parents('.dimdiv--fr').removeClass('dimdiv--checked');
          $('span.dimdiv--fr__checked').remove();
          $('.dimdiv--fr .fronteretro_class').prop('checked',true);
          $('.section-fronteretro .fr-png').css('border', '3px solid #333');
          $('.adviced-fr').hide();

          $('.change_number_files').html(nf);
          $('#upi-0').hide();

          $('#plus_file').val(0);


        } else {
          $(this).parents('.dimdiv--fr').addClass('dimdiv--checked');
          $(this).parents('.dimdiv--fr').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--operator-extra__checked dimdiv--fr__checked"></span>');
          $('.section-fronteretro .fr-png').css('border', '3px solid #ED7400');
          $('.dimdiv--fr .fronteretro_class').prop('checked',false);

          $('.adviced-fr').show();
          $('.extra_side').prop('checked',false);
        }
      });


          $('.extra_side').on("click", function () {    
            if ($(this).val() == 2) {
              $('#upi-0').show();
              $('.change_number_files').html(nf + 1);

              $('#plus_file').val(1);

            } else {
              $('.change_number_files').html(nf);
              $('#plus_file').val(0);
              $('#upi-0').hide();
            }
          });


          $('.dimdiv--only input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--only__checked"></span>');
          $('.dimdiv--only-border img').click(function() {
            //console.log($(this).prev().is(':checked'));
            var input_checked = $(this).prev();
            if( input_checked.is(':checked') == true ){
              //console.log('oki');
              input_checked.prop('checked', false).trigger("change");
              $(this).siblings('span.dimdiv--only__checked').remove();

            } else {
              //console.log('iko');
              input_checked.prop('checked', true).trigger("change");
              $(this).parents('.dimdiv--only').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--only__checked"></span>');
            }

          });




            /*
             * INIZIO FUNZIONI DI CARRELLO
             */

            //aggiungi al carrello
            $('#smallproceed').on('click', function (event) {
                event.preventDefault();
                //console.log( 'form valid: ' + $("#product-small-format").valid() );
                if ($("#product-small-format").valid()) {
                    add_cart();
                }

            });
            
            $('#smallproceed2').on('click', function (event) {
                event.preventDefault();
                //console.log( 'form valid: ' + $("#product-small-format").valid() );
                if ($("#product-small-format").valid()) {
                    add_cart();
                }

            });


            function add_cart() {


                var data = new FormData();

                var upload_images_fid = [];
                var gallery_images_fid = [];
                var editor_images_fid = [];
                var editor_preview_fid = new Array();
                
                $('.upload_image_fid').each(function (i, obj) {

                    if ($(obj).val() != '') {
                        //alert($(obj).val());
                        upload_images_fid.push($(obj).val());
                    }
                });

                $('.gallery_image_fid').each(function (i, obj) {

                    if ($(obj).val() != '') {
                        //alert($(obj).val());
                        gallery_images_fid.push($(obj).val());
                    }
                });

                $('.editor_image_fid').each(function (i, obj) {

                    if ($(obj).val() != '') {
                        var fid = $(obj).val();
                        editor_images_fid.push(fid);
                        editor_preview_fid[fid] = $(obj).data('preview');
                    }

                });
                
                var product_type = $("#type").val();
                var product_id = $("#productid").val();
                var material_id = $(".material_class:checked").val();
                var dimension_id = $('input[name=dimensions]:checked').val();

                var folding_id = $('input[name=piega]:checked').val();
                var orientation = $('input[name=order]:checked').val();
                var weight_id = $('input[name=grammatura]:checked').val();
                var frontback = $('input[name=fronteretro]:checked').val();
                var plus_file = $("#plus_file").val();
                var facciate = $('input[name=facciate]').val();
                var cover = $('input[name=copertina]:checked').val();
                var cover_material_id = $('input[name=custom_cover]:checked').val();
                var cover_weight_id = $('input[name=custom_grammatura]:checked').val();


                //var processing_id = $('input[name*=baseprocessing]:checked').val();
                //var processing_cost = $("#processing_cost").val();
                var longterm_id = $('input[name=lngtmprdctn]:checked').val();
                var shipping_date = $("#shipping_date").val();
                var quantity = $("#productQuanity").val();
                var operator_review = $('input[name=operatorreview]:checked').val();
                //var anonimous_pack = $('input[name=anonshipping]:checked').val();
                var name = $("#name").val();
                var note = $("#note").val();
                var total_price = $("#total_cost").val();
                var promo_value = $("#promo_value").val();

                //LAVORAZIONI ACCESSORIE SELEZIONATE
                var extraprocessing = new Array();

                $('.lavorazioni_class').each(function (index, element) {
                    if ($(element).is(':checked')) {

                        extraprocessing.push({id: $(element).val(), cost: $(element).data('price')});

                    }
                });

                //FINE LAVORAZIONI ACCESSORIE


                //ACCESSORI

                // ACCESSORI SELEZIONATI
                var accessories_arr = new Array();

                $('.extraaccessories-qnty-Value').each(function (index, element) {

                    if ($(element).val() > 0) {
                        var extraaccqnty = parseFloat($(element).val());
                        var extraaccprice = parseFloat($(element).data('price'));
                        accessories_arr.push({id: $(element).data('id'), quantity: extraaccqnty, cost: extraaccprice});
                    }
                });



                //FINE ACCESSORI

                var post_data = {
                    product_type: product_type,
                    product_id: product_id,
                    material_id: material_id,
                    dimension_id: dimension_id,
                    longterm_id: longterm_id,
                    shipping_date: shipping_date,
                    quantity: quantity,
                    operator_review: operator_review,
                    //anonimous_pack: anonimous_pack,
                    name: name,
                    note: note,
                    total_price: total_price,
                    promo_value: promo_value,
                    extra_processing_arr: JSON.stringify(extraprocessing),
                    accessories_arr: JSON.stringify(accessories_arr),
                    upload_images_fid: JSON.stringify(upload_images_fid),
                    gallery_images_fid: JSON.stringify(gallery_images_fid),
                    editor_images_fid: JSON.stringify(editor_images_fid),
                    editor_preview_fid: JSON.stringify(editor_preview_fid),
                    plus_file: plus_file,
                    folding_id: folding_id,
                    orientation: orientation,
                    weight_id: weight_id,
                    frontback: frontback,
                    facciate: facciate,
                    cover: cover,
                    cover_material_id: cover_material_id,
                    cover_weight_id: cover_weight_id
                };


                for (var k in post_data) {
                    data.append(k, post_data[k]);
                }

                //console.log( data );

                $.ajax({
                    type: "POST",
                    url: "/add-cart-small",
                    processData: false,
                    contentType: false,
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        if (response == 'date-error') {                            
                            showModal("alertModal", Drupal.t("Shipping Date"),Drupal.t("date of shipment is not allowed")+ '!' );
                        } else if (response == 'ok') {
                            var maxheight = $('input[name=dbheight2-' + material_id + ']').val();
                            var maxwidth = $('input[name=dbwidth2-' + material_id + ']').val();

                            if ($('#productHeight').val() > maxheight || $('#productWidth').val() > maxwidth) {
                                $('#modalmaterial' + material_id).css("display", "block");
                                $('#modalclick').click();
                                setTimeout(function () {
                                    $('#dimensionModal').modal('hide');
                                    window.location.replace("/carrello");
                                }, 5000);
                            }
                            else
                                window.location.replace("/carrello");
                        }
                    }
                });
            }

            //var frameSrc = "/login";

            $('.btn-editor').click(function () {
                //$('#myModal').on('show', function () {
                //$('iframe').attr("src",frameSrc);
                //});
                //$('#myModal').modal({show:true});

                var id = $(this).attr('id');
                var id_arr = id.split("-");

                var id_num = id_arr[3];

                var gallery_img_id = $('.galleryimage--choose').attr('id');
                if (gallery_img_id != null) {
                    var gallery_img_id_arr = gallery_img_id.split("-");
                    var gallery_fid = gallery_img_id_arr[3];
                }

                var dimensionFinder = $('input[name=dimensions]:checked').val();
                var productheight = $('#productHeight').val();
                var productwidth = $('#productWidth').val();
                var dim_dets = "";
                var dimension_details = 0 + "*" + 0;

                if (productheight === "") {
                    productheight = 1;
                }

                if (productwidth === "") {
                    productwidth = 1;
                }

                if ((dimensionFinder != null) && (dimensionFinder != "custom")) {
                    dim_dets = "#bd" + dimensionFinder;
                    dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    productwidth = res[0];
                    productheight = res[1];
                }

                editorval['min_image_dpi_y'] = 72;
                editorval['min_image_dpi_x'] = 72;
                editorval['colors_type'] = 1;
                editorval['cutting_grid'] = 0;
                

                editorval['product_width'] = productwidth;
                editorval['product_height'] = productheight;
                editorval['save_file_id'] = id_num;

                if (gallery_fid !== "") {
                    editorval['gallery_fid'] = gallery_fid;
                }

                var preview_fid = 0;
                $.ajax({
                    type: "POST",
                    url: "/getPreview",
                    data: { gallery_fid: gallery_fid },
                    success: function( response ){
                        console.log(response);
                        preview_fid= response["preview"];
                        console.log(preview_fid);
                    },
                    complete: function() {
                        if(preview_fid !== ""){
                            editorval['preview_fid'] = preview_fid;
                        }
                        else{
                            editorval['preview_fid'] = "";
                        }

                        iRefreshEditor(editorval);
                    }
                });
                //console.log(editorval);

                //var source='/publishereditor';
                //$('#editorModal .publisher-editor').attr('src', source);
                //var x = document.getElementById("editorModalFrame");
                //var y = (x.contentWindow || x.contentDocument);
                //if (y.document)y = y.document;
                //var input = y.getElementById('refresh_trick');
                //console.log(input);
                //iRefreshEditor(editorval);
            });

            $('.fileuploader').each(function(i, obj) {
          
          var divID = $(obj).attr('c');
          
          $(obj).uploadFile({
                dragDrop: false,
                showAbort: true,
                url:'/save_image',
                showDelete: false,
                showFileCounter:false,
                returnType: "json",
                fileName:"uploaded_file",
                onSubmit:function(files)
                {
                  //nascondo il bottone EDITOR e modifico il testo del bottone tema
                  $('#custom-file-theme-'+divID+', #custom-file-editor-'+divID).hide();
                },
                onSuccess: function(files,response,xhr,pd)
                {
                    //console.log(response);
                    
                    $('#upi-'+divID).addClass('uploadproductimage--selected');

                    $("#image_fid_"+divID).val(response.fid);

                    $(".ImageError_"+divID).html("");

                    $(".dvPreview_"+divID).html('<img style="width:200px" src="'+ response.url+'" class="uploadproductimage--choose" ><a class="uploadproductimage__remove2"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');

                    $('#image_'+divID).hide();
                    $('#upi-'+divID + ' .ajax-file-upload-statusbar').remove();
                    var loaded= $('#loaded').val();
                    loaded++;
                    
                    $('#loaded').val(loaded);
                    if (loaded == $('#plus_file').val()+1) {
                        $('.d0').show();
                        $('.dx').hide();
                    }
                    
                  //pageObject.addPicture(new PictureBox(pageObject, 5, 5, response.width, response.height, response.url, response.fid));
                },
                onError: function(files,status,errMsg,pd)
                {
                    $('#image_'+divID).val("");
                    $('#image_'+divID).show();                              
                    showModal("alertModal", Drupal.t("error"), Drupal.t("error loading image"));
                }
                /*customProgressBar: function(obj,s)
		{
                this.statusbar = $("<div class='custom-statusbar'></div>");
                this.filename = $("<div class='custom-filename'></div>").appendTo(this.statusbar);
                this.progressDiv = $("<div class='custom-progress'>").appendTo(this.statusbar).hide();
                this.progressbar = $("<div class='custom-bar'></div>").appendTo(this.progressDiv);
                this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
                this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
                this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
                this.download = $("<div>" + s.downloadStr + "</div>").appendTo(this.statusbar).hide();
                this.del = $("<div>" + s.deletelStr + "</div>").appendTo(this.statusbar).hide();

                this.abort.addClass("custom-red");
                this.done.addClass("custom-green");
                this.download.addClass("custom-green");            
                this.cancel.addClass("custom-red");
                this.del.addClass("custom-red");
                
                            return this;

                    }*/
            });
      });
      
        /*    $('.ImageUploadBig').each(function (i, obj) {

                var divID = $(obj).attr('c');
                  $(obj).on('change', function (evt) {
                    //var file = fileInput.files[0];

                    var files = evt.target.files; // FileList object
                    var data = new FormData();

                    $.each(files, function (i, file) {
                        data.append(i, file);
                    });

                    $.ajax({
                        url: '/save_image',
                        type: 'POST',
                        data: data,
                        cache: false,
                        processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                        success: function (response, textStatus, jqXHR)
                        {
                            //alert(divID);

                            $('#upi-'+divID).addClass('uploadproductimage--selected');

                            $("#image_fid_"+divID).val(response.fid);

                            $(".ImageError_"+divID).html("");

                            $(".dvPreview_"+divID).html('<img style="width:200px" src="'+ response.url+'" class="uploadproductimage--choose" ><a class="uploadproductimage__remove2"><span class="fa fa-trash"></span> ' + Drupal.t("remove image") + '</a>').addClass('uploadproductimage--selected');

                            $('#image_'+divID).hide();

                            //nascondo il bottone EDITOR e modifico il testo del bottone tema
                            $('#custom-file-theme-'+divID+', #custom-file-editor-'+divID).hide();


                            //pageObject.addPicture(new PictureBox(pageObject, 5, 5, response.width, response.height, response.url, response.fid));
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            $('#image_' + divID).val("");
                            $('#image_' + divID).show();
                            //alert("Errore nel caricamento dell'immagine");
                            showModal("alertModal", Drupal.t("error"), Drupal.t("error loading image"));
                        }

                    });
                });
            });
            */
            
            
              
            function capitalizeFirstLetter(string) {
              return string.charAt(0).toUpperCase() + string.slice(1);
            }

            function showModal(id,title,content){
              $('#'+id+' .modal-title').html(title);
              $('#'+id+' .modal-body').html( capitalizeFirstLetter(content) );
              $('#'+id).modal('show');
            }
              
            
            
        }
    };
    
})(jQuery);