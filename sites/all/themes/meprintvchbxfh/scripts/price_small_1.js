(function ($) {
    
    var prices_base = [];
    var prices_frontback = [];
    var prices_processing = [];
    var noOfProducts = 0;
    var totlPrice = 0;
    
    Drupal.behaviors.meprint = {
        
        attach: function (context, settings) {
            
            function setPrice() {
                
                noOfProducts = $('#productQuanity').val();
                
                if (noOfProducts == "") {
                    noOfProducts = 1;
                }
                
                var promotionalflag = $('#promotionalFlag').val();
                var promotype = $('#promotionalType').val();
                var promoval = $('#promotionalValue').val();
                var promoprice = 0;
                var operatorreview = $('input[name=operatorreview]:checked').val();
                
                if ((operatorreview == "") || (operatorreview == null)) {
                    operatorreview = 0;
                }
                
                $('#adtnlcostdiv').html(totlPrice);
                
                
                if (promotionalflag == "Promo") {
                    if (promotype == "value") {
                        promoval = parseFloat(noOfProducts) * parseFloat(promoval);
                        promoprice = totlPrice;
                        totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
                    }
                    if (promotype == "percentage") {
                        promoval = ((parseFloat(totlPrice) / 100) * parseFloat(promoval));
                        promoprice = totlPrice;
                        totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
                    }
                }
                
                var lavorazioni_accessorie = parseFloat(0);
                if (noOfProducts in prices_frontback) {
                    lavorazioni_accessorie = prices_frontback[noOfProducts];
                    lavorazioni_accessorie = lavorazioni_accessorie * noOfProducts;
                }
                
                
                
                /***
                 * 
                 * CALCOLO COSTO ACCESSORI
                 */
                //alert(extaccsids);
                var extraaccessories = parseFloat(0);
                $('.extraaccessories-qnty-Value').each(function (index, element) {
                    
                    
                    
                    var extraaccqnty = $(element).val();
                                        
                    
                    var extraaccprice = parseFloat($(element).data('price'));
                    //console.log("Accessori qnty: "+ extraaccqnty);
                    //console.log("Accessori price: "+ extraaccprice);
                    extraaccessories += parseFloat(extraaccprice*extraaccqnty);
                });
                
                var al_iva = 0.22;

                //matrlpricefrml = parseFloat(matrlpricefrml).toFixed(2);
                //processingCost = parseFloat(processingCost).toFixed(2);
                //productprice = parseFloat(productprice).toFixed(2);
                //singleproductPrice = parseFloat(singleproductPrice).toFixed(2);
                totlPrice = totlPrice + operatorreview + lavorazioni_accessorie + extraaccessories;
                totlPrice = parseFloat(totlPrice).toFixed(2);
                
                
                var iva = parseFloat(al_iva * totlPrice).toFixed(2);

                var totlPriceIva = parseFloat(totlPrice) + parseFloat(iva);
                totlPriceIva = parseFloat(totlPriceIva).toFixed(2);

                //$('.printigmaterialdiv').html(productprice);
                //$('.adtnlcostdiv').html(processingCost);
                //$('.singleproductcostdiv').html(singleproductPrice);
                var totalproducts = (prices_base[noOfProducts] * noOfProducts) + lavorazioni_accessorie + extraaccessories;
                totalproducts = parseFloat(totalproducts).toFixed(2);
                
                $('.totalproductcostdiv').html(totalproducts);
                $('.productcostdiv').html(prices_base[noOfProducts]);
                $('.ivadiv').html(iva);
                $('.totalcostdiv').html(totlPriceIva);
                $('.totalnoofproductbigfrmtdiv').html(noOfProducts);
                $('.bigtotlprice').val(totlPrice);
                
                //IMPOSTO GLI INPUT HIDDEN PER IL CARRELLO
                //$('#material_cost').val(productprice);
                //$('#processing_cost').val(processingCost);
                $('#total_cost').val(totlPrice);
                //$('#structure_cost').val(baseaccessories);
                $('#promo_value').val(promoval);
                
                if (promoprice != 0) {
                    $('.promopricetd').show();
                    promoprice = parseFloat(promoprice).toFixed(2);
                    $('.promopricediv').html(promoprice);
                } else {
                    $('.promopricetd').hide();
                }

                if (promoval != 0) {
                    $('.promovaltd').show();
                    promoval = parseFloat(promoval).toFixed(2);
                    $('.promovaldiv').html("-" + promoval);
                } else {
                    $('.promovaltd').hide();
                }
                
                if (operatorreview != 0) {
                    $('.operatorreviewtd').show();
                    operatorreview = parseFloat(operatorreview).toFixed(2);
                    $('.operatorreviewdiv').html(operatorreview);
                } else {
                    $('.operatorreviewtd').hide();
                }
                
                if (extraaccessories != 0) {
                    $('.extraaccessoriestd').show();
                    extraaccessories = parseFloat(extraaccessories).toFixed(2);
                    $('.extraacccstdiv').html(extraaccessories);
                } else {
                    $('.extraaccessoriestd').hide();
                }
                
                if (lavorazioni_accessorie != 0) {
                    $('.extraprocessingtd').show();
                    lavorazioni_accessorie = parseFloat(lavorazioni_accessorie).toFixed(2);
                    $('.extraprocessingdiv').html(lavorazioni_accessorie);
                } else {
                    $('.extraprocessingtd').hide();
                }
            }
            
            

            //code starts

            /************* On Load **********************/
            // var shippingidj =  $('input[name=shipping]:checked').val();
            //  updateShipping(shippingidj);   
            // alert(shippingidj);
            updateSmallDates();
            updateFixedCosts();
            /* Le due funzioni sottostanti vengono richiamate direttamente da updateFixedCosts() */
            //updateSmallDates();
            //updateValues();
          
            /**************** On Load ********************/

            
             
            //////// PRICE TABLE FIXED //////////////////////////
            ///////////////////////////////////////////////
            
            $('#ship-table .ship-cell-fixed').click(function () {
                
                
                var splittedResult = $(this).attr('id').split('-');
                var day = splittedResult[1];
                var quantity = splittedResult[0];
                totlPrice = $(this).data('price');
                
                $("#selected_day").val(day);
                
                $('#ship-table .fa-check-circle').remove();
                $('.ship-cell-fixed').removeClass('selected');
                $(this).append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>').addClass('selected');
                $('#ship-table th').removeClass('day-selected');
                $('#' + day).addClass('day-selected');
                $('.ship-col').removeClass('quantity-selected');
                $('#' + quantity).addClass('quantity-selected');
                
                $('.date-div input').prop('checked', false);
                $('.date-div span.dimdiv__checked').remove();
                $('#'+day).prop('checked', true);
                
                var only_quantity = $("#"+quantity).data('value');
                
                $("#productQuanity").val(only_quantity);
                
                var day_n = day.substring(1, 2);
                var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                $("#shipping_date").val(date_txt);
                $(".shippingdatediv").html(date_txt);
                
                setPrice();
                
            });
            
            // stampa fronte-retro
            $(".print-input").on('click', function () {
                
                if($(this).val() == 2) {
                    updateFrontBackCosts();
                } else {
                    prices_frontback = [];
                    updateValues();
                }
                
            });
                        
            $("input[name=operatorreview]").change(function () {
                setPrice();
            });
            
            ///////   ACCESSORIES QUANTITY NUMBER ///////////////
            $(".extraaccessories-qnty-Value").change(function () {
                                
                var value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                
                if(value === "" || !$.isNumeric( value ) ){
                    //alert("Deve essere un numero!");
                    $(this).focus();
                }else{
                    setPrice();
                }

            });
            
            // accessories checkbox
            $(".extraaccessories_class").on('click', function() {

              if ($(this).prop('checked')) {
                $(this).closest('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--accessories__checked"></span>');
                $(this).closest('.text-center').find('.acc-qnty').show();
                $(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(1);

              } else {
                $(this).closest('.dimdiv--circle').find('span.dimdiv--accessories__checked').remove();
                $(this).closest('.text-center').find('.acc-qnty').hide();
                $(this).closest('.text-center').find('.acc-qnty .form-group input.extraaccessories-qnty-Value').val(0);
                
              }
              setPrice();
            });
  
  
            /************* On change long term prouction start ******************/

            function getDataProd(mese, giorno, lingua) {

                var monthNames = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
                var dayNames = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];

                var monthNamesFr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
                var dayNamesFr = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

                var meseOut = "";
                var giornoOut = "";

                if (lingua == "it") {
                    meseOut = monthNames[mese];
                    giornoOut = dayNames[giorno];
                } else {
                    meseOut = monthNamesFr[mese];
                    giornoOut = dayNamesFr[giorno];
                }

                var result = [meseOut, giornoOut];

                return result;

            }

            function updateSmallDates() {

                var matdaysj = 0;
                var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();
                //var productgroupidj = $('#productgroupid').val();

                var formidj = $('#formid').val();
                var languageidj = $('#languageid').val();

                var delytimj = 0;
                var lngtmprdctnvals = 0;
                var lngrmprdtnid = "#lngtm" + lngtmprdctn;
                var lngtmprdctnvals = $(lngrmprdtnid).val();
                if ((lngtmprdctn != "") && (lngtmprdctn != null)) {
                    var lngtrmprdctnvl = lngtmprdctnvals.split("+");
                    delytimj = lngtrmprdctnvl[0];
                }

                //var workloadtypej = $("#workloadtype").val();
                //var workloadlimitj = $("#workloadlimit").val();
                //var formtype = $("#formtype").val();
                //if ((workloadlimitj == null) || (workloadlimitj == "")) {
                    //workloadlimitj = 0
                    //workloadtypej = "Products";
                //}

                //if (formtype == 'small') {

                    //var material_id_selected_name = $("#smaterialId option:selected").val();
                    //var material_prices = "#smtid" + material_id_selected_name;
                //} else {
                var material_id_selected_name = $(".material_class:checked").val();
                var material_prices = "#mtid" + material_id_selected_name;
                //}

                var materialdet = $(material_prices).val();

                if (materialdet != null) {
                    var matdetails = materialdet.split("+");
                    matdaysj = matdetails[4];
                } else if (materialdet == null) {
                    matdaysj = 0;
                }

                var dates_path = $('input[name=dates_path]').val();

                $.ajax({
                    url: dates_path,
                    data: {
                        mtrldys: matdaysj,
                        delytim: delytimj,
                        //workloadlimit: workloadlimitj,
                        //workloadtype: workloadtypej,
                        //productgroupid: productgroupidj,
                        formid: formidj,
                        languageid: languageidj
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (data) {
                        var dates = data.split("**")
                        var datelength = dates.length;
                        datelength = parseFloat(datelength) - 1;
                        for (var i = 0; i < datelength; i++) {

                            var dates_count = $("#dates_count").val();

                            if (i == 0 && dates_count == 0) {
                                $("#shipping_date").val(dates[i]);
                                $(".shippingdatediv").html(dates[i]);
                            }

                            var shipingdivid = "#shippingdateprintdiv" + i;

                            var data_arr = dates[i].split("-");
                            var giorno_mese = data_arr[0];
                            var mese = data_arr[1];
                            var anno = data_arr[2];

                            var d = new Date(anno, mese - 1, giorno_mese);

                            var giorno_sett = d.getDay();
                            var mese_from = d.getMonth();

                            var arr_date = getDataProd(mese_from, giorno_sett, languageidj);

                            var mese_html = arr_date[0];
                            var giorno_html = arr_date[1];

                            var html = "<div class='date-div__day'>" + giorno_html + "</div><div class='date-div__day-n'>" + giorno_mese + "</div><div class='date-div__month'>" + mese_html + "</div>";

                            $(shipingdivid).data("shippingdate", dates[i]);
                            $(shipingdivid).html(html);

                            $("#dates_count").val(i);
                        }


                    }
                });
            }

            /************* On change long term prouction End ******************/


            function updateFixedCosts() {
                //var fixed_costs_path = $('input[name=fixed_costs_path]').val();
                var material_id = $(".material_class:checked").val();
                var dimension_id = $(".dimension-input:checked").val();
                var weight_id = $(".grammatura-input:checked").val();
                
                var form_id = $('#formid').val();

                $.ajax({
                    url: "/get-small-costs",
                    data: {
                        mat_id: material_id,
                        form_id: form_id,
                        dimension_id: dimension_id,
                        weight_id: weight_id,
                    },
                    dataType: "json",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {
                        
                        
                        prices_base = data;
                        
                        updateValues();
                    },
                    error: function(data) {
                        //console.log("Error!");
                    }
                });
            }
            
            function updateFrontBackCosts() {
                
                var dimension_id = $(".dimension-input:checked").val();
                var form_id = $('#formid').val();

                $.ajax({
                    url: "/get-frontback-costs",
                    data: {
                        form_id: form_id,
                        dimension_id: dimension_id
                    },
                    dataType: "json",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {
                        
                        
                        prices_frontback = data;
                        
                        updateValues();
                    },
                    error: function(data) {
                        //console.log("Error!");
                    }
                });
            }
            
            function updateProcessingCosts(processing_id) {
                
                var dimension_id = $(".dimension-input:checked").val();
                var form_id = $('#formid').val();
                
                $.ajax({
                    url: "/get-processing-costs",
                    data: {
                        form_id: form_id,
                        dimension_id: dimension_id,
                        processing_id: processing_id
                    },
                    dataType: "json",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {
                        
                        
                        prices_processing[processing_id] = data;
                        
                        updateValues();
                    },
                    error: function(data) {
                        //console.log("Error!");
                    }
                });
            }
            
            
            /**********Multiplication Formula for Big Format Start *************/

            function updateValues() {
                
                //CODICE PER IL CALCOLO DEI PREZZI DELLA TABELLA DELLE DATE DI SPEDIZIONE //

                //CALCOLO PER LA TABELLA CON DIMENSIONI PREDEFINITE

                

                //ciclo le quantità predefinite
                $('.fixed-amounts').each(function () {

                    
                    var amount = $(this).data("value");

                    var discount = ($(this).data("discount") != '')?$(this).data("discount"):0;

                    var totalPrice_divided = prices_base[amount];

                    var ci_discount = ((parseFloat(totalPrice_divided) / 100) * parseFloat(discount));
                    var totalPrice_netto = parseFloat(totalPrice_divided) - parseFloat(ci_discount);
                    var ci_molt_tot_first = totalPrice_netto * amount;

                    $("#q" + amount + "-d0").attr("data-price", parseFloat(ci_molt_tot_first).toFixed(2));
                    $("#total-q" + amount + "-lngtm0").html(parseFloat(ci_molt_tot_first).toFixed(2));

                    if ($("#selected_day").val() == 0 && amount == $("#productQuanity").val()) {
                        totlPrice = parseFloat(ci_molt_tot_first).toFixed(2);
                        
                        var day_n = 0;
                        var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                        $("#shipping_date").val(date_txt);
                        $(".shippingdatediv").html(date_txt);
                    }


                    //CICLO LE DATE SUCCESSIVE PER AGGIORNARE I PREZZI
                    $('.lngtmnpt').each(function () {
                        var value = $(this).prop('value');
                        var id = $(this).prop('id');

                        var data_id = $(this).data('id');

                        if ((value !== "") && (value !== null)) {

                            var ci_lngtrmprdctnvl = value.split("+");
                            var ci_lantrmamnt = ci_lngtrmprdctnvl[1];
                            var ci_lantrmtype = ci_lngtrmprdctnvl[2];

                            if (ci_lantrmtype === "") {
                                ci_lantrmtype = "Value";
                            }
                            if (ci_lantrmamnt === "") {
                                ci_lantrmamnt = 0;
                            }
                        }

                        if (ci_lantrmtype == "Value") {
                            var ci_tot = parseFloat(totalPrice_divided) - parseFloat(ci_lantrmamnt);
                        }
                        else if (ci_lantrmtype == "Percentage") {
                            var ci_lngtrmdsc = ((parseFloat(totalPrice_divided) / 100) * parseFloat(ci_lantrmamnt));
                            var ci_tot = parseFloat(totalPrice_divided) - parseFloat(ci_lngtrmdsc);
                        }

                        var ci_lantrm_discount = ((parseFloat(ci_tot) / 100) * parseFloat(discount));
                        var totalPrice_lantrm__netto = parseFloat(ci_tot) - parseFloat(ci_lantrm_discount);

                        var ci_molt_tot = totalPrice_lantrm__netto * amount;
                        
                        $("#q" + amount + "-d" + data_id).attr("data-price", parseFloat(ci_molt_tot).toFixed(2));
                        $("#total-q" + amount + "-" + id).html(ci_molt_tot.toFixed(2));
                        
                        if ($("#selected_day").val() == data_id && amount == $("#productQuanity").val()) {
                            totlPrice = parseFloat(ci_molt_tot).toFixed(2);
                            var day_n = data_id;
                            var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                            $("#shipping_date").val(date_txt);
                            $(".shippingdatediv").html(date_txt);
                        }
                        
                    });
                });
                
                
                setPrice();
                
            }
            
        }
    };



})(jQuery);
