(function ($) {
    Drupal.behaviors.meprint = {
        attach: function (context, settings) {
            
            /************* Shippping price Calculation start ******************/

            function updateShipping() {
                var dimwgtj = 0;
                var matweightj = 1;
                var matthicknessj = 1;
                var productwidthj = 1;
                var productheightj = 1;
                var productgroupidj = $('#productgroupid').val();
                var formidj = $('#formid').val();
                var shippingidj = $('input[name=shipping]:checked').val();
                var languageidj = $('#languageid').val();
                var noOfProductsj = $('#productQuanity').val();
                productheightj = $('#productHeight').val();
                productwidthj = $('#productWidth').val();
                dimwgtj = $('#dimwgt').val();

                languageidj = "it";
                if ((dimwgtj == "") || (dimwgtj == null)) {
                    dimwgtj = 0;
                }
                var dimensionFinder = $('input[name=dimensions]:checked').val();
                if ((dimensionFinder != null) && (dimensionFinder != "custom")) {
                    var dim_dets = "#bd" + dimensionFinder;
                    var dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    productwidthj = res[0];
                    productheightj = res[1];
                } else if (dimensionFinder == "custom") {
                    productheightj = $('#productHeight').val();
                    productwidthj = $('#productWidth').val();
                } else if ((productheightj == "") && (productwidthj == "")) {
                    var dmsnidj = $('input[name=sdimensions]:checked').val();
                    if ((dmsnidj != null) && (dmsnidj != "")) {
                        var sres = dmsnidj.split("*");
                        productwidthj = sres[0];
                        productheightj = sres[1];
                    }
                } else {
                    productwidthj = 1;
                    productheightj = 1;
                }

                if ((noOfProductsj == "") || (noOfProductsj == null)) {
                    noOfProductsj = $("#sproductQuanity").val();
                    if ((noOfProductsj == "") || (noOfProductsj == null)) {
                        noOfProductsj = 1;
                    }
                }
                //  debugger; 
                var material_id_selected_name = $(".material_class:checked").val();
                var material_prices = "#mtid" + material_id_selected_name;
                var materialdet = $(material_prices).val();
                if (materialdet != null) {
                    var matdetails = materialdet.split("+");
                    matweightj = matdetails[5];
                    matthicknessj = matdetails[6];
                } else if (material_id_selected_name == null) {
                    material_id_selected_name = $("#smaterialId option:selected").val();
                    if (material_id_selected_name != null) {
                        material_prices = "#smtid" + material_id_selected_name;
                        materialdet = $(material_prices).val();
                        var matdetails = materialdet.split("+");
                        matweightj = matdetails[5];
                        matthicknessj = matdetails[6];
                    }
                } else {
                    matweightj = 1;
                    matthicknessj = 1;
                }

                var shipping_path = $('input[name=shipping_path]').val();
                if (shipping_path != "") {
                    $.ajax({
                        url: shipping_path,
                        data: {
                            matweight: matweightj,
                            matthickness: matthicknessj,
                            productgroupid: productgroupidj,
                            formid: formidj,
                            languageid: languageidj,
                            noOfProducts: noOfProductsj,
                            productwidth: productwidthj,
                            productheight: productheightj,
                            dimwgt: dimwgtj,
                            shippingid: shippingidj,
                            myshipid: shippingidj,
                        },
                        async: true,
                        dataType: "html",
                        type: "POST",
                        success: function (data) {
                            $('.htotalshippingprice').val(data);
                            $('#shippingprice').val(data);
                            $('.totalshippingprice').html(data);
                        },
                        error: function () {
                        },
                        complete: function () {
                        }
                    });
                }
            }
            /************* Shippping price Calculation End ******************/
            
        }
    };
})(jQuery);



