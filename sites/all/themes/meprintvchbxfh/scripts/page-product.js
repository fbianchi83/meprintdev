var editorval = {
  'save_file_id': 0,
  'product_width': '1000',
  'product_height': '1020',
  'template_id': 1,
  'external_image': 1,
  'gallery_fid': 609,
  'preview_fid': 609,
  'modal_id': 'editorModal',
  'font_size': 100,
  'font_max': 3000,
  'font_min': 4,
  'font_step': 2,
  'colors_type': 0,  //0 -> tutti colori ,1 -> cmyk ,2 -> solo elenco
  'font_color': {
    rgb: {
      r: 0,
      g: 0,
      b: 0
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 100
    }
  },
  'font_style': 'normal', /* normal, italic*/
  'font_weight': 'normal', /* normal, bold*/
  'text_decoration': 'none',
  'text_align': 'left',
  'shape_fill_color' : {
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'shape_border_color': {
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'circle_fill_color' : {
        rgb: {
            r: 42,
            g: 139,
            b: 200
        },
        cmyk: {
            c: 94,
            m: 32,
            y: 11,
            k: 0
        }
    },
    'circle_border_color': {
        rgb: {
            r: 9,
            g: 111,
            b: 176
        },
        cmyk: {
            c: 100,
            m: 48,
            y: 13,
            k: 1
        }
    },
  'text_content': 'Testo',
  'text_fill_color': {
    transparent: 1,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'text_border_color': {
    transparent: 1,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'dpi': 39.370,
  'min_image_dpi_y': 72,
  'min_image_dpi_x': 72,
  'z_index': 11,
  'cutting_grid': 0
};

var iRefreshEditor = undefined;


(function ($) {

  Drupal.behaviors.meprint = {
    attach: function (context, settings) {
            
      //previene il submit del form alla pressione del tasto 'invio' da tastiera
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
       });
            
      $('.material_class').each(function() {
          if (!$(this).prop('checked')) {
              $('#cert'+$(this).val()).css('display', 'none');
          }
      });
      
      // RADIO BUTTON DIMENSIONI
      $('.section-dimension:not(".section-disable") .dimension-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
      $('.section-dimension:not(".section-disable")').on("click",'input[type="radio"]',function() {
        $('.section-dimension .dimension-input').prop('checked', false);
        $('.section-dimension span.dimdiv__checked').remove();
        $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked"></span>');
        $(this).prop('checked', true);
      });

      // RADIO BUTTON MODAL GALLERY
      $('.modal-gallery .theme-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
      $('.modal-gallery .theme-input[type="radio"]').click(function() {
        $('.modal-gallery .theme-input').prop('checked', false);
        $('.modal-gallery span.dimdiv__checked').remove();
        $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
        $(this).prop('checked', true);
      });


      $("button.zoom-button").on('click', function(ev) {
        ev.stopPropagation();
        ev.preventDefault();

        var src = $(this).data('src');
        var el = $(this).closest("label.dimdiv--theme").find("img.img-responsive");

        el.elevateZoom({
          url: src
        });

      });

      // RADIO DATA
      $('.date-div input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>');
      $('.date-div input').click(function() {
        $('.date-div input').prop('checked', false);
        $('.date-div span.dimdiv__checked').remove();
        $(this).parents('.date-div').append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>');
        $(this).prop('checked', true);

        var splittedResult = $(this).attr('id').split('-');
        var day = splittedResult[2];
        var day_n = day.substring(1, 2);
        var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
        $("#shipping_date").val(date_txt);
        $(".shippingdatediv").html(date_txt);

      });

      var ship_table_type = $("#ship-table").data("type");

      if (ship_table_type === "fixed"){ //CALCOLO PER LA TABELLA CON DIMENSIONI PREDEFINITE

        $('.date-div').css( 'cursor', 'default' );

        var amount_first = $('.fixed-amounts').first().data("value");
        $('#ship-table .fa-check-circle').remove();
        $('.ship-cell-fixed').removeClass('selected');
        $("#q"+amount_first+"-d0").append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>').addClass('selected');
        $('#ship-table th').removeClass('day-selected');
        $('#d0').addClass('day-selected');
        $('.ship-col').removeClass('quantity-selected');
        $('#q'+ amount_first).addClass('quantity-selected');

        $('.date-div input').prop('checked', false);
        $('.date-div span.dimdiv__checked').remove();
        $('#d0').prop('checked', true);
      }

      // RADIO MATERIALI
      $('.dimdiv--circle .baseprocessing_class:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--processing__checked"></span>');
      $('.dimdiv--circle .baseprocessing_class').click(function() {
        $('.dimdiv--circle .baseprocessing_class').prop('checked', false);
        $('.dimdiv--circle span.dimdiv--processing__checked').remove();
        $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--processing__checked"></span>');
        $(this).prop('checked', true);
      });

      // RADIO shipping
      $('.dimdiv--shipping input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--shipping__checked"></span>');
      $('.dimdiv--shipping input').click(function() {
        $('.dimdiv--shipping input').prop('checked', false);
        $('.dimdiv--shipping span.dimdiv__checked').remove();
        $(this).parents('.dimdiv--shipping').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--shipping__checked"></span>');
        $(this).prop('checked', true);
      });

      // MATERIALI RADIO
      var matCheck = $('.material_class:checked').val();
      ctrlFRtoMaterial( matCheck );
      
      $('.dimdiv--circle .material_class:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--material__checked"></span>');
      
      $('.section-material:not(".section-disable")').on("click",'input[type="radio"]',function() {
        $('.dimdiv--circle .material_class').prop('checked', false);
        $('.dimdiv--circle span.dimdiv--material__checked').remove();
        $(this).parents('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--material__checked"></span>');
        $(this).prop('checked', true).trigger('change');
        ctrlFRtoMaterial( $('.material_class:checked').val() );
        $('.material_class').each(function() {
          $('#cert'+$(this).val()).css('display', 'none');
        });
        $('#cert'+$(this).val()).css('display', 'block');
      });


      // VISUALIZZAZIONE LAVORAZIONE FRONTE RETRO
      function ctrlFRtoMaterial( id ){    
        
        $.ajax({
          type: "POST",
          url: "/ctrlFRtoMaterial",
          data: { material_id: id },
          success: function( response ){
            
            if( response["FrontBack"] == true ){
              $('.section-fronteretro').show();
              $('.section-fronteretro #frontback_calc_type').val( response["FrontBackCalcType"] );
              $('.section-fronteretro #frontback_price').val( response["FrontBackPrice"] );
            } else {
              $('.section-fronteretro').hide();
              $('.section-fronteretro #frontback_calc_type').val('');
              $('.section-fronteretro #frontback_price').val('');
            }
          }
        });
      }
      
      
      
      // FRONTE RETRO RADIO      
      $('.adviced-fr').hide();
      $('#upi-0').hide();
      var nf = parseInt($('.change_number_files').html());
      $('.section-fronteretro .fr-png').on("click",function() {
        //console.log( $(this).parents('.dimdiv--fr').attr('class') );
        if( $(this).parents('.dimdiv--fr').hasClass('dimdiv--checked') == true ){
          $(this).parents('.dimdiv--fr').removeClass('dimdiv--checked');
          $('span.dimdiv--fr__checked').remove();
          $('.dimdiv--fr .fronteretro_class').prop('checked',true);
          $('.section-fronteretro .fr-png').css('border', '3px solid #333');
          $('.adviced-fr').hide();

          $('.change_number_files').html(nf);
          $('#upi-0').hide();

          $('#plus_file').val(0);


        } else {
          $(this).parents('.dimdiv--fr').addClass('dimdiv--checked');
          $(this).parents('.dimdiv--fr').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--operator-extra__checked dimdiv--fr__checked"></span>');
          $('.section-fronteretro .fr-png').css('border', '3px solid #ED7400');
          $('.dimdiv--fr .fronteretro_class').prop('checked',false);

          $('.adviced-fr').show();
          $('.extra_side').prop('checked',false);
        }
      });

      $('.extra_side').on("click", function () {    
        if ($(this).val() == 2) {
          $('#upi-0').show();
          $('.change_number_files').html(nf + 1);

          $('#plus_file').val(1);

        } else {
          $('.change_number_files').html(nf);
          $('#plus_file').val(0);
          $('#upi-0').hide();
        }
      });


      // accessories checkbox
      $('.carousel-accessories:not(".section-disable")').on("click",'input[type="checkbox"]',function() {
        if ($(this).prop('checked')) {
          $(this).closest('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--accessories__checked"></span>');
          $(this).closest('.text-center').find('.acc-qnty').show();

        } else {
          $(this).closest('.dimdiv--circle').find('span.dimdiv--accessories__checked').remove();
          $(this).closest('.text-center').find('.acc-qnty').hide();

        }
      });



      // CHECKBOX STRUTTURA
      $('.dimdiv--circle .baseaccessories_class:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--baseaccessories__checked"></span>');
      $('.dimdiv--circle .baseaccessories_class').click(function() {
        $('.dimdiv--circle .baseaccessories_class').prop('checked', false);
        $('.dimdiv--circle span.dimdiv--baseaccessories__checked').remove();
        $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--baseaccessories__checked"></span>');
        $(this).prop('checked', true);
      });



      $('.dimdiv--only input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--only__checked"></span>');
      $('.dimdiv--only-border img').click(function() {
        //console.log($(this).prev().is(':checked'));
        var input_checked = $(this).prev();
        if( input_checked.is(':checked') == true ){
          //console.log('oki');
          input_checked.prop('checked', false).trigger("change");
          $(this).siblings('span.dimdiv--only__checked').remove();
                
        } else {
          //console.log('iko');
          input_checked.prop('checked', true).trigger("change");
          $(this).parents('.dimdiv--only').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--only__checked"></span>');
        }
              
      });

      //se le dimensioni custom sono grandi devo aprire alert modal

      


      // CAROUSEL
      $('.carousel').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false                
              }
            },            
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false
              }
            }
        ]
      });

      $('.carousel-theme').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>"
      });

      $('.carousel-material').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false                
              }
            },            
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false
              }
            }
        ]
      });

      $('.carousel-lavorazioni').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false                
              }
            },            
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false
              }
            }
        ]
      });

      $('.top-carousel').slick({
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
        dots: true,
        autoplay: true
      });

      $('.carousel-related').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-down fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-up fa-2x'></span>",
        vertical: true
      });

      $('.carousel-accessories').slick({
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        pauseOnHover: false,
        nextArrow: "<span class='fa fa-caret-right fa-2x'></span>",
        prevArrow: "<span class='fa fa-caret-left fa-2x'></span>",
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: false                
              }
            },            
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: false
              }
            }
        ]
      });


      // TOOLTIP
      
      if( $(window).width() > 1024 ){
        $("[data-toggle='tooltip']").tooltip();
      } else {
        $("[data-toggle='tooltip']").tooltip('disable');
      }
      


      //aggiungio ai preferiti
      $('#add-prefer').click(function() {
        var form_id = $(this).data("form-id");
        var form_type = $(this).data("form-type");

        $.ajax({
          url: "/add-wishlist",
          data: {
            form_id: form_id,
            form_type: form_type
          },
          dataType: "html",
          type: "POST",
          success: function( response ) {                      
                $('#add-prefer').css("display", "none");
                //alert("Prodotto aggiunto ai preferiti");
                showModal("alertModal", Drupal.t("add prefer"),Drupal.t("product added to favorites") );
          }
        });

      });



      // mi serve sapere quale upload image Ã¨ stato selezionato
      $('.custom-file-theme').on('click', function (){
        var id = $(this).attr('id').replace('custom-file-theme-', '');    
        //al click si apre una modale (che Ã¨ unica)...per sapere a quale upload image corrisponde inserisco un campo con l'id del chiamante
        $('#exampleModal').attr('data-upi', id);
      });

      // IMAGE GALLERY che appende l'immagine scelta in zona upload
      $('#modal-save').click(function() {
        var imgId = $('.theme-input:checked').val();
        var img = $('#theme_img-' + imgId).attr('src');
        var upi = $('#exampleModal').data('upi');
        //console.log( 'save upi: ' + upi );
        $('#exampleModal').modal('hide');
        $('#upi-'+upi+' .galleryimage--choose, #upi-'+upi+' .uploadproductimage__remove').remove();//nel caso di modifica elimino img caricata precedentemente
        $('#upi-'+upi).append('<img id="load-img-'+ upi + '-' + imgId +'" class="galleryimage--choose" src="' + img + '" /><a class="uploadproductimage__remove"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');
        $('#upi-' + upi + ' .fileuploader').hide();
        $('#upi-'+upi+' .gallery_image_fid').val(imgId);
        $('.dimdiv--gallery__checked').remove();
        //var elemID = $(this).parents('.modal').attr('id');
        //$('a[data-target="#' + elemID + '"]').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--gallery__checked"></span>');

        //modifico il bottone EDITOR e modifico il testo del bottone tema
        //$('#upi-'+upi+' .custom-file-editor, #upi-'+upi+' input[type="file"]').hide();
        //$('#upi-'+upi+' input[type="file"]').hide();
        $('#upi-'+upi+' .custom-file-theme').text(Drupal.t('change theme'));
        $('#upi-'+upi+' .custom-file-editor').html('<i class="fa fa-paint-brush"></i> ' + Drupal.t('change theme with editor'));
        //rimuovo il data-upi per la successiva modale
        $('#exampleModal').removeData( "upi" );  
        $('#custom-file-editor-'+upi).click();
      });

      $('.btn-white').on('click', function() {
         var id = $('.uploadproductimage--selected').attr('id').replace('upi-', '');
         //console.log(!$.isNumeric($("#editor_fid_"+id).val()) +" "+ $.isNumeric($('#gallery_fid_'+id).val()))
          if (!$.isNumeric($("#editor_fid_"+id).val()) && $.isNumeric($('#gallery_fid_'+id).val()))
            $('.uploadproductimage__remove').click();
      });
              
      $('.uploadproductimage').on('click', '.uploadproductimage__remove', function () {
        var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');
        //console.log( 'remove id: ' + id );
        $('#upi-' + id + ' .fileuploader').show();
        $('#upi-'+id+' img').remove();
        $('#upi-'+id).removeClass('uploadproductimage--selected');
        $('#upi-'+id+' .uploadproductimage__remove').remove();
        $('.dimdiv--theme__checked').remove();
        $('.dimdiv--theme .theme-input').prop('checked',false);
        $('#upi-' + id + ' .gallery_image_fid').val('');
        //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
        $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
        $('#upi-'+id+' .custom-file-editor').show().html('<i class="fa fa-paint-brush"></i> ' + Drupal.t('choose editor')); 
        //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
        $('#image_'+id).show();
      });

      $('.uploadproductimage').on('click', '.uploadproductimage__remove2', function () {
        var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

        var fid = jQuery("#image_fid_"+id).val();

        $.post( '/remove_image', {
          'fid': fid
        }, function() {
          $('#upi-' + id + ' .fileuploader').show();
          $('#upi-'+id+' img').remove();
          $("#image_fid_"+id).val('');
          $('#upi-'+id).removeClass('uploadproductimage--selected');
          $('#upi-'+id+' .uploadproductimage__remove2').remove();
          $('.dimdiv--theme__checked').remove();
          $('.dimdiv--theme .theme-input').prop('checked',false);
          //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
          $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
          $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
          //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
          $('#image_'+id).show();
          var loaded= $('#loaded').val();
          loaded--;
                    
          $('#loaded').val(loaded);
          if ($('#beforenoon').val() == 1) {
              $('.d0').hide();
              $('.dx').show();
              if ($('.d0').hasClass("selected") || document.getElementById('date0').childNodes.length > 5) {
                  $('#d1 .date-div input').click();
                  $('#q1-d1').click();
              }
          }
        })
        .fail(function() {
          $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
        });

      });

      $('.uploadproductimage').on('click', '.uploadproductimage__remove3', function () {
        var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

        var fid = jQuery("#editor_fid_"+id).val();
        var previewfid = jQuery("#editor_fid_"+id).data('preview');
        
        $.post( '/remove_image', {
          'fid': fid,
          'previewfid': previewfid
        }, function() {
          $('#upi-' + id + ' .fileuploader').show();
          $('#upi-'+id+' img').remove();
          $("#editor_fid_"+id).val('');
          $("#editor_fid_"+id).attr('data-preview','');
          $('#upi-'+id).removeClass('uploadproductimage--selected');
          $('#upi-'+id+' .uploadproductimage__remove3').remove();
          $('.dimdiv--theme__checked').remove();
          $('.dimdiv--theme .theme-input').prop('checked',false);
          //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
          $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
          $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
          //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
          $('#image_'+id).show();
          var loaded= $('#loaded').val();
          loaded--;
                    
          $('#loaded').val(loaded);
          if ($('#beforenoon').val() == 1) {
              $('.d0').hide();
              $('.dx').show();
              if ($('.d0').hasClass("selected") || document.getElementById('date0').childNodes.length > 5) {
                  $('#d1 .date-div input').click();
                  $('#q1-d1').click();
              }
          }
        })
        .fail(function() {
          $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
        });

      });
      
      /*console.log($('#relatedpanel').height());
      if ($('#relatedpanel').height() === null) {
        $('#panel-file').scrollToFixed({
            marginTop: $('#summary-cart--product').height() + 30, 
            limit: $($('.blue-bg')).offset().top - $('#summary-cart--product').height() - $('#relatedpanel').height()
        });
      }
      else {
        $('#panel-file').scrollToFixed({
            marginTop: $('#summary-cart--product').height() + $('#relatedpanel').height(), 
            limit: $($('.blue-bg')).offset().top - $('#summary-cart--product').height() - $('#relatedpanel').height()
        });
      }
      $('#relatedpanel').scrollToFixed({
        marginTop: $('#summary-cart--product').height(), 
        limit: $($('.blue-bg')).offset().top - $('#summary-cart--product').height(),
        preFixed: function() { $('#panel-file').css('top', $('#summary-cart--product').height() + 90 + $('#relatedpanel').height()); }
      });*/
      
      
      var wWindow = $(window).width();        
      if( wWindow > 767 ){

        $('.followcart').scrollToFixed({
          marginTop: 30, 
          limit: $('.blue-bg').offset().top - $('#summary-cart--product').height()
        });
      }
      

      //modifica FRANCESCA per disabilitare il form grande formato se viene selezionato SOLO STRUTTURA della sezione struttura
      $('.section-struttura .label-structure').on('click', function (){
        //console.log( $('.section-struttura .label-structure input:checked').attr('id') );
        var section_struttura_id = $('.section-struttura .label-structure input:checked').attr('id');
        
        if( section_struttura_id == 'only_structure'){
            if (!$('.order-view-top').hasClass('section-disable'))
                $('.order-view-top:not(".section-struttura")').addClass('section-disable').append('<div class="overlay-disable"></div>');
            $('.section-quantity').removeClass('section-disable');
            $('.section-shipping-date').removeClass('section-disable');
            $('.section-quote').removeClass('section-disable');
        }
        else {
          $('.order-view-top').removeClass('section-disable');
          $('.overlay-disable').remove();
        }
      });


      function checkdate(date) {
          if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(date))
            return false;
        else {
            if (date == "00-00-0000")
                return false;
            if (date == "0")
                return false;
            
            return true;
        }
      }
      /*
      * INIZIO FUNZIONI DI CARRELLO
      */

      
      //aggiungi al carrello
      var numBigProceed = 0;
      $('#bigproceed').on('click', function (event){
        event.preventDefault();
        //console.log( 'form valid: ' + $("#product-big-format").valid() );
        numBigProceed++;
        if( $("#product-big-format").valid() && checkdate($("#shipping_date").val()) && numBigProceed <= 1){
          add_cart();
        }

      });
      
      var numBigProceed2 = 0;
      $('#bigproceed2').on('click', function (event){
        event.preventDefault();
        //console.log( 'form valid: ' + $("#product-big-format").valid() );
         numBigProceed2++;
        if( $("#product-big-format").valid() &&  checkdate($("#shipping_date").val()) && numBigProceed2 <= 1){
          add_cart();
        }

      });
        
/*
        $('#bigproceed').off("click.mePrint");
        $('#bigproceed').on("click.mePrint", function(event){
            event.preventDefault();
            if( $("#product-big-format").valid() && checkdate($("#shipping_date").val())){
              add_cart();
            }
        });
        
        $('#bigproceed2').off("click.mePrint");
        $('#bigproceed2').on("click.mePrint", function(event){
            event.preventDefault();
            if( $("#product-big-format").valid() && checkdate($("#shipping_date").val())){
              add_cart();
            }
        });
        $(document).off('click', '#bigproceed').on('click', '#bigproceed',function(event) {
            event.preventDefault();
            if( $("#product-big-format").valid() && checkdate($("#shipping_date").val())){
              add_cart();
            }
        }); 
        $(document).off('click', '#bigproceed2').on('click', '#bigproceed2',function(event) {
            event.preventDefault();
            if( $("#product-big-format").valid() && checkdate($("#shipping_date").val())){
              add_cart();
            }
        }); */


      function add_cart() {


        var data = new FormData();

        var upload_images_fid = [];
        var gallery_images_fid = [];
        var editor_images_fid = [];
        var editor_preview_fid = new Array();
        
        $('.upload_image_fid').each(function(i, obj) {

          if ($(obj).val() != '') {
            //alert($(obj).val());
            upload_images_fid.push($(obj).val());
          }
        });

        $('.editor_image_fid').each(function (i, obj) {

            if ($(obj).val() != '') {
                var fid = $(obj).val();
                editor_images_fid.push(fid);
                editor_preview_fid[fid] = $(obj).data('preview');
            }
            
        });
        
        var product_type = $("#type").val();
        var product_id = $("#productid").val();
        var material_id = $(".material_class:checked").val();
        var dimension_id = $('input[name=dimensions]:checked').val();
        var height = $("#productHeight").val();
        var width = $("#productWidth").val();
        var material_cost = $("#material_cost").val();
        var processing_id = $('input[name*=baseprocessing]:checked').val();
        var processing_cost = $("#processing_cost").val();
        var structure_id = $('input[name=baseaccessories]:checked').val();
        var structure_cost = $("#structure_cost").val();
        var longterm_id = $('input[name=lngtmprdctn]:checked').val();
        var shipping_date = $("#shipping_date").val();
        //console.log(shipping_date);
        var quantity = $("#productQuanity").val();
        var operator_review = $('input[name=operatorreview]:checked').val();
        var frontback = $('input[name=fronteretro]:checked').val();
        //var anonimous_pack = $('input[name=anonshipping]:checked').val();
        var name = $("#name").val();
        var note = $("#note").val();
        var total_price = $("#total_cost").val();
        var promo_value = $("#promo_value").val();
        var plus_file = $("#plus_file").val();

        //DATI LAVORAZIONE BASE//
        var baseisExtraRight = 0;
        if($("#baseisExtraRight").is(':checked')){
          baseisExtraRight = 1;
        }
        var baseisExtraLeft = 0;
        if($("#baseisExtraLeft").is(':checked')){
          baseisExtraLeft = 1;
        }
        var baseisExtraTop = 0;
        if($("#baseisExtraTop").is(':checked')){
          baseisExtraTop = 1;
        }
        var baseisExtraBottom = 0;
        if($("#baseisExtraBottom").is(':checked')){
          baseisExtraBottom = 1;
        }
        var baseextradef = $("#extradefValue").val();
        var baseDistanceSpecial = $("#base_distance_special_"+processing_id).val();
        var baseNumberSpecial = $("#base_number_special_"+processing_id).val();

        var processing_data = {
          top:baseisExtraTop, 
          bottom:baseisExtraBottom, 
          right:baseisExtraRight, 
          left:baseisExtraLeft, 
          def_value:baseextradef, 
          distance_special: baseDistanceSpecial, 
          number_special: baseNumberSpecial
        };
        //FINE DATI LAVORAZIONE BASE//



        //LAVORAZIONI ACCESSORIE SELEZIONATE
        var extrslct = $('#extrslct').val();
        var extprcsids = "";
        if (typeof extrslct !== "undefined" && extrslct != '') {
          if (extrslct.indexOf("+") > -1) {
            extprcsids = extrslct.split("+");
          } else {
            extprcsids = [extrslct];
          }
        }

        var extraprocessing = new Array();

        for (i = 0; i < extprcsids.length; i++) {

          var isextraright = 0;
          if($("#isextraright_" + extprcsids[i]).is(':checked')){
            isextraright = 1;
          }
          var isextraleft = 0;
          if($("#isextraleft_" + extprcsids[i]).is(':checked')){
            isextraleft = 1;
          }
          var isextratop = 0;
          if($("#isextratop_" + extprcsids[i]).is(':checked')){
            isextratop = 1;
          }
          var isextrabottom = 0;
          if($("#isextrabottom_" + extprcsids[i]).is(':checked')){
            isextrabottom = 1;
          }
          var extradef = $("#epextradefValue" + extprcsids[i]).val();
          var extra_cost = $("#extprcs_cost_" + extprcsids[i]).val();
          var distance_special = $("#distance_special_" + extprcsids[i]).val();
          var number_special = $("#number_special_"+extprcsids[i]).val();

          extraprocessing[i] = {
            id:extprcsids[i], 
            top:isextratop, 
            bottom:isextrabottom, 
            right:isextraright, 
            left:isextraleft, 
            distance_special: distance_special, 
            number_special: number_special, 
            def_value: extradef, 
            cost: extra_cost
          };
        }

        //FINE LAVORAZIONI ACCESSORIE


        //ACCESSORI

        // ACCESSORI SELEZIONATI
        var extraccslct = $('#extraccslct').val();
        var extaccsids = "";
        if (typeof extraccslct !== "undefined" && extraccslct !== '') {
          if (extraccslct.indexOf("+") > -1) {
            extaccsids = extraccslct.split("+");
          } else {
            extaccsids = [extraccslct];
          }
        }
        var accessories_arr = new Array();

        for (i = 0; i < extaccsids.length; i++) {
          var extraaccqnty = $("#extraaccessories-qnty_"+extaccsids[i]).val();
          var extraaccprice = parseFloat($("#extacc"+extaccsids[i]).val());

          accessories_arr[i] = {
            id:extaccsids[i], 
            quantity:extraaccqnty ,
            cost:extraaccprice
          };
        }
        //FINE ACCESSORI

        var post_data = {
          product_type: product_type,
          product_id: product_id,
          material_id: material_id,
          dimension_id: dimension_id,
          height: height,
          width: width,
          material_cost: material_cost,
          processing_id: processing_id,
          frontback: frontback,
          processing_cost: processing_cost,
          processing_data: JSON.stringify(processing_data),
          structure_id: structure_id,
          structure_cost: structure_cost,
          longterm_id: longterm_id,
          shipping_date: shipping_date,
          quantity: quantity,
          operator_review: operator_review,
          //anonimous_pack: anonimous_pack,
          name: name,
          note: note,
          total_price: total_price,
          promo_value: promo_value,
          extra_processings: extprcsids,
          extra_processing_arr: JSON.stringify(extraprocessing),
          accessories: extaccsids,
          accessories_arr: JSON.stringify(accessories_arr),
          upload_images_fid: JSON.stringify(upload_images_fid),
          gallery_images_fid: JSON.stringify(gallery_images_fid),
          editor_images_fid: JSON.stringify(editor_images_fid),
          editor_preview_fid: JSON.stringify(editor_preview_fid),
          plus_file: plus_file
        };


        for (var k in post_data){
          data.append(k, post_data[k]);
        }

        //console.log( data );

        $.ajax( {
          type: "POST",
          url: "/add-cart-big",
          processData: false,
          contentType: false,
          data:data,
          dataType: "json",
          success: function( response ) {
            if(response == 'date-error'){                          
              showModal("alertModal", Drupal.t("Shipping Date"),Drupal.t("date of shipment is not allowed")+ '!' );                          
            }else{
              window.location.replace("/carrello");
            }
          }
        });
      }

      //var frameSrc = "/login";

      $('.btn-editor').click(function(event){
         event.preventDefault();
        //$('#myModal').on('show', function () {
        //$('iframe').attr("src",frameSrc);
        //});
        //$('#myModal').modal({show:true});
        var dimensionFinder = $('input[name=dimensions]:checked').val();
        var productheight = $('#productHeight').val();
        var productwidth = $('#productWidth').val();
              
        var producthewi= productheight * productwidth;
        if ((dimensionFinder != null) && (dimensionFinder == "custom") && ((productwidth === "" || productheight === "") || (producthewi > 250000))) {   
            $('#erroreditor').modal({show:true});
        }
        else {
            $('#editorModal').modal({show:true});
            var id = $(this).attr('id');
            var id_arr = id.split("-");

            var id_num = id_arr[3];

            var gallery_img_id = $('.galleryimage--choose').attr('id');
            if(gallery_img_id != null){
                var gallery_img_id_arr = gallery_img_id.split("-");
                var gallery_fid = gallery_img_id_arr[3];
            }
            
            var dim_dets = "";
            var dimension_details = 0 + "*" + 0;

            if (productheight === "") {
                productheight = 1;
            }

            if (productwidth === "") {
                productwidth = 1;
            }

            if ((dimensionFinder != null) && (dimensionFinder != "custom")) {
                dim_dets = "#bd" + dimensionFinder;
                dimension_details = $(dim_dets).val();
                var res = dimension_details.split("*");
                productwidth = res[0];
                productheight = res[1];
                
                //mask_file
                var mask_file_det = "#bd_mask_file_" + dimensionFinder;
                var mask_file_fid = $(mask_file_det).val();
                
            }
        
            //Setup editor
            var editor_dpi = $('#editor_dpi').val();
            var editor_colors_type = $('#editor_colors_type').val();
            var editor_cutting_grid = $('#editor_cutting_grid').val();
            var editor_colors = $('#editor_colors').val();
        
            editorval['min_image_dpi_y'] = editor_dpi;
            editorval['min_image_dpi_x'] = editor_dpi;
            editorval['colors_type'] = editor_colors_type;
            editorval['cutting_grid'] = editor_cutting_grid;
            editorval['colors'] = editor_colors;

            editorval['product_width'] = productwidth;
            editorval['product_height'] = productheight;
            
            //use external image (per prespaziati)
            var use_external_image = $('#use_external_image').val();
            editorval['external_image'] = use_external_image;
            
            //mask_file
            editorval['mask_file'] = mask_file_fid;
            
            editorval['save_file_id'] = id_num;

            if(gallery_fid !== ""){
               editorval['gallery_fid'] = gallery_fid;
            }
            
            var preview_fid = 0;
            $.ajax({
                type: "POST",
                url: "/getPreview",
                data: { gallery_fid: gallery_fid },
                success: function( response ){
                    console.log(response);
                    preview_fid= response["preview"];
                    console.log(preview_fid);
                },
                complete: function() {
                    if(preview_fid !== ""){
                        editorval['preview_fid'] = preview_fid;
                    }
                    else{
                        editorval['preview_fid'] = "";
                    }
                    
                    iRefreshEditor(editorval);
                }
            });
            

            //console.log(editorval);
            
            //var source='/publishereditor';
            //$('#editorModal .publisher-editor').attr('src', source);
            //var x = document.getElementById("editorModalFrame");
            //var y = (x.contentWindow || x.contentDocument);
            //if (y.document)y = y.document;
            //var input = y.getElementById('refresh_trick');
            //console.log(input);
            
        }
      });

      $('.fileuploader').each(function(i, obj) {
          
          var divID = $(obj).attr('c');
          
          $(obj).uploadFile({
                dragDrop: false,
                showAbort: true,
                url:'/save_image',
                showDelete: false,
                showFileCounter:false,
                returnType: "json",
                fileName:"uploaded_file",
                onSubmit:function(files)
                {
                  //nascondo il bottone EDITOR e modifico il testo del bottone tema
                  $('#custom-file-theme-'+divID+', #custom-file-editor-'+divID).hide();
                },
                onSuccess: function(files,response,xhr,pd)
                {
                    
                    
                    $('#upi-'+divID).addClass('uploadproductimage--selected');

                    $("#image_fid_"+divID).val(response.fid);

                    $(".ImageError_"+divID).html("");

                    var extens = response.url.substr(response.url.length - 3);
                    if (extens == "pdf")
                        $(".dvPreview_"+divID).html('<img style="width:200px" src="/sites/default/files/pdf_icon.png" class="uploadproductimage--choose" ><a class="uploadproductimage__remove2"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');
                    else    
                        $(".dvPreview_"+divID).html('<img style="width:200px" src="'+ response.url+'" class="uploadproductimage--choose" ><a class="uploadproductimage__remove2"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');

                    $('#image_'+divID).hide();
                    $('#upi-'+divID + ' .ajax-file-upload-statusbar').remove();
                    var loaded= $('#loaded').val();
                    loaded++;
                    
                    $('#loaded').val(loaded);
                    if (loaded == $('#imagecount').val()) {
                        $('.d0').show();
                        $('.dx').hide();
                    }
                  //pageObject.addPicture(new PictureBox(pageObject, 5, 5, response.width, response.height, response.url, response.fid));
                },
                onError: function(files,status,errMsg,pd)
                {
                    $('#image_'+divID).val("");
                    $('#image_'+divID).show();                              
                    showModal("alertModal", Drupal.t("error"), Drupal.t("error loading image"));
                }
                /*customProgressBar: function(obj,s)
		{
                this.statusbar = $("<div class='custom-statusbar'></div>");
                this.filename = $("<div class='custom-filename'></div>").appendTo(this.statusbar);
                this.progressDiv = $("<div class='custom-progress'>").appendTo(this.statusbar).hide();
                this.progressbar = $("<div class='custom-bar'></div>").appendTo(this.progressDiv);
                this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
                this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
                this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
                this.download = $("<div>" + s.downloadStr + "</div>").appendTo(this.statusbar).hide();
                this.del = $("<div>" + s.deletelStr + "</div>").appendTo(this.statusbar).hide();

                this.abort.addClass("custom-red");
                this.done.addClass("custom-green");
                this.download.addClass("custom-green");            
                this.cancel.addClass("custom-red");
                this.del.addClass("custom-red");
                
                            return this;

                    }*/
            });
      });
      
      function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
              
      function showModal(id,title,content){
        $('#'+id+' .modal-title').html(title);
        $('#'+id+' .modal-body').html( capitalizeFirstLetter(content) );
        $('#'+id).modal('show');
      }
            
        
      //$('#d2 .date-div input').click();
      //updateValues();
    }
  };
})(jQuery);
