(function ($) {
    
    Drupal.behaviors.price_small = {
        attach: function (context, settings) {
            
        
        var prices_base = [];
        var prices_frontback = [];
        var prices_processing = [];
        var prices_folding = [];
        var prices_coverbook = [];


                function setTablePrices() {

                    var promotionalflag = $('#promotionalFlag').val();
                    var promotype = $('#promotionalType').val();
                    var promoval = $('#promotionalValue').val();
                    
                
                    //ciclo le quantità predefinite
                    $('.fixed-amounts').each(function () {

                        var quantity = $(this).data("value");
                        var totalPrice_divided = prices_base[quantity];
                        
                        
                        var isBook = $("#isBook").val();
                        if (isBook) {
                            var nFacciate = $('#faceQuantity').val();
                            
                            var copertina = $('input[name=copertina]:checked').val();
                        
                            if (copertina == 2) {

                                var weight_id = $('input[name=custom_grammatura]:checked').val();
                                var material_id = $('input[name=custom_cover]:checked').val();

                                if (weight_id && material_id) {
                                    nFacciate = nFacciate - 4;
                                    totalPrice_divided += prices_coverbook[quantity];
                                    
                                }
                            }
                            
                            //console.log(totalPrice_divided);
                            
                            //nFacciate = nFacciate - 4;
                            totalPrice_divided = totalPrice_divided * (nFacciate / 4);
                            
                            //console.log(totalPrice_divided);
                           

                            //console.log(nFacciate + ' - ' + copertina + ' - ' + grammatura + ' - ' + materiale);

                        }
                        
                        console.log(prices_frontback);
                        if (quantity in prices_frontback) {
                            totalPrice_divided += parseFloat(prices_frontback[quantity]);
                            //lavorazioni_accessorie = lavorazioni_accessorie * amount;
                        }

                        if (quantity in prices_folding) {
                            totalPrice_divided += parseFloat(prices_folding[quantity]);
                            //lavorazioni_accessorie = lavorazioni_accessorie * amount;
                        }

                        $('.lavorazioni_class').each(function (index, element) {
                            if ($(element).is(':checked') && $(element).val() in prices_processing && quantity in prices_processing[$(element).val()]) {
                                totalPrice_divided += parseFloat(prices_processing[$(element).val()][quantity]);
                            }
                        });

                        var totalPrice_finito = totalPrice_divided * quantity;
                        
                        var startUpCost = $('#bookPrintingStartupCost').val();
                        totalPrice_finito += parseFloat(startUpCost);
                        
                        
                        if ($('#lessthan').val() == 1)
                            totalPrice_finito = parseFloat(totalPrice_finito) + (parseFloat(totalPrice_finito) * parseInt($("#incr_percentage").val()) / 100);
                        
                       
                        if (promotionalflag.toLowerCase() == "promo") {
                            if (promotype.toLowerCase() == "value") {
                                totalPrice_finito = parseFloat(totalPrice_finito) - parseFloat(promoval);
                            } else
                            if (promotype.toLowerCase() == "percentage") {
                                promoval2 = ((parseFloat(totalPrice_finito) / 100) * parseFloat(promoval));
                                totalPrice_finito = parseFloat(totalPrice_finito) - parseFloat(promoval2);

                            }
                        }

                        var discount = $(this).data("discount");
                        if ((discount == "") || (discount == null))
                            discount = 0;

                        var ci_discount = ((parseFloat(totalPrice_finito) / 100) * parseFloat(discount));
                        var totalPrice_netto = parseFloat(totalPrice_finito) - parseFloat(ci_discount);
                        var ci_molt_tot_first = totalPrice_netto; // * quantity;

                        
                        
                        $("#total-q" + quantity + "-lngtm0").html(parseFloat(ci_molt_tot_first).toFixed(2));

                        //CICLO LE DATE SUCCESSIVE PER AGGIORNARE I PREZZI
                        $('.lngtmnpt').each(function () {
                            var value = $(this).prop('value');
                            var id = $(this).prop('id');

                            if ((value !== "") && (value !== null)) {

                                var ci_lngtrmprdctnvl = value.split("+");
                                var ci_lantrmamnt = ci_lngtrmprdctnvl[1];
                                var ci_lantrmtype = ci_lngtrmprdctnvl[2];

                                if (ci_lantrmtype === "") {
                                    ci_lantrmtype = "value";
                                }
                                if (ci_lantrmamnt === "") {
                                    ci_lantrmamnt = 0;
                                }
                            }

                            if (ci_lantrmtype.toLowerCase() == "value") {
                                var ci_tot = parseFloat(totalPrice_finito) - parseFloat(ci_lantrmamnt);
                            }
                            else if (ci_lantrmtype.toLowerCase() == "percentage") {
                                var ci_lngtrmdsc = ((parseFloat(totalPrice_finito) / 100) * parseFloat(ci_lantrmamnt));
                                var ci_tot = parseFloat(totalPrice_finito) - parseFloat(ci_lngtrmdsc);
                            }

                            var ci_lantrm_discount = ((parseFloat(ci_tot) / 100) * parseFloat(discount));
                            var totalPrice_lantrm__netto = parseFloat(ci_tot) - parseFloat(ci_lantrm_discount);

                            var ci_molt_tot = totalPrice_lantrm__netto; // * quantity;
                            $("#total-q" + quantity + "-" + id).html(ci_molt_tot.toFixed(2));
                        });
                    });
                }

                function setPrice(promoval, promoprice, extraaccessories, processingCost, 
                                    frontbackCost, singleproductPrice, 
                                    operatorreview, 
                                    totlPrice, noOfProducts){

                                    //console.log(frontbackCost);
                    $('.productcostdiv').html(parseFloat(singleproductPrice).toFixed(2));
                    $('.totalnoofproductbigfrmtdiv').html(noOfProducts);

                    if (extraaccessories != 0) {
                        $('.extraaccessoriestd').show();
                        extraaccessories = parseFloat(extraaccessories).toFixed(2);
                        $('.extraacccstdiv').html(extraaccessories);
                    } else {
                        $('.extraaccessoriestd').hide();
                    }

                    if (promoprice != 0 && promoprice != totlPrice) {
                        $('.promopricetd').show();
                        totlPrice = parseFloat(totlPrice).toFixed(2);
                        $('.promopricediv').html(totlPrice);
                        totlPrice = promoprice;
                    } else {
                        $('.promopricetd').hide();
                    }

                    if (promoval != 0 || promoprice != totlPrice) {
                        $('.promovaltd').show();
                        promoval = parseFloat(promoval).toFixed(2);
                        $('.promovaldiv').html("-" + promoval);
                        if (promoval == 0)
                            $('.promovaldiv').html("-" +(totlPrice - promoprice).toFixed(2));
                        
                    } else {
                        $('.promovaltd').hide();
                    }

                    var extraprocssing = (frontbackCost + processingCost) * noOfProducts;
                    if (extraprocssing != 0) {
                        $('.extraprocessingtd').show();
                        extraprocssing = parseFloat(extraprocssing).toFixed(2);
                        $('.extraprocessingdiv').html(extraprocssing);
                    } else {
                        $('.extraprocessingtd').hide();
                    }

                    if (operatorreview != 0) {
                        $('.operatorreviewtd').show();
                        operatorreview = parseFloat(operatorreview).toFixed(2);
                        $('.operatorreviewdiv').html(operatorreview);
                    } else {
                        $('.operatorreviewtd').hide();
                    }

                    totlPrice += parseFloat(extraaccessories);
                    totlPrice += parseFloat(operatorreview);

                    var startUpCost = $('#bookPrintingStartupCost').val();
                    totlPrice += parseFloat(startUpCost);

                    var al_iva = parseFloat($('#vat_iva').val()) / 100;

                    var iva = parseFloat(al_iva * totlPrice).toFixed(2);

                    var totlPriceIva = parseFloat(totlPrice) + parseFloat(iva);
                    totlPriceIva = parseFloat(totlPriceIva).toFixed(2);
                    totlPrice = parseFloat(totlPrice).toFixed(2);

                    $('.ivadiv').html(iva);
                    $('.totalproductcostdiv').html(totlPrice);
                    $('.totalcostdiv').html(totlPriceIva);
                    $('#total_cost').val(totlPrice);
                    $('#promo_value').val(promoval);
                    $('.bigtotlprice').val(totlPrice);

                    /**if (promoval != 0)
                        var promoship = parseFloat(promoprice) - parseFloat(promoval) - parseFloat(totlPrice) + parseFloat(operatorreview) +parseFloat(extraaccessories);
                    else
                        var promoship = 0;


                    if (promoship != 0) {
                        $('.promoshiptd').show();
                        promoship = parseFloat(promoship).toFixed(2);
                        $('.promoshipdiv').html("-" + promoship);
                    } else {
                        $('.promoshiptd').hide();
                    }
*/
                    $('.promoshiptd').hide();
                    $('#relatedpanel').css('top', $('#summary-cart--product').height() + 60);

                }



                //code starts

                /************* On Load **********************/
                // var shippingidj =  $('input[name=shipping]:checked').val();
                //  updateShipping(shippingidj);   
                // alert(shippingidj);
                updateSmallDates();
                updateFixedCosts();
                updateBookFixedCosts();


                if ($('#isFolding').val()) 
                    updateFoldingCosts();

                /* Le due funzioni sottostanti vengono richiamate direttamente da updateFixedCosts() */
                //updateSmallDates();
                //updateValues();

                /**************** On Load ********************/

                $('#fixed_costs').on("change", function () {

                    if ($(this).val() == 1) {
                        updateFixedCosts();
                        if ($('#isFolding').val()) 
                            updateFoldingCosts();
                    }
                });

                //////// PRICE TABLE FIXED //////////////////////////
                ///////////////////////////////////////////////

                $('#ship-table .ship-cell-fixed').click(function () {

                    var splittedResult = $(this).attr('id').split('-');
                    var day = splittedResult[1];
                    var quantity = splittedResult[0];

                    $('#ship-table .fa-check-circle').remove();
                    $('.ship-cell-fixed').removeClass('selected');
                    $(this).append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>').addClass('selected');
                    $('#ship-table th').removeClass('day-selected');
                    $('#' + day).addClass('day-selected');
                    $('.ship-col').removeClass('quantity-selected');
                    $('#' + quantity).addClass('quantity-selected');

                    $('.date-div input').prop('checked', false);
                    $('.date-div span.dimdiv__checked').remove();
                    $('#'+day).prop('checked', true);

                    var only_quantity = $("#"+quantity).data('value');

                    $("#productQuanity").val(only_quantity);

                    var day_n = day.substring(1, 2);
                    var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                    $("#shipping_date").val(date_txt);
                    $(".shippingdatediv").html(date_txt);

                    updateValues();

                });

                // stampa fronte-retro
                $(".section-fronteretro").change(function () {
                    if($('.adviced-fr').css('display') == "none") {
                        prices_frontback = [];
                        setTablePrices();
                        updateValues();
                    } else {
                        updateFrontBackCosts();
                    }
                });


                $('#operatorreview').click(function () {
                    if ($(this).prop('checked')) {
                        $(this).parents('.dimdiv--operator-extra').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--operator-extra__checked"></span>');
                        expchkd = $(this).prop("value");
                    }
                    else {
                        $(this).parents('.dimdiv--operator-extra').find('span.dimdiv--operator-extra__checked').remove();
                        expchkd = "";
                    }
                });
  
                $("input[name=operatorreview]").change(function () {
                    updateValues();
                });
            
                $("#faceQuantity").change(function () {
                    setTablePrices();
                    updateValues();
                });

                $("input[name=copertina]").change(function () {
                    updateBookFixedCosts();
                });
                
                $("input[name=custom_cover]").change(function () {
                    updateBookFixedCosts();
                });

                $("input[name=custom_grammatura]").change(function () {
                    updateBookFixedCosts();
                });
                
                ///////   ACCESSORIES QUANTITY NUMBER ///////////////
                $(".extraaccessories-qnty-Value").change(function () {

                    var value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////

                    if(value === "" || !$.isNumeric( value ) ){
                        //alert("Deve essere un numero!");
                        $(this).focus();
                    }else{
                        updateValues();
                    }

                });

                $(".lavorazioni_class").on('change', function () {

                    var id = $(this).val();

                    if ($(this).is(':checked')) {
                        updateProcessingCosts(id);
                    } else {
                        delete prices_processing[id];
                        updateValues();
                    }

                });

                /************* On change long term prouction start ******************/

                function getDataProd(mese, giorno, lingua) {

                    var monthNames = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
                    var dayNames = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];

                    var monthNamesFr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
                    var dayNamesFr = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

                    var meseOut = "";
                    var giornoOut = "";

                    if (lingua == "it") {
                        meseOut = monthNames[mese];
                        giornoOut = dayNames[giorno];
                    } else {
                        meseOut = monthNamesFr[mese];
                        giornoOut = dayNamesFr[giorno];
                    }

                    var result = [meseOut, giornoOut];

                    return result;

                }

                function updateSmallDates() {

                    var matdaysj = 0;
                    var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();
                    //var productgroupidj = $('#productgroupid').val();

                    var formidj = $('#formid').val();
                    var languageidj = $('#languageid').val();

                    var delytimj = 0;
                    var lngtmprdctnvals = 0;
                    var lngrmprdtnid = "#lngtm" + lngtmprdctn;
                    var lngtmprdctnvals = $(lngrmprdtnid).val();
                    if ((lngtmprdctn != "") && (lngtmprdctn != null)) {
                        var lngtrmprdctnvl = lngtmprdctnvals.split("+");
                        delytimj = lngtrmprdctnvl[0];
                    }

                    //var workloadtypej = $("#workloadtype").val();
                    //var workloadlimitj = $("#workloadlimit").val();
                    //var formtype = $("#formtype").val();
                    //if ((workloadlimitj == null) || (workloadlimitj == "")) {
                        //workloadlimitj = 0
                        //workloadtypej = "Products";
                    //}

                    //if (formtype == 'small') {

                        //var material_id_selected_name = $("#smaterialId option:selected").val();
                        //var material_prices = "#smtid" + material_id_selected_name;
                    //} else {
                    var material_id_selected_name = $(".material_class:checked").val();
                    var material_prices = "#mtid" + material_id_selected_name;
                    //}

                    var materialdet = $(material_prices).val();

                    if (materialdet != null) {
                        var matdetails = materialdet.split("+");
                        matdaysj = matdetails[4];
                    } else if (materialdet == null) {
                        matdaysj = 0;
                    }

                    var dates_path = $('input[name=dates_path]').val();

                    $.ajax({
                        url: dates_path,
                        data: {
                            mtrldys: matdaysj,
                            delytim: delytimj,
                            //workloadlimit: workloadlimitj,
                            //workloadtype: workloadtypej,
                            //productgroupid: productgroupidj,
                            formid: formidj,
                            languageid: languageidj
                        },
                        dataType: "html",
                        type: "POST",
                        success: function (data) {
                            var dates = data.split("**")
                            var datelength = dates.length;
                            datelength = parseFloat(datelength) - 1;
                            for (var i = 0; i < datelength; i++) {

                                var dates_count = $("#dates_count").val();
                                
                                var data_arr = dates[i].split("-");
                                var giorno_mese = data_arr[0];
                                var mese = data_arr[1];
                                var anno = data_arr[2];
                                
                                if (i == 0 && dates_count == 0) {

                                    $("#shipping_date").val(dates[i]);
                                    $(".shippingdatediv").html(dates[i]);
                                    var currentTime = new Date();
                                
                                    var frimon= 0;
                                    if (currentTime.getDay() == 5 && (isDate(currentTime.getDate(), giorno_mese, mese, anno, 3) || isDate(currentTime.getDate(), giorno_mese, mese, anno, 4)))
                                        frimon = 1;
                                
                                    if (currentTime.getDate() == giorno_mese || isDate(currentTime.getDate(), giorno_mese, mese, anno, -1) || isDate(currentTime.getDate(), giorno_mese, mese, anno, -2) || frimon == 1){
                                        $("#lessthan").val('1');
                                    }
                                }

                                var shipingdivid = "#shippingdateprintdiv" + i;

                                var d = new Date(anno, mese - 1, giorno_mese);

                                var giorno_sett = d.getDay();
                                var mese_from = d.getMonth();

                                var arr_date = getDataProd(mese_from, giorno_sett, languageidj);

                                var mese_html = arr_date[0];
                                var giorno_html = arr_date[1];

                                var html = "<div class='date-div__day'>" + giorno_html + "</div><div class='date-div__day-n'>" + giorno_mese + "</div><div class='date-div__month'>" + mese_html + "</div>";

                                $(shipingdivid).data("shippingdate", dates[i]);
                                $(shipingdivid).html(html);

                                $("#dates_count").val(i);
                                if(currentTime.getDate() == giorno_mese) {
                                    $('#beforenoon').val('1');
                                }
                            }
                            
                            if ($('#beforenoon').val() == "1") {
                                $('.d0').hide();
                                $('.dx').show();
                            }
                            else {
                                $('.dx').hide();
                                $('.d0').show();
                            }
                            var day_n = 0;
                            var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                            $("#shipping_date").val(date_txt);
                            $(".shippingdatediv").html(date_txt);
                            
                        },
                        complete: function() {
                            $('.dsel').click();
                            updateValues();
                        }
                    });
                }

                /************* On change long term prouction End ******************/
                function isDate(actualday, day, month, year, difference) {
                    if (difference != 0) {
                        day = parseInt(day) + parseInt(difference);
                        if (day <= 0) {
                            switch (month) {
                                case 1,3,5,7,8,10,12: day = 31 + day; break; 
                                case 4,6,9,11: day = 30 + day; break; 
                                case 2: 
                                    if (year % 4 == 0)
                                        day= 29 + day;
                                    else
                                        day= 28 + day;
                                break;
                            }
                        }
                        else {
                            if (day > 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12))
                                day = day - 31;
                            else if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11))
                                day = day - 30;
                            else if (day > 29 && month == 2 && year %4 == 0)
                                day = day - 29;
                            else if (day > 28 && month == 2 && year %4 != 0)
                                day = day - 28;
                        }
                    }
                    if (parseInt(actualday) == parseInt(day))
                        return true;
                    else
                        return false;
                }

                function updateFixedCosts() {
                    
                    var material_id = $(".material_class:checked").val();
                    var dimension_id = $(".dimension-input:checked").val();
                    var weight_id = $(".grammatura-input:checked").val();

                    var form_id = $('#formid').val();

                    $.ajax({
                        url: "/get-small-costs",
                        data: {
                            mat_id: material_id,
                            form_id: form_id,
                            dimension_id: dimension_id,
                            weight_id: weight_id
                        },
                        dataType: "json",
                        //processData: false,
                        //contentType: false,
                        type: "POST",
                        success: function (data) {

                            $('#fixed_costs').val(0);
                            prices_base = data;

                            setTablePrices();
                            updateValues();
                        },
                        error: function(data) {
                            //console.log("Error!");
                        }
                    });
                }

                function updateBookFixedCosts() {
                    //var fixed_costs_path = $('input[name=fixed_costs_path]').val();
                    prices_coverbook = [];
                    
                    var isBook = $("#isBook").val();
                    //alert(isBook);
                    if (isBook) {
                        
                        var copertina = $('input[name=copertina]:checked').val();
                        
                        if (copertina == 2) {
                            
                            var weight_id = $('input[name=custom_grammatura]:checked').val();
                            var material_id = $('input[name=custom_cover]:checked').val();
                            
                            if (weight_id && material_id) {
                            
                                var dimension_id = $(".dimension-input:checked").val();
                                var form_id = $('#formid').val();

                                $.ajax({
                                    url: "/get-small-costs",
                                    data: {
                                        mat_id: material_id,
                                        form_id: form_id,
                                        dimension_id: dimension_id,
                                        weight_id: weight_id
                                    },
                                    dataType: "json",
                                    //processData: false,
                                    //contentType: false,
                                    type: "POST",
                                    success: function (data) {

                                        prices_coverbook = data;

                                        setTablePrices();
                                        updateValues();
                                    },
                                    error: function(data) {
                                        //console.log("Error!");
                                    }
                                });
                            }
                        }
                    }
                }

                function updateFrontBackCosts() {
                    var dimension_id = $(".dimension-input:checked").val();
                    var form_id = $('#formid').val();

                    $.ajax({
                        url: "/get-frontback-costs",
                        data: {
                            form_id: form_id,
                            dimension_id: dimension_id
                        },
                        dataType: "json",
                        //processData: false,
                        //contentType: false,
                        type: "POST",
                        success: function (data) {

                            prices_frontback = data;
                            setTablePrices();
                            updateValues();
                        },
                        error: function(data) {
                            //console.log("Error!");
                        }
                    });
                }

                function updateProcessingCosts(processing_id) {

                    var dimension_id = $(".dimension-input:checked").val();
                    var form_id = $('#formid').val();

                    $.ajax({
                        url: "/get-processing-costs",
                        data: {
                            form_id: form_id,
                            dimension_id: dimension_id,
                            processing_id: processing_id
                        },
                        dataType: "json",
                        //processData: false,
                        //contentType: false,
                        type: "POST",
                        success: function (data) {

                            prices_processing[processing_id] = data;
                            setTablePrices();
                            updateValues();
                        },
                        error: function(data) {
                            //console.log("Error!");
                        }
                    });
                }

                function updateFoldingCosts() {

                    var dimension_id = $(".dimension-input:checked").val();
                    var piega_id = $(".piega-input:checked").val();
                    var form_id = $('#formid').val();

                    $.ajax({
                        url: "/get-folding-costs",
                        data: {
                            form_id: form_id,
                            dimension_id: dimension_id,
                            piega_id: piega_id
                        },
                        dataType: "json",
                        //processData: false,
                        //contentType: false,
                        type: "POST",
                        success: function (data) {


                            prices_folding = data;

                            setTablePrices();
                            updateValues();
                        },
                        error: function(data) {
                            //console.log("Error!");
                        }
                    });
                }

                /**********Multiplication Formula for Big Format Start *************/

                function updateValues() {

                    //CODICE PER IL CALCOLO DEI PREZZI DELLA TABELLA DELLE DATE DI SPEDIZIONE //

                    //CALCOLO PER LA TABELLA CON DIMENSIONI PREDEFINITE

                    var noOfProducts = $('#productQuanity').val();
    //alert(noOfProducts);
                    var promoval = 0;
                    var operatorreview = $('input[name=operatorreview]:checked').val();
                    var promoprice = $('#promotionalValue').val();
                    var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();     
                    var lantrmdays = 0;
                    var lantrmamnt = 0;
                    var lantrmtype = "value";
                    var basePrice = prices_base[noOfProducts];
    //alert(basePrice);                

                    var isBook = $("#isBook").val();
                    if (isBook) {
                        var nFacciate = $('#faceQuantity').val();

                        var copertina = $('input[name=copertina]:checked').val();

                        if (copertina == 2) {

                            var weight_id = $('input[name=custom_grammatura]:checked').val();
                            var material_id = $('input[name=custom_cover]:checked').val();

                            if (weight_id && material_id) {
                                nFacciate = nFacciate - 4;
                                basePrice += prices_coverbook[noOfProducts];

                            }
                        }

                        //console.log(totalPrice_divided);

                        //nFacciate = nFacciate - 4;
                        basePrice = basePrice * (nFacciate / 4);

                        //console.log(totalPrice_divided);



                        //console.log(nFacciate + ' - ' + copertina + ' - ' + grammatura + ' - ' + materiale);

                    }

                    var frontbackCost = parseFloat(0);
                    if (noOfProducts in prices_frontback) {
                        frontbackCost = prices_frontback[noOfProducts];
                        //lavorazioni_accessorie = lavorazioni_accessorie * amount;
                    }

    //console.log("FBCOST:" + frontbackCost);  

                    var foldingCost = parseFloat(0);
                    if (noOfProducts in prices_folding) {
                        foldingCost = parseFloat(prices_folding[noOfProducts]);
                        //lavorazioni_accessorie = lavorazioni_accessorie * amount;
                    }

    //alert(foldingCost);  

                    var processingCost = parseFloat(0);
                    $('.lavorazioni_class').each(function (index, element) {
                        if ($(element).is(':checked') && $(element).val() in prices_processing && noOfProducts in prices_processing[$(element).val()]) {
                            processingCost += prices_processing[$(element).val()][noOfProducts];

                            var processing_id = $(element).val();
                            var price_lavorazione = parseFloat(0);
                            if (noOfProducts in prices_processing[processing_id]) {
                                price_lavorazione = parseFloat(prices_processing[processing_id][noOfProducts]).toFixed(2);
                            }
                            $('#lavorazioni'+processing_id).data('price', price_lavorazione);
                        }
                    });

    //alert(processingCost);

                    var singleproductPrice = parseFloat(basePrice) + parseFloat(processingCost) + parseFloat(frontbackCost) + parseFloat(foldingCost);

    //alert(singleproductPrice);                   

                    var totlPrice = singleproductPrice * noOfProducts;
                    if ($('#lessthan').val() == 1)
                        totlPrice = parseFloat(totlPrice) + (parseFloat(totlPrice) * parseInt($("#incr_percentage").val()) / 100);
                    
                    if ($('#promotionalType').val() == "percentage")
                        promoval= (totlPrice * promoprice / 100)
                    else if ($('#promotionalType').val() == "value")
                        promoval= promoprice;
                    
                   
                    // ESTRAZIONE DATI DELLA DATA DI PRODUZIONE SELEZIONATA
                    var lngrmprdtnid = "#lngtm" + lngtmprdctn;
                    var lngtmprdctnvals = $(lngrmprdtnid).val();
                    if ((lngtmprdctn != "") && (lngtmprdctn != null)) {
                        //alert(lngtmprdctnvals);
                        var lngtrmprdctnvl = lngtmprdctnvals.split("+");
                        lantrmdays = lngtrmprdctnvl[0];
                        lantrmamnt = lngtrmprdctnvl[1];
                        lantrmtype = lngtrmprdctnvl[2];
                        if (lantrmtype == "") {
                            lantrmtype = "value";
                        }
                        if (lantrmamnt == "") {
                            lantrmamnt = 0;
                        }
                        if (lantrmdays == "") {
                            lantrmdays = 0;
                        }

                        if (lantrmtype.toLowerCase() == "value") {
                            promoval += parseFloat(lantrmamnt);
                            //totlPrice = parseFloat(totlPrice) - parseFloat(lantrmamnt);
                        } else if (lantrmtype.toLowerCase() == "percentage") {
                            
                            //alert(totlPrice);
                            
                            promoval += ((parseFloat(totlPrice) / 100) * lantrmamnt);
                            //totlPrice = totlPrice - promoval;
                            
                            //alert(totlPrice);
                        }
                        
                    }
                    
                    var discount = $('#day_discount').val();
                    if ((discount == "") || (discount == null))
                        discount = 0;

                    var ci_discount = ((parseFloat(totlPrice) / 100) * parseFloat(discount));
                    //var totlPrice = parseFloat(totlPrice) - parseFloat(ci_discount);
                    promoval += ci_discount;

                    promoprice = totlPrice - promoval;
                    
                    var extraaccessories = parseFloat(0);
                    $('.extraaccessories-qnty-Value').each(function (index, element) {

                        if ($(element).val() > 0) {
                            extraaccessories += (parseFloat($(element).data('price')) * parseFloat($(element).val()));
                        }
                    });

                    if ((operatorreview == "") || (operatorreview == null) || (typeof(operatorreview) == "undefined")) {
                        operatorreview = 0;
                    } else {
                        operatorreview = $('#file_review_cost').val();
                    }

                    //RICHIAMO LA FUNZIONE PER IMPOSTARE TUTTI I PREZZI
                    setPrice(promoval, promoprice, extraaccessories, processingCost, 
                                    frontbackCost, singleproductPrice, operatorreview, 
                                    totlPrice, noOfProducts);


                }
            }
    };

})(jQuery);
