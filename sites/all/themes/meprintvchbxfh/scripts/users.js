var editorval = {
  'save_file_id': 0,
  'product_width': '1000',
  'product_height': '1020',
  'template_id': 1,
  'external_image': 1,
  'gallery_fid': 609,
  'modal_id': 'editorModal',
  'font_size': 100,
  'font_max': 3000,
  'font_min': 4,
  'font_step': 2,
  'colors_type': 0,  //0 -> tutti colori ,1 -> cmyk ,2 -> solo elenco
  'font_color': {
    rgb: {
      r: 0,
      g: 0,
      b: 0
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 100
    }
  },
  'font_style': 'normal', /* normal, italic*/
  'font_weight': 'normal', /* normal, bold*/
  'text_decoration': 'none',
  'text_align': 'left',
  'shape_fill_color' : {
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'shape_border_color': {
    rgb: {
      r: 0,
      g: 205,
      b: 0
    },
    cmyk: {
        c: 100,
        m: 0,
        y: 100,
        k: 20
    }
  },
  'circle_fill_color' : {
        rgb: {
            r: 42,
            g: 139,
            b: 200
        },
        cmyk: {
            c: 94,
            m: 32,
            y: 11,
            k: 0
        }
    },
    'circle_border_color': {
        rgb: {
            r: 9,
            g: 111,
            b: 176
        },
        cmyk: {
            c: 100,
            m: 48,
            y: 13,
            k: 1
        }
    },
  'text_content': 'Testo',
  'text_fill_color': {
    transparent: 1,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'text_border_color': {
    transparent: 1,
    rgb: {
      r: 255,
      g: 255,
      b: 255
    },
    cmyk: {
        c: 0,
        m: 0,
        y: 0,
        k: 0
    }
  },
  'dpi': 39.370,
  'min_image_dpi_y': 72,
  'min_image_dpi_x': 72,
  'z_index': 11,
  'cutting_grid': 0
};

var iRefreshEditor = undefined;

(function ($) {

    Drupal.behaviors.meprint = {
        attach: function (context, settings) {
            
            $('.cartdetails--open').click(function() {
                //console.log("QUI");
                $(this).children('.fa').toggleClass('fa-plus fa-minus');
                var collapse = $(this).parents('.cartdetailspage').find('.collapse');
                if( collapse.hasClass('in') ){
                  collapse.css('margin-top','0');
                } else {
                  collapse.css('margin-top','7px')
                }

              });

              
              if( $(window).width() > 1024 ){
                $("[data-toggle='tooltip']").tooltip();
              } else {
                $("[data-toggle='tooltip']").tooltip('disable');
              }


              // mi serve sapere quale upload image è stato selezionato
                $('.custom-file-theme').on('click', function (){
                  var id = $(this).attr('id').replace('custom-file-theme-', '');    
                  //al click si apre una modale (che è unica)...per sapere a quale upload image corrisponde inserisco un campo con l'id del chiamante
                  $('#exampleModal').attr('data-upi', id);
                });
      
              // RADIO BUTTON MODAL GALLERY
              $('.modal-gallery .theme-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
              $('.modal-gallery .theme-input[type="radio"]').click(function() {
                $('.modal-gallery .theme-input').prop('checked', false);
                $('.modal-gallery span.dimdiv__checked').remove();
                $(this).parents('.dimdiv').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--theme__checked"></span>');
                $(this).prop('checked', true);
              });

              // IMAGE GALLERY che appende l'immagine scelta in zona upload
                $('#modal-save').click(function() {
                  var imgId = $('.theme-input:checked').val();
                  var img = $('#theme_img-' + imgId).attr('src');
                  var upi = $('#exampleModal').data('upi');
                  //console.log( 'save upi: ' + upi );
                  $('#exampleModal').modal('hide');
                  $('#upi-'+upi+' .galleryimage--choose, #upi-'+upi+' .uploadproductimage__remove').remove();//nel caso di modifica elimino img caricata precedentemente
                  $('#upi-'+upi).append('<img id="load-img-'+ upi + '-' + imgId +'" class="galleryimage--choose" src="' + img + '" /><a class="uploadproductimage__remove"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');
                  $('#upi-' + upi + ' .fileuploader').hide();
                  $('#upi-'+upi+' .gallery_image_fid').val(imgId);
                  $('.dimdiv--gallery__checked').remove();
                  //var elemID = $(this).parents('.modal').attr('id');
                  //$('a[data-target="#' + elemID + '"]').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--gallery__checked"></span>');

                  //modifico il bottone EDITOR e modifico il testo del bottone tema
                  //$('#upi-'+upi+' .custom-file-editor, #upi-'+upi+' input[type="file"]').hide();
                  //$('#upi-'+upi+' input[type="file"]').hide();
                  $('#upi-'+upi+' .custom-file-theme').text(Drupal.t('change theme'));
                  $('#upi-'+upi+' .custom-file-editor').html('<i class="fa fa-paint-brush"></i> ' + Drupal.t('change theme with editor'));
                  //rimuovo il data-upi per la successiva modale
                  $('#exampleModal').removeData( "upi" );   
                  $('#custom-file-editor-'+upi).click();
                });
      
              $('.uploadproductimage').on('click', '.uploadproductimage__remove', function () {
                var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');
                //console.log( 'remove id: ' + id );
                $('#upi-' + id + ' .fileuploader').show();
                $('#upi-'+id+' img').remove();
                $('#upi-'+id).removeClass('uploadproductimage--selected');
                $('#upi-'+id+' .uploadproductimage__remove').remove();
                $('.dimdiv--theme__checked').remove();
                $('.dimdiv--theme .theme-input').prop('checked',false);
                $('#upi-' + id + ' .gallery_image_fid').val('');
                //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                $('#upi-'+id+' .custom-file-editor').show().html('<i class="fa fa-paint-brush"></i> ' + Drupal.t('choose editor')); 
                //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
                $('#image_'+id).show();
              });

              $('.uploadproductimage').on('click', '.uploadproductimage__remove2', function () {
                var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

                var fid = jQuery("#image_fid_"+id).val();

                $.post( '/remove_image', {
                  'fid': fid
                }, function() {
                  $('#upi-' + id + ' .fileuploader').show();
                  $('#upi-'+id+' img').remove();
                  $("#image_fid_"+id).val('');
                  $('#upi-'+id).removeClass('uploadproductimage--selected');
                  $('#upi-'+id+' .uploadproductimage__remove2').remove();
                  $('.dimdiv--theme__checked').remove();
                  $('.dimdiv--theme .theme-input').prop('checked',false);
                  //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                  $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                  $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
                  //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
                  $('#image_'+id).show();
                })
                .fail(function() {
                  $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
                });

              });

              $('#editor_close').on('click', function() {
                var id = $('.uploadproductimage').attr('id').replace('upi-', '');
                var galFid = $('#gallery_fid_'+id).val();
                
                if (galFid > 0) {
                    $('.uploadproductimage__remove').click();
                }
              });
              
              $('.uploadproductimage').on('click', '.uploadproductimage__remove3', function () {
                var id = $(this).parents('.uploadproductimage').attr('id').replace('upi-', '');

                var fid = jQuery("#editor_fid_"+id).val();
                var previewfid = jQuery("#editor_fid_"+id).data('preview');

                $.post( '/remove_image', {
                  'fid': fid,
                    'previewfid': previewfid
                }, function() {
                  $('#upi-' + id + ' .fileuploader').show();
                  $('#upi-'+id+' img').remove();
                  $("#editor_fid_"+id).val('');
                    $("#editor_fid_"+id).attr('data-preview','');
                  $('#upi-'+id).removeClass('uploadproductimage--selected');
                  $('#upi-'+id+' .uploadproductimage__remove3').remove();
                  $('.dimdiv--theme__checked').remove();
                  $('.dimdiv--theme .theme-input').prop('checked',false);
                  //$('#upi-'+id+' .custom-file-editor, #upi-'+id+' input[type="file"]').show();
                  $('#upi-'+id+' .custom-file-theme').show().text(Drupal.t('choose theme'));
                  $('#upi-'+id+' .custom-file-editor').show().text(Drupal.t('choose editor')); 
                  //$('#upi-'+id+' input[type="file"]').replaceWith($('#upi-'+id+' input[type="file"]').val('').removeClass('valid').clone(true));
                  $('#image_'+id).show();
                })
                .fail(function() {
                  $(".ImageError_"+id).html(Drupal.t("Error deleting the image"));
                });

              });

              $('.btn-editor').on('click', function(){
                    

                    var id = $(this).attr('id');
                    var id_arr = id.split("-");
                    var orderid = $(this).attr('data-orderid');
                    var id_num = id_arr[3];

                    var gallery_img_id = $('.galleryimage--choose').attr('id');
                    if(gallery_img_id != null){
                        var gallery_img_id_arr = gallery_img_id.split("-");
                        var gallery_fid = gallery_img_id_arr[3];
                    }
                    
                    var productheight = $('#productHeight_'+orderid).val();
                    var productwidth = $('#productWidth_'+orderid).val();
                    
                    //Setup editor
                    if ($('#editor_dpi_'+orderid).length > 0) {
                        var editor_dpi = $('#editor_dpi_'+orderid).val();
                    } else {
                        var editor_dpi = 72;
                    }

                    if ($('#editor_colors_type_'+orderid).length > 0) {
                        var editor_colors_type = $('#editor_colors_type_'+orderid).val();
                    } else {
                        var editor_colors_type = 1;
                    }

                    if ($('#editor_cutting_grid_'+orderid).length > 0) {
                        var editor_cutting_grid = $('#editor_cutting_grid_'+orderid).val();
                    } else {
                        var editor_cutting_grid = 0;
                    }
                    
                    if ($('#editor_colors_'+orderid).length > 0) {
                        var editor_colors = $('#editor_colors_'+orderid).val();
                        editorval['colors'] = editor_colors;
                    }
                    
                    //var editor_dpi = $('#editor_dpi_'+orderid).val();
                    //var editor_colors_type = $('#editor_colors_type_'+orderid).val();
                    //var editor_cutting_grid = $('#editor_cutting_grid_'+orderid).val();
                    

                    editorval['min_image_dpi_y'] = editor_dpi;
                    editorval['min_image_dpi_x'] = editor_dpi;
                    editorval['colors_type'] = editor_colors_type;
                    editorval['cutting_grid'] = editor_cutting_grid;
                    

                    editorval['product_width'] = productwidth;
                    editorval['product_height'] = productheight;
                    editorval['save_file_id'] = id_num;

                    if(gallery_fid !== ""){
                        editorval['gallery_fid'] = gallery_fid;
                    }

                    var preview_fid = 0;
                    $.ajax({
                        type: "POST",
                        url: "/getPreview",
                        data: { gallery_fid: gallery_fid },
                        success: function( response ){
                            console.log(response);
                            preview_fid= response["preview"];
                            console.log(preview_fid);
                        },
                        complete: function() {
                            if(preview_fid !== ""){
                                editorval['preview_fid'] = preview_fid;
                            }
                            else{
                                editorval['preview_fid'] = "";
                            }

                            iRefreshEditor(editorval);
                        }
                    });
                    //var source='/publishereditor';
                    //$('#editorModal .publisher-editor').attr('src', source);
                    //var x = document.getElementById("editorModalFrame");
                    //var y = (x.contentWindow || x.contentDocument);
                    //if (y.document)y = y.document;
                    //var input = y.getElementById('refresh_trick');
                    //console.log(input);
                    //iRefreshEditor(editorval);
                });

              $('.fileuploader').each(function(i, obj) {
          
                var divID = $(obj).attr('c');

                $(obj).uploadFile({
                      dragDrop: false,
                      showAbort: true,
                      url:'/save_image',
                      showDelete: false,
                      showFileCounter:false,
                      returnType: "json",
                      fileName:"uploaded_file",
                      onSubmit:function(files)
                      {
                        //nascondo il bottone EDITOR e modifico il testo del bottone tema
                        $('#custom-file-theme-'+divID+', #custom-file-editor-'+divID).hide();
                      },
                      onSuccess: function(files,response,xhr,pd)
                      {
                          //console.log(response);

                          $('#upi-'+divID).addClass('uploadproductimage--selected');

                          $("#image_fid_"+divID).val(response.fid);

                          $(".ImageError_"+divID).html("");

                          $(".dvPreview_"+divID).html('<img style="width:200px" src="'+ response.url+'" class="uploadproductimage--choose" ><a class="uploadproductimage__remove2"><span class="fa fa-trash"></span> '+Drupal.t('remove image') + '</a>').addClass('uploadproductimage--selected');

                          $('#image_'+divID).hide();
                          $('#upi-'+divID + ' .ajax-file-upload-statusbar').remove();




                        //pageObject.addPicture(new PictureBox(pageObject, 5, 5, response.width, response.height, response.url, response.fid));
                      },
                      onError: function(files,status,errMsg,pd)
                      {
                          $('#image_'+divID).val("");
                          $('#image_'+divID).show();                              
                          showModal("alertModal", Drupal.t("error"), Drupal.t("error loading image"));
                      }
                      /*customProgressBar: function(obj,s)
                      {
                      this.statusbar = $("<div class='custom-statusbar'></div>");
                      this.filename = $("<div class='custom-filename'></div>").appendTo(this.statusbar);
                      this.progressDiv = $("<div class='custom-progress'>").appendTo(this.statusbar).hide();
                      this.progressbar = $("<div class='custom-bar'></div>").appendTo(this.progressDiv);
                      this.abort = $("<div>" + s.abortStr + "</div>").appendTo(this.statusbar).hide();
                      this.cancel = $("<div>" + s.cancelStr + "</div>").appendTo(this.statusbar).hide();
                      this.done = $("<div>" + s.doneStr + "</div>").appendTo(this.statusbar).hide();
                      this.download = $("<div>" + s.downloadStr + "</div>").appendTo(this.statusbar).hide();
                      this.del = $("<div>" + s.deletelStr + "</div>").appendTo(this.statusbar).hide();

                      this.abort.addClass("custom-red");
                      this.done.addClass("custom-green");
                      this.download.addClass("custom-green");            
                      this.cancel.addClass("custom-red");
                      this.del.addClass("custom-red");

                                  return this;

                          }*/
                  });
            });

            $('.orderproceed').each(function(i, obj) {
          
                var orderID = $(obj).attr('c');
                var nFiles = $(obj).data('nfiles');
                 
                $(obj).on('click', function (event){
                    
                    event.preventDefault();
                    //console.log( 'form valid: ' + $("#product-big-format").valid() );
                    modify_order(orderID, nFiles);
                    //
                });
            });


            
            function modify_order(orderID, nFiles) {
                var data = new FormData();
                
                var upload_images_fid = [];
                var gallery_images_fid = [];
                var editor_images_fid = [];

                $('.upload_image_fid' + orderID).each(function(i, obj) {

                  if ($(obj).val() != '') {
                    //console.log($(obj).val());
                    upload_images_fid.push($(obj).val());
                  }
                });

                $('.gallery_image_fid' + orderID).each(function(i, obj) {

                  if ($(obj).val() != '') {
                    //alert($(obj).val());
                    gallery_images_fid.push($(obj).val());
                  }
                });

                $('.editor_image_fid' + orderID).each(function (i, obj) {

                    if ($(obj).val() != '') {
                        //alert($(obj).val());
                        editor_images_fid.push($(obj).val());
                    }
                });
                
                var tot_files = upload_images_fid.length + gallery_images_fid.length + editor_images_fid.length;
                
                
                //alert(tot_files);
                
                var post_data = {
                  order_id: orderID,
                  upload_images_fid: JSON.stringify(upload_images_fid),
                  gallery_images_fid: JSON.stringify(gallery_images_fid),
                  editor_images_fid: JSON.stringify(editor_images_fid)
                };

                //console.log( post_data );
                /*data.append(0, post_data[0]);
                alert(orderID);
                */
                
                
                $.ajax( {
                  type: "POST",
                  url: "/modify-order-images",
                  data: post_data,
                  dataType: "json",
                  success: function( response ) {
                    if(response == 'ok' && (nFiles - tot_files) <= 0) {
                        //alert('ooooo');
                        $('.box-messaggi'+orderID).html('');
                    } else if(response == 'ok' && (nFiles - tot_files) > 0) {
                        
                        
                        var html = '<p>'+ Drupal.t('You have not entered all the required files.')+ '</p>';
                        
                        $('.box-messaggi'+orderID).html(html);
                        
                        
                    } else if(response == 'error') {
                        
                        var html = '<p>'+ Drupal.t('There are files incorrect.')+ '</p>';
                        
                        $('.box-messaggi'+orderID).html(html);
                        
                        
                    }
                  },
                  complete: function () {
                      location.reload();
                  }
                });
              }

            function capitalizeFirstLetter(string) {
              return string.charAt(0).toUpperCase() + string.slice(1);
            }

            function showModal(id,title,content){
              $('#'+id+' .modal-title').html(title);
              $('#'+id+' .modal-body').html( capitalizeFirstLetter(content) );
              $('#'+id).modal('show');
            }


        }
    };
})(jQuery);


