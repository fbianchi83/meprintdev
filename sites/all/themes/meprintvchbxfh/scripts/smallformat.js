(function($) {
Drupal.behaviors.smallformatModule = {
	attach: function (context, settings) {  
    

/************ Promo Price *******************/

$(".form-item-promotion-price-small").hide();
 $("#edit-promo-flag-small-promo").click(function () { 

        if($("#edit-promo-flag-small-promo").is(':checked')){ 
            $(".form-item-promotion-price-small").show();  
            }       
});
$("#edit-promo-flag-small-best-price").click(function () {  
    
    if($("#edit-promo-flag-small-best-price").is(':checked')){  
          $(".form-item-promotion-price-small").hide();  
        
    } 
});
/************ Promo Price End *******************/
/************ Extra Processing *******************/
$("#hasextra_process_small").hide();
 if($("#edit-small-extra-processing-small-1").is(':checked')){    
	$("#hasextra_process_small").show();	
} else {
	 $("#hasextra_process_small").hide();
}
$("#edit-small-extra-processing-small-1").click(function () {      
    if($("#edit-small-extra-processing-small-1").is(':checked')){    
        $("#hasextra_process_small").show();      
    } else {
         $("#hasextra_process_small").hide();
    }
});
/************ Extra Processing End *******************/

 $(document).ready (   	 
            function() {   
		/************ Materials start *******************/
               $('#arrow_mat_small').click(  
                    function(e) {  
                        $('#material_group_id_small > option:selected').appendTo('#material_group_list_small');  
                        e.preventDefault();  
                    });  
               $('#arrowback_mat_small').click(  
                    function(e) {   
                        $('#material_group_list_small > option:selected').appendTo('#material_group_id_small');  
                        e.preventDefault();  
                });  
                
                 
                
              /************ Materials end *******************/
                
              /************ Dimensions start *******************/
              $('#arrow_dimensions_small').click(  
                    function(e) {  
                        $('#dimension_id_small > option:selected').appendTo('#dimension_list_small');  
                        e.preventDefault();  
                    });  
               $('#arrow_dimensions_back_small').click(  
                    function(e) {  
                        $('#dimension_list_small > option:selected').appendTo('#dimension_id_small');  
                        e.preventDefault();  
                });  
                /************ dimensions End *******************/
                
               /************ Processing Start *******************/
                 $('#arrow_process_small').click(  
                    function(e) {  
                        $('#small_extra_processing_id > option:selected').appendTo('#small_extra_processing_list');  
                        e.preventDefault();  
                    });  
               $('#arrow_process_back_small').click(  
                    function(e) {  
                        $('#small_extra_processing_list > option:selected').appendTo('#small_extra_processing_id');  
                        e.preventDefault();  
                });  
                
                
              /************ Processing End *******************/
                
            
                
               
            });
            /************** Multi Site Start **********/

//alert('pppp');

$("#fr_web").hide();
if ($('#edit-choose-website-small-fr').is(":checked")) {       
    $("#fr_web").show();     
}  else {
	$("#fr_web").hide();
}
 $("#edit-choose-website-small-fr").click(function () { 
    if ($('#edit-choose-website-small-fr').is(":checked")) {       
        $("#fr_web").show();     
    }
	else {
        $("#fr_web").hide();
    }
});
$("#it_web").hide(); 
if ($('#edit-choose-website-small-it').is(":checked")) {       
       $("#it_web").show();     
    } 
    else {
          $("#it_web").hide();
    }
$("#edit-choose-website-small-it").click(function () { 
    if ($('#edit-choose-website-small-it').is(":checked")) {       
       $("#it_web").show();     
    } 
    else {
          $("#it_web").hide();
    }
});

$("#es_web").hide();
if ($('#edit-choose-website-small-es').is(":checked")) {       
    $("#es_web").show();     
}  else {
	$("#es_web").hide();
}
 $("#edit-choose-website-small-es").click(function () { 
    if ($('#edit-choose-website-small-es').is(":checked")) {       
        $("#es_web").show();     
    }
	else {
        $("#es_web").hide();
    }
});

    /************** Multi Site End **********/ 
$(".form-item-min-copies-small").hide();  
if ($('#edit-choice-of-copy-number-small-1').is(":checked")) {
   $(".form-item-min-copies-small").show();     
}  else {
	 $(".form-item-min-copies-small").hide();   
}
$("#edit-choice-of-copy-number-small-1").click(function () { 
    if ($('#edit-choice-of-copy-number-small-1').is(":checked")) {
           $(".form-item-min-copies-small").show();     
        }  else {
             $(".form-item-min-copies-small").hide();   
        }

});

//edit-choice-of-copy-number-it-1

            /************** Price Type Promo Start**********/
            
   $(".form-item-promotion-price-small-fr").hide();
   $("#edit-promo-flag-small-fr-promo").click(function () { 
    if ($('#edit-promo-flag-small-fr-promo').is(":checked")) {
           $(".form-item-promotion-price-small-fr").show();     
        }  else {
             $(".form-item-promotion-price-small-fr").hide();   
        }

});

  $(".form-item-promotion-price-small-it").hide();
   $("#edit-promo-flag-small-it-promo").click(function () { 
    if ($('#edit-promo-flag-small-it-promo').is(":checked")) {
           $(".form-item-promotion-price-small-it").show();     
        }  else {
             $(".form-item-promotion-price-small-it").hide();   
        }

});


 /************** Price Type Promo End**********/ 
 
  /************** Price Type Best Price Start**********/

 $("#edit-promo-flag-small-fr-best-price").click(function () { 
    if ($('#edit-promo-flag-small-fr-best-price').is(":checked")) { 
       
             $(".form-item-promotion-price-small-fr").hide();   
        } 

});


 $("#edit-promo-flag-small-it-best-price").click(function () { 
    if ($('#edit-promo-flag-small-it-best-price').is(":checked")) { 
       
             $(".form-item-promotion-price-small-it").hide();   
        } 

});
 
  /************** Price Type Best Price End**********/



  }
};

      
})(jQuery);



   
