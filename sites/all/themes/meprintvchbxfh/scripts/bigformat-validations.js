jQuery(document).ready(function($) {
  /****** big Format Form ************/

  $.validator.addMethod("check_q", function (value, element, param) {
    var val_a = $('input[name="minQuantity"]').val();
    return (parseInt(value) >= parseInt(val_a));
  }, "Your error message.");

  //validation checkbox
  $('.extra-check').parent().each(function(key, value){
    $(this).find('input').addClass('check'+key);
    $(this).find('input').addClass('checkrequired');

    var name = 'check'+key;

    $.validator.addClassRules(name,{
        require_from_group: [1, '.'+name]
    });
  });

  $.validator.addMethod("baseDistanceControl", function (value, element, param) {
    if( value == "") return false;
    else return true;
  }, "Seleziona un valore.");

  $.validator.addClassRules("base_distance_special", {baseDistanceControl: true});


  $.validator.addMethod("extraDistanceControl", function (value, element, param) {      
    if( value == "") return false;
    else return true;
  }, "Seleziona un valore.");

  $.validator.addClassRules("distance_special", {extraDistanceControl: true});
  
  $.validator.addMethod("require_from_group2", function(value, element, options) {
	var $fields = $(options[1], element.form),
		$fieldsFirst = $fields.eq(0),
		validator = $fieldsFirst.data("valid_req_grp") ? $fieldsFirst.data("valid_req_grp") : $.extend({}, this),
		isValid = $fields.filter(function() {
			return validator.elementValue(this);
		}).length >= options[0];

	// Store the cloned validator for future validation
	$fieldsFirst.data("valid_req_grp", validator);

	// If element isn't being validated, run each require_from_group field's validation rules
	if (!$(element).data("being_validated")) {
		$fields.data("being_validated", true);
		$fields.each(function() {
			validator.element(this);
		});
		$fields.data("being_validated", false);
        }
        if (!isValid && $(element).hasClass('mand')) {
            var topPosition = jQuery('.section-processing-accessories').offset().top;
            jQuery('html, body').animate({scrollTop:topPosition}, 'fast');
        }
        return isValid;
}, $.validator.format(Drupal.t("Select one of the red mandatory processes.")));
  
  if( $('#extra_processing_mandatory').val() == 1 ){
    //console.log('extra_processing_mandatory');
    $.validator.addClassRules('mand',{
        require_from_group2: [1, '.mand']
    });
  }
  
  
  var material_id = $(".material_class:checked").val();
  $(".material_class").on('click', function(){
    material_id = $(".material_class:checked").val();      
  });

  $("#product-big-format").validate({
    rules: {
      quanitity: {
        required: true,
        number: true,
        check_q: true
      },
      productHeight: {
        required: true,
        number: true,
        min: [$('input[name="dbheight1-'+material_id+'"]').val()]

      },
      productWidth: {
        required: true,
        number: true,
        min: [$('input[name="dbwidth1-'+material_id+'"]').val()]

      }
    }, messages: {            
        productHeight: {
            required: "Inserire un numero",
            number: "Caratteri consentiti: Numeri [0-9] Punteggiatura di separazione decimali [.]",
            range: "Inserire un'altezza tra " + $('input[name="dbheight1-'+material_id+'"]').val() + " e " + $('input[name="dbheight2-'+material_id+'"]').val() + " cm"
        },
        productWidth: {
            required: "Inserire un numero",
            number: "Caratteri consentiti: Numeri [0-9] Punteggiatura di separazione decimali [.]",
            range: "Inserire una larghezza tra " + $('input[name="dbwidth1-'+material_id+'"]').val() + " e " + $('input[name="dbwidth2-'+material_id+'"]').val()  + " cm"
        },
        extradefValue: {
          number: "Inserire un numero"
        }
      },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      if( element.hasClass('extra_side') || element.hasClass('distance_special') ){
        var id_extra_side = element.attr("id").split("_");
        error.appendTo(element.parents('#extinfodiv'));

      } else {
        error.appendTo(element.parents('.order-view-top'));
      }

    }
  });
});