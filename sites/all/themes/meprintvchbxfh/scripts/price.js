(function ($) {
    
    Drupal.behaviors.price = {
        attach: function (context, settings) {
            
            function setTablePrices(baseprice, baseprices, height, width, squareyes, isfixed, fixed_cost_price) {

                var promotionalflag = $('#promotionalFlag').val();
                var promotype = $('#promotionalType').val();
                var promoval = $('#promotionalValue').val();
                
                //ciclo le quantità predefinite
                $('.fixed-amounts').each(function () {
                    var quantity = $(this).data("value");

                    if (isfixed) {
                        totalPrice_finito = fixed_cost_price * quantity;
                    }
                    else { 
                        prdctarea= height * width * quantity;
                        if (baseprices != null) {
                            for (var i = 0; i < baseprices.length; i++) {
                                var costbreak = baseprices[i].split(" ");
                                //if (costbreak[0]== 1) costbreak[0]= 0;
                                if (costbreak[2] !== undefined)
                                    price= costbreak[2];
                                
                                if (parseFloat(prdctarea.toFixed(2)) <= parseFloat(costbreak[1]) || !parseFloat(costbreak[1])) {
                                    var matrlpricefrml = parseFloat(costbreak[2]) * parseFloat(prdctarea);
                                    totalPrice_finito = parseFloat(matrlpricefrml);
                                    break;
                                }
                            }
                            
                            if (isNaN(totalPrice_finito))
                                totalPrice_finito = parseFloat(price) * parseFloat(prdctarea);
                        }
                        else {
                            if (height * width * quantity <= 1 && squareyes == 1)
                                totalPrice_finito = baseprice;
                            else
                                totalPrice_finito = height * width * quantity * baseprice;
                        }
                        
                        
                        
                    }
                    
                    if ($('#lessthan').val() == 1)
                        totalPrice_finito = parseFloat(totalPrice_finito) + (parseFloat(totalPrice_finito) * parseInt($("#incr_percentage").val()) / 100);
                        
                    if (promotionalflag.toLowerCase() == "promo") {
                        if (promotype.toLowerCase() == "value") {
                            totalPrice_finito = parseFloat(totalPrice_finito) - parseFloat(promoval);
                        } else
                        if (promotype.toLowerCase() == "percentage") {
                            promoval2 = ((parseFloat(totalPrice_finito) / 100) * parseFloat(promoval));
                            totalPrice_finito = parseFloat(totalPrice_finito) - parseFloat(promoval2);

                        }
                    }

                    var discount = $(this).data("discount");
                    if ((discount == "") || (discount == null))
                        discount = 0;

                    var ci_discount = ((parseFloat(totalPrice_finito) / 100) * parseFloat(discount));
                    var totalPrice_netto = parseFloat(totalPrice_finito) - parseFloat(ci_discount);
                    var ci_molt_tot_first = totalPrice_netto; // * quantity;

                    $("#total-q" + quantity + "-lngtm0").html(parseFloat(ci_molt_tot_first).toFixed(2));

                    //CICLO LE DATE SUCCESSIVE PER AGGIORNARE I PREZZI
                    $('.lngtmnpt').each(function () {
                        var value = $(this).prop('value');
                        var id = $(this).prop('id');

                        if ((value !== "") && (value !== null)) {
                            var ci_lngtrmprdctnvl = value.split("+");
                            var ci_lantrmamnt = ci_lngtrmprdctnvl[1];
                            var ci_lantrmtype = ci_lngtrmprdctnvl[2];

                            if (ci_lantrmtype === "") {
                                ci_lantrmtype = "value";
                            }
                            if (ci_lantrmamnt === "") {
                                ci_lantrmamnt = 0;
                            }
                        }

                        if (ci_lantrmtype.toLowerCase() == "value") {
                            var ci_tot = parseFloat(totalPrice_finito) - parseFloat(ci_lantrmamnt);
                        }
                        else if (ci_lantrmtype.toLowerCase() == "percentage") {
                            var ci_lngtrmdsc = ((parseFloat(totalPrice_finito) / 100) * parseFloat(ci_lantrmamnt));
                            var ci_tot = parseFloat(totalPrice_finito) - parseFloat(ci_lngtrmdsc);
                        }

                        var ci_lantrm_discount = ((parseFloat(ci_tot) / 100) * parseFloat(discount));
                        var totalPrice_lantrm__netto = parseFloat(ci_tot) - parseFloat(ci_lantrm_discount);
                        var ci_molt_tot = totalPrice_lantrm__netto; // * quantity;
                        $("#total-q" + quantity + "-" + id).html(ci_molt_tot.toFixed(2));
                    });
                });
            }
                
            function setPrice(promotionalflag, promotype, promoval, promoprice, lantrmtype, lantrmamnt, fbprice, baseaccessories, extraaccessories, extraprocssing, matrlpricefrml, processingCost, productprice, singleproductPrice, operatorreview, totlPrice, noOfProducts){              
                
                var totlPriceNoPromo = totlPrice;
                
                promoval = ((parseFloat(totlPrice) / 100) * parseFloat(promoval));
                
                promoprice = totlPrice;
                if ($('#promotionalFlag').val() === "Promo") {
                    if ($('#promotionalType').val() === "value") {
                        promoval += $('#promotionalValue').val();
                    }
                    if ($('#promotionalType').val() === "percentage") {
                        promoval += ((parseFloat(totlPrice) / 100) * $('#promotionalValue').val());
                    }
                }
                
                totlPrice = parseFloat(promoprice) - parseFloat(promoval);
                
                var section_struttura_id = $('.section-struttura .label-structure input:checked').attr('id');
                if( section_struttura_id == 'only_structure'){
                    singleproductPrice = totlPrice = totlPriceNoPromo = parseFloat(baseaccessories*noOfProducts).toFixed(2);
                    productprice= parseFloat(0).toFixed(2);
                }
                
                //CODICE PER IL CALCOLO DEI PREZZI DELLA TABELLA DELLE DATE DI SPEDIZIONE //

                var ship_table_type = $("#ship-table").data("type");
                
                    //alert(totlPrice);
                if (ship_table_type.toLowerCase() == "free") { //CALCOLO PER LA TABELLA CON DIMENSIONI LIBERE

                    //console.log("PRIMA DATA"+totlPrice);
                    //IMPOSTO IL TOTALE PER LA PRIMA DATA DISPONIBILE
                    $("#totallngtm0").html(parseFloat(totlPrice).toFixed(2));

                    //CICLO LE DATE SUCCESSIVE PER AGGIORNARE I PREZZI
                    $('.lngtmnpt').each(function () {
                        var value = $(this).prop('value');
                        var id = $(this).prop('id');

                        if ((value !== "") && (value !== null)) {

                            var ci_lngtrmprdctnvl = value.split("+");
                            var ci_lantrmamnt = ci_lngtrmprdctnvl[1];
                            var ci_lantrmtype = ci_lngtrmprdctnvl[2];

                            if (ci_lantrmtype === "") {
                                ci_lantrmtype = "value";
                            }
                            if (ci_lantrmamnt === "") {
                                ci_lantrmamnt = 0;
                            }
                        }

                        if (ci_lantrmtype.toLowerCase() == "value") {
                            var ci_tot = parseFloat(totlPrice) - parseFloat(ci_lantrmamnt);
                            //promoval += parseFloat(ci_lantrmamnt);
                        }
                        else if (ci_lantrmtype.toLowerCase() == "percentage") {
                            
                            var ci_lngtrmdsc = ((parseFloat(totlPrice) / 100) * parseFloat(ci_lantrmamnt));
                            var ci_tot = parseFloat(totlPrice) - parseFloat(ci_lngtrmdsc);
                            //promoval += parseFloat(ci_lngtrmdsc);
                        }

                        $("#total" + id).html(ci_tot.toFixed(2));
                        //console.log($("#total" + id).html());
                    });

                } 
                
                //console.log(promoval);
                
                /*else if (ship_table_type.toLowerCase() == "fixed") { //CALCOLO PER LA TABELLA CON DIMENSIONI PREDEFINITE

                
                    //ciclo le quantità predefinite
                    $('.fixed-amounts').each(function () {
                        var quantity = $("#productQuanity").prop('value');
                        
                        var totalPrice_divided = parseFloat(totlPrice / quantity);
                        var amount = $(this).data("value");
                        
                        var discount = $(this).data("discount");
                        
                        var ci_discount = ((parseFloat(totalPrice_divided) / 100) * parseFloat(discount));
                        var totalPrice_netto = parseFloat(totalPrice_divided) - parseFloat(ci_discount);
                        var ci_molt_tot_first = totalPrice_netto * amount;
                        
                        $("#total-q" + amount + "-lngtm0").html(parseFloat(ci_molt_tot_first).toFixed(2));

                        //CICLO LE DATE SUCCESSIVE PER AGGIORNARE I PREZZI
                        $('.lngtmnpt').each(function () {
                            var value = $(this).prop('value');
                            var id = $(this).prop('id');

                            if ((value !== "") && (value !== null)) {

                                var ci_lngtrmprdctnvl = value.split("+");
                                var ci_lantrmamnt = ci_lngtrmprdctnvl[1];
                                var ci_lantrmtype = ci_lngtrmprdctnvl[2];

                                if (ci_lantrmtype === "") {
                                    ci_lantrmtype = "value";
                                }
                                if (ci_lantrmamnt === "") {
                                    ci_lantrmamnt = 0;
                                }
                            }

                            if (ci_lantrmtype.toLowerCase() == "value") {
                                var ci_tot = parseFloat(totalPrice_divided) - parseFloat(ci_lantrmamnt);
                            }
                            else if (ci_lantrmtype.toLowerCase() == "percentage") {
                                var ci_lngtrmdsc = ((parseFloat(totalPrice_divided) / 100) * parseFloat(ci_lantrmamnt));
                                var ci_tot = parseFloat(totalPrice_divided) - parseFloat(ci_lngtrmdsc);
                            }
                            
                            var ci_lantrm_discount = ((parseFloat(ci_tot) / 100) * parseFloat(discount));
                            var totalPrice_lantrm__netto = parseFloat(ci_tot) - parseFloat(ci_lantrm_discount);
                            
                            var ci_molt_tot = totalPrice_lantrm__netto * amount;
                            $("#total-q" + amount + "-" + id).html(ci_molt_tot.toFixed(2));
                        });
                    });

                }*/

                // FINE CODICE PER IL CALCOLO DEI PREZZI DELLA TABELLA DELLE DATE DI SPEDIZIONE //
                if (lantrmtype.toLowerCase() == "value") {
                    promoval += parseFloat(lantrmamnt);
                    totlPrice = parseFloat(totlPrice) - parseFloat(lantrmamnt);
                }
                if (lantrmtype.toLowerCase() == "percentage") {
                    var lngtrmdsc = ((parseFloat(totlPrice) / 100) * parseFloat(lantrmamnt));
                    promoval += parseFloat(lngtrmdsc);
                    totlPrice = parseFloat(totlPrice) - parseFloat(lngtrmdsc);
                }
                
                //console.log(totlPrice - extraaccessories);
                if (baseaccessories != 0) {
                    $('.baseaccessoriestd').show();
                    baseaccessories = parseFloat(baseaccessories * noOfProducts).toFixed(2);
                    $('.baseacccstdiv').html(baseaccessories);
                } else {
                    $('.baseaccessoriestd').hide();
                }

                if (fbprice !== 0) {
                    $('.frontbacktd').show();
                    fbprice = parseFloat(fbprice).toFixed(2);
                    $('.frcostdiv').html(fbprice);
                } else {
                    $('.frontbacktd').hide();
                }
                //console.log(extraaccessories);
                if (extraaccessories != 0) {
                    $('.extraaccessoriestd').show();
                    extraaccessories = parseFloat(extraaccessories).toFixed(2);
                    $('.extraacccstdiv').html(extraaccessories);
                } else {
                    $('.extraaccessoriestd').hide();
                }

                if (promoprice != 0 && promoval != 0) {
                    $('.promopricetd').show();
                    promoprice = parseFloat(promoprice).toFixed(2);
                    $('.promopricediv').html(promoprice);
                } else {
                    $('.promopricetd').hide();
                }

                if (promoval != 0) {
                    $('.promovaltd').show();
                    promoval = parseFloat(promoval).toFixed(2);
                    $('.promovaldiv').html("-" + promoval);
                } else {
                    $('.promovaltd').hide();
                }

                if (extraprocssing != 0) {
                    $('.extraprocessingtd').show();
                    extraprocssing = parseFloat(extraprocssing).toFixed(2) ;
                    $('.extraprocessingdiv').html(extraprocssing);
                } else {
                    $('.extraprocessingtd').hide();
                }

                if (operatorreview != 0) {
                    $('.operatorreviewtd').show();
                    operatorreview = parseFloat(operatorreview).toFixed(2);
                    $('.operatorreviewdiv').html(operatorreview);
                } else {
                    $('.operatorreviewtd').hide();
                }
                
                var al_iva = parseFloat($('#vat_iva').val()) / 100;

                matrlpricefrml = parseFloat(matrlpricefrml).toFixed(2);
                processingCost = parseFloat(processingCost).toFixed(2);
                productprice = parseFloat(productprice).toFixed(2);
                singleproductPrice = parseFloat(singleproductPrice).toFixed(2);
                totlPrice = parseFloat(totlPrice) + parseFloat(operatorreview);
                totlPrice = parseFloat(totlPrice).toFixed(2);
                            
                var iva = parseFloat(al_iva * totlPrice).toFixed(2);

                var totlPriceIva = parseFloat(totlPrice) + parseFloat(iva);
                totlPriceIva = parseFloat(totlPriceIva).toFixed(2);

                var printingmaterialcost= parseFloat(( totlPriceNoPromo - extraaccessories) / noOfProducts).toFixed(2);
                //var printingmaterialcost= parseFloat(productprice).toFixed(2);
                
                $('.printigmaterialdiv').html(printingmaterialcost);
                $('.adtnlcostdiv').html(processingCost);
                $('.singleproductcostdiv').html(singleproductPrice);
                $('.totalproductcostdiv').html(totlPrice);
                $('.ivadiv').html(iva);
                $('.totalcostdiv').html(totlPriceIva);
                $('.totalnoofproductbigfrmtdiv').html(noOfProducts);
                $('.bigtotlprice').val(totlPrice);
                
                //IMPOSTO GLI INPUT HIDDEN PER IL CARRELLO
                $('#material_cost').val(productprice);
                $('#processing_cost').val(processingCost);
                $('#total_cost').val(totlPrice);
                $('#structure_cost').val(baseaccessories);
                $('#promo_value').val(promoval);
                
                $('#relatedpanel').css('top', $('#summary-cart--product').height() + 45);
                $('#panel-file').css('top', $('#summary-cart--product').height() +  $('#relatedpanel').height() + 90);
            }

            //code starts

            /************* On Load **********************/
            // var shippingidj =  $('input[name=shipping]:checked').val();
            //  updateShipping(shippingidj);   
            // alert(shippingidj);
            
            updateFixedCosts();
            /* Le due funzioni sottostanti vengono richiamate direttamente da updateFixedCosts() */
            //updateDates();
            //updateValues();
          
            /**************** On Load ********************/
            $('#fixed_costs').on("change", function () {
                if ($(this).val() == 1) {
                    updateFixedCosts();
                }
            });
            /*************** On Change OF MATERIAL START ****************/
            $(".material_class").change(function () {
                updateFixedCosts();
                /* Le due funzioni sottostanti vengono richiamate direttamente da updateFixedCosts() */
                //updateDates();
                //updateValues();
            });
            /*************** On Change MATERIAL END *********************/

            /*************** On Blur Quantity START *********************/
            $("#productQuanity").blur(function () {
                updateValues();
            });
            /*************** On Blur Quantity END ************************/

            /*************** On Blur Height START ************************/
            $("#productHeight").blur(function () {
                updateValues();
            });
            /*************** On Blur Height END **************************/

            /************* On Blur Width START *************/
            $("#productWidth").blur(function () {
                updateValues();
            });
            /*********** On Blur Width END ********************/

            /************* On change Base Acessory (Structure) Start*************/
            $("input[name=baseaccessories]").change(function () {
                //$('input:checkbox[name=extraaccessories]').attr('checked', false);
                updateValues();
            });
            /*********** On change Base Acessory (Structure) END ********************/
            
            /************* On change FrontBack (Structure) Start*************/
            $("input[name=fronteretro]").change(function () {
                updateValues();
            });
            /*********** On change Base Acessory (Structure) END ********************/
            /************* On change Extra Acessory Start*************/
            /*$("input[name=extraaccessories]").change(function () {
                
                var extraaccProcList = "";
                $('input[name*=extraaccessories]:checked').each(function () {
                    var value = this.value;
                    extraaccProcList += (extraaccProcList == "" ? value : "+" + value);
                });

                $("#extraccslct").val(extraaccProcList);
                
                updateValues();
            });*/
            
            //var chkd = "";
            $(".extraaccessories_class").on('click change', function () {
                
                
                var extraaccProcList = "";
                $('input[name*=extraaccessories]:checked').each(function () {
                    var value = this.value;
                    extraaccProcList += (extraaccProcList == "" ? value : "+" + value);
                });

                $("#extraccslct").val(extraaccProcList);                
                
                //$('input:checkbox[name=baseaccessories]').attr('checked', false);
                /*var totalCheckboxes = $('input[name=extraaccessories]:checkbox').length;
                if (totalCheckboxes > 1) {
                    $(".extraaccessories_class").attr("checked", false); //uncheck all checkboxes
                    if ($(this).attr("value") == chkd) {
                        $(this).attr("checked", false);  //check the clicked one
                        chkd = "";
                    } else {
                        $(this).attr("checked", true);  //check the clicked one 
                        chkd = $(this).attr("value");
                    }
                }*/
                
                
                updateValues();
            });
            /*********** On change Extra Acessory END ********************/

            /************* On change Processing START *************/
            $("input[name*=baseprocessing]:radio").change(function () {
                updateValues();
            });
            /*********** On change Processing END ********************/

            /************* On change Processing START *************/
            var expchkd = "";
            var expchkdcnt = 0;
            
            
            function setBaseSpecialData(base_id){
                
                var perimeter = 0;
                var distance = $("#base_distance_special_"+base_id).prop('value');
                var dimension = $('input[name=dimensions]:checked').val();

                if ((dimension !== null) && (dimension !== "custom")) {
                    var dim_dets = "#bd" + dimension;
                    var dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    var productwidth = res[0];
                    var productheight = res[1];
                }
                
                if(dimension.toLowerCase() === "custom"){
                    productwidth = $("#productWidth").prop('value');
                    productheight = $("#productHeight").prop('value');
                }

                if (productheight === "") {
                    productheight = 100;
                }

                if (productwidth === "") {
                    productwidth = 100;
                }
                
                if($("#baseisExtraLeft").prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productheight);
                }
                if($("#baseisExtraRight").prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productheight);
                }
                if($("#baseisExtraBottom").prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productwidth);
                }
                if($("#baseisExtraTop").prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productwidth);
                }
                
                var special_number = Math.floor(parseInt(perimeter)/parseInt(distance));

                //console.log('special: ' + special_number );
                $("#base_number_special_span_"+base_id).html(special_number);
                $("#base_number_special_"+base_id).val(special_number);
                
                updateValues();
                
            }
            
            //////// BASE PROCESSING SIDES CHECKBOXES //////////////////////////
            ///////////////////////////////////////////////////////////////
            //$(".number_special_extra, #number_special_div_9").show();
            $(".base_side").click(function () {
                
                var special = $(this).data("special");
                if(special === 1){//controllo se si tratta di una lavorazione speciale (occhielli / laccetti)
                    
                    var base_id = $(this).data("base-id");

                    if ($(this).prop('checked') === true){
                        $("#base_distance_special_div_"+base_id).show();
                        if($("#base_distance_special").val() !== ""){//se è stata selezionata la distanza allora calcolo/ricalcolo il numero e prezzo
                            setBaseSpecialData(base_id);
                        }
                      //rimuovo eventuali span di errore
                      $(this).parents('.order-view-top').find('span.error').remove();
                        
                    }else{
                        if( //controllo se tutti i lati non sono selezionati allora tolgo informazioni aggiuntive
                            $("#baseisExtraLeft").prop('checked') === false
                            && $("#baseisExtraRight").prop('checked') === false
                            && $("#baseisExtraBottom").prop('checked') === false
                            && $("#baseisExtraTop").prop('checked') === false
                          ){
                            $("#base_distance_special_"+base_id).val("");
                            $("#base_distance_special_div_"+base_id).hide();
                            $("#base_number_special_"+base_id).val("");
                            $("#base_number_special_div_"+base_id).hide();
                          }else{//altrimenti se è stata selezionata la distanza calcolo/ricalcolo il numero e prezzo
                              if($("#base_distance_special").val() !== ""){
                                setBaseSpecialData(base_id);
                              }
                          }
                    }
                
                }

            });
            
            //////// BASE PROCESSING DISTANCE SPECIAL SELECT //////////////////////////
            ///////////////////////////////////////////////////////////////
            
            $(".base_distance_special").change(function () {
                
                var selected_name = $(this).prop('name');
                var base_id_arr = selected_name.split("_");
                var base_id = base_id_arr[3];
                
                var selected_value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                
                if(selected_value !== ""){
                    $("#base_number_special_div_"+base_id).show();
                    setBaseSpecialData(base_id);                    
                    //rimuovo eventuali span di errore
                    $('#base_distance_special_'+base_id+'-error').remove();
                }else{
                    $("#base_number_special_"+base_id).val("");
                    $("#base_number_special_div_"+base_id).hide();
                }

            });
            
            
            //////// BASE PROCESSING NUMBER SPECIAL INPUT //////////////////////////
            ///////////////////////////////////////////////////////////////
            
            $(".base_number_special").change(function () {
                                
                var text_value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                var base_id = $(this).data("base-id");
                
                if(text_value === "" || !$.isNumeric( text_value ) ){
                    alert("Deve essere un numero!");
                    setBaseSpecialData(base_id);
                    $(this).focus();
                }else{
                    updateValues();
                }

            });
            
            
            function setExtraSpecialData(extra_id){
                
                var perimeter = 0;
                var distance = $("#distance_special_"+extra_id).prop('value');
                var dimension = $('input[name=dimensions]:checked').val();

                if ((dimension !== null) && (dimension.toLowerCase() !== "custom")) {
                    var dim_dets = "#bd" + dimension;
                    var dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    var productwidth = res[0];
                    var productheight = res[1];
                }
                
                if(dimension.toLowerCase() === "custom"){
                    productwidth = $("#productWidth").prop('value');
                    productheight = $("#productHeight").prop('value');
                }

                if (productheight === "") {
                    productheight = 100;
                }

                if (productwidth === "") {
                    productwidth = 100;
                }
                
                var num_lati = 0;
                var left = 0;
                var right = 0;
                var bottom = 0;
                var top = 0;
                
                if($("#isextraleft_"+extra_id).prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productheight);
                    num_lati++;
                    left = 1;
                }
                if($("#isextraright_"+extra_id).prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productheight);
                    num_lati++;
                    right = 1;
                }
                if($("#isextrabottom_"+extra_id).prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productwidth);
                    num_lati++;
                    bottom = 1;
                }
                if($("#isextratop_"+extra_id).prop('checked') === true){
                    perimeter = parseInt(perimeter) + parseInt(productwidth);
                    num_lati++;
                    top = 1;
                }

                var special_number = Math.floor(parseInt(perimeter)/parseInt(distance));
                
                if(num_lati < 4){
                    if(num_lati == 2){
                        
                        if( (left == 1 && right == 1) || (top == 1 && bottom == 1)){
                            special_number = special_number + 2;
                        }else{
                             special_number = special_number + 1;
                        }
                        
                    }else{
                        special_number = special_number + 1;
                    }
                }

                if ((special_number %2) == 1 && (num_lati % 2) == 0) {
                    special_number++;
                }
                
                $("#number_special_span_"+extra_id).html(special_number);
                $("#number_special_"+extra_id).val(special_number);
                
                updateValues();
                
            }
            
            $('#cstdisp input').on('change' , function(){
         $('#productHeight').val($('#productHeight').val().replace(",", "."));
         $('#productWidth').val($('#productWidth').val().replace(",", "."));
          var productHeight = parseFloat($('#productHeight').val());
        var productWidth = parseFloat($('#productWidth').val());
              
        if( productHeight != '' && productWidth != '' ){
          var material_id = $(".material_class:checked").val();
          var maxheight = parseFloat($('input[name=dbheight2-'+material_id+']').val());
          var maxwidth = parseFloat($('input[name=dbwidth2-'+material_id+']').val());

          if (productHeight > maxheight || productWidth > maxwidth) {
            if (productHeight > maxwidth || productWidth > maxheight) {
                //console.log("fuori");
                $('#modalmaterial'+material_id).css("display", "block");
                $('#modalclick').click();          
            }
            else {
                  $('#modalmaterial'+material_id).css("display", "none");
              }
          }else{
              $('#modalmaterial'+material_id).css("display", "none");
              
          }
        } else {
        //console.log('una delle dimensioni nn è stata settata!');
        }
        
        $(".number_special").each(function (index) {
            if( $(this).prop('value') !== "" ){
                var extra_id = $(this).data("extra-id");
                setExtraSpecialData(extra_id);
            }
           });  
      });
      
            //////// EXTRA PROCESSING SIDES CHECKBOXES //////////////////////////
            ///////////////////////////////////////////////////////////////
            $(".extra_side").click(function () {
                
                var special = $(this).data("special");
                if(special === 1){//controllo se si tratta di una lavorazione speciale (occhielli / laccetti)
                    
                    var extra_id = $(this).data("extra-id");

                    if ($(this).prop('checked') === true){
                        $("#distance_special_div_"+extra_id).show();
                        if($("#distance_special").val() !== ""){//se è stata selezionata la distanza allora calcolo/ricalcolo il numero e prezzo
                            setExtraSpecialData(extra_id);
                        }
                        
                        //rimuovo eventuali span di errore
                        $(this).parents('.extinfodiv'+extra_id).find('span.error').remove();
                        
                    }else{
                        if( //controllo se tutti i lati non sono selezionati allora tolgo informazioni aggiuntive
                            $("#isextraleft_"+extra_id).prop('checked') === false
                            && $("#isextraright_"+extra_id).prop('checked') === false
                            && $("#isextrabottom_"+extra_id).prop('checked') === false
                            && $("#isextratop_"+extra_id).prop('checked') === false
                          ){
                            $("#distance_special_"+extra_id).val("");
                            $("#distance_special_div_"+extra_id).hide();
                            $("#number_special_"+extra_id).val("");
                            $("#number_special_div_"+extra_id).hide();
                          }else{//altrimenti se è stata selezionata la distanza calcolo/ricalcolo il numero e prezzo
                              if($("#distance_special").val() !== ""){
                                setExtraSpecialData(extra_id);
                              }
                          }
                    }
                
                }

            });
            
            //////// EXTRA PROCESSING DISTANCE SPECIAL SELECT //////////////////////////
            ///////////////////////////////////////////////////////////////
            
            $(".distance_special").change(function () {
                
                var selected_name = $(this).prop('name');
                var extra_id_arr = selected_name.split("_");
                var extra_id = extra_id_arr[2];
                
                var selected_value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                
                if(selected_value !== ""){
                    $("#number_special_div_"+extra_id).show();
                    setExtraSpecialData(extra_id);
                }else{
                    $("#number_special_"+extra_id).val("");
                    $("#number_special_div_"+extra_id).hide();
                }

            });
            
            
            //////// EXTRA PROCESSING NUMBER SPECIAL INPUT //////////////////////////
            ///////////////////////////////////////////////////////////////
            
            $(".number_special").change(function () {
                                
                var text_value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                var extra_id = $(this).data("extra-id");
                
                if(text_value === "" || !$.isNumeric( text_value ) ){
                    //alert("Deve essere un numero!");
                    setExtraSpecialData(extra_id);
                    $(this).focus();
                }else{
                    updateValues();
                }

            });
            
            
            ///////   ACCESSORIES QUANTITY NUMBER ///////////////
            $(".extraaccessories-qnty-Value").change(function () {
                                
                var value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                
                if(value === "" || !$.isNumeric( value ) ){
                    //alert("Deve essere un numero!");
                    $(this).focus();
                }else{
                    updateValues();
                }

            });
            
            //////// EXTRA PROCESSING CHECKBOXES //////////////////////////
            ///////////////////////////////////////////////////////////////

            $(".extraprocessing_class").on('click change',function () {
                
                var possible = true;
                
                var processing_rules_path = $('input[name=processing_rules_path]').val();
                
                var linked = {};
                var forbidden = {};
                var adviced = {};
                
                
                
                $.ajax({
                    type: 'POST',
                    url: processing_rules_path,
                    dataType: 'json',
                    cache: false,
                    success: function(result) {
                        linked = result['linked'];
                        forbidden = result['forbidden'];
                        adviced = result['adviced'];
                    },
                    async: false 
                });
                
                //console.log(linked_top);

                /*
                // Oggetto di elementi collegati tra loro /////////////
                var linked = {
                    extra1: [3, 9],
                    extra3: [4]
                };

                // Oggetto di elementi che non possono coesistere //////////////
                var forbidden = {
                    extra1: [4, 10],
                    extra3: [1, 9]
                };

                // Array dei consigliati /////////////////////
                var adviced = {
                    extra1: [3, 9],
                    extra3: [4, 6]
                };
                
                console.log(linked);*/

                var clicked_value = $(this).prop('value'); // Attributo value dell'elemento cliccato ///////
                
                //console.log(JSON.parse(JSON.stringify(forbidden)));

                // Se più in basso viene settato l'attributo datafather viene inibito il click /////////////
                ////////////////////////////////////////////////////////////////////////////////////////////
                if ($(this).prop('datafather')) {
                    return false;
                }

                // Verifico la compatibilità dei click in base a Forbidden e nel caso passo oltre /////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////////////
                $(".extraprocessing_class").each(function (index) {
                    if ($(this).prop('checked') == true && $(this).prop('value') != clicked_value && $(this).prop('datafather') != clicked_value) {
                        var this_value = "extra" + $(this).prop('value');
                        var click_value = "extra" + clicked_value;
                        //console.log(this_value + " " + click_value);
                            
                        if (typeof forbidden[this_value] !== "undefined") {
                            for (var n = 0; n < forbidden[this_value].length; n++) {

                                //console.log("FORBIDDEN: "+forbidden[this_value][n]);
                                if (clicked_value == forbidden[this_value][n]) {
                                    possible = false;
                                    //alert('La lavorazione non è compatibile con quella selezionata');
                                    showModal("alertModal", Drupal.t("processing not compatible"),Drupal.t("the processing is not compatible with the selected one") );
                                    continue;
                                }
                            }
                        }
                        if (typeof forbidden[click_value] !== "undefined") {
                            for (var n = 0; n < forbidden[click_value].length; n++) {

                                if ($(this).prop('value') == forbidden[click_value][n]) {
                                    possible = false;
                                    //alert('La lavorazione non è compatibile con quella selezionata');
                                    showModal("alertModal", Drupal.t("processing not compatible"),Drupal.t("the processing is not compatible with the selected one") );
                                    continue;
                                }
                            }
                        }
                    }
                });

                // Seleziono deseleziono e stilo checkbox cliccati e apro div nascosti se è verificata condizione possible ///////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                var index = $(this).prop('id').replace('extraprocessing', '');
                if (possible == true) {
                    if ($(this).prop('checked')) {
                        $(this).parents('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--circle__checked"></span>');
                        $(".extinfodiv" + index).show();
                        expchkd = $(this).prop("value");
                    }
                    else {
                        $(this).parents('.dimdiv--circle').find('span.dimdiv--circle__checked').remove();
                        $(".extinfodiv" + index).hide();
                        expchkd = "";
                    }
                } else {
                    return false;
                }

                // seleziona e apre al click i relativi dell'array linked ///////////////
                /////////////////////////////////////////////////////////////////////////
                var this_value_linked = "extra" + $(this).prop('value');

                if (typeof linked[this_value_linked] !== "undefined") {
                    for (var s = 0; s < linked[this_value_linked].length; s++) {
                        if ($(this).prop('checked')) {
                            $('#extraprocessing' + linked[this_value_linked][s]).prop({checked: true, datafather: clicked_value});
                            $('#extraprocessing' + linked[this_value_linked][s]).parents('.dimdiv--circle').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--circle__checked"></span>');
                            $(".extinfodiv" + linked[this_value_linked][s]).show();
                            expchkd = $(this).prop("value");
                        } else {
                            $('#extraprocessing' + linked[this_value_linked][s]).prop({checked: false, datafather: false});
                            $('#extraprocessing' + linked[this_value_linked][s]).parents('.dimdiv--circle').find('span.dimdiv--circle__checked').remove();
                            $(".extinfodiv" + linked[this_value_linked][s]).hide();
                            expchkd = '';
                        }
                    }
                }

                // stampa i consigliati ///////////////
                /////////////////////////////////////////////////////////////////////////
                var title = '';
                if (typeof adviced[this_value_linked] !== "undefined") {
                    for (var a = 0; a < adviced[this_value_linked].length; a++) {
                        title += $('#extraprocessing' + adviced[this_value_linked][a]).siblings('p').text() + '<br />';
                    }
                    if(title !== ''){
                        $('#adviced').toggle();
                        if ($(this).prop('checked')) {
                            $('#adviced').append('<p>Ti consigliamo anche: <strong>' + title + '</strong></p>');
                        } else {
                            $('#adviced p').remove();
                        }
                    }
                }


                var extraProcList = "";
                var verniciatura = false;
                $('input[name*=extraprocessing]:checked').each(function () {
                    var value = this.value;                    
                    extraProcList += (extraProcList == "" ? value : "+" + value);                    
                    
                    if(value == 13) {
                        verniciatura = true;
                    }
                  
                });
                
                if(verniciatura){
                    //$("#date-q0-d0").attr("disabled", "disabled").off('click');
                    $("#date-q0-d0").prop("disabled", true);
                    $("#date-q0-d0").prop("checked", false);
                    $("#date-q0-d1").prop("checked", true).trigger('click');
                    //$("#ship-table .ship-cell-fixed").trigger( "click" );
                    if (!$('#shippingdateprintdiv0').hasClass('section-disable')){
                        $('#shippingdateprintdiv0').addClass('section-disable').append('<div class="overlay-disable"></div>');
                    }
                    $('#shippingdateprintdiv0').css( 'cursor', 'default' );
                    

                }else{

                    $("#date-q0-d0").prop("disabled", false);
                    
                    $('#shippingdateprintdiv0').removeClass('section-disable');
                    $('.overlay-disable').remove();
                    
                    $('#shippingdateprintdiv0').css( 'cursor', 'pointer' );
                }
                                
                $("#extrslct").val(extraProcList);
                
//     $('input[name*=extraprocessing]').each(function () {
//        if( $(this).prop('checked') == false ){
//            var value = $(this).prop('value');
//            var calc_ids = $("#extrcalc").prop('value');
//            //console.log(calc_ids);
//            var piu = "+"+value;
//                if(calc_ids.indexOf(piu) > -1){
//                    var new_calc = calc_ids.replace("+"+value, "");
//                    $("#extrcalc").prop( 'value' , new_calc);
//                }
//                else if(calc_ids.indexOf(value) > -1){
//                    var new_calc = calc_ids.replace(value, "");
//                    $("#extrcalc").prop( 'value' , new_calc);
//                }
//            }
//            //console.log(new_calc2);
//     });

                updateValues();

            });


            // EXTRA OPERATOR REVIEW ///////////////////////
            ////////////////////////////////////////////////

            $('#operatorreview').click(function () {
                if ($(this).prop('checked')) {
                    $(this).parents('.dimdiv--operator-extra').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--operator-extra__checked"></span>');
                    expchkd = $(this).prop("value");
                }
                else {
                    $(this).parents('.dimdiv--operator-extra').find('span.dimdiv--operator-extra__checked').remove();
                    expchkd = "";
                }
            });

            // EXTRA OPERATOR REVIEW ///////////////////////
            ////////////////////////////////////////////////

            $('#annonshipping').click(function () {
                if ($(this).prop('checked')) {
                    $(this).parents('.dimdiv--anonimous').append('<span class="fa fa-check-circle dimdiv__checked dimdiv--operator-extra__checked"></span>');
                    expchkd = $(this).prop("value");
                }
                else {
                    $(this).parents('.dimdiv--anonimous').find('span.dimdiv--operator-extra__checked').remove();
                    expchkd = "";
                }
            });
             
            //////// PRICE TABLE FIXED //////////////////////////
            ///////////////////////////////////////////////
            
            $('#ship-table .ship-cell-fixed').click(function () {
                
                var splittedResult = $(this).attr('id').split('-');
                var day = splittedResult[1];
                var quantity = splittedResult[0];
              
                $('#ship-table .fa-check-circle').remove();
                $('.ship-cell-fixed').removeClass('selected');
                $(this).append('<span class="fa fa-check-circle dimdiv__checked date-div__checked"></span>').addClass('selected');
                $('#ship-table th').removeClass('day-selected');
                $('#' + day).addClass('day-selected');
                $('.ship-col').removeClass('quantity-selected');
                $('#' + quantity).addClass('quantity-selected');
                
                $('.date-div input').prop('checked', false);
                $('.date-div span.dimdiv__checked').remove();
                $('#'+day).prop('checked', true);
                
                var only_quantity = $("#"+quantity).data('value');
                
                $("#productQuanity").val(only_quantity);
                
                var day_n = day.substring(1, 2);
                var date_txt = $("#shippingdateprintdiv"+day_n).data("shippingdate");
                //console.log(day+" "+only_quantity);
                
                $("#shipping_date").val(date_txt);
                $(".shippingdatediv").html(date_txt);
                
                updateValues();
                
            });
            
            
            /*********** On change Processing END ********************/

            /************* On change Shipping START *************/
            $("input[name=shipping]:radio").change(function () {
                updateValues();
            });
            /*********** On change Shipping END ********************/

            $(".extra_side").change(function () {
                //console.log("PUPPA");
                updateValues();
            });
            /************* On change Dimesions START *************/
            //$("input[name=dimensions]:radio").change(function () {
                //updateFixedCosts();
                /* La funzione updateValues() viene richiamata direttamente da updateFixedCosts() */
                //updateValues();

            //});
             $('.section-dimension').on("click",'input[type="radio"]',function() {
                 updateValues();
                 
                 //aggiorno anche il numero di occhielli/laccetti nel caso di lavorazioni speciali
                 $(".number_special").each(function (index) {
                     if( $(this).prop('value') !== "" ){
                         var extra_id = $(this).data("extra-id");
                         setExtraSpecialData(extra_id);
                     }
                 });
                 
             });
            /*********** On change Dimensions END ********************/

            /************* On change Operator Review Start*************/
            $("input[name=operatorreview]").change(function () {
                updateValues();
            });
            /*********** On change Operator Review END ********************/

            /************* On change long term prouction start ******************/
            $(".lngtmprdctnclass").click(function () { //debugger;
                /*   var totalCheckboxes = $('input[name=lngtmprdctn]:checkbox').length;
                 if(totalCheckboxes > 1){
                 $(".lngtmprdctnclass").attr("checked", false); //uncheck all checkboxes
                 if($(this).attr("value") == expchkd ){
                 $(this).attr("checked", false); 
                 expchkd = "";
                 }else{
                 $(this).attr("checked", true);
                 expchkd = $(this).attr("value");
                 }
                 }*/

                //updateDates();
                updateValues();

            });
            /************* On change long term prouction start ******************/

            function getDataProd(mese, giorno, lingua) {

                var monthNames = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];
                var dayNames = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];

                var monthNamesFr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
                var dayNamesFr = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];

                var meseOut = "";
                var giornoOut = "";

                if (lingua == "it") {
                    meseOut = monthNames[mese];
                    giornoOut = dayNames[giorno];
                } else {
                    meseOut = monthNamesFr[mese];
                    giornoOut = dayNamesFr[giorno];
                }

                var result = [meseOut, giornoOut];

                return result;

            }

            function updateDates() {

                var matdaysj = 0;
                var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();
                var productgroupidj = $('#productgroupid').val();

                var formidj = $('#formid').val();
                var proddelayj = $('#production_delay').val();
                var languageidj = $('#languageid').val();

                var delytimj = 0;
                var lngtmprdctnvals = 0;
                var lngrmprdtnid = "#lngtm" + lngtmprdctn;
                var lngtmprdctnvals = $(lngrmprdtnid).val();
                if ((lngtmprdctn != "") && (lngtmprdctn != null)) {
                    var lngtrmprdctnvl = lngtmprdctnvals.split("+");
                    delytimj = lngtrmprdctnvl[0];
                }

                var workloadtypej = $("#workloadtype").val();
                var workloadlimitj = $("#workloadlimit").val();
                var workloaddailyj = $("#workloaddaily").val();
                var formtype = $("#formtype").val();
                if ((workloadlimitj == null) || (workloadlimitj == "")) {
                    workloadlimitj = 0
                    workloadtypej = "Products";
                }

                if (formtype == 'small') {

                    var material_id_selected_name = $("#smaterialId option:selected").val();
                    var material_prices = "#smtid" + material_id_selected_name;
                } else {
                    var material_id_selected_name = $(".material_class:checked").val();
                    var material_prices = "#mtid" + material_id_selected_name;
                }

                var materialdet = $(material_prices).val();

                if (materialdet != null) {
                    var matdetails = materialdet.split("+");
                    matdaysj = matdetails[4];
                } else if (materialdet == null) {
                    matdaysj = 0;
                }

                var dates_path = $('input[name=dates_path]').val();

                $.ajax({
                    url: dates_path,
                    data: {
                        mtrldys: matdaysj,
                        delytim: delytimj,
                        workloadlimit: workloadlimitj,
                        workloadtype: workloadtypej,
                        workloaddaily: workloaddailyj,
                        productgroupid: productgroupidj,
                        formid: formidj,
                        proddelay: proddelayj,
                        languageid: languageidj
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (data) {
                        console.log(data);
                        var dates = data.split("**")
                        var datelength = dates.length;
                        datelength = parseFloat(datelength) - 1;
                        for (var i = 0; i < datelength; i++) {
                            
                            var dates_count = $("#dates_count").val();
                            
                            var data_arr = dates[i].split("-");
                            var giorno_mese = data_arr[0];
                            var mese = data_arr[1];
                            var anno = data_arr[2];
                            
                            if( i == 0 && dates_count == 0 ){
                                $("#shipping_date").val(dates[i]);
                                $(".shippingdatediv").html(dates[i]);
                                var currentTime = new Date();
                                
                                var frimon= 0;
                                if (currentTime.getDay() == 5 && (isDate(currentTime.getDate(), giorno_mese, mese, anno, 3) || isDate(currentTime.getDate(), giorno_mese, mese, anno, 4)))
                                    frimon = 1;
       
                                if (currentTime.getDay() == 6 && isDate(currentTime.getDate(), giorno_mese, mese, anno, 2) )
                                    frimon = 1;
                                
                                if (currentTime.getDay() == 0 && isDate(currentTime.getDate(), giorno_mese, mese, anno, 1) )
                                    frimon = 1;
                                
                                

                                if (currentTime.getDate() == giorno_mese || isDate(currentTime.getDate(), giorno_mese, mese, anno, 1) || isDate(currentTime.getDate(), giorno_mese, mese, anno, 2) || frimon == 1){
                                    $("#lessthan").val('1');
                                }
                            }
                            
                            var shipingdivid = "#shippingdateprintdiv" + i;

                            var d = new Date(anno, mese - 1, giorno_mese);

                            var giorno_sett = d.getDay();
                            var mese_from = d.getMonth();

                            var arr_date = getDataProd(mese_from, giorno_sett, languageidj);

                            var mese_html = arr_date[0];
                            var giorno_html = arr_date[1];

                            var html = "<div class='date-div__day'>" + giorno_html + "</div><div class='date-div__day-n'>" + giorno_mese + "</div><div class='date-div__month'>" + mese_html + "</div>";
                            
                            $(shipingdivid).data("shippingdate", dates[i]);
                            $(shipingdivid).html(html);
                            
                            $("#dates_count").val(i);
                            
                            if(currentTime.getDate() == giorno_mese) {
                                $('#beforenoon').val('1');
                            }
                        }
                        if ($('#beforenoon').val() == "1") {
                            $('.d0').hide();
                            $('.dx').show();
                        }
                        else {
                            $('.dx').hide();
                            $('.d0').show();
                        }
                    },
                    complete: function() {
                        $('#d2 .date-div input').click();
                        $('#q1-d2').click();
                        updateValues();
                    }
                });
            }

            /************* On change long term prouction End ******************/

            function isDate(day, actualday, month, year, difference) {
                //console.log(day, actualday, month, year, difference);
                if (difference != 0) {
                    day = parseInt(day) + parseInt(difference);
                    if (day <= 0) {
                        switch (month) {
                            case 1,3,5,7,8,10,12: day = 31 + day; break; 
                            case 4,6,9,11: day = 30 + day; break; 
                            case 2: 
                                if (year % 4 == 0)
                                    day= 29 + day;
                                else
                                    day= 28 + day;
                            break;
                        }
                    }
                    else {
                        if (day > 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12))
                            day = day - 31;
                        else if (day > 30 && (month == 4 || month == 6 || month == 9 || month == 11))
                            day = day - 30;
                        else if (day > 29 && month == 2 && year %4 == 0)
                            day = day - 29;
                        else if (day > 28 && month == 2 && year %4 != 0)
                            day = day - 28;
                    }
                }
                   
                if (parseInt(actualday) == parseInt(day))
                    return true;
                else
                    return false;
            }
                
            function updateFixedCosts() {
                
                //var fixed_costs_path = $('input[name=fixed_costs_path]').val();
                var material_id = $(".material_class:checked").val();
                var form_id = $('#formid').val();
                var dimid= $('input[name=dimensions]:checked').val();
                //console.log(material_id + ' ' + form_id);

                $.ajax({
                    url: "/get-fixed-costs",
                    data: {
                        mat_id: material_id,
                        form_id: form_id
                    },
                    dataType: "html",
                    //processData: false,
                    //contentType: false,
                    type: "POST",
                    success: function (data) {
                        $("#dimensions-div").html(data);
                        $('.section-dimension .dimension-input:checked').parent().append('<span class="fa fa-check-circle dimdiv__checked"></span>');
                        updateDates();
                        updateValues();
                        $('[data-toggle="tooltip"]').tooltip();
                    },
                    error: function(data) {
                        //console.log("Error!");
                    },
                    complete: function() { 
                        var numItems = $('#dimensions-div .dimdiv').length;
                        if (numItems > 0) {
                            $('#dimensions').prop('checked', false);
                            $('.dimdiv--custom-size span.dimdiv__checked').remove();
                        }
                        $('input[name=dimensions][value='+dimid+']').click();
                    }
                });
            }
            
            /**********Multiplication Formula for Big Format Start *************/

            function updateValues() {
                // debugger;
                //updateShipping();
                //  alert($('input[name=htotalshippingprice]').val());
                var material_id_selected_name = $(".material_class:checked").val();
                var material_prices = "#mtid" + material_id_selected_name;
                var noOfProducts = $('#productQuanity').val();
                var materialdet = $(material_prices).val();
                var productheight = $('#productHeight').val();
                var productwidth = $('#productWidth').val();
                var extradef = $('#extraDef').val();
                var addtnlPricePerCopy = $('#additonal_price_copy').val();
                var detCostCopy = $('#det_cost_copy-' + material_id_selected_name).val();
                //var priceCalculationFunction = $('#price_calculation_function').val();
                var basePrice = $('#base_price').val();
                var dimensionFinder = $('input[name=dimensions]:checked').val();
                
                if (detCostCopy == 0)
                    detCostCopy = 1;
                
                //var shippingCost = $('input[name=htotalshippingprice]').val();
                var shippingCost = 0;
                //alert("ttt"+shippingCost);
                //alert("visal");
                var getbaseaccId = $('input[name=baseaccessories]:checked').val();
                var baseaccId = "#baseacc" + getbaseaccId;
                var baseacc_type = $('input[name=baseaccessories]:checked').prop('id');
                //console.log("Struttura tipo: "+baseacc_type);
                var baseaccessories = $(baseaccId).val();
                //var getexaccId = $('input[name=extraaccessories]:checked').val();
                //var exaccId = "#extacc" + getexaccId;
                //var extraaccessories = $(exaccId).val();
                
                var promotionalflag = "percentage";
                var promotype = 0;
                var promoval = 0;
                var dim_dets = "";
                var dimension_details = 0 + "*" + 0;
                var costcopies = $('#costCopies' + material_id_selected_name).val();

                //var extprcsid = $('input[name*=extraprocessing]:checked').val();
                

                //LAVORAZIONI ACCESSORIE SELEZIONATE
                var extrslct = $('#extrslct').val();
                
                if (typeof extrslct !== "undefined" && extrslct != '') {
                    if (extrslct.indexOf("+") > -1) {
                        var extprcsids = extrslct.split("+");
                    } else {
                        var extprcsids = [extrslct];
                    }
                }
                
                // ACCESSORI SELEZIONATI
                var extraccslct = $('#extraccslct').val();
                if (typeof extraccslct !== "undefined" && extraccslct !== '') {
                    if (extraccslct.indexOf("+") > -1) {
                        var extaccsids = extraccslct.split("+");
                    } else {
                        var extaccsids = [extraccslct];
                    }
                }
                //var extdefdivlist = $('#extdefdividlst').val();
                var promoprice = 0;
                var operatorreview = $('input[name=operatorreview]:checked').val();
                var fbprices = $('.fronteretro_class:checked').val();
                var material_price = 0;
                var matheight = 0;
                var matwidth = 0;
                var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();     
                var lantrmdays = 0;
                var lantrmamnt = 0;
                var lantrmtype = "value";
                var extrapackingcheck = $('#packingpricecheck').val();
                var extrapackingdetails = $('#packingpricevalues').val();
                var extraprackingprice = 0;
 
                if (fbprices == undefined)
                    fbprices = 0;
                
                // ESTRAZIONE DATI DELLA DATA DI PRODUZIONE SELEZIONATA
                var lngrmprdtnid = "#lngtm" + lngtmprdctn;
                
                //console.log(lngrmprdtnid);
                
                var lngtmprdctnvals = $(lngrmprdtnid).val();
                
                if ((lngtmprdctn != "") && (lngtmprdctn != null)) {
                    var lngtrmprdctnvl = lngtmprdctnvals.split("+");
                    lantrmdays = lngtrmprdctnvl[0];
                    lantrmamnt = lngtrmprdctnvl[1];
                    lantrmtype = lngtrmprdctnvl[2];
                    if (lantrmtype == "") {
                        lantrmtype = "value";
                    }
                    if (lantrmamnt == "") {
                        lantrmamnt = 0;
                    }
                    if (lantrmdays == "") {
                        lantrmdays = 0;
                    }
                }
                
                
                /* TODO togliere, non ci serve
                 if(extprcsid != null){
                 var extdefdivcount = extdefdivlist.split("+");
                 var extdefdivlength = extdefdivcount.length;
                 for (var i=0; i<extdefdivlength; i++){
                 var epextradefdivcondt = "#epextradefdiv"+extdefdivcount[i]; 
                 var epextraprcsdivcont = "#epextraprcsdiv"+extdefdivcount[i];       
                 if(extdefdivcount[i] == extprcsid){
                 $(epextradefdivcondt).show();
                 $(epextraprcsdivcont).show();
                 }else{
                 $(epextradefdivcondt).hide();
                 $(epextraprcsdivcont).hide();
                 }
                 }
                 }
                 /******/
                
                //controllo se il materiale selezionato è a prezzo fisso
                var fixed_costs = $("#fixed_costs").val();
                var is_fixed_cost = false;
                var fixed_cost_price = 0;

                if(fixed_costs == 1 && dimensionFinder.toLowerCase() != "custom"){
                    is_fixed_cost = true;
                    fixed_cost_price = $("#fc_price_"+dimensionFinder).val();
                }

                $('.fixed-amounts').each(function () {
                    if (noOfProducts == $(this).data('value') && $(this).data('discount') && $(this).data('discount') != 0) {
                        promoval= $(this).data('discount');
                        promotype= "percentage";
                        promotionalflag= "promo";
                    }
                });
                
                if (dimensionFinder.toLowerCase() == "custom") {
                    $('#cstdisp').show();
                } else {
                    $('#cstdisp').hide();
                }

                if ((operatorreview == "") || (operatorreview == null)) {
                    operatorreview = 0;
                } else {
                    operatorreview = $('#file_review_cost').val();;
                }

                /*    if(dimensionFinder == "custom"){
                 $('#cstdisp').show();
                 }else{
                 $('#cstdisp').hide();
                 } */

                if (noOfProducts == "") {
                    noOfProducts = 1;
                }

                if (addtnlPricePerCopy == "") {
                    addtnlPricePerCopy = 0;
                }

                if (baseaccessories == null) {
                    baseaccessories = 0;
                }
                
                /*
                 * Controllo se è stato selezionata la vendita della stampa senza struttura
                 * Se si, tolgo il prezzo della struttura
                */
                if(baseacc_type === "only_print") {
                    baseaccessories = 0;
                }

                //if (extraaccessories == null) {
                    //extraaccessories = 0;
                //}

                if ((dimensionFinder != null) && (dimensionFinder.toLowerCase() != "custom")) {
                    dim_dets = "#bd" + dimensionFinder;
                    dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    var productwidth = res[0];
                    var productheight = res[1];
                }

                if (productheight == "") {
                    productheight = 100;
                }

                if (productwidth == "") {
                    productwidth = 100;
                }
                
                $.ajax({
                    url: "/get_custom_template",
                    data: {
                        dimfind: dimensionFinder,
                        width: productwidth,
                        height: productheight,
                        type: 1
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (data) {
                        //IMPOSTO URL PER IL DOWNLOAD DEL TEMPLATE
                        $("#template-download").attr("href", data);
                    }
                });
                
                var totalsqarea = parseFloat(productwidth) * parseFloat(productwidth) * parseFloat(noOfProducts);
                totalsqarea = parseFloat(totalsqarea) / 10000;
                if (extrapackingcheck == 1) {
                    var ecxpcklvlone = extrapackingdetails.split("__");
                    var ecxpcklvlonelngth = ecxpcklvlone.length;
                    ecxpcklvlonelngth = ecxpcklvlonelngth - 1;
                    for (var i = 0; parseFloat(i) < parseFloat(ecxpcklvlonelngth); i++) {
                        var ecxpcklvltwo = ecxpcklvlone[i].split("+");
                        if ((parseFloat(ecxpcklvltwo[1]) >= parseFloat(totalsqarea)) && (parseFloat(ecxpcklvltwo[0]) <= parseFloat(totalsqarea))) {
                            extraprackingprice = ecxpcklvltwo[2];
                        }
                    }
                }


                if (materialdet != null) {
                    var is_min_sm = false;
                    var matdetails = materialdet.split("+");
                    
                    var material_price = matdetails[0];
                    matheight = matdetails[2];
                    matwidth = matdetails[3];
                    material_price = parseFloat(material_price);
                    if ((matheight == null) || (matheight == "")) {
                        matheight = 0;
                    }
                    if ((matwidth == null) || (matwidth == "")) {
                        matwidth = 0;
                    }
                    matheight = parseFloat(matheight);
                    matwidth = parseFloat(matwidth);
                    
                    if (parseFloat(productheight) > parseFloat(matheight)) {
                        matheight = productheight;
                    }
                    if (parseFloat(productwidth) > parseFloat(matwidth)) {
                        matwidth = productwidth;
                    }
                    if (matdetails[1] == 1) {
                        if (parseFloat(noOfProducts * (productheight * productwidth)) <= 10000) {
                            //console.log(productheight + " + matwidth: " + productwidth + " + nof: " + noOfProducts);
                            matheight = 100;
                            matwidth = 100;
                            is_min_sm = true;
                        }else{
                            matheight = productheight;
                            matwidth = productwidth;
                        }
                    }
                    
                    //CONTROLLO SE LE MISURE SONO INFERIORI ALLE MISURE MINIME CONSENTITE, IN QUESTO CASO CALCOLO IL PREZZO SULLE MISURE MINIME
                    
                    var width_min_length = matdetails[7];
                    var height_min_length = matdetails[8];
                    
                    width_min_length = parseFloat(width_min_length);
                    height_min_length = parseFloat(height_min_length);
                    
                    if(matwidth < width_min_length){
                        matwidth = width_min_length;
                    }
                    
                    if(matheight < height_min_length){
                        matheight = height_min_length;
                    }
                    
                    //console.log('Mat width: '+width_min_length);
                    //console.log('Mat height: '+height_min_length);
                    
                    //FINE CONTROLLO MISURE MINIME
                    

                }
                    
                if (materialdet == null) {
                    var matheight = 0;
                    var matwidth = 0;
                    var material_price = 0;
                }


                if (extradef == "") {
                    extradef = 1;
                }


                matheight = parseFloat(matheight / 100);
                matwidth = parseFloat(matwidth / 100);
                
                productheight = parseFloat(productheight / 100);
                productwidth = parseFloat(productwidth / 100);
                
                var processingCost = baseprocessingPrice();
                if ((processingCost == "") || (processingCost == null)) {
                    processingCost = 0;
                }

                if ((shippingCost == "") || (shippingCost == null)) {
                    shippingCost = 0;
                }

                /***
                 * 
                 * CALCOLO COSTO LAVORAZIONI ACCESSORIE
                 */
                //var extraprocssing = extraprocessingPrice();
                var extraprocssing = parseFloat(0);
              
                if (typeof extprcsids !== "undefined") {
                    
                    extprcsids.forEach(function (entry) {
                        //var calc_val = $("#extrcalc").val();
                        //if(calc_val.indexOf(entry) == -1){
                     
                        $("#extprcs_cost_"+entry).val(parseFloat(extraprocessingPrice(entry)));

                        extraprocssing += parseFloat(extraprocessingPrice(entry));

                        //if(calc_val == ""){
                        //calc_val += entry;
                        //}else{
                        //calc_val += "+"+entry;
                        //}
                        //$("#extrcalc").val(calc_val);
                        //}
                    });
                   
                    //extraprocssing -= parseFloat(extraprocessingPrice(entry));
                }

                /***
                 * 
                 * CALCOLO COSTO ACCESSORI
                 */
                var extraaccessories = parseFloat(0);
                if (typeof extaccsids !== "undefined") {
                    extaccsids.forEach(function (entry) {
                        var extraaccqnty = $("#extraaccessories-qnty_"+entry).val();
                        var extraaccprice = parseFloat($("#extacc"+entry).val());
                        //console.log("Accessori qnty: "+ extraaccqnty);
                        //console.log("Accessori price: "+ extraaccprice);
                        extraaccessories += parseFloat(extraaccprice*extraaccqnty);
                    });
                }
                
                if (detCostCopy == 1) {
                    
                    if(is_fixed_cost && fixed_cost_price != 0){
                        var matrlpricefrml = fixed_cost_price;
                    }else{
                        var matrlpricefrml = parseFloat(material_price * matheight * matwidth);
                    }
                    
                    var productprice = parseFloat(matrlpricefrml);// + parseFloat(basePrice);
                    var singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                    var sproductprice = singleproductPrice + parseFloat(addtnlPricePerCopy);

                    if (is_min_sm) {
                        var totlPrice = parseFloat(sproductprice) + (parseFloat(processingCost) * parseFloat(noOfProducts) );
                    } else {
                        var totlPrice = parseFloat(noOfProducts) * parseFloat(sproductprice);
                    }
                    //console.log('****************************' + shippingCost);
                    
                    //console.log(totlPrice);
                    totlPrice = parseFloat(totlPrice) - parseFloat(addtnlPricePerCopy);
                    totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories) + parseFloat(extraprocssing);
                    totlPrice = parseFloat(totlPrice) + parseFloat(extraprackingprice);
                    
                }

                /***************** varying cost on no of products *********/
                if (detCostCopy == 2) {

                    var totlPrice = 0;
                    if (costcopies != null) {
                        var costsplit = costcopies.split("+");
                        var cstarrlength = costsplit.length
                        cstarrlength = parseFloat(cstarrlength - 1);
                        var pcount = noOfProducts;
                        var _j = 1;
                        var _tprice = 0;
                        var _remProcnt = 0;
                        var matrlpricefrml = 0;
                        var productprice = 0;
                        var processingCost = 0;
                        var singleproductPrice = 0
                        
                        for (var i = 0; i < cstarrlength; i++) {
                            var costbreak = costsplit[i].split(" ");

                            if (parseFloat(pcount) <= parseFloat(costbreak[1]) || !parseFloat(costbreak[1])) {
                                var xyz = pcount - _remProcnt;
                                if(is_fixed_cost && fixed_cost_price != 0){
                                    matrlpricefrml = fixed_cost_price;
                                }else{
                                    matrlpricefrml = parseFloat(costbreak[2]) * parseFloat(matheight) * parseFloat(matwidth);
                                }
                                productprice = parseFloat(matrlpricefrml);// + parseFloat(costbreak[2]);
                                
                                processingCost = baseprocessingPrice();
                                singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                                totlPrice = parseFloat(totlPrice) + parseFloat(xyz * singleproductPrice);
                                break;
                            } else {
//                   if (i == 0){
                                if(is_fixed_cost && fixed_cost_price != 0){
                                    matrlpricefrml = fixed_cost_price;
                                }else{
                                    matrlpricefrml = parseFloat(costbreak[2]) * parseFloat(matheight) * parseFloat(matwidth);
                                }
                                productprice = parseFloat(matrlpricefrml);// + parseFloat(costbreak[2]);
                                processingCost = baseprocessingPrice();
                                singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                                
                                if (i == 0) {
                                    _tprice = parseFloat(_tprice) + parseFloat(costbreak[1] * singleproductPrice);
                                } else {
                                    _tprice = parseFloat(_tprice) + parseFloat((costbreak[1] - _remProcnt) * singleproductPrice);
                                }
                                totlPrice += parseFloat(_tprice);
                                _remProcnt = costbreak[1];
                            }
//               
//              if(parseFloat(pcount) <= parseFloat(costbreak[1])){
//                  if(i=0){
//                  totlPrice = parseFloat(pcount * singleproductPrice); 
//                  }else{
//                      var onevar = parseFloat(costbreak[1]) - parseFloat(pcount);
//                     totlPrice = parseFloat(totlPrice) + parseFloat(onevar * singleproductPrice); 
//                    }
//                  var nocount = noOfProducts - costbreak[1];
//                  if(nocount <= 0){
//                      break;
//                  }
//              }
//              if(parseFloat(pcount) > parseFloat(costbreak[1])){
//                  var nocount = noOfProducts - costbreak[1];
//                  if(nocount>0){
//                  totlPrice = parseFloat(totlPrice) + parseFloat(costbreak[1] * singleproductPrice);
//                  }if(nocount<0){
//                   nocount = costbreak[1] - noOfProducts; 
//                  totlPrice = parseFloat(totlPrice) + parseFloat(nocount * singleproductPrice);                  
//                   }
//              }
                        }
                    }
                    //var extraprocssing = extraprocessingPrice();
                    totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories) + parseFloat(extraprocssing);
                    totlPrice = parseFloat(totlPrice) + parseFloat(extraprackingprice);  
                }

                /***************** varying cost on no of square area *********/
                if (detCostCopy == 3) { // && ((!is_fixed_cost) || (dimensionFinder == "custom"))) {
                    
                    var totlPrice = 0;
                    if (costcopies != null) {
                        var costsplit = costcopies.split("+");
                        var cstarrlength = costsplit.length
                        cstarrlength = parseFloat(cstarrlength - 1);
                        //var prdctarea = parseFloat(productheight) * parseFloat(productwidth) * noOfProducts;
                        var prdctarea = parseFloat(matheight) * parseFloat(matwidth) * noOfProducts;
                        // controllo flag min 1mq
                        
                        if (is_min_sm) {
                            prdctarea = 1;
                        }
                        
                        if(is_fixed_cost && fixed_cost_price != 0){
                            productprice = totlPrice = parseFloat(fixed_cost_price) * noOfProducts;
                        }
                        else {
                            for (var i = 0; i < cstarrlength; i++) {
                                var costbreak = costsplit[i].split(" ");
                                //if (costbreak[0]== 1) costbreak[0]= 0;
                        
                                if (costbreak[2] !== undefined)
                                    price= costbreak[2];
                                
                                if (parseFloat(prdctarea.toFixed(2)) <= parseFloat(costbreak[1]) || !parseFloat(costbreak[1])) {
                                    //console.log(prdctarea.toFixed(2));
                                    var prdPrice = costbreak[2];
                                    var matrlpricefrml = parseFloat(costbreak[2]) * parseFloat(prdctarea);
                                    var productprice = parseFloat(matrlpricefrml);// + parseFloat(costbreak[2]);
                                    totlPrice = parseFloat(totlPrice) + (parseFloat(productprice));
                                    break;
                                }
                            }
                            if (isNaN(productprice)) {
                                productprice = parseFloat(price) * parseFloat(prdctarea);
                                totlPrice = parseFloat(totlPrice) + (parseFloat(productprice));
                            }
                        }
                    }
                    
                    processingCost2 = baseprocessingPrice() * noOfProducts;
                    productprice= parseFloat(totlPrice / noOfProducts);
                    baseaccessories *= noOfProducts;
                    //var extraprocssing = extraprocessingPrice();

                    //var totlPrice = parseFloat(noOfProducts * singleproductPrice);

                    /*if (is_min_sm) {
                        var totlPrice = parseFloat(singleproductPrice);
                    } else {
                        var totlPrice = parseFloat(noOfProducts * singleproductPrice);
                    }*/

                    totlPrice = parseFloat(totlPrice) + parseFloat(processingCost2) + parseFloat(baseaccessories);
                    totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories) + parseFloat(extraprocssing);
                    totlPrice = parseFloat(totlPrice) + parseFloat(extraprackingprice);
                }
                
                var fbprice= 0;
                if (fbprices == 1) {
                    calctype= $('#frontback_calc_type').val();
                    amount= $('#frontback_price').val();
                    if (calctype == "percentage") {
                        fbprice= totlPrice * amount / 100;
                        totlPrice = parseFloat(totlPrice) + parseFloat(fbprice);
                    }
                    else {
                        fbprice= matheight * matwidth * amount;
                        //console.log(matheight+" "+matwidth+" "+amount+" "+noOfProducts+" "+fbprice);
                        totlPrice = parseFloat(totlPrice) + (parseFloat(fbprice)* noOfProducts);
                    }
                    //console.log(matheight+" "+matwidth+" "+amount+" "+noOfProducts+" "+fbprice);
                }
                
                
                //RICHIAMO LA FUNZIONE PER IMPOSTARE TUTTI I PREZZI
                
                 if ($('#lessthan').val() == 1)
                    totlPrice = totlPrice + (totlPrice * $("#incr_percentage").val() / 100);
                
                   
                material_price2= null;
                if (dimensionFinder.toLowerCase() == "custom") {
                    is_fixed_cost = false;
                    if (detCostCopy == 3) {
                        material_price2= costsplit;
                        material_price= prdPrice;
                    }
                }
                else {
                    if (detCostCopy == 3) {
                        material_price2= costsplit;
                        material_price= prdPrice;
                    }
                }
                
                setTablePrices(material_price, material_price2, productwidth, productheight, matdetails[1], is_fixed_cost, fixed_cost_price);
                setPrice(promotionalflag, promotype, promoval, promoprice, lantrmtype, lantrmamnt, fbprice, baseaccessories, extraaccessories, extraprocssing, matrlpricefrml, processingCost, productprice, singleproductPrice, operatorreview, totlPrice, noOfProducts);

            }
/////////////////////Multiplication Formula   Big Format    End


            /*******************base processing function ************/
            function baseprocessingPrice() {
                //debugger;
                var material_id_selected_name = $(".material_class:checked").val();
                var material_prices = "#mtid" + material_id_selected_name;
                var noOfProducts = $('#productQuanity').val();
                var materialdet = $(material_prices).val();
                var productheight = $('#productHeight').val();
                var productwidth = $('#productWidth').val();
                var extradef = $('#extradefValue').val();
                //var addtnlPricePerCopy = $('#additonal_price_copy').val();
                //var detCostCopy = $('#det_cost_copy').val();
                //var priceCalculationFunction = $('#price_calculation_function').val();
                //var basePrice = $('#base_price').val();
                var dimensionFinder = $('input[name=dimensions]:checked').val();
                //var shippingCost = $('input[name=htotalshippingprice]').val();
                // alert(shippingCost);
                //var baseaccessories = $('input[name=baseaccessories]:checked').val();
                //var extraaccessories = $('input[name=extraaccessories]:checked').val();
                //var promotionalflag = $('#promotionalFlag').val();
                //var promotype = $('#promotionalType').val();
                //var promoval = $('#promotionalValue').val();
                var bsprcsid = $('input[name*=baseprocessing]:checked').val();
                var bsrpcsgetId = "#bsprcs" + bsprcsid;
                var processingValues = $(bsrpcsgetId).val();
                var Pcost = 0;
                var dim_dets = "";
                var dimension_details = 0 + "*" + 0;
                var material_price = 0;
                //var matheight = 0;
                //var matwidth = 0;

                /*   var productheight = 0;   
                 var productwidth = 0;
                 var noOfProducts = 1;
                 var material_id_selected_name = $("#materialId option:selected").val();
                 var material_prices = "#mtid"+material_id_selected_name;
                 var materialdet = $(material_prices).val();
                 noOfProducts = $('#productQuanity').val();
                 var material_price = $(material_prices).val();
                 productheight = $('#productHeight').val();   
                 productwidth = $('#productWidth').val();
                 var extradef = $('#extradefValue').val();
                 var addtnlPricePerCopy = $('#additonal_price_copy').val();
                 var detCostCopy = $('#det_cost_copy').val();
                 var basePrice = $('#base_price').val();
                 var dimensionFinder = $('input[name=dimensions]:checked').val();
                 var bsprcsid = $('input[name=baseprocessing]:checked').val();
                 var bsrpcsgetId = "#bsprcs"+bsprcsid;
                 var processingValues = $(bsrpcsgetId).val();
                 var Pcost = 0;
                 var dim_dets = "";
                 var dimension_details = 0+"*"+0; */
                if (materialdet != null) {
                    var matdetails = materialdet.split("+");
                    material_price = matdetails[0];
                    material_price = parseFloat(material_price);
                }


                if (productheight == "") {
                    productheight = 1;
                }

                if (productwidth == "") {
                    productwidth = 1;
                }


                if (noOfProducts == "") {
                    noOfProducts = 1;
                }


                if ((dimensionFinder != null) && (dimensionFinder != "custom")) {
                    dim_dets = "#bd" + dimensionFinder;
                    dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    productwidth = res[0];
                    productheight = res[1];
                }



                if ((extradef == null) || (extradef == "")) {
                    extradef = 1;
                }
                
                var isextraright = $("#baseisExtraRight").val();
                var isextraleft = $("#baseisExtraLeft").val();
                var isextratop = $("#baseisExtraTop").val();
                var isextrabottom = $("#baseisExtraBottom").val();
                
                ////CONTROLLO SE SI TRATTA DI UNA LAVORAZIONE SPECIALE//////
                var is_special = false;
                var number_special = $("#base_number_special_"+bsprcsid).val();
                
                if(number_special > 0){
                    is_special = true;
                }

                if (processingValues != null) {
                    var processres = processingValues.split("+");
                    var processFunction = processres[1];
                    var processBaseprice = processres[0];
                    var minCopy = processres[2];
                    var minLength = processres[3];
                    var minHeight = processres[4];
                    var dimchk = processres[5];
                    var strtCost = processres[6];
                    if (parseFloat(dimchk) == 1) {
                        if (parseFloat(productheight) < parseFloat(minHeight)) {
                            productheight = minHeight;
                        }
                        if (parseFloat(productwidth) < parseFloat(minLength)) {
                            productwidth = minLength;
                        }
                    }

                }

                if (processingValues == null) {
                    processFunction = 7;
                    processBaseprice = 0;
                }
                if (minCopy == 1) {
                    if (noOfProducts == "") {
                        noOfProducts = 1;
                    }
                    if ((parseFloat(productheight * (productwidth * noOfProducts)) <= 10000)) {
                        productheight = 100;
                        productwidth = 100;
                    }
                }


                productheight = parseFloat(productheight / 100);
                productwidth = parseFloat(productwidth / 100);
                
                /// PREZZO LAVORAZIONE SPECIALE ////
                if(is_special === true){
                    var Pcost = parseFloat(processBaseprice * number_special);
                    return parseFloat( parseFloat(Pcost) + parseFloat(strtCost) );
                }
                /// FINE PREZZO LAVORAZIONE SPECIALE ////

                if (processFunction == 7)
                { /* Base Price*/
                    Pcost = processBaseprice;
                }

                if (processFunction == 10)
                { /* Height Formula*/
                    Pcost = parseFloat(processBaseprice * productheight);
                }
                if (processFunction == 9)
                { /* Width Formula*/
                    Pcost = parseFloat(processBaseprice * productwidth);
                }
                if (processFunction == 8)
                {  /* area formula */

                    Pcost = parseFloat(processBaseprice * productheight * productwidth);
                }
                if (processFunction == 12)
                { /*Twice Height Formula*/
                    Pcost = parseFloat(processBaseprice * productheight * 2);

                }
                if (processFunction == 11)
                { /*Twice Width Formula*/
                    Pcost = parseFloat(processBaseprice * productwidth * 2);

                }
                if (processFunction == 14)
                {  /* perimeter formula */
                    var lh = parseFloat(productheight + productwidth);
                    var perimeter = parseFloat(lh * 2);
                    Pcost = parseFloat(processBaseprice * perimeter);

                }

                if (processFunction == 13)
                {  /* semiperimeter formula */
                    var perimeter = parseFloat(productheight + productwidth);
                    Pcost = (processBaseprice * perimeter);

                }
                if (processFunction == 15)
                { /* etra definition field with base price*/
                    Pcost = parseFloat(processBaseprice) * parseFloat(extradef);
                }

                if (processFunction == 16)
                { /* Width divided by  etra definition field */
                    var matrlpricefrml = parseFloat(productwidth) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
                }

                if (processFunction == 17)
                { /* Height divided by extradefinition feild*/
                    var matrlpricefrml = parseFloat(productheight) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
                }

                if (processFunction == 18)
                {  /* semiperimeter divided by extradefinition feild*/
                    var perimeter = parseFloat(productheight) + parseFloat(productwidth);
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
                }

                if (processFunction == 19)
                {  /* perimeter divided by extradefinition feild*/
                    var lh = parseFloat(productheight) + parseFloat(productwidth);
                    var perimeter = lh * 2;
                    perimeter = perimeter / 2
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
                }

                if (processFunction == 20)
                {  /* Extra field definition as a percentage of cost of materials */
                    var matrlpricefrml = parseFloat(material_price) / parseFloat(100);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(extradef);
                }


                if (processFunction == 22)
                {  /* perimeter DINAMICO formula */

                    var perimeter = 0;

                    if (isextraright == 1) {
                        perimeter += productheight;
                    }
                    if (isextraleft == 1) {
                        perimeter += productheight;
                    }
                    if (isextratop == 1) {
                        perimeter += productwidth;
                    }
                    if (isextrabottom == 1) {
                        perimeter += productwidth;
                    }

                    //var lh = parseFloat(productheight + productwidth);
                    //var perimeter = parseFloat(lh * 2);
                    var Pcost = parseFloat(processBaseprice * perimeter);

                }

                if (processFunction == 23)
                {  /* perimeter DINAMICO divided by extradefinition feild*/
                    var perimeter = 0;

                    if (isextraright == 1) {
                        perimeter += productheight;
                    }
                    if (isextraleft == 1) {
                        perimeter += productheight;
                    }
                    if (isextratop == 1) {
                        perimeter += productwidth;
                    }
                    if (isextrabottom == 1) {
                        perimeter += productwidth;
                    }
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    var Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
                }


                //return Pcost;
                return parseFloat( parseFloat(Pcost) + parseFloat(strtCost) );
            }
            /*******************base processing function ************/

            /*******************extra processing function *************/
            function extraprocessingPrice(ext_id) {
                //debugger;
                /* orginial    var productheight = 0;   
                 var productwidth = 0;
                 var noOfProducts = 1;
                 var material_id_selected_name = $("#materialId option:selected").val();
                 var material_prices = "[id='"+material_id_selected_name+"']";
                 var noOfProducts = $('#productQuanity').val();
                 var material_price = $(material_prices).val();
                 var productheight = $('#productHeight').val();   
                 var productwidth = $('#productWidth').val();
                 var extradef = $('#extraDef').val();
                 var addtnlPricePerCopy = $('#additonal_price_copy').val();
                 var detCostCopy = $('#det_cost_copy').val();
                 var basePrice = $('#base_price').val();
                 var dimensionFinder = $('input[name=dimensions]:checked').val();
                 var processingValues = $('input[name=extraprocessing]:checked').val();
                 var materialdet = $(material_prices).val();
                 var Pcost = 0;
                 var dim_dets = "";
                 var dimension_details = 0+"*"+0; */

                var material_id_selected_name = $(".material_class:checked").val();
                var material_prices = "#mtid" + material_id_selected_name;
                var noOfProducts = $('#productQuanity').val();
                var materialdet = $(material_prices).val();
                var productheight = $('#productHeight').val();
                var productwidth = $('#productWidth').val();
                //var addtnlPricePerCopy = $('#additonal_price_copy').val();
                //var detCostCopy = $('#det_cost_copy').val();
                //var priceCalculationFunction = $('#price_calculation_function').val();
                //var basePrice = $('#base_price').val();
                var dimensionFinder = $('input[name=dimensions]:checked').val();
                //var shippingCost = $('input[name=shipping]:checked').val();

                //var baseaccessories = $('input[name=baseaccessories]:checked').val();
                //var extraaccessories = $('input[name=extraaccessories]:checked').val();
                //var promotionalflag = $('#promotionalFlag').val();
                //var promotype = $('#promotionalType').val();
                //var promoval = $('#promotionalValue').val();


                //var extprcsid = $('input[name*=extraprocessing]:checked').val();
                var extprcsid = ext_id;
                var extrpcsgetId = "#extprcs" + extprcsid;
                var processingValues = $(extrpcsgetId).val();
                var extrdefId = "#epextradefValue" + extprcsid;

                var isextrarightId = "#isextraright_" + extprcsid;
                var isextraleftId = "#isextraleft_" + extprcsid;
                var isextratopId = "#isextratop_" + extprcsid;
                var isextrabottomId = "#isextrabottom_" + extprcsid;

                var isextraright = $(isextrarightId+":checked").val();
                var isextraleft = $(isextraleftId+":checked").val();
                var isextratop = $(isextratopId+":checked").val();
                var isextrabottom = $(isextrabottomId+":checked").val();
               
               
                ////CONTROLLO SE SI TRATTA DI UNA LAVORAZIONE SPECIALE//////
                var is_special = false;
                var number_special = $("#number_special_"+extprcsid).val();
                
                if(number_special > 0){
                    is_special = true;
                }

                var extradef = $(extrdefId).val();
                var Pcost = 0;
                var dim_dets = "";
                var dimension_details = 0 + "*" + 0;
                var material_price = 0;
                var matheight = 0;
                var matwidth = 0;


                if (materialdet != null) {
                    var matdetails = materialdet.split("+");
                    material_price = matdetails[0];
                    matheight = matdetails[2];
                    matwidth = matdetails[3];
                    material_price = parseFloat(material_price);
                }

                if (productheight == "") {
                    productheight = 1;
                }

                if (productwidth == "") {
                    productwidth = 1;
                }


                if (noOfProducts == "") {
                    noOfProducts = 1;
                }


                if ((dimensionFinder != null) && (dimensionFinder != "custom")) {
                    dim_dets = "#bd" + dimensionFinder;
                    dimension_details = $(dim_dets).val();
                    var res = dimension_details.split("*");
                    var productwidth = res[0];
                    var productheight = res[1];
                }



                if ((extradef == null) || (extradef == "")) {
                    extradef = 1;
                }

                if (processingValues != null) {
                    var extprcssres = processingValues.split("+");
                    var processFunction = extprcssres[1];
                    var processBaseprice = extprcssres[0];
                    var minCopy = extprcssres[2];
                    var minLength = extprcssres[3];
                    var minHeight = extprcssres[4];
                    var dimchk = extprcssres[5];
                    var strtCost = extprcssres[6];
                    
                    if (parseFloat(dimchk) == 1) {
                        if (parseFloat(productheight) < parseFloat(minHeight)) {
                            productheight = minHeight;
                        }
                        if (parseFloat(productwidth) < parseFloat(minLength)) {
                            productwidth = minLength;
                        }
                    }
                }
                if (processingValues == null) {
                    processFunction = 7;
                    processBaseprice = 0;
                }
                if (minCopy == 1) {
                    if (noOfProducts == "") {
                        noOfProducts = 1;
                    }
                    if ((parseFloat(productheight * (productwidth * noOfProducts)) <= 10000)) {
                        productheight = 100;
                        productwidth = 100;
                    }
                }


                productheight = parseFloat(productheight / 100);
                productwidth = parseFloat(productwidth / 100);
                
                /// PREZZO LAVORAZIONE SPECIALE ////
                if(is_special === true){
                    var Pcost = parseFloat(processBaseprice * number_special);
                    /*console.log("SONO IN SPECIALE E QUESTO PREZZO BASE: "+processBaseprice);
                    console.log("SONO IN SPECIALE E QUESTO NUMERO ELEMENTI: "+number_special);
                    console.log("SONO IN SPECIALE E QUESTO PREZZO STRUTTURA: "+strtCost);
                    console.log("SONO IN SPECIALE E QUESTO PREZZO TOTALE: "+Pcost);*/
                    return parseFloat( (parseFloat(Pcost)*noOfProducts) + parseFloat(strtCost) );
                }
                /// FINE PREZZO LAVORAZIONE SPECIALE ////
                if (processFunction == 7)
                { /* Base Price*/
                    var matrlpricefrml = processBaseprice;
                    var Pcost = matrlpricefrml * (noOfProducts-1);

                }

                if (processFunction == 10)
                { /* Height Formula*/
                    var Pcost = parseFloat(processBaseprice * productheight) * (noOfProducts);
                }
                if (processFunction == 9)
                { /* Width Formula*/
                    var Pcost = parseFloat(processBaseprice * productwidth) * (noOfProducts);
                }
                if (processFunction == 8)
                {  /* area formula */

                    var Pcost = parseFloat(processBaseprice * productheight * productwidth) * (noOfProducts);
                }
                if (processFunction == 12)
                { /*Twice Height Formula*/
                    var Pcost = parseFloat(processBaseprice * productheight * 2) * (noOfProducts);

                }
                if (processFunction == 11)
                { /*Twice Width Formula*/
                    var Pcost = parseFloat(processBaseprice * productwidth * 2) * (noOfProducts);

                }
                if (processFunction == 14)
                {  /* perimeter formula */
                    var lh = parseFloat(productheight + productwidth);
                    var perimeter = parseFloat(lh * 2);
                    var Pcost = parseFloat(processBaseprice * perimeter) * (noOfProducts);

                }

                if (processFunction == 13)
                {  /* semiperimeter formula */
                    var perimeter = parseFloat(productheight + productwidth);
                    var Pcost = (processBaseprice * perimeter) * (noOfProducts);

                }
                if (processFunction == 15)
                { /* etra definition field with base price*/
                    Pcost = parseFloat(processBaseprice) * parseFloat(extradef)* (noOfProducts);
                }

                if (processFunction == 16)
                { /* Width divided by  etra definition field */
                    var matrlpricefrml = parseFloat(productwidth) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice)* (noOfProducts);
                }

                if (processFunction == 17)
                { /* Height divided by extradefinition feild*/
                    var matrlpricefrml = parseFloat(productheight) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice)* (noOfProducts);
                }

                if (processFunction == 18)
                {  /* semiperimeter divided by extradefinition feild*/
                    var perimeter = parseFloat(productheight) + parseFloat(productwidth);
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice)* (noOfProducts);
                }

                if (processFunction == 19)
                {  /* perimeter divided by extradefinition feild*/
                    var lh = parseFloat(productheight) + parseFloat(productwidth);
                    var perimeter = lh * 2;
                    perimeter = perimeter / 2
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice) * (noOfProducts);
                }

                if (processFunction == 20)
                {  /* Extra field definition as a percentage of cost of materials */
                    var matrlpricefrml = parseFloat(material_price) / parseFloat(100);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(extradef) * (noOfProducts);
                }

                if (processFunction == 22)
                {  /* perimeter DINAMICO formula */

                    var perimeter = 0;

                    if (isextraright == 1) {
                        perimeter += productheight;
                    }
                    if (isextraleft == 1) {
                        perimeter += productheight;
                    }
                    if (isextratop == 1) {
                        perimeter += productwidth;
                    }
                    if (isextrabottom == 1) {
                        perimeter += productwidth;
                    }

                    //var lh = parseFloat(productheight + productwidth);
                    //var perimeter = parseFloat(lh * 2);
                    var Pcost = parseFloat(processBaseprice * perimeter) * (noOfProducts);

                }

                if (processFunction == 23)
                {  /* perimeter DINAMICO divided by extradefinition feild*/
                    var perimeter = 0;

                    if (isextraright == 1) {
                        perimeter += productheight;
                    }
                    if (isextraleft == 1) {
                        perimeter += productheight;
                    }
                    if (isextratop == 1) {
                        perimeter += productwidth;
                    }
                    if (isextrabottom == 1) {
                        perimeter += productwidth;
                    }
                    var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef);
                    Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice) * (noOfProducts);
                }

                return parseFloat( (parseFloat(Pcost) ) + parseFloat(strtCost) );
            }

            /****************** extra processing end *******************/


            ////////////////////////////////////////////////
            //               Small Format        
            ///////////////////////////////////////////////

            /************* On Load **********************/
            updateValuesSmall();
            /**************** On Load ********************/


            $("#sproductQuanity").blur(function () {
                updateValuesSmall();
            });

            /*************** On Change OF MATERIAL START ****************/
            $("#smaterialId").change(function () {
                updateValuesSmall();
            });
            /*************** On Change MATERIAL END *********************/

            /************* On change Processing START *************/
            $("input[name=processing]:radio").change(function () {
                updateValuesSmall();
            });
            /*********** On change Processing END ********************/

            /************* On change Shipping START *************/
            $("input[name=shipping]:radio").change(function () {
                updateValuesSmall();
            });
            /*********** On change Shipping END ********************/

            /************* On change Dimesions START *************/
            $("input[name=sdimensions]:radio").change(function () {
                updateValuesSmall();
            });
            /*********** On change Dimensions END ********************/


            /**********Multiplication Formula for Big Format Start *************/

            function updateValuesSmall() {

                var bsprcj = $("#basePricesmall").val();
                var frmidj = $("#formid").val();
                var mtrlidj = $("#smaterialId option:selected").val();
                var dmsnidj = $('input[name=sdimensions]:checked').val();
                var prcsngidj = $('input[name=processing]:checked').val();
                var noofproductsj = $("#sproductQuanity").val();
                var spromotypej = $("#spromotype").val();
                var spromopricej = $("#spromoprice").val();
                var shippingCostj = $('#shippingprice').val();
                if ((spromotypej == null) || (spromotypej == "")) {
                    spromotypej = "value";
                    spromopricej = 0;
                }

                if ((spromopricej == null) || (spromopricej == "")) {
                    spromopricej = 0;
                }

                if ((bsprcj == null) || (bsprcj == "")) {
                    bsprcj = 0;
                }
                var price_path = $('input[name=price_path]').val();
                $.ajax({
                    url: price_path,
                    data: {
                        bsprc: bsprcj,
                        frmid: frmidj,
                        mtrlid: mtrlidj,
                        prcsngid: prcsngidj,
                        dmsnid: dmsnidj,
                        noofproducts: noofproductsj,
                        spromotype: spromotypej,
                        spromoprice: spromopricej,
                        shippingCost: shippingCostj
                    },
                    dataType: "html",
                    type: "POST",
                    success: function (data) {
                        var res = data.split("__");

                        if (res[4] != res[5]) {
                            $('#spromotd').show();
                            $('#spromodiv').html(res[4]);
                        } else {
                            $('#spromotd').hide();
                        }

                        $('#sprintigmaterialdiv').html(res[1]);
                        $('#sadtnlcostdiv').html(res[2]);
                        $('#ssingleproductcostdiv').html(res[3]);
                        $('#stotalproductcostdiv').html(res[5]);
                        $('.stotalproductcostdivqnty').html(res[0]);
                        $('#smalltotlprice').val(res[5]);
                    }
                });

            }
        
        
        
        
        
            function capitalizeFirstLetter(string) {
              return string.charAt(0).toUpperCase() + string.slice(1);
            }

            function showModal(id,title,content){
              $('#'+id+' .modal-title').html(title);
              $('#'+id+' .modal-body').html( capitalizeFirstLetter(content) );
              $('#'+id).modal('show');
            }
    
  
        }
    };
})(jQuery);


