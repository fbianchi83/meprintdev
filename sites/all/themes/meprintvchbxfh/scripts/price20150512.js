(function($) {
Drupal.behaviors.meprint = {
   
  attach: function (context, settings) {
    
    //code starts
  
/************* On Load **********************/
     // var shippingidj =  $('input[name=shipping]:checked').val();
    //  updateShipping(shippingidj);   
     // alert(shippingidj);
      updateDates();
      updateValues();
       
/**************** On Load ********************/
 
      
/*************** On Change OF MATERIAL START ****************/      
$("#materialId").change(function() { 
   updateDates();
   updateValues();
    });
/*************** On Change MATERIAL END *********************/         
 
 
/*************** On Blur Quantity START *********************/            
   $("#productQuanity").blur(function(){
       
      updateValues();
     });    
/*************** On Blur Quantity END ************************/               
  
  
  
/*************** On Blur Height START ************************/            
   $("#productHeight").blur(function(){ 
       updateValues();      
   });
/*************** On Blur Height END **************************/    


/************* On Blur Width START *************/            
   $("#productWidth").blur(function(){ 
       updateValues();      
   });   
/*********** On Blur Width END ********************/

/************* On change Base Acessory Start*************/            
   $("input[name=baseaccessories]").change(function () {
     $('input:checkbox[name=extraaccessories]').attr('checked',false);  
       updateValues();      
   });   
/*********** On change Base Acessory END ********************/

/************* On change Extra Acessory Start*************/            
   $("input[name=extraaccessories]:radio").change(function () { 
          
   });
   var chkd = "";
   $(".extraaccessories_class").click(function() {
       $('input:checkbox[name=baseaccessories]').attr('checked',false);  
      var totalCheckboxes = $('input[name=extraaccessories]:checkbox').length;
       if(totalCheckboxes > 1){
          $(".extraaccessories_class").attr("checked", false); //uncheck all checkboxes
              if($(this).attr("value") == chkd ){
                  $(this).attr("checked", false);  //check the clicked one
                  chkd = "";
              }else{
                  $(this).attr("checked", true);  //check the clicked one 
                  chkd = $(this).attr("value");  
            }
           }
    updateValues(); 
});  
/*********** On change Extra Acessory END ********************/

/************* On change Processing START *************/            
   $("input[name=baseprocessing]:radio").change(function () {
    updateValues();      
   });   
/*********** On change Processing END ********************/

/************* On change Processing START *************/    
var expchkd = "";
var expchkdcnt = 0;

   $(".extraprocessing_class").click(function() { //debugger;
      var totalCheckboxes = $('input[name=extraprocessing]:checkbox').length;
       if(totalCheckboxes > 1){
          $(".extraprocessing_class").attr("checked", false); //uncheck all checkboxes
           var xx =   $(this).attr("value");
           if($(this).attr("value") == expchkd ){
                  $(this).attr("checked", false); 
                  expchkd = "";
              }else{
                  $(this).attr("checked", true);
                  expchkd = $(this).attr("value");
              }
          }
            if ($('input[name="extraprocessing"]').is(':checked')) {
               $(".extinfodiv").show();
           }
           else {
               $(".extinfodiv").hide();
           }
          
    updateValues(); 
});  


/*********** On change Processing END ********************/

/************* On change Shipping START *************/            
   $("input[name=shipping]:radio").change(function () {  
        updateValues();      
   });   
/*********** On change Shipping END ********************/

/************* On change Dimesions START *************/            
   $("input[name=dimensions]:radio").change(function () {
      updateValues(); 
      
   });   
/*********** On change Dimensions END ********************/

/************* On change Operator Review Start*************/            
   $("input[name=operatorreview]").change(function () {
    updateValues();      
   });   
/*********** On change Operator Review END ********************/

/************* On change long term prouction start ******************/
 $(".lngtmprdctnclass").click(function() { //debugger;
   /*   var totalCheckboxes = $('input[name=lngtmprdctn]:checkbox').length;
       if(totalCheckboxes > 1){
          $(".lngtmprdctnclass").attr("checked", false); //uncheck all checkboxes
           if($(this).attr("value") == expchkd ){
                  $(this).attr("checked", false); 
                  expchkd = "";
              }else{
                  $(this).attr("checked", true);
                  expchkd = $(this).attr("value");
              }
          }*/
     
    updateDates();  
    updateValues();
   
});  
/************* On change long term prouction start ******************/

function updateDates(){ 
  
var matdaysj = 0;
var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();
var productgroupidj = $('#productgroupid').val();

var formidj = $('#formid').val();
var languageidj = $('#languageid').val();
//debugger;

    var delytimj = 0;
    var lngtmprdctnvals = 0;
    var lngrmprdtnid = "#lngtm"+lngtmprdctn;
    var lngtmprdctnvals = $(lngrmprdtnid).val();
    if((lngtmprdctn != "")&&(lngtmprdctn != null)){
        var lngtrmprdctnvl = lngtmprdctnvals.split("+");
        delytimj = lngtrmprdctnvl[0];
    }
   
    var workloadtypej  = $("#workloadtype").val();
    var workloadlimitj = $("#workloadlimit").val();
    var formtype= $("#formtype").val();
    if((workloadlimitj == null) || (workloadlimitj == "")){
        workloadlimitj = 0
        workloadtypej = "Products";
    }
    
    if( formtype == 'small'){
        
    var material_id_selected_name = $("#smaterialId option:selected").val();
    var material_prices = "#smtid"+material_id_selected_name;
    }else {
    var material_id_selected_name = $("#materialId option:selected").val(); 
    var material_prices = "#mtid"+material_id_selected_name;
    }
    
//var material_prices = "#mtid"+material_id_selected_name;
var materialdet = $(material_prices).val();

if(materialdet != null){
        var matdetails = materialdet.split("+");
         matdaysj = matdetails[4];
      }else if(materialdet == null){
          matdaysj = 0;
      }
//   debugger;

var dates_path = $('input[name=dates_path]').val();
     
	$.ajax({
		//url: "http://localhost:81/Meprintv3/sites/all/themes/meprint/get-dates.php",
                url:dates_path,
		data: { 
                        mtrldys : matdaysj, 
                        delytim : delytimj,
                        workloadlimit : workloadlimitj,
                        workloadtype : workloadtypej,
                        productgroupid : productgroupidj,
                        formid : formidj,
                        languageid : languageidj
                    },
		dataType: "html",
		type: "POST",
		success: function(data){			
			//console.log(data);
                        var dates = data.split("**")
                        var datelength = dates.length;
                        datelength = parseFloat(datelength) - 1;
                        for (var i=0;i<datelength;i++){
                            var shipingdivid = "#shippingdateprintdiv"+i;
                          $(shipingdivid).html(dates[i]);  
                        }
                        
                      
		}
                }); 
}

/************* On change long term prouction End ******************/


/************* Shippping price Calculation start ******************/

function updateShipping(){
  // debugger;
 //alert("test");
var dimwgtj = 0;
var matweightj = 1;
var matthicknessj = 1;
var productwidthj = 1;
 var productheightj = 1;
var productgroupidj = $('#productgroupid').val();
var formidj = $('#formid').val();
var shippingidj =  $('input[name=shipping]:checked').val();
/*$('input[name="shipping"]').click(function() { 
    if($('input[name="shipping"]').is(':checked')){ 
           shippingidj = $(this).val();
    }
    else {
           shippingidj = $(this).val();
    }
});*/
//alert(shippingidj);
var languageidj = $('#languageid').val();
var noOfProductsj = $('#productQuanity').val();
 productheightj = $('#productHeight').val();   
 productwidthj = $('#productWidth').val();
 dimwgtj = $('#dimwgt').val();

 languageidj = "it";
if((dimwgtj == "") || (dimwgtj == null)){
        dimwgtj = 0;
    }
var dimensionFinder = $('input[name=dimensions]:checked').val();
if((dimensionFinder != null) && (dimensionFinder != "custom")){
        var dim_dets = "#bd"+dimensionFinder;
        var dimension_details = $(dim_dets).val(); 
        var res = dimension_details.split("*");
        productwidthj = res[0];
        productheightj = res[1];
     }else if((productheightj =="")&&(productwidthj =="")){
         var dmsnidj = $('input[name=sdimensions]:checked').val();      
        var sres = dmsnidj.split("*");
        productwidthj = sres[0];
        productheightj = sres[1];
     }else{
         productwidthj = 1;
         productheightj = 1;
     }

 if((noOfProductsj == "") || (noOfProductsj == null)){    
    noOfProductsj = $("#sproductQuanity").val();
    if((noOfProductsj == "") || (noOfProductsj == null)){
        noOfProductsj = 1;
    }    
}
 //  debugger; 
var material_id_selected_name = $("#materialId option:selected").val();
var material_prices = "#mtid"+material_id_selected_name;
var materialdet = $(material_prices).val();
if(materialdet != null){
        var matdetails = materialdet.split("+");
         matweightj = matdetails[5];
         matthicknessj = matdetails[6];
      }else if(material_id_selected_name == null){
         material_id_selected_name = $("#smaterialId option:selected").val();
        material_prices = "#smtid"+material_id_selected_name;
         materialdet = $(material_prices).val();
         var matdetails = materialdet.split("+");
         matweightj = matdetails[5];
         matthicknessj = matdetails[6]; 
      }else{
           matweightj = 1;
         matthicknessj = 1;
      }
      

//alert(shippingidj);
var shipping_path = $('input[name=shipping_path]').val();
   //   alert(shipping_path);
     //  event.preventDefault();
        $.ajax({
		//url: "http://localhost:81/Meprintv3/sites/all/themes/meprint/set-shipping.php",
                url:shipping_path,
		data: { 
                        matweight : matweightj,
                        matthickness : matthicknessj,
                        productgroupid : productgroupidj,
                        formid : formidj,
                        languageid : languageidj,
                        noOfProducts : noOfProductsj,
                        productwidth : productwidthj,
                        productheight : productheightj,
                        dimwgt : dimwgtj,
                        shippingid : shippingidj,
                        myshipid:shippingidj,
                    },
                async: false,
		dataType: "html",
		type: "POST",
		success: function(data){
                   // alert('fff');
                      $('.htotalshippingprice').val(data);
                       $('#shippingprice').val(data);                        
                       $('.totalshippingprice').html(data);
                     
                       
		},
                error: function () {   },
                complete: function () {   }
                });
         
}
/************* Shippping price Calculation End ******************/

/**********Multiplication Formula for Big Format Start *************/

function updateValues(){   
    //debugger;
    updateShipping();    
  //  alert("updtae values");
  //  alert($('input[name=htotalshippingprice]').val());
    var material_id_selected_name = $("#materialId option:selected").val();
    var material_prices = "#mtid"+material_id_selected_name;
    var noOfProducts = $('#productQuanity').val();
    var materialdet = $(material_prices).val();
    var productheight = $('#productHeight').val();   
    var productwidth = $('#productWidth').val();
    var extradef = $('#extraDef').val();
    var addtnlPricePerCopy = $('#additonal_price_copy').val();
    var detCostCopy = $('#det_cost_copy').val();
    var priceCalculationFunction = $('#price_calculation_function').val();
    var basePrice = $('#base_price').val();
    var dimensionFinder = $('input[name=dimensions]:checked').val();
    var shippingCost =  $('input[name=htotalshippingprice]').val();
    //alert("ttt"+shippingCost);
    //alert("visal");
    var getbaseaccId =  $('input[name=baseaccessories]:checked').val();
    var baseaccId =  "#baseacc"+getbaseaccId;
    var baseaccessories =  $(baseaccId).val();
    var getexaccId =  $('input[name=extraaccessories]:checked').val();
    var exaccId =  "#extacc"+getexaccId;
    var extraaccessories =  $(exaccId).val();
    var promotionalflag = $('#promotionalFlag').val();
    var promotype = $('#promotionalType').val();
    var promoval = $('#promotionalValue').val();
    var dim_dets = "";
    var dimension_details = 0+"*"+0; 
    var costcopies = $('#costCopies').val();
    var extprcsid = $('input[name=extraprocessing]:checked').val();
    var extdefdivlist = $('#extdefdividlst').val();
    var promoprice = 0;
    var operatorreview = $('input[name=operatorreview]:checked').val(); 
    var material_price = 0;
    var matheight = 0;
    var matwidth = 0;
    var lngtmprdctn = $('input[name=lngtmprdctn]:checked').val();
    var lantrmdays = 0;
    var lantrmamnt = 0;
    var lantrmtype = "value";
    var extrapackingcheck =  $('#packingpricecheck').val();
    var extrapackingdetails =  $('#packingpricevalues').val();
    var extraprackingprice = 0;
    
    
   // debugger;
    var lngrmprdtnid = "#lngtm"+lngtmprdctn;
    var lngtmprdctnvals = $(lngrmprdtnid).val();
    if((lngtmprdctn != "")&&(lngtmprdctn != null)){
        var lngtrmprdctnvl = lngtmprdctnvals.split("+");
        lantrmdays = lngtrmprdctnvl[0];
        lantrmamnt = lngtrmprdctnvl[1];
        lantrmtype = lngtrmprdctnvl[2];
        if(lantrmtype == ""){
            lantrmtype = "Value";
        }
        if(lantrmamnt == ""){
            lantrmamnt = 0;
        }
        if(lantrmdays == ""){
            lantrmdays = 0;
        }
    }
   if(extprcsid != null){
    var extdefdivcount = extdefdivlist.split("+");
    var extdefdivlength = extdefdivcount.length;
     for (var i=0; i<extdefdivlength; i++){
         var epextradefdivcondt = "#epextradefdiv"+extdefdivcount[i]; 
        var epextraprcsdivcont = "#epextraprcsdiv"+extdefdivcount[i];       
         if(extdefdivcount[i] == extprcsid){
              $(epextradefdivcondt).show();
               $(epextraprcsdivcont).show();
         }else{
             $(epextradefdivcondt).hide();
             $(epextraprcsdivcont).hide();
         }
     }
    }
    
    if(dimensionFinder == "custom"){
        $('#cstdisp').show();
    }else{
        $('#cstdisp').hide();
    }
    
    if((operatorreview == "")||(operatorreview == null)){
    operatorreview = 0;
    }else{
        operatorreview = 5;
    }    
    
/*    if(dimensionFinder == "custom"){
        $('#cstdisp').show();
    }else{
        $('#cstdisp').hide();
    } */
    
    if(noOfProducts == ""){
         noOfProducts = 1;
     }
   
     if(addtnlPricePerCopy == ""){
         addtnlPricePerCopy = 0;
     }
     
     if(baseaccessories == null){
         baseaccessories = 0;
     }
     
     if(extraaccessories == null){
         extraaccessories = 0;
     }
     
     if((dimensionFinder != null) && (dimensionFinder != "custom")){
        dim_dets = "#bd"+dimensionFinder;
        dimension_details = $(dim_dets).val(); 
        var res = dimension_details.split("*");
        var productwidth = res[0];
        var productheight = res[1];
     }
     
     if(productheight == ""){
         productheight = 100;
     }
     
     if(productwidth == ""){
         productwidth = 100;
     }
    
    //debugger;
    var totalsqarea = parseFloat(productwidth)*parseFloat(productwidth)*parseFloat(noOfProducts);
    totalsqarea = parseFloat(totalsqarea)/10000;
    if(extrapackingcheck == 1){
        var ecxpcklvlone = extrapackingdetails.split("__");
       var ecxpcklvlonelngth =  ecxpcklvlone.length;
       ecxpcklvlonelngth = ecxpcklvlonelngth-1;
       for(var i=0;parseFloat(i)<parseFloat(ecxpcklvlonelngth);i++){
          var ecxpcklvltwo = ecxpcklvlone[i].split("+");
          if((parseFloat(ecxpcklvltwo[1]) >= parseFloat(totalsqarea)) && (parseFloat(ecxpcklvltwo[0]) <= parseFloat(totalsqarea))){
              extraprackingprice = ecxpcklvltwo[2];
          } 
       }
    }
    
    
     if(materialdet != null){
        var matdetails = materialdet.split("+");
        var material_price = matdetails[0];
        matheight = matdetails[2];
        matwidth = matdetails[3];
        material_price = parseFloat(material_price);
        if((matheight == null)||(matheight == "")){
           matheight = 0; 
        }
        if((matwidth == null)||(matwidth == "")){
            matwidth = 0;
        }
        matheight = parseFloat(matheight);
        matwidth = parseFloat(matwidth);
        if( parseFloat(productheight) > parseFloat(matheight)){
                 matheight = productheight;
             }
             if( parseFloat(productwidth) > parseFloat(matwidth)){
                 matheight = productwidth;
             }
        if(matdetails[1] == 1){
            if( parseFloat(noOfProducts * productheight * productwidth) < 10000){
                 matheight = 100;
                 matwidth = 100;
            }else{
                 matheight = productheight;
                 matwidth = productwidth;
            }
            
        }
        
     }
     
     if(materialdet == null){
        var matheight = 0;
        var matwidth = 0;
        var material_price = 0;
     }
     
     
     if(extradef == ""){
         extradef = 1;
     }
     

     matheight = parseFloat(matheight/100);
      matwidth  = parseFloat(matwidth/100);  
     
      productheight = parseFloat(productheight/100);
      productwidth  = parseFloat(productwidth/100);  
      var processingCost = baseprocessingPrice();
      if((processingCost == "") ||(processingCost == null)){
                    processingCost = 0;
      }

      if((shippingCost == "") ||(shippingCost == null)){
                    shippingCost = 0;
      } 
      
    if(detCostCopy == 1){  
      var matrlpricefrml = parseFloat(material_price * matheight * matwidth); 
      var productprice = parseFloat(matrlpricefrml) + parseFloat(basePrice);
      
      var singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
      var extraprocssing = extraprocessingPrice();
      var sproductprice =  singleproductPrice+ parseFloat(addtnlPricePerCopy);
      
      var totlPrice = parseFloat(noOfProducts) * parseFloat(sproductprice);
      totlPrice = parseFloat(totlPrice) - parseFloat(addtnlPricePerCopy);
      totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories)+ parseFloat(extraprocssing);
      totlPrice =  parseFloat(totlPrice) + parseFloat(extraprackingprice); 
        if(promotionalflag == "Promo"){
         if(promotype == "value"){
            promoval = parseFloat(noOfProducts) * parseFloat(promoval);
            promoprice = totlPrice; 
            totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
          if(promotype == "percentage"){
             promoval = ((parseFloat(totlPrice)/ 100)* parseFloat(promoval));
             promoprice = totlPrice;
             totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
    }
    
     
         if(lantrmtype == "Value"){
            totlPrice = parseFloat(totlPrice) - parseFloat(lantrmamnt);
         }
          if(lantrmtype == "Percentage"){
             var lngtrmdsc = ((parseFloat(totlPrice)/ 100)* parseFloat(lantrmamnt));
             totlPrice = parseFloat(totlPrice) - parseFloat(lngtrmdsc);
         }
    
    
    
    
     if(baseaccessories != 0){
        $('#baseaccessoriestd').show();
        baseaccessories = parseFloat(baseaccessories).toFixed(2);
        $('#baseacccstdiv').html(baseaccessories);
    }else{
        $('#baseaccessoriestd').hide();
    }
    
    if(promoprice != 0){
         $('#promopricetd').show();
        promoprice = parseFloat(promoprice).toFixed(2);
        $('#promopricediv').html(promoprice);
    }else{
        $('#promopricetd').hide();
    }
    
    if(extraprocssing != 0){
         $('#extraprocessingtd').show();
        extraprocssing = parseFloat(extraprocssing).toFixed(2);
        $('#extraprocessingdiv').html(extraprocssing);
    }else{
        $('#extraprocessingtd').hide();
    }
    
    matrlpricefrml = parseFloat(matrlpricefrml).toFixed(2);
    processingCost = parseFloat(processingCost).toFixed(2);
    productprice = parseFloat(productprice).toFixed(2);
    singleproductPrice = parseFloat(singleproductPrice).toFixed(2);
    totlPrice = parseFloat(totlPrice) + parseFloat(operatorreview);
    totlPrice = parseFloat(totlPrice).toFixed(2);
   
     $('#printigmaterialdiv').html(productprice);
     $('#adtnlcostdiv').html(processingCost);
     $('#singleproductcostdiv').html(singleproductPrice);
     $('#totalproductcostdiv').html(totlPrice);
     $('.totalnoofproductbigfrmtdiv').html(noOfProducts);
     $('#bigtotlprice').val(totlPrice);
    }
    
    /***************** varying cost on no of products *********/
    if(detCostCopy == 2){
        
       var totlPrice =0;
        if(costcopies != null){
        var costsplit = costcopies.split("+");
         var cstarrlength = costsplit.length
         cstarrlength = parseFloat(cstarrlength - 1);
         var pcount = noOfProducts;
         var _j = 1;
         var _tprice = 0;
         var _remProcnt = 0;
          var matrlpricefrml = 0;
          var productprice = 0;
          var processingCost = 0;
          var singleproductPrice = 0
           for(var i=0; i < cstarrlength; i++){
               var costbreak = costsplit[i].split(" ");
               
               if (parseFloat(pcount) <= parseFloat(costbreak[1]) || !parseFloat(costbreak[1])){
                    var xyz = pcount - _remProcnt;
                   
                    matrlpricefrml = parseFloat(material_price) * parseFloat(matheight) * parseFloat(matwidth); 
                    productprice = parseFloat(matrlpricefrml) + parseFloat(costbreak[2]);
                    processingCost = baseprocessingPrice();
                    singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                    totlPrice = parseFloat(totlPrice) + parseFloat(xyz * singleproductPrice);
                    break;
               }else{
//                   if (i == 0){
                        matrlpricefrml = parseFloat(material_price) * parseFloat(matheight) * parseFloat(matwidth); 
                        productprice = parseFloat(matrlpricefrml) + parseFloat(costbreak[2]);
                        processingCost = baseprocessingPrice();
                        singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                        if (i==0){
                            _tprice = parseFloat(_tprice) + parseFloat(costbreak[1] * singleproductPrice);
                        }else{
                            _tprice = parseFloat(_tprice) + parseFloat((costbreak[1]-_remProcnt) * singleproductPrice);
                        }
                       totlPrice = parseFloat(_tprice); 
                       _remProcnt = costbreak[1];
               }
//               
//                
//              if(parseFloat(pcount) <= parseFloat(costbreak[1])){
//                  if(i=0){
//                  totlPrice = parseFloat(pcount * singleproductPrice); 
//                  }else{
//                      var onevar = parseFloat(costbreak[1]) - parseFloat(pcount);
//                     totlPrice = parseFloat(totlPrice) + parseFloat(onevar * singleproductPrice); 
//                    }
//                  var nocount = noOfProducts - costbreak[1];
//                  if(nocount <= 0){
//                      break;
//                  }
//              }
//              if(parseFloat(pcount) > parseFloat(costbreak[1])){
//                  var nocount = noOfProducts - costbreak[1];
//                  if(nocount>0){
//                  totlPrice = parseFloat(totlPrice) + parseFloat(costbreak[1] * singleproductPrice);
//                  }if(nocount<0){
//                   nocount = costbreak[1] - noOfProducts; 
//                  totlPrice = parseFloat(totlPrice) + parseFloat(nocount * singleproductPrice);                  
//                   }
//              }
             }
        }
      var extraprocssing = extraprocessingPrice();
      totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories)+ parseFloat(extraprocssing);
     totlPrice =  parseFloat(totlPrice) + parseFloat(extraprackingprice); 
        if(promotionalflag == "Promo"){
         if(promotype == "value"){
            promoval = parseFloat(noOfProducts) * parseFloat(promoval);
            promoprice = totlPrice;
            totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
          if(promotype == "percentage"){
             promoval = ((parseFloat(totlPrice)/ 100)* parseFloat(promoval));
             promoprice = totlPrice;
             totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
    }
    
     if(lantrmtype == "Value"){
          totlPrice = parseFloat(totlPrice) - parseFloat(lantrmamnt);
       }
     if(lantrmtype == "Percentage"){
          var lngtrmdsc = ((parseFloat(totlPrice)/ 100)* parseFloat(lantrmamnt));
          totlPrice = parseFloat(totlPrice) - parseFloat(lngtrmdsc);
     }
   
     if(baseaccessories != 0){
        $('#baseaccessoriestd').show();
        baseaccessories = parseFloat(baseaccessories).toFixed(2);
        $('#baseacccstdiv').html(baseaccessories);
    }else{
        $('#baseaccessoriestd').hide();
    }
    
    if(extraprocssing != 0){
         $('#extraprocessingtd').show();
        extraprocssing = parseFloat(extraprocssing).toFixed(2);
        $('#extraprocessingdiv').html(extraprocssing);
    }else{
        $('#extraprocessingtd').hide();
    }
    
    
    if(promoprice != 0){
         $('#promopricetd').show();
        promoprice = parseFloat(promoprice).toFixed(2);
        $('#promopricediv').html(promoprice);
    }else{
        $('#promopricetd').hide();
    }
    
     matrlpricefrml = parseFloat(matrlpricefrml).toFixed(2);
     processingCost = parseFloat(processingCost).toFixed(2);
     productprice = parseFloat(productprice).toFixed(2);
     singleproductPrice = parseFloat(singleproductPrice).toFixed(2);
     totlPrice = parseFloat(totlPrice) + parseFloat(operatorreview);
     totlPrice = parseFloat(totlPrice).toFixed(2);
   
    
   
     $('#printigmaterialdiv').html(productprice);
     $('#adtnlcostdiv').html(processingCost);
     $('#singleproductcostdiv').html(singleproductPrice);
     $('#totalproductcostdiv').html(totlPrice);
     $('.totalnoofproductbigfrmtdiv').html(noOfProducts);
     $('#bigtotlprice').val(totlPrice);
    }
    
    /***************** varying cost on no of square area *********/    
    if(detCostCopy == 3){
      
        
         if(costcopies != null){
        var costsplit = costcopies.split("+");
         var cstarrlength = costsplit.length
         cstarrlength = parseFloat(cstarrlength - 1);
           for(var i=0; i < cstarrlength; i++){
               var costbreak = costsplit[i].split(" ");
               var prdctarea = parseFloat(productheight) * parseFloat(productwidth);
               if ((prdctarea >= costbreak[0]) && (prdctarea <= costbreak[1])){  
                  var matrlpricefrml = parseFloat(material_price) * parseFloat(matheight) * parseFloat(matwidth); 
                  var productprice = parseFloat(matrlpricefrml) + parseFloat(costbreak[2]);
                  var processingCost = baseprocessingPrice();
                  var singleproductPrice = parseFloat(productprice) + parseFloat(processingCost) + parseFloat(baseaccessories);
                  prdctarea = parseFloat(prdctarea - costbreak[1]);
              }
             }
       }
     
      var extraprocssing = extraprocessingPrice();
      var totlPrice = parseFloat(noOfProducts * singleproductPrice);
      totlPrice = parseFloat(totlPrice) + parseFloat(shippingCost) + parseFloat(extraaccessories)+ parseFloat(extraprocssing);
     totlPrice =  parseFloat(totlPrice) + parseFloat(extraprackingprice); 
        if(totlPrice != 0){
        if(promotionalflag == "Promo"){
         if(promotype == "value"){
            promoval = parseFloat(noOfProducts) * parseFloat(promoval);
            promoprice = totlPrice;
            totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
          if(promotype == "percentage"){
             promoval = ((parseFloat(totlPrice)/ 100)* parseFloat(promoval));
             promoprice = totlPrice;
             totlPrice = parseFloat(totlPrice) - parseFloat(promoval);
         }
    }
     }else{
         totlPrice = 0;
     }
 
   
     if(baseaccessories != 0){
        $('#baseaccessoriestd').show();
        baseaccessories = parseFloat(baseaccessories).toFixed(2);
        $('#baseacccstdiv').html(baseaccessories);
    }else{
        $('#baseaccessoriestd').hide();
    }
   
   if(extraprocssing != 0){
         $('#extraprocessingtd').show();
        extraprocssing = parseFloat(extraprocssing).toFixed(2);
        $('#extraprocessingdiv').html(extraprocssing);
    }else{
        $('#extraprocessingtd').hide();
    }
   
    if(promoprice != 0){
         $('#promopricetd').show();
        promoprice = parseFloat(promoprice).toFixed(2);
        $('#promopricediv').html(promoprice);
    }else{
        $('#promopricetd').hide();
    }
   
    if(lantrmtype == "Value"){
          totlPrice = parseFloat(totlPrice) - parseFloat(lantrmamnt);
       }
     if(lantrmtype == "Percentage"){
          var lngtrmdsc = ((parseFloat(totlPrice)/ 100)* parseFloat(lantrmamnt));
          totlPrice = parseFloat(totlPrice) - parseFloat(lngtrmdsc);
     }
   
    matrlpricefrml = parseFloat(matrlpricefrml).toFixed(2);
    processingCost = parseFloat(processingCost).toFixed(2);
    productprice = parseFloat(productprice).toFixed(2);
    singleproductPrice = parseFloat(singleproductPrice).toFixed(2);
    totlPrice = parseFloat(totlPrice) + parseFloat(operatorreview);
    totlPrice = parseFloat(totlPrice).toFixed(2);
    
    
   
     $('#printigmaterialdiv').html(productprice);
     $('#adtnlcostdiv').html(processingCost);
     $('#singleproductcostdiv').html(singleproductPrice);
     $('#totalproductcostdiv').html(totlPrice);
     $('.totalnoofproductbigfrmtdiv').html(noOfProducts);
     $('#bigtotlprice').val(totlPrice);
    }    
    
}
/////////////////////Multiplication Formula   Big Format    End


/*******************base processing function ************/
function baseprocessingPrice(){   
   //debugger;
    var material_id_selected_name = $("#materialId option:selected").val();
    var material_prices = "#mtid"+material_id_selected_name;
    var noOfProducts = $('#productQuanity').val();
    var materialdet = $(material_prices).val();
    var productheight = $('#productHeight').val();   
    var productwidth = $('#productWidth').val();
    var extradef = $('#extradefValue').val();
    var addtnlPricePerCopy = $('#additonal_price_copy').val();
    var detCostCopy = $('#det_cost_copy').val();
    var priceCalculationFunction = $('#price_calculation_function').val();
    var basePrice = $('#base_price').val();
    var dimensionFinder = $('input[name=dimensions]:checked').val();
    var shippingCost =  $('input[name=htotalshippingprice]').val();
   // alert(shippingCost);
    var baseaccessories =  $('input[name=baseaccessories]:checked').val();
    var extraaccessories =  $('input[name=extraaccessories]:checked').val();
    var promotionalflag = $('#promotionalFlag').val();
    var promotype = $('#promotionalType').val();
    var promoval = $('#promotionalValue').val();
    var bsprcsid = $('input[name=baseprocessing]:checked').val();
    var bsrpcsgetId = "#bsprcs"+bsprcsid;
    var processingValues = $(bsrpcsgetId).val();
    var Pcost = 0;
    var dim_dets = "";
    var dimension_details = 0+"*"+0; 
    var material_price = 0;
    var matheight = 0;
    var matwidth = 0;
    
 /*   var productheight = 0;   
    var productwidth = 0;
    var noOfProducts = 1;
    var material_id_selected_name = $("#materialId option:selected").val();
    var material_prices = "#mtid"+material_id_selected_name;
    var materialdet = $(material_prices).val();
    noOfProducts = $('#productQuanity').val();
    var material_price = $(material_prices).val();
    productheight = $('#productHeight').val();   
    productwidth = $('#productWidth').val();
    var extradef = $('#extradefValue').val();
    var addtnlPricePerCopy = $('#additonal_price_copy').val();
    var detCostCopy = $('#det_cost_copy').val();
    var basePrice = $('#base_price').val();
    var dimensionFinder = $('input[name=dimensions]:checked').val();
    var bsprcsid = $('input[name=baseprocessing]:checked').val();
    var bsrpcsgetId = "#bsprcs"+bsprcsid;
    var processingValues = $(bsrpcsgetId).val();
    var Pcost = 0;
    var dim_dets = "";
    var dimension_details = 0+"*"+0; */
      if(materialdet != null){
        var matdetails = materialdet.split("+");
        material_price = matdetails[0];
        material_price = parseFloat(material_price);
      }

     
       if(productheight == ""){
         productheight = 1;
     }
     
     if(productwidth == ""){
         productwidth  = 1;
     }
     
    
      if(noOfProducts == ""){
         noOfProducts = 1;
     }
  
    
   if((dimensionFinder != null) && (dimensionFinder != "custom")){
        dim_dets = "#bd"+dimensionFinder;
        dimension_details = $(dim_dets).val(); 
        var res = dimension_details.split("*");
        productwidth = res[0];
        productheight = res[1];
     }
     
  
     
     if((extradef == null)||(extradef == "")){
      extradef = 1;
     }
     
      if(processingValues != null){
        var processres = processingValues.split("+");
        var processFunction = processres[1];
        var processBaseprice = processres[0];
        var minCopy = processres[2];
        var minLength = processres[3];
        var minHeight = processres[4];
        var dimchk = processres[5];
        if(parseFloat(dimchk) == 1){
               if(parseFloat(productheight) < parseFloat(minHeight)){ 
                   productheight = minHeight;
                  }
               if(parseFloat(productwidth) < parseFloat(minLength)){ 
                   productwidth = minLength;
                 }
        }
        
      }
      
     if(processingValues == null){
      processFunction = 7; 
      processBaseprice = 0;
     }
    if(minCopy == 1){
          if(noOfProducts == ""){
         noOfProducts = 1;
     }
     if((parseFloat(productheight * productwidth * noOfProducts) < 10000)){
         productheight = 100;
         productwidth = 100;
     }
    }
     
  
      productheight = parseFloat(productheight/100);
      productwidth  = parseFloat(productwidth/100);  

    if (processFunction == 7) 
    { /* Base Price*/
      Pcost = processBaseprice;
    }
     
      if (processFunction == 10) 
    { /* Height Formula*/
        Pcost = parseFloat(processBaseprice * productheight); 
        }
    if (processFunction == 9) 
    { /* Width Formula*/ 
       Pcost = parseFloat(processBaseprice * productwidth); 
    }
    if (processFunction == 8) 
    {  /* area formula */
       
        Pcost = parseFloat(processBaseprice * productheight * productwidth);  
     }
      if (processFunction == 12) 
    { /*Twice Height Formula*/
       Pcost = parseFloat(processBaseprice * productheight *2); 
     
      }
    if (processFunction == 11) 
    { /*Twice Width Formula*/
        Pcost = parseFloat(processBaseprice * productwidth *2);
      
     }
    if (processFunction == 14) 
    {  /* perimeter formula */
      var lh = parseFloat(productheight + productwidth);
      var perimeter = parseFloat(lh * 2);
       Pcost = parseFloat(processBaseprice * perimeter); 
    
     }
    
    if (processFunction == 13) 
    {  /* semiperimeter formula */
      var perimeter = parseFloat(productheight + productwidth);
       Pcost = (processBaseprice * perimeter); 
     
    }
        if (processFunction == 15) 
    { /* etra definition field with base price*/
        Pcost = parseFloat(processBaseprice) * parseFloat(extradef); 
        }
     
       if (processFunction == 16) 
    { /* Width divided by  etra definition field */
       var matrlpricefrml = parseFloat(productwidth) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
        }
        
    if (processFunction == 17) 
    { /* Height divided by extradefinition feild*/ 
       var matrlpricefrml = parseFloat(productheight) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
    
    if (processFunction == 18) 
    {  /* semiperimeter divided by extradefinition feild*/
       var perimeter = parseFloat(productheight) + parseFloat(productwidth);
       var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
   
   if (processFunction == 19) 
    {  /* perimeter divided by extradefinition feild*/
       var lh = parseFloat(productheight) + parseFloat(productwidth);
       var perimeter = lh * 2;
       perimeter = perimeter/2
       var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
    
    if (processFunction == 20) 
    {  /* Extra field definition as a percentage of cost of materials */
      var matrlpricefrml = parseFloat(material_price)  / parseFloat(100);
      Pcost = parseFloat(matrlpricefrml) * parseFloat(extradef);
     }
    
   
    
    
    
    return Pcost;
}
/*******************base processing function ************/

/*******************extra processing function *************/
function extraprocessingPrice(){ 
   //debugger;
/* orginial    var productheight = 0;   
    var productwidth = 0;
    var noOfProducts = 1;
    var material_id_selected_name = $("#materialId option:selected").val();
    var material_prices = "[id='"+material_id_selected_name+"']";
    var noOfProducts = $('#productQuanity').val();
    var material_price = $(material_prices).val();
    var productheight = $('#productHeight').val();   
    var productwidth = $('#productWidth').val();
    var extradef = $('#extraDef').val();
    var addtnlPricePerCopy = $('#additonal_price_copy').val();
    var detCostCopy = $('#det_cost_copy').val();
    var basePrice = $('#base_price').val();
    var dimensionFinder = $('input[name=dimensions]:checked').val();
    var processingValues = $('input[name=extraprocessing]:checked').val();
    var materialdet = $(material_prices).val();
    var Pcost = 0;
    var dim_dets = "";
    var dimension_details = 0+"*"+0; */
    
     var material_id_selected_name = $("#materialId option:selected").val();
    var material_prices = "#mtid"+material_id_selected_name;
    var noOfProducts = $('#productQuanity').val();
    var materialdet = $(material_prices).val();
    var productheight = $('#productHeight').val();   
    var productwidth = $('#productWidth').val();
    var addtnlPricePerCopy = $('#additonal_price_copy').val();
    var detCostCopy = $('#det_cost_copy').val();
    var priceCalculationFunction = $('#price_calculation_function').val();
    var basePrice = $('#base_price').val();
    var dimensionFinder = $('input[name=dimensions]:checked').val();
    var shippingCost =  $('input[name=shipping]:checked').val();
    
    var baseaccessories =  $('input[name=baseaccessories]:checked').val();
    var extraaccessories =  $('input[name=extraaccessories]:checked').val();
    var promotionalflag = $('#promotionalFlag').val();
    var promotype = $('#promotionalType').val();
    var promoval = $('#promotionalValue').val();
    var extprcsid = $('input[name=extraprocessing]:checked').val();
    var extrpcsgetId = "#extprcs"+extprcsid;
    var processingValues = $(extrpcsgetId).val();
    var extrdefId = "#epextradefValue"+extprcsid;
    var extradef = $(extrdefId).val();
    var Pcost = 0;
    var dim_dets = "";
    var dimension_details = 0+"*"+0;
    var material_price = 0;
    var matheight = 0;
    var matwidth = 0;
    
    
      if(materialdet != null){
        var matdetails = materialdet.split("+");
        material_price = matdetails[0];
        matheight = matdetails[2];
        matwidth = matdetails[3];
        material_price = parseFloat(material_price);
      }
    
     if(productheight == ""){
         productheight = 1;
     }
     
     if(productwidth == ""){
         productwidth  = 1;
     }
     
    
      if(noOfProducts == ""){
         noOfProducts = 1;
     }
  
    
 if((dimensionFinder != null) && (dimensionFinder != "custom")){
        dim_dets = "#bd"+dimensionFinder;
        dimension_details = $(dim_dets).val(); 
        var res = dimension_details.split("*");
        var productwidth = res[0];
        var productheight = res[1];
     }
     
  
     
     if((extradef == null)||(extradef == "")){
      extradef = 1;
     }
     
      if(processingValues != null){
        var extprcssres = processingValues.split("+");
        var processFunction = extprcssres[1];
        var processBaseprice = extprcssres[0];
        var minCopy = extprcssres[2];
        var minLength = extprcssres[3];
        var minHeight = extprcssres[4];
        var dimchk = extprcssres[5];
        if(parseFloat(dimchk) == 1){
               if(parseFloat(productheight) < parseFloat(minHeight)){ 
                   productheight = minHeight;
                  }
               if(parseFloat(productwidth) < parseFloat(minLength)){ 
                   productwidth = minLength;
                 }
        }
      }
     if(processingValues == null){
      processFunction = 7; 
      processBaseprice = 0;
     }
    if(minCopy == 1){
          if(noOfProducts == ""){
         noOfProducts = 1;
     }
     if((parseFloat(productheight * productwidth * noOfProducts) < 10000)){
         productheight = 100;
         productwidth = 100;
     }
    }
     
  
      productheight = parseFloat(productheight/100);
      productwidth  = parseFloat(productwidth/100);  

    if (processFunction == 7) 
    { /* Base Price*/
      var matrlpricefrml = processBaseprice; 
      var Pcost = matrlpricefrml;
     
    }
     
      if (processFunction == 10) 
    { /* Height Formula*/
       var Pcost = parseFloat(processBaseprice * productheight); 
        }
    if (processFunction == 9) 
    { /* Width Formula*/ 
      var Pcost = parseFloat(processBaseprice * productwidth); 
    }
    if (processFunction == 8) 
    {  /* area formula */
       
       var Pcost = parseFloat(processBaseprice * productheight * productwidth);  
     }
      if (processFunction == 12) 
    { /*Twice Height Formula*/
      var Pcost = parseFloat(processBaseprice * productheight *2); 
     
      }
    if (processFunction == 11) 
    { /*Twice Width Formula*/
       var Pcost = parseFloat(processBaseprice * productwidth *2);
      
     }
    if (processFunction == 14) 
    {  /* perimeter formula */
      var lh = parseFloat(productheight + productwidth);
      var perimeter = parseFloat(lh * 2);
      var Pcost = parseFloat(processBaseprice * perimeter); 
    
     }
    
    if (processFunction == 13) 
    {  /* semiperimeter formula */
      var perimeter = parseFloat(productheight + productwidth);
      var Pcost = (processBaseprice * perimeter); 
     
    }
       if (processFunction == 15) 
    { /* etra definition field with base price*/
        Pcost = parseFloat(processBaseprice) * parseFloat(extradef); 
        }
     
       if (processFunction == 16) 
    { /* Width divided by  etra definition field */
       var matrlpricefrml = parseFloat(productwidth) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
        }
        
    if (processFunction == 17) 
    { /* Height divided by extradefinition feild*/ 
       var matrlpricefrml = parseFloat(productheight) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
    
    if (processFunction == 18) 
    {  /* semiperimeter divided by extradefinition feild*/
       var perimeter = parseFloat(productheight) + parseFloat(productwidth);
       var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
   
   if (processFunction == 19) 
    {  /* perimeter divided by extradefinition feild*/
       var lh = parseFloat(productheight) + parseFloat(productwidth);
       var perimeter = lh * 2;
       perimeter = perimeter/2
       var matrlpricefrml = parseFloat(perimeter) / parseFloat(extradef); 
       Pcost = parseFloat(matrlpricefrml) * parseFloat(processBaseprice);
    }
    
    if (processFunction == 20) 
    {  /* Extra field definition as a percentage of cost of materials */
      var matrlpricefrml = parseFloat(material_price) / parseFloat(100);
      Pcost = parseFloat(matrlpricefrml) * parseFloat(extradef);
     }
    
    return Pcost;
}

/****************** extra processing end *******************/


////////////////////////////////////////////////
//               Small Format        
///////////////////////////////////////////////

/************* On Load **********************/
     updateValuesSmall();
/**************** On Load ********************/
 


 $("#sproductQuanity").blur(function(){  
     updateValuesSmall();
     });   
   
/*************** On Change OF MATERIAL START ****************/      
$("#smaterialId").change(function() { 
   updateValuesSmall();
    });
/*************** On Change MATERIAL END *********************/         

/************* On change Processing START *************/            
   $("input[name=processing]:radio").change(function () { 
    updateValuesSmall();      
   });   
/*********** On change Processing END ********************/

/************* On change Shipping START *************/            
   $("input[name=shipping]:radio").change(function () {
    updateValuesSmall();      
   });   
/*********** On change Shipping END ********************/

/************* On change Dimesions START *************/            
   $("input[name=sdimensions]:radio").change(function () {
    updateValuesSmall();      
   });   
/*********** On change Dimensions END ********************/


/**********Multiplication Formula for Big Format Start *************/

function updateValuesSmall(){
   //debugger;
    
//updateShipping()   
  
var bsprcj = $("#basePricesmall").val();
var frmidj = $("#formid").val(); 
var mtrlidj = $("#smaterialId option:selected").val();
var dmsnidj = $('input[name=sdimensions]:checked').val();
var prcsngidj =  $('input[name=processing]:checked').val();
var noofproductsj = $("#sproductQuanity").val();
var spromotypej = $("#spromotype").val();
var spromopricej = $("#spromoprice").val();
var shippingCostj =  $('#shippingprice').val();
if((spromotypej == null )||(spromotypej == "")){
    spromotypej = "value";
    spromopricej = 0;
}

if((spromopricej == null )||(spromopricej == "")){
    spromopricej = 0;
}

if((bsprcj == null )||(bsprcj == "")){
    bsprcj = 0;
}
var price_path = $('input[name=price_path]').val();
//url: "http://localhost:81/Meprintv3/sites/all/themes/meprint/get-price.php"
	$.ajax({
		url: price_path,
		data: { 
                    bsprc: bsprcj, 
                    frmid: frmidj, 
                    mtrlid : mtrlidj, 
                    prcsngid : prcsngidj, 
                    dmsnid: dmsnidj, 
                    noofproducts: noofproductsj,
                    spromotype: spromotypej,
                    spromoprice: spromopricej,
                    shippingCost : shippingCostj
                },
		dataType: "html",
		type: "POST",
		success: function(data){			
			//console.log(data);
                       var res = data.split("__");  
                       
                       if(res[4] != res[5]){
                        $('#spromotd').show();   
                        $('#spromodiv').html(res[4]);
                           }else{
                        $('#spromotd').hide();
                           }
                       
                      $('#sprintigmaterialdiv').html(res[1]);
                      $('#sadtnlcostdiv').html(res[2]);
                      $('#ssingleproductcostdiv').html(res[3]);
                      $('#stotalproductcostdiv').html(res[5]);
                      $('.stotalproductcostdivqnty').html(res[0]);
                      $('#smalltotlprice').val(res[5]);
		}		
	});     

}

  }  
};    
})(jQuery);


