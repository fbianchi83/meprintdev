(function($) {
Drupal.behaviors.myBehavior = {   
  attach: function (context, settings) {  
      
     
    $('span.downloadable').each(function(){  
        var $this = $(this);
        $this.wrap('<a href="' + $this.attr('src') + '" download class="downloadtemp"/>')
    });    
    
      $(".deletecart").click(function() {   
        if(confirm("Sei sicuro di voler eliminare questo prodotto?")) {  
           var cart_item_id = $(this).attr('url');         
           $.ajax({
                type:"POST",
                data:"cart_item_id="+cart_item_id,
                url:"/delete-item/"+cart_item_id,
                success: function(result) {  
                  $(".cart_"+cart_item_id).fadeOut();
                  window.location.reload(true);
                }
            });
        } else {
             return false;
        }
      });
      
      $(".deletecartwishlist").click(function(){
          if(confirm("Are You Sure You Want to Delete Record!")) {  
              var k = $(this).attr('url');
              var jk = $('input[name="path"]').val();
              $.ajax({
              type:"POST",
              data:"wish="+k,
              url:jk,
              success: function(result) {
                 if(result === 'true') { 
                    $(".deleteTable_"+k).hide();
                    location.reload();
                 }
              }
          });
          }
      });
      
       
       
       
       
      
    /********************************Coupan code ***************/
    
    //$(".discountyes").hide();
    $(".dishide").hide();
       $('input[name="discountcheck"]').click(function () {
           if ($('input[name="discountcheck"]').is(':checked')) {
               $(".discountyes").show();
               //$(".discountyes").show();
               $(".dishide").show();
           }
           else {
               var cart_id = $('input[name="cart_id"]').val();
               var totalprice = parseFloat($('input[name="totalprice"]').val());
                $.ajax({
                    type:'POST',
                    url:"/discount-remove",
                    data:'cartid='+cart_id,
                    dataType:'json',
                    success: function(result) {  
                        $('input[name="dicountamount"]').val(0);
                        $('input[name="finalamount"]').val(totalprice);
                        $('.valid').hide();
                        $('input[name="coupancode"]').val('');
                        //$(".discountyes").hide();
                        //$(".discountyes").hide();
                        $(".dishide").hide();
                  }
                });
           }
       });
       
       $("#discountbutton").click(function(){
            
           var coupancode = $('input[name="coupancode"]').val();
           var totalprice = parseFloat($('input[name="totalprice"]').val());
           var shipprice = parseFloat($('input[name="ship"]').val());
           var packprice = parseFloat($('input[name="pack"]').val());
           var ivapercentage = parseFloat($('input[name="ivapercentage"]').val());
           //var path = $('input[name="couponpath"]').val();
           var cart_id = $('input[name="cart_id"]').val();
         
           var iva = $('input[name="iva"]').val();
           var session_id = $('input[name="session_id"]').val();
           
           $.ajax({
               type:'POST',
               url:"/discount",
               data:'coupancode='+coupancode+'&totalprice='+totalprice+'&ivapercentage='+ivapercentage+'&shipprice='+shipprice+'&packprice='+packprice+'&session='+session_id+'&cartid='+cart_id,
               dataType:'json',
               success: function(result) {  
                   
                if(result !=0) {
                   $.each(result, function(i,item ) {
                     var discountprice =  parseFloat(item.discountprice);
                     var productprice = parseFloat(item.productprice);
                     var ivaprice = parseFloat(item.iva);
                     var finalamount = parseFloat(totalprice) + shipprice + packprice + parseFloat(item.iva) - parseFloat(discountprice)  ;                     
                     $('input[name="dicountamount"]').val( discountprice.toFixed(2) );
                     $('input[name="finalamount"]').val(finalamount.toFixed(2));
                     $('.dicountamount').text( discountprice.toFixed(2) );
                     $('.ivapriceval').text( ivaprice.toFixed(2) );
                     $('.taxableprice').text( (finalamount - ivaprice).toFixed(2) );
                     $('.finalamount').text(finalamount.toFixed(2));
                     $('input[name="productprice"]').val(productprice.toFixed(2));
                     $('input[name="iva"]').val(ivaprice.toFixed(2));
                     $('input[name="discount_id"]').val(item.discountid);
                    
                 });
                  $('.valid').show();
                  $('.invalid').hide();
                  $(".dishide").show();
                  //$(".disshow").removeClass('bg-total');
                  //$(".disshow .producttotalprice").addClass('discountlabel');
                } else {
                  $('.invalid').show();
                  $('.valid').hide();
                  $(".dishide").hide();
                  $(".disshow:last-child").addClass('bg-total');
                  $(".disshow .producttotalprice").removeClass('discountlabel');                  
                  $('input[name="finalamount"]').val(0);
                  $('.dicountamount').text(0);
                  $('input[name="dicountamount"]').val(0);
                  $('input[name="finalamount"]').val(totalprice);
                }
                  
              }

               
           });
           
       });
       
      /*
      $(".ImageUploadBig").change(function (e) { 
          //debugger;
       
        var id = $(this).attr('name');      
        var strnumber = id.split("image_");
        
          var F = this.files;
          if(F && F[0]) for(var i=0; i<F.length; i++) readImageBig(F[i],strnumber[1],id);
          
      });*/
      
       function readImageBig(file,divID,id) { 
          // debugger;
            var productwidth = 0;
            var productheight = 0;
             var displaywidth = 100;
             var displayheight = 100;
             var dimensionFinder ;
             dimensionFinder = $('input[name=dimensions]:checked').val();
            var hashedid = '#'+id;
           if((dimensionFinder != null) && (dimensionFinder != "custom")) {
                var dim_dets = "#bd"+dimensionFinder;
                var  dimension_details = $(dim_dets).val(); 
                var res = dimension_details.split("*");
                productwidth = res[0];
                productheight = res[1];
          } else if(dimensionFinder == "custom") {
                productheight = $('#productHeight').val();   
                productwidth = $('#productWidth').val();
          }else if(dimensionFinder == ""){
                productheight = 0;   
                productwidth = 0;
          } 
          //console.log( dimensionFinder );
           
            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(file);  
            reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                    var w = this.width,
                        h = this.height,
                        t = file.type,                           // ext only: // file.type.split('/')[1],
                        n = file.name,
                        s = ~~(file.size/1024) +'KB';
               /*********************** dimension calculation start************/ 
                var productarea = parseFloat(productheight) * parseFloat(productwidth);
               
         var productno = parseFloat(productarea)/ 10000;
         productno = Math.ceil(productno);
         if(productno == 0){
             productno = 1;
         }
         var minwdth = 2400 * parseFloat(productno);
         var maxwdth = 7500 * parseFloat(productno)
         
        
         if (w >= minwdth && w <= maxwdth) {
             $(".dvPreview_"+divID).html('');
                 $("#image_"+divID).val(null);
                 $(".ImageError_"+divID).html("Please choose image with widht above 2400px");
              // alert("Please choose a proper image");
                 return;
         /***********************dimension calculation end****************/   
         }else {
          /***************** Calculation image ratio start *********************/
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);
            
            
            if(provideimgaspect == dbimgaspect) {
              $(".ImageError_"+divID).html("");
              $(".dvPreview_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" class="uploadproductimage--choose" ><a class="uploadproductimage__remove"><span class="fa fa-trash"></span> Rimuovi immagine</a>').addClass('uploadproductimage--selected');
              //nascondo il bottone EDITOR e modifico il testo del bottone tema
              $('#custom-file-theme, #custom-file-editor').hide();
    
    
              //  $(".dvPreview_"+divID).html('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
            } else {  
                $(".ImageError_"+divID).html("Image Ratios between "+productwidth+"*"+productheight);
                 // alert("Image ratios are not same");
                   $("#image_"+divID).val(null);
                
                   $(".dvPreview_"+divID).html('');
               
                
            }
        }
         /*************** Calculation image ratio end  *********************/
                      //  $(".dvPreview_"+divID).append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
                };
                image.onerror= function() {
                      $(".dvPreview_"+divID).html('');
                      $(".ImageError_"+divID).html('Upload .jpg,.gif.png only');
                       $("#image_"+divID).val(null);
                       return;
                };      
            };
         
         
       }
       /*******************Image Upload big end *******************************/
       /*****************Image Upload small***********************************/
       
       /*
        $(".ImageUploadsmall").change(function(e) { 
             // alert("fdfd");
            var id = $(this).attr('name');      
            var strnumber = id.split("image_");
            var F = this.files;


            if(F && F[0]) for(var i=0; i<F.length; i++) readImageSmall(F[i],strnumber[1],id);
       });*/
       
       
       function  readImageSmall(file,divID,id) { 
            var productwidth = 0;
            var productheight = 0;
             var displaywidth = 100;
             var displayheight = 100;
             var dimensionFinder ;
             dimensionFinder = $('input[name=dimensions]:checked').val();
            var hashedid = '#'+id;
             if((dimensionFinder != null)) {
                var dim_dets = "#bd"+dimensionFinder;
                var  dimension_details = $(dim_dets).val(); 
                var res = dimension_details.split("*");
                productwidth = res[0];
                productheight = res[1];
          } else if(dimensionFinder == ""){
                productheight = 0;   
                productwidth = 0;
          } 
          var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(file); 
            reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                    var w = this.width,
                        h = this.height,
                        t = file.type,                           // ext only: // file.type.split('/')[1],
                        n = file.name,
                        s = ~~(file.size/1024) +'KB';
               /*********************** dimension calculation start************/ 
                var productarea = parseFloat(productheight) * parseFloat(productwidth);
               
         var productno = parseFloat(productarea)/ 10000;
         productno = Math.ceil(productno);
         if(productno == 0){
             productno = 1;
         }
         var minwdth = 2400 * parseFloat(productno);
         var maxwdth = 7500 * parseFloat(productno)
         
         //if((w < minwdth) || (w > maxwdth)){
         if (w >= minwdth && w <= maxwdth) {
               $(".ImageError_"+divID).html("Please choose image with widht above 2400px");
                  //alert("Please choose a proper image");
                  $(".dvPreviewSmall_"+divID).html('');
                  $("#image_"+divID).val(null);
                   return;
         /***********************dimension calculation end****************/   
         }else {
          /***************** Calculation image ratio start *********************/
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);
             
            
            if(provideimgaspect == dbimgaspect) { 
               // alert("dsfdsfds");
                $(".ImageError_"+divID).html("");
                //$(".dvPreviewSmall_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" >');
                $(".dvPreviewSmall_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" class="uploadproductimage--choose" ><a class="uploadproductimage__remove"><span class="fa fa-trash"></span> Rimuovi immagine</a>').addClass('uploadproductimage--selected');
                
                //nascondo il bottone EDITOR e modifico il testo del bottone tema
                $('#custom-file-theme, #custom-file-editor').hide();
              
            } else {  
                // $(".ImageError_"+divID).html("Image Ratios between "+productwidth+"*"+productheight);
                 $(".ImageError_"+divID).html("Image ratios between "+productwidth+"*"+productheight);
                  //alert("Image ratios are not same");
                 $("#image_"+divID).val(null);
                  $(".dvPreviewSmall_"+divID).html('');
                
            }
        }
         /*************** Calculation image ratio end  *********************/
                      //  $(".dvPreview_"+divID).append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
                };
                image.onerror= function() { 
                       $(".dvPreviewSmall_"+divID).html('');
                   // alert('Invalid file type: '+ file.type);
                    $(".ImageError_"+divID).html('Upload .jpg,.gif.png only');
                     $("#image_"+divID).val(null);
                      return;
                };      
            };
         
          
          
          
       }
        /**************************** Image upload small End***************/ 
        
        /********************Image Existed Big Start ******************************/
           
           /*
           $(".existedBig").change(function(e){ 
                var id = $(this).attr('name');      
                var strnumber = id.split("upload_");
                var F = this.files;
                if(F && F[0]) for(var i=0; i<F.length; i++) existedImageBig(F[i],strnumber[1],id);
           });*/
           
            function existedImageBig(file,divID,id) { 
           
            var productwidth = 0;
            var productheight = 0;
             var displaywidth = 100;
             var displayheight = 100;
             var dimensionFinder ;
             dimensionFinder = $('input[name=dimensions]:checked').val();
            var hashedid = '#'+id;
           if((dimensionFinder != null) && (dimensionFinder != "custom")) {
                var dim_dets = "#bd"+dimensionFinder;
                var  dimension_details = $(dim_dets).val(); 
                var res = dimension_details.split("*");
                productwidth = res[0];
                productheight = res[1];
          } else if(dimensionFinder == "custom") {
                productheight = $('#productHeight').val();   
                productwidth = $('#productWidth').val();
          }else if(dimensionFinder == ""){
                productheight = 0;   
                productwidth = 0;
          } 
       
           
            var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(file);  
            reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                    var w = this.width,
                        h = this.height,
                        t = file.type,                           // ext only: // file.type.split('/')[1],
                        n = file.name,
                        s = ~~(file.size/1024) +'KB';
               /*********************** dimension calculation start************/ 
         var productarea = parseFloat(productheight) * parseFloat(productwidth);
         var productno = parseFloat(productarea)/ 10000;
         productno = Math.ceil(productno);
         if(productno == 0){
             productno = 1;
         }
         var minwdth = 2400 * parseFloat(productno);
         var maxwdth = 7500 * parseFloat(productno)
         
        
         if (w >= minwdth && w <= maxwdth) { 
             $(".dbImage_"+divID).show();//show the existed image
             $(".dvPreviewExisted_"+divID).html('');
             $("#upload_"+divID).val(null);
              $(".ImageError_"+divID).html("Please choose image with widht above 2400px");
            // alert("Please choose a proper image");
             return false;
         /***********************dimension calculation end****************/   
         }else {
          /***************** Calculation image ratio start *********************/
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);
            
            
            if(provideimgaspect == dbimgaspect) {
                $(".dbImage_"+divID).hide();//hide the existed image
                $(".ImageError_"+divID).html("");
                $(".dvPreviewExisted_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" >');
               
             
            } else {  
                   $(".ImageError_"+divID).html("Image ratios between "+productwidth+"*"+productheight);
                   $(".dbImage_"+divID).show();//show the existed image
                   $("#upload_"+divID).val(null);
                   $(".dvPreviewExisted_"+divID).html('');
               
                
            }
        }
         /*************** Calculation image ratio end  *********************/
                      //  $(".dvPreview_"+divID).append('<img src="'+ this.src +'"> '+w+'x'+h+' '+s+' '+t+' '+n+'<br>');
                };
                image.onerror= function() { 
                    
                    //alert('Invalid file type: '+ file.type);
                      $(".ImageError_"+divID).html('Upload .jpg,.gif.png only');
                        $("#upload_"+divID).val(null);
                         $(".dvPreviewExisted_"+divID).html('');
                       return;
                };      
            };
         
         
       }
        
         
           
       /********************Image Existed Big End ******************************/ 
         /********************Image Existed Small start ******************************/
        // $(".existedSmall").on('change', function() { 
        
        /*
        $(".existedSmall").change(function(){
            
            var id = $(this).attr('name');      
            var strnumber = id.split("upload_");
            var F = this.files;
            if(F && F[0]){					
                for(var i=0;i<F.length;i++){existedImageSmall(F[i],strnumber[1],id); }
            }
       }); */
           
           
           function  existedImageSmall(file,divID,id) { 
            
            var productwidth = 0;
            var productheight = 0;
             var displaywidth = 100;
             var displayheight = 100;
             var dimensionFinder ;
             dimensionFinder = $('input[name=sdimensions]:checked').val();
            var hashedid = '#'+id;
             if((dimensionFinder != null)) {
                var dim_dets = "#bd"+dimensionFinder;
                var  dimension_details = $(dim_dets).val(); 
                var res = dimension_details.split("*");
                productwidth = res[0];
                productheight = res[1];
          } else if(dimensionFinder == ""){
                productheight = 0;   
                productwidth = 0;
          } 
          var reader = new FileReader();
            var image  = new Image();
            reader.readAsDataURL(file); 
            reader.onload = function(_file) {
                image.src    = _file.target.result;              // url.createObjectURL(file);
                image.onload = function() {
                    var w = this.width,
                        h = this.height,
                        t = file.type,                           // ext only: // file.type.split('/')[1],
                        n = file.name,
                        s = ~~(file.size/1024) +'KB';
               /*********************** dimension calculation start************/ 
                var productarea = parseFloat(productheight) * parseFloat(productwidth);
               
         var productno = parseFloat(productarea)/ 10000;
         productno = Math.ceil(productno);
         if(productno == 0){
             productno = 1;
         }
         var minwdth = 2400 * parseFloat(productno);
         var maxwdth = 7500 * parseFloat(productno)
         
       
         if (w >= minwdth && w <= maxwdth) {
                  $(".dbimagesmall_"+divID).show();
                   $(".ImageError_"+divID).html("Please choose image with widht above 2400px");
                
                  $(".dvPreviewExistedSmall_"+divID).html('');
                  $("#upload_"+divID).val(null);
                  
                  return;
        
         }else {
         
            var provideimgaspect = parseFloat(w) /parseFloat(h);
            var dbimgaspect = parseFloat(productwidth) /parseFloat(productheight);
            provideimgaspect = parseFloat(provideimgaspect).toFixed(2);
            dbimgaspect = parseFloat(dbimgaspect).toFixed(2);
             
            
            if(provideimgaspect == dbimgaspect) {
             $(".dbimagesmall_"+divID).hide();
               $(".ImageError_"+divID).html("");
                $(".dvPreviewExistedSmall_"+divID).html('<img src="'+ this.src +'" style="width:100px;height:100px; margin-bottom:5px;" >');
                
             
            } else { 
               $(".dbimagesmall_"+divID).show();
                 $(".ImageError_"+divID).html("Image ratios between "+productwidth+"*"+productheight);
                $("#upload_"+divID).val(null);
                  $(".dvPreviewExistedSmall_"+divID).html('');
                
            }
        }
       
                };
                image.onerror= function() {
                   $(".dvPreviewExistedSmall_"+divID).html('');
                     $(".ImageError_"+divID).html('Upload .jpg,.gif.png only');
                      $("#upload_"+divID).val(null);
                      return;
                };      
            };
         
          
          
          
       }
           
           
           
         /********************Image Existed Small end ******************************/ 
       //existedSmall
       
       /*
       $('input[type=radio][name=dimensions]').change(function() {
         var count = $('input[name=imagecount]').val(); 
         
          for(var i=1;i<=count;i++) {
                 $(".dvPreview_"+i).html('');
                 $("#image_"+i).val(null);
                 $(".ImageError_"+i).html("");
          }
          
          $('input[name^="jsbig"]').each(function() { 
              var k = $(this).val();
              $(".dbImage_"+k).hide();
              $(".dvPreviewExisted_"+k).html('');
              $("#upload_"+k).val(null);
           
          });
          
       });*/

      /******big Format End ************/
      /*
       $.validator.addClassRules({
           ImageUploadBig: {
               required: true
           }

       });

       $.validator.addClassRules({
           ImageUploadsmall: {
               required: true
           }

       });*/

       

      //validation checkbox
      // SPOSTATO IN bigformat-validations.js
      /*$('.extra-check').parent().each(function(key, value){
        $(this).find('input').addClass('check'+key);
        $(this).find('input').addClass('checkrequired');

        //#epextraprcsdiv1 > div:nth-child(3) > label

        var name = 'check'+key;
        $.validator.addClassRules(name,{
            require_from_group: [1, '.'+name],
            messages: {require_from_group: "Riempi almeno uno di questi campi."}
        });
      });

        $.validator.addClassRules('epextadefValue',{
          required: true,
          number: true
        });


       $("#bigproceed").click(function(){
         var material_id = $(".material_class:checked").val();
         
           
         $("#product-big-format").validate({
          ignore: ':hidden',
          rules: {
           quanitity: {
             required: true,
             number: true,
             check_b: true
           },
           productHeight: {
             number: true,
             min: [$('input[name="dbheight1-'+material_id+'"]').val()],

           },
           productWidth: {
             number: true,
             min: [$('input[name="dbwidth1-'+material_id+'"]').val()]

           },
           extradefValue: {
             number: true
           }


          }, messages: {
           quanitity: {
             required: "Inserire la quantità",
             number: "Inserire un numero",
             check_b: "Inserire una quantità minima di " + $('input[name="minQuantity"]').val()
           },
           productHeight: {
             number: "Inserire un numero",
             range: "Inserire un'altezza tra " + $('input[name="dbheight1-'+material_id+'"]').val() + " e " + $('input[name="dbheight2-'+material_id+'"]').val() + " cm"
           },
           productWidth: {
             number: "Inserire un numero",
             range: "Inserire una larghezza tra " + $('input[name="dbwidth1-'+material_id+'"]').val() + " e " + $('input[name="dbwidth2-'+material_id+'"]').val()  + " cm"
           },
           extradefValue: {
             number: "Inserire un numero"
           }
         }
        });
       });
      
      
  
       $.validator.addMethod("check_b", function (value, element, param) {

           var val_a = $('input[name="minQuantity"]').val(); //5 
           return (parseInt(value) >= parseInt(val_a));

       }, "Your error message.");


       $.validator.addMethod("check_s", function (value, element, param) {

           var val_a = $('input[name="minQuantitySmall"]').val(); //5   
          
           return (parseInt(value) >= parseInt(val_a));

       }, "Your error message.");

       */


       /*$("#product-small-format").validate({
           rules: {
               squanitity: {
                   required: true,
                   number: true,
                   check_s: true
               }
           }, messages: {
               squanitity: {
                   required: true,
                   number: "Please Enter Numbers only",
                   check_s: "Please Enter Quantity  More than Are Equal To " + $('input[name="minQuantitySmall"]').val()
               }
           }
       });*/

       $("#edit-cart-small").validate({
           rules: {
               squanitity: {
                   required: true,
                   number: true,
                   check_s: true
               }
           }, messages: {
               squanitity: {
                   required: true,
                   number: "Please Enter Numbers only",
                    check_s: "Please Enter Quantity  More than Are Equal To "+ $('input[name="minQuantitySmall"]').val()
               }
           }
       });

       $("#edit-cart-big").validate({
           rules: {
               quanitity: { required: true, number: true, check_b: true },
               productHeight: {
                   number: true,
                   min: [$('input[name="height1"]').val()]
               },
               productWidth: {
                   number: true,
                   min: [$('input[name="dbwidth1"]').val()]
               },
               extadefValue: { number: true },
              

           }, messages: {
               quanitity: {
                   required: "Please Enter Quantity",
                   number: "Please Enter Numbers only",
                   check_b: "Please Enter Quantity  More than Are Equal To " + $('input[name="minQuantity"]').val(),
               },
               productHeight: {
                   number: "Please Enter Numbers only",
                   range: "Please Enter width between " + $('input[name="height1"]').val() + "-" + $('input[name="height2"]').val(),
               },
               productWidth: {
                   number: "Please Enter Numbers only",
                   range: "Please Enter width between " + $('input[name="dbwidth1"]').val() + "-" + $('input[name="dbwidth2"]').val(),
               },
               extadefValue: { number: "Please Enter Numbers only" },
              
           }
       });
       
       
       
   
       
 }  
};    
})(jQuery);