<?php
/***************************************************************************************/
/***************************************************************************************/

  /*propel installation*/

  // setup the autoloading
  require_once '/home/meprint/sites/all/libraries/propel/vendor/autoload.php';
  // setup Propel
  require_once '/home/meprint/sites/all/libraries/propel/generated-conf/config.php';

  /**
   * Root directory of Drupal installation.
   */
  define('DRUPAL_ROOT', '/home/meprint/');
  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/***************************************************************************************/


  if(isset($_POST['mat_id'])){
    $mat_id = $_POST['mat_id'];
    $form_id = $_POST['form_id'];
    
    $fixed_costs = MP\MaterialsFixedCostsQuery::create()->filterByMaterialId($mat_id)->find();
    
    $num_fixed_costs = count($fixed_costs);
    
    if($num_fixed_costs > 0){
      echo mp_front_get_material_fixed_costs($mat_id);
    }  else {
      echo mp_front_get_formone_dimensions($form_id);
    }
    
    
    
  }
