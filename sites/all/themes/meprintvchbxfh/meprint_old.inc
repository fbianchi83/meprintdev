<?php 
$promo = t('Promo');
$bestprice = t('Best Price');
$productdesc = t('Product Description');
$ordertitle = t('ORDER THIS PRODUCT');
$quanitity = t('Quantity');
$dimension = t('Dimensions');
$cdimension = t('Dimension');
$custdimension = t('Custom Dimensions');
$height = t('Height');
$width = t('Width');
$tmaterial = t('Material');
$cmaterial = t('Material cart');
$processing = t('Processing');
$accessorie = t('Accessories');
$accessorie_base = t('Structure');
$Shippingtype = t('Shipping Type');
$Shippingdate = t('Shipping Date');
$shipdesc = t('Delivery times are valid throughout the national territory. The couriers selected by us, DELIVER normally the day after the SHIPMENT For the islands and some areas are difficult to reach, could serve an additional 2 days. . Print file and confirmation of payment (Credit Card, Bank Policy) sent after the specified time will cause delays in the shipment of the material.');
$summery = t('Summary');
$product = t('Product');
$qty = t('Qty');
$price =t('Price');
$costpfprintingandmaterial = t('Cost of printing and materials');
$processcost = t('Processing Cost Per Copy');
$singleproductcost = t('Complete Single Product Cost');
$totalproductcost = t('Total product cost');
$proceed = t('Proceed');
$wishlist = t('Add to Wishlist');
$download = t('Click to download product template');
$bdownload = t('Download');
$operatorreview = t('Extra operator review for 5 euros');
$operatorreview_cost = t('Operator review');
$beforpromoprice = t('Price Before Promo');
$promoval = t('Promo Value');
$extraprocessingcost = t('Extra Processing Cost');
$wishlistlogin = t('Please login to add wishlist');

$cartproduct = t('Product');
$file= t('File');
$quantity = t('Quantity');
//$price = t('Price');
$cartpagetittle= t('Cart Details');
$nocopies =t('No of Copies');
$weight = t('Weight');
$cartdate = t('Cart Date');
$totprice =t('Total Price');
$conshoping = t('Continue Shopping');
$proceedpay = t('Proceed to Payment');
$proceedcart = t('Proceed to Cart');
//$promo = t('Promo');
//$bestprice = t('Best price');
 $noproductsavail = t('No products available in your cart');
 
 $pdetails = t('Personal Details');
 $lname = t('Name');
 $lamount = t('Amount');
 $lemail = t('Email');
 $ptype= t('Payment Type');
 $lphone = t('Phone');
 $tnameid = t('Transaction Id');
 $billingandshipping = t('BILLING / SHIPPING ADDRESS');
 $billing = t('BILLING ADDRESS');
 $shipping = t('SHIPPING ADDRESS');
 $laddress = t('ADDRESS');
 $lzipcode =t('ZIPCODE');
 $llocation = t('LOCATION');
 $lprovince = t('PROVIENCE');
 $lcontry = t('COUNTRY');
 $lrecipient = t('Recipient');
 $laccountholder = t('Accountholder');
 $productdetails = t('Product Details');
 $sameasbill =t('Same as Billing Address');
 $paymentoptions = t('Payment Options');
 $paymenttype = t('Payment Type:');
 $spaymenttype = t('Select payment type');
 $p1 = t('Bank wire transfer');
 $p2 = t('Cash on Delivery');
 $p3 = t('PayPal');

 
 $finalprice = t('Final Price');
 $orderconfi = t('Order Confirmation Sucess');
 $nwishlist = t('No products available in your Wishlist');


$summary = t('Summary');
$pname = t('Product Name');
$ordrconf =t('Order sucessfully created');

$extraprocessing = t('Extra Processing');

$ExtraProcessingTop = t('Extra Processing Top');

$ExtraProcessingRight = t('Extra Processing Right');
$ExtraProcessingLeft = t('Extra Processing Left');
$ExtraProcessingBottom = t('Extra Processing Bottom');



$discounttittle = t('Discount');
$sno = t('S.No');
$date = t('Date');
$DiscountPrice = t('Discount Price');
$TotalPrice = t('Total Price');
$couponinput = t('I have an offer code or cash coupon');
$Couponcode = t('Coupon Code');
$invalidcoupon = t('Invalid Coupan Code');
$validcoupon = t('Coupon code updated successfully');
$update = t('Update');
$PartialShipping = t('Partial Shipping');
$CompleteShipping = t('Complete Shipping');
$Proceedtoship = t('Proceed to Shipping');
$uploadimagetitle = t('Upload Images');
$operatorreviewtitle = t('Operator Review');

$shippinginfo = t('SHIPPING INFO');
$orderconfirm = t('ORDER CONFIRM');
$producttotalprice = t('Product Total Price');
$discountprice = t('Discount Price');
//$finalprice = t('Total Price');
$coupanlabel = t('I have an offer code or cash coupon');
$scoupon = t('Coupon code updated successfully');
$fcoupon = t('Invalid Coupon Code');
$pshipping = t('Partial Shipping');
$cshipping = t('Complete Shipping');
$Coupon = t('Coupon');

$anonoshipingtext = t('From address will not be published on the box');     

$Home = t('Home');
$bdiscount = t('Discount');
$bcart = t('CART DEATILS');
$bshipping =t('Shipping Details');
$bordeconfirm = t('Order Confirm');
$bordersucess =t('Order Sucess');
$orderempty = t('your order is EMPTY');

$samecategory = t('You might also like');

$addcart = t('Add cart');

$side = t('Choose side');

$iva_cost = t('IVA (22%)');

$totalcost = t('Total');

$paynow = t('Pay now');

$backtocart = t('Back to cart');

$back = t('Come back');

$buy = t('Buy');

$quote_data = t('Quote data');

$note = t('Note');

$print_cart = t('Stampa carrello');


?>